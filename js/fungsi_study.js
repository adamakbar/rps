
 function s_2(target, tulisan, clear, isi){ 
                $(target).select2({
                    placeholder: tulisan,
                    allowClear: clear,
                    data: isi
                });
            }
            function bikin_sekaligus(list_id, list_data){
                for (var x in list_id){
                        s_2(list_id[x], "Pilih", true, list_data[x]);
                }
            }
            function nyala_s2(target){
                for (var x in target){
                    $(target[x]).select2("enable", true);
                }
            }
            function mati_s2(target){
                for (var x in target){
                    $(target[x]).select2("enable", false);
                }
            }
            function clear_s2(target){
                for (var x in target){
                    $(target[x]).select2("val", "");
                }
            }
            function nyala_input(target){
                for (var x in target){
                    var x =document.getElementById(target[x])
                    var x = "#" .concat(x.id);
                    $(x).prop('disabled',false);
                }
            }
            function mati_input(target){
                for (var x in target){
                    var x =document.getElementById(target[x])
                    var x = "#" .concat(x.id);
                    $(x).prop('disabled',true);
                
            }
        }

            function clear_this(target){
                for (var x in target){
                    document.getElementById(target[x]).value = "";
                }
            }
//======================================//
function disabledElement_play(id) {
    switch (id) {
    	//========== PLAY =============//
        case 'padj1' :
        	//var data = $('#' + id).select2("data");
        	//var pilihan_ = data.text;
          if(($('#' + id).val() == 1) || ($('#' + id).val() == "")){
                mati_s2(["#padj1a", "#padj1b", "#padj1c"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_s2(["#padj1a", "#padj1b", "#padj1c"]);
            }
            break;
        case 'posd1' :
          if(($('#' + id).val()==1) || ($('#' + id).val() == "")){
                mati_s2(["#posd2","#posd3","#posd4","#posd5"]);
                mati_input(["posd3a","posd4a","posd5a"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_s2(["#posd2","#posd3","#posd4","#posd5"]);
                nyala_input(["posd3a","posd4a","posd5a"]);
            }
            break;
        case 'pos2d6' :
          if(($('#' + id).val()==1) || ($('#' + id).val() == "")){
                mati_input(["pos2d6a","pos2d6b"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_input(["pos2d6a","pos2d6b"]);
            }
            break;
        case 'pos2d7' :
          if(($('#' + id).val()==1) || ($('#' + id).val() == "")){
                mati_input(["pos2d7a","pos2d7b"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_input(["pos2d7a","pos2d7b"]);
            }
            break;
        case 'pos2d8' :
          if(($('#' + id).val()==1) || ($('#' + id).val() == "")){
                mati_input(["pos2d8a","pos2d8b"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_input(["pos2d8a","pos2d8b"]);
            }
            break;
        case 'pos2d9' :
          if(($('#' + id).val()==1) || ($('#' + id).val() == "")){
                mati_input(["pos2d9a"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_input(["pos2d9a"]);
            }
            break;
        case 'pgcfsrock' :
          if(($('#' + id).val()==2) || ($('#' + id).val() == "")){
                mati_s2(["#pgcfsrock1","#pgcfsrock1a","#pgcfsrock2","#pgcfsrock3","#pgcfsrock4","#pgcfsrock5","#pgcfsrock6","#pgcfsrock7","#pgcfsrock8","#pgcfsrock9"]);
            }
            else if ($('#' + id).val() == 1) {
                nyala_s2(["#pgcfsrock6","#pgcfsrock7","#pgcfsrock9"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_s2(["#pgcfsrock1","#pgcfsrock1a","#pgcfsrock2","#pgcfsrock3","#pgcfsrock4","#pgcfsrock5","#pgcfsrock6","#pgcfsrock7","#pgcfsrock8","#pgcfsrock9"]);
            }
            break;
        case 'pgcfres' :
          if(($('#' + id).val()==2) || ($('#' + id).val() == "")){
                mati_s2(["#pgcfres1","#pgcfres1a","#pgcfres2","#pgcfres3","#pgcfres4","#pgcfres5","#pgcfres6","#pgcfres7","#pgcfres8","#pgcfres9","#pgcfres10","#pgcfres11"]);
            }
            else if ($('#' + id).val() == 1) {
                nyala_s2(["#pgcfres1","#pgcfres1a","#pgcfres2","#pgcfres3","#pgcfres4","#pgcfres5","#pgcfres6","#pgcfres9","#pgcfres10","#pgcfres11"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_s2(["#pgcfres1","#pgcfres1a","#pgcfres2","#pgcfres3","#pgcfres4","#pgcfres5","#pgcfres6","#pgcfres7","#pgcfres8","#pgcfres9","#pgcfres10","#pgcfres11"]);
            }
            break;
        case 'pgcftrap' :
          if(($('#' + id).val()==2) || ($('#' + id).val() == "")){
                mati_s2(["#pgcftrap1","#pgcftrap1a","#pgcftrap2","#pgcftrap3","#pgcftrap4","#pgcftrap5","#pgcftrap6","#pgcftrap7","#pgcftrap7a","#pgcftrap8","#pgcftrap9","#pgcftrap10"]);
            }
            else if ($('#' + id).val() == 1) {
                nyala_s2(["#pgcftrap7","#pgcftrap7a","#pgcftrap9"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_s2(["#pgcftrap1","#pgcftrap1a","#pgcftrap2","#pgcftrap3","#pgcftrap4","#pgcftrap5","#pgcftrap6","#pgcftrap7","#pgcftrap7a","#pgcftrap8","#pgcftrap9","#pgcftrap10"]);
            }
            break;
        case 'pgcfdyn' :
          if(($('#' + id).val()==2) || ($('#' + id).val() == "")){
                mati_s2(["#pgcfdyn1","#pgcfdyn2","#pgcfdyn3","#pgcfdyn4","#pgcfdyn4a","#pgcfdyn5","#pgcfdyn5a","#pgcfdyn6","#pgcfdyn7","#pgcfdyn8","#pgcfdyn8a"]);
            }
            else if ($('#' + id).val() == 1) {
                nyala_s2(["#pgcfdyn4","#pgcfdyn4a","#pgcfdyn5","#pgcfdyn5a"]);
                mati_s2(["#pgcfdyn1","#pgcfdyn2","#pgcfdyn3","#pgcfdyn6","#pgcfdyn7","#pgcfdyn8","#pgcfdyn8a"]);
            }
            else if ($('#' + id).val() == 0) {
                nyala_s2(["#pgcfdyn1","#pgcfdyn2","#pgcfdyn3","#pgcfdyn4","#pgcfdyn4a","#pgcfdyn5","#pgcfdyn5a","#pgcfdyn6","#pgcfdyn7","#pgcfdyn8","#pgcfdyn8a"]);
            }
            break;        
    }
  }


//========================================
$("#pos2d1" ).datepicker({
          changeMonth: true,
          changeYear: true
        });


//==========================================        
bikin_sekaligus(_id_play, _data_play);



  
//======================================

$("#padj1")
           .on("change", function(e) { disabledElement_play("padj1"); })
$("#posd1")
           .on("change", function(e) { disabledElement_play("posd1"); })
$("#pos2d6")
           .on("change", function(e) { disabledElement_play("pos2d6"); })
$("#pos2d7")
           .on("change", function(e) { disabledElement_play("pos2d6"); })
$("#pos2d8")
           .on("change", function(e) { disabledElement_play("pos2d6"); })
$("#pos2d9")
           .on("change", function(e) { disabledElement_play("pos2d6"); })
$("#pgcfsrock")
           .on("change", function(e) { disabledElement_play("pgcfsrock"); })
$("#pgcfres")
           .on("change", function(e) { disabledElement_play("pgcfres"); })           
$("#pgcftrap")
           .on("change", function(e) { disabledElement_play("pgcftrap"); })
$("#pgcfdyn")
           .on("change", function(e) { disabledElement_play("pgcfdyn"); })

mati_s2(["#padj1a","#padj1b","#padj1c",
            "#posd2","#posd3","#posd4","#posd5",            
            "#pgcfsrock1","#pgcfsrock1a","#pgcfsrock2","#pgcfsrock3","#pgcfsrock4","#pgcfsrock5","#pgcfsrock6","#pgcfsrock7","#pgcfsrock8","#pgcfsrock9",
            "#pgcfres1","#pgcfres1a","#pgcfres2","#pgcfres3","#pgcfres4","#pgcfres5","#pgcfres6","#pgcfres7","#pgcfres8","#pgcfres9","#pgcfres10","#pgcfres11",
            "#pgcftrap1","#pgcftrap1a","#pgcftrap2","#pgcftrap3","#pgcftrap4","#pgcftrap5","#pgcftrap6","#pgcftrap7","#pgcftrap7a","#pgcftrap8","#pgcftrap9","#pgcftrap10",
            "#pgcfdyn1","#pgcfdyn2","#pgcfdyn3","#pgcfdyn4","#pgcfdyn4a","#pgcfdyn5","#pgcfdyn5a","#pgcfdyn6","#pgcfdyn7","#pgcfdyn8","#pgcfdyn8a",
            ]);
mati_input(["posd3a","posd4a","posd5a",
            "pos2d6a","pos2d6b",
            "pos2d7a","pos2d7b",
            "pos2d8a","pos2d8b",
            "pos2d9a",
            ]);