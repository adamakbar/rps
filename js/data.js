//================================================
$('.dialog')
		.dialog(
				{
					modal : true,
					open : function() {
						if ($.ui
								&& $.ui.dialog
								&& !$.ui.dialog.prototype._allowInteractionRemapped
								&& $(this).closest(".ui-dialog").length) {
							if ($.ui.dialog.prototype._allowInteraction) {
								$.ui.dialog.prototype._allowInteraction = function(
										e) {
									if ($(e.target).closest('.select2-drop').length)
										return true;
									return ui_dialog_interaction.apply(this,
											arguments);
								};
								$.ui.dialog.prototype._allowInteractionRemapped = true;
							} else {
								$.error("You must upgrade jQuery UI or else.");
							}
						}
					},
					_allowInteraction : function(event) {
						return !!$(event.target).is(".select2-input")
								|| this._super(event);
					}
				});
// ==============

$('#rd_penny').dataTable({
	"aoColumnDefs" : [ {
		"bSortable" : false,
		"aTargets" : [ 1, 3, 4, 5 ]
	} ]

});
// ======================= BEGIN VARIABLE SELECT 2 =======================//

var tahun_rps = [{
	id : '2014',
	text : '2014'
}, {
	id : '2015',
	text : '2015'
}, {
	id : '2016',
	text : '2016'
}, {
	id : '2017',
	text : '2017'
}];

var status_name = [{
	id : 'original',
	text : 'original'
}, {
	id : 'update',
	text : 'update'
}];

var near_field = [{
	id : '< 5',
	text : '< 5'
}, {
	id : '5 - 10',
	text : '5 - 10'
}, {
	id : '10 - 25',
	text : '10 - 25'
}, {
	id : '> 25',
	text : '> 25'
}];

var near_infra_structure = [{
	id : '< 50',
	text : '< 50'
}, {
	id : '50 - 100',
	text : '50 - 100'
}, {
	id : '100 - 200',
	text : '100 - 200'
}, {
	id : '> 200',
	text : '> 200'
}];


var well_result_postdrill = [{
	id : 'Oil',
	text : 'Oil'
}, {
	id : 'Gas',
	text : 'Gas'
}, {
	id : 'Oil+Gas',
	text : 'Oil+Gas'
}, {
	id : 'Condensate',
	text : 'Condensate'
}, {
	id : 'Condensate+Gas',
	text : 'Condensate+Gas'
}, {
	id : 'Oil+Gas+Condensate',
	text : 'Oil+Gas+Condensate'
}, {
	id : 'Oil+Condensate',
	text : 'Oil+Condensate'
}, {
	id : 'Dry',
	text : 'Dry'
}, {
	id : 'Not Conclusive',
	text : 'Not Conclusive'
}];

var well_result_discovery = [{
	id : 'Oil',
	text : 'Oil'
}, {
	id : 'Gas',
	text : 'Gas'
}, {
	id : 'Oil+Gas',
	text : 'Oil+Gas'
}, {
	id : 'Condensate',
	text : 'Condensate'
}, {
	id : 'Condensate+Gas',
	text : 'Condensate+Gas'
}, {
	id : 'Oil+Gas+Condensate',
	text : 'Oil+Gas+Condensate'
}, {
	id : 'Oil+Condensate',
	text : 'Oil+Condensate'
}];

var prospect_type = [{
	id : 'Postdrill',
	text : 'Postdrill'
}, {
	id : 'Discovery',
	text : 'Discovery'
}];

var map_category = [ {
	id : 'Resources Map',
	text : 'Resources Map'
}, {
	id : 'Survey or Seismic Line',
	text : 'Survey or Seismic Line'
}, {
	id : 'Location of Working Area in Existing Basin',
	text : 'Location of Working Area in Existing Basin'
}, {
	id : 'Regional Tectonical Setting Picture',
	text : 'Regional Tectonical Setting Picture'
}, {
	id : 'Maturity of Source Rock Picture',
	text : 'Maturity of Source Rock Picture'
}, {
	id : 'Regional Stratigraphy Picture',
	text : 'Regional Stratigraphy Picture'
}, {
	id : 'Play System Picture',
	text : 'Play System Picture'
}, {
	id : 'Petroleum System Picture',
	text : 'Petroleum System Picture'
}, {
	id : 'Time-Depth structural Map',
	text : 'Time-Depth structural Map'
}, {
	id : 'Structural Isopach Map',
	text : 'Structural Isopach Map'
}, {
	id : 'Sedimentology Facies Distribution Map',
	text : 'Sedimentology Facies Distribution Map'
}, {
	id : 'Net To Gross Map',
	text : 'Net To Gross Map'
}, {
	id : 'Porosity Distribution Map',
	text : 'Porosity Distribution Map'
}, {
	id : 'Water Saturation Distribution Map',
	text : 'Water Saturation Distribution Map'
}, {
	id : 'Stratigraphic Column',
	text : 'Stratigraphic Column'
}, {
	id : 'Ranges Probabilistic Graphical',
	text : 'Ranges Probabilistic Graphical'
}, {
	id : 'Other Document',
	text : 'Other Document'
}, {
	id : 'Other Image',
	text : 'Other Image'
},];

var format_name = [ {
	id : 'Agam',
	text : 'Agam'
}];

var format_name_post = [{
	id : 'Lower',
	text : 'Lower'
}, {
	id : 'Middle',
	text : 'Middle'
}, {
	id : 'Upper',
	text : 'Upper'
}, {
	id : 'Not Available',
	text : 'Not Available'
}]

var agesystem = [ {
	id : 'Holocene',
	text : 'Holocene'
}, {
	id : 'Pleistocene',
	text : 'Pleistocene'
}, {
	id : 'Miocene',
	text : 'Miocene'
}, {
	id : 'Oligocene',
	text : 'Oligocene'
}, {
	id : 'Eocene',
	text : 'Eocene'
}, {
	id : 'Paleocene',
	text : 'Paleocene'
}, {
	id : 'Cretaceous',
	text : 'Cretaceous'
}, {
	id : 'Jurassic',
	text : 'Jurassic'
}, {
	id : 'Triassic',
	text : 'Triassic'
}, {
	id : 'Permian',
	text : 'Permian'
}, {
	id : 'Pliocene',
	text : 'Pliocene'
}, {
	id : 'Carboniferous',
	text : 'Carboniferous'
}, {
	id : 'Devonian',
	text : 'Devonian'
}, {
	id : 'Silurian',
	text : 'Silurian'
}, {
	id : 'Ordovician',
	text : 'Ordovician'
}, {
	id : 'Cambrian',
	text : 'Cambrian'
} ];
var ageseries = [ {
	id : 'Early',
	text : 'Early'
}, {
	id : 'Middle',
	text : 'Middle'
}, {
	id : 'Late',
	text : 'Late'
}, {
	id : 'Not Available',
	text : 'Not Available'
} ];
var lpm = [ {
	id : 'PSTM',
	text : 'PSTM'
}, {
	id : 'PSDM',
	text : 'PSDM'
} ];
var ahc = [ {
	id : 'Analog',
	text : 'Analog'
}, {
	id : 'Preserve Sample',
	text : 'Preserve Sample'
} ];
var clarifiedby_dl = [ {
	id : 0,
	text : 'Infill Seismic 2D'
}, {
	id : 1,
	text : 'Seismic 3D'
} ];
var onoffshore = [ {
	id : 'Onshore',
	text : 'OnShore'
}, {
	id : 'Offshore',
	text : 'OffShore'
}, {
	id : 'Both',
	text : 'Both'
} ];
var welltype = [ {
	id : 'Wildcat',
	text : 'Wildcat'
}, {
	id : 'Delineation',
	text : 'Delineation'
}, {
	id : 'Abondoned',
	text : 'Abondoned'
} ];
var welltype_pd_dc = [ {
	id : 'Wildcat',
	text : 'Wildcat'
}, {
	id : 'Delineation',
	text : 'Delineation'
}, {
	id : 'Abondoned',
	text : 'Abondoned'
}, {
	id : 'Re-entry',
	text : 'Re-entry'
} ];
var terrain = [ {
	id : 'Swamp',
	text : 'Swamp'
}, {
	id : 'Jungle',
	text : 'Jungle'
}, {
	id : 'Grassland',
	text : 'Grassland'
}, {
	id : 'Urban',
	text : 'Urban'
}, {
	id : 'Rural',
	text : 'Rural'
}, {
	id : 'Farmland',
	text : 'Farmland'
}, {
	id : 'Forest Range',
	text : 'Forest Range'
}, {
	id : 'Conservation Area',
	text : 'Conservation Area'
}, {
	id : 'Mountanous',
	text : 'Mountanous'
}, {
	id : '< 12 miles Offshore',
	text : '< 12 miles Offshore'
}, {
	id : '> 12 miles Offshore',
	text : '> 12 miles Offshore'
} ];
var siq = [ {
	id : 'Poor',
	text : 'Poor'
}, {
	id : 'Moderate',
	text : 'Moderate'
}, {
	id : 'Good',
	text : 'Good'
}, {
	id : 'Excellent',
	text : 'Excellent'
} ];
var surveymethod_geo = [ {
	id : 'Stream Sampling',
	text : 'Stream Sampling'
}, {
	id : 'Random Sampling',
	text : 'Random Sampling'
} ];
var surveymethods = [ {
	id : 'Airbones',
	text : 'Airbones'
}, {
	id : 'Surface',
	text : 'Surface'
}, {
	id : 'Others',
	text : 'Others'
} ];
var numofvintage = [ {
	id : 'Single',
	text : 'Single'
}, {
	id : 'Multiple',
	text : 'Multiple'
} ];
var processingtype = [ {
	id : 'Field Processing',
	text : 'Field Processing'
}, {
	id : 'Processing PSTM',
	text : 'Processing PSTM'
}, {
	id : 'Processing PSDM',
	text : 'Processing PSDM'
}, {
	id : 'Other Processing',
	text : 'Other Processing'
} ];
var wellstatus = [ {
	id : 'Temporary Suspanded',
	text : 'Temporary Suspanded'
}, {
	id : 'Mecanical Failure Suspanded',
	text : 'Mecanical Failure Suspanded'
}, {
	id : 'Plug and Abandoned',
	text : 'Plug and Abandoned'
}, {
	id : 'On Going Drilling',
	text : 'On Going Drilling'
} ];
var numsample = [ {
	id : '1',
	text : '1'
}, {
	id : '2',
	text : '2'
}, {
	id : '3',
	text : '3'
}, {
	id : '4',
	text : '4'
}, {
	id : '5',
	text : '5'
}, {
	id : '6',
	text : '6'
}, {
	id : '7',
	text : '7'
}, {
	id : '8',
	text : '8'
}, {
	id : '9',
	text : '9'
}, {
	id : '> 10',
	text : '> 10'
}, {
	id : '> 100',
	text : '> 100'
} ];
var awi = [ {
	id : 'Good',
	text : 'Good'
}, {
	id : 'Poor',
	text : 'Poor'
}, {
	id : 'Collapse',
	text : 'Collapse'
} ];
var yes_no_data = [ {
	id : 'Yes',
	text : 'Yes'
}, {
	id : 'No',
	text : 'No'
} ];
var yes_no_unknown_data = [ {
	id : 'Yes',
	text : 'Yes'
}, {
	id : 'No',
	text : 'No'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var methodsroks = [ {
	id : 'Cutting Sampling',
	text : 'Cutting Sampling'
}, {
	id : 'Random Side Wall Core',
	text : 'Random Side Wall Core'
}, {
	id : 'Intervaled Side Wall Core',
	text : 'Intervaled Side Wall Core'
}, {
	id : 'Conventional Corring',
	text : 'Conventional Corring'
} ];
var prov_analog = [ {
	id : 'Proven',
	text : 'Proven'
}, {
	id : 'Analog',
	text : 'Analog'
} ];
var typekerogen = [ {
	id : 'I',
	text : 'I'
}, {
	id : 'I/II',
	text : 'I/II'
}, {
	id : 'II',
	text : 'II'
}, {
	id : 'II/III',
	text : 'II/III'
}, {
	id : 'III',
	text : 'III'
}, {
	id : 'IV',
	text : 'IV'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var capacity_toc = [ {
	id : '0 - 0.5',
	text : '0 - 0.5'
}, {
	id : '0.6 - 1.0',
	text : '0.6 - 1.0'
}, {
	id : '1.1 - 2.0',
	text : '1.1 - 2.0'
}, {
	id : '2.1 - 4.0',
	text : '2.1 - 4.0'
}, {
	id : '> 4',
	text : '> 4'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var hfu = [ {
	id : '<= 1.0',
	text : '<= 1.0'
}, {
	id : '1.1 - 2.0',
	text : '1.1 - 2.0'
}, {
	id : '2.1 - 3.0',
	text : '2.1 - 3.0'
}, {
	id : '> 3.0',
	text : '> 3.0'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var distrib_lim = [ {
	id : 'Localized',
	text : 'Localized'
}, {
	id : 'Multiple Limited',
	text : 'Multiple Limited'
}, {
	id : 'Multiple Wide',
	text : 'Multiple Wide'
}, {
	id : 'Single Limited',
	text : 'Single Limited'
}, {
	id : 'Single Wide',
	text : 'Single Wide'
} ];
var continuity_good = [ {
	id : 'Very Good',
	text : 'Very Good'
}, {
	id : 'Good',
	text : 'Good'
}, {
	id : 'Fair',
	text : 'Fair'
}, {
	id : 'Bad',
	text : 'Bad'
} ];
var maturity = [ {
	id : 'Immature',
	text : 'Immature'
}, {
	id : 'Early Mature',
	text : 'Early Mature'
}, {
	id : 'Mature',
	text : 'Mature'
}, {
	id : 'Over Mature',
	text : 'Over Mature'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var depos_set = [ {
	id : 'Aeolian',
	text : 'Aeolian'
}, {
	id : 'Bathyal',
	text : 'Bathyal'
}, {
	id : 'Coastal',
	text : 'Coastal'
}, {
	id : 'Continental Fan',
	text : 'Continental Fan'
}, {
	id : 'Delta Slope',
	text : 'Delta Slope'
}, {
	id : 'Delta Top',
	text : 'Delta Top'
}, {
	id : 'Deltaic',
	text : 'Deltaic'
}, {
	id : 'Fluvial',
	text : 'Fluvial'
}, {
	id : 'Glacial',
	text : 'Glacial'
}, {
	id : 'Lacustrine',
	text : 'Lacustrine'
}, {
	id : 'Marine Shelf',
	text : 'Marine Shelf'
}, {
	id : 'Marine Shoreface',
	text : 'Marine Shoreface'
}, {
	id : 'Reefal',
	text : 'Reefal'
}, {
	id : 'Paralic',
	text : 'Paralic'
}, {
	id : 'Platform Margin',
	text : 'Platform Margin'
}, {
	id : 'Not Available',
	text : 'Not Available'
} ];
var depos_env = [ {
	id : 'Aeolian Dune',
	text : 'Aeolian Dune'
}, {
	id : 'Alluvial Fan',
	text : 'Alluvial Fan'
}, {
	id : 'Barrier Bar',
	text : 'Barrier Bar'
}, {
	id : 'Barrier Reef',
	text : 'Barrier Reef'
}, {
	id : 'Deep Sea Sand Channel',
	text : 'Deep Sea Sand Channel'
}, {
	id : 'Deep Sea Sand Fan',
	text : 'Deep Sea Sand Fan'
}, {
	id : 'Deltaic Distributed Channel',
	text : 'Deltaic Distributed Channel'
}, {
	id : 'Deltaic Distributed Mud Bar',
	text : 'Deltaic Distributed Mud Bar'
}, {
	id : 'Fluviatile Braided',
	text : 'Fluviatile Braided'
}, {
	id : 'Fluviatile Meandering',
	text : 'Fluviatile Meandering'
}, {
	id : 'Issolated Reef',
	text : 'Issolated Reef'
}, {
	id : 'Lagoon',
	text : 'Lagoon'
}, {
	id : 'Patch Reef',
	text : 'Patch Reef'
}, {
	id : 'Financle Reef',
	text : 'Financle Reef'
}, {
	id : 'Platformal Reef',
	text : 'Platformal Reef'
}, {
	id : 'Regressive Reef',
	text : 'Regressive Reef'
}, {
	id : 'Regressive Sand',
	text : 'Regressive Sand'
}, {
	id : 'Ridge',
	text : 'Ridge'
}, {
	id : 'Schoals Reef',
	text : 'Schoals Reef'
}, {
	id : 'Tidal Flat',
	text : 'Tidal Flat'
}, {
	id : 'Tidal Sand',
	text : 'Tidal Sand'
}, {
	id : 'Transgressive Sand',
	text : 'Transgressive Sand'
}, {
	id : 'Others',
	text : 'Others'
} ];
var distrib_ = [ {
	id : 'Single Distribution',
	text : 'Single Distribution'
}, {
	id : 'Double Distribution',
	text : 'Double Distribution'
}, {
	id : 'Multiple Distribution',
	text : 'Multiple Distribution'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var continuity_ = [ {
	id : 'Extensive Spread',
	text : 'Extensive Spread'
}, {
	id : 'Limited Spread',
	text : 'Limited Spread'
}, {
	id : 'Spread',
	text : 'Spread'
}, {
	id : 'Tank',
	text : 'Tank'
}, {
	id : 'Truncated',
	text : 'Truncated'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var lithology = [ {
	id : 'Alternated Rocks',
	text : 'Alternated Rocks'
}, {
	id : 'Coal',
	text : 'Coal'
}, {
	id : 'Naturally Fractured',
	text : 'Naturally Fractured'
}, {
	id : 'Siltstone',
	text : 'Siltstone'
}, {
	id : 'Platform Carbonate',
	text : 'Platform Carbonate'
}, {
	id : 'Reef Carbonate',
	text : 'Reef Carbonate'
}, {
	id : 'Sandstone',
	text : 'Sandstone'
}, {
	id : 'Others',
	text : 'Others'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var porositytype = [ {
	id : 'Average Primary Porosity Reservoir',
	text : 'Average Primary Porosity Reservoir'
}, {
	id : 'Secondary Porosity',
	text : 'Secondary Porosity'
}, {
	id : 'Both',
	text : 'Both'
} ];
var prim_porosity = [ {
	id : '> 30',
	text : '> 30'
}, {
	id : '0 - 10',
	text : '0 - 10'
}, {
	id : '11 - 15',
	text : '11 - 15'
}, {
	id : '16 - 20',
	text : '16 - 20'
}, {
	id : '21 - 30',
	text : '21 - 30'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var sec_porosity = [ {
	id : 'Vugs Porosity',
	text : 'Vugs Porosity'
}, {
	id : 'Fracture Porosity',
	text : 'Fracture Porosity'
}, {
	id : 'Integrated Porosity',
	text : 'Integrated Porosity'
}, {
	id : 'None',
	text : 'None'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var sealingtype = [ {
	id : 'Primary',
	text : 'Primary'
}, {
	id : 'Diagenetic',
	text : 'Diagenetic'
}, {
	id : 'Alternate Tectonic Mech',
	text : 'Alternate Tectonic Mech'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var sealing_distrib = [ {
	id : 'Single Distribution Impermeable Rocks',
	text : 'Single Distribution Impermeable Rocks'
}, {
	id : 'Double Distribution Impermeable Rocks',
	text : 'Double Distribution Impermeable Rocks'
}, {
	id : 'Multiple Distribution Impermeable Rocks',
	text : 'Multiple Distribution Impermeable Rocks'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var trapping_type = [ {
	id : 'Structural Tectonic Extensional',
	text : 'Structural Tectonic Extensional'
}, {
	id : 'Structural Tectonic Compressional Thin Skinned',
	text : 'Structural Tectonic Compressional Thin Skinned'
}, {
	id : 'Structural Tectonic Compressional Inverted',
	text : 'Structural Tectonic Compressional Inverted'
}, {
	id : 'Structural Tectonic Strike Slip',
	text : 'Structural Tectonic Strike Slip'
}, {
	id : 'Structural Tectonic Unqualified',
	text : 'Structural Tectonic Unqualified'
}, {
	id : 'Structural Drape',
	text : 'Structural Drape'
}, {
	id : 'Structural Compactional',
	text : 'Structural Compactional'
}, {
	id : 'Structural Diapiric Salt',
	text : 'Structural Diapiric Salt'
}, {
	id : 'Structural Diapiric Mud',
	text : 'Structural Diapiric Mud'
}, {
	id : 'Structural Diapiric Unqualified',
	text : 'Structural Diapiric Unqualified'
}, {
	id : 'Structural Gravitational',
	text : 'Structural Gravitational'
}, {
	id : 'Structural Unqualified',
	text : 'Structural Unqualified'
}, {
	id : 'Stratigraphic Depositional Pinch-out',
	text : 'Stratigraphic Depositional Pinch-out'
}, {
	id : 'Stratigraphic Depositional Shale-out',
	text : 'Stratigraphic Depositional Shale-out'
}, {
	id : 'Stratigraphic Depositional Facies-limited',
	text : 'Stratigraphic Depositional Facies-limited'
}, {
	id : 'Stratigraphic Depositional Reef',
	text : 'Stratigraphic Depositional Reef'
}, {
	id : 'Stratigraphic Depositional Unqualifed',
	text : 'Stratigraphic Depositional Unqualifed'
}, {
	id : 'Stratigraphic Unconformity Subcrop',
	text : 'Stratigraphic Unconformity Subcrop'
}, {
	id : 'Stratigraphic Unconformity Onlap',
	text : 'Stratigraphic Unconformity Onlap'
}, {
	id : 'Stratigraphic Unconformity Unqualified',
	text : 'Stratigraphic Unconformity Unqualified'
}, {
	id : 'Stratigraphic Other Diagenetic',
	text : 'Stratigraphic Other Diagenetic'
}, {
	id : 'Stratigraphic Other Tar Mat',
	text : 'Stratigraphic Other Tar Mat'
}, {
	id : 'Stratigraphic Other Gas Hydrate',
	text : 'Stratigraphic Other Gas Hydrate'
}, {
	id : 'Stratigraphic Other Permafrost',
	text : 'Stratigraphic Other Permafrost'
}, {
	id : 'Stratigraphic Unqualified',
	text : 'Stratigraphic Unqualified'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var closure_type = [ {
	id : '2-Way',
	text : '2-Way'
}, {
	id : '3-Way',
	text : '3-Way'
}, {
	id : '4-Way',
	text : '4-Way'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var trapping_geom = [ {
	id : 'Anticline Hangingwall',
	text : 'Anticline Hangingwall'
}, {
	id : 'Anticline Footwall',
	text : 'Anticline Footwall'
}, {
	id : 'Anticline Pericline',
	text : 'Anticline Pericline'
}, {
	id : 'Anticline Dome',
	text : 'Anticline Dome'
}, {
	id : 'Anticline Nose',
	text : 'Anticline Nose'
}, {
	id : 'Anticline Unqualified',
	text : 'Anticline Unqualified'
}, {
	id : 'Faulted Anticline Hangingwall',
	text : 'Faulted Anticline Hangingwall'
}, {
	id : 'Faulted Anticline Footwall',
	text : 'Faulted Anticline Footwall'
}, {
	id : 'Faulted Anticline Pericline',
	text : 'Faulted Anticline Pericline'
}, {
	id : 'Faulted Anticline Dome',
	text : 'Faulted Anticline Dome'
}, {
	id : 'Faulted Anticline Nose',
	text : 'Faulted Anticline Nose'
}, {
	id : 'Faulted Anticline Unqualified',
	text : 'Faulted Anticline Unqualified'
}, {
	id : 'Tilted Hangingwall',
	text : 'Tilted Hangingwall'
}, {
	id : 'Tilted Fault Block Footwall',
	text : 'Tilted Fault Block Footwall'
}, {
	id : 'Tilted Fault Block Terrace',
	text : 'Tilted Fault Block Terrace'
}, {
	id : 'Tilted Fault Block Unqualified',
	text : 'Tilted Fault Block Unqualified'
}, {
	id : 'Horst Simple',
	text : 'Horst Simple'
}, {
	id : 'Horst Faulted',
	text : 'Horst Faulted'
}, {
	id : 'Horst Complex',
	text : 'Horst Complex'
}, {
	id : 'Horst Unqualified',
	text : 'Horst Unqualified'
}, {
	id : 'Wedge Tilted',
	text : 'Wedge Tilted'
}, {
	id : 'Wedge Felxtured',
	text : 'Wedge Felxtured'
}, {
	id : 'Wedge Radial',
	text : 'Wedge Radial'
}, {
	id : 'Wedge Marginal',
	text : 'Wedge Marginal'
}, {
	id : 'Wedge Faulted',
	text : 'Wedge Faulted'
}, {
	id : 'Wedge Onlap',
	text : 'Wedge Onlap'
}, {
	id : 'Wedge Subcrop',
	text : 'Wedge Subcrop'
}, {
	id : 'Wedge Unqualified',
	text : 'Wedge Unqualified'
}, {
	id : 'Abutment Hangingwall',
	text : 'Abutment Hangingwall'
}, {
	id : 'Abutment Footwall',
	text : 'Abutment Footwall'
}, {
	id : 'Abutment Pericline',
	text : 'Abutment Pericline'
}, {
	id : 'Abutment Terrace',
	text : 'Abutment Terrace'
}, {
	id : 'Abutment Complex',
	text : 'Abutment Complex'
}, {
	id : 'Abutment Unqualified',
	text : 'Abutment Unqualified'
}, {
	id : 'Irregular Hangingwall',
	text : 'Irregular Hangingwall'
}, {
	id : 'Irregular Footwall',
	text : 'Irregular Footwall'
}, {
	id : 'Irregular Pericline',
	text : 'Irregular Pericline'
}, {
	id : 'Irregular Dome',
	text : 'Irregular Dome'
}, {
	id : 'Irregular Terrace',
	text : 'Irregular Terrace'
}, {
	id : 'Irregular Unqualified',
	text : 'Irregular Unqualified'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var autentic_migration = [ {
	id : 'Near By Well Discovery',
	text : 'Near By Well Discovery'
}, {
	id : 'Oil Seep',
	text : 'Oil Seep'
}, {
	id : 'Oil Trace',
	text : 'Oil Trace'
}, {
	id : 'Not Available',
	text : 'Not Available'
} ];
var tpdtk = [ {
	id : 'Very Long (> 20 Km)',
	text : 'Very Long (> 20 Km)'
}, {
	id : 'Long (10 - 20 Km)',
	text : 'Long (10 - 20 Km)'
}, {
	id : 'Middle (5 - 10 Km)',
	text : 'Middle (5 - 10 Km)'
}, {
	id : 'Near (2 - 5 Km)',
	text : 'Near (2 - 5 Km)'
}, {
	id : 'Very Near (0 - 2 Km)',
	text : 'Very Near (0 - 2 Km)'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var toeps = [ {
	id : 'Multiple Order',
	text : 'Multiple Order'
}, {
	id : 'Single Order',
	text : 'Single Order'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var p_spe = [ {
	id : 'Not Occur',
	text : 'Not Occur'
}, {
	id : 'Occur',
	text : 'Occur'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var migration_pathway = [ {
	id : 'Horizontal',
	text : 'Horizontal'
}, {
	id : 'Multiple Directions',
	text : 'Multiple Directions'
}, {
	id : 'Vertical',
	text : 'Vertical'
}, {
	id : 'Unknown',
	text : 'Unknown'
} ];
var basin = [ {
	id : 1,
	text : 'B86S01-NORTH SUMATRA'
}, {
	id : 2,
	text : 'B86S02-SIBOLGA'
}, {
	id : 3,
	text : 'B86S03-NIAS'
}, {
	id : 4,
	text : 'B86S04-CENTRAL SUMATRA'
}, {
	id : 5,
	text : 'B86S05-PENDALIAN'
}, {
	id : 6,
	text : 'B86S06-OMBILIN'
}, {
	id : 7,
	text : 'B86S07-MENTAWAI'
}, {
	id : 8,
	text : 'B86S08-SOUTH SUMATRA'
}, {
	id : 9,
	text : 'B86S09-BENGKULU'
}, {
	id : 10,
	text : 'B86S10-WEST NATUNA'
}, {
	id : 11,
	text : 'B86S11-EAST NATUNA'
}, {
	id : 12,
	text : 'B86S12-SOUTH NATUNA'
}, {
	id : 13,
	text : 'B86S13-WEST BANGKA'
}, {
	id : 14,
	text : 'B86S14-BANGKA'
}, {
	id : 15,
	text : 'B86S15-SUNDA'
}, {
	id : 16,
	text : 'B86S16-SUNDA STRAIT'
}, {
	id : 17,
	text : 'B86S17-UJUNG KULON'
}, {
	id : 18,
	text : 'B86S18-NORTH WEST JAVA'
}, {
	id : 19,
	text : 'B86S19-BOGOR'
}, {
	id : 20,
	text : 'B86S20-SOUTH WEST JAVA'
}, {
	id : 21,
	text : 'B86S21-VERA'
}, {
	id : 22,
	text : 'B86S22-BILITON'
}, {
	id : 23,
	text : 'B86S23-NORTH CENTRAL JAVA'
}, {
	id : 24,
	text : 'B86S24-BANYUMAS'
}, {
	id : 25,
	text : 'B86S25-SOUTH CENTRAL JAVA'
}, {
	id : 26,
	text : 'B86S26-SOUTH JAVA'
}, {
	id : 27,
	text : 'B86S27-PATI'
}, {
	id : 28,
	text : 'B86S28-NORTH EAST JAVA'
}, {
	id : 29,
	text : 'B86S29-EAST JAVA'
}, {
	id : 30,
	text : 'B86S30-SOUTH EAST JAVA'
}, {
	id : 31,
	text : 'B86S31-LOMBOK-BALI'
}, {
	id : 32,
	text : 'B86S32-KETUNGAU'
}, {
	id : 33,
	text : 'B86S33-MELAWI'
}, {
	id : 34,
	text : 'B86S34-PEMBUANG'
}, {
	id : 35,
	text : 'B86S35-TARAKAN'
}, {
	id : 36,
	text : 'B86S36-KUTAI'
}, {
	id : 37,
	text : 'B86S37-KENDILO'
}, {
	id : 38,
	text : 'B86S38-BARITO'
}, {
	id : 39,
	text : 'B86S39-PASIR'
}, {
	id : 40,
	text : 'B86S40-ASEM-ASEM'
}, {
	id : 41,
	text : 'B86S41-CELEBES'
}, {
	id : 42,
	text : 'B86S42-MINAHASA'
}, {
	id : 43,
	text : 'B86S43-GORONTALO'
}, {
	id : 44,
	text : 'B86S44-LARIANG'
}, {
	id : 45,
	text : 'B86S45-KALOSI'
}, {
	id : 46,
	text : 'B86S46-WEST SENGKANG'
}, {
	id : 47,
	text : 'B86S47-SOUTH MAKASSAR'
}, {
	id : 48,
	text : 'B86S48-BONE'
}, {
	id : 49,
	text : 'B86S49-SPERMONDE'
}, {
	id : 50,
	text : 'B86S50-FLORES'
}, {
	id : 51,
	text : 'B86S51-SAWU'
}, {
	id : 52,
	text : 'B86S52-SUMBA'
}, {
	id : 53,
	text : 'B86S53-TIMOR SEA'
}, {
	id : 54,
	text : 'B86S54-TIMOR'
}, {
	id : 55,
	text : 'B86S55-WETAR'
}, {
	id : 56,
	text : 'B86S56-TUKANG BESI'
}, {
	id : 57,
	text : 'B86S57-BUTON'
}, {
	id : 58,
	text : 'B86S58-WEST BURU'
}, {
	id : 59,
	text : 'B86S59-MANUI'
}, {
	id : 60,
	text : 'B86S60-BANGGAI'
}, {
	id : 61,
	text : 'B86S61-SULA'
}, {
	id : 62,
	text : 'B86S62-SOUTH SULA'
}, {
	id : 63,
	text : 'B86S63-BURU'
}, {
	id : 64,
	text : 'B86S64-BANDA'
}, {
	id : 65,
	text : 'B86S65-SANGIHE'
}, {
	id : 66,
	text : 'B86S66-KAU BAY'
}, {
	id : 67,
	text : 'B86S67-NORTH OBI'
}, {
	id : 68,
	text : 'B86S68-SOUTH OBI'
}, {
	id : 69,
	text : 'B86S69-WEDA BAY'
}, {
	id : 70,
	text : 'B86S70-BULI BAY'
}, {
	id : 71,
	text : 'B86S71-SALAWATI'
}, {
	id : 72,
	text : 'B86S72-BERAU'
}, {
	id : 73,
	text : 'B86S73-BINTUNI'
}, {
	id : 74,
	text : 'B86S74-SERAM'
}, {
	id : 75,
	text : 'B86S75-WEBER'
}, {
	id : 76,
	text : 'B86S76-TANIMBAR'
}, {
	id : 77,
	text : 'B86S77-PALUNG ARU'
}, {
	id : 78,
	text : 'B86S78-CENDRAWASIH'
}, {
	id : 79,
	text : 'B86S79-BIAK'
}, {
	id : 80,
	text : 'B86S80-MEMBERAMO'
}, {
	id : 81,
	text : 'B86S81-TEER'
}, {
	id : 82,
	text : 'B86S82-MEERVLAKTE'
}, {
	id : 83,
	text : 'B86S83-AKIMEUGAH'
}, {
	id : 84,
	text : 'B86S84-IWUR'
}, {
	id : 85,
	text : 'B86S85-SAHUL'
}, {
	id : 86,
	text : 'B86S86-ARAFURA'
}, {
	id : 87,
	text : 'B86S87-PRE-TERTIARY PASSIVE MARGIN'
}, {
	id : 88,
	text : 'B86S88-RANGKASBITUNG'
} ];
var byanalogto = [ {
	id : 'Discovery',
	text : 'Discovery'
}, {
	id : 'Field',
	text : 'Field'
} ];
var distancetoanalog = [ {
	id : '0 - 3',
	text : '0 - 3'
}, {
	id : '4 - 10',
	text : '4 - 10'
}, {
	id : '11 - 20',
	text : '11 - 20'
}, {
	id : '> 20',
	text : '> 20'
} ];
var explorationmethods = [ {
	id : 'Step Out Vertical',
	text : 'Step Out Vertical'
}, {
	id : 'Step Out Horizontal',
	text : 'Step Out Horizontal'
}, {
	id : 'Frontier',
	text : 'Frontier'
} ];
var dsipl = [ {
	id : '1',
	text : '1'
}, {
	id : '5',
	text : '5'
}, {
	id : '10',
	text : '10'
}, {
	id : '>10',
	text : '>10'
} ];
var select_db = [ {
	id : 0,
	text : 'Ambil Database'
} ];
var clarifiedby_l = [ {
	id : 'Join Study',
	text : 'Join Study'
}, {
	id : 'In House Study',
	text : 'In House Study'
}, {
	id : 'Infill Seismic 2D',
	text : 'Infill Seismic 2D'
}, {
	id : 'Seismic 3D',
	text : 'Seismic 3D'
}, {
	id : 'Third Party Study',
	text : 'Third Party Study'
} ];
var prov_analog = [ {
	id : 'Proven',
	text : 'Proven'
}, {
	id : 'Analog',
	text : 'Analog'
} ];
var ana_esti_data = [ {
	id : 'Analog',
	text : 'Analog'
}, {
	id : 'Estimated',
	text : 'Estimated'
}, {
	id : 'Data',
	text : 'Data'
} ];
var antrip = [ {
	id : 'Analog',
	text : 'Analog'
}, {
	id : 'Estimated',
	text : 'Estimated'
} ];
var wellcat = [ {
	id : 'Wet',
	text : 'Wet'
}, {
	id : 'Dry',
	text : 'Dry'
}, {
	id : 'Not Conclusive',
	text : 'Not Conclusive'
} ];
var howwell = [ {
	id : 1,
	text : '1'
}, {
	id : 2,
	text : '2'
}, {
	id : 3,
	text : '3'
}, {
	id : 4,
	text : '4'
} ];
var howzone = [ {
	id : 1,
	text : '1'
}, {
	id : 2,
	text : '2'
}, {
	id : 3,
	text : '3'
}, {
	id : 4,
	text : '4'
}, {
	id : 5,
	text : '5'
} ];
var kosong = [ {
	id : 0,
	text : '?'
}, {
	id : 1,
	text : '?'
} ];
var sortby = [ {
	id : 0,
	text : 'Date'
}, {
	id : 1,
	text : 'Title'
} ];
var oil_grav = [ {
	id : 0,
	text : '10'
}, {
	id : 1,
	text : '11'
}, {
	id : 2,
	text : '12'
}, {
	id : 3,
	text : '13'
}, {
	id : 4,
	text : '14'
}, {
	id : 5,
	text : '15'
}, {
	id : 6,
	text : '16'
}, {
	id : 7,
	text : '17'
}, {
	id : 8,
	text : '18'
}, {
	id : 9,
	text : '19'
}, {
	id : 10,
	text : '20'
}, {
	id : 11,
	text : '21'
}, {
	id : 12,
	text : '22'
}, {
	id : 13,
	text : '23'
}, {
	id : 14,
	text : '24'
}, {
	id : 15,
	text : '25'
}, {
	id : 16,
	text : '26'
}, {
	id : 17,
	text : '27'
}, {
	id : 18,
	text : '28'
}, {
	id : 19,
	text : '29'
}, {
	id : 20,
	text : '30'
}, {
	id : 21,
	text : '31'
}, {
	id : 22,
	text : '32'
}, {
	id : 23,
	text : '33'
}, {
	id : 24,
	text : '34'
}, {
	id : 25,
	text : '35'
} ];
var gas_grav = [ {
	id : 0,
	text : '0.55'
}, {
	id : 1,
	text : '0.56'
}, {
	id : 2,
	text : '0.57'
}, {
	id : 3,
	text : '0.58'
}, {
	id : 4,
	text : '0.59'
}, {
	id : 5,
	text : '0.6'
}, {
	id : 6,
	text : '0.61'
}, {
	id : 7,
	text : '0.62'
}, {
	id : 8,
	text : '0.63'
}, {
	id : 9,
	text : '0.64'
}, {
	id : 10,
	text : '0.65'
}, {
	id : 11,
	text : '0.66'
}, {
	id : 12,
	text : '0.67'
}, {
	id : 13,
	text : '0.68'
}, {
	id : 14,
	text : '0.69'
}, {
	id : 15,
	text : '0.7'
} ];
var welltest = [ {
	id : 0,
	text : 'Drawdown Buildup Test'
}, {
	id : 1,
	text : 'Injection Falloff Test'
}, {
	id : 2,
	text : 'Interference Test'
}, {
	id : 3,
	text : 'Pressure Gradient Survey'
}, {
	id : 4,
	text : 'Deliverability Test'
} ];

var zoneresult = [{
	id: 'Oil',
	text: 'Oil'
}, {
	id: 'Gas',
	text: 'Gas'
}]
var wellcategory = [ {
	id : 'Oil',
	text : 'Oil'
}, {
	id : 'Gas',
	text : 'Gas'
}, {
	id : 'Oil & Gas',
	text : 'Oil & Gas'
}, {
	id : 'Dry',
	text : 'Dry'
}, {
	id : 'Not Conclusive',
	text : 'Not Conclusive'
} ];
var play_name = [
		{
			id : 0,
			text : 'Sandstone/ Marine Shoreface/ Sentolo/ Miocene/ Early/ Structural Tectonic Extensional'
		},
		{
			id : 1,
			text : 'Coal/ Delta Slope/ Semarung/ Cretaceous/ Middle/ Structural Tectonic Extensional'
		},
		{
			id : 2,
			text : 'Siltstone/ Bathyal/ Jungkong-Jungkong/ Pliocene/ Early/ Structural Tectonic Extensional'
		}, ];

var list_k3s = [
		{
			id : 0,
			text : 'AED Rombebai'
		},
		{
			id : 1,
			text : 'Altar Resources'
		},
		{
			id : 2,
			text : 'Amstelco Karapan'
		},
		{
			id : 3,
			text : 'Anadarko Indonesia Company'
		},
		{
			id : 4,
			text : 'Anadarko Papalang'
		},
		{
			id : 5,
			text : 'Anadarko Popodi'
		},
		{
			id : 6,
			text : 'ANP Energy Lampung'
		},
		{
			id : 7,
			text : 'Anugrah Mutiara Sentosa'
		},
		{
			id : 8,
			text : 'Anugrah Teknologi Indonesia'
		},
		{
			id : 9,
			text : 'AWE (Anambas)'
		},
		{
			id : 10,
			text : 'AWE (North Madura)'
		},
		{
			id : 11,
			text : 'AWE (Terumbu)'
		},
		{
			id : 12,
			text : 'AWE (Titan)'
		},
		{
			id : 13,
			text : 'Berdikari Semesta'
		},
		{
			id : 14,
			text : 'Bintang Berlian Energy'
		},
		{
			id : 15,
			text : 'Black Gold Indonesia'
		},
		{
			id : 16,
			text : 'Black Gold South Matindok'
		},
		{
			id : 17,
			text : 'Black Platinum Energy'
		},
		{
			id : 18,
			text : 'BOB Pertamina Hulu - Bumi Siak Pusako'
		},
		{
			id : 19,
			text : 'BP Berau'
		},
		{
			id : 20,
			text : 'BP Exploration Indonesia'
		},
		{
			id : 21,
			text : 'BP Muturi Holdings'
		},
		{
			id : 22,
			text : 'BP West Aru I'
		},
		{
			id : 23,
			text : 'BP West Aru II'
		},
		{
			id : 24,
			text : 'BP Wiriagar'
		},
		{
			id : 25,
			text : 'Brilliance Energy'
		},
		{
			id : 26,
			text : 'Bukit Energy Bohorok'
		},
		{
			id : 27,
			text : 'Bumi Hasta Mukti'
		},
		{
			id : 28,
			text : 'Bunga Mas International Company'
		},
		{
			id : 29,
			text : 'Caelus Energy'
		},
		{
			id : 30,
			text : 'Cahaya Batu Raja'
		},
		{
			id : 31,
			text : 'Cakra Nusa Darma (SKKMG)'
		},
		{
			id : 32,
			text : 'Camar Resources Canada'
		},
		{
			id : 33,
			text : 'Chevron East Ambalat'
		},
		{
			id : 34,
			text : 'Chevron Ganal'
		},
		{
			id : 35,
			text : 'Chevron Indonesia'
		},
		{
			id : 36,
			text : 'Chevron Makassar'
		},
		{
			id : 37,
			text : 'Chevron Pacific Indonesia'
		},
		{
			id : 38,
			text : 'Chevron Rapak'
		},
		{
			id : 39,
			text : 'Chevron West Papua I'
		},
		{
			id : 40,
			text : 'Chevron West Papua III'
		},
		{
			id : 41,
			text : 'CITIC Seram Energy'
		},
		{
			id : 42,
			text : 'CNOOC Batanghari'
		},
		{
			id : 43,
			text : 'CNOOC SE Palung Aru'
		},
		{
			id : 44,
			text : 'CNOOC SES'
		},
		{
			id : 45,
			text : 'ConocoPhillips (Amborip VI)'
		},
		{
			id : 46,
			text : 'ConocoPhillips (Arafura Sea Block Indonesia)'
		},
		{
			id : 47,
			text : 'ConocoPhillips (Grissik)'
		},
		{
			id : 48,
			text : 'ConocoPhillips (Kuma)'
		},
		{
			id : 49,
			text : 'ConocoPhillips (South Jambi)'
		},
		{
			id : 50,
			text : 'ConocoPhillips Indonesia'
		},
		{
			id : 51,
			text : 'ConocoPhillips Warim'
		},
		{
			id : 52,
			text : 'Conrad Petroluem'
		},
		{
			id : 53,
			text : 'Cooper Energy'
		},
		{
			id : 54,
			text : 'Easco East Sepanjang'
		},
		{
			id : 55,
			text : 'East Bawean'
		},
		{
			id : 56,
			text : 'Ecosse Energy (Bengkulu)'
		},
		{
			id : 57,
			text : 'Ecosse Energy (Manokwari)'
		},
		{
			id : 58,
			text : 'Ekuator Energi Kuningan'
		},
		{
			id : 59,
			text : 'Ekuator Energi Sokang'
		},
		{
			id : 60,
			text : 'Energi Mineral Langgeng'
		},
		{
			id : 61,
			text : 'Energy Equity Epic (Sengkang)'
		},
		{
			id : 62,
			text : 'Eni Ambalat'
		},
		{
			id : 63,
			text : 'Eni Bukat'
		},
		{
			id : 64,
			text : 'Eni Bulungan'
		},
		{
			id : 65,
			text : 'Eni East Sepinggan'
		},
		{
			id : 66,
			text : 'Eni Indonesia'
		},
		{
			id : 67,
			text : 'Eni Krueng Mane'
		},
		{
			id : 68,
			text : 'Eni Muara Bakau'
		},
		{
			id : 69,
			text : 'Eni North Ganal'
		},
		{
			id : 70,
			text : 'Eni West Timor'
		},
		{
			id : 71,
			text : 'Eurorich Group'
		},
		{
			id : 72,
			text : 'ExxonMobil E&P Indonesia (Gunting)'
		},
		{
			id : 73,
			text : 'ExxonMobil E&P Indonesia (Mandar)'
		},
		{
			id : 74,
			text : 'ExxonMobil E&P Indonesia (Surumana)'
		},
		{
			id : 75,
			text : 'ExxonMobil E&P Indonesia Cendrawasih'
		},
		{
			id : 76,
			text : 'ExxonMobil Oil Indonesia'
		},
		{
			id : 77,
			text : 'Genting Oil Kasuri'
		},
		{
			id : 78,
			text : 'Geo Bukit Batu'
		},
		{
			id : 79,
			text : 'Geraldo Energy'
		},
		{
			id : 80,
			text : 'Greenstar Assets'
		},
		{
			id : 81,
			text : 'Gujarat State Petroleum Corporation'
		},
		{
			id : 82,
			text : 'Harpindo Mitra Kharisma'
		},
		{
			id : 83,
			text : 'Harvest Budong-Budong'
		},
		{
			id : 84,
			text : 'Hess (Indonesia South Sesulu)'
		},
		{
			id : 85,
			text : 'Hess (Indonesia-Pangkah)'
		},
		{
			id : 86,
			text : 'Hess (Indonesia-Semai V)'
		},
		{
			id : 87,
			text : 'Hess (Indonesia-V)'
		},
		{
			id : 88,
			text : 'Hexindo Gemilang Jaya'
		},
		{
			id : 89,
			text : 'Husky Oil (Madura)'
		},
		{
			id : 90,
			text : 'Husky Oil North Sumbawa'
		},
		{
			id : 91,
			text : 'Indonesia Petroleum (INPEX)'
		},
		{
			id : 92,
			text : 'Indoreach Exploration'
		},
		{
			id : 93,
			text : 'Indrillco South Lirik'
		},
		{
			id : 94,
			text : 'INPEX Banda Sea'
		},
		{
			id : 95,
			text : 'INPEX Masela'
		},
		{
			id : 96,
			text : 'Insani Bina Perkasa'
		},
		{
			id : 97,
			text : 'JAPEX Buton'
		},
		{
			id : 98,
			text : 'JOB Pertamina - Golden Spike Indonesia'
		},
		{
			id : 99,
			text : 'JOB Pertamina - Hess Jambi Merang'
		},
		{
			id : 100,
			text : 'JOB Pertamina - Medco Simenggaris'
		},
		{
			id : 101,
			text : 'JOB Pertamina - Medco Tomori Sulawesi'
		},
		{
			id : 102,
			text : 'JOB Pertamina - PetroChina East Java'
		},
		{
			id : 103,
			text : 'JOB Pertamina - PetroChina Salawati'
		},
		{
			id : 104,
			text : 'JOB Pertamina - Talisman (Ogan Komering)'
		},
		{
			id : 105,
			text : 'Kalila (Korinci Baru)'
		},
		{
			id : 106,
			text : 'Kalila Bentu Segat'
		},
		{
			id : 107,
			text : 'Kalimantan Kutai Energi'
		},
		{
			id : 108,
			text : 'Kalisat Energi Nusantara'
		},
		{
			id : 109,
			text : 'Kalrez Petroleum (Seram)'
		},
		{
			id : 110,
			text : 'Kangean Energy Indonesia'
		},
		{
			id : 111,
			text : 'KE Babai Tanjung'
		},
		{
			id : 112,
			text : 'Kondur Petroleum'
		},
		{
			id : 113,
			text : 'Konsorsium Mubadala Petroleum Holdings (SE Asia) - INPEX Corporation'
		}, {
			id : 114,
			text : 'Konsorsium Samudra Energy - Caelus Energy Asia'
		}, {
			id : 115,
			text : 'Konsorsium Sandico Bumindo Raya - Duta Adhikarya Negeri'
		}, {
			id : 116,
			text : 'Konsorsium SPR - Kingswood Capital'
		}, {
			id : 117,
			text : 'KrisEnergy (East Muriah)'
		}, {
			id : 118,
			text : 'KrisEnergy (Satria)'
		}, {
			id : 119,
			text : 'KrisEnergy (Tanjung Aru)'
		}, {
			id : 120,
			text : 'KrisEnergy (Udan Emas)'
		}, {
			id : 121,
			text : 'KrisEnergy East Seruway'
		}, {
			id : 122,
			text : 'KrisEnergy Kutei'
		}, {
			id : 123,
			text : 'Kutai Etam Petroleum'
		}, {
			id : 124,
			text : 'Lapindo Brantas'
		}, {
			id : 125,
			text : 'Lundin Baronang'
		}, {
			id : 126,
			text : 'Lundin Cakalang'
		}, {
			id : 127,
			text : 'Lundin Cendrawasih VII'
		}, {
			id : 128,
			text : 'Lundin Gurita'
		}, {
			id : 129,
			text : 'Lundin Rangkas'
		}, {
			id : 130,
			text : 'Lundin South Sokang'
		}, {
			id : 131,
			text : 'M3nergy Gamma'
		}, {
			id : 132,
			text : 'Mandira Mahesa Energi'
		}, {
			id : 133,
			text : 'Mandiri Panca Usaha'
		}, {
			id : 134,
			text : 'Manhattan Kalimantan Investment'
		}, {
			id : 135,
			text : 'Marathon Indonesia (Bone Bay)'
		}, {
			id : 136,
			text : 'Marathon Indonesia (Kumawa)'
		}, {
			id : 137,
			text : 'Medco E&P Bengara'
		}, {
			id : 138,
			text : 'Medco E&P Indonesia'
		}, {
			id : 139,
			text : 'Medco E&P Malaka'
		}, {
			id : 140,
			text : 'Medco E&P Merangin'
		}, {
			id : 141,
			text : 'Medco E&P Rimau'
		}, {
			id : 142,
			text : 'Mentari Pambuang Internasional'
		}, {
			id : 143,
			text : 'Mitra Energy'
		}, {
			id : 144,
			text : 'Mitra Energy (Biliton)'
		}, {
			id : 145,
			text : 'Mitra Energy (Indonesia Sibaru)'
		}, {
			id : 146,
			text : 'Mobil Cepu'
		}, {
			id : 147,
			text : 'Mobil Exploration Indonesia'
		}, {
			id : 148,
			text : 'Mont d-Or Salawati'
		}, {
			id : 149,
			text : 'Mosesa Petroleum'
		}, {
			id : 150,
			text : 'MRI Energy'
		}, {
			id : 151,
			text : 'MRI Lirik'
		}, {
			id : 152,
			text : 'Murphy Overseas Ventures'
		}, {
			id : 153,
			text : 'Murphy Semai IV'
		}, {
			id : 154,
			text : 'Murphy Semai Oil'
		}, {
			id : 155,
			text : 'Murphy South Barito'
		}, {
			id : 156,
			text : 'Niko Resources (Aru)'
		}, {
			id : 157,
			text : 'Niko Resources (Cendrawasih Bay III)'
		}, {
			id : 158,
			text : 'Niko Resources (Cendrawasih Bay IV)'
		}, {
			id : 159,
			text : 'Niko Resources (East Bula)'
		}, {
			id : 160,
			text : 'Niko Resources (Halmahera Kofiau)'
		}, {
			id : 161,
			text : 'Niko Resources (Kofiau)'
		}, {
			id : 162,
			text : 'Niko Resources (North Makassar Strait)'
		}, {
			id : 163,
			text : 'Niko Resources (Obi)'
		}, {
			id : 164,
			text : 'Niko Resources (Overseas III)'
		}, {
			id : 165,
			text : 'Niko Resources (South East Seram)'
		}, {
			id : 166,
			text : 'Niko Resources (Sunda Strait I)'
		}, {
			id : 167,
			text : 'Niko Resources (West Papua Iv)'
		}, {
			id : 168,
			text : 'Niko Resources (West Sageri)'
		}, {
			id : 169,
			text : 'North Sokang Energy'
		}, {
			id : 170,
			text : 'Northern Yamano Technology Oil'
		}, {
			id : 171,
			text : 'Odira Energy Karang Agung'
		}, {
			id : 172,
			text : 'Orchard Energy (West Belida)'
		}, {
			id : 173,
			text : 'Pacific Oil & Gas Kisaran'
		}, {
			id : 174,
			text : 'Pan Orient Energy'
		}, {
			id : 175,
			text : 'Pan Orient Energy (Citarum)'
		}, {
			id : 176,
			text : 'Pandawa Prima Lestari'
		}, {
			id : 177,
			text : 'Pasir Petroleum'
		}, {
			id : 178,
			text : 'PC Ketapang II'
		}, {
			id : 179,
			text : 'PC Muriah'
		}, {
			id : 180,
			text : 'Pearl Oil (Sebuku)'
		}, {
			id : 181,
			text : 'Pearl Oil (Tachylyte)'
		}, {
			id : 182,
			text : 'Pearl Oil (Tungkal)'
		}, {
			id : 183,
			text : 'Perfect Circle Engineering'
		}, {
			id : 184,
			text : 'Perusda Benuo Taka'
		}, {
			id : 185,
			text : 'Petcon Borneo'
		}, {
			id : 186,
			text : 'PetroChina International (Bermuda)'
		}, {
			id : 187,
			text : 'PetroChina International Bangko'
		}, {
			id : 188,
			text : 'PetroChina International Jabung'
		}, {
			id : 189,
			text : 'PetroJava North Kangean'
		}, {
			id : 190,
			text : 'Petronas (West Glagah Kambuna)'
		}, {
			id : 191,
			text : 'Petronas Carigali Lampung II'
		}, {
			id : 192,
			text : 'Petroselat'
		}, {
			id : 193,
			text : 'PHE Gebang North Sumatra'
		}, {
			id : 194,
			text : 'PHE Nunukan'
		}, {
			id : 195,
			text : 'PHE ONWJ'
		}, {
			id : 196,
			text : 'PHE Randugunting'
		}, {
			id : 197,
			text : 'PHE WMO'
		}, {
			id : 198,
			text : 'Prabu Energy'
		}, {
			id : 199,
			text : 'Premier Oil Natuna Sea'
		}, {
			id : 200,
			text : 'Premier Oil Tuna'
		}, {
			id : 201,
			text : 'PTTEP Malunda'
		}, {
			id : 202,
			text : 'PTTEP South Mandar'
		}, {
			id : 203,
			text : 'Puri Petroleum'
		}, {
			id : 204,
			text : 'Putindo Bintech'
		}, {
			id : 205,
			text : 'Radiant Bukit Barisan E&P'
		}, {
			id : 206,
			text : 'Ranhill Jambi'
		}, {
			id : 207,
			text : 'Ranhill Pamai Taluk Energy'
		}, {
			id : 208,
			text : 'Realto Energi Nusantara Corelasi'
		}, {
			id : 209,
			text : 'Repsol Exploracion Cendrawasih II'
		}, {
			id : 210,
			text : 'Salamander Energy'
		}, {
			id : 211,
			text : 'Salamander Energy (Bontang)'
		}, {
			id : 212,
			text : 'Salamander Energy (Indonesia)'
		}, {
			id : 213,
			text : 'Salamander Energy (Js)'
		}, {
			id : 214,
			text : 'Salamander Energy (SE Sangatta)'
		}, {
			id : 215,
			text : 'Santos (Madura Offshore)'
		}, {
			id : 216,
			text : 'Santos (Sampang)'
		}, {
			id : 217,
			text : 'Santos Northwest Natuna'
		}, {
			id : 218,
			text : 'Sarmi Papua Asia Oil'
		}, {
			id : 219,
			text : 'Schintar'
		}, {
			id : 220,
			text : 'Sele Raya Energi'
		}, {
			id : 221,
			text : 'Sele Raya Merangin Dua'
		}, {
			id : 222,
			text : 'Seruway Offshore Exploration'
		}, {
			id : 223,
			text : 'Sigma Energy Petrogas'
		}, {
			id : 224,
			text : 'Sonlaw United Corporation'
		}, {
			id : 225,
			text : 'South Madura Exploration Company'
		}, {
			id : 226,
			text : 'SPC (Mahakam Hilir)'
		}, {
			id : 227,
			text : 'SPE Petroleum'
		}, {
			id : 228,
			text : 'Star Energy (Kakap)'
		}, {
			id : 229,
			text : 'Star Energy (Sebatik)'
		}, {
			id : 230,
			text : 'Star Energy (Sekayu)'
		}, {
			id : 231,
			text : 'Star Energy Bayumas'
		}, {
			id : 232,
			text : 'Statoil Indonesia Halmahera II'
		}, {
			id : 233,
			text : 'Statoil Karama'
		}, {
			id : 234,
			text : 'Suma Sarana'
		}, {
			id : 235,
			text : 'Sumatera Persada Energi'
		}, {
			id : 236,
			text : 'Talisman (South Makkasar)'
		}, {
			id : 237,
			text : 'Talisman Asia'
		}, {
			id : 238,
			text : 'Talisman Sadang'
		}, {
			id : 239,
			text : 'Talisman South Sageri'
		}, {
			id : 240,
			text : 'Tately'
		}, {
			id : 241,
			text : 'Techwin Energy'
		}, {
			id : 242,
			text : 'Techwin Energy Northeast Madura'
		}, {
			id : 243,
			text : 'Terra Global Vestal Baturaja'
		}, {
			id : 244,
			text : 'Texcal Mahato EP'
		}, {
			id : 245,
			text : 'Three Golden Energy West Tungkal'
		}, {
			id : 246,
			text : 'Tiara Bumi Petrloleum'
		}, {
			id : 247,
			text : 'Titan Resources (Natuna) Indonesia'
		}, {
			id : 248,
			text : 'Total E&P'
		}, {
			id : 249,
			text : 'Total E&P Indonesie'
		}, {
			id : 250,
			text : 'Total E&P Inonesia Telen'
		}, {
			id : 251,
			text : 'Total E&P Inonesie Mentawai'
		}, {
			id : 252,
			text : 'Total E&P Southeast Mahakam'
		}, {
			id : 253,
			text : 'Triangle Pase'
		}, {
			id : 254,
			text : 'Tropik Energi'
		}, {
			id : 255,
			text : 'Vico'
		}, {
			id : 256,
			text : 'West Natuna Exploration'
		}, {
			id : 257,
			text : 'Zaratex'
		} ];

var list_province = [ {
	id : 1100,
	text : 'Nanggroe Aceh Darussalam'
}, {
	id : 2,
	text : 'Sumatra Utara'
}, {
	id : 3,
	text : 'Sumatra Barat'
}, {
	id : 4,
	text : 'Riau'
}, {
	id : 5,
	text : 'Jambi'
}, {
	id : 6,
	text : 'Sumatra Selatan'
}, {
	id : 7,
	text : 'Bengkulu'
}, {
	id : 8,
	text : 'Lampung'
}, {
	id : 9,
	text : 'Kepulauan Bangka Belitung'
}, {
	id : 10,
	text : 'Kepulauan Riau'
}, {
	id : 11,
	text : 'Daerah Khusus Ibukota Jakarta'
}, {
	id : 12,
	text : 'Jawa Barat'
}, {
	id : 13,
	text : 'Jawa Tengah'
}, {
	id : 14,
	text : 'Daerah Istimewa Yogyakarta'
}, {
	id : 15,
	text : 'Jawa Timur'
}, {
	id : 16,
	text : 'Banten'
}, {
	id : 17,
	text : 'Bali'
}, {
	id : 18,
	text : 'Nusa Tenggara Barat'
}, {
	id : 19,
	text : 'Nusa Tenggara Timur'
}, {
	id : 20,
	text : 'Kalimantan Barat'
}, {
	id : 21,
	text : 'Kalimantan Tengah'
}, {
	id : 22,
	text : 'Kalimantan Selatan'
}, {
	id : 23,
	text : 'Kalimantan Timur'
}, {
	id : 24,
	text : 'Sulawesi Utara'
}, {
	id : 25,
	text : 'Sulawesi Tengah'
}, {
	id : 26,
	text : 'Sulawesi Selatan'
}, {
	id : 27,
	text : 'Sulawesi Tenggara'
}, {
	id : 28,
	text : 'Gorontalo'
}, {
	id : 29,
	text : 'Sulawesi Barat'
}, {
	id : 30,
	text : 'Maluku'
}, {
	id : 31,
	text : 'Maluku Utara'
}, {
	id : 32,
	text : 'Papua'
}, {
	id : 33,
	text : 'Papua Barat'
}, ];
// ======================= END VARIABLE SELECT 2 =======================//

var status_data = [tahun_rps, status_name];

var status_id = ['#tahun_rps', '#status_name'];

var adjacent_activity_data = [near_field, near_infra_structure];

var adjacent_activity_id = ['#n_facility', '#n_developmentwell'];

var well_result_postdrill_data = [well_result_postdrill];

var well_result_postdrill_id = ['#well_result_postdrill'];

var well_result_discovery_data = [well_result_discovery];

var well_result_discovery_id = ['#well_result_discovery'];

var prospect_type_data = [prospect_type];

var prospect_type_id = ['#prospect_type'];

var map_data = [map_category];

var map_id = ['#map_category'];

// ======================= DATA AND ID PLAY =======================//
var gcf_data = [ 
        prov_analog, agesystem, ageseries, format_name, 
        format_name_post, typekerogen, capacity_toc, hfu, 
        distrib_lim, continuity_good, maturity, yes_no_unknown_data,
		prov_analog, agesystem, ageseries, format_name, 
		format_name_post, depos_set, depos_env, distrib_, 
		continuity_, lithology, porositytype, prim_porosity,
		sec_porosity, prov_analog, agesystem, ageseries, 
		format_name, format_name_post, sealing_distrib, continuity_, sealingtype, 
		agesystem, ageseries, trapping_geom, trapping_type, 
		closure_type, prov_analog, autentic_migration, tpdtk, 
		toeps, agesystem, ageseries, agesystem,
		ageseries, p_spe, migration_pathway, agesystem, 
		ageseries,

		prov_analog, agesystem, ageseries, format_name, 
		format_name_post, typekerogen,
		capacity_toc, hfu, distrib_lim, continuity_good, maturity, yes_no_unknown_data,
		prov_analog, agesystem, ageseries, format_name, format_name_post, depos_set, depos_env,
		distrib_, continuity_, lithology, porositytype, prim_porosity,
		sec_porosity, prov_analog, agesystem, ageseries, format_name, format_name_post,
		sealing_distrib, continuity_, sealingtype, agesystem, ageseries,
		trapping_geom, trapping_type, closure_type, prov_analog,
		autentic_migration, tpdtk, toeps, agesystem, ageseries, agesystem,
		ageseries, p_spe, migration_pathway, agesystem, ageseries ];

var gcf_id = [ 
        "#pgcfsrock", "#pgcfsrock1", "#pgcfsrock1a", "#pgcfsrock2",
        "#pgcf_sr_formation_serie", "#pgcfsrock3", "#pgcfsrock4", "#pgcfsrock5", 
        "#pgcfsrock6", "#pgcfsrock7", "#pgcfsrock8", "#pgcfsrock9", 
        "#pgcfres", "#pgcfres1", "#pgcfres1a", "#pgcfres2", 
        "#pgcf_res_formation_serie", "#pgcfres3", "#pgcfres4", "#pgcfres5",
		"#pgcfres6", "#pgcfres7", "#pgcfres8", "#pgcfres9", 
		"#pgcfres10", "#pgcftrap", "#pgcftrap1", "#pgcftrap1a", 
		"#pgcftrap2", "#pgcf_trap_seal_formation_serie", "#pgcftrap3", "#pgcftrap4", "#pgcftrap5", 
		"#pgcftrap6", "#pgcftrap6a", "#pgcftrap7", "#pgcftrap8", 
		"#pgcftrap9", "#pgcfdyn", "#pgcfdyn1", "#pgcfdyn2",
		"#pgcfdyn3", "#pgcfdyn4", "#pgcfdyn4a", "#pgcfdyn5", 
		"#pgcfdyn5a", "#pgcfdyn6", "#pgcfdyn7", "#pgcfdyn8", 
		"#pgcfdyn8a",

		"#gcfsrock", "#gcfsrock1", "#gcfsrock1a", "#gcfsrock2", 
		"#gcf_sr_formation_serie", "#gcfsrock3",
		"#gcfsrock4", "#gcfsrock5", "#gcfsrock6", "#gcfsrock7", "#gcfsrock8",
		"#gcfsrock9", "#gcfres", "#gcfres1", "#gcfres1a", "#gcfres2", "#gcf_res_formation_serie",
		"#gcfres3", "#gcfres4", "#gcfres5", "#gcfres6", "#gcfres7", "#gcfres8",
		"#gcfres9", "#gcfres10", "#gcftrap", "#gcftrap1", "#gcftrap1a",
		"#gcftrap2", "#gcf_trap_seal_formation_serie", "#gcftrap3", "#gcftrap4", "#gcftrap5", "#gcftrap6",
		"#gcftrap6a", "#gcftrap7", "#gcftrap8", "#gcftrap9", "#gcfdyn",
		"#gcfdyn1", "#gcfdyn2", "#gcfdyn3", "#gcfdyn4", "#gcfdyn4a",
		"#gcfdyn5", "#gcfdyn5a", "#gcfdyn6", "#gcfdyn7", "#gcfdyn8",
		"#gcfdyn8a" ];

var environment_data = [ onoffshore, terrain, ];
var environment_id = [ "#env_onoffshore", "#env_terrain" ];

var survey_availability_data = [ surveymethod_geo, numofvintage, lpm, siq,
		antrip, surveymethods, ahc, surveymethods, surveymethods, numofvintage,
		lpm, siq, antrip, yes_no_data, yes_no_data, yes_no_data ];
var survey_availability_id = [ "#dav_gfs_surmethod", "#dav_2dss_numvin",
		"#dav_2dss_lpm", "#dav_2dss_siq", "#dav_2dss_antrip",
		"#dav_grasur_surveymethods", "#dav_geo_ahc", "#dav_elec_surveymethods",
		"#dav_res_surveymethods", "#dav_3dss_numvin", "#dav_3dss_lpm",
		"#dav_3dss_siq", "#dav_3dss_antrip", "#dav_3dss_adwpo",
		"#dav_3dss_ar3p", "#dav_3dss_arrfp" ];

var wellandzone_id = [];

var _data = [ 
        list_k3s, kosong, basin, list_province, byanalogto,
		distancetoanalog, explorationmethods, prov_analog, dsipl, siq,
		yes_no_data, yes_no_data, yes_no_data, yes_no_data, play_name, 
		clarifiedby_l, processingtype, ana_esti_data, ana_esti_data, ana_esti_data, 
		ana_esti_data, ana_esti_data, ana_esti_data,

		howwell, welltype, welltype_pd_dc, wellstatus, wellstatus, 
		onoffshore, onoffshore, terrain, terrain, format_name,
		numsample, numsample, numsample, numsample, awi, 
		awi, yes_no_data, yes_no_data, methodsroks, numsample, 
		yes_no_data, welltype_pd_dc, wellstatus, onoffshore, terrain, 
		format_name, numsample, numsample, awi, yes_no_data, 
		methodsroks, numsample, yes_no_data, welltype_pd_dc, wellstatus, 
		onoffshore, terrain, format_name, numsample, numsample, 
		awi, yes_no_data, methodsroks, numsample, yes_no_data, 
		welltype_pd_dc, wellstatus, onoffshore, terrain, format_name,
		numsample, numsample, awi, yes_no_data, methodsroks, 
		numsample, yes_no_data,

		howzone, howzone, howzone, howzone, howzone, 
		yes_no_data, yes_no_data, yes_no_data, yes_no_data, yes_no_data,
		zoneresult, zoneresult, zoneresult, zoneresult, zoneresult,
		welltest, welltest, welltest, welltest, welltest, 
		welltest, welltest, methodsroks, numsample, methodsroks, 
		numsample, methodsroks, numsample, methodsroks, numsample, 
		methodsroks, numsample, methodsroks, numsample, welltest, 
		welltest, methodsroks, numsample, welltest, welltest, 
		methodsroks, numsample, welltest, welltest, methodsroks,
		numsample, welltest, welltest, methodsroks, numsample,

		welltest, welltest, methodsroks, numsample, welltest, welltest,
		methodsroks, numsample, welltest, welltest, methodsroks, numsample,
		welltest, welltest, methodsroks, numsample, welltest, welltest,
		methodsroks, numsample,

		welltest, welltest, methodsroks, numsample, welltest, welltest,
		methodsroks, numsample, welltest, welltest, methodsroks, numsample,
		welltest, welltest, methodsroks, numsample, welltest, welltest,
		methodsroks, numsample,

		welltest, welltest, methodsroks, numsample, welltest, welltest,
		methodsroks, numsample, welltest, welltest, methodsroks, numsample,
		welltest, welltest, methodsroks, numsample, welltest, welltest,
		methodsroks, numsample,

		wellcategory, yes_no_data, yes_no_data,
		yes_no_data, yes_no_data, yes_no_data, yes_no_data, yes_no_data,
		yes_no_data, yes_no_data, yes_no_data, yes_no_data, yes_no_data,
		yes_no_data,
		yes_no_data, yes_no_data, yes_no_data, yes_no_data, yes_no_data ];

var _id = [ 
        "#k3s_", "#e_cov_operator", "#_basin", "#_province", "#_byanalogto", 
        "#_distancetoanalog", "#_explorationmethods", "#_supportdata", "#osd_dsipl", "#osd_siq", 
        "#oos_1", "#oos_4", "#oos_6", "#oos_9", "#play_in", 
        "#_clarifiedby", "#_processingtype", "#porosityrock_p90", "#porosityrock_p50", "#porosityrock_p10", 
		"#watersat_p90", "#watersat_p50", "#watersat_p10",

		"#w", "#well_type", "#well_type1", "#well_status", "#well_status1", 
		"#onoffshore", "#onoffshore1", "#terrain", "#terrain1", "#targeted_forname1", 
		"#num_mdt", "#num_mdt1", "#num_rft", "#num_rft1", "#actual_well_integrity", 
		"#actual_well_integrity1", "#availability_electrolog", "#availability_electrolog1", "#rocksampling1", "#petrograp_analys1",
		"#report_avail", "#well_type2", "#well_status2", "#onoffshore2", "#terrain2", 
		"#targeted_forname2", "#num_mdt2", "#num_rft2", "#actual_well_integrity2", "#availability_electrolog2",
		"#rocksampling2", "#petrograp_analys2", "#report_avail2", "#well_type3", "#well_status3", 
		"#onoffshore3", "#terrain3", "#targeted_forname3", "#num_mdt3", "#num_rft3",
		"#actual_well_integrity3", "#availability_electrolog3", "#rocksampling3", "#petrograp_analys3", "#report_avail3",
		"#well_type4", "#well_status4", "#onoffshore4", "#terrain4", "#targeted_forname4", 
		"#num_mdt4", "#num_rft4", "#actual_well_integrity4", "#availability_electrolog4", "#rocksampling4", 
		"#petrograp_analys4", "#report_avail4",

		"#wz_", "#wz_1", "#wz_2", "#wz_3", "#wz_4", 
		"#welltest1", "#welltest2", "#welltest3", "#welltest4", "#welltest5",
		"#zoneresult1", "#zoneresult2", "#zoneresult3", "#zoneresult4", "#zoneresult5",
		"#welltest11",  "#welltest_type1", "#welltest_type2", "#welltest_type3", "#welltest_type4", 
		"#welltest_type5","#welltest_type11", "#rock_sampling1", "#petro_analys1", "#rock_sampling2", 
		"#petro_analys2", "#rock_sampling3", "#petro_analys3", "#rock_sampling4", "#petro_analys4", 
		"#rock_sampling5", "#petro_analys5", "#rock_sampling11", "#petro_analys11", "#welltest12", 
		"#welltest_type12", "#rock_sampling12", "#petro_analys12", "#welltest13", "#welltest_type13",
		"#rock_sampling13", "#petro_analys13", "#welltest14", "#welltest_type14", "#rock_sampling14", 
		"#petro_analys14", "#welltest15", "#welltest_type15", "#rock_sampling15", "#petro_analys15",

		"#welltest21", "#welltest_type21", "#rock_sampling21", "#petro_analys21",
		"#welltest22", "#welltest_type22", "#rock_sampling22",
		"#petro_analys22", "#welltest23", "#welltest_type23",
		"#rock_sampling23", "#petro_analys23", "#welltest24",
		"#welltest_type24", "#2rock_sampling4", "#petro_analys24",
		"#welltest25", "#welltest_type25", "#rock_sampling25",
		"#petro_analys25",

		"#welltest31", "#welltest_type31", "#rock_sampling31", "#petro_analys31",
		"#welltest32", "#welltest_type32", "#rock_sampling32",
		"#petro_analys32", "#welltest33", "#welltest_type33",
		"#rock_sampling33", "#petro_analys33", "#welltest34",
		"#welltest_type34", "#rock_sampling34", "#petro_analys34",
		"#welltest35", "#welltest_type35", "#rock_sampling35",
		"#petro_analys35",

		"#welltest41", "#welltest_type41", "#rock_sampling41", "#petro_analys41",
		"#welltest42", "welltest_type42", "#rock_sampling42",
		"#petro_analys42", "#welltest43", "#welltest_type43",
		"#rock_sampling43", "#petro_analys43", "#welltest44",
		"#welltest_type44", "#rock_sampling44", "#petro_analys44",
		"#welltest45", "#welltest_type45", "#rock_sampling45",
		"#petro_analys45",

		"#wellcategory_", "#oilshow_reading1", "#gasshow_reading1", 
		"#oilshow_read11", "#oilshow_read12", "#oilshow_read13", "#oilshow_read14", "#oilshow_read15",
		"#gasshow_read11", "#gasshow_read12", "#gasshow_read13", "#gasshow_read14", "#gasshow_read15",
		"#pvt_analysis",
		"#pvt_1", "#pvt_2", "#pvt_3", "#pvt_4", "#pvt_5"];

// ======================= DATA AND ID DISCOVERY =======================//

var oo = "#"

function s_2(target, tulisan, clear, isi) {
	$(target).select2({
		placeholder : tulisan,
		allowClear : clear,
		data : isi
	});
}
function bikin_sekaligus(list_id, list_data) {
	for ( var x in list_id) {
		s_2(list_id[x], "Pilih", true, list_data[x]);
	}
}
function nyala_s2(target) {
	for ( var x in target) {
		$(target[x]).select2("enable", true);
		$(target[x]).select2("readonly", false);
	}
}
function mati_s2(target) {
	for ( var x in target) {
		if (target[x] == '#gcfres1' || target[x] == '#gcfres1a'
				|| target[x] == '#gcfres2' || target[x] == '#gcf_res_formation_serie' || target[x] == '#gcfres4' || target[x] == '#gcfres7' || target[x] == '#gcftrap8') {
			$(target[x]).select2("readonly", true);
		} else {
			$(target[x]).select2("readonly", true);
			$(target[x]).select2("val", "");
		}

	}
}
function clear_s2(target) {
	for ( var x in target) {
		$(target[x]).select2("val", "");
	}
}
function nyala_input(target) {
	for ( var x in target) {
		$(target[x]).prop('readonly', false);
	}
}
function mati_input(target) {
	for ( var x in target) {
		$(target[x]).prop('readonly', true);
		$(target[x]).val("");
	}
}

function clear_this(target) {
	for ( var x in target) {
		document.getElementById(target[x]).value = "";
	}
}

function enable_in(id_nya) {
	document.getElementById(id_nya).setAttribute('class', 'control-group ');
}
function disable_in(id_nyo) {
	document.getElementById(id_nyo).setAttribute('class',
			'control-group hidden');
}

function defaultGcf(id) {
	for(var x in id) {
		$(id[x]).text(0.5);
	}
}

function disabledElement(id) {
	switch (id) {
	case 'oos_1':
		if ($('#' + id).val() == "") {
			mati_input([ "#oos_2", "#oos_3" ]);
		} else if ($('#' + id).val() == 'Yes') {
			nyala_input([ "#oos_2", "#oos_3" ]);
		} else if ($('#' + id).val() == 'No') {
			mati_input([ "#oos_2", "#oos_3" ]);
		}
		break;
	case 'oos_4':
		if ($('#' + id).val() == "") {
			mati_input([ "#oos_5", "#oos_11" ]);
		} else if ($('#' + id).val() == 'Yes') {
			nyala_input([ "#oos_5", "#oos_11" ]);
		} else if ($('#' + id).val() == 'No') {
			mati_input([ "#oos_5", "#oos_11" ]);
		}
		break;
	case 'oos_6':
		if ($('#' + id).val() == "") {
			mati_input([ "#oos_7", "#oos_8" ]);
		} else if ($('#' + id).val() == 'Yes') {
			nyala_input([ "#oos_7", "#oos_8" ]);
		} else if ($('#' + id).val() == 'No') {
			mati_input([ "#oos_7", "#oos_8" ]);
		}
		break;
	case 'oos_9':
		if ($('#' + id).val() == "") {
			mati_input([ "#oos_10"]);
		} else if ($('#' + id).val() == 'Yes') {
			nyala_input([ "#oos_10"]);
		} else if ($('#' + id).val() == 'No') {
			mati_input([ "#oos_10"]);
		}
		break;
	// ============== PLAY FUNCTION ON/OFF ==============//
	case 'pgcfsrock':
		if ($('#' + id).val() == "") {
			mati_s2([ "#pgcfsrock1", "#pgcfsrock1a", "#pgcfsrock2",
					"#pgcf_sr_formation_serie", "#pgcfsrock3", "#pgcfsrock4", "#pgcfsrock5", "#pgcfsrock6",
					"#pgcfsrock7", "#pgcfsrock8", "#pgcfsrock9" ]);
			defaultGcf([
				"#sr_ker", "#sr_toc", "#sr_hfu", "#sr_mat", "#sr_otr"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#pgcfsrock1", "#pgcfsrock1a", "#pgcfsrock2",
			           "#pgcf_sr_formation_serie", "#pgcfsrock3", "#pgcfsrock4", "#pgcfsrock5", "#pgcfsrock6",
					"#pgcfsrock7", "#pgcfsrock8", "#pgcfsrock9" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#pgcfsrock1", "#pgcfsrock1a", "#pgcfsrock2",
			           "#pgcf_sr_formation_serie", "#pgcfsrock3", "#pgcfsrock4", "#pgcfsrock5", "#pgcfsrock6",
					"#pgcfsrock7", "#pgcfsrock8", "#pgcfsrock9" ]);
		}
		break;
	case 'pgcfres':
		if ($('#' + id).val() == "") {
			mati_s2([ "#pgcfres1", "#pgcfres1a", "#pgcfres2", 
			          "#pgcf_res_formation_serie", "#pgcfres3",
					"#pgcfres4", "#pgcfres5", "#pgcfres6", "#pgcfres7",
					"#pgcfres8", , "#pgcfres9", "#pgcfres10" ]);
			defaultGcf([
				"#re_dis", "#re_lit", "#re_pri", "#sr_mat", "#re_sec"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#pgcfres1", "#pgcfres1a", "#pgcfres2", 
			           "#pgcf_res_formation_serie", "#pgcfres3",
					"#pgcfres4", "#pgcfres5", "#pgcfres6", "#pgcfres7",
					"#pgcfres8", , "#pgcfres9", "#pgcfres10" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#pgcfres1", "#pgcfres1a", "#pgcfres2", 
			           "#pgcf_res_formation_serie", "#pgcfres3",
					"#pgcfres4", "#pgcfres5", "#pgcfres6", "#pgcfres7",
					"#pgcfres8", , "#pgcfres9", "#pgcfres10" ]);
		}
		break;
	case 'pgcftrap':
		if ($('#' + id).val() == "") {
			mati_s2([ "#pgcftrap1", "#pgcftrap1a", "#pgcftrap2", 
			          "#pgcf_trap_seal_formation_serie", "#pgcftrap3",
					"#pgcftrap4", "#pgcftrap5", "#pgcftrap6", "#pgcftrap6a",
					"#pgcftrap7", "#pgcftrap8", "#pgcftrap9" ]);
			defaultGcf([
				"#tr_sdi", "#tr_scn", "#tr_stp", "#tr_geo", "#tr_trp"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#pgcftrap1", "#pgcftrap1a", "#pgcftrap2", 
			           "#pgcf_trap_seal_formation_serie", "#pgcftrap3",
					"#pgcftrap4", "#pgcftrap5", "#pgcftrap6", "#pgcftrap6a",
					"#pgcftrap7", "#pgcftrap8", "#pgcftrap9" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#pgcftrap1", "#pgcftrap1a", "#pgcftrap2", 
			           "#pgcf_trap_seal_formation_serie", "#pgcftrap3",
					"#pgcftrap4", "#pgcftrap5", "#pgcftrap6", "#pgcftrap6a",
					"#pgcftrap7", "#pgcftrap8", "#pgcftrap9" ]);
		}
		break;
	case 'pgcfdyn':
		if ($('#' + id).val() == "") {
			mati_s2([ "#pgcfdyn1", "#pgcfdyn2", "#pgcfdyn3", "#pgcfdyn4",
					"#pgcfdyn4a", "#pgcfdyn5", "#pgcfdyn5a", "#pgcfdyn6",
					"#pgcfdyn7", "#pgcfdyn8", "#pgcfdyn8a" ]);
			defaultGcf([
				"#dn_kit", "#dn_tec", "#dn_prv", "#dn_mig"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#pgcfdyn1", "#pgcfdyn2", "#pgcfdyn3", "#pgcfdyn4",
					"#pgcfdyn4a", "#pgcfdyn5", "#pgcfdyn5a", "#pgcfdyn6",
					"#pgcfdyn7", "#pgcfdyn8", "#pgcfdyn8a" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#pgcfdyn1", "#pgcfdyn2", "#pgcfdyn3", "#pgcfdyn4",
					"#pgcfdyn4a", "#pgcfdyn5", "#pgcfdyn5a", "#pgcfdyn6",
					"#pgcfdyn7", "#pgcfdyn8", "#pgcfdyn8a" ]);
		}
		break;
	case 'pgcfres8':
		if ($('#' + id).val() == 'Both') {
			enable_in("prim_pro");
			enable_in("second_pro");
		} else if ($('#' + id).val() == "") {
			disable_in("prim_pro");
			disable_in("second_pro");
		} else if ($('#' + id).val() == 'Secondary Porosity') {
			disable_in("prim_pro");
			enable_in("second_pro");
		} else if ($('#' + id).val() == 'Average Primary Porosity Reservoir') {
			enable_in("prim_pro");
			disable_in("second_pro");
		}
		break;

	// ============== LEAD, DRILLABLE, POST DRILL, DISCOVERY FUNCTION ON/OFF
	// ==============//
	case 'gcfsrock':
		if ($('#' + id).val() == "") {
			mati_s2([ "#gcfsrock1", "#gcfsrock1a", "#gcfsrock2", 
			          "#gcf_sr_formation_serie", "#gcfsrock3",
					"#gcfsrock4", "#gcfsrock5", "#gcfsrock6", "#gcfsrock7",
					"#gcfsrock8", "#gcfsrock9" ]);
			defaultGcf([
				"#sr_ker", "#sr_toc", "#sr_hfu", "#sr_mat", "#sr_otr"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#gcfsrock1", "#gcfsrock1a", "#gcfsrock2", "#gcf_sr_formation_serie", "#gcfsrock3", "#gcfsrock4", "#gcfsrock5",
					"#gcfsrock6", "#gcfsrock7", "#gcfsrock8", "#gcfsrock9" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#gcfsrock1", "#gcfsrock1a", "#gcfsrock2", "#gcf_sr_formation_serie", "#gcfsrock3", "#gcfsrock4", "#gcfsrock5",
					"#gcfsrock6", "#gcfsrock7", "#gcfsrock8", "#gcfsrock9" ]);
		}
		break;
	case 'gcfres':
		if ($('#' + id).val() == "") {
			mati_s2([ "#gcfres1", "#gcfres1a", "#gcfres2", "#gcf_res_formation_serie", "#gcfres3",
					"#gcfres4", "#gcfres5", "#gcfres6", "#gcfres7", "#gcfres8",
					, "#gcfres9", "#gcfres10" ]);
			defaultGcf([
				"#re_dis", "#re_pri", "#sr_mat", "#re_sec"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#gcfres3", "#gcfres5", "#gcfres6", "#gcfres8", "#gcfres9", "#gcfres10" ]);
			mati_s2([ "#gcfres1", "#gcfres1a", "#gcfres2", "#gcf_res_formation_serie", "#gcfres4",
					"#gcfres7" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#gcfres3", "#gcfres5", "#gcfres6", "#gcfres8", "#gcfres9", "#gcfres10" ]);
			mati_s2([ "#gcfres1", "#gcfres1a", "#gcfres2", "#gcf_res_formation_serie", "#gcfres4",
					"#gcfres7" ]);
		}
		break;
	case 'gcftrap':
		if ($('#' + id).val() == "") {
			mati_s2([ "#gcftrap1", "#gcftrap1a", "#gcftrap2", "#gcf_trap_seal_formation_serie", "#gcftrap3",
					"#gcftrap4", "#gcftrap5", "#gcftrap6", "#gcftrap6a",
					"#gcftrap7", "#gcftrap8", "#gcftrap9" ]);
			defaultGcf([
				"#tr_sdi", "#tr_scn", "#tr_stp", "#tr_geo"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#gcftrap1", "#gcftrap1a", "#gcftrap2", "#gcf_trap_seal_formation_serie", "#gcftrap3",
					"#gcftrap4", "#gcftrap5", "#gcftrap6", "#gcftrap6a",
					"#gcftrap7", "#gcftrap9" ]);
			mati_s2([ "#gcftrap8" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#gcftrap1", "#gcftrap1a", "#gcftrap2", "#gcf_trap_seal_formation_serie", "#gcftrap3",
					"#gcftrap4", "#gcftrap5", "#gcftrap6", "#gcftrap6a",
					"#gcftrap7", "#gcftrap9" ]);
			mati_s2([ "#gcftrap8" ]);
		}
		break;
	case 'gcfdyn':
		if ($('#' + id).val() == "") {
			mati_s2([ "#gcfdyn1", "#gcfdyn2", "#gcfdyn3", "#gcfdyn4",
					"#gcfdyn4a", "#gcfdyn5", "#gcfdyn5a", "#gcfdyn6",
					"#gcfdyn7", "#gcfdyn8", "#gcfdyn8a" ]);
			defaultGcf([
				"#dn_kit", "#dn_tec", "#dn_prv", "#dn_mig"
			]);
		} else if ($('#' + id).val() == 'Analog') {
			nyala_s2([ "#gcfdyn3", "#gcfdyn4", "#gcfdyn4a", "#gcfdyn5",
					"#gcfdyn5a", "#gcfdyn6", "#gcfdyn7", "#gcfdyn8",
					"#gcfdyn8a", "#gcfdyn1", "#gcfdyn2" ]);
		} else if ($('#' + id).val() == 'Proven') {
			nyala_s2([ "#gcfdyn3", "#gcfdyn4", "#gcfdyn4a", "#gcfdyn5",
					"#gcfdyn5a", "#gcfdyn6", "#gcfdyn7", "#gcfdyn8",
					"#gcfdyn8a", "#gcfdyn1", "#gcfdyn2" ]);
		}
		break;
	case 'gcfres8':
		if ($('#' + id).val() == 'Both') {
			enable_in("prim_pro");
			enable_in("second_pro");
		} else if ($('#' + id).val() == "") {
			disable_in("prim_pro");
			disable_in("second_pro");
		} else if ($('#' + id).val() == 'Secondary Porosity') {
			disable_in("prim_pro");
			enable_in("second_pro");
		} else if ($('#' + id).val() == 'Average Primary Porosity Reservoir') {
			enable_in("prim_pro");
			disable_in("second_pro");
		}
		break;

	// ===== Well =====//
	case 'w':
		if ($('#' + id).val() == 4) {
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_1").removeClass("hidden");
			$("#well_2").removeClass("hidden");
			$("#well_3").removeClass("hidden");
			$("#well_4").removeClass("hidden");
			disabledElement("wz_1");
			disabledElement("wz_2");
			disabledElement("wz_3");
			disabledElement("wz_4");
		} else if ($('#' + id).val() == 3) {
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_1").removeClass("hidden");
			$("#well_2").removeClass("hidden");
			$("#well_3").removeClass("hidden");
			$("#well_4").addClass("hidden");
			$("#tab_w4").addClass("hidden"); // tambahan
			$("#tab_w1").addClass("active"); // tambahan
			disabledElement("wz_1");
			disabledElement("wz_2");
			disabledElement("wz_3");
		} else if ($('#' + id).val() == "") {
			$("#rumah_well").find("div").addClass("hidden");
			$("#well_1").addClass("hidden");
			$("#well_2").addClass("hidden");
			$("#well_3").addClass("hidden");
			$("#well_4").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_1").removeClass("hidden");
			$("#well_2").removeClass("hidden");
			$("#well_3").addClass("hidden");
			$("#well_4").addClass("hidden");
			$("#tab_w3").addClass("hidden"); // tambahan
			$("#tab_w4").addClass("hidden"); // tambahan
			$("#tab_w1").addClass("active"); // tambahan
			disabledElement("wz_1");
			disabledElement("wz_2");
		} else if ($('#' + id).val() == 1) {
			$("#well_1").removeClass("hidden");
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_2").addClass("hidden");
			$("#well_3").addClass("hidden");
			$("#well_4").addClass("hidden");
			$("#tab_w2").addClass("hidden"); // tambahan
			$("#tab_w3").addClass("hidden"); // tambahan
			$("#tab_w4").addClass("hidden"); // tambahan
			$("#tab_w1").addClass("active"); // tambahan
			disabledElement("wz_1");
		}
		break;

	case 'waw':
		if ($('#' + id).val() == 4) {
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_1").removeClass("hidden");
			$("#well_2").removeClass("hidden");
			$("#well_3").removeClass("hidden");
			$("#well_4").removeClass("hidden");
			disabledElement("total_wz_1");
			disabledElement("total_wz_2");
			disabledElement("total_wz_3");
			disabledElement("total_wz_4");
		} else if ($('#' + id).val() == 3) {
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_1").removeClass("hidden");
			$("#well_2").removeClass("hidden");
			$("#well_3").removeClass("hidden");
			$("#well_4").addClass("hidden");
			$("#tab_w4").addClass("hidden"); // tambahan
			$("#tab_w1").addClass("active"); // tambahan
			disabledElement("total_wz_1");
			disabledElement("total_wz_2");
			disabledElement("total_wz_3");
		} else if ($('#' + id).val() == "") {
			$("#rumah_well").find("div").addClass("hidden");
			$("#well_1").addClass("hidden");
			$("#well_2").addClass("hidden");
			$("#well_3").addClass("hidden");
			$("#well_4").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_1").removeClass("hidden");
			$("#well_2").removeClass("hidden");
			$("#well_3").addClass("hidden");
			$("#well_4").addClass("hidden");
			$("#tab_w3").addClass("hidden"); // tambahan
			$("#tab_w4").addClass("hidden"); // tambahan
			$("#tab_w1").addClass("active"); // tambahan
			disabledElement("total_wz_1");
			disabledElement("total_wz_2");
		} else if ($('#' + id).val() == 1) {
			$("#well_1").removeClass("hidden");
			$("#rumah_well").find("div").removeClass("hidden");
			$("#well_2").addClass("hidden");
			$("#well_3").addClass("hidden");
			$("#well_4").addClass("hidden");
			$("#tab_w2").addClass("hidden"); // tambahan
			$("#tab_w3").addClass("hidden"); // tambahan
			$("#tab_w4").addClass("hidden"); // tambahan
			$("#tab_w1").addClass("active"); // tambahan
			disabledElement("total_wz_1");
		}
		break;
	// ===== Zone for Well =====//
	case 'wz_':
		if ($('#' + id).val() == 5) {
			$("#well_rumah_zone").find("div").removeClass("hidden");
			$("#well_zone_1").removeClass("hidden");
			$("#well_zone_2").removeClass("hidden");
			$("#well_zone_3").removeClass("hidden");
			$("#well_zone_4").removeClass("hidden");
			$("#well_zone_5").removeClass("hidden");
			
			$("#li_well_zone_1").addClass("active");
			$("#li_well_zone_2").removeClass("active");
			$("#li_well_zone_3").removeClass("active");
			$("#li_well_zone_4").removeClass("active");
			$("#li_well_zone_5").removeClass("active");
			
			$("#well_tab_z1").removeClass("hidden");
			$("#well_tab_z2").removeClass("hidden");
			$("#well_tab_z3").removeClass("hidden");
			$("#well_tab_z4").removeClass("hidden");
			$("#well_tab_z5").removeClass("hidden");
			
			$("#well_tab_z1").addClass("active");
			$("#well_tab_z2").removeClass("active");
			$("#well_tab_z3").removeClass("active");
			$("#well_tab_z4").removeClass("active");
			$("#well_tab_z5").removeClass("active");
		} else if ($('#' + id).val() == 4) {
			$("#well_rumah_zone").find("div").removeClass("hidden");
			$("#well_zone_1").removeClass("hidden");
			$("#well_zone_2").removeClass("hidden");
			$("#well_zone_3").removeClass("hidden");
			$("#well_zone_4").removeClass("hidden");
			$("#well_zone_5").addClass("hidden");
			
			$("#li_well_zone_1").addClass("active");
			$("#li_well_zone_2").removeClass("active");
			$("#li_well_zone_3").removeClass("active");
			$("#li_well_zone_4").removeClass("active");
			$("#li_well_zone_5").removeClass("active");
			
			$("#well_tab_z1").removeClass("hidden");
			$("#well_tab_z2").removeClass("hidden");
			$("#well_tab_z3").removeClass("hidden");
			$("#well_tab_z4").removeClass("hidden");
			$("#well_tab_z5").addClass("hidden");
			
			$("#well_tab_z1").addClass("active");
			$("#well_tab_z2").removeClass("active");
			$("#well_tab_z3").removeClass("active");
			$("#well_tab_z4").removeClass("active");
			$("#well_tab_z5").removeClass("active");
		} else if ($('#' + id).val() == 3) {
			$("#well_rumah_zone").find("div").removeClass("hidden");
			$("#well_zone_1").removeClass("hidden");
			$("#well_zone_2").removeClass("hidden");
			$("#well_zone_3").removeClass("hidden");
			$("#well_zone_4").addClass("hidden");
			$("#well_zone_5").addClass("hidden");
			
			$("#li_well_zone_1").addClass("active");
			$("#li_well_zone_2").removeClass("active");
			$("#li_well_zone_3").removeClass("active");
			$("#li_well_zone_4").removeClass("active");
			$("#li_well_zone_5").removeClass("active");
			
			$("#well_tab_z1").removeClass("hidden");
			$("#well_tab_z2").removeClass("hidden");
			$("#well_tab_z3").removeClass("hidden");
			$("#well_tab_z4").addClass("hidden");
			$("#well_tab_z5").addClass("hidden");
			
			$("#well_tab_z1").addClass("active");
			$("#well_tab_z2").removeClass("active");
			$("#well_tab_z3").removeClass("active");
			$("#well_tab_z4").removeClass("active");
			$("#well_tab_z5").removeClass("active");
		} else if ($('#' + id).val() == "") {
			$("#well_rumah_zone").find("div").addClass("hidden");
			$("#well_zone_1").addClass("hidden");
			$("#well_zone_2").addClass("hidden");
			$("#well_zone_3").addClass("hidden");
			$("#well_zone_4").addClass("hidden");
			$("#well_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well_rumah_zone").find("div").removeClass("hidden");
			$("#well_zone_1").removeClass("hidden");
			$("#well_zone_2").removeClass("hidden");
			$("#well_zone_3").addClass("hidden");
			$("#well_zone_4").addClass("hidden");
			$("#well_zone_5").addClass("hidden");
			
			$("#li_well_zone_1").addClass("active");
			$("#li_well_zone_2").removeClass("active");
			$("#li_well_zone_3").removeClass("active");
			$("#li_well_zone_4").removeClass("active");
			$("#li_well_zone_5").removeClass("active");
			
			$("#well_tab_z1").removeClass("hidden");
			$("#well_tab_z2").removeClass("hidden");
			$("#well_tab_z3").addClass("hidden");
			$("#well_tab_z4").addClass("hidden");
			$("#well_tab_z5").addClass("hidden");
			
			$("#well_tab_z1").addClass("active");
			$("#well_tab_z2").removeClass("active");
			$("#well_tab_z3").removeClass("active");
			$("#well_tab_z4").removeClass("active");
			$("#well_tab_z5").removeClass("active");
		} else if ($('#' + id).val() == 1) {
			$("#well_rumah_zone").find("div").removeClass("hidden");
			$("#well_zone_1").removeClass("hidden");
			$("#well_zone_2").addClass("hidden");
			$("#well_zone_3").addClass("hidden");
			$("#well_zone_4").addClass("hidden");
			$("#well_zone_5").addClass("hidden");
			
			$("#li_well_zone_1").addClass("active");
			$("#li_well_zone_2").removeClass("active");
			$("#li_well_zone_3").removeClass("active");
			$("#li_well_zone_4").removeClass("active");
			$("#li_well_zone_5").removeClass("active");
			
			$("#well_tab_z1").removeClass("hidden");
			$("#well_tab_z2").addClass("hidden");
			$("#well_tab_z3").addClass("hidden");
			$("#well_tab_z4").addClass("hidden");
			$("#well_tab_z5").addClass("hidden");
			
			$("#well_tab_z1").addClass("active");
			$("#well_tab_z2").removeClass("active");
			$("#well_tab_z3").removeClass("active");
			$("#well_tab_z4").removeClass("active");
			$("#well_tab_z5").removeClass("active");
		}
		break;
	// ===== Zone for Well 1 =====//
	case 'wz_1':
		if ($('#' + id).val() == 5) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").removeClass("hidden");
			$("#well1_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").removeClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well1_rumah_zone").find("div").addClass("hidden");
			$("#well1_zone_1").addClass("hidden");
			$("#well1_zone_2").addClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").addClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		}
		break;
	// ===== Zone for Well 2 =====//
	case 'wz_2':
		if ($('#' + id).val() == 5) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").removeClass("hidden");
			$("#well2_zone_4").removeClass("hidden");
			$("#well2_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").removeClass("hidden");
			$("#well2_zone_4").removeClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").removeClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well2_zone_1").addClass("hidden");
			$("#well2_rumah_zone").find("div").addClass("hidden");
			$("#well2_zone_2").addClass("hidden");
			$("#well2_zone_3").addClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").addClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").addClass("hidden");
			$("#well2_zone_3").addClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		}
		break;
	// ===== Zone for Well 3 =====//
	case 'wz_3':
		if ($('#' + id).val() == 5) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").removeClass("hidden");
			$("#well3_zone_4").removeClass("hidden");
			$("#well3_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").removeClass("hidden");
			$("#well3_zone_4").removeClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").removeClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well3_zone_1").addClass("hidden");
			$("#well3_rumah_zone").find("div").addClass("hidden");
			$("#well3_zone_2").addClass("hidden");
			$("#well3_zone_3").addClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").addClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").addClass("hidden");
			$("#well3_zone_3").addClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		}
		break;
	// ===== Zone for Well 4 =====//
	case 'wz_4':
		if ($('#' + id).val() == 5) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").removeClass("hidden");
			$("#well4_zone_4").removeClass("hidden");
			$("#well4_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").removeClass("hidden");
			$("#well4_zone_4").removeClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").removeClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well4_zone_1").addClass("hidden");
			$("#well4_rumah_zone").find("div").addClass("hidden");
			$("#well4_zone_2").addClass("hidden");
			$("#well4_zone_3").addClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").addClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").addClass("hidden");
			$("#well4_zone_3").addClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		}
		break;
		
	case 'total_wz_1':
		if ($('#' + id).val() == 5) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").removeClass("hidden");
			$("#well1_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").removeClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well1_rumah_zone").find("div").addClass("hidden");
			$("#well1_zone_1").addClass("hidden");
			$("#well1_zone_2").addClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").addClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		}
		break;
		
	case 'total_wz_2':
		if ($('#' + id).val() == 5) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").removeClass("hidden");
			$("#well2_zone_4").removeClass("hidden");
			$("#well2_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").removeClass("hidden");
			$("#well2_zone_4").removeClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").removeClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well2_zone_1").addClass("hidden");
			$("#well2_rumah_zone").find("div").addClass("hidden");
			$("#well2_zone_2").addClass("hidden");
			$("#well2_zone_3").addClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").removeClass("hidden");
			$("#well2_zone_3").addClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well2_rumah_zone").find("div").removeClass("hidden");
			$("#well2_zone_1").removeClass("hidden");
			$("#well2_zone_2").addClass("hidden");
			$("#well2_zone_3").addClass("hidden");
			$("#well2_zone_4").addClass("hidden");
			$("#well2_zone_5").addClass("hidden");
		}
		break;
		
	case 'total_wz_3':
		if ($('#' + id).val() == 5) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").removeClass("hidden");
			$("#well3_zone_4").removeClass("hidden");
			$("#well3_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").removeClass("hidden");
			$("#well3_zone_4").removeClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").removeClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well3_zone_1").addClass("hidden");
			$("#well3_rumah_zone").find("div").addClass("hidden");
			$("#well3_zone_2").addClass("hidden");
			$("#well3_zone_3").addClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").removeClass("hidden");
			$("#well3_zone_3").addClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well3_rumah_zone").find("div").removeClass("hidden");
			$("#well3_zone_1").removeClass("hidden");
			$("#well3_zone_2").addClass("hidden");
			$("#well3_zone_3").addClass("hidden");
			$("#well3_zone_4").addClass("hidden");
			$("#well3_zone_5").addClass("hidden");
		}
		break;
		
	case 'total_wz_4':
		if ($('#' + id).val() == 5) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").removeClass("hidden");
			$("#well4_zone_4").removeClass("hidden");
			$("#well4_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").removeClass("hidden");
			$("#well4_zone_4").removeClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").removeClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well4_zone_1").addClass("hidden");
			$("#well4_rumah_zone").find("div").addClass("hidden");
			$("#well4_zone_2").addClass("hidden");
			$("#well4_zone_3").addClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").removeClass("hidden");
			$("#well4_zone_3").addClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well4_rumah_zone").find("div").removeClass("hidden");
			$("#well4_zone_1").removeClass("hidden");
			$("#well4_zone_2").addClass("hidden");
			$("#well4_zone_3").addClass("hidden");
			$("#well4_zone_4").addClass("hidden");
			$("#well4_zone_5").addClass("hidden");
		}
		break;
		
	case 'total_wz_5':
		if ($('#' + id).val() == 5) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").removeClass("hidden");
			$("#well1_zone_5").removeClass("hidden");
		} else if ($('#' + id).val() == 4) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").removeClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 3) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").removeClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == "") {
			$("#well1_rumah_zone").find("div").addClass("hidden");
			$("#well1_zone_1").addClass("hidden");
			$("#well1_zone_2").addClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 2) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").removeClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		} else if ($('#' + id).val() == 1) {
			$("#well1_rumah_zone").find("div").removeClass("hidden");
			$("#well1_zone_1").removeClass("hidden");
			$("#well1_zone_2").addClass("hidden");
			$("#well1_zone_3").addClass("hidden");
			$("#well1_zone_4").addClass("hidden");
			$("#well1_zone_5").addClass("hidden");
		}
		break;

	}
}

// ======================= PLAY FUNCTION CHANGE =======================//
$("#oos_1").on("change", function(e) {
	disabledElement("oos_1");
})
$("#oos_4").on("change", function(e) {
	disabledElement("oos_4");
})
$("#oos_6").on("change", function(e) {
	disabledElement("oos_6");
})
$("#oos_9").on("change", function(e) {
	disabledElement("oos_9");
})
$("#pgcfsrock").on("change", function(e) {
	disabledElement("pgcfsrock");
})
$("#pgcfres").on("change", function(e) {
	disabledElement("pgcfres");
})
$("#pgcftrap").on("change", function(e) {
	disabledElement("pgcftrap");
})
$("#pgcfdyn").on("change", function(e) {
	disabledElement("pgcfdyn");
})
$("#pgcfres8").on("change", function(e) {
	disabledElement("pgcfres8");
})
// ====== LEAD, DRILLABLE, POST DRILL, DISCOVERY FUNCTION CHANGE =======//
$("#gcfsrock").on("change", function(e) {
	disabledElement("gcfsrock");
})
$("#gcfres").on("change", function(e) {
	disabledElement("gcfres");
})
$("#gcftrap").on("change", function(e) {
	disabledElement("gcftrap");
})
$("#gcfdyn").on("change", function(e) {
	disabledElement("gcfdyn");
})
$("#gcfres8").on("change", function(e) {
	disabledElement("gcfres8");
})
$("#w").on("change", function(e) {
	disabledElement("w");
})
$("#waw").on("change", function(e) {
	disabledElement("waw");
})
$("#wz_").on("change", function(e) {
	disabledElement("wz_");
})
$("#wz_1").on("change", function(e) {
	disabledElement("wz_1");
})
$("#wz_2").on("change", function(e) {
	disabledElement("wz_2");
})
$("#wz_3").on("change", function(e) {
	disabledElement("wz_3");
})
$("#wz_4").on("change", function(e) {
	disabledElement("wz_4");
})
$("#total_wz_1").on("change", function(e) {
	disabledElement("total_wz_1");
})
$("#total_wz_2").on("change", function(e) {
	disabledElement("total_wz_2");
})
$("#total_wz_3").on("change", function(e) {
	disabledElement("total_wz_3");
})
$("#total_wz_4").on("change", function(e) {
	disabledElement("total_wz_4");
})
$("#total_wz_5").on("change", function(e) {
	disabledElement("total_wz_5");
})

bikin_sekaligus(status_id, status_data);
bikin_sekaligus(adjacent_activity_id, adjacent_activity_data);
bikin_sekaligus(well_result_postdrill_id, well_result_postdrill_data);
bikin_sekaligus(well_result_discovery_id, well_result_discovery_data);
bikin_sekaligus(prospect_type_id, prospect_type_data);
bikin_sekaligus(map_id, map_data);
bikin_sekaligus(gcf_id, gcf_data);
bikin_sekaligus(environment_id, environment_data);
bikin_sekaligus(survey_availability_id, survey_availability_data);
bikin_sekaligus(_id, _data);

// ======================= DATEPICKER =======================//
$("#l5").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#dl7").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#d7").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#d9").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#1dw8").datepicker({
	changeMonth : true,
	changeYear : true
});

// ===== datepicker : discovery =====//
$("#ds7").datepicker({
	changeMonth : true,
	changeYear : true
});
$("#ds9").datepicker({
	changeMonth : true,
	changeYear : true
});
// ===== discovery : well =====//
$("#dsw8_").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#dsw8_1").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#dsw8_2").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#dsw8_3").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#dsw8_4").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
// ===== discovery : zone =====//
$("#z_1").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_2").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_3").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_4").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_5").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_11").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_12").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_13").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_14").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_15").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_21").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_22").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_23").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_24").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_25").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_31").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_32").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_33").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_34").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_35").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_41").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_42").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_43").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_44").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_45").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_51").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_52").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_53").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_54").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z_55").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
// ===== zone : fluid property =====//
$("#z2_1").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_2").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_3").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_4").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_5").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_11").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_12").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_13").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_14").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_15").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_21").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_22").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_23").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_24").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_25").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_31").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_32").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_33").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_34").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_35").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_41").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_42").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_43").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_44").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_45").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_51").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_52").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_53").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_54").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});
$("#z2_55").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});

//profile date
$("#date_sign").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});

$("#date_end").datepicker({
	dateFormat : 'yy-mm-dd',
	changeMonth : true,
	changeYear : true
});

function toggleStatus(cekbok, huruf, kolapse) {
	if ($('#' + cekbok).is(':checked')) {
		$("#DsGfs").prop("checked", true);
		$("#" + huruf).css("color", "#008000");
		$('#' + kolapse + ' :input').removeAttr('disabled');
		document.getElementById(kolapse).className = document
				.getElementById(kolapse).className.replace(/\bin\b/, '');
	} else {
		$("#" + kolapse).css("height", "0px");
		$("#" + huruf).css("color", "gray");
		$("#" + kolapse).find("input,select,textarea,button").prop("disabled",
				true);
	}
}

// ========= accordion Lead =========//
$("#LGfs_link").click(function() {
	if ($('#LGfs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#LDss_link").click(function() {
	if ($('#LDss').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#LGras_link").click(function() {
	if ($('#LGras').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#LGeos_link").click(function() {
	if ($('#LGeos').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#LEs_link").click(function() {
	if ($('#LEs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#LRs_link").click(function() {
	if ($('#LRs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#LOs_link").click(function() {
	if ($('#LOs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
// ========= accordion Drillable =========//
$("#DlGfs_link").click(function() {
	if ($('#DlGfs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlDss2_link").click(function() {
	if ($('#DlDss2').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlGras_link").click(function() {
	if ($('#DlGras').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlGeos_link").click(function() {
	if ($('#DlGeos').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlEs_link").click(function() {
	if ($('#DlEs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlRs_link").click(function() {
	if ($('#DlRs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlOs_link").click(function() {
	if ($('#DlOs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DlDss3_link").click(function() {
	if ($('#DlDss3').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
// ========= accordion Drill =========//
$("#DGfs_link").click(function() {
	if ($('#DGfs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DDss2_link").click(function() {
	if ($('#DDss2').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DGras_link").click(function() {
	if ($('#DGras').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DGeos_link").click(function() {
	if ($('#DGeos').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DEs_link").click(function() {
	if ($('#DEs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DRs_link").click(function() {
	if ($('#DRs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DOs_link").click(function() {
	if ($('#DOs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DDss3_link").click(function() {
	if ($('#DDss3').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
// ========= accordion Discovery =========//
$("#DsGfs_link").click(function() {
	if ($('#DsGfs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsDss2_link").click(function() {
	if ($('#DsDss2').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsGras_link").click(function() {
	if ($('#DsGras').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsGeos_link").click(function() {
	if ($('#DsGeos').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsEs_link").click(function() {
	if ($('#DsEs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsRs_link").click(function() {
	if ($('#DsRs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsOs_link").click(function() {
	if ($('#DsOs').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});
$("#DsDss3_link").click(function() {
	if ($('#DsDss3').is(':checked')) {
		return true;
	} else {
		alert("Please Click Checkbox First!");
		return false
	}
});

disabledElement("pgcfsrock");
disabledElement("pgcfres");
disabledElement("pgcftrap");
disabledElement("pgcfdyn");
disabledElement("pgcfres8");
disabledElement("gcfsrock");
disabledElement("gcfres");
disabledElement("gcftrap");
disabledElement("gcfdyn");
disabledElement("gcfres8");
disabledElement("w");
disabledElement("oos_1");
disabledElement("oos_4");
disabledElement("oos_6");
disabledElement("oos_9");

$("#well4_zone_1").addClass("hidden");
$("#well4_rumah_zone").find("div").addClass("hidden");

$("#well3_zone_1").addClass("hidden");
$("#well3_rumah_zone").find("div").addClass("hidden");

$("#well2_zone_1").addClass("hidden");
$("#well2_rumah_zone").find("div").addClass("hidden");

$("#zone_1").addClass("hidden");
$("#rumah_zone").find("div").addClass("hidden");

$('#notice_play')
		.click(
				function() {
					$.gritter
							.add({
								title : 'Sorry!',
								text : 'You cant change this data, because it is past the specified date.'
							});
					return false;
				});

function tooltip_stat(id_nya, title, kata_kata) {

	$('#' + id_nya).click(function() {
		$.gritter.add({
			title : title,
			text : kata_kata
		});
		return false;
	});

};

$('.number-only').keydown(function(event) {
	if ($.inArray(event.keyCode, [ 46, 8, 9, 27, 13]) !== -1
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39)) {
		return;
	} else {
		if (event.shiftKey
				|| (event.keyCode < 48 || event.keyCode > 57)
				&& (event.keyCode < 96 || event.keyCode > 105)) {
			event.preventDefault();
		}
	}
});

$('.number').keydown(function(event) {
	if ($.inArray(event.keyCode, [ 46, 8, 9, 27, 13]) !== -1
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39)) {
		return;
	} else {
		if (event.shiftKey
				|| (event.keyCode < 48 || event.keyCode > 57)
				&& (event.keyCode < 96 || event.keyCode > 105) && (event.keyCode != 190)) {
			event.preventDefault();
		}
	}
});

$('.number-minus').keydown(function(event) {
	if ($.inArray(event.keyCode, [ 46, 8, 9, 27, 13]) !== -1
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39)) {
		return;
	} else {
		if (event.shiftKey
				|| (event.keyCode < 48 || event.keyCode > 57)
				&& (event.keyCode < 96 || event.keyCode > 105) && (event.keyCode != 190) && (event.keyCode != 189) && (event.keyCode != 173)) {
			event.preventDefault();
		}
	}
});

$('.number-comma').keydown(function(event) {
	if ($.inArray(event.keyCode, [ 46, 8, 9, 27, 13]) !== -1
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39)) {
		return;
	} else {
		if (event.shiftKey
				|| (event.keyCode < 48 || event.keyCode > 57)
				&& (event.keyCode < 96 || event.keyCode > 105) && (event.keyCode != 188)) {
			event.preventDefault();
		}
	}
});

$('.no-comma').keydown(function(event) {
	if ($.inArray(event.keyCode, [ 46, 8, 9, 27, 13]) !== -1
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39)) {
		return;
	} else {
		if (event.shiftKey
				|| (event.keyCode == 188)) {
			event.preventDefault();
		}
	}
});

$('.number-colon').keydown(function(event) {
	if ($.inArray(event.keyCode, [ 46, 8, 9, 27, 13]) !== -1
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39) || (event.shiftKey && (event.keyCode == 186)) || (event.shiftKey && (event.keyCode == 59))) {
		return;
	} else {
		if (event.shiftKey
				|| (event.keyCode < 48 || event.keyCode > 57)
				&& (event.keyCode < 96 || event.keyCode > 105)) {
			event.preventDefault();
		}
	}
});

$('.directionlat').keydown(function(e) {
    if(e.which == 83 /* S */ || e.which == 78 /* N */ || e.which == 08) {
    } else {
    	e.preventDefault();
    }
});

$('.directionlong').keydown(function(e) {
    if(e.which == 69 /* E */ || e.which == 87 /* W */ || e.which == 08) {
    } else {
    	e.preventDefault();
    }
});

$('.disable-input').keydown(false);

$('#play_form input').blur(function() {
	if (!$(this).val()) {
		$(this).parents('p').addClass('warning');
	}
});

$("play_form").each(function() {
	$(this).filter(':input') // <-- Should return all input elements in that
								// specific form.
});

function clearDate(id)
{
	atribut = id.substr(5);
	$('#' + atribut).val('');
}

function IsEmpty() {
	if (document.forms['frm'].question.value == "") {
		alert("empty");
		return false;
	}
	return true;
}

function submit() {
	document[form].submit();
}