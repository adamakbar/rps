<?php

// change the following paths if necessary
$root   = dirname(__FILE__);
$yii    = $root . '/framework/yii.php';
$config = $root . '/protected/config/main.php';

// Implement poorEnv
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once $root . '/protected/components/helpers.php';
require_once $root . '/protected/vendor/PHPExcel/PHPExcel.php';
require_once $root . '/protected/vendor/kint/Kint.class.php';
// require_once $root . '/protected/vendor/autoload.php'; // A sad composer usage

require_once $yii;
Yii::createWebApplication($config)->run();
