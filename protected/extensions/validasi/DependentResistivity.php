<?php
	class DependentResistivity extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'rst_vol_p10_area')
			{
				if($object['attributes']['rst_vol_p10_area'] != '')
				{
					if($object['attributes']['rst_vol_p10_area'] == 0 || $object['attributes']['rst_vol_p10_area'] <= $object['attributes']['rst_vol_p50_area'] || $object['attributes']['rst_vol_p10_area'] <= $object['attributes']['rst_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Resistivity P50 Areal Closure Estimation and Resistivity P90 Areal Closure Estimation');
				}
			}
		
			if($attribute == 'rst_vol_p50_area')
			{
				if($object['attributes']['rst_vol_p50_area'] != '')
				{
					if($object['attributes']['rst_vol_p50_area'] == 0 || $object['attributes']['rst_vol_p50_area'] <= $object['attributes']['rst_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Resistivity P90 Areal Closure Estimation');
				}
			}
		}
	}
?>