<?php
	class DependentAreaGravity extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'lsgv_high_estimate')
			{
				if($object['attributes']['lsgv_high_estimate'] != '')
				{
					if($object['attributes']['lsgv_high_estimate'] == 0 || $object['attributes']['lsgv_high_estimate'] <= $object['attributes']['lsgv_best_estimate'] || $object['attributes']['lsgv_high_estimate'] <= $object['attributes']['lsgv_low_estimate'])
						$this->addError($object, $attribute, '{attribute} must greater than Gravity Best Estimate and Gravity Low Estimate');
				}
			}
				
			if($attribute == 'lsgv_best_estimate')
			{
				if($object['attributes']['lsgv_best_estimate'] != '')
				{
					if($object['attributes']['lsgv_best_estimate'] == 0 || $object['attributes']['lsgv_best_estimate'] <= $object['attributes']['lsgv_low_estimate'])
						$this->addError($object, $attribute, '{attribute} must greater than Gravity Low Estimate');
				}
			}
		}
	}
?>