<?php
 class DependentAreaGeochemistry extends CValidator
 {
 	protected function validateAttribute($object, $attribute)
 	{
 		$value = $object->$attribute;
 		if($attribute == 'lsgc_high_estimate')
 		{
 			if($object['attributes']['lsgc_high_estimate'] != '')
 			{
 				if($object['attributes']['lsgc_high_estimate'] == 0 || $object['attributes']['lsgc_high_estimate'] <= $object['attributes']['lsgc_best_estimate'] || $object['attributes']['lsgc_high_estimate'] <= $object['attributes']['lsgc_low_estimate'])
 					$this->addError($object, $attribute, '{attribute} must greater than Geochemistry Best Estimate and Geochemistry Low Estimate');
 			}
 		}
 			
 		if($attribute == 'lsgc_best_estimate')
 		{
 			if($object['attributes']['lsgc_best_estimate'] != '')
 			{
 				if($object['attributes']['lsgc_best_estimate'] == 0 || $object['attributes']['lsgc_best_estimate'] <= $object['attributes']['lsgc_low_estimate'])
 					$this->addError($object, $attribute, '{attribute} must greater than Geochemistry Low Estimate');
 			}
 		}
 	}
 }
?>