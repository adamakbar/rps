<?php
class DependentAreaOther extends CValidator
{
	protected function validateAttribute($object, $attribute)
	{
		$value = $object->$attribute;
		if($attribute == 'lsor_high_estimate')
		{
			if($object['attributes']['lsor_high_estimate'] != '')
			{
				if($object['attributes']['lsor_high_estimate'] == 0 || $object['attributes']['lsor_high_estimate'] <= $object['attributes']['lsor_best_estimate'] || $object['attributes']['lsor_high_estimate'] <= $object['attributes']['lsor_low_estimate'])
					$this->addError($object, $attribute, '{attribute} must greater than Other Best Estimate and Other Low Estimate');
			}
		}
			
		if($attribute == 'lsor_best_estimate')
		{
			if($object['attributes']['lsor_best_estimate'] != '')
			{
				if($object['attributes']['lsor_best_estimate'] == 0 || $object['attributes']['lsor_best_estimate'] <= $object['attributes']['lsor_low_estimate'])
					$this->addError($object, $attribute, '{attribute} must greater than Other Low Estimate');
			}
		}
	}
}
?>