<?php
	class DependentOther extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'sor_vol_p10_area')
			{
				if($object['attributes']['sor_vol_p10_area'] != '')
				{
					if($object['attributes']['sor_vol_p10_area'] == 0 || $object['attributes']['sor_vol_p10_area'] <= $object['attributes']['sor_vol_p50_area'] || $object['attributes']['sor_vol_p10_area'] <= $object['attributes']['sor_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Other P50 Areal Closure Estimation and Other P90 Areal Closure Estimation');
				}
			}
		
			if($attribute == 'sor_vol_p50_area')
			{
				if($object['attributes']['sor_vol_p50_area'] != '')
				{
					if($object['attributes']['sor_vol_p50_area'] == 0 || $object['attributes']['sor_vol_p50_area'] <= $object['attributes']['sor_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Other P90 Areal Closure Estimation');
				}
			}
		}	
	}
?>