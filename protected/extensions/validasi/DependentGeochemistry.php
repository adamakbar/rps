<?php
	class DependentGeochemistry extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'sgc_vol_p10_area')
			{
				if($object['attributes']['sgc_vol_p10_area'] != '')
				{
					if($object['attributes']['sgc_vol_p10_area'] == 0 || $object['attributes']['sgc_vol_p10_area'] <= $object['attributes']['sgc_vol_p50_area'] || $object['attributes']['sgc_vol_p10_area'] <= $object['attributes']['sgc_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Geochemistry P50 Areal Closure Estimation and Geochemistry P90 Areal Closure Estimation');
				}
			}
		
			if($attribute == 'sgc_vol_p50_area')
			{
				if($object['attributes']['sgc_vol_p50_area'] != '')
				{
					if($object['attributes']['sgc_vol_p50_area'] == 0 || $object['attributes']['sgc_vol_p50_area'] <= $object['attributes']['sgc_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Geochemistry P90 Areal Closure Estimation');
				}
			}
		}
	}
?>