<?php
	class FluidZPattern extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$pattern1 = '/^\d+(\.\d)+$/';
			$pattern2 = '/^([1-9]{0,1})([0-9]{1})(\.[0-9])?$/';
			
			if(preg_match($pattern1, $object['attributes']['zone_fluid_z']))
			{
				if(!preg_match($pattern2, $object['attributes']['zone_fluid_z']))
					$this->addError($object, $attribute, '{attribute} invalid value');
			} else {
				$this->addError($object, $attribute, '{attribute} invalid value');
			}
		}
	}
?>