<?php
	class DependentArea2d extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'ls2d_hight_estimate')
			{
				if($object['attributes']['ls2d_hight_estimate'] != '')
				{
					if($object['attributes']['ls2d_hight_estimate'] == 0 || $object['attributes']['ls2d_hight_estimate'] <= $object['attributes']['ls2d_best_estimate'] || $object['attributes']['ls2d_hight_estimate'] <= $object['attributes']['ls2d_low_estimate'])
						$this->addError($object, $attribute, '{attribute} must greater than 2D Seismic Best Estimate and 2D Seismic Low Estimate');
				}
			}
				
			if($attribute == 'ls2d_best_estimate')
			{
				if($object['attributes']['ls2d_best_estimate'] != '')
				{
					if($object['attributes']['ls2d_best_estimate'] == 0 || $object['attributes']['ls2d_best_estimate'] <= $object['attributes']['ls2d_low_estimate'])
						$this->addError($object, $attribute, '{attribute} must greater than 2D Seismic Low Estimate');
				}
			}
		}
	}
?>