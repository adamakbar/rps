<?php
class DependentAreaResistivity extends CValidator
{
	protected function validateAttribute($object, $attribute)
	{
		$value = $object->$attribute;
		if($attribute == 'lsrt_high_estimate')
		{
			if($object['attributes']['lsrt_high_estimate'] != '')
			{
				if($object['attributes']['lsrt_high_estimate'] == 0 || $object['attributes']['lsrt_high_estimate'] <= $object['attributes']['lsrt_best_estimate'] || $object['attributes']['lsrt_high_estimate'] <= $object['attributes']['lsrt_low_estimate'])
					$this->addError($object, $attribute, '{attribute} must greater than Resistivity Best Estimate and Resistivity Low Estimate');
			}
		}
			
		if($attribute == 'lsrt_best_estimate')
		{
			if($object['attributes']['lsrt_best_estimate'] != '')
			{
				if($object['attributes']['lsrt_best_estimate'] == 0 || $object['attributes']['lsrt_best_estimate'] <= $object['attributes']['lsrt_low_estimate'])
					$this->addError($object, $attribute, '{attribute} must greater than Resistivity Low Estimate');
			}
		}
	}
}
?>