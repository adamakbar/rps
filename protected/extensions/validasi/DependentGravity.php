<?php
	class DependentGravity extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'sgv_vol_p10_area')
			{
				if($object['attributes']['sgv_vol_p10_area'] != '')
				{
					if($object['attributes']['sgv_vol_p10_area'] == 0 || $object['attributes']['sgv_vol_p10_area'] <= $object['attributes']['sgv_vol_p50_area'] || $object['attributes']['sgv_vol_p10_area'] <= $object['attributes']['sgv_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Gravity P50 Areal Closure Estimation and Gravity P90 Areal Closure Estimation');
				}
			}
		
			if($attribute == 'sgv_vol_p50_area')
			{
				if($object['attributes']['sgv_vol_p50_area'] != '')
				{
					if($object['attributes']['sgv_vol_p50_area'] == 0 || $object['attributes']['sgv_vol_p50_area'] <= $object['attributes']['sgv_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Gravity P90 Areal Closure Estimation');
				}
			}
		}	
	}
?>