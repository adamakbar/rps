<?php
	class DependentAreaGeological extends CValidator 
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'lsgf_high_estimate')
			{
				if($object['attributes']['lsgf_high_estimate'] != '') 
				{
					if($object['attributes']['lsgf_high_estimate'] == 0 || $object['attributes']['lsgf_high_estimate'] <= $object['attributes']['lsgf_best_estimate'] || $object['attributes']['lsgf_high_estimate'] <= $object['attributes']['lsgf_low_estimate'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological Best Estimate and Geological Low Estimate');
				}
			}
			
			if($attribute == 'lsgf_best_estimate')
			{
				if($object['attributes']['lsgf_best_estimate'] != '') 
				{
					if($object['attributes']['lsgf_best_estimate'] == 0 || $object['attributes']['lsgf_best_estimate'] <= $object['attributes']['lsgf_low_estimate'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological Low Estimate');
				}
			}
		}
	}
?>