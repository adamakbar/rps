<?php
	class DependentElectromagnetic extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'sel_vol_p10_area')
			{
				if($object['attributes']['sel_vol_p10_area'] != '')
				{
					if($object['attributes']['sel_vol_p10_area'] == 0 || $object['attributes']['sel_vol_p10_area'] <= $object['attributes']['sel_vol_p50_area'] || $object['attributes']['sel_vol_p10_area'] <= $object['attributes']['sel_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Electromagnetic P50 Areal Closure Estimation and Electromagnetic P90 Areal Closure Estimation');
				}
			}
		
			if($attribute == 'sel_vol_p50_area')
			{
				if($object['attributes']['sel_vol_p50_area'] != '')
				{
					if($object['attributes']['sel_vol_p50_area'] == 0 || $object['attributes']['sel_vol_p50_area'] <= $object['attributes']['sel_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Electromagnetic P90 Areal Closure Estimation');
				}
			}
		}
	}
?>