<?php
	class DependentGeological extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 'sgf_p10_area')
			{
				if($object['attributes']['sgf_p10_area'] != '')
				{
					if($object['attributes']['sgf_p10_area'] == 0 || $object['attributes']['sgf_p10_area'] <= $object['attributes']['sgf_p50_area'] || $object['attributes']['sgf_p10_area'] <= $object['attributes']['sgf_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological P50 Areal Closure Estimation and Geological P90 Areal Closure Estimation');
				}
			}
				
			if($attribute == 'sgf_p50_area')
			{
				if($object['attributes']['sgf_p50_area'] != '')
				{
					if($object['attributes']['sgf_p50_area'] == 0 || $object['attributes']['sgf_p50_area'] <= $object['attributes']['sgf_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological P90 Areal Closure Estimation');
				}
			}
			
			if($attribute == 'sgf_p10_thickness')
			{
				if($object['attributes']['sgf_p10_thickness'] != '')
				{
					if($object['attributes']['sgf_p10_thickness'] == 0 || $object['attributes']['sgf_p10_thickness'] <= $object['attributes']['sgf_p50_thickness'] || $object['attributes']['sgf_p10_thickness'] <= $object['attributes']['sgf_p90_thickness'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological P50 Gross Sand Thickness and Geological P90 Gross Sand Thickness');
				}
			}
				
			if($attribute == 'sgf_p50_thickness')
			{
				if($object['attributes']['sgf_p50_thickness'] != '')
				{
					if($object['attributes']['sgf_p50_thickness'] == 0 || $object['attributes']['sgf_p50_thickness'] <= $object['attributes']['sgf_p90_thickness'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological P90 Gross Sand Thickness');
				}
			}
			
			if($attribute == 'sgf_p10_net')
			{
				if($object['attributes']['sgf_p10_net'] != '')
				{
					if($object['attributes']['sgf_p10_net'] == 0 || $object['attributes']['sgf_p10_net'] <= $object['attributes']['sgf_p50_net'] || $object['attributes']['sgf_p10_net'] <= $object['attributes']['sgf_p90_net'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological P50 Net to Gross and Geological P90 Net to Gross');
				}
			}
				
			if($attribute == 'sgf_p50_net')
			{
				if($object['attributes']['sgf_p50_net'] != '')
				{
					if($object['attributes']['sgf_p50_net'] == 0 || $object['attributes']['sgf_p50_net'] <= $object['attributes']['sgf_p90_net'])
						$this->addError($object, $attribute, '{attribute} must greater than Geological P90 Net to Gross');
				}
			}
		}
	}
?>