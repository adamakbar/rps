<?php
	class Dependent3dSeismic extends CValidator
	{
		protected function validateAttribute($object, $attribute)
		{
			$value = $object->$attribute;
			if($attribute == 's3d_net_p10_thickness')
			{
				if($object['attributes']['s3d_net_p10_thickness'] != '')
				{
					if($object['attributes']['s3d_net_p10_thickness'] == 0 || $object['attributes']['s3d_net_p10_thickness'] <= $object['attributes']['s3d_net_p50_thickness'] || $object['attributes']['s3d_net_p10_thickness'] <= $object['attributes']['s3d_net_p90_thickness'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P50 Thickness Net Pay and 3d Seismic P90 Thickness Net Pay');
				}
			}
		
			if($attribute == 's3d_net_p50_thickness')
			{
				if($object['attributes']['s3d_net_p50_thickness'] != '')
				{
					if($object['attributes']['s3d_net_p50_thickness'] == 0 || $object['attributes']['s3d_net_p50_thickness'] <= $object['attributes']['s3d_net_p90_thickness'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P90 Thickness Net Pay');
				}
			}
			
			if($attribute == 's3d_net_p10_por')
			{
				if($object['attributes']['s3d_net_p10_por'] != '')
				{
					if($object['attributes']['s3d_net_p10_por'] == 0 || $object['attributes']['s3d_net_p10_por'] <= $object['attributes']['s3d_net_p50_por'] || $object['attributes']['s3d_net_p10_por'] <= $object['attributes']['s3d_net_p90_por'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P50 Porosity Cut Off and 3d Seismic P90 Porosity Cut Off');
				}
			}
			
			if($attribute == 's3d_net_p50_por')
			{
				if($object['attributes']['s3d_net_p50_por'] != '')
				{
					if($object['attributes']['s3d_net_p50_por'] == 0 || $object['attributes']['s3d_net_p50_por'] <= $object['attributes']['s3d_net_p90_por'])
						$this->addError($object, $attribute, '{attribute} must greater 3d Seismic P90 Porosity Cut Off');
				}
			}
			
			if($attribute == 's3d_net_p10_vsh')
			{
				if($object['attributes']['s3d_net_p10_vsh'] != '')
				{
					if($object['attributes']['s3d_net_p10_vsh'] == 0 || $object['attributes']['s3d_net_p10_vsh'] <= $object['attributes']['s3d_net_p50_vsh'] || $object['attributes']['s3d_net_p10_vsh'] <= $object['attributes']['s3d_net_p90_vsh'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P50 Vsh Cut Off and 3d Seismic P90 Vsh Cut Off');
				}
			}
			
			if($attribute == 's3d_net_p50_vsh')
			{
				if($object['attributes']['s3d_net_p50_vsh'] != '')
				{
					if($object['attributes']['s3d_net_p50_vsh'] == 0 || $object['attributes']['s3d_net_p50_vsh'] <= $object['attributes']['s3d_net_p90_vsh'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P90 Vsh Cut Off');
				}
			}
			
			if($attribute == 's3d_net_p10_satur')
			{
				if($object['attributes']['s3d_net_p10_satur'] != '')
				{
					if($object['attributes']['s3d_net_p10_satur'] == 0 || $object['attributes']['s3d_net_p10_satur'] <= $object['attributes']['s3d_net_p50_satur'] || $object['attributes']['s3d_net_p10_satur'] <= $object['attributes']['s3d_net_p90_satur'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P50 Saturation Cut Off and 3d Seismic P90 Saturation Cut Off');
				}
			}
			
			if($attribute == 's3d_net_p50_satur')
			{
				if($object['attributes']['s3d_net_p50_satur'] != '')
				{
					if($object['attributes']['s3d_net_p50_satur'] == 0 || $object['attributes']['s3d_net_p50_satur'] <= $object['attributes']['s3d_net_p90_satur'])
						$this->addError($object, $attribute, '{attribute} must greater 3d Seismic P90 Saturation Cut Off');
				}
			}
			
			if($attribute == 's3d_vol_p10_area')
			{
				if($object['attributes']['s3d_vol_p10_area'] != '')
				{
					if($object['attributes']['s3d_vol_p10_area'] == 0 || $object['attributes']['s3d_vol_p10_area'] <= $object['attributes']['s3d_vol_p50_area'] || $object['attributes']['s3d_vol_p10_area'] <= $object['attributes']['s3d_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P50 Areal Closure Estimation and 3d Seismic P90 Areal Closure Estimation');
				}
			}
			
			if($attribute == 's3d_vol_p50_area')
			{
				if($object['attributes']['s3d_vol_p50_area'] != '')
				{
					if($object['attributes']['s3d_vol_p50_area'] == 0 || $object['attributes']['s3d_vol_p50_area'] <= $object['attributes']['s3d_vol_p90_area'])
						$this->addError($object, $attribute, '{attribute} must greater than 3d Seismic P90 Areal Closure Estimation');
				}
			}
		}
	}
?>