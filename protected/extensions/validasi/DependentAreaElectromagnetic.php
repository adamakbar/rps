<?php
class DependentAreaElectromagnetic extends CValidator
{
	protected function validateAttribute($object, $attribute)
	{
		$value = $object->$attribute;
		if($attribute == 'lsel_high_estimate')
		{
			if($object['attributes']['lsel_high_estimate'] != '')
			{
				if($object['attributes']['lsel_high_estimate'] == 0 || $object['attributes']['lsel_high_estimate'] <= $object['attributes']['lsel_best_estimate'] || $object['attributes']['lsel_high_estimate'] <= $object['attributes']['lsel_low_estimate'])
					$this->addError($object, $attribute, '{attribute} must greater than Electromagnetic Best Estimate and Electromagnetic Low Estimate');
			}
		}
			
		if($attribute == 'lsel_best_estimate')
		{
			if($object['attributes']['lsel_best_estimate'] != '')
			{
				if($object['attributes']['lsel_best_estimate'] == 0 || $object['attributes']['lsel_best_estimate'] <= $object['attributes']['lsel_low_estimate'])
					$this->addError($object, $attribute, '{attribute} must greater than Electromagnetic Low Estimate');
			}
		}
	}
}
?>