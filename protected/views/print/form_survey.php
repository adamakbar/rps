<hr>
<h3>Data Availability</h3>
<hr>

<div class="forth">
<h3>3D Seismic Survey</h3>
<?=oneField('Year survey', $data['s3d_year_survey'])?>
<?=oneField('Vintage number', $data['s3d_vintage_number'])?>
<?=oneField('Bin size', $data['s3d_bin_size'])?>
<?=oneField('Coverage area (acre)', $data['s3d_coverage_area'])?>
<?=oneField('Frequency', $data['s3d_frequency'])?>
<?=oneField('Frequency lateral', $data['s3d_frequency_lateral'])?>
<?=oneField('Frequency vertical', $data['s3d_frequency_vertical'])?>
<?=oneField('Latest processing year', $data['s3d_year_late_process'])?>
<?=oneField('Latest method', $data['s3d_late_method'])?>
<?=oneField('Image quality', $data['s3d_img_quality'])?>
<?=oneField('Top depth (feet)', $data['s3d_top_depth_ft'])?>
<?=oneField('Top depth (ms)', $data['s3d_top_depth_ms'])?>
<?=oneField('Bottom depth (feet)', $data['s3d_bot_depth_ft'])?>
<?=oneField('Bottom depth (ms)', $data['s3d_bot_depth_ms'])?>
<?=oneField('Depth contour spill point', $data['s3d_depth_spill'])?>
<?=oneField('Depth contour estimate OWC/GWC (feet)', $data['s3d_depth_estimate'])?>
<?=oneField('Depth contour lowest known oil/gas (feet)', $data['s3d_depth_low'])?>
<?=oneField('Formation thickness (feet)', $data['s3d_formation_thickness'])?>
<?=oneField('Gross sand or reservoir thickness (feet)', $data['s3d_gross_thickness'])?>
<?=oneField('Area closure estimation P90 (acre)', $data['s3d_vol_p90_area'])?>
<?=oneField('Area closure estimation P50 (acre)', $data['s3d_vol_p50_area'])?>
<?=oneField('Area closure estimation P10 (acre)', $data['s3d_vol_p10_area'])?>
<?=oneField('Net pay thickness P90 (feet)', $data['s3d_net_p90_thickness'])?>
<?=oneField('Net pay thickness P50 (feet)', $data['s3d_net_p50_thickness'])?>
<?=oneField('Net pay thickness P10 (feet)', $data['s3d_net_p10_thickness'])?>
<?=oneField('Porosity cut-off P90 (%)', $data['s3d_net_p90_por'])?>
<?=oneField('Porosity cut-off P50 (%)', $data['s3d_net_p50_por'])?>
<?=oneField('Porosity cut-off P10 (%)', $data['s3d_net_p10_por'])?>
<?=oneField('Saturation cut-off P90 (%)', $data['s3d_net_p90_satur'])?>
<?=oneField('Saturation cut-off P50 (%)', $data['s3d_net_p50_satur'])?>
<?=oneField('Saturation cut-off P10 (%)', $data['s3d_net_p10_satur'])?>
<?=oneField('Vshale cut-off P90 (%)', $data['s3d_net_p90_vsh'])?>
<?=oneField('Vshale cut-off P50 (%)', $data['s3d_net_p50_vsh'])?>
<?=oneField('Vshale cut-off P10 (%)', $data['s3d_net_p10_vsh'])?>
<?=oneField('Remark', $data['s3d_remark'])?>

<h3>2D Seismic Survey</h3>
<?=oneField('Year survey', $data['s2d_year_survey'])?>
<?=oneField('Vintage number', $data['s2d_vintage_number'])?>
<?=oneField('Total crossline', $data['s2d_total_crossline'])?>
<?=oneField('Seismic line', $data['s2d_seismic_line'])?>
<?=oneField('Average spacing parallel interval', $data['s2d_average_interval'])?>
<?=oneField('Record length (ms)', $data['s2d_record_length_ms'])?>
<?=oneField('Record length (feet)', $data['s2d_record_length_ft'])?>
<?=oneField('Latest processing year', $data['s2d_year_late_process'])?>
<?=oneField('Latest method', $data['s2d_late_method'])?>
<?=oneField('Image quality', $data['s2d_img_quality'])?>
<?=oneField('Top depth (feet)', $data['s2d_top_depth_ft'])?>
<?=oneField('Top depth (ms)', $data['s2d_top_depth_ms'])?>
<?=oneField('Bottom depth (feet)', $data['s2d_bot_depth_ft'])?>
<?=oneField('Bottom depth (ms)', $data['s2d_bot_depth_ms'])?>
<?=oneField('Depth contour spill point', $data['s2d_depth_spill'])?>
<?=oneField('Depth contour estimate OWC/GWC (feet)', $data['s2d_depth_estimate'])?>
<?=oneField('Depth contour lowest known oil/gas (feet)', $data['s2d_depth_low'])?>
<?=oneField('Formation thickness (feet)', $data['s2d_formation_thickness'])?>
<?=oneField('Gross sand or reservoir thickness (feet)', $data['s2d_gross_thickness'])?>
<?=oneField('Area closure estimation P90 (acre)', $data['s2d_vol_p90_area'])?>
<?=oneField('Area closure estimation P50 (acre)', $data['s2d_vol_p50_area'])?>
<?=oneField('Area closure estimation P10 (acre)', $data['s2d_vol_p10_area'])?>
<?=oneField('Net pay thickness P90 (feet)', $data['s2d_net_p90_thickness'])?>
<?=oneField('Net pay thickness P50 (feet)', $data['s2d_net_p50_thickness'])?>
<?=oneField('Net pay thickness P10 (feet)', $data['s2d_net_p10_thickness'])?>
<?=oneField('Porosity cut-off P90 (%)', $data['s2d_net_p90_por'])?>
<?=oneField('Porosity cut-off P50 (%)', $data['s2d_net_p50_por'])?>
<?=oneField('Porosity cut-off P10 (%)', $data['s2d_net_p10_por'])?>
<?=oneField('Saturation cut-off P90 (%)', $data['s2d_net_p90_satur'])?>
<?=oneField('Saturation cut-off P50 (%)', $data['s2d_net_p50_satur'])?>
<?=oneField('Saturation cut-off P10 (%)', $data['s2d_net_p10_satur'])?>
<?=oneField('Vshale cut-off P90 (%)', $data['s2d_net_p90_vsh'])?>
<?=oneField('Vshale cut-off P50 (%)', $data['s2d_net_p50_vsh'])?>
<?=oneField('Vshale cut-off P10 (%)', $data['s2d_net_p10_vsh'])?>
<?=oneField('Remark', $data['s2d_remark'])?>

<h3>Geological Field Survey</h3>
<?=oneField('Field year survey', $data['sgf_year_survey'])?>
<?=oneField('Field survey method', $data['sgf_survey_method'])?>
<?=oneField('Field total coverage area (acre)', $data['sgf_coverage_area'])?>
<?=oneField('Field areal closure estimation P90 (acre)', $data['sgf_p90_area'])?>
<?=oneField('Field areal closure estimation P50 (acre)', $data['sgf_p50_area'])?>
<?=oneField('Field areal closure estimation P10 (acre)', $data['sgf_p10_area'])?>
<?=oneField('Field gross sand thickness P90 (feet)', $data['sgf_p90_thickness'])?>
<?=oneField('Field gross sand thickness P50 (feet)', $data['sgf_p50_thickness'])?>
<?=oneField('Field gross sand thickness P10 (feet)', $data['sgf_p10_thickness'])?>
<?=oneField('Field net to Gross P90 (%)', $data['sgf_p90_net'])?>
<?=oneField('Field net to Gross P50 (%)', $data['sgf_p50_net'])?>
<?=oneField('Field net to Gross P10 (%)', $data['sgf_p10_net'])?>
<?=oneField('Field remark', $data['sgf_remark'])?>

<h3>Geochemistry Survey</h3>
<?=oneField('Year survey', $data['sgc_year_survey'])?>
<?=oneField('Sample interval', $data['sgc_sample_interval'])?>
<?=oneField('Total sample', $data['sgc_number_sample'])?>
<?=oneField('Total rock sample', $data['sgc_number_rock'])?>
<?=oneField('Total fluid sample', $data['sgc_number_fluid'])?>
<?=oneField('HC composition', $data['sgc_hc_composition'])?>
<?=oneField('Areal closure estimation P90 (acre)', $data['sgc_vol_p90_area'])?>
<?=oneField('Areal closure estimation P50 (acre)', $data['sgc_vol_p50_area'])?>
<?=oneField('Areal closure estimation P10 (acre)', $data['sgc_vol_p10_area'])?>
<?=oneField('Remark', $data['sgc_remark'])?>

<h3>Gravity Survey</h3>
<?=oneField('Year survey', $data['sgv_year_survey'])?>
<?=oneField('Survey method', $data['sgv_survey_method'])?>
<?=oneField('Total coverage area (acre)', $data['sgv_coverage_area'])?>
<?=oneField('Depth survey penetration range (feet)', $data['sgv_depth_range'])?>
<?=oneField('Recorder spacing interval (feet)', $data['sgv_spacing_interval'])?>
<?=oneField('Depth contour spill point (feet)', $data['sgv_depth_spill'])?>
<?=oneField('Depth contour estimate OWC/GWC (feet)', $data['sgv_depth_estimate'])?>
<?=oneField('Depth contour lowest known oil/gas (feet)', $data['sgv_depth_low'])?>
<?=oneField('Reservoir thickness (feet)', $data['sgv_res_thickness'])?>
<?=oneField('Top sand seismic depth (feet)', $data['sgv_res_top_depth'])?>
<?=oneField('Bottom sand seismic depth (feet)', $data['sgv_res_bot_depth'])?>
<?=oneField('Gross sand thickness (%)', $data['sgv_gross_thickness'])?>
<?=oneField('Gross sand thickness according to', $data['sgv_gross_according'])?>
<?=oneField('Net to gross (%)', $data['sgv_net_gross'])?>
<?=oneField('Areal closure estimation P90 (acre)', $data['sgv_vol_p90_area'])?>
<?=oneField('Areal closure estimation P50 (acre)', $data['sgv_vol_p50_area'])?>
<?=oneField('Areal closure estimation P10 (acre)', $data['sgv_vol_p10_area'])?>
<?=oneField('Remark', $data['sgv_remark'])?>

<h3>Electromagnetic Survey</h3>
<?=oneField('Year survey', $data['sel_year_survey'])?>
<?=oneField('Survey method', $data['sel_survey_method'])?>
<?=oneField('Total coverage area (acre)', $data['sel_coverage_area'])?>
<?=oneField('Depth survey penetration range (feet)', $data['sel_depth_range'])?>
<?=oneField('Recorder spacing interval (feet)', $data['sel_spacing_interval'])?>
<?=oneField('Areal closure estimation P90 (acre)', $data['sel_vol_p90_area'])?>
<?=oneField('Areal closure estimation P50 (acre)', $data['sel_vol_p50_area'])?>
<?=oneField('Areal closure estimation P10 (acre)', $data['sel_vol_p10_area'])?>
<?=oneField('Remark', $data['sel_remark'])?>
<?=oneField('Year survey', $data['rst_year_survey'])?>
<?=oneField('Survey method', $data['rst_survey_method'])?>
<?=oneField('Total coverage area (acre)', $data['rst_coverage_area'])?>
<?=oneField('Depth survey penetration range (feet)', $data['rst_depth_range'])?>
<?=oneField('Recorder spacing interval (feet)', $data['rst_spacing_interval'])?>
<?=oneField('Areal closure estimation P90 (acre)', $data['rst_vol_p90_area'])?>
<?=oneField('Areal closure estimation P50 (acre)', $data['rst_vol_p50_area'])?>
<?=oneField('Areal closure estimation P10 (acre)', $data['rst_vol_p10_area'])?>
<?=oneField('Remark', $data['rst_remark'])?>

<h3>Other Survey</h3>
<?=oneField('Year survey', $data['sor_year_survey'])?>
<?=oneField('Areal closure estimation P90 (acre)', $data['sor_vol_p90_area'])?>
<?=oneField('Areal closure estimation P50 (acre)', $data['sor_vol_p50_area'])?>
<?=oneField('Areal closure estimation P10 (acre)', $data['sor_vol_p10_area'])?>
<?=oneField('Remark', $data['sor_remark'])?>
</div>