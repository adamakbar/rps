<!doctype html>
<html>
<head>

  <title>Basin <?=$name?></title>

  <link rel="stylesheet" href="<?= assets('css/pure.min.css', true) ?>" />
  <style type="text/css">
    html {
      font-family: "Roboto";
      font-size: 11px;
    }
    hr {
      border: 0;
      height: 1px;
      background: #000;
    }
    .p-form > div:nth-child(1) {
      font-family: "Roboto";
      text-align: right;
      margin-right: 5px;
      font-weight: bold;
    }
    .p-form > div:nth-child(2) {
      font-family: "Roboto";
    }
    .p-form > div:nth-child(2)::before {
      content: ": ";
    }
    .break {
      page-break-before: always;
    }
    .forth {
      margin-left: 25px;
      margin-bottom: 20px;
    }
    h3 {
      margin-left: 10px;
    }
  </style>
</head>

<body>

<h1>Basin<br/><?=$name?></h1>

<h4>Daftar Wilayah Kerja</h4>
<ul>
<?php foreach($listWk as $wk): ?>
  <li><a href="<?=$wk?>"><?=getWkName($wk)?></a></li>
<?php endforeach; ?>
</ul>
<div class="break"></div>

<?= $contents ?>

</body>
</html>
