<div class="break"></div>
<hr>
<h3 id="<?=$data['wk_id'].$data['structure_name']?>">Drillable: <?=$data['structure_name']?></h3>
<hr>

<div class="forth">
<?=oneField('Play', $data['play_name'])?>
<?=oneField('Basin', $data['basin_name'])?>
<?=oneField('Province', $data['province_name'])?>
<?=oneField('WKID', $data['wk_id'])?>
<?=oneField('Working area name', $data['wk_name'])?>
<?=oneField('Clarified by', $data['prospect_clarified'])?>
<?=oneField('Initiate date', $data['prospect_date_initiate'])?>
<?=oneField('Center latitude', $data['prospect_latitude'])?>
<?=oneField('Center longiture', $data['prospect_longitude'])?>
<?=oneField('Onshore or offshore', $data['prospect_shore'])?>
<?=oneField('Terrain', $data['prospect_terrain'])?>
<?=oneField('Nearby field', $data['prospect_near_field'])?>
<?=oneField('Nearby infrastructure', $data['prospect_near_infra_structure'])?>

<h3>Adjacent Well Data Availability</h3>
<?=oneField('Porosity P90', $data['dr_por_p90'])?>
<?=oneField('Porosity P50', $data['dr_por_p50'])?>
<?=oneField('Porosity P10', $data['dr_por_p10'])?>
<?=oneField('Porosity remark', $data['dr_por_remark'])?>
<?=oneField('Water saturation P90', $data['dr_satur_p90'])?>
<?=oneField('Water saturation P50', $data['dr_satur_p50'])?>
<?=oneField('Water saturation P10', $data['dr_satur_p10'])?>
<?=oneField('Water saturation remark', $data['dr_satur_remark'])?>
</div>