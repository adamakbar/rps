<h2 id="<?=$wkid?>">Wilayah Kerja<br/><?=getWkName($wkid)?></h2>

<?php if ($data[0]): ?>
  <h3>Play</h3>
  <ul>
  <?php foreach ($data[0] as $play): ?>
    <li><a href="<?=$wkid.$play['play_name']?>"><?=$play['play_name']?></a></li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>

<?php if ($data[1]): ?>
  <h3>Lead</h3>
  <ul>
  <?php foreach ($data[1] as $lead): ?>
    <li><a href="<?=$wkid.$lead['structure_name']?>"><?=$lead['structure_name']?></a></li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>

<?php if ($data[2]): ?>
  <h3>Drillable</h3>
  <ul>
  <?php foreach ($data[2] as $drillable): ?>
    <li><a href="<?=$wkid.$drillable['structure_name']?>"><?=$drillable['structure_name']?></a></li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>

<?php if ($data[3]): ?>
  <h3>Postdrill</h3>
  <ul>
  <?php foreach ($data[3] as $postdrill): ?>
    <li><a href="<?=$wkid.$postdrill['structure_name']?>"><?=$postdrill['structure_name']?></a></li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>

<?php if ($data[4]): ?>
  <h3>Discovery</h3>
  <ul>
  <?php foreach ($data[4] as $discovery): ?>
    <li><a href="<?=$wkid.$discovery['structure_name']?>"><?=$discovery['structure_name']?></a></li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>