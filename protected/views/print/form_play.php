<div class="break"></div>
<hr>
<h3 id="<?=$data['wk_id'].$data['play_name']?>">Play: <?=$data['play_name']?></h3>
<hr>

<div class="forth">
<?=oneField('Basin', $data['basin_name'])?>
<?=oneField('Province', $data['province_name'])?>
<?=oneField('KKKS Name', $data['kkks_name'])?>
<?=oneField('WKID', $data['wk_id'])?>
<?=oneField('Working area ', $data['wk_name'])?>
<?=oneField('Analog to', $data['play_analog_to'])?>
<?=oneField('Analog distance', $data['play_analog_distance'], 'Km')?>
<?=oneField('Exploration method', $data['play_exr_method'])?>
<?=oneField('Onshore or offshore', $data['play_shore'])?>
<?=oneField('Terrain', $data['play_terrain'])?>
<?=oneField('Nearby field', $data['play_near_field'], 'Km')?>
<?=oneField('Nearby infrastructure', $data['play_near_infra_structure'], 'Km')?>
</div>

<hr>
<h3>Data Availability</h3>
<hr>

<div class="forth">
<?=oneField('Support data', $data['play_support_data'])?>
<?=oneField('Outcrop distance', $data['play_outcrop_distance'], 'Km')?>

<h3>2D Seismic Survey</h3>

<?=oneField('Acquisition year', $data['play_s2d_year'])?>
<?=oneField('Total seismic crossline', $data['play_s2d_crossline'])?>
<?=oneField('Distance seismic intervall', $data['play_s2d_line_intervall'], 'Km')?>

<h3>Geochemistry Survey</h3>

<?=oneField('Total sample', $data['play_sgc_sample'])?>
<?=oneField('Survey range depth', $data['play_sgc_depth'], 'Feet')?>

<h3>Gravity Survey</h3>

<?=oneField('Survey acreage', $data['play_sgv_acre'], 'Acre')?>
<?=oneField('Survey range depth', $data['play_sgv_depth'], 'Feet')?>

<h3>Electromagnetic Survey</h3>

<?=oneField('Survey acreage', $data['play_sel_acre'], 'Acre')?>
<?=oneField('Survey range depth', $data['play_sel_depth'], 'Feet')?>

<h3>Resistivity Survey</h3>

<?=oneField('Survey acreage', $data['play_srt_acre'], 'Acre')?>

<h3>Geological Regional Map</h3>

<?=oneField('Scale', $data['play_map_scale'])?>
<?=oneField('Map author', $data['play_map_author'])?>
<?=oneField('Year of published', $data['play_map_year'])?>
</div>