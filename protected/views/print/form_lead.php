<div class="break"></div>
<hr>
<h3 id="<?=$data['wk_id'].$data['structure_name']?>">Lead: <?=$data['structure_name']?></h3>
<hr>

<div class="forth">
<?=oneField('Play name', $data['play_name'])?>
<?=oneField('Basin', $data['basin_name'])?>
<?=oneField('Province', $data['province_name'])?>
<?=oneField('KKKS', $data['kkks_name'])?>
<?=oneField('WKID', $data['wk_id'])?>
<?=oneField('Wilayah Kerja', $data['wk_name'])?>
<?=oneField('Clarified by', $data['lead_clarified'])?>
<?=oneField('Study year', $data['lead_year_study'])?>
<?=oneField('Lead date initiate', $data['lead_date_initiate'])?>
<?=oneField('Center latitude', $data['lead_latitude'])?>
<?=oneField('Center longitude', $data['lead_longitude'])?>
<?=oneField('Shore', $data['lead_shore'])?>
<?=oneField('Terrain', $data['lead_terrain'])?>
<?=oneField('Nearby field', $data['lead_near_field'])?>
<?=oneField('Nearby infrastructure', $data['lead_near_infra_structure'])?>
</div>

<hr>
<h3>Data Availability</h3>
<hr>

<div class="forth">
<h3>2D Seismic Survey</h3>
<?=oneField('Vintage number', $data['ls2d_vintage_number'])?>
<?=oneField('Year survey', $data['ls2d_year_survey'])?>
<?=oneField('Total crossline', $data['ls2d_total_crossline'])?>
<?=oneField('Total coverage', $data['ls2d_total_coverage'])?>
<?=oneField('Average interval', $data['ls2d_average_interval'])?>
<?=oneField('Latest processing year', $data['ls2d_year_late_process'])?>
<?=oneField('Latest method', $data['ls2d_late_method'])?>
<?=oneField('Image quality', $data['ls2d_img_quality'])?>
<?=oneField('Low estimate (acre)', $data['ls2d_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['ls2d_best_estimate'])?>
<?=oneField('High estimate (acre)', $data['ls2d_hight_estimate'])?>
<?=oneField('Remark', $data['ls2d_remark'])?>

<h3>Geological Field Survey</h3>
<?=oneField('Year survey', $data['lsgf_year_survey'])?>
<?=oneField('Survey method', $data['lsgf_survey_method'])?>
<?=oneField('Coverage area', $data['lsgf_coverage_area'])?>
<?=oneField('Low estimate (acre)', $data['lsgf_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['lsgf_best_estimate'])?>
<?=oneField('High estimate (acre)', $data['lsgf_high_estimate'])?>
<?=oneField('Remark', $data['lsgf_remark'])?>

<h3>Geochemistry Survey</h3>
<?=oneField('Year survey', $data['lsgc_year_survey'])?>
<?=oneField('Range interval', $data['lsgc_range_interval'])?>
<?=oneField('Sample number', $data['lsgc_number_sample'])?>
<?=oneField('Rock sample number', $data['lsgc_number_rock'])?>
<?=oneField('Fluid sample number', $data['lsgc_number_fluid'])?>
<?=oneField('HC availability', $data['lsgc_hc_availability'])?>
<?=oneField('HC composition', $data['lsgc_hc_composition'])?>
<?=oneField('Year report', $data['lsgc_year_report'])?>
<?=oneField('Low estimate (acre)', $data['lsgc_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['lsgc_high_estimate'])?>
<?=oneField('High estimate (acre)', $data['lsgc_best_estimate'])?>
<?=oneField('Remark', $data['lsgc_remark'])?>

<h3>Gravity Survey</h3>
<?=oneField('Year survey', $data['lsgv_year_survey'])?>
<?=oneField('Survey method', $data['lsgv_survey_method'])?>
<?=oneField('Coverage area', $data['lsgv_coverage_area'])?>
<?=oneField('Penetration range', $data['lsgv_range_penetration'])?>
<?=oneField('Spacing interval', $data['lsgv_spacing_interval'])?>
<?=oneField('Low estimate (acre)', $data['lsgv_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['lsgv_best_estimate'])?>
<?=oneField('High estimate (acre)', $data['lsgv_high_estimate'])?>
<?=oneField('Remark', $data['lsgv_remark'])?>

<h3>Electromagnetic Survey</h3>
<?=oneField('Year survey', $data['lsel_year_survey'])?>
<?=oneField('Survey method', $data['lsel_survey_method'])?>
<?=oneField('Coverage area', $data['lsel_coverage_area'])?>
<?=oneField('Penetration range', $data['lsel_range_penetration'])?>
<?=oneField('Spacing interval', $data['lsel_spacing_interval'])?>
<?=oneField('Low estimate (acre)', $data['lsel_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['lsel_best_estimate'])?>
<?=oneField('High estimate (acre)', $data['lsel_high_estimate'])?>
<?=oneField('Remark', $data['lsel_remark'])?>

<h3>Resistivity Survey</h3>
<?=oneField('Year survey', $data['lsrt_year_survey'])?>
<?=oneField('Survey method', $data['lsrt_survey_method'])?>
<?=oneField('Coverage area', $data['lsrt_coverage_area'])?>
<?=oneField('Penetration range', $data['lsrt_range_penetration'])?>
<?=oneField('Spacing interval', $data['lsrt_spacing_interval'])?>
<?=oneField('Low estimate (acre)', $data['lsrt_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['lsrt_best_estimate'])?>
<?=oneField('High estimate (acre)', $data['lsrt_high_estimate'])?>
<?=oneField('Remark', $data['lsrt_remark'])?>

<h3>Other Survey</h3>
<?=oneField('Year survey', $data['lsor_year_survey'])?>
<?=oneField('Low estimate (acre)', $data['lsor_low_estimate'])?>
<?=oneField('Best estimate (acre)', $data['lsor_best_estimate'])?>
<?=oneField('High estimate (acre)', $data['lsor_high_estimate'])?>
<?=oneField('Remark', $data['lsor_remark'])?>
</div>