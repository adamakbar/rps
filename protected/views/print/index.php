<?php
  $dataPrint = new DataPrint();
  $wkid = Yii::app()->user->wk_id;
?>

<div class="container-fluid">
  <div class="row-fluid">
    <h2>Print All Data</h2>
    <table class="table table-bordered table-hover table-condensed">
      <thead>
        <tr>
          <th>Classification</th>
          <th>Name</th>
          <th>Print to PDF</th>
        </tr>
      </thead>

      <tbody>
      <?php foreach($dataPrint->list_play($wkid) as $play): ?>
        <tr>
          <td><?= $play['classification'] ?></td>
          <td><?= $play['play_name'] ?></td>
          <td><a href="<?= route('/print/playDetail', ['id' => $play['play_id']]) ?>">PDF</a></td>
        </tr>
      <?php endforeach; ?>
      </tbody>

    </table>
  </div>
</div>