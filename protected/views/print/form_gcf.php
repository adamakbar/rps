<hr>
<h3>Geological Chance Factor</h3>
<hr>

<div class="forth">
<h3>Source Rock</h3>

<?=oneField('Proven or analog', $data['gcf_is_sr'])?>
<?=oneField('Source age', "{$data['gcf_sr_age_serie']} {$data['gcf_sr_age_system']}")?>
<?=oneField('Source formation', "{$data['gcf_sr_formation_serie']} {$data['gcf_sr_formation']}")?>
<?=oneField('Kerogen type', $data['gcf_sr_kerogen'])?>
<?=oneField('Source capacity (TOC)', $data['gcf_sr_toc'])?>
<?=oneField('Heatflow unit', $data['gcf_sr_hfu'])?>
<?=oneField('Distribution', $data['gcf_sr_distribution'])?>
<?=oneField('Maturity', $data['gcf_sr_maturity'])?>
<?=oneField('Present of other source rock', $data['gcf_sr_otr'])?>
<?=oneField('Remark', $data['gcf_sr_remark'])?>

<h3>Reservoir</h3>

<?=oneField('Proven or analog', $data['gcf_is_res'])?>
<?=oneField('Reservoir age', "{$data['gcf_res_age_serie']} {$data['gcf_res_age_system']}")?>
<?=oneField('Reservoir formation', "{$data['gcf_res_formation_serie']} {$data['gcf_res_formation']}")?>
<?=oneField('Depositional setting', $data['gcf_res_depos_set'])?>
<?=oneField('Depositional environment', $data['gcf_res_depos_env'])?>
<?=oneField('Distribution', $data['gcf_res_distribution'])?>
<?=oneField('Continuity', $data['gcf_res_continuity'])?>
<?=oneField('Lithology', $data['gcf_res_lithology'])?>
<?=oneField('Primary porosity', $data['gcf_res_por_primary'])?>
<?=oneField('Secondary porosity', $data['gcf_res_por_secondary'])?>
<?=oneField('Remark', $data['gcf_res_remark'])?>

<h3>Trap</h3>

<?=oneField('Proven or analog', $data['gcf_is_trap'])?>
<?=oneField('Sealing age', "{$data['gcf_trap_seal_age_serie']} {$data['gcf_trap_seal_age_system']}")?>
<?=oneField('Sealing formation', "{$data['gcf_trap_seal_formation_serie']} {$data['gcf_trap_seal_formation']}")?>
<?=oneField('Sealing distribution', $data['gcf_trap_seal_distribution'])?>
<?=oneField('Sealing continuity', $data['gcf_trap_seal_continuity'])?>
<?=oneField('Sealing type', $data['gcf_trap_seal_type'])?>
<?=oneField('Source age', "{$data['gcf_trap_age_serie']} {$data['gcf_trap_age_system']}")?>
<?=oneField('Trapping geometry', $data['gcf_trap_geometry'])?>
<?=oneField('Trapping type', $data['gcf_trap_type'])?>
<?=oneField('Closure type', $data['gcf_trap_closure'])?>
<?=oneField('Remark', $data['gcf_trap_remark'])?>

<h3>Dynamic</h3>

<?=oneField('Proven or analog', $data['gcf_is_dyn'])?>
<?=oneField('Authenticate migration', $data['gcf_dyn_migration'])?>
<?=oneField('Trap position due to kitchen', $data['gcf_dyn_kitchen'])?>
<?=oneField('Tectonic order', $data['gcf_dyn_petroleum'])?>
<?=oneField('Tectonic regime (earliest)', "{$data['gcf_dyn_early_age_serie']} {$data['gcf_dyn_early_age_system']}")?>
<?=oneField('Tectonic regime (latest) age', "{$data['gcf_dyn_late_age_serie']} {$data['gcf_dyn_late_age_system']}")?>
<?=oneField('Preservation', $data['gcf_dyn_preservation'])?>
<?=oneField('Migration pathways', $data['gcf_dyn_pathways'])?>
<?=oneField('Tectonic regime (earliest)', "{$data['gcf_dyn_migration_age_serie']} {$data['gcf_dyn_migration_age_system']}")?>
<?=oneField('Remark', $data['gcf_dyn_remark'])?>
</div>