<div class="break"></div>
<hr>
<h3>Well: <?=$data['wl_name']?></h3>
<hr>

<div class="forth">
  <h3>Well General Data</h3>
  <?=oneField('Center latitude', $data['wl_latitude'])?>
  <?=oneField('Center longitude', $data['wl_longitude'])?>
  <?=oneField('Type', $data['wl_type'])?>
  <?=oneField('Result', $data['wl_result'])?>
  <?=oneField('Shore', $data['wl_shore'])?>
  <?=oneField('Terrain', $data['wl_terrain'])?>
  <?=oneField('Status', $data['wl_status'])?>
  <?=oneField('Formation', $data['wl_formation'])?>
  <?=oneField('Completed date', $data['wl_date_complete'])?>
  <?=oneField('Target total depth TVD (feet)', $data['wl_target_depth_tvd'])?>
  <?=oneField('Target total depth MD (feet)', $data['wl_target_depth_md'])?>
  <?=oneField('Actual total depth TVD (feet)', $data['wl_actual_depth'])?>
  <?=oneField('Targeted penetrated play or reservoir target TVD (feet)', $data['wl_target_play'])?>
  <?=oneField('Actual penetrated play or reservoir target TVD (feet)', $data['wl_actual_play'])?>
  <?=oneField('Total MDT sample', $data['wl_number_mdt'])?>
  <?=oneField('Total RFT sample', $data['wl_number_rft'])?>
  <?=oneField('Reservoir initial pressure (psig)', $data['wl_res_pressure'])?>
  <?=oneField('Last reservoir pressure (psig)', $data['wl_last_pressure'])?>
  <?=oneField('Pressure gradient (psgi/feet)', $data['wl_pressure_gradient'])?>
  <?=oneField('Last reservoir temperatur (Celcius)', $data['wl_last_temp'])?>
  <?=oneField('Actual integrity', $data['wl_integrity'])?>
  <?=oneField('List of electrolog data', $data['wl_electro_list'])?>
</div>