<?php
/* @var $this SiteController */

$this->pageTitle='SKK Administrator';
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">User Management</h3>
            <hr/>
		</div>
    </div>
    <script>
        $(document).ready(function() {
            $("#wk-name").select2({width: 'resolve'});
        
            $("#wk-name").change(function() {
                $.ajax({
                    url: '<?php echo $this->createUrl('/admin/Getwkdetail'); ?>',
                    type: 'get',
                    data: {"wk_id": $("#wk-name").val()},
                    dataType: 'json',
                    success: function(result) {
                        $("#kkks-name").val(result.kkks_name);
                        $("#username-kkks").val("");
                        $("#password-kkks").val("");
                    }
                });
            });

            $("#gen-pass").click(function() {
                $.ajax({
                    url: '<?php echo $this->createUrl('/admin/Changeuserpass'); ?>',
                    type: 'get',
                    data: {"wk_id": $("#wk-name").val()},
                    dataType: 'json',
                    success: function(result) {
                        $("#username-kkks").val(result.user);
                        $("#password-kkks").val(result.password);
                        alert('Save new user password complete, please note down the user and password.');
                    }
                });
            });
            
            $("#reset-pass").click(function() {
                $.ajax({
                    url: '<?php echo $this->createUrl('/admin/Resetallpass'); ?>',
                    type: 'get',
                    dataType: 'json',
                    success: function(res) {
                        $.fileDownload(res.filename);
                    }
                });
            });
            
            $("#admin-set-pass").click(function() {
                $.ajax({
                    url: '<?php echo $this->createUrl('/admin/Changeadminpass'); ?>',
                    type: 'get',
                    data: {"new_pass": $("#admin-new-pass").val()},
                    dataType: 'json',
                    success: function() {
                        alert('Save new admin password complete.');
                    }
                });
            });
        });
    </script>

<h4>Change KKKS User & Password</h4>
<table border="0" cellpadding="5">
    <tr>
        <td align="right">WK Name</td>
        <td>
            <select id="wk-name">
                <option value="">-- Choose --</option>
                <?php foreach ($n['wkid'] as $key => $val) { ?>
                    <option value="<?php echo $val['wk_id']; ?>"><?php echo $val['wk_name']; ?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td align="right">KKKS Name</td>
        <td><input type="text" id="kkks-name" readonly></td>
    </tr>
    <tr>
        <td align="right">Username</td>
        <td><input type="text" id="username-kkks" readonly></td>
    </tr>
    <tr>
        <td align="right">Password</td>
        <td><input type="text" id="password-kkks" readonly></td>
    </tr>
</table>
<button type="button" id="gen-pass">Generate Password</button>

<button style="float: right;" type="button" id="reset-pass">
    <b>RESET ALL KKKS USER &amp; PASSWORD</b>
</button>

<hr/>
    
<h4>Change Administrator Password</h4>
<input type="text" id="admin-new-pass">
<button type="button" id="admin-set-pass">Save</button>

