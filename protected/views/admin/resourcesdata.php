<?php
/* @var $this SiteController */

$this->pageTitle='Resources Data';
?>
<style>
table tr td.sudah {
    color: green;
}
table tr td.belum {
    color: red;
}
div.total {
    display: block;
    margin: 0 auto;
}
</style>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">Resources Data</h3>
		</div>
    </div>
<script>
$(document).ready(function() {
    $("[name^=surat-]").change(function() {
        $.ajax({
            url: '<?php echo $this->createUrl('/admin/Changesurat'); ?>',
            type: 'get',
            data: {"wk_id": this.value},
            dataType: 'json',
            success: function() {
                alert('Status pengiriman surat sudah dirubah');
            }
        });
    });
});
</script>
    <div>

		<a class="btn" href="<?=route('admin/PrintDetailAll')?>">Print All Data</a>
    <a class="btn" href="<?=route('admin/PrintDetailAllPdf')?>">Print All Data PDF</a>
		<a class="btn" href="<?=route('admin/KenMaret')?>">KEN Maret</a>

        <?php $row = 0; $this->widget('zii.widgets.grid.CGridView', array(
            'id'=> 'list-wk',
            'dataProvider'=> $wk_provider,
            'columns'=> array(
                array(
                    'name' => 'Basin',
                    'value' => '$data["basin"]',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Nama WK',
                    'value' => 'CHtml::link($data["wk"], Yii::app()->createUrl("admin/impersonate",
                                array("user" => $data["username"])))',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Play',
                    'value' => '$data["play"]',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Lead',
                    'value' => '$data["lead"]',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Drillable',
                    'value' => '$data["drillable"]',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Postdrill',
                    'value' => '$data["postdrill"]',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Discovery',
                    'value' => '$data["discovery"]',
                    'type'=>'raw',
                ),
                array(
                    'name' => 'Surat',
                    'type' => 'raw',
                    'value' => 'CHtml::CheckBox("surat-".$data["wk_id"], ($data["surat"]=="Sudah"?true:false), array("value"=>$data["wk_id"]));',
                ),
                array(
                    'name' => 'Montage 2012',
                    'type' => 'raw',
                    'value' => '!file_exists($data["montage_2012"])?"":CHtml::Link("PNG", $data["montage_2012"]);',
                ),
                array(
                    'name' => 'Montage 2013',
                    'type' => 'raw',
                    'value' => '!file_exists($data["montage_2013"])?"":CHtml::Link("PDF", $data["montage_2013"]);',
                ),
		array(
                    'name' => 'Montage 2014',
                    'type' => 'raw',
                    'value' => '!file_exists($data["montage_2014"])?"":CHtml::Link("PDF", $data["montage_2014"]);',
                ),
                array(
                    'name' => 'Status WK',
                    'value' => '$data["stage"]',
                    'type'=>'raw',
                ),
            ),
        ));?>
    </div>

    <form action="" method="post">
        <div align="right">
            <input type="submit" name="rekap-all-data" value="All Data" />
        </div>
        <div>
        <table border="0" cellpadding="5" cellspacing="5">

         <tr>
          <td align="right">Total WK</td><td><input type="submit" name="rekap-list-wk" value="<?php echo $total_wk; ?>"</td>
          <td align="right">Total Play</td><td><input type="submit" name="rekap-play" value="<?php echo $total_play; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total WK Eksplorasi</td><td><b><?php echo $total_wk_eksplorasi; ?></b></td>
          <td align="right">Total lead</td><td><input type="submit" name="rekap-lead" value="<?php echo $total_lead; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total WK Eksploitasi</td><td><b><?php echo $total_wk_eksploitasi; ?></b></td>
          <td align="right">Total Drillable</td><td><input type="submit" name="rekap-drillable" value="<?php echo $total_drillable; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total WK Terminasi</td><td><b><?php echo $total_wk_terminasi; ?></b></td>
          <td align="right">Total Postdrill</td><td><input type="submit" name="rekap-postdrill" value="<?php echo $total_postdrill; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total Sudah Mengirim Surat</td><td><b><?php echo $total_kirim_surat; ?></b></td>
          <td align="right">Total Discovery</td><td><input type="submit" name="rekap-discovery" value="<?php echo $total_discovery; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total Tidak Mengirim Surat</td><td><b><?php echo $total_tidak_kirim_surat; ?></b></td>
          <td align="right">Total Well Postdrill</td><td><input type="submit" name="rekap-well-postdrill" value="<?php echo $total_well_postdrill; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total Sudah Ada Data</td><td><b><?php echo $total_ada_data; ?></b></td>
          <td align="right">Total Well Discovery</td><td><input type="submit" name="rekap-well-discovery" value="<?php echo $total_well_discovery; ?>"</td>
         </tr>
         <tr>
          <td align="right">Total Tidak Ada Data</td><td><b><?php echo $total_tidak_ada_data; ?></b></td>
         </tr>
        </table> 
        </div>
    </form>