<?php
/* @var $this SiteController */

$this->pageTitle='Status Management';
$this->breadcrumbs=array(
	'Status Management',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<div id="play_page">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
	            <h3 class="page-title">Status Management</h3>
	            <ul class="breadcrumb">
	                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
	                <li><a href="#"><strong>Set Status Time</strong></a><span class="divider-last">&nbsp;</span></li>
	            </ul>
			</div>
		</div>
	
		<!-- BEGIN PAGE CONTENT-->
	
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN LIST DATA PLAY -->
				<div id="_list-data-play" class="widget">
					<div class="widget-title">
						<h4><i class="icon-table"></i> LIST DATA STATUS</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'status-grid',
							'dataProvider'=>$statusDataProvider,
							'htmlOptions'=>array('class'=>'table table-striped'),
							'columns'=>array(
								array(
									'header'=>'No.',
									'value'=>'$row+1',
								),
								array(	
									'name'=>'RPS Year',
									'value'=>'$data->rps_year',
								),
								array(
									'name'=>'Status',
									'value'=>'$data->status_name',
								),
								array(
									'name'=>'Begin',
									'value'=>'$data->begin',
								),
								array(
									'name'=>'End',
									'value'=>'$data->end',
								),
								array(
									'template'=>'{edit}',
									'class'=>'CButtonColumn',
									'buttons'=>array(
										'edit'=>array(
											'options'=>array(
												'class'=>'icon-edit',
											),
											'url'=>'Yii::app()->createUrl("/admin/updatestatustime", array("id"=>$data->id))',
										),
									),
								),
							),
						));?>
					</div>
				</div>
				<!-- END LIST DATA PLAY -->
			</div>
		</div>
	
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'createstatus-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'enableAjaxValidation'=>true,
		));?>
		
		
        
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div id="_gen_profile" class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> CREATE STATUS</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">RPS Year :</label>
                                <div class="controls wajib">
                                    <!-- <input id="_wellname" class="span3" type="text" /> -->
                                    <?php echo $form->textField($mdlStatus, 'rps_year', array('id'=>'tahun_rps', 'class'=>'span1', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                    <?php echo $form->error($mdlStatus, 'rps_year');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Status :</label>
                                <div class="controls wajib">
                                    <!-- <input id="wellcategory_" class="span3" style="text-align: center;" /> -->
                                    <?php echo $form->textField($mdlStatus, 'status_name', array('id'=>'status_name', 'class'=>'span1', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                    <?php echo $form->error($mdlStatus, 'status_name');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Begin :</label>
                                <div class="controls wajib">
                                	<div class=" input-append">
	                                    <?php echo $form->textField($mdlStatus, 'begin', array('id'=>'dl7', 'class'=>'m-wrap medium popovers disable-input', 'style'=>'max-width:138px;'));?>
	                                    <span class="add-on"><i class="icon-calendar"></i></span>
	                                    <span id="cleardl7" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                    </div>
                                    <?php echo $form->error($mdlStatus, 'begin');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">End :</label>
                                <div class="controls wajib">
                                	<div class=" input-append">
	                                    <?php echo $form->textField($mdlStatus, 'end', array('id'=>'l5', 'class'=>'m-wrap medium popovers disable-input', 'style'=>'max-width:138px;'));?>
	                                    <span class="add-on"><i class="icon-calendar"></i></span>
	                                    <span id="clearl5" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                    </div>
                                    <?php echo $form->error($mdlStatus, 'end');?>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
        <?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/admin/statustime'), array(
            'type'=>'POST',
            'dataType'=>'json',
            'beforeSend'=>'function(data) {
                $("#pesan").show();
                $("#pesan").html("Sending...");
            }',
            'success'=>'js:function(data) {
                $(".tooltips").attr("data-original-title", "");
                
                $(".has-err").removeClass("has-err");
                $(".errorMessage").hide();
                
                if(data.result === "success") {
                    $(".close").addClass("redirect");
                    $("#message").html(data.msg);
                    $("#popup").modal("show");
                    $("#pesan").hide();
                    $.fn.yiiGridView.update("status-grid");
                    $(".redirect").click( function () {
                        var redirect = "' . Yii::app()->createUrl('/admin/statustime') . '";
                        window.location=redirect;
                    });
                } else {
                    var myArray = JSON.parse(data.err);
                    $.each(myArray, function(key, val) {
                        if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
                        {
                            $("#createstatus-form #"+key+"_em_").parent().addClass("has-err");
                            $("#createstatus-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
                            
                        } else {
                            $("#createstatus-form #"+key+"_em_").text(val);                                                    
                            $("#createstatus-form #"+key+"_em_").show();
                            $("#createstatus-form #"+key+"_em_").parent().addClass("has-err");
                        }
                        
                    });
                
                    $("#message").html(data.msg);
                    $("#popup").modal("show");
                    $("#pesan").hide();
                }
            }',
        ),
        array('class'=>'btn btn-inverse')
        );?>
        <?php $this->endWidget();?>
      
	</div>
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->		
<!-- popup submit -->
	<div id="popup" class="modal hide fade" tabindex="-10" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    <h3 id="myModalLabel">Info</h3>
		</div>
		<div class="modal-body">
			<p id="message"></p>
		</div>
	</div>
<!-- end popup submit -->

<?php 
$cs  = $cs = Yii::app()->getClientScript();

$css = <<<EOD
    table.items {
        width: 100%;
    }

EOD;

$cs->registerCss($this->id . 'css', $css);
?>