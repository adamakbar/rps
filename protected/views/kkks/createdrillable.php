<?php
/* @var $this SiteController */

$this->pageTitle='Drillable';
$this->breadcrumbs=array(
	'Drillable',
);
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">		    			
			<h3 class="page-title">DRILLABLE PROSPECT</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Drillable</strong></a><span class="divider-last">&nbsp;</span></li>
			</ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
		<div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-table"></i> LIST DATA DRILLABLE</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <?php $this->widget('zii.widgets.grid.CGridView', array(
                        	'id'=>'drillable-grid',
                        	'dataProvider'=>$model->searchRelatedDrillable(),
                            'filter'=>$model,
                        	'htmlOptions'=>array('class'=>'table table-striped'),
                        	'columns'=>array(
                        		array(
									'header'=>'No.',
                                    'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
								),
                                array(
                                    'header'=>'Basin',
                                    'filter'=>CHtml::activeTextField($model, 'basin_name'),
                                    'value'=>'$data->plays->basin->basin_name',
                                ),
								array(
									'header'=>'Prospect Name / Closure Name',
                                    'filter'=>CHtml::activeTextField($model, 'drillable_name'),
// 									'value'=>'CHtml::link($data->structure_name, array("GetPreviousDataDrillable", "id"=>$data->prospect_id), array("class"=>"ajaxupdate"))',
									'value'=>'$data->getProspectName1($data->gcf_id)',
									'type'=>'raw',
								),
								array(
									'header'=>'Status',
									'value'=>'$data->prospect_submit_status',
								),
								array(
                                    'header'=>'Actions',
									'template'=>'{edit} {delete }',
									'class'=>'CButtonColumn',
									'buttons'=>array(
										'edit'=>array(
											'options'=>array(
												'class'=>'icon-edit',
											),
											'url'=>'Yii::app()->createUrl("/kkks/updatedrillable", array("id"=>$data->prospect_id))',
										),
										'delete '=>array(
											'options'=>array(
													'class'=>'icon-remove'
											),
											'url'=>'Yii::app()->createUrl("/kkks/deletedrillable", array("id"=>$data->prospect_id))',
											'click'=>"function() {
												$.fn.yiiGridView.update('drillable-grid', {  //change my-grid to your grid's name
											        type:'POST',
											        url:$(this).attr('href'),
											        success:function(data) {
											              $.fn.yiiGridView.update('drillable-grid'); //change my-grid to your grid's name
											        }
											    })
											    return false;
											  }
											",
										),
									),
								),
                        	),
                        ));?>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', array(
        	'id'=>'createdrillable-form',
        	'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnChange'=>false,
			),
        	'enableAjaxValidation'=>true,
        ));?>
        <?php //echo $form->errorSummary(array($mdlProspect, $mdlDrillable, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));?>
        <?php echo $form->hiddenField($mdlProspect, 'prospect_update_from', array('id'=>'prospect_update_from'));?>
		<?php echo $form->hiddenField($mdlProspect, 'prospect_submit_revision', array('id'=>'prospect_submit_revision'));?>
        <div id="pesan"></div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> GENERAL DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <div class="accordion" id="accordion1a">
                                <!-- Begin Data Prospect Administration Data -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <strong>Prospect Administration Data</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                          <div class="alert alert-success">
                                            Structure name must be unique and meaningful, name that contain "Lead",
                                            "Drillable", "Discovery" or other
                                            general term is not permitted.
                                          </div>
                                            <div class="control-group">
                                                <label class="control-label">Play Name :</label>
                                                <div class="controls wajib">
                                                	<?php 
//                                                 		$mdlPlaySearch = Play::model();
// 	                                                	$model = new Play('search');
// 	                                                	$model->unsetAttributes();
	                                                	
// 	                                                	$criteria = new CDbCriteria;
// 	                                                	$criteria->select="max(b.play_submit_date)";
// 	                                                	$criteria->alias = 'b';
// 	                                                	$criteria->condition="b.play_update_from = t.play_update_from";
// 	                                                	$subQuery=$model->getCommandBuilder()->createFindCommand($model->getTableSchema(),$criteria)->getText();
	                                                	
// 	                                                	$mainCriteria = new CDbCriteria;
// 	                                                	$mainCriteria->with = array('gcfs');
// 	                                                	$mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
// 	                                                	$mainCriteria->addCondition('play_submit_date = (' . $subQuery . ')', 'AND');
// 	                                                	$mainCriteria->group = 'play_update_from';
                                                	
                                                	?>
                                                    <!-- <input id="play_in" class="span5" type="text" style="text-align: center;"/> -->
                                                    <?php //echo $form->dropDownList($mdlProspect, 'play_id', CHtml::listData( Play::model()->findAll($mainCriteria), 'play_id', 'gcfs.playName' ), array('empty'=>'-- Select --', 'style'=>'text-align: center;', 'id'=>'plyName', 'class'=>'span5'));?>
                                                    <?php echo $form->textField($mdlProspect, 'play_id', array('style'=>'text-align: center; position: relative; display: inline-block;', 'id'=>'plyName', 'class'=>'span6'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'play_id');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Structure Name :</label>
                                                <div class="controls wajib">
                                                	<table>
                                                		<tr>
                                                			<td>
                                                				<!-- <input id="_prospectname" type="text" class="span5"/> -->
			                                                    <?php echo $form->textField($mdlProspect, 'structure_name', array('id'=>'structure_name', 'class'=>'span5', 'style'=>'width: 375px;'));?>
			                                                    <?php echo $form->error($mdlProspect, 'structure_name');?>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                            	<label class="control-label">Prospect Name / Closure Name :</label>
                                            	<div class="controls">
                                            		<div class="alert alert-success span12" style="width: 375px;">
                                            			<span id="additional_lead_name" class="add-on"></span>
                                            		</div>
                                            	</div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Clarified by :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="_clarifiedby" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_clarified', array('id'=>'_clarifiedby', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_clarified');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Prospect Name Initiation Date :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input id="dl7" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Initiation Date"/> -->
                                                        <?php echo $form->textField($mdlProspect, 'prospect_date_initiate', array('id'=>'dl7', 'class'=>'m-wrap medium popovers disable-input', 'style'=>'max-width:138px;', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Prospect Name Initiation Date'));?>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                        <span id="cleardl7" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlProspect, 'prospect_date_initiate');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Prospect Administration Data -->
                                <!-- Begin Geographical Prospect Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2_drillable">
                                            <strong>Geographical Prospect Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2_drillable" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    For one Drillable Prospect structure, please fill with exact same coordinate or with difference of 25'' with other Drillable Prospect of one structure.
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <label class="control-label">Center Latitude :</label>
                                                <div class="controls">
                                                    <table>
                                                    	<tr>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
																		<?php echo $form->textField($mdlProspect, 'center_lat_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                        				<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_degree');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
																		<?php echo $form->textField($mdlProspect, 'center_lat_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
	                                                        			<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_minute');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div> -->
																		<?php echo $form->textField($mdlProspect, 'center_lat_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
	                                                        			<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_second');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"></span> -->
																		<?php echo $form->textField($mdlProspect, 'center_lat_direction', array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_direction');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                    		</td>
                                                    	</tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Center Longitude :</label>
                                                <div class="controls">
                                                    <table>
                                                    	<tr>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
																		<?php echo $form->textField($mdlProspect, 'center_long_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                        				<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_degree');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
																		<?php echo $form->textField($mdlProspect, 'center_long_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
	                                                        			<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_minute');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div> -->
																		<?php echo $form->textField($mdlProspect, 'center_long_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
	                                                        			<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_second');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on"></span> -->
																		<?php echo $form->textField($mdlProspect, 'center_long_direction', array('value'=>'E', 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_direction');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                    		</td>
                                                    	</tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Geographical Prospect Area -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <strong>Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Onshore or Offshore :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_shore', array('id'=>'env_onoffshore', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_shore');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Terrain :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_terrain', array('id'=>'env_terrain', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_terrain');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4">
                                            <strong>Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Field :</label>
                                                <div class="controls wajib">
                                                	<p style="margin-bottom: 2px;">
                                                    <!-- <input id="n_facility" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_near_field', array('id'=>'n_facility', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_near_field');?>
                                                </div>
                                            </div>                    
                                            <div class="control-group">
                                                <label class="control-label">Nearby Infra Structure :</label>
                                                <div class="controls wajib">
                                                	<p style="margin-bottom: 2px;">
                                                    <!-- <input id="n_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_near_infra_structure', array('id'=>'n_developmentwell', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_near_infra_structure');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> ADJACENT WELL DATA AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                    	<div class="control-group">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert">×</button>
                                <p>P10 = Probability to recover HC accumulation with certain volume or greater than expected is 10%</p>
                                <p>P50 = Probability to recover HC accumulation with certain volume or greater than expected is 50%</p>
                                <p>P90 = Probability to recover HC accumulation with certain volume or greater than expected is 90%</p>
                            </div>
                        </div>
                        <span action="#" class="form-horizontal">
                            <div class="accordion" id="accordion1b">
                                <!-- Begin Porosity Rock Data from Adjacent Well -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_adjwell_dav1">
                                            <strong>Porosity Rock Data from Adjacent Well</strong>
                                        </a>
                                    </div>
                                    <div id="col_adjwell_dav1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Sample Quantity :</label>
                                                <div class="controls">
                                                    <!-- <input id="porosityrock_sample" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlDrillable, 'dr_por_quantity', array('id'=>'porosityrock_sample', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlDrillable, 'dr_por_quantity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Rocks :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div>
																		<?php echo $form->textField($mdlDrillable, 'dr_por_p90_type', array('id'=>'porosityrock_p90', 'class'=>'span3', 'style'=>'width:180px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlDrillable, 'dr_por_p90_type');?>
																	</div>
																</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 200px;">
                                                				<span>
                                                					<span class="wajib">
																		<div class="input-prepend input-append">
																			<span style="margin-left:24px;" class="add-on">P90</span>
				                                                        	<!-- <input id="porosityrock_P90" name="custom" type="text" style="max-width:137px"/> -->
				                                                        	<?php echo $form->textField($mdlDrillable, 'dr_por_p90', array('id'=>'porosityrock_P90', 'style'=>'max-width:137px', 'class'=>"popovers number"));?>
				                                                        	<span class="add-on">%</span>
																		</div>
																		<?php echo $form->error($mdlDrillable, 'dr_por_p90', array('style'=>'margin-left:24px;'));?>
																	</span>
																</span>
                                                			</td>
                                                		</tr>
                                                	</table>                                  
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                            	<div class="controls">
                                            		<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div>
																		<?php echo $form->textField($mdlDrillable, 'dr_por_p50_type', array('id'=>'porosityrock_p50', 'class'=>'span3', 'style'=>'width:180px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlDrillable, 'dr_por_p50_type');?>
																	</div>
																</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 200px;">
                                                				<span>
                                                					<span class="wajib">
	                                                					<div class="input-prepend input-append">
	                                                						<span style="margin-left:24px;" class="add-on">P50</span>
				                                                        	<!-- <input id="porosityrock_P50" name="custom" type="text" style="max-width:137px"/> -->
				                                                        	<?php echo $form->textField($mdlDrillable, 'dr_por_p50', array('id'=>'porosityrock_P50', 'style'=>'max-width:137px', 'class'=>"popovers number"));?>
				                                                        	<span class="add-on">%</span>
	                                                					</div>
	                                                					<?php echo $form->error($mdlDrillable, 'dr_por_p50',  array('style'=>'margin-left:24px;'));?>
                                                					</span>
																</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                            	<div class="controls">
                                            		<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div>
																		<?php echo $form->textField($mdlDrillable, 'dr_por_p10_type', array('id'=>'porosityrock_p10', 'class'=>'span3', 'style'=>'width:180px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlDrillable, 'dr_por_p10_type');?>
																	</div>
																</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 200px;">
                                                				<span>
                                                					<span class="wajib">
                                                						<div class="input-prepend input-append">
				                                                    		<span style="margin-left:24px;" class="add-on">P10</span>
				                                                        	<!-- <input id="porosityrock_P10" name="custom" type="text" style="max-width:137px"/> -->
				                                                        	<?php echo $form->textField($mdlDrillable, 'dr_por_p10', array('id'=>'porosityrock_P10', 'style'=>'max-width:137px', 'class'=>"popovers number"));?>
				                                                        	<span class="add-on">%</span>
					                                                    </div>
					                                                    <?php echo $form->error($mdlDrillable, 'dr_por_p10',  array('style'=>'margin-left:24px;'));?>
                                                					</span>
																</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <!-- <input id="porosityrock_p90" class="span3" type="text" style="text-align: center;"/> -->
                                                    <!-- <input id="porosityrock_p50" class="span3" type="text" style="text-align: center;"/> -->
                                                    <!-- <input id="porosityrock_p10" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php //echo $form->textField($mdlDrillable, 'dr_por_p90_type', array('id'=>'porosityrock_p90', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <?php //echo $form->textField($mdlDrillable, 'dr_por_p50_type', array('id'=>'porosityrock_p50', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <?php //echo $form->textField($mdlDrillable, 'dr_por_p10_type', array('id'=>'porosityrock_p10', 'class'=>'span3', 'style'=>'text-align: center;'));?>                                  
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="porosityrock_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlDrillable, 'dr_por_remark', array('id'=>'porosityrock_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Porosity Rock Data from Adjacent Well -->
                                <!-- Begin Water Saturation Data from Adjacent Well -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_adjwell_dav2">
                                            <strong>Water Saturation (S<sub>w</sub>) Data from Adjacent Well</strong>
                                        </a>
                                    </div>
                                    <div id="col_adjwell_dav2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Sample Quantity :</label>
                                                <div class="controls">
                                                    <!-- <input id="watersat_sample" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlDrillable, 'dr_satur_quantity', array('id'=>'watersat_sample', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlDrillable, 'dr_satur_quantity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Water Saturation (S<sub>w</sub>):</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div>
																		<?php echo $form->textField($mdlDrillable, 'dr_satur_p90_type', array('id'=>'watersat_p90', 'class'=>'span3', 'style'=>'width:180px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlDrillable, 'dr_satur_p90_type');?>
																	</div>
																</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 200px;">
                                                				<span>
                                                					<span class="wajib">
                                                						<div class="input-prepend input-append">
                                                							<span style="margin-left:24px;" class="add-on">P90</span>
				                                                        	<!-- <input id="watersat_P90" name="custom" type="text" style="max-width:137px"/> -->
				                                                        	<?php echo $form->textField($mdlDrillable, 'dr_satur_p90', array('id'=>'watersat_P90', 'style'=>'max-width:137px', 'class'=>"popovers number"));?>
				                                                        	<span class="add-on">%</span>   
					                                                    </div>
					                                                    <?php echo $form->error($mdlDrillable, 'dr_satur_p90',  array('style'=>'margin-left:24px;'));?>
                                                					</span>
																</span>
                                                			</td>
                                                		</tr>
                                                	</table>            
                                                </div>
                                            </div>
                                            <div class="control-group">
                                            	<div class="controls">
                                            		<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div>
																		<?php echo $form->textField($mdlDrillable, 'dr_satur_p50_type', array('id'=>'watersat_p50', 'class'=>'span3', 'style'=>'width:180px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlDrillable, 'dr_satur_p50_type');?>
																	</div>
																</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 200px;">
                                                				<span>
                                                					<span class="wajib">
                                                						<div class="input-prepend input-append">
                                                							<span style="margin-left:24px;" class="add-on">P50</span>
				                                                        	<!-- <input id="watersat_P50" name="custom" type="text" style="max-width:137px"/> -->
				                                                        	<?php echo $form->textField($mdlDrillable, 'dr_satur_p50', array('id'=>'watersat_P50', 'style'=>'max-width:137px', 'class'=>"popovers number"));?>
				                                                        	<span class="add-on">%</span>
					                                                    </div>
					                                                    <?php echo $form->error($mdlDrillable, 'dr_satur_p50',  array('style'=>'margin-left:24px;'));?>
                                                					</span>
																</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                            	<div class="controls">
                                            		<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div>
																		<?php echo $form->textField($mdlDrillable, 'dr_satur_p10_type', array('id'=>'watersat_p10', 'class'=>'span3', 'style'=>'width:180px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlDrillable, 'dr_satur_p10_type');?>
																	</div>
																</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 200px;">
                                                				<span>
                                                					<span class="wajib">
                                                						<div class="input-prepend input-append">
                                                							<span style="margin-left:24px;" class="add-on">P10</span>
				                                                        	<!-- <input id="watersat_P10" name="custom" type="text" style="max-width:137px"/> -->
				                                                        	<?php echo $form->textField($mdlDrillable, 'dr_satur_p10', array('id'=>'watersat_P10', 'style'=>'max-width:137px', 'class'=>"popovers number"));?>
					                                                    	<span class="add-on">%</span>
					                                                    </div>
					                                                    <?php echo $form->error($mdlDrillable, 'dr_satur_p10',  array('style'=>'margin-left:24px;'));?>
                                                					</span>
																</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                            	</div>
                                            </div>
                                            <!-- <div class="control-group"> -->
                                                <!-- <div class="controls"> -->
                                                    <!-- <input id="watersat_p90" class="span3" type="text" style="text-align: center;"/> -->
                                                    <!-- <input id="watersat_p50" class="span3" type="text" style="text-align: center;"/> -->
                                                    <!-- <input id="watersat_p10" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php //echo $form->textField($mdlDrillable, 'dr_satur_p90_type', array('id'=>'watersat_p90', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <?php //echo $form->textField($mdlDrillable, 'dr_satur_p50_type', array('id'=>'watersat_p50', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <?php //echo $form->textField($mdlDrillable, 'dr_satur_p10_type', array('id'=>'watersat_p10', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                <!-- </div> -->
                                            <!-- </div> -->
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="watersat_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlDrillable, 'dr_satur_remark', array('id'=>'watersat_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Water Saturation Data from Adjacent Well -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN OCCURANCE SURVEY DESCRIPTION-->
                <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-list"></i> SURVEY DATA AVAILABILITY</h4>
                    <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                </div>
                <div class="widget-body">
                    <span action="#" class="form-horizontal">
                        <!-- Notification -->
                        <div class="control-group">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert">×</button>
                                <strong>Info! </strong> If You have 3D Seismic Data, it's Automatically called Drillable Prospect.
                                <p>P10 = Probability to recover HC accumulation with certain volume or greater than expected is 10%</p>
                                <p>P50 = Probability to recover HC accumulation with certain volume or greater than expected is 50%</p>
                                <p>P90 = Probability to recover HC accumulation with certain volume or greater than expected is 90%</p>
                            </div>
                        </div>
                        <!-- Notification -->      
                        <div class="accordion">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlGfs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGfs','DlGfs_link','col_dav1')" /> -->
                                        <?php echo $form->checkBox($mdlGeological, 'is_checked', array('id'=>'DlGfs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlGfs","DlGfs_link","col_dav1")'));?> 
                                        <a id="DlGfs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">Geological Field Survey</a>
                                    </div>
                                </div>
                        		<div id="col_dav1" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_gfs_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlGeological, 'sgf_year_survey', array('id'=>'dav_gfs_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlGeological, 'sgf_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Method :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_gfs_surmethod" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlGeological, 'sgf_survey_method', array('id'=>'dav_gfs_surmethod', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlGeological, 'sgf_survey_method');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Coverage Area : </label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_gfs_tca" type="text" style="max-width:170px"/> -->
                                                    <?php echo $form->textField($mdlGeological, 'sgf_coverage_area', array('id'=>'dav_gfs_tca', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                    <span class="add-on">acre</span>
                                                </div>
                                                <?php echo $form->error($mdlGeological, 'sgf_coverage_area');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Volumetric Estimation</th>
                                                        <th>Areal Closure Estimation</th>
                                                        <th>Gross Sand Thickness</th>
                                                        <th>Net to Gross</th>
<!--                                                         <th>Porosity</th> -->
<!--                                                         <th>HC Saturation (1-Sw)</th> -->
<!--                                                         <th>Oil Case</th> -->
<!--                                                         <th>Gas Case</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th><strong>P90</strong></th>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_area', array('id'=>'sgf_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">acre</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p90_area');?>
                                                        	</span>
                                                        </td>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_thickness', array('id'=>'sgf_p90_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">ft</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p90_thickness');?>
                                                        	</span>
                                                        </td>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_net', array('id'=>'sgf_p90_net', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">%</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p90_net');?>
                                                        	</span>
                                                        </td>
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p90_porosity" name="custom" class="span6" type="text" readonly/> -->
<!--                                                         	<span class="add-on">%</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p90_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">%</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p90_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">MMBO</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p90_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">BCF</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
                                                    </tr>
                                                    <tr>
                                                        <th><strong>P50</strong></th>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_area', array('id'=>'sgf_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">acre</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p50_area');?>
                                                        	</span>
                                                        </td>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_thickness', array('id'=>'sgf_p50_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">ft</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p50_thickness');?>
                                                        	</span>
                                                        </td>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_net', array('id'=>'sgf_p50_net', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">%</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p50_net');?>
                                                        	</span>
                                                        </td>
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p50_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">%</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p50_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">%</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p50_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">MMBO</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p50_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">BCF</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
                                                    </tr>
                                                    <tr>
                                                        <th><strong>P10</strong></th>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_area', array('id'=>'sgf_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">acre</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p10_area');?>
                                                        	</span>
                                                        </td>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_thickness', array('id'=>'sgf_p10_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">ft</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p10_thickness');?>
                                                        	</span>
                                                        </td>
                                                        <td>
                                                        	<span class="wajib">
	                                                        	<div class=" input-append">
	                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_net', array('id'=>'sgf_p10_net', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">%</span>
	                                                        	</div>
	                                                        	<?php echo $form->error($mdlGeological, 'sgf_p10_net');?>
                                                        	</span>
                                                        </td>
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p10_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">%</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p10_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">%</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p10_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">MMBO</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
<!--                                                         <td> -->
<!--                                                         	<div class=" input-append"> -->
<!--                                                         		<input id="sgf_p10_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                         	<span class="add-on">BCF</span> -->
<!--                                                         	</div> -->
<!--                                                         </td> -->
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_gfs_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGeological, 'sgf_remark', array('id'=>'dav_gfs_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        	<div class="accordion-group">
                        		<div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlDss2" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss2','DlDss2_link','col_dav2' )"/> -->
                                        <?php echo $form->checkBox($mdlSeismic2d, 'is_checked', array('id'=>'DlDss2', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlDss2","DlDss2_link","col_dav2")'));?>
                                        <a id="DlDss2_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2"> 2D Seismic Survey</a>
                                    </div>
                        		</div>
                        		<div id="col_dav2" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_2dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_year_survey', array('id'=>'dav_2dss_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number Of Vintage :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_numvin" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_vintage_number', array('id'=>'dav_2dss_numvin', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_vintage_number');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Crossline Number :</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_2dss_tcn" type="text" class="span3"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_total_crossline', array('id'=>'dav_2dss_tcn', 'class'=>'span3 number'));?>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_total_crossline');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Line Total Length :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_sltl" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_seismic_line', array('id'=>'dav_2dss_sltl', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">km</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_seismic_line');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Average Spacing Parallel Interval :</label>
                                            <div class="controls wajib">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_aspi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_average_interval', array('id'=>'dav_2dss_aspi', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">km</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_average_interval');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Record Length :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
                                            						<!-- <input id="dav_2dss_rl1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_record_length_ms', array('id'=>'dav_2dss_rl1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ms</span>
                                            					</div>
                                            					<?php echo $form->error($mdlSeismic2d, 's2d_record_length_ms');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
                                            						<!-- <input id="dav_2dss_rl2" type="text" style="max-width:170px; margin-left:24px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_record_length_ft', array('id'=>'dav_2dss_rl2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
				                                                    <span class="add-on">ft</span>
                                            					</div>
                                            					<?php echo $form->error($mdlSeismic2d, 's2d_record_length_ft');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Year Of Latest Processing :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_yearlatest" class="span3" type="text"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_year_late_process', array('id'=>'dav_2dss_yearlatest', 'class'=>'span3 number-comma'));?>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_year_late_process');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Processing Method :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_lpm" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_late_method', array('id'=>'dav_2dss_lpm', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_late_method');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Image Quality :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_siq" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_img_quality', array('id'=>'dav_2dss_siq', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_img_quality');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Top Reservoir Target Depth :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class="input-prepend input-append">
                                            						<span class="add-on">MD</span>
                                            						<!-- <input id="dav_2dss_trtd1" type="text" style="max-width:157px"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_top_depth_ft', array('id'=>'dav_2dss_trtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px'));?>
				                                                    <span class="add-on">ft</span>
                                            					</div>
                                            					<?php echo $form->error($mdlSeismic2d, 's2d_top_depth_ft');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
                                            						<!-- <input id="dav_2dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_top_depth_ms', array('id'=>'dav_2dss_trtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
				                                                    <span class="add-on">ms</span>
                                            					</div>
                                            					<?php echo $form->error($mdlSeismic2d, 's2d_top_depth_ms');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Bottom Reservoir Target Depth :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class="input-prepend input-append">
                                            						<span class="add-on">MD</span>
                                            						<!-- <input id="dav_2dss_brtd1" type="text" style="max-width:157px"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_bot_depth_ft', array('id'=>'dav_2dss_brtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px'));?>
				                                                    <span class="add-on">ft</span>
                                            					</div>
                                            					<?php echo $form->error($mdlSeismic2d, 's2d_bot_depth_ft');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
                                            						<!-- <input id="dav_2dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_bot_depth_ms', array('id'=>'dav_2dss_brtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
				                                                    <span class="add-on">ms</span>
                                            					</div>
                                            					<?php echo $form->error($mdlSeismic2d, 's2d_bot_depth_ms');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_dcsp" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_spill', array('id'=>'dav_2dss_dcsp', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_depth_spill');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class="input-append">
				                                                    <!-- <input id="dav_2dss_owc1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate', array('id'=>'dav_2dss_owc1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic2d, 's2d_depth_estimate');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dlosd2m2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/> -->
                                                				<?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate_analog', array('id'=>'dlosd2m2', 'class'=>'tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                				<?php echo $form->error($mdlSeismic2d, 's2d_depth_estimate_analog');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class="input-append">
				                                                    <!-- <input id="dav_2dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate_spill', array('id'=>'dav_2dss_owc2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
				                                                    <span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic2d, 's2d_depth_estimate_spill');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil or Gas :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_2dss_oilgas1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_low', array('id'=>'dav_2dss_oilgas1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
					                                                <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic2d, 's2d_depth_low');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dlosd2n2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/> -->
                                                				<?php echo $form->textField($mdlSeismic2d, 's2d_depth_low_analog', array('id'=>'dlosd2n2', 'class'=>'tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                				<?php echo $form->error($mdlSeismic2d, 's2d_depth_low_analog');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/> -->
				                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_low_spill', array('id'=>'oilgas2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
				                                               		<span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic2d, 's2d_depth_low_spill');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Formation Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_forthickness" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_formation_thickness', array('id'=>'dav_2dss_forthickness', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                	<span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_formation_thickness');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Gross Sand or Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_gross_resthickness" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_gross_thickness', array('id'=>'dav_2dss_gross_resthickness', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                	<span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_gross_thickness');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability Net Thickness Reservoir Pay :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_antrip" class="span3" type="text" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_avail_pay', array('id'=>'dav_2dss_antrip', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic2d, 's2d_avail_pay');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Pay Zone Thickness/ Net Pay</th>
                                                <th>Net Pay Thickness</th>
<!--                                                 <th>Net Pay to Gross Sand</th> -->
                                                <th>Vsh Cut-off</th>
                                                <th>Porosity Cut-off</th>
                                                <th>Saturation Cut-Off</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_thickness', array('id'=>'s2d_net_p90_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">ft</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_thickness');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_net_p90_net_to_gross" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_vsh', array('id'=>'s2d_net_p90_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_vsh');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_por', array('id'=>'s2d_net_p90_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_por');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_satur', array('id'=>'s2d_net_p90_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_satur');?>
                                                	</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_thickness', array('id'=>'s2d_net_p50_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">ft</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_thickness');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_net_p50_net_to_gross" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_vsh', array('id'=>'s2d_net_p50_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_vsh');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_por', array('id'=>'s2d_net_p50_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_por');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_satur', array('id'=>'s2d_net_p50_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_satur');?>
                                                	</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_thickness', array('id'=>'s2d_net_p10_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">ft</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_thickness');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_net_p10_net_to_gross" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_vsh', array('id'=>'s2d_net_p10_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_vsh');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_por', array('id'=>'s2d_net_p10_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_por');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_satur', array('id'=>'s2d_net_p10_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_satur');?>
                                                	</span>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
<!--                                                 <th>Net Pay Thickness</th> -->
<!--                                                 <th>Porosity</th> -->
<!--                                                 <th>HC Saturation (1-Sw)</th> -->
<!--                                                 <th>Oil Case</th> -->
<!--                                                 <th>Gas Case</th> -->
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p90_area', array('id'=>'s2d_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_vol_p90_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p90_thickness" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p90_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p90_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p90_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p90_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p50_area', array('id'=>'s2d_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_vol_p50_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p50_thickness" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p50_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span><here> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p50_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p50_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p50_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p10_area', array('id'=>'s2d_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic2d, 's2d_vol_p10_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p10_thickness" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p10_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p10_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p10_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s2d_vol_p10_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_2dss_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlSeismic2d, 's2d_remark', array('id'=>'dav_2dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlGras" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGras','DlGras_link','col_dav3' )"/> -->
                                        <?php echo $form->checkBox($mdlGravity, 'is_checked', array('id'=>'DlGras', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlGras","DlGras_link","col_dav3")'));?> 
                                        <a id="DlGras_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3"> Gravity Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav3" class="accordion-body collapse">
                                    <div class="accordion-inner">
                        	            <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_grasur_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_year_survey', array('id'=>'dav_grasur_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition '));?>
                                                <?php echo $form->error($mdlGravity, 'sgv_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_grasur_surveymethods" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_survey_method', array('id'=>'dav_grasur_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlGravity, 'sgv_survey_method');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_sca" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_coverage_area', array('id'=>'dav_grasur_sca', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">acre</span>
                                                </div>
                                                <?php echo $form->error($mdlGravity, 'sgv_coverage_area');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range:</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_dspr" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_range', array('id'=>'dav_grasur_dspr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlGravity, 'sgv_depth_range');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_rsi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_spacing_interval', array('id'=>'dav_grasur_rsi', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlGravity, 'sgv_spacing_interval');?>
                                            </div>
                                        </div> 
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_dcsp" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_spill', array('id'=>'dav_grasur_dcsp', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlGravity, 'sgv_depth_spill');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_grasur_owc1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate', array('id'=>'dav_grasur_owc1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlGravity, 'sgv_depth_estimate');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dlosd3g2" class="span3" type="text" placeholder="By analog to"/> -->
                                                				<?php echo $form->textField($mdlGravity, 'sgv_depth_estimate_analog', array('id'=>'dlosd3g2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                				<?php echo $form->error($mdlGravity, 'sgv_depth_estimate_analog');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_grasur_owc2" type="text" style="max-width:170px; margin-left:18px;"placeholder="Estimated From Spill Point"/> -->
				                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate_spill', array('id'=>'dav_grasur_owc2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:18px;', 'placeholder'=>'Estimated From Spill Point'));?>
				                                                    <span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlGravity, 'sgv_depth_estimate_spill');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil or Gas</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_grasur_oilgas1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_low', array('id'=>'dav_grasur_oilgas1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlGravity, 'sgv_depth_low');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dlosd3h2" class="span3" type="text" placeholder="By analog to"/> -->
                                                				<?php echo $form->textField($mdlGravity, 'sgv_depth_low_analog', array('id'=>'dlosd3h2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                				<?php echo $form->error($mdlGravity, 'sgv_depth_low_analog');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_grasur_oilgas2" type="text" style="max-width:170px; margin-left:18px;" placeholder="Estimated from Spill Point"/> -->
				                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_low_spill', array('id'=>'dav_grasur_oilgas2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:18px;', 'placeholder'=>'Estimated from Spill Point'));?>
				                                                    <span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlGravity, 'sgv_depth_low_spill');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Reservoir Thickness :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_grasur_resthickness1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlGravity, 'sgv_res_thickness', array('id'=>'dav_grasur_resthickness1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlGravity, 'sgv_res_thickness');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dav_grasur_resthickness2" class="span3" type="text" placeholder="According to"/> -->
                                                				<?php echo $form->textField($mdlGravity, 'sgv_res_according', array('id'=>'dav_grasur_resthickness2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'According to'));?>
                                                				<?php echo $form->error($mdlGravity, 'sgv_res_according');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                        	<label class="control-label">Top Sand Seismic Depth :</label>
                                        	<div class="controls">
                                        		<!-- <input id="dav_grasur_resthickness3" class="span3" type="text" placeholder="Top Sand Seismic Depth" /> -->
                                        		<?php echo $form->textField($mdlGravity, 'sgv_res_top_depth', array('id'=>'dav_grasur_resthickness3', 'class'=>'span3 number'));?>
                                        		<?php echo $form->error($mdlGravity, 'sgv_res_top_depth');?>
                                        	</div>
                                        </div>
                                        <div class="control-group">
                                        	<label class="control-label">Bottom Sand Seismic Depth :</label>
                                        	<div class="controls">
                                        		<!-- <input id="dav_grasur_resthickness4" class="span3" type="text" placeholder="Bottom Sand Seismic Depth"/> -->
                                        		<?php echo $form->textField($mdlGravity, 'sgv_res_bot_depth', array('id'=>'dav_grasur_resthickness4', 'class'=>'span3 number'));?>
                                        		<?php echo $form->error($mdlGravity, 'sgv_res_bot_depth');?>
                                        	</div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Gross Sand Thickness :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_grasur_gst1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlGravity, 'sgv_gross_thickness', array('id'=>'dav_grasur_gst1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlGravity, 'sgv_gross_thickness');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dav_grasur_gst2" class="span3" type="text" placeholder="According to"/> -->
                                                				<?php echo $form->textField($mdlGravity, 'sgv_gross_according', array('id'=>'dav_grasur_gst2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'According to'));?>
                                                				<?php echo $form->error($mdlGravity, 'sgv_gross_according');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                       	<div class="control-group">
                                        	<label class="control-label">According to Well Name :</label>
                                        	<div class="controls">
                                        		<!-- <input id="dav_grasur_gst3" class="span6" type="text" placeholder="Well Name"/> -->
                                        		<?php echo $form->textField($mdlGravity, 'sgv_gross_well', array('id'=>'dav_grasur_gst3', 'class'=>'span6'));?>
                                        		<?php echo $form->error($mdlGravity, 'sgv_gross_well');?>
                                        	</div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Net To Gross (Net Pay/Gross Sand) :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_netogross" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_net_gross', array('id'=>'dav_grasur_netogross', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">%</span>
                                                </div>
                                                <?php echo $form->error($mdlGravity, 'sgv_net_gross');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Areal Closure Estimation</th>
<!--                                                     <th>Net Pay Thickness</th> -->
<!--                                                     <th>Porosity</th> -->
<!--                                                     <th>HC Saturation (1-Sw)</th> -->
<!--                                                     <th>Oil Case</th> -->
<!--                                                     <th>Gas Case</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p90_area', array('id'=>'sgv_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlGravity, 'sgv_vol_p90_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p90_thickness" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p90_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p90_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p90_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p90_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p50_area', array('id'=>'sgv_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlGravity, 'sgv_vol_p50_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p50_thickness" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p50_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p50_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p50_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p50_gas" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p10_area', array('id'=>'sgv_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlGravity, 'sgv_vol_p10_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p10_thickness" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p10_porosity" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p10_sw" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p10_oil" name="custom" class="span6" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sgv_vol_p10_gas" name="custom" class="span6" type="text" readonly/> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_grasur_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGravity, 'sgv_remark', array('id'=>'dav_grasur_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlGeos" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGeos','DlGeos_link','col_dav4' )"/> -->
                                        <?php echo $form->checkBox($mdlGeochemistry, 'is_checked', array('id'=>'DlGeos', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlGeos","DlGeos_link","col_dav4")'));?>
                                        <a id="DlGeos_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4"> Geochemistry Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav4" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_geo_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_year_survey', array('id'=>'dav_geo_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Interval Samples Range :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_geo_isr" type="text" style="max-width:170px;"/>-->
                                                    <?php echo $form->textField($mdlGeochemistry, 'sgc_sample_interval', array('id'=>'dav_geo_isr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_sample_interval');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Sample Location :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_numsample" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_sample', array('id'=>'dav_geo_numsample', 'class'=>'span3 number'));?>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_number_sample');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Rocks Sample :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_numrock" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_rock', array('id'=>'dav_geo_numrock', 'class'=>'span3 number'));?>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_number_rock');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Fluid Sample :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_numfluid" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_fluid', array('id'=>'dav_geo_numfluid', 'class'=>'span3 number'));?>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_number_fluid');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability HC Composition :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_ahc" class="span3" style="text-align: center;" />-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_avail_hc', array('id'=>'dav_geo_ahc', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_avail_hc');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Hydrocarbon Composition : </label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_hycomp" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_hc_composition', array('id'=>'dav_geo_hycomp', 'class'=>'span3'));?>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_hc_composition');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Laboratorium Evaluation & Report Year</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_lery" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" placeholder="angka,koma"/> -->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_lab_report', array('id'=>'dav_geo_lery', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlGeochemistry, 'sgc_lab_report');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
<!--                                                 <th>Net Pay Thickness</th> -->
<!--                                                 <th>Porosity</th> -->
<!--                                                 <th>HC Saturation (1-Sw)</th> -->
<!--                                                 <th>Oil Case</th> -->
<!--                                                 <th>Gas Case</th> -->
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p90_area', array('id'=>'sgc_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlGeochemistry, 'sgc_vol_p90_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p90_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p90_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p90_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p90_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p90_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p50_area', array('id'=>'sgc_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlGeochemistry, 'sgc_vol_p50_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p50_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p50_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p50_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p50_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p50_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p10_area', array('id'=>'sgc_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlGeochemistry, 'sgc_vol_p10_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p10_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p10_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p10_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p10_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sgc_vol_p10_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_geo_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGeochemistry, 'sgc_remark', array('id'=>'dav_geo_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlEs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlEs','DlEs_link','col_dav5' )"/> --> 
                                        <?php echo $form->checkBox($mdlElectromagnetic, 'is_checked', array('id'=>'DlEs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlEs","DlEs_link","col_dav5")'));?>
                                        <a id="DlEs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav5"> Electromagnetic Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav5" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_elec_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlElectromagnetic, 'sel_year_survey', array('id'=>'dav_elec_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlElectromagnetic, 'sel_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_elec_surveymethods" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlElectromagnetic, 'sel_survey_method', array('id'=>'dav_elec_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlElectromagnetic, 'sel_survey_method');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_elec_sca" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_coverage_area', array('id'=>'dav_elec_sca', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">acre</span>
                                                </div>
                                                <?php echo $form->error($mdlElectromagnetic, 'sel_coverage_area');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <!-- <input id="dav_elec_dspr" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_depth_range', array('id'=>'dav_elec_dspr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlElectromagnetic, 'sel_depth_range');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <!-- <input id="dav_elec_rsi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_spacing_interval', array('id'=>'dav_elec_rsi', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlElectromagnetic, 'sel_spacing_interval');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Area Closure Estimation</th>
<!--                                                 <th>Net Pay Thickness</th> -->
<!--                                                 <th>Porosity</th> -->
<!--                                                 <th>HC Saturation(1-Sw)</th> -->
<!--                                                 <th>Oil Case</th> -->
<!--                                                 <th>Gas Case</th> -->
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p90_area', array('id'=>'sel_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlElectromagnetic, 'sel_vol_p90_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p90_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p90_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p90_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p90_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p90_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p50_area', array('id'=>'sel_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlElectromagnetic, 'sel_vol_p50_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p50_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p50_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p50_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p50_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p50_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p10_area', array('id'=>'sel_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlElectromagnetic, 'sel_vol_p10_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p10_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p10_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p10_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p10_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="sel_vol_p10_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_elec_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlElectromagnetic, 'sel_remark', array('id'=>'dav_elec_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlRs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlRs','DlRs_link','col_dav6' )"/> -->
                                        <?php echo $form->checkBox($mdlResistivity, 'is_checked', array('id'=>'DlRs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlRs","DlRs_link","col_dav6")'));?>
                                        <a id="DlRs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav6"> Resistivity Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav6" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_res_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlResistivity, 'rst_year_survey', array('id'=>'dav_res_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlResistivity, 'rst_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_res_surveymethods" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlResistivity, 'rst_survey_method', array('id'=>'dav_res_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlResistivity, 'rst_survey_method');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_res_sca" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlResistivity, 'rst_coverage_area', array('id'=>'dav_res_sca', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">acre</span>
                                                </div>
                                                <?php echo $form->error($mdlResistivity, 'rst_coverage_area');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_res_dspr" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlResistivity, 'rst_depth_range', array('id'=>'dav_res_dspr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlResistivity, 'rst_depth_range');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_res_rsi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlResistivity, 'rst_spacing_interval', array('id'=>'dav_res_rsi', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlResistivity, 'rst_spacing_interval');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Area Closure Estimation</th>
<!--                                                     <th>Net Pay Thickness</th> -->
<!--                                                     <th>Porosity</th> -->
<!--                                                     <th>HC Saturation (1-Sw)</th> -->
<!--                                                     <th>Oil Case</th> -->
<!--                                                     <th>Gas Case</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p90_area', array('id'=>'rst_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlResistivity, 'rst_vol_p90_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p90_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p90_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p90_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p90_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p90_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p50_area', array('id'=>'rst_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlResistivity, 'rst_vol_p50_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p50_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p50_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p50_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p50_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p50_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p10_area', array('id'=>'rst_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlResistivity, 'rst_vol_p10_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p10_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p10_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p10_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p10_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="rst_vol_p10_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     </td> -->
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_res_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlResistivity, 'rst_remark', array('id'=>'dav_res_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlOs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlOs','DlOs_link','col_dav7' )"/> -->
                                        <?php echo $form->checkBox($mdlOther, 'is_checked', array('id'=>'DlOs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlOs","DlOs_link","col_dav7")'));?>
                                        <a id="DlOs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav7"> Other Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav7" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_other_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlOther, 'sor_year_survey', array('id'=>'dav_other_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlOther, 'sor_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Area Clossure Estimation</th>
<!--                                                     <th>Net Pay Thickness</th> -->
<!--                                                     <th>Porosity</th> -->
<!--                                                     <th>HC Saturation (1-Sw)</th> -->
<!--                                                     <th>Oil Case</th> -->
<!--                                                     <th>Gas Case</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p90_area', array('id'=>'sor_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlOther, 'sor_vol_p90_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p90_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p90_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p90_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p90_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p90_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p50_area', array('id'=>'sor_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlOther, 'sor_vol_p50_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p50_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p50_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p50_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p50_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p50_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td>
                                                    	<span class="wajib">
	                                                    	<div class=" input-append">
	                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p10_area', array('id'=>'sor_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                    		<span class="add-on">acre</span>
	                                                    	</div>
	                                                    	<?php echo $form->error($mdlOther, 'sor_vol_p10_area');?>
                                                    	</span>
                                                    </td>
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p10_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">ft</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p10_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p10_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">%</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p10_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">MMBO</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
<!--                                                     <td> -->
<!--                                                     	<div class=" input-append"> -->
<!--                                                     		<input id="sor_vol_p10_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                     	<span class="add-on">BCF</span> -->
<!--                                                     	</div> -->
<!--                                                     </td> -->
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_other_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlOther, 'sor_remark', array('id'=>'dav_other_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="DlDss3" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss3','DlDss3_link','col_dav8' )"/> -->
                                        <?php echo $form->checkBox($mdlSeismic3d, 'is_checked', array('id'=>'DlDss3', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlDss3","DlDss3_link","col_dav8")'));?> 
                                        <a id="DlDss3_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav8"> Survey Seismic 3D</a>
                                    </div>
                                </div>
                                <div id="col_dav8" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls wajib">
                                                <!-- <input id="dav_3dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_year_survey', array('id'=>'dav_3dss_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_year_survey');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of 3D Vintage :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_numvin" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_vintage_number', array('id'=>'dav_3dss_numvin', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_vintage_number');?>
                                            </div>
                                        </div>                                        
                                        <div class="control-group">
                                            <label class="control-label">Bin Size :</label>
                                            <div class="controls">
                                            	<div class=" input-append">
	                                                <!-- <input id="dav_3dss_binsize" type="text" class="span3" /> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_bin_size', array('id'=>'dav_3dss_binsize', 'class'=>'number', 'style'=>'max-width:130px;'));?>
	                                                <span class="add-on">km<sup>2</sup></span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_bin_size');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Coverage Area : </label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_tca" type="text" style="max-width:160px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_coverage_area', array('id'=>'dav_3dss_tca', 'class'=>'number', 'style'=>'max-width:130px;'))?>
                                                    <span class="add-on">km<sup>2</sup></span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_coverage_area');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Dominant Frequency at Reservoir Target:</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_dfrt1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_frequency', array('id'=>'dav_3dss_dfrt1', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">hz</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_frequency');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Lateral Seismic Resolution :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_dfrt2" type="text" class="span3" placeholder="Lateral Seismic Resolution"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_frequency_lateral', array('id'=>'dav_3dss_dfrt2', 'class'=>'span3 number'));?>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_frequency_lateral');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Vertical Seismic Resolution :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_dfrt3" type="text" class="span3" placeholder="Vertical Seismic Resolution"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_frequency_vertical', array('id'=>'dav_3dss_dfrt3', 'class'=>'span3 number'));?>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_frequency_vertical');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Processing Year:</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_lpy" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Latest Processing Year"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_year_late_process', array('id'=>'dav_3dss_lpy', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Latest Processing Year'));?>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_year_late_process');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Proccessing Method :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_lpm" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_late_method', array('id'=>'dav_3dss_lpm', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_late_method');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Image Quality :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_siq" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_img_quality', array('id'=>'dav_3dss_siq', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_img_quality');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Top Reservoir Target Depth :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class="input-prepend input-append">
                                            						<span class="add-on">MD</span>
				                                                    <!-- <input id="dav_3dss_trtd1" type="text" style="max-width:155px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ft', array('id'=>'dav_3dss_trtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_top_depth_ft');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_3dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ms', array('id'=>'dav_3dss_trtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
				                                                    <span class="add-on">ms</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_top_depth_ms');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Bottom Reservoir Target Depth :</label>
                                            <div class="controls">
                                            	<table>
	                                            	 <tr>
	                                            	 	<td>
	                                            	 		<span>
	                                            	 			<div class="input-prepend input-append">
	                                            	 				<span class="add-on">MD</span>
				                                                    <!-- <input id="dav_3dss_brtd1" type="text" style="max-width:155px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_bot_depth_ft', array('id'=>'dav_3dss_brtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_bot_depth_ft');?>
	                                            	 		</span>
	                                            	 	</td>
	                                            	 	<td>
	                                            	 		<span>
	                                            	 			<div class=" input-append">
				                                                    <!-- <input id="dav_3dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_bot_depth_ms', array('id'=>'dav_3dss_brtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
				                                                    <span class="add-on">ms</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_bot_depth_ms');?>
	                                            	 		</span>
	                                            	 	</td>
	                                            	 </tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_dcsp" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_spill', array('id'=>'dav_3dss_dcsp', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_depth_spill');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Distinguish Wave Propagation by Offset :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_adwpo" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_offset', array('id'=>'dav_3dss_adwpo', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_ability_offset');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Resolve Reservoir Rock Property :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_ar3p" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_rock', array('id'=>'dav_3dss_ar3p', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_ability_rock');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Resolve Reservoir Fluid Property :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_arrfp" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_fluid', array('id'=>'dav_3dss_arrfp', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_ability_fluid');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_3dss_owc1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate', array('id'=>'dav_3dss_owc1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_depth_estimate');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dlosd8n2" class="span3" type="text" placeholder="By analog to" /> -->
                                                				<?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate_analog', array('id'=>'dlosd8n2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                				<?php echo $form->error($mdlSeismic3d, 's3d_depth_estimate_analog');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_3dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" /> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate_spill', array('id'=>'dav_3dss_owc2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
				                                                    <span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_depth_estimate_spill');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil/Gas :</label>
                                            <div class="controls">
                                            	<table>
                                            		<tr>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_3dss_oilgas1" type="text" style="max-width:170px;"/> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low', array('id'=>'dav_3dss_oilgas1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px;'));?>
				                                                    <span class="add-on">ft</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_depth_low');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<!-- <input id="dlosd8o2" class="span3" type="text" placeholder="By analog to" /> -->
                                                				<?php echo $form->textField($mdlSeismic3d, 's3d_depth_low_analog', array('id'=>'dlosd8o2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width:165px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                				<?php echo $form->error($mdlSeismic3d, 's3d_depth_low_analog');?>
                                            				</span>
                                            			</td>
                                            			<td>
                                            				<span>
                                            					<div class=" input-append">
				                                                    <!-- <input id="dav_3dss_oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" /> -->
				                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low_spill', array('id'=>'dav_3dss_oilgas2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
				                                                    <span class="add-on">%</span>
				                                                </div>
				                                                <?php echo $form->error($mdlSeismic3d, 's3d_depth_low_spill');?>
                                            				</span>
                                            			</td>
                                            		</tr>
                                            	</table>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Formation Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_forthickness" type="text" style="max-width:170px;" /> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_formation_thickness', array('id'=>'dav_3dss_forthickness', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_formation_thickness');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Grass Sand or Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_grass_resthickness" type="text" style="max-width:170px;" /> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_gross_thickness', array('id'=>'dav_3dss_grass_resthickness', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_gross_thickness');?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability Net Thickness Reservoir Pay :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_antrip" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_avail_pay', array('id'=>'dav_3dss_antrip', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                <?php echo $form->error($mdlSeismic3d, 's3d_avail_pay');?>
                                            </div>
                                        </div>  
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Pay Zone Thickness / Net Pay</th>
                                                <th>Thickness Net Pay</th>
<!--                                                 <th>NTG(Net Pay to Gross Sand)</th> -->
                                                <th>Porosity Cut Off</th>
                                                <th>Vsh Cut Off</th>
                                                <th>Saturation Cut Off</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_thickness', array('id'=>'s3d_net_p90_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">ft</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_thickness');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_net_p90_net_to_gross" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 		<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_por', array('id'=>'s3d_net_p90_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_por');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_vsh', array('id'=>'s3d_net_p90_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_vsh');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_satur', array('id'=>'s3d_net_p90_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_satur');?>
                                                	</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_thickness', array('id'=>'s3d_net_p50_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">ft</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_thickness');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_net_p50_net_to_gross" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 		<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_por', array('id'=>'s3d_net_p50_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_por');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_vsh', array('id'=>'s3d_net_p50_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_vsh');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_satur', array('id'=>'s3d_net_p50_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_satur');?>
                                                	</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_thickness', array('id'=>'s3d_net_p10_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">ft</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_thickness');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_net_p10_net_to_gross" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 		<span class="add-on">%</span> -->
<!--                                                 	</div> -->
<!--                                                 </td> -->
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_por', array('id'=>'s3d_net_p10_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_por');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_vsh', array('id'=>'s3d_net_p10_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>                                                		
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_vsh');?>
                                                	</span>
                                                </td>
                                                <td>
                                                	<span>
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_satur', array('id'=>'s3d_net_p10_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">%</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_satur');?>
                                                	</span>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
<!--                                                 <th>Net Pay Thickness</th> -->
<!--                                                 <th>Porosity</th> -->
<!--                                                 <th>HC Saturation (1-Sw)</th> -->
<!--                                                 <th>Oil Case</th> -->
<!--                                                 <th>Gas Case</th> -->
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p90_area', array('id'=>'s3d_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_vol_p90_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p90_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p90_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p90_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p90_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p90_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p50_area', array('id'=>'s3d_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_vol_p50_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p50_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p50_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p50_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p50_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p50_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 </td> -->
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<span class="wajib">
	                                                	<div class=" input-append">
	                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
	                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p10_area', array('id'=>'s3d_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                		<span class="add-on">acre</span>
	                                                	</div>
	                                                	<?php echo $form->error($mdlSeismic3d, 's3d_vol_p10_area');?>
                                                	</span>
                                                </td>
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p10_thickness" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">ft</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p10_porosity" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p10_sw" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">%</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p10_oil" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">MMBO</span> -->
<!--                                                 </td> -->
<!--                                                 <td> -->
<!--                                                 	<div class=" input-append"> -->
<!--                                                 		<input id="s3d_vol_p10_gas" class="span6" name="custom" type="text" readonly /> -->
<!--                                                 	<span class="add-on">BCF</span> -->
<!--                                                 </td> -->
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_3dss_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlSeismic3d, 's3d_remark', array('id'=>'dav_3dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                <!-- END OCCURANCE SURVEY DESCRIPTION-->                
            	</div>
        	</div>
        </div>
        
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN LEAD GCF -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> DRILLABLE PROSPECT GEOLOGICAL CHANCE FACTOR</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">×</button>
                                    <strong>Drillable Prospect Geological Chance Factor</strong>
                                    <p>When Play chosen this Geological Chance Factor automatically filled with Play Geological Chance Factor, change according to newest Geological Change Factor or leave as it is.</p>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="accordion"> 
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Source Rock :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="gcfsrock" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_sr', array('id'=>'gcfsrock', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_sr');?>
                                                    <?php echo $form->hiddenField($mdlGcf, 'gcf_id_reference', array('id'=>'gcf_id_reference'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input id="gcfsrock1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_age_system', array('id'=>'gcfsrock1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input id="gcfsrock1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_age_serie', array('id'=>'gcfsrock1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input id="gcfsrock2" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_formation', array('id'=>'sourceformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_formation');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<?php echo $form->textField($mdlGcf, 'gcf_sr_formation_serie', array('id'=>'gcf_sr_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_sr_formation_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_kerogen', array('id'=>'gcfsrock3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_ker" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_kerogen');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_toc', array('id'=>'gcfsrock4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_toc" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_toc');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_hfu', array('id'=>'gcfsrock5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_hfu" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_hfu');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_distribution', array('id'=>'gcfsrock6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_continuity', array('id'=>'gcfsrock7', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_maturity', array('id'=>'gcfsrock8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_mat" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_maturity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_otr', array('id'=>'gcfsrock9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_otr" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_otr');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Source Rock:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_sr_remark', array('id'=>'gcfsrock10', 'class'=>'span3', 'row'=>'2', 'style'=>'text-align: left;'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Reservoir :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfres" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_res', array('id'=>'gcfres', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_res');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_age_system', array('id'=>'gcfres1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_age_serie', array('id'=>'gcfres1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_formation', array('id'=>'reservoirformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_formation');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div class="wajib">
                                                						<?php echo $form->textField($mdlGcf, 'gcf_res_formation_serie', array('id'=>'gcf_res_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_res_formation_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_set', array('id'=>'gcfres3', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_set');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_env', array('id'=>'gcfres4', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_env');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_distribution', array('id'=>'gcfres5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_dis" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_continuity', array('id'=>'gcfres6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_lithology', array('id'=>'gcfres7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_lit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_lithology');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_type', array('id'=>'gcfres8', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_type');?>
                                                </div>
                                            </div>
                                            <!-- <div id="prim_pro" class="control-group"> -->
                                                <!-- <label class="control-label">Average Primary Porosity Reservoir :</label> -->
                                                <!-- <div class="controls"> -->
                                                    <!-- <p style="margin-bottom: 2px;"> -->
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php //echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3', 'style'=>'text-align: center; max-width: 155px; position: relative; display: inline-block;'));?>
                                                    <!-- <span class="addkm">%</span> -->
                                                    <!-- </p> -->
                                                    <?php //echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                <!-- </div> -->
                                            <!-- </div> -->
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Average Primary Porosity Reservoir % :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_pri" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_secondary', array('id'=>'gcfres10', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_sec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_secondary');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Reservoir :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfres11" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_res_remark', array('id'=>'gcfres11', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Trap :</strong></label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_trap', array('id'=>'gcftrap', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_trap');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_system', array('id'=>'gcftrap1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_serie', array('id'=>'gcftrap1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" /> -->
                                                						<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation', array('id'=>'sealingformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation_serie', array('id'=>'gcf_trap_seal_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_distribution', array('id'=>'gcftrap3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_sdi" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_continuity', array('id'=>'gcftrap4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_scn" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_type', array('id'=>'gcftrap5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_stp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_type');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_age_system', array('id'=>'gcftrap6', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_age_serie', array('id'=>'gcftrap6a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_geometry', array('id'=>'gcftrap7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_geo" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_geometry');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_type', array('id'=>'gcftrap8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_trp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_type');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_closure', array('id'=>'gcftrap9', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_closure');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Trap:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcftrap10"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_trap_remark', array('id'=>'gcftrap10', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Dynamic :</strong></label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_dyn', array('id'=>'gcfdyn', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_dyn');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration', array('id'=>'gcfdyn1', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_migration');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_kitchen', array('id'=>'gcfdyn2', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_kit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_kitchen');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_petroleum', array('id'=>'gcfdyn3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_tec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_petroleum');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" /> -->
                                                						<?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_system', array('id'=>'gcfdyn4', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_serie', array('id'=>'gcfdyn4a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_system', array('id'=>'gcfdyn5', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_serie', array('id'=>'gcfdyn5a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_preservation', array('id'=>'gcfdyn6', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_prv" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_preservation');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_pathways', array('id'=>'gcfdyn7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_mig" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_pathways');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_system', array('id'=>'gcfdyn8', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_serie', array('id'=>'gcfdyn8a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Dynamic:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfdyn9"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_dyn_remark', array('id'=>'gcfdyn9', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- BEGIN LEAD GCF -->
            </div>
        </div>
	    <?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
	    <div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
		    <?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/createdrillable'), array(
	        	'type'=>'POST',
	        	'dataType'=>'json',
	        	'beforeSend'=>'function(data) {
                    $("#yt0").prop("disabled", true);
	        	}',
	        	'success'=>'js:function(data) {
		    		$(".tooltips").attr("data-original-title", "");
	        		
	        		$(".has-err").removeClass("has-err");
	        		$(".errorMessage").hide();
		    		
	        		if(data.result === "success") {
		    			$(".close").addClass("redirect");
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
	        			$("#pesan").hide();
	        			$.fn.yiiGridView.update("drillable-grid");
	        			$(".redirect").click( function () {
							var redirect = "' . Yii::app()->createUrl('/Kkks/createdrillable') . '";
							window.location=redirect;
						});
                        $("#yt0").prop("disabled", false);
	        		} else {
		    			var myArray = JSON.parse(data.err);
	        			$.each(myArray, function(key, val) {
	        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
			        		{
	        					$("#createdrillable-form #"+key+"_em_").parent().addClass("has-err");
	        					$("#createdrillable-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
	        					
			        		} else {
	        					$("#createdrillable-form #"+key+"_em_").text(val);                                                    
								$("#createdrillable-form #"+key+"_em_").show();
		        				$("#createdrillable-form #"+key+"_em_").parent().addClass("has-err");
	        				}
	        			});
		    		
		    		
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
                        $("#yt0").prop("disabled", false);
	        		}
	        	}',
	        ),
	        array('class'=>'btn btn-inverse')
	        );?>
        </div>
	    <?php //echo CHtml::button('Get', array('onclick'=>'{getPreviousDataDrillable();}'));?>
	    <?php $this->endWidget();?>
	    
	    <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->
	</div>
</div>

<?php 
	Yii::app()->clientScript->registerScript('ambil-gcf', "

		var blank_lithology = '',
            blank_depos_env = '',
            blank_formation_serie = '',
            blank_age_serie = '',
            blank_trap_type = '';
        
        function generate_prospect_name() {
            $('#gcfres7').select2('val') == 'Unknown' ? blank_lithology = '' : blank_lithology = $('#gcfres7').select2('val');
            $('#gcfres4').select2('val') == 'Others' ? blank_depos_env = '' : blank_depos_env = $('#gcfres4').select2('val');
            $('#gcf_res_formation_serie').select2('val') == 'Not Available' ? blank_formation_serie = '' : blank_formation_serie = $('#gcf_res_formation_serie').select2('val');
            $('#gcfres1a').select2('val') == 'Not Available' ? blank_age_serie = '' : blank_age_serie = $('#gcfres1a').select2('val');
            $('#gcftrap8').select2('val') == 'Unknown' ? blank_trap_type = '' : blank_trap_type = $('#gcftrap8').select2('val');
        
            $('#additional_lead_name').text($('#structure_name').val() + '; ' + blank_lithology + ' - ' + blank_formation_serie + ' ' + $('#reservoirformationname').select2('val') + ' - ' + blank_age_serie + ' ' + $('#gcfres1').select2('val') + ' - ' + blank_depos_env + ' - ' + blank_trap_type);
        };
		
		$('#structure_name').on('keyup', function() {
			generate_prospect_name();
		});

        function gcfAttr() {
            var attrGcf = [
                '#gcfsrock3', '#gcfsrock4', '#gcfsrock5', '#gcfsrock8', '#gcfsrock9',
                '#gcfres5', '#gcfres7', '#gcfres10', '#gcfres9',
                '#gcftrap3', '#gcftrap4', '#gcftrap5', '#gcftrap7', '#gcftrap8',
                '#gcfdyn2', '#gcfdyn3', '#gcfdyn6', '#gcfdyn7',
            ];

            for(var loop = 0; loop < attrGcf.length; loop++) {
                $.ajax({
                    url: '" . Yii::app()->createUrl('/Kkks/getnilai') . "',
                    type: 'POST',
                    data: {
                        name: $(attrGcf[loop]).attr('id'),
                        choosen: $(attrGcf[loop]).val(),
                        from: 'drillable',
                        category: $(gcfCategory).val() //category proven atau analog
                    },
                    dataType: 'json',
                    error: function(xhr) {
                        console.log(xhr.status);
                    },
                    success: function(e) {
                        $(e.state).text(e.value);
                    }
                });
            }
        }
		
		$('#plyName').on('change', function() {
			var value = $('#plyName').val();
			if(value != '') {
				$.ajax({
					url : '" . Yii::app()->createUrl('/Kkks/getGcf') . "',
					data : {
						play_id : value,
					},
					type : 'POST',
					dataType : 'json',
					success : function(data) {
                		$('#gcf_id_reference').val(data.gcf_id);
						$('#gcfsrock').select2('val', data.gcf_is_sr);
						$('#gcfsrock1').select2('val', data.gcf_sr_age_system);
						$('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
						$('#sourceformationname').select2('val', data.gcf_sr_formation);
                		$('#gcf_sr_formation_serie').select2('val', data.gcf_sr_formation_serie);
						$('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
						$('#gcfsrock4').select2('val', data.gcf_sr_toc);
						$('#gcfsrock5').select2('val', data.gcf_sr_hfu);
						$('#gcfsrock6').select2('val', data.gcf_sr_distribution);
						$('#gcfsrock7').select2('val', data.gcf_sr_continuity);
						$('#gcfsrock8').select2('val', data.gcf_sr_maturity);
						$('#gcfsrock9').select2('val', data.gcf_sr_otr);
						$('#gcfsrock10').val(data.gcf_sr_remark);
						$('#gcfres').select2('val', data.gcf_is_res);
						$('#gcfres1').select2('val', data.gcf_res_age_system);
						$('#gcfres1a').select2('val', data.gcf_res_age_serie);
						$('#reservoirformationname').select2('val', data.gcf_res_formation);
                		$('#gcf_res_formation_serie').select2('val', data.gcf_res_formation_serie);
						$('#gcfres3').select2('val', data.gcf_res_depos_set);
						$('#gcfres4').select2('val', data.gcf_res_depos_env);
						$('#gcfres5').select2('val', data.gcf_res_distribution);
						$('#gcfres6').select2('val', data.gcf_res_continuity);		
						$('#gcfres7').select2('val', data.gcf_res_lithology);
						$('#gcfres8').select2('val', data.gcf_res_por_type);
						$('#gcfres9').select2('val', data.gcf_res_por_primary);
						$('#gcfres10').select2('val', data.gcf_res_por_secondary);
						$('#gcfres11').val(data.gcf_res_remark);
						$('#gcftrap').select2('val', data.gcf_is_trap);
						$('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
						$('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
						$('#sealingformationname').select2('val', data.gcf_trap_seal_formation);
                		$('#gcf_trap_seal_formation_serie').select2('val', data.gcf_trap_seal_formation_serie);
						$('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
						$('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
						$('#gcftrap5').select2('val', data.gcf_trap_seal_type);
						$('#gcftrap6').select2('val', data.gcf_trap_age_system);
						$('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
						$('#gcftrap7').select2('val', data.gcf_trap_geometry);
						$('#gcftrap8').select2('val', data.gcf_trap_type);
						$('#gcftrap9').select2('val', data.gcf_trap_closure);
						$('#gcftrap10').val(data.gcf_trap_remark);
						$('#gcfdyn').select2('val', data.gcf_is_dyn);
						$('#gcfdyn1').select2('val', data.gcf_dyn_migration);
						$('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
						$('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
						$('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
						$('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
						$('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
						$('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
						$('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
						$('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
						$('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
						$('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
						$('#gcfdyn9').val(data.gcf_dyn_remark);
						disabledElement('gcfsrock');
						disabledElement('gcfres');
						disabledElement('gcftrap');
						disabledElement('gcfdyn');
						disabledElement('gcfres8');
						disable('gcfsrock');
						disable('gcfres');
						disable('gcftrap');
                		generate_prospect_name();
                        gcfAttr();
					},
				});
			} else {
    			$('#gcfsrock').select2('val', '');
    			disabledElement('gcfsrock');
    			disable('gcfsrock');
    			$('#gcfsrock10').val('');
    		
    			$('#gcfres').select2('val', '');
    			disabledElement('gcfres');
                disable('gcfres');
    			$('#gcfres11').val('');
    			$('#gcfres1').select2('val', '');
                $('#gcfres1a').select2('val', '');
    			$('#reservoirformationname').select2('val', '');
                $('#gcf_res_formation_serie').select2('val', '');
    			$('#gcfres4').select2('val', '');
    			$('#gcfres7').select2('val', '');
    		
    			$('#gcftrap').select2('val', '');
    			disabledElement('gcftrap');
                disable('gcftrap');
    			$('#gcftrap10').val('');
    			$('#gcftrap8').select2('val', '');
    			
    			$('#gcfdyn').select2('val', '');
    			disabledElement('gcfdyn');
    			$('#gcfdyn9').val('');
                disabledElement('gcfres8');
                generate_prospect_name();
                gcfAttr();
    		}
		});
	");
?>

<script type="text/javascript">
/* Memanggil nilai gcf untuk ditampilkan */
var sr = ['gcfsrock3', 'gcfsrock4', 'gcfsrock5', 'gcfsrock8', 'gcfsrock9'];
var res = ['gcfres5', 'gcfres7', 'gcfres9', 'gcfres10'];
var trp = ['gcftrap3', 'gcftrap4', 'gcftrap5', 'gcftrap7', 'gcftrap8'];
var dyn = ['gcfdyn2', 'gcfdyn3', 'gcfdyn6', 'gcfdyn7'];

var gcfCategory = ''; 

$('.gcf').each(function() {
    $(this).bind('change', function() {
        if($.inArray($(this).attr("id"), sr) != -1)
            gcfCategory = '#gcfsrock';
        else if($.inArray($(this).attr("id"), res) != -1)
            gcfCategory = '#gcfres';
        else if($.inArray($(this).attr("id"), trp) != -1)
            gcfCategory = '#gcftrap';
        else
            gcfCategory = '#gcfdyn';

        $.ajax({
            url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
            type: 'POST',
            data: {
                "name": $(this).attr("id"),
                "choosen": $(this).val(),
                "from": "drillable",
                "category": $(gcfCategory).val() //category proven atau analog
            },
            dataType: 'json',
            error: function(xhr) {
                console.log(xhr.status);
            },
            success: function(e) {
                $(e.state).text(e.value);
            }
        });
    }).change();
});

$('#gcfsrock').bind('change', function() {
    if($("#gcfsrock").val() != '') {
        for(var loop = 0; loop < sr.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": sr[loop],
                    "choosen": $('#'+sr[loop]).val(),
                    "from": "drillable",
                    "category": $("#gcfsrock").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#gcfres').bind('change', function() {
    if($('#gcfres').val() != '') {
        for(var loop = 0; loop < res.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": res[loop],
                    "choosen": $('#'+res[loop]).val(),
                    "from": "drillable",
                    "category": $("#gcfres").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#gcftrap').bind('change', function() {
  if($('#gcftrap').val() != '') {
    for(var loop = 0; loop < trp.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": trp[loop],
          "choosen": $('#'+trp[loop]).val(),
          "from": "drillable",
          "category": $("#gcftrap").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();

$('#gcfdyn').bind('change', function() {
  if($('#gcfdyn').val() != '') {
    for(var loop = 0; loop < dyn.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": dyn[loop],
          "choosen": $('#'+dyn[loop]).val(),
          "from": "drillable",
          "category": $("#gcfdyn").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();
/* end Memanggil nilai gcf untuk ditampilkan */

function getPreviousDataDrillable()
{
	<?php echo CHtml::ajax(array(
		'url'=>Yii::app()->createUrl('Kkks/getpreviousdatadrillable'),
		'data'=>'',
		'type'=>'POST',
		'dataType'=>'json',
		'success'=>"js:function(data) {
			if(data.error === false) {
				
			}
		}",
	));
	?>
}
</script>

<?php 
Yii::app()->clientScript->registerScript('coldrill', "
//calculate geological (6 parameter) 
function calculateSurveyDrillable(area, thickness, net, porosity, sw, attrId) {
	var areas = (area != '') ? parseFloat(area) : 0;
	var thicknesses = (thickness != '') ? parseFloat(thickness) : 0;
	var nets = (net != '') ? parseFloat(net) : 0;
	var porosities = (porosity != '') ? parseFloat(porosity) : 0;
	var swes = (sw != '') ? parseFloat(sw) : 0;
	
	var oil_case = 7758 * areas * ( thicknesses * nets ) * (porosities / 100) * (swes / 100) / 1000000;
	var gas_case = 43560 * areas * ( thicknesses * nets ) * (porosities / 100) * (swes / 100) / 1000000000;
	
	$(attrId + 'oil').val(oil_case.toFixed(2));
	$(attrId + 'gas').val(gas_case.toFixed(2));
}
//end calculate geological (6 parameter)

//calculate s2d (5 parameter)
function calculateSurveysDrillable(area, netThickness, porosity, sw, attrId) {
	var areas = (area != '') ? parseFloat(area) : 0;
	var netThicknesses = (netThickness != '') ? parseFloat(netThickness) : 0;
	var porosities = (porosity != '') ? parseFloat(porosity) : 0;
	var swes = (sw != '') ? parseFloat(sw) : 0;
	
	var oil_case = 7758 * areas * ( netThicknesses ) * (porosities / 100) * (swes / 100) / 1000000;
	var gas_case = 43560 * areas * ( netThicknesses  ) * (porosities / 100) * (swes / 100) / 1000000000;
	
	$(attrId + 'oil').val(oil_case.toFixed(2));
	$(attrId + 'gas').val(gas_case.toFixed(2));
}
//end calculate s2d (5 parameter)

function calculateNetToGrossDrillable(netThickness, grossThickness, attrId) {
	var netThicknesses = (netThickness != '') ? parseFloat(netThickness) : 0;
	var grossThicknesses = (grossThickness != '') ? parseFloat(grossThickness) : 0;
	var netToGross = (netThicknesses / grossThicknesses) / 100;
	
	$(attrId + 'net_to_gross').val(netToGross.toFixed(2));
}


$('#porosityrock_P90, #watersat_P90, #s2d_net_p90_thickness, #s3d_net_p90_thickness').bind('change keyup',function() {
	var s2d_net_p90_thickness = ($('#s2d_net_p90_thickness').val() != '') ? parseFloat($('#s2d_net_p90_thickness').val()) : 0;
	var s3d_net_p90_thickness = ($('#s3d_net_p90_thickness').val() != '') ? parseFloat($('#s3d_net_p90_thickness').val()) : 0;
	
	var net_p90_thickness = (s3d_net_p90_thickness == 0) ? s2d_net_p90_thickness : s3d_net_p90_thickness;
	
	$('#s2d_vol_p90_thickness').val(s2d_net_p90_thickness);
	$('#s3d_vol_p90_thickness').val(s3d_net_p90_thickness);
	
	$('#sgv_vol_p90_thickness').val(net_p90_thickness);
	$('#sgc_vol_p90_thickness').val(net_p90_thickness);
	$('#sel_vol_p90_thickness').val(net_p90_thickness);
	$('#rst_vol_p90_thickness').val(net_p90_thickness);
	$('#sor_vol_p90_thickness').val(net_p90_thickness);
	
	$('#sgf_p90_porosity').val($('#porosityrock_P90').val());
	$('#sgf_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#s2d_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#s2d_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#sgv_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#sgv_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#sgc_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#sgc_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#sel_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#sel_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#rst_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#rst_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#sor_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#sor_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	$('#s3d_vol_p90_porosity').val($('#porosityrock_P90').val());
	$('#s3d_vol_p90_sw').val(100 - $('#watersat_P90').val());
	
	calculateSurveyDrillable($('#sgf_p90_area').val(), $('#sgf_p90_thickness').val(), $('#sgf_p90_net').val(), $('#sgf_p90_porosity').val(), $('#sgf_p90_sw').val(), '#sgf_p90_');
	calculateSurveysDrillable($('#s2d_vol_p90_area').val(), $('#s2d_vol_p90_thickness').val(), $('#s2d_vol_p90_porosity').val(), $('#s2d_vol_p90_sw').val(), '#s2d_vol_p90_');
	calculateSurveysDrillable($('#sgv_vol_p90_area').val(), $('#sgv_vol_p90_thickness').val(), $('#sgv_vol_p90_porosity').val(), $('#sgv_vol_p90_sw').val(), '#sgv_vol_p90_');
	calculateSurveysDrillable($('#sgc_vol_p90_area').val(), $('#sgc_vol_p90_thickness').val(), $('#sgc_vol_p90_porosity').val(), $('#sgc_vol_p90_sw').val(), '#sgc_vol_p90_');
	calculateSurveysDrillable($('#sel_vol_p90_area').val(), $('#sel_vol_p90_thickness').val(), $('#sel_vol_p90_porosity').val(), $('#sel_vol_p90_sw').val(), '#sel_vol_p90_');
	calculateSurveysDrillable($('#rst_vol_p90_area').val(), $('#rst_vol_p90_thickness').val(), $('#rst_vol_p90_porosity').val(), $('#rst_vol_p90_sw').val(), '#rst_vol_p90_');
	calculateSurveysDrillable($('#sor_vol_p90_area').val(), $('#sor_vol_p90_thickness').val(), $('#sor_vol_p90_porosity').val(), $('#sor_vol_p90_sw').val(), '#sor_vol_p90_');
	calculateSurveysDrillable($('#s3d_vol_p90_area').val(), $('#s3d_vol_p90_thickness').val(), $('#s3d_vol_p90_porosity').val(), $('#s3d_vol_p90_sw').val(), '#s3d_vol_p90_');
	
	calculateNetToGrossDrillable($('#s2d_net_p90_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p90_');
	calculateNetToGrossDrillable($('#s3d_net_p90_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p90_');
})
.change();

$('#porosityrock_P50, #watersat_P50, #s2d_net_p50_thickness, #s3d_net_p50_thickness').bind('change keyup',function() {
	var s2d_net_p50_thickness = ($('#s2d_net_p50_thickness').val() != '') ? parseFloat($('#s2d_net_p50_thickness').val()) : 0;
	var s3d_net_p50_thickness = ($('#s3d_net_p50_thickness').val() != '') ? parseFloat($('#s3d_net_p50_thickness').val()) : 0;
	
	var net_p50_thickness = (s3d_net_p50_thickness == 0) ? s2d_net_p50_thickness : s3d_net_p50_thickness;
	
	$('#s2d_vol_p50_thickness').val(s2d_net_p50_thickness);
	$('#s3d_vol_p50_thickness').val(s3d_net_p50_thickness);
	
	$('#sgv_vol_p50_thickness').val(net_p50_thickness);
	$('#sgc_vol_p50_thickness').val(net_p50_thickness);
	$('#sel_vol_p50_thickness').val(net_p50_thickness);
	$('#rst_vol_p50_thickness').val(net_p50_thickness);
	$('#sor_vol_p50_thickness').val(net_p50_thickness);
	
	$('#sgf_p50_porosity').val($('#porosityrock_P50').val());
	$('#sgf_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#s2d_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#s2d_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#sgv_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#sgv_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#sgc_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#sgc_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#sel_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#sel_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#rst_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#rst_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#sor_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#sor_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	$('#s3d_vol_p50_porosity').val($('#porosityrock_P50').val());
	$('#s3d_vol_p50_sw').val(100 - $('#watersat_P50').val());
	
	calculateSurveyDrillable($('#sgf_p50_area').val(), $('#sgf_p50_thickness').val(), $('#sgf_p50_net').val(), $('#sgf_p50_porosity').val(), $('#sgf_p50_sw').val(), '#sgf_p50_');
	calculateSurveysDrillable($('#s2d_vol_p50_area').val(), $('#s2d_vol_p50_thickness').val(), $('#s2d_vol_p50_porosity').val(), $('#s2d_vol_p50_sw').val(), '#s2d_vol_p50_');
	calculateSurveysDrillable($('#sgv_vol_p50_area').val(), $('#sgv_vol_p50_thickness').val(), $('#sgv_vol_p50_porosity').val(), $('#sgv_vol_p50_sw').val(), '#sgv_vol_p50_');
	calculateSurveysDrillable($('#sgc_vol_p50_area').val(), $('#sgc_vol_p50_thickness').val(), $('#sgc_vol_p50_porosity').val(), $('#sgc_vol_p50_sw').val(), '#sgc_vol_p50_');
	calculateSurveysDrillable($('#sel_vol_p50_area').val(), $('#sel_vol_p50_thickness').val(), $('#sel_vol_p50_porosity').val(), $('#sel_vol_p50_sw').val(), '#sel_vol_p50_');
	calculateSurveysDrillable($('#rst_vol_p50_area').val(), $('#rst_vol_p50_thickness').val(), $('#rst_vol_p50_porosity').val(), $('#rst_vol_p50_sw').val(), '#rst_vol_p50_');
	calculateSurveysDrillable($('#sor_vol_p50_area').val(), $('#sor_vol_p50_thickness').val(), $('#sor_vol_p50_porosity').val(), $('#sor_vol_p50_sw').val(), '#sor_vol_p50_');
	calculateSurveysDrillable($('#s3d_vol_p50_area').val(), $('#s3d_vol_p50_thickness').val(), $('#s3d_vol_p50_porosity').val(), $('#s3d_vol_p50_sw').val(), '#s3d_vol_p50_');
	
	calculateNetToGrossDrillable($('#s2d_net_p50_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p50_');
	calculateNetToGrossDrillable($('#s3d_net_p50_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p50_');
})
.change();

$('#porosityrock_P10, #watersat_P10, #s2d_net_p10_thickness, #s3d_net_p10_thickness').bind('change keyup',function() {
	var s2d_net_p10_thickness = ($('#s2d_net_p10_thickness').val() != '') ? parseFloat($('#s2d_net_p10_thickness').val()) : 0;
	var s3d_net_p10_thickness = ($('#s3d_net_p10_thickness').val() != '') ? parseFloat($('#s3d_net_p10_thickness').val()) : 0;
	
	var net_p10_thickness = (s3d_net_p10_thickness == 0) ? s2d_net_p10_thickness : s3d_net_p10_thickness;
	
	$('#s2d_vol_p10_thickness').val(s2d_net_p10_thickness);
	$('#s3d_vol_p10_thickness').val(s3d_net_p10_thickness);
	
	$('#sgv_vol_p10_thickness').val(net_p10_thickness);
	$('#sgc_vol_p10_thickness').val(net_p10_thickness);
	$('#sel_vol_p10_thickness').val(net_p10_thickness);
	$('#rst_vol_p10_thickness').val(net_p10_thickness);
	$('#sor_vol_p10_thickness').val(net_p10_thickness);
	
	$('#sgf_p10_porosity').val($('#porosityrock_P10').val());
	$('#sgf_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#s2d_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#s2d_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#sgv_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#sgv_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#sgc_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#sgc_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#sel_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#sel_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#rst_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#rst_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#sor_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#sor_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	$('#s3d_vol_p10_porosity').val($('#porosityrock_P10').val());
	$('#s3d_vol_p10_sw').val(100 - $('#watersat_P10').val());
	
	calculateSurveyDrillable($('#sgf_p10_area').val(), $('#sgf_p10_thickness').val(), $('#sgf_p10_net').val(), $('#sgf_p10_porosity').val(), $('#sgf_p10_sw').val(), '#sgf_p10_');
	calculateSurveysDrillable($('#s2d_vol_p10_area').val(), $('#s2d_vol_p10_thickness').val(), $('#s2d_vol_p10_porosity').val(), $('#s2d_vol_p10_sw').val(), '#s2d_vol_p10_');
	calculateSurveysDrillable($('#sgv_vol_p10_area').val(), $('#sgv_vol_p10_thickness').val(), $('#sgv_vol_p10_porosity').val(), $('#sgv_vol_p10_sw').val(), '#sgv_vol_p10_');
	calculateSurveysDrillable($('#sgc_vol_p10_area').val(), $('#sgc_vol_p10_thickness').val(), $('#sgc_vol_p10_porosity').val(), $('#sgc_vol_p10_sw').val(), '#sgc_vol_p10_');
	calculateSurveysDrillable($('#sel_vol_p10_area').val(), $('#sel_vol_p10_thickness').val(), $('#sel_vol_p10_porosity').val(), $('#sel_vol_p10_sw').val(), '#sel_vol_p10_');
	calculateSurveysDrillable($('#rst_vol_p10_area').val(), $('#rst_vol_p10_thickness').val(), $('#rst_vol_p10_porosity').val(), $('#rst_vol_p10_sw').val(), '#rst_vol_p10_');
	calculateSurveysDrillable($('#sor_vol_p10_area').val(), $('#sor_vol_p10_thickness').val(), $('#sor_vol_p10_porosity').val(), $('#sor_vol_p10_sw').val(), '#sor_vol_p10_');
	calculateSurveysDrillable($('#s3d_vol_p10_area').val(), $('#s3d_vol_p10_thickness').val(), $('#s3d_vol_p10_porosity').val(), $('#s3d_vol_p10_sw').val(), '#s3d_vol_p10_');
	
	calculateNetToGrossDrillable($('#s2d_net_p10_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p10_');
	calculateNetToGrossDrillable($('#s3d_net_p10_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p10_');
})
.change();

//calculate volumetric p90
$('#sgf_p90_area, #sgf_p90_thickness, #sgf_p90_net').live('keyup', function() {
	calculateSurveyDrillable($('#sgf_p90_area').val(), $('#sgf_p90_thickness').val(), $('#sgf_p90_net').val(), $('#sgf_p90_porosity').val(), $('#sgf_p90_sw').val(), '#sgf_p90_');
});
$('#s2d_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#s2d_vol_p90_area').val(), $('#s2d_vol_p90_thickness').val(), $('#s2d_vol_p90_porosity').val(), $('#s2d_vol_p90_sw').val(), '#s2d_vol_p90_');
});
$('#sgv_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#sgv_vol_p90_area').val(), $('#sgv_vol_p90_thickness').val(), $('#sgv_vol_p90_porosity').val(), $('#sgv_vol_p90_sw').val(), '#sgv_vol_p90_');
});
$('#sgc_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#sgc_vol_p90_area').val(), $('#sgc_vol_p90_thickness').val(), $('#sgc_vol_p90_porosity').val(), $('#sgc_vol_p90_sw').val(), '#sgc_vol_p90_');
});
$('#sel_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#sel_vol_p90_area').val(), $('#sel_vol_p90_thickness').val(), $('#sel_vol_p90_porosity').val(), $('#sel_vol_p90_sw').val(), '#sel_vol_p90_');
});
$('#rst_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#rst_vol_p90_area').val(), $('#rst_vol_p90_thickness').val(), $('#rst_vol_p90_porosity').val(), $('#rst_vol_p90_sw').val(), '#rst_vol_p90_');
});
$('#sor_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#sor_vol_p90_area').val(), $('#sor_vol_p90_thickness').val(), $('#sor_vol_p90_porosity').val(), $('#sor_vol_p90_sw').val(), '#sor_vol_p90_');
});
$('#s3d_vol_p90_area').live('keyup', function() {
	calculateSurveysDrillable($('#s3d_vol_p90_area').val(), $('#s3d_vol_p90_thickness').val(), $('#s3d_vol_p90_porosity').val(), $('#s3d_vol_p90_sw').val(), '#s3d_vol_p90_');
});
//end calculate volumetric p90

//calculate volumetric p50
$('#sgf_p50_area, #sgf_p50_thickness, #sgf_p50_net').live('keyup', function() {
	calculateSurveyDrillable($('#sgf_p50_area').val(), $('#sgf_p50_thickness').val(), $('#sgf_p50_net').val(), $('#sgf_p50_porosity').val(), $('#sgf_p50_sw').val(), '#sgf_p50_');
});
$('#s2d_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#s2d_vol_p50_area').val(), $('#s2d_vol_p50_thickness').val(), $('#s2d_vol_p50_porosity').val(), $('#s2d_vol_p50_sw').val(), '#s2d_vol_p50_');
});
$('#sgv_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#sgv_vol_p50_area').val(), $('#sgv_vol_p50_thickness').val(), $('#sgv_vol_p50_porosity').val(), $('#sgv_vol_p50_sw').val(), '#sgv_vol_p50_');
});
$('#sgc_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#sgc_vol_p50_area').val(), $('#sgc_vol_p50_thickness').val(), $('#sgc_vol_p50_porosity').val(), $('#sgc_vol_p50_sw').val(), '#sgc_vol_p50_');
});
$('#sel_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#sel_vol_p50_area').val(), $('#sel_vol_p50_thickness').val(), $('#sel_vol_p50_porosity').val(), $('#sel_vol_p50_sw').val(), '#sel_vol_p50_');
});
$('#rst_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#rst_vol_p50_area').val(), $('#rst_vol_p50_thickness').val(), $('#rst_vol_p50_porosity').val(), $('#rst_vol_p50_sw').val(), '#rst_vol_p50_');
});
$('#sor_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#sor_vol_p50_area').val(), $('#sor_vol_p50_thickness').val(), $('#sor_vol_p50_porosity').val(), $('#sor_vol_p50_sw').val(), '#sor_vol_p50_');
});
$('#s3d_vol_p50_area').live('keyup', function() {
	calculateSurveysDrillable($('#s3d_vol_p50_area').val(), $('#s3d_vol_p50_thickness').val(), $('#s3d_vol_p50_porosity').val(), $('#s3d_vol_p50_sw').val(), '#s3d_vol_p50_');
});
//end calculate volumetric p50

//calculate volumetric p10
$('#sgf_p10_area, #sgf_p10_thickness, #sgf_p10_net').live('keyup', function() {
	calculateSurveyDrillable($('#sgf_p10_area').val(), $('#sgf_p10_thickness').val(), $('#sgf_p10_net').val(), $('#sgf_p10_porosity').val(), $('#sgf_p10_sw').val(), '#sgf_p10_');
});
$('#s2d_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#s2d_vol_p10_area').val(), $('#s2d_vol_p10_thickness').val(), $('#s2d_vol_p10_porosity').val(), $('#s2d_vol_p10_sw').val(), '#s2d_vol_p10_');
});
$('#sgv_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#sgv_vol_p10_area').val(), $('#sgv_vol_p10_thickness').val(), $('#sgv_vol_p10_porosity').val(), $('#sgv_vol_p10_sw').val(), '#sgv_vol_p10_');
});
$('#sgc_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#sgc_vol_p10_area').val(), $('#sgc_vol_p10_thickness').val(), $('#sgc_vol_p10_porosity').val(), $('#sgc_vol_p10_sw').val(), '#sgc_vol_p10_');
});
$('#sel_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#sel_vol_p10_area').val(), $('#sel_vol_p10_thickness').val(), $('#sel_vol_p10_porosity').val(), $('#sel_vol_p10_sw').val(), '#sel_vol_p10_');
});
$('#rst_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#rst_vol_p10_area').val(), $('#rst_vol_p10_thickness').val(), $('#rst_vol_p10_porosity').val(), $('#rst_vol_p10_sw').val(), '#rst_vol_p10_');
});
$('#sor_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#sor_vol_p10_area').val(), $('#sor_vol_p10_thickness').val(), $('#sor_vol_p10_porosity').val(), $('#sor_vol_p10_sw').val(), '#sor_vol_p10_');
});
$('#s3d_vol_p10_area').live('keyup', function() {
	calculateSurveysDrillable($('#s3d_vol_p10_area').val(), $('#s3d_vol_p10_thickness').val(), $('#s3d_vol_p10_porosity').val(), $('#s3d_vol_p10_sw').val(), '#s3d_vol_p10_');
});
//end calculate volumetric p10

$('#dav_2dss_gross_resthickness').live('keyup', function() {
	calculateNetToGrossDrillable($('#s2d_net_p90_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p90_');
	calculateNetToGrossDrillable($('#s2d_net_p50_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p50_');
	calculateNetToGrossDrillable($('#s2d_net_p10_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p10_');
});
$('#dav_3dss_grass_resthickness').live('keyup', function() {
	calculateNetToGrossDrillable($('#s3d_net_p90_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p90_');
	calculateNetToGrossDrillable($('#s3d_net_p50_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p50_');
	calculateNetToGrossDrillable($('#s3d_net_p10_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p10_');
});

calculateSurveyDrillable($('#sgf_p90_area').val(), $('#sgf_p90_thickness').val(), $('#sgf_p90_net').val(), $('#sgf_p90_porosity').val(), $('#sgf_p90_sw').val(), '#sgf_p90_');
calculateSurveysDrillable($('#s2d_vol_p90_area').val(), $('#s2d_vol_p90_thickness').val(), $('#s2d_vol_p90_porosity').val(), $('#s2d_vol_p90_sw').val(), '#s2d_vol_p90_');
calculateSurveysDrillable($('#sgv_vol_p90_area').val(), $('#sgv_vol_p90_thickness').val(), $('#sgv_vol_p90_porosity').val(), $('#sgv_vol_p90_sw').val(), '#sgv_vol_p90_');
calculateSurveysDrillable($('#sgc_vol_p90_area').val(), $('#sgc_vol_p90_thickness').val(), $('#sgc_vol_p90_porosity').val(), $('#sgc_vol_p90_sw').val(), '#sgc_vol_p90_');
calculateSurveysDrillable($('#sel_vol_p90_area').val(), $('#sel_vol_p90_thickness').val(), $('#sel_vol_p90_porosity').val(), $('#sel_vol_p90_sw').val(), '#sel_vol_p90_');
calculateSurveysDrillable($('#rst_vol_p90_area').val(), $('#rst_vol_p90_thickness').val(), $('#rst_vol_p90_porosity').val(), $('#rst_vol_p90_sw').val(), '#rst_vol_p90_');
calculateSurveysDrillable($('#sor_vol_p90_area').val(), $('#sor_vol_p90_thickness').val(), $('#sor_vol_p90_porosity').val(), $('#sor_vol_p90_sw').val(), '#sor_vol_p90_');
calculateSurveysDrillable($('#s3d_vol_p90_area').val(), $('#s3d_vol_p90_thickness').val(), $('#s3d_vol_p90_porosity').val(), $('#s3d_vol_p90_sw').val(), '#s3d_vol_p90_');

calculateSurveyDrillable($('#sgf_p50_area').val(), $('#sgf_p50_thickness').val(), $('#sgf_p50_net').val(), $('#sgf_p50_porosity').val(), $('#sgf_p50_sw').val(), '#sgf_p50_');
calculateSurveysDrillable($('#s2d_vol_p50_area').val(), $('#s2d_vol_p50_thickness').val(), $('#s2d_vol_p50_porosity').val(), $('#s2d_vol_p50_sw').val(), '#s2d_vol_p50_');
calculateSurveysDrillable($('#sgv_vol_p50_area').val(), $('#sgv_vol_p50_thickness').val(), $('#sgv_vol_p50_porosity').val(), $('#sgv_vol_p50_sw').val(), '#sgv_vol_p50_');
calculateSurveysDrillable($('#sgc_vol_p50_area').val(), $('#sgc_vol_p50_thickness').val(), $('#sgc_vol_p50_porosity').val(), $('#sgc_vol_p50_sw').val(), '#sgc_vol_p50_');
calculateSurveysDrillable($('#sel_vol_p50_area').val(), $('#sel_vol_p50_thickness').val(), $('#sel_vol_p50_porosity').val(), $('#sel_vol_p50_sw').val(), '#sel_vol_p50_');
calculateSurveysDrillable($('#rst_vol_p50_area').val(), $('#rst_vol_p50_thickness').val(), $('#rst_vol_p50_porosity').val(), $('#rst_vol_p50_sw').val(), '#rst_vol_p50_');
calculateSurveysDrillable($('#sor_vol_p50_area').val(), $('#sor_vol_p50_thickness').val(), $('#sor_vol_p50_porosity').val(), $('#sor_vol_p50_sw').val(), '#sor_vol_p50_');
calculateSurveysDrillable($('#s3d_vol_p50_area').val(), $('#s3d_vol_p50_thickness').val(), $('#s3d_vol_p50_porosity').val(), $('#s3d_vol_p50_sw').val(), '#s3d_vol_p50_');

calculateSurveyDrillable($('#sgf_p10_area').val(), $('#sgf_p10_thickness').val(), $('#sgf_p10_net').val(), $('#sgf_p10_porosity').val(), $('#sgf_p10_sw').val(), '#sgf_p10_');
calculateSurveysDrillable($('#s2d_vol_p10_area').val(), $('#s2d_vol_p10_thickness').val(), $('#s2d_vol_p10_porosity').val(), $('#s2d_vol_p10_sw').val(), '#s2d_vol_p10_');
calculateSurveysDrillable($('#sgv_vol_p10_area').val(), $('#sgv_vol_p10_thickness').val(), $('#sgv_vol_p10_porosity').val(), $('#sgv_vol_p10_sw').val(), '#sgv_vol_p10_');
calculateSurveysDrillable($('#sgc_vol_p10_area').val(), $('#sgc_vol_p10_thickness').val(), $('#sgc_vol_p10_porosity').val(), $('#sgc_vol_p10_sw').val(), '#sgc_vol_p10_');
calculateSurveysDrillable($('#sel_vol_p10_area').val(), $('#sel_vol_p10_thickness').val(), $('#sel_vol_p10_porosity').val(), $('#sel_vol_p10_sw').val(), '#sel_vol_p10_');
calculateSurveysDrillable($('#rst_vol_p10_area').val(), $('#rst_vol_p10_thickness').val(), $('#rst_vol_p10_porosity').val(), $('#rst_vol_p10_sw').val(), '#rst_vol_p10_');
calculateSurveysDrillable($('#sor_vol_p10_area').val(), $('#sor_vol_p10_thickness').val(), $('#sor_vol_p10_porosity').val(), $('#sor_vol_p10_sw').val(), '#sor_vol_p10_');
calculateSurveysDrillable($('#s3d_vol_p10_area').val(), $('#s3d_vol_p10_thickness').val(), $('#s3d_vol_p10_porosity').val(), $('#s3d_vol_p10_sw').val(), '#s3d_vol_p10_');

calculateNetToGrossDrillable($('#s2d_net_p90_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p90_');
calculateNetToGrossDrillable($('#s2d_net_p50_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p50_');
calculateNetToGrossDrillable($('#s2d_net_p10_thickness').val(), $('#dav_2dss_gross_resthickness').val(), '#s2d_net_p10_');
calculateNetToGrossDrillable($('#s3d_net_p90_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p90_');
calculateNetToGrossDrillable($('#s3d_net_p50_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p50_');
calculateNetToGrossDrillable($('#s3d_net_p10_thickness').val(), $('#dav_3dss_grass_resthickness').val(), '#s3d_net_p10_');
")
?>

<?php 
	Yii::app()->clientScript->registerScript('formationname', "
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/playname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('#plyName').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.play,
                    escapeMarkup: function(m) {
                        // Do not escape HTML in the select options text
                        return m;
                    },
				});
			},
		});	
		
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('.formationname').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.formation,
				});
		
				disable('gcfsrock');
				disable('gcfres');
				disable('gcftrap');
			},
		});	
		
		function disable(id) {
        	switch(id) {
				case 'gcfsrock' :
					if($('#' + id).val() == '') {
                		mati(['#sourceformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#sourceformationname']);
                	}
                	break;
                case 'gcfres' :
                		mati2(['#reservoirformationname']);
                	break;
                case 'gcftrap' :
					if($('#' + id).val() == '') {
                		mati(['#sealingformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#sealingformationname']);
                	}
                	break;
            }
        }
		
		function mati(target) {
        	$(target).select2(\"readonly\", true);
            $(target).select2(\"val\", \"\");
		}
		
		function mati2(target) {
			$(target).select2(\"readonly\", true);
		}
                		
        function nyala(target) {
             $(target).select2(\"enable\", true);
             $(target).select2(\"readonly\", false);
		}
                		
        $('#gcfsrock').on('change', function(e) {
			disable('gcfsrock');
		});
        $('#gcfres').on('change', function(e) {
			disable('gcfres');
		});
        $('#gcftrap').on('change', function(e) {
			disable('gcftrap');
		});  
	");
	
	Yii::app()->clientScript->registerScript('ajaxupdate', "
		$('#drillable-grid a.ajaxupdate').live('click', function() {
            $.ajax({
				url : $(this).attr('href'),
				type: 'POST',
				data: '',
				dataType: 'json',
				success : function(data) {
					if(data.error === false) {
                        if(data.prospect_update_from != '') $('#prospect_update_from').val(data.prospect_update_from);
                        if(data.prospect_submit_revision != '') $('#prospect_submit_revision').val(data.prospect_submit_revision);
						$('#structure_name').val(data.structure_name);
						$('#plyName').val(data.play_id);
						$('#_clarifiedby').select2('val', data.prospect_clarified);
						$('#dl7').val(data.prospect_date_initiate);
						$('#" . CHtml::activeId($mdlProspect, 'center_lat_degree') . "').val(data.center_lat_degree);
						$('#" . CHtml::activeId($mdlProspect, 'center_lat_minute') . "').val(data.center_lat_minute);
						$('#" . CHtml::activeId($mdlProspect, 'center_lat_second') . "').val(data.center_lat_second);
						$('#" . CHtml::activeId($mdlProspect, 'center_lat_direction') . "').val(data.center_lat_direction);
						$('#" . CHtml::activeId($mdlProspect, 'center_long_degree') . "').val(data.center_long_degree);
						$('#" . CHtml::activeId($mdlProspect, 'center_long_minute') . "').val(data.center_long_minute);
						$('#" . CHtml::activeId($mdlProspect, 'center_long_second') . "').val(data.center_long_second);
						$('#" . CHtml::activeId($mdlProspect, 'center_long_direction') . "').val(data.center_long_direction);
						$('#env_onoffshore').select2('val', data.prospect_shore);
						$('#env_terrain').select2('val', data.prospect_terrain);
                      	$('#n_facility').val(data.prospect_near_field);
						$('#n_developmentwell').val(data.prospect_near_infra_structure);
                        $('#porosityrock_sample').val(data.dr_por_quantity);
                       	$('#porosityrock_P90').val(data.dr_por_p90);
                        $('#porosityrock_P50').val(data.dr_por_p50);
                        $('#porosityrock_P10').val(data.dr_por_p10);
                       	$('#s2id_porosityrock_p90').select2('val', data.dr_por_p90_type);
                        $('#s2id_porosityrock_p50').select2('val', data.dr_por_p50_type);
                        $('#s2id_porosityrock_p10').select2('val', data.dr_por_p10_type);
                        $('#porosityrock_remarks').val(data.dr_por_remark);
                        $('#watersat_sample').val(data.dr_satur_quantity);
                       	$('#watersat_P90').val(data.dr_satur_p90);
                        $('#watersat_P50').val(data.dr_satur_p50);
                        $('#watersat_P10').val(data.dr_satur_p10);
                       	$('#s2id_watersat_p90').select2('val', data.dr_satur_p90_type);
                        $('#s2id_watersat_p50').select2('val', data.dr_satur_p50_type);
                        $('#s2id_watersat_p10').select2('val', data.dr_satur_p10_type);
                        $('#watersat_remarks').val(data.dr_satur_remark);
                        
                        if(data.sgf_is_check != null) {
                        	$('#DlGfs').parent().addClass('checked');
							$('#DlGfs').prop('checked', true);
							toggleStatus('DlGfs','DlGfs_link','col_dav1');
                        } else {
                            $('#DlGfs').parent().removeClass('checked');
                           	$('#DlGfs').prop('checked', false);
                           	$('#col_dav1').css('height', '0px');
							$('#DlGfs_link').css('color', 'gray');
							$('#col_dav1').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_gfs_aqyear').val(data.sgf_year_survey);
                        $('#s2id_dav_gfs_surmethod').select2('val', data.sgf_survey_method);
                        $('#dav_gfs_tca').val(data.sgf_coverage_area);
                        $('#sgf_p90_area').val(data.sgf_p90_area);
                        $('#sgf_p90_thickness').val(data.sgf_p90_thickness);
                        $('#sgf_p90_net').val(data.sgf_p90_net);
                        $('#sgf_p50_area').val(data.sgf_p50_area);
                        $('#sgf_p50_thickness').val(data.sgf_p50_thickness);
                        $('#sgf_p50_net').val(data.sgf_p50_net);
                        $('#sgf_p10_area').val(data.sgf_p10_area);
                        $('#sgf_p10_thickness').val(data.sgf_p10_thickness);
                        $('#sgf_p10_net').val(data.sgf_p10_net);
                        $('#dav_gfs_remarks').val(data.sgf_remark);
                        if(data.s2d_is_check != null) {
                            $('#DlDss2').parent().addClass('checked');
							$('#DlDss2').prop('checked', true);
							toggleStatus('DlDss2','DlDss2_link','col_dav2');
                        } else {
                            $('#DlDss2').parent().removeClass('checked');
                           	$('#DlDss2').prop('checked', false);
                           	$('#col_dav2').css('height', '0px');
							$('#DlDss2_link').css('color', 'gray');
							$('#col_dav2').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_2dss_aqyear').val(data.s2d_year_survey);
                        $('#s2id_dav_2dss_numvin').select2('val', data.s2d_vintage_number);
                        $('#dav_2dss_tcn').val(data.s2d_total_crossline);
                        $('#dav_2dss_sltl').val(data.s2d_seismic_line);
                        $('#dav_2dss_aspi').val(data.s2d_average_interval);
                        $('#dav_2dss_rl1').val(data.s2d_record_length_ms);
                        $('#dav_2dss_rl2').val(data.s2d_record_length_ft);
                        $('#dav_2dss_yearlatest').val(data.s2d_year_late_process);
                        $('#s2id_dav_2dss_lpm').select2('val', data.s2d_late_method);
                        $('#s2id_dav_2dss_siq').select2('val', data.s2d_img_quality);
                        $('#dav_2dss_trtd1').val(data.s2d_top_depth_ft);
                        $('#dav_2dss_trtd2').val(data.s2d_top_depth_ms);
                        $('#dav_2dss_brtd1').val(data.s2d_bot_depth_ft);
                        $('#dav_2dss_brtd2').val(data.s2d_bot_depth_ms);
                        $('#dav_2dss_dcsp').val(data.s2d_depth_spill);
                        $('#dav_2dss_owc1').val(data.s2d_depth_estimate);
                        $('#dlosd2m2').val(data.s2d_depth_estimate_analog);
                        $('#dav_2dss_owc2').val(data.s2d_depth_estimate_spill);
                        $('#dav_2dss_oilgas1').val(data.s2d_depth_low);
                        $('#dlosd2n2').val(data.s2d_depth_low_analog);
                        $('#oilgas2').val(data.s2d_depth_low_spill);
                        $('#dav_2dss_forthickness').val(data.s2d_formation_thickness);
                        $('#dav_2dss_gross_resthickness').val(data.s2d_gross_thickness);
                        $('#s2id_dav_2dss_antrip').select2('val', data.s2d_avail_pay);
                        $('#s2d_net_p90_thickness').val(data.s2d_net_p90_thickness);
                        $('#s2d_net_p90_vsh').val(data.s2d_net_p90_vsh);
                        $('#s2d_net_p90_por').val(data.s2d_net_p90_por);
                        $('#s2d_net_p90_satur').val(data.s2d_net_p90_satur);
                        $('#s2d_net_p50_thickness').val(data.s2d_net_p50_thickness);
                        $('#s2d_net_p50_vsh').val(data.s2d_net_p50_vsh);
                        $('#s2d_net_p50_por').val(data.s2d_net_p50_por);
                        $('#s2d_net_p50_satur').val(data.s2d_net_p50_satur);
                        $('#s2d_net_p10_thickness').val(data.s2d_net_p10_thickness);
                        $('#s2d_net_p10_vsh').val(data.s2d_net_p10_vsh);
                        $('#s2d_net_p10_por').val(data.s2d_net_p10_por);
                        $('#s2d_net_p10_satur').val(data.s2d_net_p10_satur);
                        $('#s2d_vol_p90_area').val(data.s2d_vol_p90_area);
                        $('#s2d_vol_p50_area').val(data.s2d_vol_p50_area);
                        $('#s2d_vol_p10_area').val(data.s2d_vol_p10_area);
                        $('#dav_2dss_remarks').val(data.s2d_remark);
                       	if(data.sgv_is_check != null) {
                            $('#DlGras').parent().addClass('checked');
							$('#DlGras').prop('checked', true);
							toggleStatus('DlGras','DlGras_link','col_dav3');
                        } else {
                            $('#DlGras').parent().removeClass('checked');
                           	$('#DlGras').prop('checked', false);
                           	$('#col_dav3').css('height', '0px');
							$('#DlGras_link').css('color', 'gray');
							$('#col_dav3').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_grasur_aqyear').val(data.sgv_year_survey);
                        $('#s2id_dav_grasur_surveymethods').select2('val', data.sgv_survey_method);
                        $('#dav_grasur_sca').val(data.sgv_coverage_area);
                        $('#dav_grasur_dspr').val(data.sgv_depth_range);
                        $('#dav_grasur_rsi').val(data.sgv_spacing_interval);
                        $('#dav_grasur_dcsp').val(data.sgv_depth_spill);
                        $('#dav_grasur_owc1').val(data.sgv_depth_estimate);
                        $('#dlosd3g2').val(data.sgv_depth_estimate_analog);
                        $('#dav_grasur_owc2').val(data.sgv_depth_estimate_spill);
                        $('#dav_grasur_oilgas1').val(data.sgv_depth_low);
                        $('#dlosd3h2').val(data.sgv_depth_low_analog);
                        $('#dav_grasur_oilgas2').val(data.sgv_depth_low_spill);
                        $('#dav_grasur_resthickness1').val(data.sgv_res_thickness);
                        $('#dav_grasur_resthickness2').val(data.sgv_res_according);
                        $('#dav_grasur_resthickness3').val(data.sgv_res_top_depth);
                        $('#dav_grasur_resthickness4').val(data.sgv_res_bot_depth);
                        $('#dav_grasur_gst1').val(data.sgv_gross_thickness);
                        $('#dav_grasur_gst2').val(data.sgv_gross_according);
                        $('#dav_grasur_gst3').val(data.sgv_gross_well);
                        $('#dav_grasur_netogross').val(data.sgv_net_gross);
                        $('#sgv_vol_p90_area').val(data.sgv_vol_p90_area);
                        $('#sgv_vol_p50_area').val(data.sgv_vol_p50_area);
                        $('#sgv_vol_p10_area').val(data.sgv_vol_p10_area);
                        $('#dav_grasur_remarks').val(data.sgv_remark);
                        if(data.sgc_is_check != null) {
                            $('#DlGeos').parent().addClass('checked');
							$('#DlGeos').prop('checked', true);
							toggleStatus('DlGeos','DlGeos_link','col_dav4');
                        } else {
                            $('#DlGeos').parent().removeClass('checked');
                           	$('#DlGeos').prop('checked', false);
                           	$('#col_dav4').css('height', '0px');
							$('#DlGeos_link').css('color', 'gray');
							$('#col_dav4').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_geo_aqyear').val(data.sgc_year_survey);
                        $('#dav_geo_isr').val(data.sgc_sample_interval);
                        $('#dav_geo_numsample').val(data.sgc_number_sample);
                        $('#dav_geo_numrock').val(data.sgc_number_rock);
                        $('#dav_geo_numfluid').val(data.sgc_number_fluid);
                        $('#s2id_dav_geo_ahc').select2('val', data.sgc_avail_hc);
                        $('#dav_geo_hycomp').val(data.sgc_hc_composition);
                        $('#dav_geo_lery').val(data.sgc_lab_report);
                        $('#sgc_vol_p90_area').val(data.sgc_vol_p90_area);
                        $('#sgc_vol_p50_area').val(data.sgc_vol_p50_area);
                        $('#sgc_vol_p10_area').val(data.sgc_vol_p10_area);
                        $('#dav_geo_remarks').val(data.sgc_remark);
                       	if(data.sel_is_check != null) {
                            $('#DlEs').parent().addClass('checked');
							$('#DlEs').prop('checked', true);
							toggleStatus('DlEs','DlEs_link','col_dav5');
                        } else {
                            $('#DlEs').parent().removeClass('checked');
                           	$('#DlEs').prop('checked', false);
                           	$('#col_dav5').css('height', '0px');
							$('#DlEs_link').css('color', 'gray');
							$('#col_dav5').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_elec_aqyear').val(data.sel_year_survey);
                        $('#s2id_dav_elec_surveymethods').select2('val', data.sel_survey_method);
                        $('#dav_elec_sca').val(data.sel_coverage_area);
                        $('#dav_elec_dspr').val(data.sel_depth_range);
                        $('#dav_elec_rsi').val(data.sel_spacing_interval);
                        $('#sel_vol_p90_area').val(data.sel_vol_p90_area);
                        $('#sel_vol_p50_area').val(data.sel_vol_p50_area);
                        $('#sel_vol_p10_area').val(data.sel_vol_p10_area);
                        $('#dav_elec_remarks').val(data.sel_remark);
                        if(data.rst_is_check != null) {
                            $('#DlRs').parent().addClass('checked');
							$('#DlRs').prop('checked', true);
							toggleStatus('DlRs','DlRs_link','col_dav6');
                        } else {
                            $('#DlRs').parent().removeClass('checked');
                           	$('#DlRs').prop('checked', false);
                           	$('#col_dav6').css('height', '0px');
							$('#DlRs_link').css('color', 'gray');
							$('#col_dav6').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_res_aqyear').val(data.rst_year_survey);
                        $('#s2id_dav_res_surveymethods').select2('val', data.rst_survey_method);
                        $('#dav_res_sca').val(data.rst_coverage_area);
                        $('#dav_res_dspr').val(data.rst_depth_range);
                        $('#dav_res_rsi').val(data.rst_spacing_interval);
                        $('#rst_vol_p90_area').val(data.rst_vol_p90_area);
                        $('#rst_vol_p50_area').val(data.rst_vol_p50_area);
                        $('#rst_vol_p10_area').val(data.rst_vol_p10_area);
                        $('#dav_res_remarks').val(data.rst_remark);
                        if(data.sor_is_check != null) {
                            $('#DlOs').parent().addClass('checked');
							$('#DlOs').prop('checked', true);
							toggleStatus('DlOs','DlOs_link','col_dav7');
                        } else {
                            $('#DlOs').parent().removeClass('checked');
                           	$('#DlOs').prop('checked', false);
                           	$('#col_dav7').css('height', '0px');
							$('#DlOs_link').css('color', 'gray');
							$('#col_dav7').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_other_aqyear').val(data.sor_year_survey);
                        $('#sor_vol_p90_area').val(data.sor_vol_p90_area);
                        $('#sor_vol_p50_area').val(data.sor_vol_p50_area);
                        $('#sor_vol_p10_area').val(data.sor_vol_p10_area);
                        $('#dav_other_remarks').val(data.sor_remark);
                        if(data.s3d_is_check != null) {
                            $('#DlDss3').parent().addClass('checked');
							$('#DlDss3').prop('checked', true);
							toggleStatus('DlDss3','DlDss3_link','col_dav8');
                        } else {
                            $('#DlDss3').parent().removeClass('checked');
                           	$('#DlDss3').prop('checked', false);
                           	$('#col_dav8').css('height', '0px');
							$('#DlDss3_link').css('color', 'gray');
							$('#col_dav8').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_3dss_aqyear').val(data.s3d_year_survey);
                        $('#s2id_dav_3dss_numvin').select2('val', data.s3d_vintage_number);
                        $('#dav_3dss_binsize').val(data.s3d_bin_size);
                        $('#dav_3dss_tca').val(data.s3d_coverage_area);
                        $('#dav_3dss_dfrt1').val(data.s3d_frequency);
                        $('#dav_3dss_dfrt2').val(data.s3d_frequency_lateral);
                        $('#dav_3dss_dfrt3').val(data.s3d_frequency_vertical);
                        $('#dav_3dss_lpy').val(data.s3d_year_late_process);
                        $('#s2id_dav_3dss_lpm').select2('val', data.s3d_late_method);
                        $('#s2id_dav_3dss_siq').select2('val', data.s3d_img_quality);
                        $('#dav_3dss_trtd1').val(data.s3d_top_depth_ft);
                        $('#dav_3dss_trtd2').val(data.s3d_top_depth_ms);
                        $('#dav_3dss_brtd1').val(data.s3d_bot_depth_ft);
                        $('#dav_3dss_brtd2').val(data.s3d_top_depth_ms);
                        $('#dav_3dss_dcsp').val(data.s3d_depth_spill);
                        $('#s2id_dav_3dss_adwpo').select2('val', data.s3d_ability_offset);
                        $('#s2id_dav_3dss_ar3p').select2('val', data.s3d_ability_rock);
                        $('#s2id_dav_3dss_arrfp').select2('val', data.s3d_ability_fluid);
                        $('#dav_3dss_owc1').val(data.s3d_depth_estimate);
                        $('#dlosd8n2').val(data.s3d_depth_estimate_analog);
                        $('#dav_3dss_owc2').val(data.s3d_depth_estimate_spill);
                        $('#dav_3dss_oilgas1').val(data.s3d_depth_low);
                        $('#dlosd8o2').val(data.s3d_depth_low_analog);
                        $('#dav_3dss_oilgas2').val(data.s3d_depth_low_spill);
                        $('#dav_3dss_forthickness').val(data.s3d_formation_thickness);
                        $('#dav_3dss_grass_resthickness').val(data.s3d_gross_thickness);
                        $('#s2id_dav_3dss_antrip').select2('val', data.s3d_avail_pay);			
                        $('#s3d_net_p90_thickness').val(data.s3d_net_p90_thickness);			
                        $('#s3d_net_p90_vsh').val(data.s3d_net_p90_vsh);
                        $('#s3d_net_p90_por').val(data.s3d_net_p90_por);
                        $('#s3d_net_p90_satur').val(data.s3d_net_p90_satur);
                        $('#s3d_net_p50_thickness').val(data.s3d_net_p50_thickness);
                        $('#s3d_net_p50_vsh').val(data.s3d_net_p50_vsh);
                        $('#s3d_net_p50_por').val(data.s3d_net_p50_por);
                        $('#s3d_net_p50_satur').val(data.s3d_net_p50_satur);
                        $('#s3d_net_p10_thickness').val(data.s3d_net_p10_thickness);
                        $('#s3d_net_p10_vsh').val(data.s3d_net_p10_vsh);
                        $('#s3d_net_p10_por').val(data.s3d_net_p10_por);
                        $('#s3d_net_p10_satur').val(data.s3d_net_p10_satur);
                        $('#s3d_vol_p90_area').val(data.s3d_vol_p90_area);
                        $('#s3d_vol_p50_area').val(data.s3d_vol_p50_area);
                        $('#s3d_vol_p10_area').val(data.s3d_vol_p10_area);
                        $('#dav_3dss_remarks').val(data.s3d_remark);
                        $('#gcfsrock').select2('val', data.gcf_is_sr);
						$('#gcfsrock1').select2('val', data.gcf_sr_age_system);
						$('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
						$('#sourceformationname').select2('val', data.gcf_sr_formation_name_pre);
						$('#gcf_sr_formation_name_post').select2('val', data.gcf_sr_formation_name_post);
						$('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
						$('#gcfsrock4').select2('val', data.gcf_sr_toc);
						$('#gcfsrock5').select2('val', data.gcf_sr_hfu);
						$('#gcfsrock6').select2('val', data.gcf_sr_distribution);
						$('#gcfsrock7').select2('val', data.gcf_sr_continuity);
						$('#gcfsrock8').select2('val', data.gcf_sr_maturity);
						$('#gcfsrock9').select2('val', data.gcf_sr_otr);
						$('#gcfsrock10').val(data.gcf_sr_remark);
						$('#gcfres').select2('val', data.gcf_is_res);
						$('#gcfres1').select2('val', data.gcf_res_age_system);
						$('#gcfres1a').select2('val', data.gcf_res_age_serie);
						$('#reservoirformationname').select2('val', data.gcf_res_formation_name_pre);
						$('#gcf_res_formation_name_post').select2('val', data.gcf_res_formation_name_post);
						$('#gcfres3').select2('val', data.gcf_res_depos_set);
						$('#gcfres4').select2('val', data.gcf_res_depos_env);
						$('#gcfres5').select2('val', data.gcf_res_distribution);
						$('#gcfres6').select2('val', data.gcf_res_continuity);		
						$('#gcfres7').select2('val', data.gcf_res_lithology);
						$('#gcfres8').select2('val', data.gcf_res_por_type);
						$('#gcfres9').select2('val', data.gcf_res_por_primary);
						$('#gcfres10').select2('val', data.gcf_res_por_secondary);
						$('#gcfres11').val(data.gcf_res_remark);
						$('#gcftrap').select2('val', data.gcf_is_trap);
						$('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
						$('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
						$('#sealingformationname').select2('val', data.gcf_trap_formation_name_pre);
						$('#gcf_trap_formation_name_post').select2('val', data.gcf_trap_formation_name_post);
						$('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
						$('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
						$('#gcftrap5').select2('val', data.gcf_trap_seal_type);
						$('#gcftrap6').select2('val', data.gcf_trap_age_system);
						$('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
						$('#gcftrap7').select2('val', data.gcf_trap_geometry);
						$('#gcftrap8').select2('val', data.gcf_trap_type);
						$('#gcftrap9').select2('val', data.gcf_trap_closure);
						$('#gcftrap10').val(data.gcf_trap_remark);
						$('#gcfdyn').select2('val', data.gcf_is_dyn);
						$('#gcfdyn1').select2('val', data.gcf_dyn_migration);
						$('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
						$('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
						$('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
						$('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
						$('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
						$('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
						$('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
						$('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
						$('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
						$('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
						$('#gcfdyn9').val(data.gcf_dyn_remark);
					}

					disabledElement('gcfsrock');
					disable('gcfsrock');
					disabledElement('gcfres');
					disable('gcfres');
					disabledElement('gcftrap');
					disable('gcftrap');
					disabledElement('gcfdyn');
                    disabledElement('gcfres8');
					generate_prospect_name();
				},
			});
			return false;
		});
	");
?>

<?php 
$cs  = $cs = Yii::app()->getClientScript();

$css = <<<EOD
    table.items {
		width: 100%;
	}

    #drillable-grid table td, #drillable-grid table th {
        text-align: left;
    }

EOD;

$cs->registerCss($this->id . 'css', $css);
?>
