<?php
/* @var $this SiteController */

$this->pageTitle='Drill';
$this->breadcrumbs=array(
	'Drill',
);
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">		    			
			<h3 class="page-title">POSTDRILL PROSPECT</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Input RPS</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Drill</strong></a><span class="divider-last">&nbsp;</span></li>
			</ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
        <?php $form = $this->beginWidget('CActiveForm', array(
        	'id'=>'createpostdrill-form',
        	'enableClientValidation'=>true,
        	'clientOptions'=>array(
        		'validateOnSubmit'=>true,
			),
        ));?>
        <?php //echo $form->errorSummary(array($mdlProspect, $mdlPostdrill, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));?>
        <div id="pesan"></div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> GENERAL DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <div class="accordion" id="accordion1a">
                                <!-- Begin Data Prospect Administration Data -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <strong>Prospect Administration Data</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Prospect Name :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="d2" type="text" class="span6 popovers" data-trigger="hover" data-container="body" data-content="Postdrill Prospect should already in one structure and Postdrill Prospect name should unique within Working Area." data-original-title="Prospect Name"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_name', array('id'=>'d2', 'class'=>'span6 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Postdrill Prospect should already in one structure and Postdrill Prospect name should unique within Working Area.', 'data-original-title'=>'Prospect Name'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Play Name :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="play_in" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php echo $form->dropDownList($mdlProspect, 'play_id', CHtml::listData( Play::model()->with(array('gcfs'=>array('condition'=>'wk_id="' . Yii::app()->user->name . '"')))->findAll(), 'play_id', 'gcfs.playName' ), array('empty'=>'-- Select --',  'id'=>'plyName', 'class'=>'span5'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Clarified by :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="_clarifiedby" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_clarified', array('id'=>'_clarifiedby', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Seismic Year :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="_seismicyear" type="text" class="span3 popovers" data-trigger="hover" data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012" data-original-title="Seismic Year" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_year_seismic', array('id'=>'_seismicyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012', 'data-original-title'=>'Seismic Year'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Processing Type :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="_processingtype" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_processing_type', array('id'=>'_processingtype', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Latest Year of Processing :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="_latestyearpro" type="text" class="span3 popovers" data-trigger="hover" data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012" data-original-title="Latest Year of Processing" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_year_late', array('id'=>'_latestyearpro', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012', 'data-original-title'=>'atest Year of Processing'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Revised Prospect Name :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="_revisedprosname" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlPostdrill, 'pd_name_revise', array('id'=>'_revisedprosname', 'class'=>'span3'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Prospect Name Revised Date :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input id="d9" type="text" class="m-wrap medium popovers" data-trigger="hover" style="max-width:172px;" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Revised Date"/> -->
                                                        <?php echo $form->textField($mdlPostdrill, 'pd_name_revise_date', array('id'=>'d9', 'class'=>'m-wrap medium popovers', 'data-trigger'=>'hover', 'style'=>'max-width:172px;', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Prospect Name Revised Date'));?>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Prospect Name Initiation Date :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input id="d7" type="text" class="m-wrap medium popovers" data-trigger="hover" style="max-width:172px;" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Initiation Date"/> -->
                                                        <?php echo $form->textField($mdlProspect, 'prospect_date_initiate', array('id'=>'d7', 'class'=>'m-wrap medium popovers', 'data-trigger'=>'hover', 'style'=>'max-width:172px;', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Prospect Name Initiation Date'));?>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Prospect Administration Data -->
                                <!-- Begin Geographical Prospect Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2_drill">
                                            <strong>Geographical Prospect Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2_drill" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Center Latitude :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                        <tr class="">
                                                        <td>
                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div>-->
                                                            <!-- <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"></span> -->
                                                            <div class=" input-append">
                                                        		<?php echo $form->textField($mdlProspect, 'center_lat_degree', array('class'=>'input-mini', 'placeholder'=>'degree'));?>
                                                        		<span class="add-on"><sub>o</sub></span>
	                                                        </div>
	                                                        <div class=" input-append">
	                                                        	<?php echo $form->textField($mdlProspect, 'center_lat_minute', array('class'=>'input-mini', 'placeholder'=>'minute'));?>
	                                                        	<span class="add-on">'</span>
	                                                        </div>
	                                                        <div class=" input-append">
	                                                        	<?php echo $form->textField($mdlProspect, 'center_lat_second', array('class'=>'input-mini', 'placeholder'=>'second'));?>
	                                                        	<span class="add-on">"</span>
	                                                        </div>
                                                       		<?php echo $form->textField($mdlProspect, 'center_lat_direction', array('class'=>'input-mini', 'placeholder'=>'S/ N'));?>
                                                       		WGS '84
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Center Longitude :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                        <tr class="">
                                                        <td>
                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div> -->
                                                            <!-- <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on"></span> -->
                                                            <div class=" input-append">
                                                        		<?php echo $form->textField($mdlProspect, 'center_long_degree', array('class'=>'input-mini', 'placeholder'=>'degree'));?>
                                                        	<span class="add-on"><sub>o</sub></span>
	                                                        </div>
	                                                        <div class=" input-append">
	                                                        	<?php echo $form->textField($mdlProspect, 'center_long_minute', array('class'=>'input-mini', 'placeholder'=>'minute'));?>
	                                                        	<span class="add-on">'</span>
	                                                        </div>
	                                                        <div class=" input-append">
	                                                        	<?php echo $form->textField($mdlProspect, 'center_long_second', array('class'=>'input-mini', 'placeholder'=>'second'));?>
	                                                        	<span class="add-on">"</span>
	                                                        </div>
                                                        	<?php echo $form->textField($mdlProspect, 'center_long_direction', array('class'=>'input-mini', 'placeholder'=>'E/ W'));?>
                                                        	WGS '84
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Geographical Prospect Area -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <strong>Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Onshore or Offshore :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_shore', array('id'=>'env_onoffshore', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Terrain :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_terrain', array('id'=>'env_terrain', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4">
                                            <strong>Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Facility :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/> -->
                                                        <?php echo $form->textField($mdlProspect, 'prospect_near_facility', array('id'=>'n_facility', 'class'=>'popovers', 'style'=>'max-width:165px', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Estimated range to the nearest facility, if more than 100 Km, leave it blank.', 'data-original-title'=>'Nearby Facility'));?>
                                                        <span class="add-on">Km</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Development Well :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input name="custom" type="text" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/> -->
                                                        <?php echo $form->textField($mdlProspect, 'prospect_near_dev_well', array('id'=>'n_developmentwell', 'class'=>'popovers', 'style'=>'max-width:165px', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Estimated range to the nearest development well, if more than 100 Km, leave it blank.', 'data-original-title'=>'Nearby Development Well'));?>
                                                        <span class="add-on">Km</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Discovery Exploration Well :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input name="custom" type="text" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest exploration development well, if more than 100 km, leave it blank." data-original-title="Nearby Discovery Exploration Well"/> -->
                                                        <?php echo $form->textField($mdlProspect, 'prospect_near_exr_well', array('id'=>'n_exploration_developmentwell', 'class'=>'popovers', 'style'=>'max-width:165px', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Estimated range to the nearest exploration development well, if more than 100 km, leave it blank.', 'data-original-title'=>'Nearby Discovery Exploration Well'));?>
                                                        <span class="add-on">Km</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Oil & Gas Infrastructure :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest Oil & Gas Infrastructure, if more thatn 100 km, leave it blank." data-original-title="Nearby Oil & Gas Infrastructure"/> -->
                                                        <?php echo $form->textField($mdlPostdrill, 'pd_near_infra', array('id'=>'n_infra', 'class'=>'popovers', 'style'=>'max-width:165px', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Estimated range to the nearest Oil & Gas Infrastructure, if more thatn 100 km, leave it blank.', 'data-original-title'=>'Nearby Oil & Gas Infrastructure'));?>
                                                        <span class="add-on">Km</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div id="1_well_discovery" class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> WELL DATA AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div  class="widget-body">
                        <span action="#" class="form-horizontal">
                            <div id="wajib" class="control-group">
                                <label class="control-label"><strong>Number of Well :</strong></label>
                                <div class="controls">
                                    <!-- <input id="w" class="span3" type="text" style="text-align: center;"/> -->
                                    <?php echo $form->textField($mdlPostdrill, 'pd_total_well', array('id'=>'w', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="controls">
                                    <blockquote>
                                        <small>Only well that drilled this year should submitted in this form, any other well like not drilled this year or non-active well can be submitted in Well A2 form. Detail of well data can be filled at generated form below.</small>
                                    </blockquote>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div id="rumah_well" class="tabbable tabbable-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a id="well_1" href="#tab_w1" data-toggle="tab">Well 1</a></li>
                                    <li><a id="well_2" href="#tab_w2" data-toggle="tab" class="hidden">Well 2</a></li>
                                    <li><a id="well_3" href="#tab_w3" data-toggle="tab" class="hidden">Well 3</a></li>
                                    <li><a id="well_4" href="#tab_w4" data-toggle="tab" class="hidden">Well 4</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- Well 1 -->
                                    <?php $a = 0;
                                    	$total = 0;
                                    	$i = 1;
                                    ?>
                                    <?php $ac = 'active';?>
                                    <?php //for($i = 1; $i <= 4; $i++) { ?>
                                    <?php foreach ($mdlWellPostdrills as $j => $mdlWellPostdrill) {?>
                                    <div class="tab-pane <?php if($i == 1) echo $ac;?>" id="tab_w<?=$i;?>">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <!-- <input id="well_name" type="text" name="wajib" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_name", array('id'=>'well_name', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Please using unique and meaningful well name.', 'data-original-title'=>'Well Name'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                                                <!-- <input type="text" name="wajib" class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
                                                                <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]lat_degree", array('class'=>'input-mini', 'placeholder'=>'degree'));?>
		                                                        	<span class="add-on"><sub>o</sub></span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]lat_minute", array('class'=>'input-mini', 'placeholder'=>'minute'));?>
		                                                        	<span class="add-on">'</span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]lat_second", array('class'=>'input-mini', 'placeholder'=>'second'));?>
		                                                        	<span class="add-on">"</span>
		                                                        </div>
		                                                        <?php echo $form->textField($mdlWellPostdrill, "[$i]lat_direction", array('class'=>'input-mini', 'placeholder'=>'S/ N'));?>
		                                                        WGS '84
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                                                <!-- <input type="text" name="wajib" class="input-mini" placeholder="E/ W"/><span class="add-on"> -->
                                                                <div class=" input-append">
	                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]long_degree", array('class'=>'input-mini', 'placeholder'=>'degree'));?>
	                                                        	<span class="add-on"><sub>o</sub></span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]long_minute", array('class'=>'input-mini', 'placeholder'=>'minute'));?>
		                                                        	<span class="add-on">'</span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]long_second", array('class'=>'input-mini', 'placeholder'=>'second'));?>
		                                                        	<span class="add-on">"</span>
		                                                        </div>
	                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]long_direction", array('class'=>'input-mini', 'placeholder'=>'E/ W'));?>
	                                                        	WGS '84
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="well_category" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_category", array('id'=>'well_category', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="well_type" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_type", array('id'=>"well_type$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="onoffshore" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_shore", array('id'=>"onoffshore$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="terrain" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_terrain", array('id'=>"terrain$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <!-- <input id="well_status" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_status", array('id'=>"well_status$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <!-- <input id="targeted_forname" class="span3" type="text" name="wajib" style="text-align: center;"/>  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_formation", array('id'=>"targeted_forname$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_date_complete", array('id'=>"dsw8_$i", 'class'=>'m-wrap medium popovers', 'style'=>'max-width:172px;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Date Well Completed'));?>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="ttd_1" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_target_depth_tvd", array('id'=>'ttd_1', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <!-- <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_target_depth_md", array('id'=>'ttd_2', 'style'=>'max-width:160px; margin-left:24px;'));?>
                                                                    <span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="actual_total_depth" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_actual_depth", array('id'=>'actual_total_depth', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <!-- <input id="tpprt_" type="text" style="max-width:160px"/>  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_target_play", array('id'=>'tpprt_', 'style'=>'max-width:160px'));?>
                                                                <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="apprt_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_actual_play", array('id'=>'apprt_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <!-- <input id="num_mdt" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_number_mdt", array('id'=>"num_mdt$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <!-- <input id="num_rft" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_number_rft", array('id'=>"num_rft$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="rip_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_res_pressure", array('id'=>'rip_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="lpr_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_last_pressure", array('id'=>'lpr_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="pressure_gradient" type="text" style="max-width:147px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_pressure_gradient", array('id'=>'pressure_gradient', 'style'=>'max-width:147px'));?>
                                                                    <span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="lrt_" type="text" style="max-width:170px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_last_temp", array('id'=>'lrt_', 'style'=>'max-width:170px'));?>
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="actual_well_integrity" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_integrity", array('id'=>"actual_well_integrity$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <!-- <input id="availability_electrolog" class="span3" style="text-align: center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_avail", array('id'=>"availability_electrolog$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list1", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list2", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list3", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list4", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list5", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list6", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list7", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list8", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list9", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list10", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list11", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list12", array('class'=>'span3'));?>
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
																<!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list13", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list14", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list15", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list16", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <!-- <input id="oilshow_reading1" name="wajib" class="span3" type="text"/> -->
                                                                <!-- <input id="oilshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />--> 
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_oil_show", array('id'=>'oilshow_reading1', 'class'=>'span3'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_oil_show_tools", array('id'=>'oilshow_reading2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <!-- <input id="gasshow_reading1" name="wajib" class="span3" type="text"/>-->
                                                                <!-- <input id="gasshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />-->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_gas_show", array('id'=>'gasshow_reading2', 'class'=>'span3'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_gas_show_tools", array('id'=>'gasshow_reading2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="wellmaking_watercut" type="text" style="max-width:172px"/>-->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_cut", array('id'=>'wellmaking_watercut', 'style'=>'max-width:172px'));?>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span>
                                                                    	<!-- <input id="waterbearing_levdept1" type="text" style="max-width:127px"/>-->
                                                                    	<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_gwc", array('id'=>'waterbearing_levdept1', 'style'=>'max-width:127px'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                                <!-- <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_gwd_tools", array('id'=>'waterbearing_levdept2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span>
                                                                    	<!-- <input id="waterbearing_levdept3" type="text" style="max-width:127px"/> -->
                                                                    	<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_owc", array('id'=>'waterbearing_levdept3', 'style'=>'max-width:127px'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                                <!-- <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_owc_tools", array('id'=>'waterbearing_levdept4', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <!-- <input id="rocksampling" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_method", array('id'=>"rocksampling$i", 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="petrograp_analys" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_petro", array('id'=>"petrograp_analys$i", 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sample" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_petro_sample", array('id'=>'sample', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="ticsd_" type="text" style="max-width:152px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_top_depth", array('id'=>'ticsd_', 'style'=>'max-width:152px;'));?>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="bitcsd_" type="text" style="max-width:152px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_bot_depth", array('id'=>'bitcsd_', 'style'=>'max-width:152px;'));?>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="num_totalcarebarrel" type="text" style="max-width:170px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_barrel", array('id'=>'num_totalcarebarrel', 'style'=>'max-width:170px;'));?>
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="1corebarrel" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_barrel_equal", array('id'=>'1corebarrel', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="trcd_" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_total_core", array('id'=>'trcd_', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="pcd_" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_preservative", array('id'=>'pcd_', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="rca_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                <!-- <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_routine", array('id'=>'rca_1', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_routine_sample", array('id'=>'rca_2', 'class'=>'span3', 'placeholder'=>'Sample Quantity..'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="scal_data1" class="span3" type="text" style="text-align:center;" /> -->
                                                                <!-- <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_scal", array('id'=>'scal_data1', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_scal_sample", array('id'=>'scal_data2', 'class'=>'span3', 'placeholder'=>'Sample Quantity..'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/>-->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_thickness", array('id'=>"wpd_clastic_p10_thickness$i", 'class'=>'span6 clastic_grtp10'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_gr", array('id'=>"wpd_clastic_p10_gr$i", 'class'=>'span6 clastic_rvcp10'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_sp", array('id'=>"wpd_clastic_p10_sp$i", 'class'=>'span6 clastic_rvc2p10'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_net", array('id'=>"wpd_clastic_p10_net$i", 'class'=>'span6 clastic_ntgp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_por", array('id'=>"wpd_clastic_p10_por$i", 'class'=>'span6 clastic_porp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_satur", array('id'=>"wpd_clastic_p10_satur$i", 'class'=>'span6 clastic_saturp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_thickness", array('id'=>"wpd_clastic_p50_thickness$i", 'class'=>'span6 clastic_grtp50'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_gr", array('id'=>"wpd_clastic_p50_gr$i", 'class'=>'span6 clastic_rvcp50'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_sp", array('id'=>"wpd_clastic_p50_sp$i", 'class'=>'span6 clastic_rvc2p50'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_net", array('id'=>"wpd_clastic_p50_net$i", 'class'=>'span6 clastic_ntgp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_por", array('id'=>"wpd_clastic_p50_por$i", 'class'=>'span6 clastic_porp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append"> 
                                                                        		<!-- <input id="#" class="span6" type="text"> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_satur", array('id'=>"wpd_clastic_p50_satur$i", 'class'=>'span6 clastic_saturp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_thickness", array('id'=>"wpd_clastic_p90_thickness$i", 'class'=>'span6 clastic_grtp90'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_gr", array('id'=>"wpd_clastic_p90_gr$i", 'class'=>'span6 clastic_rvcp90'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_sp", array('id'=>"wpd_clastic_p90_sp$i", 'class'=>'span6 clastic_rvc2p90'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_net", array('id'=>"wpd_clastic_p90_net$i", 'class'=>'span6 clastic_ntgp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_por", array('id'=>"wpd_clastic_p90_por$i", 'class'=>'span6 clastic_porp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_satur", array('id'=>"wpd_clastic_p90_satur$i", 'class'=>'span6 clastic_saturp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_grt<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_rvc<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_rvc2<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_ntg<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_por<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_satur<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_grt<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_rvc<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_rvc2<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_ntg<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_por<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</i></span></td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_satur<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_thickness", array('id'=>"wpd_carbo_p10_thickness$i", 'class'=>'span6 carbo_grtp10'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_dtc", array('id'=>"wpd_carbo_p10_dtc$i", 'class'=>'span6 carbo_dtcp10'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_total", array('id'=>"wpd_carbo_p10_total$i", 'class'=>'span6 carbo_totalp10'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_net", array('id'=>"wpd_carbo_p10_net$i", 'class'=>'span6 carbo_netp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_por", array('id'=>"wpd_carbo_p10_por$i", 'class'=>'span6 carbo_porp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_satur", array('id'=>"wpd_carbo_p10_satur$i", 'class'=>'span6 carbo_saturp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_thickness", array('id'=>"wpd_carbo_p50_thickness$i", 'class'=>'span6 carbo_grtp50'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_dtc", array('id'=>"wpd_carbo_p50_dtc$i", 'class'=>'span6 carbo_dtcp50'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_total", array('id'=>"wpd_carbo_p50_total$i", 'class'=>'span6 carbo_totalp50'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_net", array('id'=>"wpd_carbo_p50_net$i", 'class'=>'span6 carbo_netp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_por", array('id'=>"wpd_carbo_p50_por$i", 'class'=>'span6 carbo_porp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text">  -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_satur", array('id'=>"wpd_carbo_p50_satur$i", 'class'=>'span6 carbo_saturp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_thickness", array('id'=>"wpd_carbo_p90_thickness", 'class'=>'span6 carbo_grtp90'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_dtc", array('id'=>"wpd_carbo_p90_dtc$i", 'class'=>'span6 carbo_dtcp90'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_total", array('id'=>"wpd_carbo_p90_total$i", 'class'=>'span6 carbo_totalp90'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_net", array('id'=>"wpd_carbo_p90_net$i", 'class'=>'span6 carbo_netp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_por", array('id'=>"wpd_carbo_p90_por$i", 'class'=>'span6 carbo_porp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_satur", array('id'=>"wpd_carbo_p90_satur$i", 'class'=>'span6 carbo_saturp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_grt<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_dtc<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_total<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_net<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_por<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_satur<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_grt<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_dtc<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_total<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_net<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_por<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_satur<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="z1_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_date", array('id'=>"z_1_5_$i", 'class'=>'m-wrap medium popovers', 'style'=>'max-width:172px;', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Sample Date'));?>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="sample_at" type="text" style="max-width:172px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_sample", array('id'=>'sample_at', 'style'=>'max-width:172px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="gas_oil_ratio" type="text" style="max-width:145px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_ratio", array('id'=>'gas_oil_ratio', 'style'=>'max-width:145px;'));?>
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="separator_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_pressure", array('id'=>'separator_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="separator_temperature" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_temp", array('id'=>'separator_temperature', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="tubing_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_tubing", array('id'=>'tubing_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="casing_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_casing", array('id'=>'casing_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sampleby" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_by", array('id'=>'sampleby', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <!-- <input id="report_avail" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_report", array('id'=>'report_avail', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="hydro_finger" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_finger", array('id'=>'hydro_finger', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="oilgravity_60" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_grv_oil", array('id'=>'oilgravity_60', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="gasgravity_60" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_grv_gas", array('id'=>'gasgravity_60', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="condensate_gravity" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_grv_conden", array('id'=>'condensate_gravity', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_pvt", array('id'=>'pvt_analysis', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sample_" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_quantity", array('id'=>'sample_', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <!-- <input id="gas_initialZ" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_z", array('id'=>'gas_initialZ', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_oil_p10", array('id'=>'wpd_fvf_oil_p10', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_oil_p50", array('id'=>'wpd_fvf_oil_p50', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_oil_p90", array('id'=>'wpd_fvf_oil_p90', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_gas_p10", array('id'=>'wpd_fvf_gas_p10', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_gas_p50", array('id'=>'wpd_fvf_gas_p50', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_gas_p90", array('id'=>'wpd_fvf_gas_p90', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="oil_viscocity" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_viscocity", array('id'=>'oil_viscocity', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++;?>
                                    <?php $a = 0;?>
                                    <?php $total++;?>
                                    <?php }?>
                                    <!-- Well 3 -->
                                    
                                    <?php if($total != 4) {;?>
                                    <?php $mdlWellPostdrill = new WellPostdrill();?>
                                    <?php $a = 0;?>
                                    <?php $ac = 'active';?>
                                    <?php for($i = ++$total; $i <= 4; $i++) { ?>
                                    <div class="tab-pane <?php if($i == 1) echo $ac;?>" id="tab_w<?=$i;?>">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <!-- <input id="well_name" type="text" name="wajib" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_name", array('id'=>"well_name", 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Please using unique and meaningful well name.', 'data-original-title'=>'Well Name'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                                                <!-- <input type="text" name="wajib" class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
                                                                <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]lat_degree", array('class'=>'input-mini', 'placeholder'=>'degree'));?>
		                                                        	<span class="add-on"><sub>o</sub></span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]lat_minute", array('class'=>'input-mini', 'placeholder'=>'minute'));?>
		                                                        	<span class="add-on">'</span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]lat_second", array('class'=>'input-mini', 'placeholder'=>'second'));?>
		                                                        	<span class="add-on">"</span>
		                                                        </div>
		                                                        <?php echo $form->textField($mdlWellPostdrill, "[$i]lat_direction", array('class'=>'input-mini', 'placeholder'=>'S/ N'));?>
		                                                        WGS '84
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                                                <!-- <input type="text" name="wajib" class="input-mini" placeholder="E/ W"/><span class="add-on"> -->
                                                                <div class=" input-append">
	                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]long_degree", array('class'=>'input-mini', 'placeholder'=>'degree'));?>
	                                                        	<span class="add-on"><sub>o</sub></span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]long_minute", array('class'=>'input-mini', 'placeholder'=>'minute'));?>
		                                                        	<span class="add-on">'</span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]long_second", array('class'=>'input-mini', 'placeholder'=>'second'));?>
		                                                        	<span class="add-on">"</span>
		                                                        </div>
	                                                        	<?php echo $form->textField($mdlWellPostdrill, "[$i]long_direction", array('class'=>'input-mini', 'placeholder'=>'E/ W'));?>
	                                                        	WGS '84
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="well_category" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_category", array('id'=>'well_category', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="well_type" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_type", array('id'=>"well_type$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="onoffshore" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_shore", array('id'=>"onoffshore$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="terrain" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_terrain", array('id'=>"terrain$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <!-- <input id="well_status" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_status", array('id'=>"well_status$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <!-- <input id="targeted_forname" class="span3" type="text" name="wajib" style="text-align: center;"/>  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_formation", array('id'=>"targeted_forname$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_date_complete", array('id'=>"dsw8_$i", 'class'=>'m-wrap medium popovers', 'style'=>'max-width:172px;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Date Well Completed'));?>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="ttd_1" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_target_depth_tvd", array('id'=>'ttd_1', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <!-- <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_target_depth_md", array('id'=>'ttd_2', 'style'=>'max-width:160px; margin-left:24px;'));?>
                                                                    <span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="actual_total_depth" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_actual_depth", array('id'=>'actual_total_depth', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <!-- <input id="tpprt_" type="text" style="max-width:160px"/>  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_target_play", array('id'=>'tpprt_', 'style'=>'max-width:160px'));?>
                                                                <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="apprt_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_actual_play", array('id'=>'apprt_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <!-- <input id="num_mdt" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_number_mdt", array('id'=>"num_mdt$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <!-- <input id="num_rft" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_number_rft", array('id'=>"num_rft$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="rip_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_res_pressure", array('id'=>'rip_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="lpr_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_last_pressure", array('id'=>'lpr_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="pressure_gradient" type="text" style="max-width:147px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_pressure_gradient", array('id'=>'pressure_gradient', 'style'=>'max-width:147px'));?>
                                                                    <span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="lrt_" type="text" style="max-width:170px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_last_temp", array('id'=>'lrt_', 'style'=>'max-width:170px'));?>
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="actual_well_integrity" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_integrity", array('id'=>"actual_well_integrity$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <!-- <input id="availability_electrolog" class="span3" style="text-align: center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_avail", array('id'=>"availability_electrolog$i", 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list1", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list2", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list3", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list4", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list5", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list6", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list7", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list8", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list9", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list10", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list11", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list12", array('class'=>'span3'));?>
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
																<!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list13", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list14", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list15", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_electro_list16", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <!-- <input id="oilshow_reading1" name="wajib" class="span3" type="text"/> -->
                                                                <!-- <input id="oilshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />--> 
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_oil_show", array('id'=>'oilshow_reading1', 'class'=>'span3'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_oil_show_tools", array('id'=>'oilshow_reading2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <!-- <input id="gasshow_reading1" name="wajib" class="span3" type="text"/>-->
                                                                <!-- <input id="gasshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />-->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_gas_show", array('id'=>'gasshow_reading2', 'class'=>'span3'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_gas_show_tools", array('id'=>'gasshow_reading2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="wellmaking_watercut" type="text" style="max-width:172px"/>-->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_cut", array('id'=>'wellmaking_watercut', 'style'=>'max-width:172px'));?>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span>
                                                                    	<!-- <input id="waterbearing_levdept1" type="text" style="max-width:127px"/>-->
                                                                    	<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_gwc", array('id'=>'waterbearing_levdept1', 'style'=>'max-width:127px'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                                <!-- <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_gwd_tools", array('id'=>'waterbearing_levdept2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span>
                                                                    	<!-- <input id="waterbearing_levdept3" type="text" style="max-width:127px"/> -->
                                                                    	<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_owc", array('id'=>'waterbearing_levdept3', 'style'=>'max-width:127px'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                                <!-- <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_hc_water_owc_tools", array('id'=>'waterbearing_levdept4', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <!-- <input id="rocksampling" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_method", array('id'=>"rocksampling$i", 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="petrograp_analys" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_petro", array('id'=>"petrograp_analys$i", 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sample" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_petro_sample", array('id'=>'sample', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="ticsd_" type="text" style="max-width:152px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_top_depth", array('id'=>'ticsd_', 'style'=>'max-width:152px;'));?>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="bitcsd_" type="text" style="max-width:152px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_bot_depth", array('id'=>'bitcsd_', 'style'=>'max-width:152px;'));?>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="num_totalcarebarrel" type="text" style="max-width:170px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_barrel", array('id'=>'num_totalcarebarrel', 'style'=>'max-width:170px;'));?>
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="1corebarrel" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_barrel_equal", array('id'=>'1corebarrel', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="trcd_" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_total_core", array('id'=>'trcd_', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="pcd_" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_preservative", array('id'=>'pcd_', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="rca_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                <!-- <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_routine", array('id'=>'rca_1', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_routine_sample", array('id'=>'rca_2', 'class'=>'span3', 'placeholder'=>'Sample Quantity..'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="scal_data1" class="span3" type="text" style="text-align:center;" /> -->
                                                                <!-- <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_scal", array('id'=>'scal_data1', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_rock_scal_sample", array('id'=>'scal_data2', 'class'=>'span3', 'placeholder'=>'Sample Quantity..'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/>-->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_thickness", array('id'=>"wpd_clastic_p10_thickness$i", 'class'=>'span6 clastic_grtp10'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_gr", array('id'=>"wpd_clastic_p10_gr$i", 'class'=>'span6 clastic_rvcp10'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_sp", array('id'=>"wpd_clastic_p10_sp$i", 'class'=>'span6 clastic_rvc2p10'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_net", array('id'=>"wpd_clastic_p10_net$i", 'class'=>'span6 clastic_ntgp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_por", array('id'=>"wpd_clastic_p10_por$i", 'class'=>'span6 clastic_porp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p10_satur", array('id'=>"wpd_clastic_p10_satur$i", 'class'=>'span6 clastic_saturp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_thickness", array('id'=>"wpd_clastic_p50_thickness$i", 'class'=>'span6 clastic_grtp50'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_gr", array('id'=>"wpd_clastic_p50_gr$i", 'class'=>'span6 clastic_rvcp50'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_sp", array('id'=>"wpd_clastic_p50_sp$i", 'class'=>'span6 clastic_rvc2p50'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_net", array('id'=>"wpd_clastic_p50_net$i", 'class'=>'span6 clastic_ntgp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_por", array('id'=>"wpd_clastic_p50_por$i", 'class'=>'span6 clastic_porp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append"> 
                                                                        		<!-- <input id="#" class="span6" type="text"> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p50_satur", array('id'=>"wpd_clastic_p50_satur$i", 'class'=>'span6 clastic_saturp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_thickness", array('id'=>"wpd_clastic_p90_thickness$i", 'class'=>'span6 clastic_grtp90'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_gr", array('id'=>"wpd_clastic_p90_gr$i", 'class'=>'span6 clastic_rvcp90'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_sp", array('id'=>"wpd_clastic_p90_sp$i", 'class'=>'span6 clastic_rvc2p90'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_net", array('id'=>"wpd_clastic_p90_net$i", 'class'=>'span6 clastic_ntgp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_por", array('id'=>"wpd_clastic_p90_por$i", 'class'=>'span6 clastic_porp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_clastic_p90_satur", array('id'=>"wpd_clastic_p90_satur$i", 'class'=>'span6 clastic_saturp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_grt<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_rvc<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_rvc2<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_ntg<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_por<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_estimate_satur<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_grt<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_rvc<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_rvc2<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_ntg<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_por<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</i></span></td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="clastic_average_satur<?=$i?>" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_thickness", array('id'=>"wpd_carbo_p10_thickness$i", 'class'=>'span6 carbo_grtp10'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_dtc", array('id'=>"wpd_carbo_p10_dtc$i", 'class'=>'span6 carbo_dtcp10'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_total", array('id'=>"wpd_carbo_p10_total$i", 'class'=>'span6 carbo_totalp10'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_net", array('id'=>"wpd_carbo_p10_net$i", 'class'=>'span6 carbo_netp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_por", array('id'=>"wpd_carbo_p10_por$i", 'class'=>'span6 carbo_porp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p10_satur", array('id'=>"wpd_carbo_p10_satur$i", 'class'=>'span6 carbo_saturp10'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_thickness", array('id'=>"wpd_carbo_p50_thickness$i", 'class'=>'span6 carbo_grtp50'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_dtc", array('id'=>"wpd_carbo_p50_dtc$i", 'class'=>'span6 carbo_dtcp50'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_total", array('id'=>"wpd_carbo_p50_total$i", 'class'=>'span6 carbo_totalp50'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_net", array('id'=>"wpd_carbo_p50_net$i", 'class'=>'span6 carbo_netp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_por", array('id'=>"wpd_carbo_p50_por$i", 'class'=>'span6 carbo_porp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text">  -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p50_satur", array('id'=>"wpd_carbo_p50_satur$i", 'class'=>'span6 carbo_saturp50'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_thickness", array('id'=>"wpd_carbo_p90_thickness$i", 'class'=>'span6 carbo_grtp90'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_dtc", array('id'=>"wpd_carbo_p90_dtc$i", 'class'=>'span6 carbo_dtcp90'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_total", array('id'=>"wpd_carbo_p90_total$i", 'class'=>'span6 carbo_totalp90'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_net", array('id'=>"wpd_carbo_p90_net$i", 'class'=>'span6 carbo_netp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_por", array('id'=>"wpd_carbo_p90_por$i", 'class'=>'span6 carbo_porp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_carbo_p90_satur", array('id'=>"wpd_carbo_p90_satur$i", 'class'=>'span6 carbo_saturp90'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_grt<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_dtc<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_total<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_net<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_por<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_estimate_satur<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_grt<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_dtc<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_total<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_net<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_por<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="carbo_average_satur<?=$i?>" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="z1_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_date", array('id'=>"z_1_5_$i", 'class'=>'m-wrap medium popovers', 'style'=>'max-width:172px;', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Sample Date'));?>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="sample_at" type="text" style="max-width:172px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_sample", array('id'=>'sample_at', 'style'=>'max-width:172px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="gas_oil_ratio" type="text" style="max-width:145px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_ratio", array('id'=>'gas_oil_ratio', 'style'=>'max-width:145px;'));?>
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="separator_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_pressure", array('id'=>'separator_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="separator_temperature" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_temp", array('id'=>'separator_temperature', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="tubing_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_tubing", array('id'=>'tubing_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="casing_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_casing", array('id'=>'casing_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sampleby" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_by", array('id'=>'sampleby', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <!-- <input id="report_avail" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_report", array('id'=>'report_avail', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="hydro_finger" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_finger", array('id'=>'hydro_finger', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="oilgravity_60" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_grv_oil", array('id'=>'oilgravity_60', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="gasgravity_60" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_grv_gas", array('id'=>'gasgravity_60', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="condensate_gravity" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_grv_conden", array('id'=>'condensate_gravity', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_pvt", array('id'=>'pvt_analysis', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sample_" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_quantity", array('id'=>'sample_', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <!-- <input id="gas_initialZ" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fluid_z", array('id'=>'gas_initialZ', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_oil_p10", array('id'=>"wpd_fvf_oil_p10$i", 'class'=>'span6 initial_oilp10'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_oil_p50", array('id'=>"wpd_fvf_oil_p50$i", 'class'=>'span6 initial_oilp50'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_oil_p90", array('id'=>"wpd_fvf_oil_p90$i", 'class'=>'span6 initial_oilp90'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="initial_estimate_oil<?=$i?>" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="initial_average_oil<?=$i?>" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_gas_p10", array('id'=>"wpd_fvf_gas_p10$i", 'class'=>'span6 initial_gasp10'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_gas_p50", array('id'=>"wpd_fvf_gas_p50$i", 'class'=>'span6 initial_gasp50'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_fvf_gas_p90", array('id'=>"wpd_fvf_gas_p90$i", 'class'=>'span6 initial_gasp90'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="initial_estimate_gas<?=$i?>" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="initial_average_gas<?=$i?>" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="oil_viscocity" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrill, "[$i]wpd_viscocity", array('id'=>'oil_viscocity', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $a = 0;?>
                                    <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN OCCURANCE SURVEY DESCRIPTION-->
                <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-list"></i> SURVEY DATA AVAILABILITY</h4>
                    <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                </div>
                <div class="widget-body">
                    <span action="#" class="form-horizontal">
                        <!-- Notification -->
                        <div class="control-group">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert">×</button>
                                <strong>Info! </strong> 3D Seismic Data is requirement to be called Postdrill Prospect.
                            </div>
                        </div>
                        <!-- Notification -->      
                        <div class="accordion">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlGfs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGfs','DlGfs_link','col_dav1')" /> 
                                        <a id="DlGfs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">Geological Field Survey</a>
                                    </div>
                                </div>
                        		<div id="col_dav1" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_gfs_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlGeological, 'sgf_year_survey', array('id'=>'dav_gfs_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Method :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_gfs_surmethod" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlGeological, 'sgf_survey_method', array('id'=>'dav_gfs_surmethod', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Coverage Area : </label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_gfs_tca" type="text" style="max-width:170px"/> -->
                                                    <?php echo $form->textField($mdlGeological, 'sgf_coverage_area', array('id'=>'dav_gfs_tca', 'style'=>'max-width:170px'));?>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Volumetric Estimation</th>
                                                        <th>Areal Closure Estimation</th>
                                                        <th>Gross Sand Thickness</th>
                                                        <th>Net to Gross</th>
                                                        <th>Porosity</th>
                                                        <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                        <th>Oil Case</th>
                                                        <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th><strong>P90</strong></th>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_area', array('id'=>'sgf_p90_area', 'class'=>'span6'));?>
                                                        	<span class="add-on">ac</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_thickness', array('id'=>'sgf_p90_thickness', 'class'=>'span6'));?>
                                                        	<span class="add-on">ft</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_net', array('id'=>'sgf_p90_net', 'class'=>'span6'));?>
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p90_porosity" name="custom" class="span6" type="text" readonly/>
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p90_sw" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p90_oil" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">MMBO</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p90_gas" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">BCF</span>
                                                        	</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th><strong>P50</strong></th>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_area', array('id'=>'sgf_p50_area', 'class'=>'span6'));?>
                                                        	<span class="add-on">ac</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_thickness', array('id'=>'sgf_p50_thickness', 'class'=>'span6'));?>
                                                        	<span class="add-on">ft</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_net', array('id'=>'sgf_p50_net', 'class'=>'span6'));?>
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p50_porosity" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p50_sw" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p50_oil" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">MMBO</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p50_gas" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">BCF</span>
                                                        	</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th><strong>P10</strong></th>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_area', array('id'=>'sgf_p10_area', 'class'=>'span6'));?>
                                                        	<span class="add-on">ac</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_thickness', array('id'=>'sgf_p10_thickness', 'class'=>'span6'));?>
                                                        	<span class="add-on">ft</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_net', array('id'=>'sgf_p10_net', 'class'=>'span6'));?>
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p10_porosity" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p10_sw" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">%</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p10_oil" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">MMBO</span>
                                                        	</div>
                                                        </td>
                                                        <td>
                                                        	<div class=" input-append">
                                                        		<input id="sgf_p10_gas" name="custom" class="span6" type="text" readonly />
                                                        	<span class="add-on">BCF</span>
                                                        	</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_gfs_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGeological, 'sgf_remark', array('id'=>'dav_gfs_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        	<div class="accordion-group">
                        		<div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlDss2" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss2','DlDss2_link','col_dav2' )"/> 
                                        <a id="DlDss2_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2"> 2D Seismic Survey</a>
                                    </div>
                        		</div>
                        		<div id="col_dav2" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year:</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_year_survey', array('id'=>'dav_2dss_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number Of Vintage :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_numvin" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_vintage_number', array('id'=>'dav_2dss_numvin', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Crossline Number :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_tcn" type="text" class="span3"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_total_crossline', array('id'=>'dav_2dss_tcn', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Line Total Length :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_sltl" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_seismic_line', array('id'=>'dav_2dss_sltl', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Average Spacing Parallel Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_aspi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_average_interval', array('id'=>'dav_2dss_aspi', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Record Length :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_rl1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_record_length_ms', array('id'=>'dav_2dss_rl1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ms</span>
                                                </div>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_rl2" type="text" style="max-width:170px; margin-left:24px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_record_length_ft', array('id'=>'dav_2dss_rl2', 'style'=>'max-width:170px; margin-left:24px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Year Of Latest Processing :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_yearlatest" class="span3" type="text"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_year_late_process', array('id'=>'dav_2dss_yearlatest', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Processing Method :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_lpm" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_late_method', array('id'=>'dav_2dss_lpm', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Image Quality :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_siq" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_img_quality', array('id'=>'dav_2dss_siq', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Top Reserovir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_trtd1" type="text" style="max-width:157px"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_top_depth_ft', array('id'=>'dav_2dss_trtd1', 'style'=>'max-width:157px'));?>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_top_depth_ms', array('id'=>'dav_2dss_trtd2', 'style'=>'max-width:170px; margin-left:24px;'));?>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Bottom Reservoir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_brtd1" type="text" style="max-width:157px"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_bot_depth_ft', array('id'=>'dav_2dss_brtd1', 'style'=>'max-width:157px'));?>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_bot_depth_ms', array('id'=>'dav_2dss_brtd2', 'style'=>'max-width:170px; margin-left:24px;'));?>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_dcsp" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_spill', array('id'=>'dav_2dss_dcsp', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <!-- <input id="dav_2dss_owc1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate', array('id'=>'dav_2dss_owc1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <!-- <input id="dlosd2m2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate_analog', array('id'=>'dlosd2m2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                <div class="input-append">
                                                    <!-- <input id="dav_2dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate_spill', array('id'=>'dav_2dss_owc2', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estiamte Oil or Gas :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_oilgas1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_low', array('id'=>'dav_2dss_oilgas1', 'style'=>'max-width:170px;'));?>
                                                <span class="add-on">ft</span>
                                                </div>
                                                	<!-- <input id="dlosd2n2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/> -->
                                                	<?php echo $form->textField($mdlSeismic2d, 's2d_depth_low_analog', array('id'=>'dlosd2n2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By analog to'));?>
                                                <div class=" input-append">
                                                    <!-- <input id="oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_low_spill', array('id'=>'oilgas2', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
                                                <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Formation Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_forthickness" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_formation_thickness', array('id'=>'dav_2dss_forthickness', 'style'=>'max-width:170px;'));?>
                                                <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Gross Sand or Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_2dss_gross_resthickness" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_gross_thickness', array('id'=>'dav_2dss_gross_resthickness', 'style'=>'max-width:170px;'));?>
                                                <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability Net Thickness Reservoir Pay :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_2dss_antrip" class="span3" type="text" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic2d, 's2d_avail_pay', array('id'=>'dav_2dss_antrip', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Pay Zone Thickness/ Net Pay</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Net Pay to Gross Sand</th>
                                                <th>Vsh Cut-off</th>
                                                <th>Porosity Cut-off</th>
                                                <th>Saturation Cut-Off</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_thickness', array('id'=>'s2d_net_p90_thickness', 'class'=>'span6'));?>
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_net_p90_net_to_gross" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_vsh', array('id'=>'s2d_net_p90_vsh', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_por', array('id'=>'s2d_net_p90_por', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_satur', array('id'=>'s2d_net_p90_satur', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_thickness', array('id'=>'s2d_net_p50_thickness', 'class'=>'span6'));?>
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_net_p50_net_to_gross" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_vsh', array('id'=>'s2d_net_p50_vsh', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_por', array('id'=>'s2d_net_p50_por', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_satur', array('id'=>'s2d_net_p50_satur', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_thickness', array('id'=>'s2d_net_p10_thickness', 'class'=>'span6'));?>
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_net_p10_net_to_gross" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_vsh', array('id'=>'s2d_net_p10_vsh', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_por', array('id'=>'s2d_net_p10_por', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_satur', array('id'=>'s2d_net_p10_satur', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p90_area', array('id'=>'s2d_vol_p90_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p90_thickness" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p90_porosity" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p90_sw" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p90_oil" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p90_gas" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p50_area', array('id'=>'s2d_vol_p50_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p50_thickness" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p50_porosity" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">%</span><here>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p50_sw" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p50_oil" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p50_gas" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p10_area', array('id'=>'s2d_vol_p10_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p10_thickness" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p10_porosity" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p10_sw" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p10_oil" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s2d_vol_p10_gas" name="custom" class="span6" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_2dss_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlSeismic2d, 's2d_remark', array('id'=>'dav_2dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlGras" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGras','DlGras_link','col_dav3' )"/> 
                                        <a id="DlGras_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3"> Gravity Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav3" class="accordion-body collapse">
                                    <div class="accordion-inner">
                        	            <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year:</label>
                                            <div class="controls">
                                                <!-- <input id="dav_grasur_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_year_survey', array('id'=>'dav_grasur_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition '));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_grasur_surveymethods" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_survey_method', array('id'=>'dav_grasur_surveymethods', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_sca" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_coverage_area', array('id'=>'dav_grasur_sca', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range:</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_dspr" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_range', array('id'=>'dav_grasur_dspr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_rsi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_spacing_interval', array('id'=>'dav_grasur_dspr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_dcsp" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_spill', array('id'=>'dav_grasur_dspr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_owc1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate', array('id'=>'dav_grasur_dspr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <!-- <input id="dlosd3g2" class="span3" type="text" placeholder="By analog to"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate_analog', array('id'=>'dlosd3g2', 'class'=>'span3', 'placeholder'=>'By analog to'));?>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_owc2" type="text" style="max-width:170px; margin-left:18px;"placeholder="Estimated From Spill Point"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate_spill', array('id'=>'dav_grasur_owc2', 'style'=>'max-width:170px; margin-left:18px;', 'placeholder'=>'Estimated From Spill Point'));?>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil or Gas</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_oilgas1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_low', array('id'=>'dav_grasur_oilgas1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <!-- <input id="dlosd3h2" class="span3" type="text" placeholder="By analog to"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_depth_low_analog', array('id'=>'dlosd3h2', 'class'=>'span3', 'placeholder'=>'By analog to'));?>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_oilgas2" type="text" style="max-width:170px; margin-left:18px;" placeholder="Estimated from Spill Point"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_low_spill', array('id'=>'dav_grasur_oilgas2', 'style'=>'max-width:170px; margin-left:18px;', 'placeholder'=>'Estimated from Spill Point'));?>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_resthickness1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_res_thickness', array('id'=>'dav_grasur_resthickness1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <!-- <input id="dav_grasur_resthickness2" class="span3" type="text" placeholder="According to"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_res_according', array('id'=>'dav_grasur_resthickness2', 'class'=>'span3', 'placeholder'=>'According to'));?>
                                                <!-- <input id="dav_grasur_resthickness3" class="span3" type="text" placeholder="Top Sand Seismic Depth" /> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_res_top_depth', array('id'=>'dav_grasur_resthickness3', 'class'=>'span3', 'placeholder'=>'Top Sand Seismic Depth'));?>
                                                <!-- <input id="dav_grasur_resthickness4" class="span3" type="text" placeholder="Bottom Sand Seismic Depth"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_res_bot_depth', array('id'=>'dav_grasur_resthickness4', 'class'=>'span3', 'placeholder'=>'Bottom Sand Seismic Depth'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Gross Sand Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_gst1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_gross_thickness', array('id'=>'dav_grasur_gst1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">%</span>
                                                </div>
                                                <!-- <input id="dav_grasur_gst2" class="span3" type="text" placeholder="According to"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_gross_according', array('id'=>'dav_grasur_gst2', 'class'=>'span3', 'placeholder'=>'According to'));?>
                                                <!-- <input id="dav_grasur_gst3" class="span6" type="text" placeholder="Well Name"/> -->
                                                <?php echo $form->textField($mdlGravity, 'sgv_gross_well', array('id'=>'dav_grasur_gst3', 'class'=>'span6', 'placeholder'=>'Well Name'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Net To Gross (Net Pay/Gross Sand) :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_grasur_netogross" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlGravity, 'sgv_net_gross', array('id'=>'dav_grasur_netogross', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Areal Closure Estimation</th>
                                                    <th>Net Pay Thickness</th>
                                                    <th>Porosity</th>
                                                    <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                    <th>Oil Case</th>
                                                    <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p90_area', array('id'=>'sgv_vol_p90_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p90_thickness" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p90_porosity" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p90_sw" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p90_oil" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p90_gas" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p50_area', array('id'=>'sgv_vol_p50_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p50_thickness" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p50_porosity" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p50_sw" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p50_oil" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p50_gas" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p10_area', array('id'=>'sgv_vol_p10_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p10_thickness" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p10_porosity" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p10_sw" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p10_oil" name="custom" class="span6" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sgv_vol_p10_gas" name="custom" class="span6" type="text" readonly/>
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_grasur_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGravity, 'sgv_remark', array('id'=>'dav_grasur_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlGeos" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGeos','DlGeos_link','col_dav4' )"/> 
                                        <a id="DlGeos_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4"> Geochemistry Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav4" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_year_survey', array('id'=>'dav_geo_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Interval Samples Range :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_geo_isr" type="text" style="max-width:170px;"/>-->
                                                    <?php echo $form->textField($mdlGeochemistry, 'sgc_sample_interval', array('id'=>'dav_geo_isr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Sample Location :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_numsample" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_sample', array('id'=>'dav_geo_numsample', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Rocks Sample :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_numrock" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_rock', array('id'=>'dav_geo_numrock', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Fluid Sample :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_numfluid" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_fluid', array('id'=>'dav_geo_numfluid', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability HC Composition :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_ahc" class="span3" style="text-align: center;" />-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_avail_hc', array('id'=>'dav_geo_ahc', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Hydrocarbon Composition : </label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_hycomp" type="text" class="span3"/>-->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_hc_composition', array('id'=>'dav_geo_hycomp', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Laboratorium Evaluation & Report Year</label>
                                            <div class="controls">
                                                <!-- <input id="dav_geo_lery" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" placeholder="angka,koma"/> -->
                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_lab_report', array('id'=>'dav_geo_lery', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year', 'placeholder'=>'angka,koma'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p90_area', array('id'=>'sgc_vol_p90_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p90_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p90_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p90_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p90_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p90_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p50_area', array('id'=>'sgc_vol_p50_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p50_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p50_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p50_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p50_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p50_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p10_area', array('id'=>'sgc_vol_p10_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p10_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p10_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p10_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p10_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sgc_vol_p10_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_geo_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGeochemistry, 'sgc_remark', array('id'=>'dav_geo_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlEs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlEs','DlEs_link','col_dav5' )"/> 
                                        <a id="DlEs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav5"> Electromagnetic Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav5" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year:</label>
                                            <div class="controls">
                                                <!-- <input id="dav_elec_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlElectromagnetic, 'sel_year_survey', array('id'=>'dav_elec_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_elec_surveymethods" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlElectromagnetic, 'sel_survey_method', array('id'=>'dav_elec_surveymethods', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_elec_sca" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_coverage_area', array('id'=>'dav_elec_sca', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <!-- <input id="dav_elec_dspr" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_depth_range', array('id'=>'dav_elec_dspr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <!-- <input id="dav_elec_rsi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_spacing_interval', array('id'=>'dav_elec_rsi', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Area Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p90_area', array('id'=>'sel_vol_p90_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p90_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p90_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p90_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p90_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p90_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p50_area', array('id'=>'sel_vol_p50_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p50_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p50_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p50_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p50_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p50_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p10_area', array('id'=>'sel_vol_p10_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p10_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p10_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p10_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p10_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="sel_vol_p10_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_elec_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlElectromagnetic, 'sel_remark', array('id'=>'dav_elec_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlRs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlRs','DlRs_link','col_dav6' )"/> 
                                        <a id="DlRs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav6"> Resistivity Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav6" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year:</label>
                                            <div class="controls">
                                                <!-- <input id="dav_res_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlResistivity, 'rst_year_survey', array('id'=>'dav_res_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_res_surveymethods" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlResistivity, 'rst_survey_method', array('id'=>'dav_res_surveymethods', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_res_sca" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlResistivity, 'rst_coverage_area', array('id'=>'dav_res_sca', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_res_dspr" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlResistivity, 'rst_depth_range', array('id'=>'dav_res_dspr', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_res_rsi" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlResistivity, 'rst_spacing_interval', array('id'=>'dav_res_rsi', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Area Closure Estimation</th>
                                                    <th>Net Pay Thickness</th>
                                                    <th>Porosity</th>
                                                    <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                    <th>Oil Case</th>
                                                    <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p90_area', array('id'=>'rst_vol_p90_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p90_thickness" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p90_porosity" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p90_sw" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p90_oil" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p90_gas" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p50_area', array('id'=>'rst_vol_p50_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p50_thickness" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p50_porosity" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p50_sw" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p50_oil" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p50_gas" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p10_area', array('id'=>'rst_vol_p10_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p10_thickness" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p10_porosity" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p10_sw" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p10_oil" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="rst_vol_p10_gas" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_res_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlResistivity, 'rst_remark', array('id'=>'dav_res_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlOs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlOs','DlOs_link','col_dav7' )"/> 
                                        <a id="DlOs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav7"> Other Survey</a>
                                    </div>
                                </div>
                                <div id="col_dav7" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_other_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlOther, 'sor_year_survey', array('id'=>'dav_other_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Area Clossure Estimation</th>
                                                    <th>Net Pay Thickness</th>
                                                    <th>Porosity</th>
                                                    <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                    <th>Oil Case</th>
                                                    <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p90_area', array('id'=>'sor_vol_p90_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p90_thickness" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p90_porosity" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p90_sw" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p90_oil" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p90_gas" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p50_area', array('id'=>'sor_vol_p50_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p50_thickness" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p50_porosity" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p50_sw" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p50_oil" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p50_gas" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p10_area', array('id'=>'sor_vol_p10_area', 'class'=>'span6'));?>
                                                    	<span class="add-on">ac</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p10_thickness" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">ft</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p10_porosity" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p10_sw" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">%</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p10_oil" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">MMBO</span>
                                                    	</div>
                                                    </td>
                                                    <td>
                                                    	<div class=" input-append">
                                                    		<input id="sor_vol_p10_gas" class="span6" name="custom" type="text" readonly />
                                                    	<span class="add-on">BCF</span>
                                                    	</div>
                                                    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_other_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlOther, 'sor_remark', array('id'=>'dav_other_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <input id="DlDss3" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss3','DlDss3_link','col_dav8' )"/> 
                                        <a id="DlDss3_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav8"> Survey Seismic 3D</a>
                                    </div>
                                </div>
                                <div id="col_dav8" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Acquisition Year :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_year_survey', array('id'=>'dav_3dss_aqyear', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of 3D Vintage :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_numvin" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_vintage_number', array('id'=>'dav_3dss_numvin', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>                                        
                                        <div class="control-group">
                                            <label class="control-label">Bin Size :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_binsize" type="text" class="span3" /> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_bin_size', array('id'=>'dav_3dss_binsize', 'class'=>'span3'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Coverage Area : </label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_tca" type="text" style="max-width:160px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_coverage_area', array('id'=>'dav_3dss_tca', 'style'=>'max-width:160px;'))?>
                                                    <span class="add-on">km<sup>2</sup></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Dominant Frequency at Reservoir Target:</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_dfrt1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_frequency', array('id'=>'dav_3dss_dfrt1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">hz</span>
                                                </div>
                                                <!-- <input id="dav_3dss_dfrt2" type="text" class="span3" placeholder="Lateral Seismic Resolution"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_frequency_lateral', array('id'=>'dav_3dss_dfrt2', 'class'=>'span3', 'placeholder'=>'Lateral Seismic Resolution'));?>
                                                <!-- <input id="dav_3dss_dfrt3" type="text" class="span3" placeholder="Vertical Seismic Resolution"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_frequency_vertical', array('id'=>'dav_3dss_dfrt3', 'class'=>'span3', 'placeholder'=>'Vertical Seismic Resolution'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Processing Year:</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_lpy" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Latest Processing Year"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_year_late_process', array('id'=>'dav_3dss_lpy', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Latest Processing Year'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Proccessing Method :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_lpm" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_late_method', array('id'=>'dav_3dss_lpm', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Image Quality :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_siq" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_img_quality', array('id'=>'dav_3dss_siq', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Top Reservoir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_trtd1" type="text" style="max-width:155px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ft', array('id'=>'dav_3dss_trtd1', 'style'=>'max-width:155px;'));?>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ms', array('id'=>'dav_3dss_trtd2', 'style'=>'max-width:170px; margin-left:24px;'));?>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Bottom Reservoir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_brtd1" type="text" style="max-width:155px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_bot_depth_ft', array('id'=>'dav_3dss_brtd1', 'style'=>'max-width:155px;'));?>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ms', array('id'=>'dav_3dss_brtd2', 'style'=>'max-width:170px; margin-left:24px;'));?>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_dcsp" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_spill', array('id'=>'dav_3dss_dcsp', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Distinguish Wave Propagation by Offset :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_adwpo" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_offset', array('id'=>'dav_3dss_adwpo', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Resolve Reservoir Rock Property :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_ar3p" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_rock', array('id'=>'dav_3dss_ar3p', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Resolve Reservoir Fluid Property :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_arrfp" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_fluid', array('id'=>'dav_3dss_arrfp', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_owc1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate', array('id'=>'dav_3dss_owc1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>  
                                                <!-- <input id="dlosd8n2" class="span3" type="text" placeholder="By analog to" /> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate_analog', array('id'=>'dlosd8n2', 'class'=>'span3', 'placeholder'=>'By analog to'));?>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" /> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate_spill', array('id'=>'dav_3dss_owc2', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
                                                    <span class="add-on">%</span>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil/Gas :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_oilgas1" type="text" style="max-width:170px;"/> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low', array('id'=>'dav_3dss_oilgas1', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>  
                                                <!-- <input id="dlosd8o2" class="span3" type="text" placeholder="By analog to" /> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low_analog', array('id'=>'dlosd8o2', 'class'=>'span3', 'placeholder'=>'By analog to'));?>
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" /> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low_spill', array('id'=>'dav_3dss_oilgas2', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
                                                    <span class="add-on">%</span>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Formation Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_forthickness" type="text" style="max-width:170px;" /> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_formation_thickness', array('id'=>'dav_3dss_forthickness', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><sup>*</sup>Grass Sand or Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <!-- <input id="dav_3dss_grass_resthickness" type="text" style="max-width:170px;" /> -->
                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_gross_thickness', array('id'=>'dav_3dss_grass_resthickness', 'style'=>'max-width:170px;'));?>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability Net Thickness Reservoir Pay :</label>
                                            <div class="controls">
                                                <!-- <input id="dav_3dss_antrip" class="span3" style="text-align: center;"/> -->
                                                <?php echo $form->textField($mdlSeismic3d, 's3d_avail_pay', array('id'=>'dav_3dss_antrip', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                            </div>
                                        </div>  
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Pay Zone Thickness / Net Pay</th>
                                                <th>Thickness Net Pay</th>
                                                <th>NTG(Net Pay to Gross Sand)</th>
                                                <th>Porosity Cut Off</th>
                                                <th>Vsh Cut Off</th>
                                                <th>Saturation Cut Off</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_thickness', array('id'=>'s3d_net_p90_thickness', 'class'=>'span6'));?>
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_net_p90_net_to_gross" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_vsh', array('id'=>'s3d_net_p90_vsh', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_por', array('id'=>'s3d_net_p90_por', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_satur', array('id'=>'s3d_net_p90_satur', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_thickness', array('id'=>'s3d_net_p50_thickness', 'class'=>'span6'));?>
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_net_p50_net_to_gross" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_vsh', array('id'=>'s3d_net_p50_vsh', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_por', array('id'=>'s3d_net_p50_por', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_satur', array('id'=>'s3d_net_p50_satur', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_thickness', array('id'=>'s3d_net_p10_thickness', 'class'=>'span6'));?>
                                                	<span class="add-on">ft</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_net_p10_net_to_gross" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_vsh', array('id'=>'s3d_net_p10_vsh', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_por', array('id'=>'s3d_net_p10_por', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_satur', array('id'=>'s3d_net_p10_satur', 'class'=>'span6'));?>
                                                	<span class="add-on">%</span>
                                                	</div>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Water Saturation (1-S<sub>w</sub>)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p90_area', array('id'=>'s3d_vol_p90_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p90_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p90_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p90_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p90_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p90_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p50_area', array('id'=>'s3d_vol_p50_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p50_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p50_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p50_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p50_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p50_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td>
                                                	<div class=" input-append">
                                                		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p10_area', array('id'=>'s3d_vol_p10_area', 'class'=>'span6'));?>
                                                	<span class="add-on">ac</span>
                                                	</div>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p10_thickness" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">ft</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p10_porosity" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p10_sw" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">%</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p10_oil" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">MMBO</span>
                                                </td>
                                                <td>
                                                	<div class=" input-append">
                                                		<input id="s3d_vol_p10_gas" class="span6" name="custom" type="text" readonly />
                                                	<span class="add-on">BCF</span>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <!-- <textarea id="dav_3dss_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlSeismic3d, 's3d_remark', array('id'=>'dav_3dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                <!-- END OCCURANCE SURVEY DESCRIPTION-->                
            	</div>
        	</div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN LEAD GCF -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> POSTDRILL PROSPECT GEOLOGICAL CHANCE FACTOR</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">×</button>
                                    <strong>Postdrill Prospect Geological Chance Factor</strong>
                                    <p>When play chosen this GCF automatically filled with Play GCF, change according to newest GCF or leave as it is.</p>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="accordion"> 
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Source Rock :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_sr', array('id'=>'gcfsrock', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_age_system', array('id'=>'gcfsrock1', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input id="gcfsrock1a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_age_serie', array('id'=>'gcfsrock1a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_formation', array('id'=>'gcfsrock2', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_kerogen', array('id'=>'gcfsrock3', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_toc', array('id'=>'gcfsrock4', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_hfu', array('id'=>'gcfsrock5', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_distribution', array('id'=>'gcfsrock6', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_continuity', array('id'=>'gcfsrock7', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_maturity', array('id'=>'gcfsrock8', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_otr', array('id'=>'gcfsrock9', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Source Rock:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_sr_remark', array('id'=>'gcfsrock10', 'class'=>'span3', 'row'=>'2', 'style'=>'text-align: left;'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Reservoir :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_res', array('id'=>'gcfres', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_age_system', array('id'=>'gcfres1', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_age_serie', array('id'=>'gcfres1a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_formation', array('id'=>'gcfres2', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_set', array('id'=>'gcfres3', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_env', array('id'=>'gcfres4', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_distribution', array('id'=>'gcfres5', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Contuinity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_continuity', array('id'=>'gcfres6', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_lithology', array('id'=>'gcfres7', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_type', array('id'=>'gcfres8', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Average Primary Porosity Reservoir :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3', 'style'=>'text-align: center; max-width: 155px;'));?>
                                                    <span class="addkm">%</span>
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                <!-- <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlGcf, 'gcf_res_por_secondary', array('id'=>'gcfres10', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Reservoir :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfres11" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_res_remark', array('id'=>'gcfres11', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Trap :</strong></label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_trap', array('id'=>'gcftrap', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls"> 
                                                    <!-- <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_system', array('id'=>'gcftrap1', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_serie', array('id'=>'gcftrap1a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name :</label>
                                                <div class="controls">
                                                <!-- <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation', array('id'=>'gcftrap2', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_distribution', array('id'=>'gcftrap3', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_continuity', array('id'=>'gcftrap4', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_type', array('id'=>'gcftrap5', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_age_system', array('id'=>'gcftrap6', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_age_serie', array('id'=>'gcftrap6a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_geometry', array('id'=>'gcftrap7', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_type', array('id'=>'gcftrap8', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_closure', array('id'=>'gcftrap9', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Trap:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcftrap10"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_trap_remark', array('id'=>'gcftrap10', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label"><sup>*</sup>Dynamic :</strong></label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_dyn', array('id'=>'gcfdyn', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration', array('id'=>'gcfdyn1', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_kitchen', array('id'=>'gcfdyn2', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_petroleum', array('id'=>'gcfdyn3', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                <!-- <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" /> -->
                                                <?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_system', array('id'=>'gcfdyn4', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_serie', array('id'=>'gcfdyn4a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_system', array('id'=>'gcfdyn5', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_serie', array('id'=>'gcfdyn5a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_preservation', array('id'=>'gcfdyn6', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_pathways', array('id'=>'gcfdyn7', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_system', array('id'=>'gcfdyn8', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                    <!-- <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_serie', array('id'=>'gcfdyn8a', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Dynamic:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfdyn9"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_dyn_remark', array('id'=>'gcfdyn9', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- BEGIN LEAD GCF -->
            </div>
        </div>
        <?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
        <?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/updatepostdrill', array('id'=>$mdlProspect->prospect_id)), array(
        	'type'=>'POST',
        	'dataType'=>'json',
        	'beforeSend'=>'function(data) {
        		$("#pesan").show();
        		$("#pesan").html("Sending...");
        	}',
        	'success'=>'js:function(data) {
        		if(data.result === "success") {
        			$("#pesan").html(data.msg);
        			$("#pesan").css("color", "#f00");
        		
        		} else {
        			$("#pesan").html(data.msg);
        			$("#pesan").css("color", "#f00");
        		}
        	}',
        ),
        array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;')
        );?>
    	<?php $this->endWidget();?>
    </div>
</div>

<?php 
	Yii::app()->clientScript->registerScript('ambil-gcf', "
		$('#plyName').on('change', function() {
			var value = $('#plyName').val();
			if(value != '') {
				$.ajax({
					url : '" . Yii::app()->createUrl('/Kkks/getGcf') . "',
					data : {
						play_id : value,
					},
					type : 'POST',
					dataType : 'json',
					success : function(data) {
						$('#gcfsrock').select2('val', data.gcf_is_sr);
						$('#gcfsrock1').select2('val', data.gcf_sr_age_system);
						$('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
						$('#gcfsrock2').select2('val', data.gcf_sr_formation);
						$('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
						$('#gcfsrock4').select2('val', data.gcf_sr_toc);
						$('#gcfsrock5').select2('val', data.gcf_sr_hfu);
						$('#gcfsrock6').select2('val', data.gcf_sr_distribution);
						$('#gcfsrock7').select2('val', data.gcf_sr_continuity);
						$('#gcfsrock8').select2('val', data.gcf_sr_maturity);
						$('#gcfsrock9').select2('val', data.gcf_sr_otr);
						$('#gcfsrock10').val(data.gcf_sr_remark);
						$('#gcfres').select2('val', data.gcf_is_res);
						$('#gcfres1').select2('val', data.gcf_res_age_system);
						$('#gcfres1a').select2('val', data.gcf_res_age_serie);
						$('#gcfres2').select2('val', data.gcf_res_formation);
						$('#gcfres3').select2('val', data.gcf_res_depos_set);
						$('#gcfres4').select2('val', data.gcf_res_depos_env);
						$('#gcfres5').select2('val', data.gcf_res_distribution);
						$('#gcfres6').select2('val', data.gcf_res_continuity);		
						$('#gcfres7').select2('val', data.gcf_res_lithology);
						$('#gcfres8').select2('val', data.gcf_res_por_type);
						$('#gcfres9').select2('val', data.gcf_res_por_primary);
						$('#gcfres10').select2('val', data.gcf_res_por_secondary);
						$('#gcfres11').val(data.gcf_res_remark);
						$('#gcftrap').select2('val', data.gcf_is_trap);
						$('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
						$('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
						$('#gcftrap2').select2('val', data.gcf_trap_seal_formation);
						$('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
						$('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
						$('#gcftrap5').select2('val', data.gcf_trap_seal_type);
						$('#gcftrap6').select2('val', data.gcf_trap_age_system);
						$('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
						$('#gcftrap7').select2('val', data.gcf_trap_geometry);
						$('#gcftrap8').select2('val', data.gcf_trap_type);
						$('#gcftrap9').select2('val', data.gcf_trap_closure);
						$('#gcftrap10').val(data.gcf_trap_remark);
						$('#gcfdyn').select2('val', data.gcf_is_dyn);
						$('#gcfdyn1').select2('val', data.gcf_dyn_migration);
						$('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
						$('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
						$('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
						$('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
						$('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
						$('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
						$('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
						$('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
						$('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
						$('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
						$('#gcfdyn9').val(data.gcf_dyn_remark);
						disabledElement('gcfsrock');
						disabledElement('gcfres');
						disabledElement('gcftrap');
						disabledElement('gcfdyn');
						disabledElement('gcfres8');
					},
				});
			}
		});
	");
?>

<?php 
Yii::app()->clientScript->registerScript('colpost', "
	$('.clastic_grtp10, .clastic_grtp50, .clastic_grtp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_clastic_p10_thickness = ($('#wpd_clastic_p10_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_thickness' + atribut_ke).val()) : 0;
		var wpd_clastic_p50_thickness = ($('#wpd_clastic_p50_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_thickness' + atribut_ke).val()) : 0;
		var wpd_clastic_p90_thickness = ($('#wpd_clastic_p90_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_thickness' + atribut_ke).val()) : 0;
		var average = (wpd_clastic_p10_thickness + wpd_clastic_p50_thickness + wpd_clastic_p90_thickness) / 3;
		var estimate = (wpd_clastic_p90_thickness - average) / 3;
		$('#clastic_average_grt' + atribut_ke).val(average.toFixed(2));
		$('#clastic_estimate_grt' + atribut_ke).val(estimate.toFixed(2));
	});
		
// 	$('.clastic_grtp10, .clastic_grtp50, .clastic_grtp90').change(function() {
// 		var atribut = $(this).attr('id');
// 		var atribut_ke = atribut.substr(atribut.length -1, 1);
// 		var wpd_clastic_p10_thickness = ($('#wpd_clastic_p10_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_thickness' + atribut_ke).val()) : 0;
// 		var wpd_clastic_p50_thickness = ($('#wpd_clastic_p50_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_thickness' + atribut_ke).val()) : 0;
// 		var wpd_clastic_p90_thickness = ($('#wpd_clastic_p90_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_thickness' + atribut_ke).val()) : 0;
// 		var average = (wpd_clastic_p10_thickness + wpd_clastic_p50_thickness + wpd_clastic_p90_thickness) / 3;
// 		var estimate = (wpd_clastic_p90_thickness - average) / 3;
// 		$('#clastic_average_grt' + atribut_ke).val(average.toFixed(2));
// 		$('#clastic_estimate_grt' + atribut_ke).val(estimate.toFixed(2));
// 	});
		
	$('.clastic_rvcp10, .clastic_rvcp50, .clastic_rvcp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_clastic_p10_gr = ($('#wpd_clastic_p10_gr' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_gr' + atribut_ke).val()) : 0;
		var wpd_clastic_p50_gr = ($('#wpd_clastic_p50_gr' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_gr' + atribut_ke).val()) : 0;
		var wpd_clastic_p90_gr = ($('#wpd_clastic_p90_gr' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_gr' + atribut_ke).val()) : 0;
		var average = (wpd_clastic_p10_gr + wpd_clastic_p50_gr + wpd_clastic_p90_gr) / 3;
		var estimate = (wpd_clastic_p90_gr - average) / 3;
		$('#clastic_average_rvc' + atribut_ke).val(average.toFixed(2));
		$('#clastic_estimate_rvc' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.clastic_rvc2p10, .clastic_rvc2p50, .clastic_rvc2p90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_clastic_p10_sp = ($('#wpd_clastic_p10_sp' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_sp' + atribut_ke).val()) : 0;
		var wpd_clastic_p50_sp = ($('#wpd_clastic_p50_sp' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_sp' + atribut_ke).val()) : 0;
		var wpd_clastic_p90_sp = ($('#wpd_clastic_p90_sp' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_sp' + atribut_ke).val()) : 0;
		var average = (wpd_clastic_p10_sp + wpd_clastic_p50_sp + wpd_clastic_p90_sp) / 3;
		var estimate = (wpd_clastic_p90_sp - average) / 3;
		$('#clastic_average_rvc2' + atribut_ke).val(average.toFixed(2));
		$('#clastic_estimate_rvc2' + atribut_ke).val(estimate.toFixed(2));
	}); 
	
	$('.clastic_ntgp10, .clastic_ntgp50, .clastic_ntgp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_clastic_p10_net = ($('#wpd_clastic_p10_net' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_net' + atribut_ke).val()) : 0;
		var wpd_clastic_p50_net = ($('#wpd_clastic_p50_net' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_net' + atribut_ke).val()) : 0;
		var wpd_clastic_p90_net = ($('#wpd_clastic_p90_net' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_net' + atribut_ke).val()) : 0;
		var average = (wpd_clastic_p10_net + wpd_clastic_p50_net + wpd_clastic_p90_net) / 3;
		var estimate = (wpd_clastic_p90_net - average) / 3;
		$('#clastic_average_ntg' + atribut_ke).val(average.toFixed(2));
		$('#clastic_estimate_ntg' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.clastic_porp10, .clastic_porp50, .clastic_porp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_clastic_p10_por = ($('#wpd_clastic_p10_por' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_por' + atribut_ke).val()) : 0;
		var wpd_clastic_p50_por = ($('#wpd_clastic_p50_por' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_por' + atribut_ke).val()) : 0;
		var wpd_clastic_p90_por = ($('#wpd_clastic_p90_por' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_por' + atribut_ke).val()) : 0;
		var average = (wpd_clastic_p10_por + wpd_clastic_p50_por + wpd_clastic_p90_por) / 3;
		var estimate = (wpd_clastic_p90_por - average) / 3;
		$('#clastic_average_por' + atribut_ke).val(average.toFixed(2));
		$('#clastic_estimate_por' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.clastic_saturp10, .clastic_saturp50, .clastic_saturp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_clastic_p10_satur = ($('#wpd_clastic_p10_satur' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p10_satur' + atribut_ke).val()) : 0;
		var wpd_clastic_p50_satur = ($('#wpd_clastic_p50_satur' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p50_satur' + atribut_ke).val()) : 0;
		var wpd_clastic_p90_satur = ($('#wpd_clastic_p90_satur' + atribut_ke).val() != '') ? parseFloat($('#wpd_clastic_p90_satur' + atribut_ke).val()) : 0;
		var average = (wpd_clastic_p10_satur + wpd_clastic_p50_satur + wpd_clastic_p90_satur) / 3;
		var estimate = (wpd_clastic_p90_satur - average) / 3;
		$('#clastic_average_satur' + atribut_ke).val(average.toFixed(2));
		$('#clastic_estimate_satur' + atribut_ke).val(estimate.toFixed(2));
	});
	
	$('.carbo_grtp10, .carbo_grtp50, .carbo_grtp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_carbo_p10_thickness = ($('#wpd_carbo_p10_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p10_thickness' + atribut_ke).val()) : 0;
		var wpd_carbo_p50_thickness = ($('#wpd_carbo_p50_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p50_thickness' + atribut_ke).val()) : 0;
		var wpd_carbo_p90_thickness = ($('#wpd_carbo_p90_thickness' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p90_thickness' + atribut_ke).val()) : 0;
		var average = (wpd_carbo_p10_thickness + wpd_carbo_p50_thickness + wpd_carbo_p90_thickness) / 3;
		var estimate = (wpd_carbo_p90_thickness - average) / 3;
		$('#carbo_average_grt' + atribut_ke).val(average.toFixed(2));
		$('#carbo_estimate_grt' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.carbo_dtcp10, .carbo_dtcp50, .carbo_dtcp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_carbo_p10_dtc = ($('#wpd_carbo_p10_dtc' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p10_dtc' + atribut_ke).val()) : 0;
		var wpd_carbo_p50_dtc = ($('#wpd_carbo_p50_dtc' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p50_dtc' + atribut_ke).val()) : 0;
		var wpd_carbo_p90_dtc = ($('#wpd_carbo_p90_dtc' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p90_dtc' + atribut_ke).val()) : 0;
		var average = (wpd_carbo_p10_dtc + wpd_carbo_p50_dtc + wpd_carbo_p90_dtc) / 3;
		var estimate = (wpd_carbo_p90_dtc - average) / 3;
		$('#carbo_average_dtc' + atribut_ke).val(average.toFixed(2));
		$('#carbo_estimate_dtc' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.carbo_totalp10, .carbo_totalp50, .carbo_totalp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_carbo_p10_total = ($('#wpd_carbo_p10_total' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p10_total' + atribut_ke).val()) : 0;
		var wpd_carbo_p50_total = ($('#wpd_carbo_p50_total' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p50_total' + atribut_ke).val()) : 0;
		var wpd_carbo_p90_total = ($('#wpd_carbo_p90_total' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p90_total' + atribut_ke).val()) : 0;
		var average = (wpd_carbo_p10_total + wpd_carbo_p50_total + wpd_carbo_p90_total) / 3;
		var estimate = (wpd_carbo_p90_total - average) / 3;
		$('#carbo_average_total' + atribut_ke).val(average.toFixed(2));
		$('#carbo_estimate_total' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.carbo_netp10, .carbo_netp50, .carbo_netp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_carbo_p10_net = ($('#wpd_carbo_p10_net' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p10_net' + atribut_ke).val()) : 0;
		var wpd_carbo_p50_net = ($('#wpd_carbo_p50_net' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p50_net' + atribut_ke).val()) : 0;
		var wpd_carbo_p90_net = ($('#wpd_carbo_p90_net' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p90_net' + atribut_ke).val()) : 0;
		var average = (wpd_carbo_p10_net + wpd_carbo_p50_net + wpd_carbo_p90_net) / 3;
		var estimate = (wpd_carbo_p90_net - average) / 3;
		$('#carbo_average_net' + atribut_ke).val(average.toFixed(2));
		$('#carbo_estimate_net' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.carbo_porp10, .carbo_porp50, .carbo_porp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_carbo_p10_por = ($('#wpd_carbo_p10_por' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p10_por' + atribut_ke).val()) : 0;
		var wpd_carbo_p50_por = ($('#wpd_carbo_p50_por' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p50_por' + atribut_ke).val()) : 0;
		var wpd_carbo_p90_por = ($('#wpd_carbo_p90_por' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p90_por' + atribut_ke).val()) : 0;
		var average = (wpd_carbo_p10_por + wpd_carbo_p50_por + wpd_carbo_p90_por) / 3;
		var estimate = (wpd_carbo_p90_por - average) / 3;
		$('#carbo_average_por' + atribut_ke).val(average.toFixed(2));
		$('#carbo_estimate_por' + atribut_ke).val(estimate.toFixed(2));
	});
		
	$('.carbo_saturp10, .carbo_saturp50, .carbo_saturp90').live('keyup', function() {
		var atribut = $(this).attr('id');
		var atribut_ke = atribut.substr(atribut.length -1, 1);
		var wpd_carbo_p10_satur = ($('#wpd_carbo_p10_satur' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p10_satur' + atribut_ke).val()) : 0;
		var wpd_carbo_p50_satur = ($('#wpd_carbo_p50_satur' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p50_satur' + atribut_ke).val()) : 0;
		var wpd_carbo_p90_satur = ($('#wpd_carbo_p90_satur' + atribut_ke).val() != '') ? parseFloat($('#wpd_carbo_p90_satur' + atribut_ke).val()) : 0;
		var average = (wpd_carbo_p10_satur + wpd_carbo_p50_satur + wpd_carbo_p90_satur) / 3;
		var estimate = (wpd_carbo_p90_satur - average) / 3;
		$('#carbo_average_satur' + atribut_ke).val(average.toFixed(2));
		$('#carbo_estimate_satur' + atribut_ke).val(estimate.toFixed(2));
	});
		
");
?>
