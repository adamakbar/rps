<?php
/* @var $this SiteController */

$this->pageTitle='Play';
$this->breadcrumbs=array(
	'Play',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">PLAY</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Play</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
        <div class="span12">
            <a class="btn btn-primary" href="#" onclick="window.print(); return false;">Print PDF</a>
        </div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'updateplay-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnChange'=>false,
			),
			'enableAjaxValidation'=>true,
		));?>
		<div id="pesan"></div>
		<?php //echo $form->errorSummary(array($mdlPlay, $mdlGcf));?>
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN GENERAL DATA-->
				<div id="_gen_data_play" class="widget">
					<div class="widget-title">
						<h4><i class="icon-file"></i> GENERAL DATA</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal">
                            <div class="accordion">
                                <!-- Begin Data Geographical Working Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geographical Working Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info!</strong> Estimated Boundary Coordinate of Working Area (Datum WGS '84).
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <label class="control-label">Boundary Latitude Top Left :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                    	<tr>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_top_left_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                        				<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_top_left_degree');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_top_left_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_top_left_minute');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_top_left_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_top_left_second');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_top_left_direction', array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_top_left_direction');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                    		</td>
                                                    	</tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Boundary Latitude Bottom Right :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                    	<tr>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_bottom_right_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                        				<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_bottom_right_degree');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_bottom_right_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_bottom_right_minute');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_bottom_right_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_bottom_right_second');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="S/ N"/> -->
																		<?php echo $form->textField($mdlPlay, 'bound_lat_bottom_right_direction', array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_lat_bottom_right_direction');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                    		</td>
                                                    	</tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Boundary Longitude Top Left :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                    	<tr>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_top_left_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                        				<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_top_left_degree');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_top_left_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_top_left_minute');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_top_left_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_top_left_second');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="E/ W"/> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_top_left_direction', array('value'=>'E', 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_top_left_direction');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                    		</td>
                                                    	</tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Boundary Longitude Bottom Right :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                    	<tr>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_bottom_right_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                        				<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_bottom_right_degree');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_bottom_right_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_bottom_right_minute');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_bottom_right_second', array('class'=>'input-mini tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
                                                        				<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_bottom_right_second');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="E/ W"/> -->
																		<?php echo $form->textField($mdlPlay, 'bound_long_bottom_right_direction', array('value'=>'E', 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlPlay, 'bound_long_bottom_right_direction');?>
																</span>
                                                    		</td>
                                                    		<td>
                                                    			<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                    		</td>
                                                    	</tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Basin :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="_basin" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'basin_id', array('id'=>'basin', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'basin_id');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Province :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="_province" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'province_id', array('id'=>'provinsi', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'province_id');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Geographical Working Area -->
                                <!-- Begin Data Geological -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geological</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">By Analog to :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="_byanalogto" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_analog_to', array('id'=>'_byanalogto', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_analog_to');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distance to Analog :</label>
                                                <div class="controls wajib">
                                                	<p style="margin-bottom: 2px;">
                                                    <!-- <input type="hidden" id="_distancetoanalog" class="span3" style="text-align: center; max-width:183px;"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_analog_distance', array('id'=>'_distancetoanalog', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlPlay, 'play_analog_distance');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Exploration Methods :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="_explorationmethods" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_exr_method', array('id'=>'_explorationmethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_exr_method');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Geological -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Onshore or Offshore :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_shore', array('id'=>'env_onoffshore', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_shore');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Terrain :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_terrain', array('id'=>'env_terrain', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_terrain');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4Play">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4Play" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Field :</label>
                                                <div class="controls wajib">
                                                	<p style="margin-bottom: 2px;">
                                                    <!-- <input id="n_facility" name="custom" type="text" style="max-width:165px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_near_field', array('id'=>'n_facility', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlPlay, 'play_near_field');?>
                                                </div>
                                            </div>                    
                                            <div class="control-group">
                                                <label class="control-label">Nearby Infra Structure:</label>
                                                <div class="controls wajib">
                                                	<p style="margin-bottom: 2px;">
                                                    <!-- <input id="n_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_near_infra_structure', array('id'=>'n_developmentwell', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlPlay, 'play_near_infra_structure');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span12">
        		<!-- BEGIN DATA AVAILABILITY-->
        		<div id="_data_availability_play" class="widget">
        			<div class="widget-title">
        				<h4><i class="icon-paper-clip"></i> DATA AVAILABILITY</h4>
        				<span class="tools">
        					<a href="javascript:;" class="icon-chevron-down"></a>
        				</span>
        			</div>
        			<div class="widget-body">
        				<span action="#" class="form-horizontal">
                            <div class="accordion">
                                <!-- Begin Data Occurence Sample Description -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Occurence Sample Description</strong>
                                        </a>
                                    </div>
                                    <div id="col_dav1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Support Data :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="_supportdata" style="text-align: center;" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Proven if available data based on actual study data, Analog if available data based on around data." data-original-title="Nearby Development Well"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_support_data', array('id'=>'_supportdata', 'class'=>'span3 popovers', 'style'=>'text-align: center; position: relative; display: inline-block;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Proven if available data based on actual proven data, Analog if available data based on other analog data.', 'data-original-title'=>'Nearby Development Well'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_support_data');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <blockquote>
                                                        <small>Proven if available data based on actual proven data, Analog if available data based on other analog data.</small>
                                                    </blockquote>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Outcrop Place Distance :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="_outcrop" type="text" style="max-width:165px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_outcrop_distance', array('id'=>'_outcrop', 'class'=>'number', 'style'=>'max-width:130px;'));?>
                                                        <span class="add-on">Km</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_outcrop_distance');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Occurence Sample Description -->
                                <!-- Begin Data Occurrence Seismic 2D Data  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Occurrence Seismic 2D Data </strong>
                                        </a>
                                    </div>
                                    <div id="col_dav2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year :</label>
                                                <div class="controls">
                                                    <!-- <input id="osd_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_s2d_year', array('id'=>'osd_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlPlay, 'play_s2d_year');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Number of Seismic Crossline :</label>
                                                <div class="controls">
                                                    <!-- <input id="osd_numseismic" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_s2d_crossline', array('id'=>'osd_numseismic', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlPlay, 'play_s2d_crossline');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distance Seismic Intervall of Parallel Line :</label>
                                                <div class="controls">
                                                	<p style="margin-bottom: 2px;">
                                                    <!-- <input type="hidden" id="osd_dsipl" class="span3" style="text-align: center; max-width:183px;"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_s2d_line_intervall', array('id'=>'osd_dsipl', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlPlay, 'play_s2d_line_intervall');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Seismic Image Quality :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="osd_siq" class="span3" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_s2d_img_quality', array('id'=>'osd_siq', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_s2d_img_quality');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Occurrence Seismic 2D Data  -->
                                <!-- Begin Data Occurrence Other Survey  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Occurrence Other Survey</strong>
                                        </a>
                                    </div>
                                    <div id="col_dav3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Geochemistry :</label>
                                                <div class="controls">
                                                    <!-- <input id="oos_1" type="text"class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Is Geochemistry data available? Please fill with any data about it." data-original-title="Geochemistry"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_sgc', array('id'=>'oos_1', 'class'=>'span3 popovers', 'style'=>'text-align: center; position: relative; display: inline-block;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Is Geochemistry data available? Please fill with any data about it.', 'data-original-title'=>'Geochemistry'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_sgc');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Geochemistry Number of Sample :</label>
                                                <div class="controls">
                                                    <!-- <input id="oos_2" type="text" class="span3" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_sgc_sample', array('id'=>'oos_2', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlPlay, 'play_sgc_sample');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Geochemistry Survey Range Depth :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="oos_3" type="text" style="max-width:172px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_sgc_depth', array('id'=>'oos_3', 'class'=>'number', 'style'=>'max-width:137px;'))?>
                                                        <span class="add-on">ft</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_sgc_depth');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Gravity :</label>
                                                <div class="controls">
                                                    <!-- <input id="oos_4" type="text"class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content=" Is Gravity data available? Please fill with any data about it." data-original-title="Gravity"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_sgv', array('id'=>'oos_4', 'class'=>'span3 popovers', 'style'=>'text-align: center; position: relative; display: inline-block;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>' Is Gravity data available? Please fill with any data about it.', 'data-original-title'=>'Gravity'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_sgv');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Gravity Survey Acreage :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="oos_5" type="text" style=" max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_sgv_acre', array('id'=>'oos_5', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ac</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_sgv_acre');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Gravity Survey Range Depth :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="oos_5" type="text" style="max-width:172px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_sgv_depth', array('id'=>'oos_11', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_sgv_depth');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Electromagnetic :</label>
                                                <div class="controls">
                                                    <!-- <input id="oos_6" type="text" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content=" Is Electromagnetic data available? Please fill with any data about it." data-original-title="Electromagnetic" /> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_sel', array('id'=>'oos_6', 'class'=>'span3 popovers', 'style'=>'text-align: center; position: relative; display: inline-block;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Is Electromagnetic data available? Please fill with any data about it.', 'data-original-title'=>'Electromagnetic'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_sel');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Electromagnetic Survey Acreage :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="oos_7" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_sel_acre', array('id'=>'oos_7', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ac</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_sel_acre');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Electromagnetic Survey Range Depth :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="oos_8" type="text" style="max-width:172px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_sel_depth', array('id'=>'oos_8', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_sel_depth');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Resistivity :</label>
                                                <div class="controls">
                                                    <!-- <input id="oos_9" type="text"class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Is Resistivity data available? Please fill with any data about it." data-original-title="Resistivity"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_srt', array('id'=>'oos_9', 'class'=>'span3 popovers', 'style'=>'text-align: center; position: relative; display: inline-block;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Is Resistivity data available? Please fill with any data about it.', 'data-original-title'=>'Resistivity'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlPlay, 'play_srt');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Resistivity Survey Acreage :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="oos_10" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlPlay, 'play_srt_acre', array('id'=>'oos_10', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ac</i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlPlay, 'play_srt_acre');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Occurrence Other Survey -->
                                <!-- Begin Data Geology Regional Map :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geology Regional Map</strong>
                                        </a>
                                    </div>
                                    <div id="col_dav4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Scale :</label>
                                                <div class="controls">
                                                    <!-- <input id="pgrm1" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_map_scale', array('id'=>'pgrm1', 'class'=>'span3 number-colon'));?>
                                                    <?php echo $form->error($mdlPlay, 'play_map_scale');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Author by :</label>
                                             <div class="controls">
                                                    <!-- <input id="pgrm2" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_map_author', array('id'=>'pgrm2', 'class'=>'span3'));?>
                                                    <?php echo $form->error($mdlPlay, 'play_map_author');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Year of Published :</label>
                                                <div class="controls">
                                                    <!-- <input id="pgrm3" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlPlay, 'play_map_year', array('id'=>'pgrm3', 'class'=>'span3'));?>
                                                    <?php echo $form->error($mdlPlay, 'play_map_year');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Geology Regional Map :  -->
                            </div>
                            <div class="control-group">
                            	<label class="control-label">Data Availability Remark :</label>
                            	<div class="controls">
                            		<!-- <textarea id="rmk1" class="span3" row="2" style="text-align: left;" ></textarea> -->
                            		<?php echo $form->textArea($mdlPlay, 'play_remark', array('id'=>'rmk1', 'class'=>'span3', 'row'=>'2', 'style'=>'text-align: left;'));?>
                            	</div>
                            </div>
        				</span>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="row-fluid">
        	<div class="span12">
        		<!-- BEGIN PLAY GEOLOGICAL CHANCE FACTOR-->
        		<div id="_gcf_play" class="widget">
        			<div class="widget-title">
        				<h4><i class="icon-hdd"></i>PLAY GEOLOGICAL CHANCE FACTOR</h4>
        				<span class="tools">
        					<a href="javascript:;" class="icon-chevron-down"></a>
        				</span>
        			</div>
        			<div class="widget-body">
        				<span action="#" class="form-horizontal">
                            <div class="accordion">
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Source Rock :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfsrock" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_sr', array('id'=>'pgcfsrock', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_sr');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfsrock1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_age_system', array('id'=>'pgcfsrock1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfsrock1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_age_serie', array('id'=>'pgcfsrock1a', 'class'=>'span3', 'style'=>'width: 175px;text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcfsrock2" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_formation', array('id'=>'sourceformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_formation');?>
					                                                    <?php //echo $form->textField($mdlFormation, 'formation_name', array('id'=>'sourceformationnamenew','class'=>'span3', 'style'=>'margin-left:20px;'));?>
					                                                    <?php //echo CHtml::button('Add', array('id'=>'btnsourceformationnamenew', 'class'=>'btn btn-mini', 'onclick'=>'{addFormationName(this.id);}'));?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<?php echo $form->textField($mdlGcf, 'gcf_sr_formation_serie', array('id'=>'pgcf_sr_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_sr_formation_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_kerogen', array('id'=>'pgcfsrock3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_ker" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_kerogen');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_toc', array('id'=>'pgcfsrock4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_toc" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_toc');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_hfu', array('id'=>'pgcfsrock5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_hfu" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_hfu');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_distribution', array('id'=>'pgcfsrock6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_continuity', array('id'=>'pgcfsrock7', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_maturity', array('id'=>'pgcfsrock8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_mat" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_maturity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfsrock9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_otr', array('id'=>'pgcfsrock9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_otr" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_otr');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Source Rock :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_sr_remark', array('id'=>'pgcfsrock10', 'class'=>'span3', 'row'=>'2', 'style'=>'text-align: left;'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Reservoir :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfres" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_res', array('id'=>'pgcfres', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_res');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div class="wajib">
																		<!-- <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_age_system', array('id'=>'pgcfres1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
																	<div class="wajib">
																		<!-- <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" /> -->
                                                   						<?php echo $form->textField($mdlGcf, 'gcf_res_age_serie', array('id'=>'pgcfres1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                   						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div class="wajib">
                                                						<!-- <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_formation', array('id'=>'reservoirformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlGcf, 'gcf_res_formation');?>
					                                                    <?php //echo $form->textField($mdlFormation, 'formation_name', array('id'=>'reservoirformationnamenew','class'=>'span3', 'style'=>'margin-left:20px;'));?>
					                                                    <?php //echo CHtml::button('Add', array('id'=>'btnreservoirformationnamenew', 'class'=>'btn btn-mini', 'onclick'=>'{addFormationName(this.id);}'));?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<?php echo $form->textField($mdlGcf, 'gcf_res_formation_serie', array('id'=>'pgcf_res_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_res_formation_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_set', array('id'=>'pgcfres3', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
													<?php echo $form->error($mdlGcf, 'gcf_res_depos_set');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_env', array('id'=>'pgcfres4', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_env');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_distribution', array('id'=>'pgcfres5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_dis" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_continuity', array('id'=>'pgcfres6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_lithology', array('id'=>'pgcfres7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_lit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
													<?php echo $form->error($mdlGcf, 'gcf_res_lithology');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_type', array('id'=>'pgcfres8', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_type');?>
                                                </div>
                                            </div>
                                            <!-- <div id="prim_pro" class="control-group"> -->
                                                <!-- <label class="control-label">Average Primary Porosity Reservoir :</label> -->
                                                <!-- <div class="controls"> -->
                                                    <!-- <p style="margin-bottom: 2px;"> -->
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php //echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'pgcfres9', 'class'=>'span3', 'style'=>'text-align: center; max-width: 155px; position: relative; display: inline-block;'));?>
                                                    <!-- <span class="addkm">%</span> -->
                                                    <!-- </p> -->
                                                    <?php //echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                <!-- </div> -->
                                            <!-- </div> -->
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Average Primary Porosity Reservoir % :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'pgcfres9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_pri" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_secondary', array('id'=>'pgcfres10', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_sec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_secondary');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Reservoir :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfres11" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_res_remark', array('id'=>'pgcfres11', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Trap :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_trap', array('id'=>'pgcftrap', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_trap');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_system', array('id'=>'pgcftrap1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_serie', array('id'=>'pgcftrap1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation', array('id'=>'sealingformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation');?>
					                                                    <?php //echo $form->textField($mdlFormation, 'formation_name', array('id'=>'sealingformationnamenew','class'=>'span3', 'style'=>'margin-left:20px;'));?>
					                                                    <?php //echo CHtml::button('Add', array('id'=>'btnsealingformationnamenew', 'class'=>'btn btn-mini', 'onclick'=>'{addFormationName(this.id);}'));?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation_serie', array('id'=>'pgcf_trap_seal_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_trap_serie_formation_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_distribution', array('id'=>'pgcftrap3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_sdi" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_continuity', array('id'=>'pgcftrap4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_scn" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_type', array('id'=>'pgcftrap5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_stp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_type');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_age_system', array('id'=>'pgcftrap6', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_age_system');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
                                                						<!-- <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" /> -->
                                                   						<?php echo $form->textField($mdlGcf, 'gcf_trap_age_serie', array('id'=>'pgcftrap6a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                   						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                   						<?php echo $form->error($mdlGcf, 'gcf_trap_age_serie');?>
                                                					</div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_geometry', array('id'=>'pgcftrap7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_geo" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_geometry');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_type', array('id'=>'pgcftrap8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_trp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
													<?php echo $form->error($mdlGcf, 'gcf_trap_type');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_closure', array('id'=>'pgcftrap9', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_closure');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Trap :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcftrap10"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_trap_remark', array('id'=>'pgcftrap10', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Dynamic :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_dyn', array('id'=>'pgcfdyn', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_dyn');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration', array('id'=>'pgcfdyn1', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_migration');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_kitchen', array('id'=>'pgcfdyn2', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_kit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_kitchen');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_petroleum', array('id'=>'pgcfdyn3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_tec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_petroleum');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
					                                                    <!-- <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_system', array('id'=>'pgcfdyn4', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_system');?>
					                                                </div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
					                                                    <!-- <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_serie', array('id'=>'pgcfdyn4a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_serie');?>
					                                                </div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
					                                                    <!-- <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_system', array('id'=>'pgcfdyn5', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_system');?>
					                                                </div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
					                                                    <!-- <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_serie', array('id'=>'pgcfdyn5a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_serie');?>
					                                                </div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_preservation', array('id'=>'pgcfdyn6', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_prv" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_preservation');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_pathways', array('id'=>'pgcfdyn7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_mig" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_pathways');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                	<table>
                                                		<tr>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
					                                                    <!-- <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_system', array('id'=>'pgcfdyn8', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_system');?>
					                                                </div>
                                                				</span>
                                                			</td>
                                                			<td style="vertical-align: top; max-width: 180px;">
                                                				<span>
                                                					<div>
					                                                    <!-- <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" /> -->
					                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_serie', array('id'=>'pgcfdyn8a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
					                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
					                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_serie');?>
					                                                </div>
                                                				</span>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Dynamic :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfdyn9"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_dyn_remark', array('id'=>'pgcfdyn9', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
        				</span>
        			</div>
        		</div>
        	</div>
        </div>	
        <?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
        <div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
	        <?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/updateplay', array('id'=>$mdlPlay->play_id)), array(
	        	'type'=>'POST',
	        	'dataType'=>'json',
	        	'beforeSend'=>'function(data) {
                    $("#yt0").prop("disabled", true);
	        	}',
	        	'success'=>'js:function(data) {
	        		$(".tooltips").attr("data-original-title", "");
	        		
	        		$(".has-err").removeClass("has-err");
	        		$(".errorMessage").hide();
	        		
	        		if(data.result === "success") {
	        			$(".close").addClass("redirect");
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
	        			$("#pesan").hide();
	        			$(".redirect").click( function () {
							var redirect = "' . Yii::app()->createUrl('/Kkks/createplay') . '";
							window.location=redirect;
						});
                        $("#yt0").prop("disabled", false);
	        		} else {
	        			var myArray = JSON.parse(data.err);
	        			$.each(myArray, function(key, val) {
	        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
			        		{
	        					$("#updateplay-form #"+key+"_em_").parent().addClass("has-err");
	        					$("#updateplay-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
	        					
			        		} else {
	        					$("#updateplay-form #"+key+"_em_").text(val);                                                    
								$("#updateplay-form #"+key+"_em_").show();
		        				$("#updateplay-form #"+key+"_em_").parent().addClass("has-err");
	        				}
	        			});
	        		
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
                        $("#yt0").prop("disabled", false);
	        		}
	        	}',
	        ),
	        array('class'=>'btn btn-inverse')
	        );?>
        </div>
        <?php $this->endWidget();?>
        
        <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->

	</div>
	<!-- END PAGE CONTENT-->
</div>

<div id="confirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">?/button>
    	<h3 id="myModalLabel">Add?</h3>
  	</div>
  	<div class="modal-body">
    	<p>Are you sure want add formation name?</p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    	<button id="ok" onclick="okHit(this.id)" class="btn btn-primary">OK</button>
  	</div>
</div>
<!-- END PAGE CONTAINER-->			

<script type="text/javascript">
/* Memanggil nilai gcf untuk ditampilkan */
var sr = ['pgcfsrock3', 'pgcfsrock4', 'pgcfsrock5', 'pgcfsrock8', 'pgcfsrock9'];
var res = ['pgcfres5', 'pgcfres7', 'pgcfres9', 'pgcfres10'];
var trp = ['pgcftrap3', 'pgcftrap4', 'pgcftrap5', 'pgcftrap7', 'pgcftrap8'];
var dyn = ['pgcfdyn2', 'pgcfdyn3', 'pgcfdyn6', 'pgcfdyn7'];

var gcfCategory = ''; 

$('.gcf').each(function() {
    $(this).bind('change', function() {
        if($.inArray($(this).attr("id"), sr) != -1)
            gcfCategory = '#pgcfsrock';
        else if($.inArray($(this).attr("id"), res) != -1)
            gcfCategory = '#pgcfres';
        else if($.inArray($(this).attr("id"), trp) != -1)
            gcfCategory = '#pgcftrap';
        else
            gcfCategory = '#pgcfdyn';

        $.ajax({
            url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
            type: 'POST',
            data: {
                "name": $(this).attr("id"),
                "choosen": $(this).val(),
                "from": "play",
                "category": $(gcfCategory).val() //category proven atau analog
            },
            dataType: 'json',
            error: function(xhr) {
                console.log(xhr.status);
            },
            success: function(e) {
                $(e.state).text(e.value);
            }
        });
    }).change();
});

$('#pgcfsrock').bind('change', function() {
    if($("#pgcfsrock").val() != '') {
        for(var loop = 0; loop < sr.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": sr[loop],
                    "choosen": $('#'+sr[loop]).val(),
                    "from": "play",
                    "category": $("#pgcfsrock").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#pgcfres').bind('change', function() {
    if($('#pgcfres').val() != '') {
        for(var loop = 0; loop < res.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": res[loop],
                    "choosen": $('#'+res[loop]).val(),
                    "from": "play",
                    "category": $("#pgcfres").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#pgcftrap').bind('change', function() {
  if($('#pgcftrap').val() != '') {
    for(var loop = 0; loop < trp.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": trp[loop],
          "choosen": $('#'+trp[loop]).val(),
          "from": "play",
          "category": $("#pgcftrap").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();

$('#pgcfdyn').bind('change', function() {
  if($('#pgcfdyn').val() != '') {
    for(var loop = 0; loop < dyn.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": dyn[loop],
          "choosen": $('#'+dyn[loop]).val(),
          "from": "play",
          "category": $("#pgcfdyn").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();
/* end Memanggil nilai gcf untuk ditampilkan */

function addFormationName(id)
{
	var btnAtributId = id;
	var newAtribut = btnAtributId.substr(3);
	if($('#' + newAtribut).val() !== '')
	{
		$('#ok').attr('id', 'ok' + newAtribut);
		$('#confirmModal').modal('show');
	}
}

function okHit(id) {
	var btnAtributId = id;
	var newAtribut = btnAtributId.substr(2);
	var name = $('#' + newAtribut).val();
	$('#confirmModal').modal('hide');
	$.ajax({
		url :  '<?php echo Yii::app()->createUrl('/Kkks/addformationname')?>',
		data: {'name': name},
		type : 'POST',
		dataType : 'json',
		success: function(data){
			callAgain();
		},
	});
	$('#' + id).attr('id', 'ok');
}

function callAgain()
{
	$.ajax({
		url :  '<?php echo Yii::app()->createUrl('/Kkks/formationname')?>',
		data: '',
		type : 'POST',
		dataType : 'json',
		success: function(data){
			$('.formationname').select2({
				placeholder: 'Pilih',
				allowClear: true,
				data : data.formation,
			});
		},
	});
}
</script>

<?php 
	Yii::app()->clientScript->registerScript('formationname', "
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('.formationname').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.formation,
				});
		
				disable('pgcfsrock');
				disable('pgcfres');
				disable('pgcftrap');
				disable('pgcfdyn');
			},
		});
		
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/basin') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('#basin').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.basin,
				});
			},
		});
		
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/province') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('#provinsi').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.province,
				});
			},
		});

        function disable(id) {
        	switch(id) {
				case 'pgcfsrock' :
					if($('#' + id).val() == '') {
                		mati(['#sourceformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#sourceformationname']);
                	}
                	break;
                case 'pgcfres' :
					if($('#' + id).val() == '') {
                		mati(['#reservoirformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#reservoirformationname']);
                	}
                	break;
                case 'pgcftrap' :
					if($('#' + id).val() == '') {
                		mati(['#sealingformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#sealingformationname']);
                	}
                	break;
            }
        }
                		
        function mati(target) {
        	$(target).select2(\"readonly\", true);
            $(target).select2(\"val\", \"\");
		}
                		
        function nyala(target) {
             $(target).select2(\"enable\", true);
             $(target).select2(\"readonly\", false);
		}
                		
        $('#pgcfsrock').on('change', function(e) {
			disable('pgcfsrock');
		});
        $('#pgcfres').on('change', function(e) {
			disable('pgcfres');
		});
        $('#pgcftrap').on('change', function(e) {
			disable('pgcftrap');
		});
	");
?>
