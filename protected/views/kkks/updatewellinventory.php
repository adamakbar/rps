<?php
/* @var $this SiteController */

$this->pageTitle='WellInventory';
$this->breadcrumbs=array(
	'WellInventory',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">WELL INVENTORY</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Input RPS</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Well Inventory</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'=>'createwellinventory-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnChange'=>false,
		),
		'enableAjaxValidation'=>true,
	));?>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
		<div id="pesan"></div>
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN GENERAL DATA-->
				<div id="_gen_data_play" class="widget">
					<div class="widget-title">
						<h4><i class="icon-file"></i> WELL INVENTORY</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Well Name :</label>
                                <div class="controls wajib">
                                    <!-- <input id="_wellname" class="span3" type="text" /> -->
                                    <?php echo $form->textField($mdlWellInventory, 'a2_name', array('id'=>'_wellname', 'class'=>'span3'));?>
                                    <?php echo $form->error($mdlWellInventory, 'a2_name');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Well Category :</label>
                                <div class="controls wajib">
                                    <!-- <input id="wellcategory_" class="span3" style="text-align: center;" /> -->
                                    <?php echo $form->textField($mdlWellInventory, 'a2_category', array('id'=>'wellcategory_', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                    <br><br>
                                    <?php echo $form->error($mdlWellInventory, 'a2_category');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Drilling Year :</label>
                                <div class="controls wajib">
                                    <!-- <input id="_drillingyear" class="span3" type="text" /> -->
                                    <?php echo $form->textField($mdlWellInventory, 'a2_year_drill', array('id'=>'_drillingyear', 'class'=>'span3'));?>
                                    <?php echo $form->error($mdlWellInventory, 'a2_year_drill');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Center Latitude :</label>
                                <div class="controls">
                                    <table>
                                    	<tr>
                                    		<td>
                                    			<span class="wajib">
                                    				<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                    				<div class=" input-append">
		                                                <?php echo $form->textField($mdlWellInventory, 'center_lat_degree', array('class'=>'input-mini number-minus tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
		                                            	<span class="add-on"><sub>o</sub></span>
		                                            </div>
		                                            <?php echo $form->error($mdlWellInventory, 'center_lat_degree');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<span class="wajib">
                                    				<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                    				<div class=" input-append">
		                                                <?php echo $form->textField($mdlWellInventory, 'center_lat_minute', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute'));?>
		                                            	<span class="add-on">'</span>
		                                            </div>
		                                            <?php echo $form->error($mdlWellInventory, 'center_lat_minute');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<span class="wajib">
                                    				<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                    				<div class=" input-append">
		                                                <?php echo $form->textField($mdlWellInventory, 'center_lat_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second'));?>
		                                            	<span class="add-on">"</span>
		                                            </div>
		                                            <?php echo $form->error($mdlWellInventory, 'center_lat_second');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<span class="wajib">
                                    				<div>
                                    					<!-- <input type="text" name="wajib" class="input-mini" placeholder="S/ N"/></td> -->
                                    					<?php echo $form->textField($mdlWellInventory, 'center_lat_direction', array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N'));?>
                                    				</div>
                                    				<?php echo $form->error($mdlWellInventory, 'center_lat_direction');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<div class=" input-append"><span class="add-on">Datum WGS '84</span></div>
                                    		</td>
                                    	</tr>
                                    </table>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Center Longitude :</label>
                                <div class="controls">
                                    <table>
                                    	<tr>
                                    		<td>
                                    			<span class="wajib">
                                    				<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                    				<div class=" input-append">
		                                                <?php echo $form->textField($mdlWellInventory, 'center_long_degree', array('class'=>'input-mini number-minus tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
		                                            	<span class="add-on"><sub>o</sub></span>
			                                        </div>
			                                        <?php echo $form->error($mdlWellInventory, 'center_long_degree');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<span class="wajib">
                                    				<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                    				<div class=" input-append">
			                                            <?php echo $form->textField($mdlWellInventory, 'center_long_minute', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute'));?>
		                                            	<span class="add-on">'</span>
		                                            </div>
		                                            <?php echo $form->error($mdlWellInventory, 'center_long_minute');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<span class="wajib">
                                    				<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                    				<div class=" input-append">
		                                                <?php echo $form->textField($mdlWellInventory, 'center_long_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second'));?>
			                                        	<span class="add-on">"</span>
			                                        </div>
			                                        <?php echo $form->error($mdlWellInventory, 'center_long_second');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<span class="wajib">
                                    				<div>
	                                    				<!-- <input type="text" name="wajib" class="input-mini" placeholder="E/ W"/></td> -->
	                                    				<?php echo $form->textField($mdlWellInventory, 'center_long_direction', array('class'=>'input-mini directionlong tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W'));?>
                                    				</div>
                                    				<?php echo $form->error($mdlWellInventory, 'center_long_direction');?>
                                    			</span>
                                    		</td>
                                    		<td>
                                    			<div class=" input-append"><span class="add-on">Datum WGS '84</span></div>
                                    		</td>
                                    	</tr>
                                    </table>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>            
	</div>
	<?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
	<?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/updatewellinventory', array('id'=>$mdlWellInventory->a2_id)), array(
        	'type'=>'POST',
        	'dataType'=>'json',
        	'beforeSend'=>'function(data) {
        		$("#pesan").show();
        		$("#pesan").html("Sending...");
        	}',
        	'success'=>'js:function(data) {
				$(".tooltips").attr("data-original-title", "");
        		
        		$(".has-err").removeClass("has-err");
        		$(".errorMessage").hide();
			
        		if(data.result === "success") {
        			$("#message").html(data.msg);
        			$("#popup").modal("show");
        			$("#pesan").hide();
        		
        		} else {
					var myArray = JSON.parse(data.err);
        			$.each(myArray, function(key, val) {
        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
		        		{
        					$("#createwellinventory-form #"+key+"_em_").parent().addClass("has-err");
        					$("#createwellinventory-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
        					
		        		} else {
        					$("#createwellinventory-form #"+key+"_em_").text(val);                                                    
							$("#createwellinventory-form #"+key+"_em_").show();
	        				$("#createwellinventory-form #"+key+"_em_").parent().addClass("has-err");
        				}
        				
        			});
			
        			$("#message").html(data.msg);
        			$("#popup").modal("show");
        			$("#pesan").hide();
        		}
        	}',
        ),
        array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;')
        );?>
	<?php $this->endWidget();?>
	
	<!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel">Info</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		  	<div class="modal-footer">
		    	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  	</div>
		</div>
		<!-- end popup submit -->
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->			
