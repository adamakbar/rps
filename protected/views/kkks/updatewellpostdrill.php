<?php 
$this->pageTitle="Well";
$this->breadcrumbs=array(
	'Well',
);
?>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">WELL POSTDRILL</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
                <li><a href="<?php echo Yii::app()->createUrl('/Kkks/createwell');?>">Well</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Well Postdrill</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	
	<div id="well_page">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'updatewellpostdrill-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnChange'=>false,
			),
			'enableAjaxValidation'=>true,
		));?>
		<div id="pesan"></div>
		<div class="row-fluid">
			<div class="span12">
				<div class="widget">
					<div class="widget-body">
						<span action="#" class="form-horizontal">



							<div class="accordion">
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#tabcol_w1_1"> <strong>Well General Data</strong>
										</a>
									</div>
									<div id="tabcol_w1_1" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Well Name :</label>
												<div class="controls wajib">
													<!-- <input id="well_name" type="text" name="wajib" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/> -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_name", array('id'=>"wl_name", 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Please using unique and meaningful well name.', 'data-original-title'=>'Well Name'));?>
							                        <div id="Well_wl_name_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Latitude :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																	<div class=" input-append">
							                                        	<?php echo CHtml::activeTextField($mdlWell, "lat_degree", array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
							                                            <span class="add-on">&#176;</span>
																	</div>
																	<div id="Well_lat_degree_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																	<div class=" input-append">
							                                        	<?php echo CHtml::activeTextField($mdlWell, "lat_minute", array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
							                                            <span class="add-on">'</span>
																	</div>
																	<div id="Well_lat_minute_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																	<div class=" input-append">
							                                        	<?php echo CHtml::activeTextField($mdlWell, "lat_second", array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
							                                            <span class="add-on">"</span>
																	</div>
																	<div id="Well_lat_second_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<div>
																		<!-- <input type="text" name="wajib" class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
																		<?php echo CHtml::activeTextField($mdlWell, "lat_direction", array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<div id="Well_lat_direction_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
															</td>
														</tr>
													</table>      
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Longitude :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																	<div class=" input-append">
							                                        	<?php echo CHtml::activeTextField($mdlWell, "long_degree", array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
							                                            <span class="add-on">&#176;</span>
																	</div>
																	<div id="Well_long_degree_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																	<div class=" input-append">
							                                        	<?php echo CHtml::activeTextField($mdlWell, "long_minute", array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
							                                            <span class="add-on">'</span>
																	</div>
																	<div id="Well_long_minute_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																	<div class=" input-append">
							                                        	<?php echo CHtml::activeTextField($mdlWell, "long_second", array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
							                                            <span class="add-on">"</span>
																	</div>
																	<div id="Well_long_second_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<div>
																		<!-- <input type="text" name="wajib" class="input-mini" placeholder="E/ W"/><span class="add-on"> -->
																		<?php echo CHtml::activeTextField($mdlWell, "long_direction", array('value'=>'E', 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<div id="Well_long_direction_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
															</td>
														</tr>
													</table>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Well Result :</label>
												<div class="controls wajib">
													<!-- <input id="well_category" class="span3" type="text"/> -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_result", array('id'=>'well_result_postdrill', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_result_em_" class="errorMessage" style="display:none"></div>
							                	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Well Type :</label>
												<div id="wajib" class="controls">
													<!-- <input id="well_type" class="span3" style="text-align: center;" />  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_type", array('id'=>"well_type1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_type_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Onshore or Offshore :</label>
												<div id="wajib" class="controls">
													<!-- <input id="onoffshore" class="span3" style="text-align: center;" />  -->
								                    <?php echo CHtml::activeTextField($mdlWell, "wl_shore", array('id'=>"onoffshore1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_shore_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Terrain :</label>
												<div id="wajib" class="controls">
													<!-- <input id="terrain" class="span3" style="text-align: center;" />  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_terrain", array('id'=>"terrain1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_terrain_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Well Status :</label>
												<div class="controls">
													<!-- <input id="well_status" class="span3" style="text-align: center;" />  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_status", array('id'=>"well_status1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_status_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Targeted Formation Name :</label>
												<div class="controls wajib">
													<!-- <input id="targeted_forname" class="span3" type="text" name="wajib" style="text-align: center;"/>  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_formation", array('id'=>"targetedformationname", 'class'=>'span3 formationname', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_formation_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Date Well Completed :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_date_complete", array('id'=>"dsw8_1", 'class'=>'m-wrap medium popovers disable-input', 'style'=>'max-width:137px;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Date Well Completed'));?>
							                            <span class="add-on"><i class="icon-calendar"></i></span>
							                            <span id="cleardsw8_1" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
													</div>
													<div id="Well_wl_date_complete_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Targeted Total Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-prepend input-append">
																		<span class="add-on">TVD</span>
																		<!-- <input id="ttd_1" type="text" style="max-width:160px"/>  -->
							                                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_depth_tvd", array('id'=>'ttd_1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:126px'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Well_wl_target_depth_tvd_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span style="margin-left: 24px">
																	<div class="input-prepend input-append">
																		<span class="add-on">MD</span>
																		<!-- <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_depth_md", array('id'=>'ttd_2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:126px;'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Well_wl_target_depth_md_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Actual Total Depth :</label>
												<div class="controls">
													<div class="input-prepend input-append">
														<span class="add-on">TVD</span>
														<!-- <input id="actual_total_depth" type="text" style="max-width:160px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_actual_depth", array('id'=>'actual_total_depth', 'class'=>'number', 'style'=>'max-width:126px'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Well_wl_actual_depth_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
												<div class="controls">
													<div class="input-prepend input-append">
														<span class="add-on">TVD</span>
														<!-- <input id="tpprt_" type="text" style="max-width:160px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_play", array('id'=>'tpprt_', 'class'=>'number', 'style'=>'max-width:126px'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Well_wl_target_play_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
												<div class="controls">
													<div class="input-prepend input-append">
														<span class="add-on">TVD</span>
														<!-- <input id="apprt_" type="text" style="max-width:160px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_actual_play", array('id'=>'apprt_', 'class'=>'number', 'style'=>'max-width:126px'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Well_wl_actual_play_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of MDT Sample :</label>
												<div class="controls">
													<!-- <input id="num_mdt" class="span3" style="text-align: center;" />  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_number_mdt", array('id'=>"num_mdt1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_number_mdt_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of RFT Sample :</label>
												<div class="controls">
													<!-- <input id="num_rft" class="span3" style="text-align: center;" />  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_number_rft", array('id'=>"num_rft1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_number_rft_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Reservoir Initial Pressure :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="rip_" type="text" style="max-width:160px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_res_pressure", array('id'=>'rip_', 'class'=>'number', 'style'=>'max-width:126px'));?>
							                            <span class="add-on">psig</span>
													</div>
													<div id="Well_wl_res_pressure_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Last Reservoir Pressure :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="lpr_" type="text" style="max-width:160px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_last_pressure", array('id'=>'lpr_', 'class'=>'number', 'style'=>'max-width:126px'));?>
							                            <span class="add-on">psig</span>
													</div>
													<div id="Well_wl_last_pressure_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Pressure Gradient :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="pressure_gradient" type="text" style="max-width:147px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_pressure_gradient", array('id'=>'pressure_gradient', 'class'=>'number', 'style'=>'max-width:117px'));?>
							                            <span class="add-on">psig/ft</span>
													</div>
													<div id="Well_wl_pressure_gradient_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Last Reservoir Temperature :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="lrt_" type="text" style="max-width:170px"/>  -->
							                            <?php echo CHtml::activeTextField($mdlWell, "wl_last_temp", array('id'=>'lrt_', 'class'=>'number', 'style'=>'max-width:137px'));?>
							                            <span class="add-on">&#176;C</span>
													</div>
													<div id="Well_wl_last_temp_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											
											<div class="control-group">
												<label class="control-label">Actual Well Integrity :</label>
												<div class="controls">
													<!-- <input id="actual_well_integrity" class="span3" style="text-align: center;" />  -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_integrity", array('id'=>"actual_well_integrity1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_integrity_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Availability of Electrolog Data Acquisition :</label>
												<div class="controls">
													<!-- <input id="availability_electrolog" class="span3" style="text-align: center;" /> -->
							                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_avail", array('id'=>"availability_electrolog1", 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Well_wl_electro_avail_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">List of Electrolog Data :</label>
												<div class="controls">
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list1", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list2", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list3", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list4", array('class'=>'span2 no-comma'));?>
							                    </div>
											</div>
											<div class="control-group">
												<div class="controls">
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list5", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list6", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list7", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list8", array('class'=>'span2 no-comma'));?>
							                    </div>
											</div>
											<div class="control-group">
												<div class="controls">
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list9", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list10", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list11", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list12", array('class'=>'span2 no-comma'));?>
							                    </div>
											</div>
											<div class="control-group">
												<div class="controls">
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list13", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list14", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list15", array('class'=>'span2 no-comma'));?>
													<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list16", array('class'=>'span2 no-comma'));?>
							                    </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#tabcol_w1_2"> <strong>Hydrocarbon Indication</strong>
										</a>
									</div>
									<div id="tabcol_w1_2" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Oil Show or Reading :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span class="">
																	<div>
																		<!-- <input id="oilshow_reading1" name="wajib" class="span3" type="text"/> -->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_oil_show", array('id'=>'oilshow_reading1', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px;'));?>
																	</div>
																	<div id="Wellzone_zone_hc_oil_show_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="oilshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />-->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_oil_show_tools", array('id'=>'oilshow_reading2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
																	</div>
																	<div id="Wellzone_zone_hc_oil_show_tools_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>       
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Gas Show or Reading :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span class="">
																	<div>
																		<!-- <input id="gasshow_reading1" name="wajib" class="span3" type="text"/>-->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_gas_show", array('id'=>'gasshow_reading1', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px;'));?>
																	</div>
																	<div id="Wellzone_zone_hc_gas_show_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="gasshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />-->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_gas_show_tools", array('id'=>'gasshow_reading2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
																	</div>
																	<div id="Wellzone_zone_hc_gas_show_tools_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>  
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Well Making Water Cut :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="wellmaking_watercut" type="text" style="max-width:172px"/>-->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_water_cut", array('id'=>'wellmaking_watercut', 'class'=>'number', 'style'=>'max-width:137px'));?>
							                            <span class="add-on">%</span>
													</div>
													<div id="Wellzone_zone_hc_water_cut_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Water Bearing Level Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-prepend input-append">
																		<span class="add-on">GWC</span>
																		<!-- <input id="waterbearing_levdept1" type="text" style="max-width:127px"/>-->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_water_gwc", array('id'=>'waterbearing_levdept1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:95px'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_hc_water_gwc_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_water_gwd_tools", array('id'=>'waterbearing_levdept2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
																	</div>
																	<div id="Wellzone_zone_hc_water_gwd_tools_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Water Bearing Level Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-prepend input-append">
																		<span class="add-on">OWC</span>
																		<!-- <input id="waterbearing_levdept3" type="text" style="max-width:127px"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_water_owc", array('id'=>'waterbearing_levdept3', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:95px'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_hc_water_owc_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_hc_water_owc_tools", array('id'=>'waterbearing_levdept4', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
							                                        </div>
							                                        <div id="Wellzone_zone_hc_water_owc_tools_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>
							                    </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#tabcol_w1_3"> <strong>Rock Property (by Sampling)</strong>
										</a>
									</div>
									<div id="tabcol_w1_3" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Rock Sampling Method :</label>
												<div class="controls">
													<!-- <input id="rocksampling" class="span3" type="text" style="text-align:center;" /> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_method", array('id'=>"rocksampling1", 'class'=>'span3', 'style'=>'text-align:center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Wellzone_zone_rock_method_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Petrography Analysis :</label>
												<div class="controls">
													<!-- <input id="petrograp_analys" class="span3" type="text" style="text-align:center;" /> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_petro", array('id'=>"petrograp_analys1", 'class'=>'span3', 'style'=>'text-align:center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Wellzone_zone_rock_petro_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sample Quantity :</label>
												<div class="controls">
													<!-- <input id="sample" class="span3" type="text"/> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_petro_sample", array('id'=>'sample', 'class'=>'span3 number'));?>
							                        <div id="Wellzone_zone_rock_petro_sample_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Top Interval Coring Sample Depth :</label>
												<div class="controls">
													<div class="input-prepend input-append">
														<span class="add-on">TVD</span>
														<!-- <input id="ticsd_" type="text" style="max-width:152px;"/> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_top_depth", array('id'=>'ticsd_', 'class'=>'number', 'style'=>'max-width:118px;'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Wellzone_zone_rock_top_depth_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Bottom Interval Coring Sample
													Depth :</label>
												<div class="controls">
													<div class="input-prepend input-append">
														<span class="add-on">TVD</span>
														<!-- <input id="bitcsd_" type="text" style="max-width:152px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_bot_depth", array('id'=>'bitcsd_', 'class'=>'number', 'style'=>'max-width:118px;'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Wellzone_zone_rock_bot_depth_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of Total Core Barrels :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="num_totalcarebarrel" type="text" style="max-width:170px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_barrel", array('id'=>'num_totalcarebarrel', 'class'=>'number', 'style'=>'max-width:137px;'));?>
							                            <span class="add-on">bbl</span>
													</div>
													<div id="Wellzone_zone_rock_barrel_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">1 Core Barrel Equal to :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="1corebarrel" type="text" style="max-width:173px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_barrel_equal", array('id'=>'1corebarrel', 'class'=>'number', 'style'=>'max-width:137px;'));?>
							                           	<span class="add-on">ft</span>
													</div>
													<div id="Wellzone_zone_rock_barrel_equal_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Total Recoverable Core Data :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="trcd_" type="text" style="max-width:173px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_total_core", array('id'=>'trcd_', 'class'=>'number', 'style'=>'max-width:137px;'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Wellzone_zone_rock_total_core_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Preservative Core Data :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="pcd_" type="text" style="max-width:173px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_preservative", array('id'=>'pcd_', 'class'=>'number', 'style'=>'max-width:137px;'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Wellzone_zone_rock_preservative_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Routine Core Analysis :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div>
																		<!-- <input id="rca_1" class="span3" type="text" style="text-align:center;" /> -->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_routine", array('id'=>'rca_1', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; text-align:center;'));?>
																	</div>
																	<div id="Wellzone_zone_rock_routine_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_routine_sample", array('id'=>'rca_2', 'class'=>'span3 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'Sample Quantity..'));?>
																	</div>
																	<div id="Wellzone_zone_rock_routine_sample_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>        
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">SCAL Data Analysis :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div>
																		<!-- <input id="scal_data1" class="span3" type="text" style="text-align:center;" /> -->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_scal", array('id'=>'scal_data1', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; text-align:center;'));?>
																	</div>
																	<div id="Wellzone_zone_rock_scal_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
																		<?php echo CHtml::activeTextField($mdlWellZone, "zone_rock_scal_sample", array('id'=>'scal_data2', 'class'=>'span3 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'Sample Quantity..'));?>
																	</div>
																	<div id="Wellzone_zone_rock_scal_sample_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</table>   
							                    </div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Clastic Reservoir Property (by Electrolog)</th>
															<th>Gross Reservoir Thickness</th>
															<th>Reservoir Vshale Content (GR Log)</th>
															<th>Reservoir Vshale Content (SP Log)</th>
															<th>Net to Gross</th>
															<th>Reservoir Porosity</th>
															<th>Reservoir Saturation (Cut)</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P10 (Max)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/>-->
								                                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p10_thickness", array('id'=>"wpd_clastic_p10_thickness1", 'class'=>'span6 clastic_grtp10 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
								                                        <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p10_thickness_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p10_gr", array('id'=>"wpd_clastic_p10_gr1", 'class'=>'span6 clastic_rvcp10 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">API</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p10_gr_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p10_sp", array('id'=>"wpd_clastic_p10_sp1", 'class'=>'span6 clastic_rvc2p10 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">mV</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p10_sp_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p10_net", array('id'=>"wpd_clastic_p10_net1", 'class'=>'span6 clastic_ntgp10 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p10_net_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p10_por", array('id'=>"wpd_clastic_p10_por1", 'class'=>'span6 clastic_porp10 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p10_por_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p10_satur", array('id'=>"wpd_clastic_p10_satur1", 'class'=>'span6 clastic_saturp10 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p10_satur_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
														<tr>
															<th>P50 (Mean)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p50_thickness", array('id'=>"wpd_clastic_p50_thickness1", 'class'=>'span6 clastic_grtp50 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p50_thickness_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p50_gr", array('id'=>"wpd_clastic_p50_gr1", 'class'=>'span6 clastic_rvcp50 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">API</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p50_gr_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p50_sp", array('id'=>"wpd_clastic_p50_sp1", 'class'=>'span6 clastic_rvc2p50 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">mV</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p50_sp_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p50_net", array('id'=>"wpd_clastic_p50_net1", 'class'=>'span6 clastic_ntgp50 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p50_net_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p50_por", array('id'=>"wpd_clastic_p50_por1", 'class'=>'span6 clastic_porp50 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p50_por_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p50_satur", array('id'=>"wpd_clastic_p50_satur1", 'class'=>'span6 clastic_saturp50 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p50_satur_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
														<tr>
															<th>P90 (Min)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p90_thickness", array('id'=>"wpd_clastic_p90_thickness1", 'class'=>'span6 clastic_grtp90 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p90_thickness_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p90_gr", array('id'=>"wpd_clastic_p90_gr1", 'class'=>'span6 clastic_rvcp90 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">API</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p90_gr_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p90_sp", array('id'=>"wpd_clastic_p90_sp1", 'class'=>'span6 clastic_rvc2p90 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">mV</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p90_sp_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p90_net", array('id'=>"wpd_clastic_p90_net1", 'class'=>'span6 clastic_ntgp90 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p90_net_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p90_por", array('id'=>"wpd_clastic_p90_por1", 'class'=>'span6 clastic_porp90 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p90_por_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_clastic_p90_satur", array('id'=>"wpd_clastic_p90_satur1", 'class'=>'span6 clastic_saturp90 rock_clastic number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_clastic_p90_satur_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</tbody>
												</table>
												<blockquote>
													<small>Ratio of reservoir net thickness with reservoir
														gross thickness</small>
												</blockquote>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Carbonate Reservoir Property (by Electrolog)</th>
															<th>Gross Reservoir Thickness</th>
															<th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
															<th>Thickness Reservoir Total Pore</th>
															<th>Net to Gross</th>
															<th>Reservoir Porosity</th>
															<th>Reservoir Saturation (Cut)</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P10 (Max)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p10_thickness", array('id'=>"wpd_carbo_p10_thickness1", 'class'=>'span6 carbo_grtp10 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p10_thickness_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p10_dtc", array('id'=>"wpd_carbo_p10_dtc1", 'class'=>'span6 carbo_dtcp10 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">usec/ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p10_dtc_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p10_total", array('id'=>"wpd_carbo_p10_total1", 'class'=>'span6 carbo_totalp10 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p10_total_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p10_net", array('id'=>"wpd_carbo_p10_net1", 'class'=>'span6 carbo_netp10 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p10_net_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p10_por", array('id'=>"wpd_carbo_p10_por1", 'class'=>'span6 carbo_porp10 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                           	<span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p10_por_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p10_satur", array('id'=>"wpd_carbo_p10_satur1", 'class'=>'span6 carbo_saturp10 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p10_satur_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
														<tr>
															<th>P50 (Mean)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p50_thickness", array('id'=>"wpd_carbo_p50_thickness1", 'class'=>'span6 carbo_grtp50 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p50_thickness_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p50_dtc", array('id'=>"wpd_carbo_p50_dtc1", 'class'=>'span6 carbo_dtcp50 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">usec/ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p50_dtc_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p50_total", array('id'=>"wpd_carbo_p50_total1", 'class'=>'span6 carbo_totalp50 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p50_total_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p50_net", array('id'=>"wpd_carbo_p50_net1", 'class'=>'span6 carbo_netp50 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p50_net_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p50_por", array('id'=>"wpd_carbo_p50_por1", 'class'=>'span6 carbo_porp50 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p50_por_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text">  -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p50_satur", array('id'=>"wpd_carbo_p50_satur1", 'class'=>'span6 carbo_saturp50 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p50_satur_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
														<tr>
															<th>P90 (Min)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p90_thickness", array('id'=>"wpd_carbo_p90_thickness1", 'class'=>'span6 carbo_grtp90 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p90_thickness_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p90_dtc", array('id'=>"wpd_carbo_p90_dtc1", 'class'=>'span6 carbo_dtcp90 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">usec/ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p90_dtc_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p90_total", array('id'=>"wpd_carbo_p90_total1", 'class'=>'span6 carbo_totalp90 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                           	<span class="add-on">ft</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p90_total_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p90_net", array('id'=>"wpd_carbo_p90_net1", 'class'=>'span6 carbo_netp90 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p90_net_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p90_por", array('id'=>"wpd_carbo_p90_por1", 'class'=>'span6 carbo_porp90 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p90_por_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_carbo_p90_satur", array('id'=>"wpd_carbo_p90_satur1", 'class'=>'span6 carbo_saturp90 rock_carbonate number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">%</span>
																	</div>
																	<div id="Wellzone_zone_carbo_p90_satur_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</tbody>
												</table>
												<blockquote>
													<small>Ratio of porous reservoir thickness with reservoir
														thickness</small>
												</blockquote>
											</div>
										</div>
									</div>
								</div>
								
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#tabcol_w1_4"> <strong>Fluid Property (by Sampling)</strong>
										</a>
									</div>
									<div id="tabcol_w1_4" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Sample Date :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="z1_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_date", array('id'=>"z2_11", 'class'=>'m-wrap medium popovers disable-input', 'style'=>'max-width:137px;', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Sample Date'));?>
							                            <span class="add-on"><i class="icon-calendar"></i></span>
							                            <span id="clearz2_11" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
													</div>
													<div id="Wellzone_zone_fluid_date_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Sampled at :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="sample_at" type="text" style="max-width:172px;"/> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_sample", array('id'=>'sample_at', 'class'=>'number', 'style'=>'max-width:137px;'));?>
							                            <span class="add-on">ft</span>
													</div>
													<div id="Wellzone_zone_fluid_sample_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Gas Oil Ratio :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="gas_oil_ratio" type="text" style="max-width:145px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_ratio", array('id'=>'gas_oil_ratio', 'class'=>'number', 'style'=>'max-width:110px;'));?>
							                            <span class="add-on">scf/bbl</span>
													</div>
													<div id="Wellzone_zone_fluid_ratio_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Separator Pressure :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="separator_pressure" type="text" style="max-width:162px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_pressure", array('id'=>'separator_pressure', 'class'=>'number', 'style'=>'max-width:126px;'));?>
							                            <span class="add-on">psig</span>
													</div>
													<div id="Wellzone_zone_fluid_pressure_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Separator Temperature :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="separator_temperature" type="text" style="max-width:173px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_temp", array('id'=>'separator_temperature', 'class'=>'number-minus', 'style'=>'max-width:137px;'));?>
							                            <span class="add-on">&#176;C</span>
													</div>
													<div id="Wellzone_zone_fluid_temp_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Tubing Pressure :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="tubing_pressure" type="text" style="max-width:162px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_tubing", array('id'=>'tubing_pressure', 'class'=>'number', 'style'=>'max-width:126px;'));?>
							                            <span class="add-on">psig</span>
													</div>
													<div id="Wellzone_zone_fluid_tubing_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Casing Pressure :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="casing_pressure" type="text" style="max-width:162px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_casing", array('id'=>'casing_pressure', 'class'=>'number', 'style'=>'max-width:126px;'));?>
							                            <span class="add-on">psig</span>
													</div>
													<div id="Wellzone_zone_fluid_casing_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Sampled by :</label>
												<div class="controls">
													<!-- <input id="sampleby" class="span3" type="text"/> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_by", array('id'=>'sampleby', 'class'=>'span3'));?>
							                        <div id="Wellzone_zone_fluid_by_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Reports Availability :</label>
												<div class="controls">
													<!-- <input id="report_avail" class="span3" type="text" style="text-align:center;" /> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_report", array('id'=>'report_avail', 'class'=>'span3', 'style'=>'text-align:center; position: relative; display: inline-block;'));?>
							                        <p style="margin-bottom: 30px; display: inline-block;"></p>
							                        <div id="Wellzone_zone_fluid_report_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
												<div class="controls">
													<!-- <input id="hydro_finger" class="span3" type="text"/> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_finger", array('id'=>'hydro_finger', 'class'=>'span3'));?>
							                        <div id="Wellzone_zone_fluid_finger_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Oil Gravity at 60 &#176;F :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="oilgravity_60" type="text" style="max-width:165px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_grv_oil", array('id'=>'oilgravity_60', 'class'=>'number', 'style'=>'max-width:130px;'));?>
							                            <span class="add-on">API</span>
													</div>
													<div id="Wellzone_zone_fluid_grv_oil_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Gas Gravity at 60 &#176;F :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="gasgravity_60" type="text" style="max-width:165px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_grv_gas", array('id'=>'gasgravity_60', 'class'=>'number', 'style'=>'max-width:130px;'));?>
							                            <span class="add-on">API</span>
													</div>
													<div id="Wellzone_zone_fluid_grv_gas_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Condensate Gravity at 60 &#176;F :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="condensate_gravity" type="text" style="max-width:165px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_grv_conden", array('id'=>'condensate_gravity', 'class'=>'number', 'style'=>'max-width:130px;'));?>
							                            <span class="add-on">API</span>
													</div>
													<div id="Wellzone_zone_fluid_grv_conden_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">PVT Analysis :</label>
												<div class="controls">
													<!-- <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" /> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_pvt", array('id'=>'pvt_analysis', 'class'=>'span3'));?>
							                        <div id="Wellzone_zone_fluid_pvt_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sample Quantity :</label>
												<div class="controls">
													<!-- <input id="sample_" class="span3" type="text"/> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_quantity", array('id'=>'sample_', 'class'=>'number', 'class'=>'span3'));?>
							                        <div id="Wellzone_zone_fluid_quantity_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<label class="control-label">Gas Deviation Factor (Initial Z) :</label>
												<div class="controls">
													<!-- <input id="gas_initialZ" class="span3" type="text"/> -->
							                        <?php echo CHtml::activeTextField($mdlWellZone, "zone_fluid_z", array('id'=>'gas_initialZ', 'class'=>'span3'));?>
							                        <div id="Wellzone_zone_fluid_z_em_" class="errorMessage" style="display:none"></div>
							                    </div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Initial Formation Volume Factor</th>
															<th>Max</th>
															<th>Mean</th>
															<th>Min</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>Oil (Boi)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fvf_oil_p10", array('id'=>"wpd_fvf_oil_p101", 'class'=>'span6 initial_oilp10 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">RBO/STB</span>
																	</div>
																	<div id="Wellzone_zone_fvf_oil_p10_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fvf_oil_p50", array('id'=>"wpd_fvf_oil_p501", 'class'=>'span6 initial_oilp50 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">RBO/STB</span>
																	</div>
																	<div id="Wellzone_zone_fvf_oil_p50_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fvf_oil_p90", array('id'=>"wpd_fvf_oil_p901", 'class'=>'span6 initial_oilp90 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">RBO/STB</span>
																	</div>
																	<div id="Wellzone_zone_fvf_oil_p90_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
														<tr>
															<th>Gas (Bgi)</th>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fvf_gas_p10", array('id'=>"wpd_fvf_gas_p101", 'class'=>'span6 initial_gasp10 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">RCF/SCF</span>
																	</div>
																	<div id="Wellzone_zone_fvf_gas_p10_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fvf_gas_p50", array('id'=>"wpd_fvf_gas_p501", 'class'=>'span6 initial_gasp50 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">RCF/SCF</span>
																	</div>
																	<div id="Wellzone_zone_fvf_gas_p50_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
							                                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_fvf_gas_p90", array('id'=>"wpd_fvf_gas_p901", 'class'=>'span6 initial_gasp90 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
							                                            <span class="add-on">RCF/SCF</span>
																	</div>
																	<div id="Wellzone_zone_fvf_gas_p90_em_" class="errorMessage" style="display:none"></div>
																</span>
															</td>
														</tr>
													</tbody>
												</table>
												<blockquote>
													<small>Please fill Bgi, Boi, or Both based on Hydrocarbon
														Indication, fill with 0 at Bgi if only Boi available or
														vice versa.</small>
												</blockquote>
											</div>
											<div class="control-group">
												<label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="oil_viscocity" type="text" style="max-width:165px;" /> -->
							                            <?php echo CHtml::activeTextField($mdlWellZone, "zone_viscocity", array('id'=>'zone_viscocity', 'class'=>'number', 'style'=>'max-width:137px;'));?>
							                            <span class="add-on">cP</span>
													</div>
													<div id="Wellzone_zone_viscocity_em_" class="errorMessage" style="display:none"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</span>
					</div>
				</div>
			</div>
		</div>
		<div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
			<?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/updatewellpostdrill', array('id'=>$mdlWell->wl_id)), array(
	        	'type'=>'POST',
	        	'dataType'=>'json',
	        	'beforeSend'=>'function(data) {
	        		$(".tooltips").attr("data-original-title", "");
	        		
	        		$(".has-err").removeClass("has-err");
	        		$(".errorMessage").hide();
	        	
                    $("#yt0").prop("disabled", true);
	        		if($("#prospect_type").val() == "") {
	        			$("#Well_prospect_type_em_").text("Prospect Type cannot be blank.");                                                    
						$("#Well_prospect_type_em_").show();
		        		$("#Well_prospect_type_em_").parent().addClass("has-err");
	        			$("#pesan").hide();
	        			data.abort();
	        			
	        		}
	        	}',
	        	'success'=>'js:function(data) {
	        		
	        		
	        		if(data.result === "success") {
						$(".close").addClass("redirect");
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
	        			$(".redirect").click( function () {
							var redirect = "' . Yii::app()->createUrl('/Kkks/createwell') . '";
							window.location=redirect;
						});
                        $("#yt0").prop("disabled", false);
	        		} else {
	        			var myArray = JSON.parse(data.err);
	        			$.each(myArray, function(key, val) {
	        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
			        		{
	        					$("#updatewellpostdrill-form #"+key+"_em_").parent().addClass("has-err");
	        					$("#updatewellpostdrill-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
	        					
			        		} else {
	        					$("#updatewellpostdrill-form #"+key+"_em_").text(val);                                                    
								$("#updatewellpostdrill-form #"+key+"_em_").show();
		        				$("#updatewellpostdrill-form #"+key+"_em_").parent().addClass("has-err");
	        				}
	        				
	        			});
	        		
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
                        $("#yt0").prop("disabled", false);
	        		}
	        	}',
	        ),
	        array('class'=>'btn btn-inverse')
	        );?>
        </div>
        <?php $this->endWidget();?>
        
        <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->
	</div>
</div>

<?php 
	Yii::app()->clientScript->registerScript('formationname', "
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('.formationname').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.formation,
				});
			},
		});
	", CClientScript::POS_END);
?>

