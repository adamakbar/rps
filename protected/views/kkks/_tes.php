
<?php var_dump(unserialize($form)); die();?>
<!-- Well 1 -->
                                    <?php $a = 0;
                                    	$total = 0;
                                    	$i = 1;
                                    ?>
                                    <?php $ac = 'active';?>
                                    <?php for($i = 2; $i <= 2; $i++) { ?>
                                    <?php //foreach ($mdlWellPostdrilljs as $j => $mdlWellPostdrillj) {?>
                                    <div class="tab-pane <?php if($i == 1) echo $ac;?>" id="tab_w<?=$i;?>">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <!-- <input id="well_name" type="text" name="wajib" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_name", array('id'=>'well_name', 'class'=>'span3 popovers', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'Please using unique and meaningful well name.', 'data-original-title'=>'Well Name'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                                                <!-- <input type="text" name="wajib" class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
                                                                <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrillj, "[$i]lat_degree", array('class'=>'input-mini', 'placeholder'=>'degree'));?>
		                                                        	<span class="add-on"><sub>o</sub></span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrillj, "[$i]lat_minute", array('class'=>'input-mini', 'placeholder'=>'minute'));?>
		                                                        	<span class="add-on">'</span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrillj, "[$i]lat_second", array('class'=>'input-mini', 'placeholder'=>'second'));?>
		                                                        	<span class="add-on">"</span>
		                                                        </div>
		                                                        <?php echo $form->textField($mdlWellPostdrillj, "[$i]lat_direction", array('class'=>'input-mini', 'placeholder'=>'S/ N'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
                                                                <!-- <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
                                                                <!-- <input type="text" name="wajib" class="input-mini" placeholder="E/ W"/><span class="add-on"> -->
                                                                <div class=" input-append">
	                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]long_degree", array('class'=>'input-mini', 'placeholder'=>'degree'));?>
	                                                        	<span class="add-on"><sub>o</sub></span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrillj, "[$i]long_minute", array('class'=>'input-mini', 'placeholder'=>'minute'));?>
		                                                        	<span class="add-on">'</span>
		                                                        </div>
		                                                        <div class=" input-append">
		                                                        	<?php echo $form->textField($mdlWellPostdrillj, "[$i]long_second", array('class'=>'input-mini', 'placeholder'=>'second'));?>
		                                                        	<span class="add-on">"</span>
		                                                        </div>
	                                                        	<?php echo $form->textField($mdlWellPostdrillj, "[$i]long_direction", array('class'=>'input-mini', 'placeholder'=>'E/ W'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="well_category" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_category", array('id'=>'well_category', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="well_type" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_type", array('id'=>'well_type', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="onoffshore" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_shore", array('id'=>'onoffshore', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <!-- <input id="terrain" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_terrain", array('id'=>'terrain', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <!-- <input id="well_status" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_status", array('id'=>'well_status', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <!-- <input id="targeted_forname" class="span3" type="text" name="wajib" style="text-align: center;"/>  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_formation", array('id'=>'targeted_forname', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_date_complete", array('id'=>'1dsw8', 'class'=>'m-wrap medium popovers', 'style'=>'max-width:172px;', 'data-trigger'=>'hover', 'data-placement'=>'right', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Date Well Completed'));?>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="ttd_1" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_target_depth_tvd", array('id'=>'ttd_1', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <!-- <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_target_depth_md", array('id'=>'ttd_2', 'style'=>'max-width:160px; margin-left:24px;'));?>
                                                                    <span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="actual_total_depth" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_actual_depth", array('id'=>'actual_total_depth', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <!-- <input id="tpprt_" type="text" style="max-width:160px"/>  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_target_play", array('id'=>'tpprt_', 'style'=>'max-width:160px'));?>
                                                                <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="apprt_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_actual_play", array('id'=>'apprt_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <!-- <input id="num_mdt" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_number_mdt", array('id'=>'num_mdt', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <!-- <input id="num_rft" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_number_rft", array('id'=>'num_rft', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="rip_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_res_pressure", array('id'=>'rip_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="lpr_" type="text" style="max-width:160px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_last_pressure", array('id'=>'lpr_', 'style'=>'max-width:160px'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="pressure_gradient" type="text" style="max-width:147px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_pressure_gradient", array('id'=>'pressure_gradient', 'style'=>'max-width:147px'));?>
                                                                    <span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="lrt_" type="text" style="max-width:170px"/>  -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_last_temp", array('id'=>'lrt_', 'style'=>'max-width:170px'));?>
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="actual_well_integrity" class="span3" style="text-align: center;" />  -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_integrity", array('id'=>'actual_well_integrity', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <!-- <input id="availability_electrolog" class="span3" style="text-align: center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_avail", array('id'=>'availability_electrolog', 'class'=>'span3', 'style'=>'text-align: center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list1", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list2", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list3", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list4", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list5", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list6", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list7", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list8", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list9", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list10", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list11", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list12", array('class'=>'span3'));?>
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
																<!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
                                                                <!-- <input class="span3" type="text" /> -->
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list13", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list14", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list15", array('class'=>'span3'));?>
																<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_electro_list16", array('class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <!-- <input id="oilshow_reading1" name="wajib" class="span3" type="text"/> -->
                                                                <!-- <input id="oilshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />--> 
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_oil_show", array('id'=>'oilshow_reading1', 'class'=>'span3'));?>
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_oil_show_tools", array('id'=>'oilshow_reading2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <!-- <input id="gasshow_reading1" name="wajib" class="span3" type="text"/>-->
                                                                <!-- <input id="gasshow_reading2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />-->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_gas_show", array('id'=>'gasshow_reading2', 'class'=>'span3'));?>
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_gas_show_tools", array('id'=>'gasshow_reading2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="wellmaking_watercut" type="text" style="max-width:172px"/>-->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_water_cut", array('id'=>'wellmaking_watercut', 'style'=>'max-width:172px'));?>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span>
                                                                    	<!-- <input id="waterbearing_levdept1" type="text" style="max-width:127px"/>-->
                                                                    	<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_water_gwc", array('id'=>'waterbearing_levdept1', 'style'=>'max-width:127px'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                                <!-- <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_water_gwd_tools", array('id'=>'waterbearing_levdept2', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span>
                                                                    	<!-- <input id="waterbearing_levdept3" type="text" style="max-width:127px"/> -->
                                                                    	<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_water_owc", array('id'=>'waterbearing_levdept3', 'style'=>'max-width:127px'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                                <!-- <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_hc_water_owc_tools", array('id'=>'waterbearing_levdept4', 'class'=>'span3', 'style'=>'margin-left:24px;', 'placeholder'=>'By Tools Indication'));?>
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <!-- <input id="rocksampling" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_method", array('id'=>'rocksampling', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="petrograp_analys" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_petro", array('id'=>'petrograp_analys', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sample" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_petro_sample", array('id'=>'sample', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="ticsd_" type="text" style="max-width:152px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_top_depth", array('id'=>'ticsd_', 'style'=>'max-width:152px;'));?>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="bitcsd_" type="text" style="max-width:152px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_bot_depth", array('id'=>'bitcsd_', 'style'=>'max-width:152px;'));?>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="num_totalcarebarrel" type="text" style="max-width:170px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_barrel", array('id'=>'num_totalcarebarrel', 'style'=>'max-width:170px;'));?>
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="1corebarrel" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_barrel_equal", array('id'=>'1corebarrel', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="trcd_" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_total_core", array('id'=>'trcd_', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="pcd_" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_preservative", array('id'=>'pcd_', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="rca_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                <!-- <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_routine", array('id'=>'rca_1', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_routine_sample", array('id'=>'rca_2', 'class'=>'span3', 'placeholder'=>'Sample Quantity..'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="scal_data1" class="span3" type="text" style="text-align:center;" /> -->
                                                                <!-- <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_scal", array('id'=>'scal_data1', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_rock_scal_sample", array('id'=>'scal_data2', 'class'=>'span3', 'placeholder'=>'Sample Quantity..'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/>-->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p10_thickness", array('id'=>'wpd_clastic_p10_thickness', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p10_gr", array('id'=>'wpd_clastic_p10_gr', 'class'=>'span6'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p10_sp", array('id'=>'wpd_clastic_p10_sp', 'class'=>'span6'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p10_net", array('id'=>'wpd_clastic_p10_net', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p10_por", array('id'=>'wpd_clastic_p10_por', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p10_satur", array('id'=>'wpd_clastic_p10_satur', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p50_thickness", array('id'=>'wpd_clastic_p50_thickness', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p50_gr", array('id'=>'wpd_clastic_p50_gr', 'class'=>'span6'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p50_sp", array('id'=>'wpd_clastic_p50_sp', 'class'=>'span6'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p50_net", array('id'=>'wpd_clastic_p50_net', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p50_por", array('id'=>'wpd_clastic_p50_por', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append"> 
                                                                        		<!-- <input id="#" class="span6" type="text"> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p50_satur", array('id'=>'wpd_clastic_p50_satur', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p90_thickness", array('id'=>'wpd_clastic_p90_thickness', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p90_gr", array('id'=>'wpd_clastic_p90_gr', 'class'=>'span6'));?>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p90_sp", array('id'=>'wpd_clastic_p90_sp', 'class'=>'span6'));?>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p90_net", array('id'=>'wpd_clastic_p90_net', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p90_por", array('id'=>'wpd_clastic_p90_por', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_clastic_p90_satur", array('id'=>'wpd_clastic_p90_satur', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">API</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">mV</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</i></span></td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p10_thickness", array('id'=>'wpd_carbo_p10_thickness', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p10_dtc", array('id'=>'wpd_carbo_p10_dtc', 'class'=>'span6'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p10_total", array('id'=>'wpd_carbo_p10_total', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p10_net", array('id'=>'wpd_carbo_p10_net', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p10_por", array('id'=>'wpd_carbo_p10_por', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p10_satur", array('id'=>'wpd_carbo_p10_satur', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p50_thickness", array('id'=>'wpd_carbo_p50_thickness', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p50_dtc", array('id'=>'wpd_carbo_p50_dtc', 'class'=>'span6'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p50_total", array('id'=>'wpd_carbo_p50_total', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p50_net", array('id'=>'wpd_carbo_p50_net', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p50_por", array('id'=>'wpd_carbo_p50_por', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text">  -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p50_satur", array('id'=>'wpd_carbo_p50_satur', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p90_thickness", array('id'=>'wpd_carbo_p90_thickness', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p90_dtc", array('id'=>'wpd_carbo_p90_dtc', 'class'=>'span6'));?>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p90_total", array('id'=>'wpd_carbo_p90_total', 'class'=>'span6'));?>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p90_net", array('id'=>'wpd_carbo_p90_net', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p90_por", array('id'=>'wpd_carbo_p90_por', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                        		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_carbo_p90_satur", array('id'=>'wpd_carbo_p90_satur', 'class'=>'span6'));?>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">usec/ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">ft</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                        <td>
                                                                        	<div class=" input-append">
                                                                        		<input id="#" name="custom" class="span6" type="text"readonly/>
                                                                        	<span class="add-on">%</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w<?=$i;?>_<?=++$a?>">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w<?=$i;?>_<?=$a?>" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="z1_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_date", array('id'=>'z1_1_5', 'class'=>'m-wrap medium popovers', 'style'=>'max-width:172px;', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Sample Date'));?>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="sample_at" type="text" style="max-width:172px;"/> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_sample", array('id'=>'sample_at', 'style'=>'max-width:172px;'));?>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="gas_oil_ratio" type="text" style="max-width:145px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_ratio", array('id'=>'gas_oil_ratio', 'style'=>'max-width:145px;'));?>
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="separator_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_pressure", array('id'=>'separator_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="separator_temperature" type="text" style="max-width:173px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_temp", array('id'=>'separator_temperature', 'style'=>'max-width:173px;'));?>
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="tubing_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_tubing", array('id'=>'tubing_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="casing_pressure" type="text" style="max-width:162px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_casing", array('id'=>'casing_pressure', 'style'=>'max-width:162px;'));?>
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sampleby" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_by", array('id'=>'sampleby', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <!-- <input id="report_avail" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_report", array('id'=>'report_avail', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="hydro_finger" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_finger", array('id'=>'hydro_finger', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="oilgravity_60" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_grv_oil", array('id'=>'oilgravity_60', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="gasgravity_60" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_grv_gas", array('id'=>'gasgravity_60', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="condensate_gravity" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_grv_conden", array('id'=>'condensate_gravity', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <!-- <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" /> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_pvt", array('id'=>'pvt_analysis', 'class'=>'span3', 'style'=>'text-align:center;'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <!-- <input id="sample_" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_quantity", array('id'=>'sample_', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <!-- <input id="gas_initialZ" class="span3" type="text"/> -->
                                                                <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fluid_z", array('id'=>'gas_initialZ', 'class'=>'span3'));?>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fvf_oil_p10", array('id'=>'wpd_fvf_oil_p10', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fvf_oil_p50", array('id'=>'wpd_fvf_oil_p50', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fvf_oil_p90", array('id'=>'wpd_fvf_oil_p90', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fvf_gas_p10", array('id'=>'wpd_fvf_gas_p10', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fvf_gas_p50", array('id'=>'wpd_fvf_gas_p50', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
                                                                    		<?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_fvf_gas_p90", array('id'=>'wpd_fvf_gas_p90', 'class'=>'span6'));?>
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                    <td>
                                                                    	<div class=" input-append">
                                                                    		<input id="#" name="custom" class="span6" type="text" readonly />
                                                                    	<span class="add-on">bbl/stb</span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <!-- <input id="oil_viscocity" type="text" style="max-width:165px;" /> -->
                                                                    <?php echo $form->textField($mdlWellPostdrillj, "[$i]wpd_viscocity", array('id'=>'oil_viscocity', 'style'=>'max-width:165px;'));?>
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++;?>
                                    <?php $a = 0;?>
                                    <?php $total++;?>
                                    <?php }?>