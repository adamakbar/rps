<?php
/* @var $this SiteController */

$this->pageTitle='Profile';
$this->breadcrumbs=array(
	'Profile',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<div id="play_page">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
	            <h3 class="page-title">Profile<small> Working Area Detail</small></h3>
	            <ul class="breadcrumb">
	                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
	                <li><a href="#"><strong>Profile</strong></a><span class="divider-last">&nbsp;</span></li>
	            </ul>
			</div>
		</div>
	
		<!-- BEGIN PAGE CONTENT-->
	
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'createparticipant-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		));?>
		
		
        
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div id="_gen_profile" class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> PROFILE DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Operator :</label>
                                <div class="controls">
                                    <!-- <input id="e_cov_operator" class="span3" style="text-align: center;"/> -->
                                    <?php echo $form->textField($mdlKkks, 'kkks_is_operator', array('id'=>'operator', 'class'=>'span5', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                    <?php echo $form->error($mdlKkks, 'kkks_is_operator');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">PSC Sign Date :</label>
                                <div class="controls">
                                    <div class=" input-append">
                                        <!-- <input id="date_cov1" type="text" class="m-wrap medium" style="max-width:180px;"/> -->
                                        <?php echo $form->textField($mdlPsc, 'psc_date_sign', array('id'=>'date_sign', 'class'=>'m-wrap medium disable-input', 'style'=>'max-width:138px;'));?>
                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                        <span id="cleardate_sign" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                    </div>
                                    <?php echo $form->error($mdlPsc, 'psc_date_sign');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">PSC End Date :</label>
                                <div class="controls">
                                    <div class=" input-append">
                                        <!-- <input id="date_cov2" type="text" class="m-wrap medium" style="max-width:180px;"/> -->
                                        <?php echo $form->textField($mdlPsc, 'psc_date_end', array('id'=>'date_end', 'class'=>'m-wrap medium disable-input', 'style'=>'max-width:138px;'));?>
                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                        <span id="cleardate_end" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                    </div>
                                    <?php echo $form->error($mdlPsc, 'psc_date_end');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Authorized By :</label>
                                <div class="controls">
                                    <!-- <input class="span3" type="text" /> -->
                                    <?php echo $form->textField($mdlPsc, 'psc_authorized_by', array('id'=>'author_by', 'class'=>'span3'));?>
                                    <?php echo $form->error($mdlPsc, 'psc_authorized_by');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">E-mail :</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <!-- <i class="icon-envelope"></i> -->
                                        <!-- <input style="max-width:162px;" type="text"/> -->
                                        <?php echo $form->textField($mdlPsc, 'psc_authorized_email', array('id'=>'author_email', 'class'=>'span3', 'style'=>'width:162px;'));?>
                                        <span class="add-on"><i class="icon-envelope"></i></span>
                                    </div>
                                    <?php echo $form->error($mdlPsc, 'psc_authorized_email');?>
                                </div>
                            </div>
                            <?php echo CHtml::ajaxSubmitButton('Save', $this->createUrl('/kkks/createprofile'), array(
					        	'type'=>'POST',
                            	'data'=>'js:jQuery(this).parents("form").serialize()+"&type=profile"',
					        	'dataType'=>'json',
					        	'beforeSend'=>'function(data) {
                            		if($("#operator").val() == "") {
                            			$("#createparticipant-form #Kkks_kkks_is_operator_em_").text("Cannot be blank");                                                    
										$("#createparticipant-form #Kkks_kkks_is_operator_em_").show();
                            			$("#createparticipant-form #Kkks_kkks_is_operator_em_").parent().addClass("has-err");
                            			$("#message").html("Failed! Has input error");
	        							$("#popup").modal("show");
                            			return false;
                            		} else {
                            			$(".has-err").removeClass("has-err");
	        							$(".errorMessage").hide();
                            		}
                            		
					        		$("#pesan").show();
					        		$("#pesan").html("Sending...");
					        	}',
					        	'success'=>'js:function(data) {
					        		if(data.result === "success") {
                            			$("#message").html(data.msg);
	        							$("#popup").modal("show");
                            		
					        			$("#pesan").html(data.msg);
					        			$("#pesan").css("color", "#f00");
					        		} else {
					        			var myArray = JSON.parse(data.err);
					        			$.each(myArray, function(key, val) {
				        					$("#createparticipant-form #"+key+"_em_").text(val);                                                    
											$("#createparticipant-form #"+key+"_em_").show();
					        				$("#createparticipant-form #"+key+"_em_").parent().addClass("has-err");
					        			});
                            			$("#message").html(data.msg);
	        							$("#popup").modal("show");
					        		}
					        	}',
					        ),
					        array('class'=>'btn', 'style'=>'margin-bottom:15px; width: 100px')
					        );?>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <?php $this->endWidget();?>
        <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel">Info</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->
	</div>
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->		
<?php 
Yii::app()->clientScript->registerScript('ajaxupdate', "
		
	contractor();
	function contractor() {
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/contractorname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('#operator').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.contractor,
				});
		
				if(data.count == 1) $('#operator').select2('val', data.selected);
			},
		});	
	}
");

?>
