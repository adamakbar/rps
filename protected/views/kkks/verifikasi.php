<?php
/* @var $this SiteController */

$this->pageTitle='Data Verification';
$this->breadcrumbs=array(
	'Verifikasi',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->			    			
			<h3 class="page-title">Data Verification</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Summary</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Data Verification</strong></a><span class="divider-last">&nbsp;</span></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
					<div class="widget-body">
						<p style="text-align: center;">
                          Before submit your report to SKK Migas, please verify your data with the button below.
                        </p>
						<div class="text-center">
							<?php echo CHtml::button('Verify', array('id'=>'btnverifikasi', 'class'=>'btn', 'onclick'=>'{verifikasi(this.id);}'));?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
			
<div id="confirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
    	<h3 id="myModalLabel">Info</h3>
  	</div>
  	<div class="modal-body">
    	<p>Are you sure?</p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    	<button id="ok" onclick="okHit(this.id)" class="btn btn-primary">OK</button>
  	</div>
</div>

<script type="text/javascript">
function verifikasi(id) {
	$('#confirmModal').modal('show');
}

function okHit(id) {
	$('#confirmModal').modal('hide');
	$.ajax({
		url :  '<?php echo Yii::app()->createUrl('/Kkks/verifikasi')?>',
		type : 'POST',
		dataType : 'json',
		success: function(data){
			alert('Success');
		},
	});
}
</script>
