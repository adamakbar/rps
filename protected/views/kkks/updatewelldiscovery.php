<?php 
$this->pageTitle="Well";
$this->breadcrumbs=array(
	'Well',
);
?>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">WELL DISCOVERY</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
                <li><a href="<?php echo Yii::app()->createUrl('/Kkks/createwell');?>">Well</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Well Discovery</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	
	<div id="well_page">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'updatewellpostdrill-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnChange'=>false,
			),
			'enableAjaxValidation'=>true,
		));?>
		<div id="pesan"></div>
		<div class="row-fluid">
			<div class="span12">
				<div class="widget">
					<div class="widget-body">
						<span action="#" class="form-horizontal">
						
						
                                                <div class="accordion">
                                                    <!-- Well General Data -->
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w_">
                                                                <strong>Well General Data</strong>
                                                            </a>
                                                        </div>
                                                        <div id="tabcol_w_" class="accordion-body collapse">
                                                            <div class="accordion-inner">
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Name :</label>
                                                                    <div class="controls wajib">
                                                                        <!-- <input id="well_name" type="text"  class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_name", array('id'=>"well_name", 'class'=>"span3 popovers", 'data-trigger'=>"hover", 'data-placement'=>"right", 'data-container'=>"body", 'data-content'=>"Please using unique and meaningful well name.", 'data-original-title'=>"Well Name"));?>
                                                                        <div id="Well_wl_name_em_" class="errorMessage" style="display:none"></div>
                                                                        <?php echo CHtml::activeHiddenField($mdlWell, "wl_id", array('id'=>"wl_id"));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Latitude :</label>
                                                                    <div class="controls">
                                                                    	<table>
                                                                    		<tr>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_degree", array('id'=>"lat_degree", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
				                                                        					<span class="add-on">&#176;</span>
																						</div>
																						<div id="Well_lat_degree_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_minute", array('id'=>"lat_minute", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">'</span>
																						</div>
																						<div id="Well_lat_minute_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_second", array('id'=>"lat_second", 'class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">"</span>
																						</div>
																						<div id="Well_lat_second_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div>
																							<!-- <input type="text"  class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_direction", array('id'=>"lat_direction", 'class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
																						</div>
																						<div id="Well_lat_direction_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                                    			</td>
                                                                    		</tr>
                                                                    	</table>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Longitude :</label>
                                                                    <div class="controls">
                                                                    	<table>
                                                                    		<tr>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_degree", array('id'=>"long_degree", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
			                                                        						<span class="add-on">&#176;</span>
																						</div>
																						<div id="Well_long_degree_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_minute", array('id'=>"long_minute", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">'</span>
																						</div>
																						<div id="Well_long_minute_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_second", array('id'=>"long_second", 'class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">"</span>
																						</div>
																						<div id="Well_long_second_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
                                                                    				<span class="wajib">
																						<div>
																							<!-- <input type="text"  class="input-mini" placeholder="E/ W"/><span class="add-on"> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_direction", array('value'=>'E', 'id'=>"long_direction", 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																						</div>
																						<div id="Well_long_direction_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
																					<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
																				</td>
                                                                    		</tr>
                                                                    	</table>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Result :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="well_type" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_result", array('id'=>"well_result_discovery", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_result_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Type :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="well_type" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_type", array('id'=>"well_type1", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_type_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                <label class="control-label">Onshore or Offshore :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="onoffshore" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_shore", array('id'=>"onoffshore", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_shore_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Terrain :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="terrain" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_terrain", array('id'=>"terrain", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_terrain_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Status :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="well_status" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_status", array('id'=>"well_status", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_status_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Targeted Formation Name :</label>
                                                                    <div class="controls wajib">
                                                                        <!-- <input id="targeted_forname" class="span3" type="text"  style="text-align: center;"/> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_formation", array('id'=>"targetedformationname", 'class'=>"span3 formationname", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_formation_em_" class="errorMessage" style="display:none"></div>
                                                                        <p class="help-block"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Date Well Completed :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:165px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_date_complete", array('id'=>"dsw8_", 'class'=>"m-wrap medium popovers disable-input", 'style'=>"max-width:137px;", 'data-trigger'=>"hover", 'data-placement'=>"right", 'data-container'=>"body", 'data-content'=>"If Year that only available, please choose 1-January for Day and Month, if not leave it blank.", 'data-original-title'=>"Date Well Completed"));?>
                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                            <span id="cleardsw8_" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                                        </div>
                                                                        <div id="Well_wl_date_complete_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Targeted Total Depth :</label>
                                                                    <div class="controls">
                                                                    	<table>
                                                                    		<tr>
                                                                    			<td>
                                                                    				<span>
	                                                                    				<div class="input-prepend input-append">
	                                                                    					<span class="add-on">TVD</span>
				                                                                            <!-- <input id="ttd_1" type="text" style="max-width:155px"/> -->
				                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_depth_tvd", array('id'=>"ttd_1", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:126px;"));?>
				                                                                            <span class="add-on">ft</span>
				                                                                        </div>
				                                                                        <div id="Well_wl_target_depth_tvd_em_" class="errorMessage" style="display:none"></div>
			                                                                        </span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span style="margin-left: 24px;">
	                                                                    				<div class="input-prepend input-append">
	                                                                    					<span class="add-on">MD</span>
				                                                                            <!-- <input id="ttd_2" type="text" style="max-width:155px; margin-left:24px;"/> -->
				                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_depth_md", array('id'=>"ttd_2", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:126px;"));?>
				                                                                            <span class="add-on">ft</span>
				                                                                        </div>
				                                                                        <div id="Well_wl_target_depth_md_em_" class="errorMessage" style="display:none"></div>
			                                                                        </span>
                                                                    			</td>
                                                                    		</tr>
                                                                    	</table>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Actual Total Depth :</label>
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                        	<span class="add-on">TVD</span>
                                                                            <!-- <input id="actual_total_depth" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_actual_depth", array('id'=>"actual_total_depth", 'class'=>"number", 'style'=>"max-width:126px"));?>
                                                                            <span class="add-on">ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_actual_depth_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                        	<span class="add-on">TVD</span>
	                                                                        <!-- <input id="tpprt_" type="text" style="max-width:155px"/> -->
	                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_target_play", array('id'=>"tpprt_", 'class'=>"number", 'style'=>"max-width:126px"));?>
	                                                                        <span class="add-on">ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_target_play_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                        	<span class="add-on">TVD</span>
                                                                            <!-- <input id="apprt_" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_actual_play", array('id'=>"apprt_", 'class'=>"number", 'style'=>"max-width:126px"));?>
                                                                            <span class="add-on">ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_actual_play_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of MDT Sample :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="num_mdt" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_number_mdt", array('id'=>"num_mdt", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_number_mdt_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of RFT Sample :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="num_rft" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_number_rft", array('id'=>"num_rft", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_number_rft_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Reservoir Initial Pressure :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="rip_" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_res_pressure", array('id'=>"rip_", 'class'=>"number", 'style'=>"max-width:126px;"));?>
                                                                            <span class="add-on">psig</span>
                                                                        </div>
                                                                        <div id="Well_wl_res_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Last Reservoir Pressure :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="lpr_" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_last_pressure", array('id'=>"lpr_", 'class'=>"number", 'style'=>"max-width:126px;"));?>
                                                                            <span class="add-on">psig</span>
                                                                        </div>
                                                                        <div id="Well_wl_last_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Pressure Gradient :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="pressure_gradient" type="text" style="max-width:145px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_pressure_gradient", array('id'=>"pressure_gradient", 'class'=>"number", 'style'=>"max-width:117px;"));?>
                                                                            <span class="add-on">psig/ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_pressure_gradient_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Last Reservoir Temperature :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="lrt_" type="text" style="max-width:165px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_last_temp", array('id'=>"lrt_", 'class'=>"number", 'style'=>"max-width:137px;"));?>
                                                                            <span class="add-on">&#176;C</span>
                                                                        </div>
                                                                        <div id="Well_wl_last_temp_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Actual Well Integrity :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="actual_well_integrity" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_integrity", array('id'=>"actual_well_integrity", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_integrity_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="availability_electrolog" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_avail", array('id'=>"availability_electrolog", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_electro_avail_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">List of Electrolog Data :</label>
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list1", array('id'=>"wl_electro_list1", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list2", array('id'=>"wl_electro_list2", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list3", array('id'=>"wl_electro_list3", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list4", array('id'=>"wl_electro_list4", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list5", array('id'=>"wl_electro_list5", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list6", array('id'=>"wl_electro_list6", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list7", array('id'=>"wl_electro_list7", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list8", array('id'=>"wl_electro_list8", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list9", array('id'=>"wl_electro_list9", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list10", array('id'=>"wl_electro_list10", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list11", array('id'=>"wl_electro_list11", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list12", array('id'=>"wl_electro_list12", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list13", array('id'=>"wl_electro_list13", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list14", array('id'=>"wl_electro_list14", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list15", array('id'=>"wl_electro_list15", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list16", array('id'=>"wl_electro_list16", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                                                   
                                                        </div>
                                                    </div>

                                                    <!-- Zone for Well 1-->
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w_2">
                                                                <strong>Zone Data</strong>
                                                            </a>
                                                        </div>
                                                        <div id="tabcol_w_2" class="accordion-body collapse">
                                                            <div class="accordion-inner">
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of Penetrated Production Zones :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="wz_" class="span3" type="text" style="text-align: center;"/> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_total_zone", array('id'=>"wz_", 'class'=>"span3 wz", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_total_zone_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <!-- Notification -->
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <blockquote>
                                                                            <small>Detail of zone data can be filled at generated form below.</small>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                                <!-- Notification -->
                                                                <div class="row-fluid">
                                                                    <div class="span12">        
                                                                        <!-- Number Zone -->
                                                                        <div id="well_rumah_zone" class="tabbable tabbable-custom">
                                                                            <ul class="nav nav-tabs">
                                                                                <li id="li_well_zone_1" class="active"><a id="well_zone_1" href="#well_tab_z1" data-toggle="tab">Zone 1</a></li>
                                                                                <li id="li_well_zone_2"><a id="well_zone_2" href="#well_tab_z2" data-toggle="tab" class="hidden">Zone 2</a></li>
                                                                                <li id="li_well_zone_3"><a id="well_zone_3" href="#well_tab_z3" data-toggle="tab" class="hidden">Zone 3</a></li>
                                                                                <li id="li_well_zone_4"><a id="well_zone_4" href="#well_tab_z4" data-toggle="tab" class="hidden">Zone 4</a></li>
                                                                                <li id="li_well_zone_5"><a id="well_zone_5" href="#well_tab_z5" data-toggle="tab" class="hidden">Zone 5</a></li>
                                                                            </ul>
                                                                            <div class="tab-content">
                                                                                <!-- Zone 1 -->
                                                                                <?php $b = 0;?>
                                                                                <?php $ac = 'active';?>
                                                                                <?php for($j = 1; $j <= 5; $j++) { ?>
                                                                                <div class="tab-pane <?php if($j == 1) echo $ac;?>" id="well_tab_z<?=$j;?>">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b?>">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <!-- <input id="zonename" class="span3"  type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_name", array('id'=>"zonename$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_name_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <?php echo CHtml::activeHiddenField($mdlWellZone, "[$j]zone_id", array('id'=>"zone_id$j"));?>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <!-- <input id="z_welltest<?=$j?>" class="span3"  type="text" style="text-align: center;"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_test", array('id'=>"welltest$j", 'class'=>"span3", "style"=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_test_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="z1_3"  type="text" class="m-wrap medium" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_test_date", array('id'=>"z_$j", 'class'=>"m-wrap medium disable-input", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <span id="clearz_<?=$j;?>" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_test_date_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Result</label>
                                                                                                        <div class="controls">
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_result", array('id'=>"zoneresult$j", 'class'=>"span3", "style"=>"text-align: center; position: relative; display: inline-block;")) ;?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_result_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Area</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_area", array('id'=>"zonearea$j", 'class'=>'number', 'style'=>'max-width:130px;')) ;?>
                                                                                                                <span class="add-on">acre</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_area_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="zonethickness"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_thickness", array('id'=>"zonethickness$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="zoneinterval"  type="text" style="max-width:148px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_depth", array('id'=>"zoneinterval$j", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_depth_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="perforation_interval" type="text" style="max-width:148px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_perforation", array('id'=>"perforation_interval$j", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_perforation_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="welltest_type" class="span3" type="text" style="text-align: center;"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_well_test", array('id'=>"welltest_type$j", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_well_test_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="welltest_total" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_well_duration", array('id'=>"welltest_total$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_well_duration_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialflow" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_flow", array('id'=>"initialflow$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_flow_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialshutin" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_shutin", array('id'=>"initialshutin$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_shutin_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="tubingsize" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_tubing_size", array('id'=>"tubingsize$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">in</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_tubing_size_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialtemperature"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_temp", array('id'=>"initialtemperature$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">&#176;C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_temp_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialreservoir"  type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_pressure", array('id'=>"zone_initial_pressure$j", 'class'=>"popovers number", 'style'=>"max-width:120px;", 'data-trigger'=>"hover", 'data-placement'=>"right", 'data-container'=>"body", 'data-content'=>"Reservoir or well pressure from well analysis result (Bottom-hole Pressure).", 'data-original-title'=>"Initial Reservoir Pressure"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudo Steady State :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="res_presure" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_pseudostate", array('id'=>"res_presure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_pseudostate_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="pressurewell_for" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_well_formation", array('id'=>"pressurewell_for$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_well_formation_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="pressurewell_head" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_head", array('id'=>"pressurewell_head$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_head_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="res_pressure" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_pressure_wellbore", array('id'=>"res_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_pressure_wellbore_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="avg_porpsity"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_average_por", array('id'=>"avg_porpsity$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_average_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="watercut"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_water_cut", array('id'=>"watercut$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_water_cut_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                            <!-- <input id="initialwater_sat"  type="text" style="max-width:161px;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_water", array('id'=>"initialwater_sat$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_water_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="lowtest_gas" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_low_gas", array('id'=>"lowtest_gas$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_low_gas_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="lowtest_oil" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_low_oil", array('id'=>"lowtest_oil$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_low_oil_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="freewater" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_free_water", array('id'=>"freewater$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_free_water_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                        	<div class="input-append">
	                                                                                                            <!-- <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/> -->
	                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_grv_gas", array('id'=>"gas_grav$j", 'class'=>"popovers", 'style'=>"max-width:130px;", 'data-trigger'=>'hover', 'data-container'=>"body", 'data-content'=>"Ratio of gas density with air density", 'data-original-title'=>"Gas Gravity"));?>
	                                                                                                            <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_grv_gas_em_" class="errorMessage" style="display:none"></div>                                                                                  
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                        	<div class="input-append">
	                                                                                                            <!-- <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/> -->
	                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_grv_grv_oil", array('id'=>"oil_grav$j", 'class'=>"popovers", 'style'=>"max-width:130px;", 'data-trigger'=>"hover", 'data-container'=>"body", 'data-content'=>"Ratio of oil density with air density (API units)", 'data-original-title'=>"Oil Gravity"));?>
                                                                                                            	<span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_grv_grv_oil_em_" class="errorMessage" style="display:none"></div>  
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellbore" type="text" style="max-width:138px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_wellbore_coefficient", array('id'=>"zone_wellbore_coefficient$j", 'class'=>"number", 'style'=>"max-width:105px;"));?>
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_wellbore_coefficient_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellbore_time" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_wellbore_time", array('id'=>"wellbore_time$j", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_wellbore_time_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="rsbt_" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_res_shape", array('id'=>"rsbt_$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_res_shape_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilchoke"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_oil_choke", array('id'=>"oilchoke$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_oil_choke_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilflow"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_oil_flow", array('id'=>"oilflow$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_oil_flow_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gaschoke"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_gas_choke", array('id'=>"gaschoke$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_gas_choke_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasflow"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_gas_flow", array('id'=>"gasflow$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_gas_flow_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasoilratio" type="text" style="max-width:139px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_gasoil_ratio", array('id'=>"gasoilratio$j", 'class'=>"number", 'style'=>"max-width:110px;"));?>
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_gasoil_ratio_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                        	<div class="input-append">
	                                                                                                            <!-- <input id="condensategas" class="span3" type="text"/> -->
	                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_conden_ratio", array('id'=>"zone_prod_conden_ratio$j", 'style'=>"max-width:130px;"));?>
	                                                                                                            <span class="add-on">bbl/scf</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_conden_ratio_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="cummulative_pro_gas" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_cumm_gas", array('id'=>"cummulative_pro_gas$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_cumm_gas_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="cummulative_pro_oil" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_cumm_oil", array('id'=>"cummulative_pro_oil$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_cumm_oil_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                            	<tr>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="ab_openflow1" type="text" style="max-width:149px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_aof_bbl", array('id'=>"ab_openflow1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px;"));?>
				                                                                                                                <span class="add-on">bbl/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_aof_bbl_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_aof_scf", array('id'=>"ab_openflow2$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px; margin-left:24px;"));?>
				                                                                                                                <span class="add-on">scf/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_aof_scf_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            	</tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                            	<tr>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="criticalrate1" type="text" style="max-width:149px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_critical_bbl", array('id'=>"criticalrate1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px;"));?>
				                                                                                                                <span class="add-on">bbl/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_critical_bbl_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_critical_scf", array('id'=>"criticalrate2$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px; margin-left:24px;"));?>
				                                                                                                                <span class="add-on">scf/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_critical_scf_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            	</tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                            	<tr>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="pro_index1" type="text" style="max-width:149px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_index_bbl", array('id'=>"pro_index1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px;"));?>
				                                                                                                                <span class="add-on">bbl/d/psi</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_index_bbl_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_index_scf", array('id'=>"pro_index2$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px; margin-left:24px;"));?>
				                                                                                                                <span class="add-on">scf/d/psi</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_index_scf_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            	</tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="diffusity_fact" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_diffusity", array('id'=>"diffusity_fact$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_diffusity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="permeability" type="text" style="max-width:158px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_permeability", array('id'=>"permeability$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_permeability_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">How Long Infinite-acting period last :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="iafit_" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_infinite", array('id'=>"iafit_$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_infinite_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius (Rw) :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellradius"  type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_well_radius", array('id'=>"wellradius$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_well_radius_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">When the Pseudostate Condition is Reached :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="pfit_" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_pseudostate", array('id'=>"pfit_$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_pseudostate_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Radius of Investigation (Re)</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="res_bondradius"  type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_res_radius", array('id'=>"res_bondradius$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_res_radius_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="deltaP" type="text" style="max-width:155px;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_delta", array('id'=>"deltaP$j", 'class'=>"span3 number"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_delta_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="wellbore_skin" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_wellbore", array('id'=>"wellbore_skin$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_wellbore_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="rock_compress" type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_compres_rock", array('id'=>"rock_compress$j", 'class'=>"number", 'style'=>"max-width:115px;"));?>
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_compres_rock_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="fluid_compress" type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_compres_fluid", array('id'=>"fluid_compress$j", 'class'=>"number", 'style'=>"max-width:115px;"));?>
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_compres_fluid_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="total_compress" type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_compres_total", array('id'=>"total_compress$j", 'class'=>"number", 'style'=>"max-width:115px;"));?>
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_compres_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="secd_porosity" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_second", array('id'=>"secd_porosity$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_wl_integrity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">&#955; :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="I_" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_lambda", array('id'=>"I_$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_second_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">&#969; :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="W_" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_omega", array('id'=>"W_$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_omega_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="oilshow_read1"  class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_oil_show", array('id'=>"oilshow_read1$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_oil_show_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="oilshow_read2" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_oil_show_tools", array('id'=>"oilshow_read2$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_oil_show_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="gasshow_read1"  class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_gas_show", array('id'=>"gasshow_read1$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_gas_show_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="gasshow_read2" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_gas_show_tools", array('id'=>"gasshow_read2$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_gas_show_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellmak_watercut" type="text" style="max-width:165px"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_cut", array('id'=>"wellmak_watercut$j", 'class'=>"number", 'style'=>"max-width:130px"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_water_cut_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div class="input-prepend input-append">
				                                                                                                                <span class="add-on">GWC</span>
				                                                                                                                <!-- <input id="water_bearing1" type="text" style="max-width:120px"/> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_gwc", array('id'=>"water_bearing1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:95px"));?>
				                                                                                                                <span class="add-on">ft</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_water_gwc_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
	                                                                                                        					<!-- <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
	                                                                                                            				<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_gwd_tools", array('id'=>"water_bearing2$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width:165px; margin-left:24px;", 'placeholder'=>"By Tools Indication"));?>
                                                                                                            				</div>
                                                                                                            				<div id="Wellzone_<?php echo $j;?>_zone_hc_water_gwd_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div class="input-prepend input-append">
				                                                                                                                <span class="add-on">OWC</span>
				                                                                                                                <!-- <input id="water_bearing3" type="text" style="max-width:120px"/> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_owc", array('id'=>"water_bearing3$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:95px"));?>
				                                                                                                                <span class="add-on">ft</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_water_owc_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                                                            					<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_owc_tools", array('id'=>"water_bearing4$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width:165px; margin-left:24px;", 'placeholder'=>"By Tools Indication"));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_hc_water_owc_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="rock_sampling" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_method", array('id'=>"rock_sampling$j", 'class'=>"span3", 'style'=>"text-align:center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_method_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="petro_analys" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_petro", array('id'=>"petro_analys$j", 'class'=>"span3", 'style'=>"text-align:center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_petro_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="sample" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_petro_sample", array('id'=>"sample$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_petro_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="ticsd_" type="text" style="max-width:145px;"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_top_depth", array('id'=>"ticsd_$j", 'class'=>"number", 'style'=>"max-width:110px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_top_depth_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="bicsd_" type="text" style="max-width:145px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_bot_depth", array('id'=>"bicsd_$j", 'class'=>"number", 'style'=>"max-width:110px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_bot_depth_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="num_totalcare" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_barrel", array('id'=>"num_totalcare$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_barrel_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="1corebarrel" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_barrel_equal", array('id'=>"corebarrel$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_barrel_equal_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="totalrec" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_total_core", array('id'=>"totalrec$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_total_core_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="precore" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_preservative", array('id'=>"precore$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_preservative_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="rca_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                        						<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_routine", array('id'=>"rca_1$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px;"));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_routine_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>-->
                                                                                                            					<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_routine_sample", array('id'=>"rca_$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px; margin-left:24px;", 'placeholder'=>"Sample Quantity.."));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_routine_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="scal_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                        						<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_scal", array('id'=>"scal_1$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px;"));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_scal_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                                                            					<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_scal_sample", array('id'=>"scal_2$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px; margin-left:24px;", 'placeholder'=>"Sample Quantity.."));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_scal_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>HC Saturation (1-Sw)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_thickness", array('id'=>"zone_clastic_p10_thickness$j", 'class'=>"span6 number zone_clastic_p10_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!--  <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_gr", array('id'=>"zone_clastic_p10_gr$j", 'class'=>"span6 number zone_clastic_p10_gr tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">API</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_gr_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_sp", array('id'=>"zone_clastic_p10_sp$j", 'class'=>"span6 number zone_clastic_p10_sp tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">mV</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_sp_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_net", array('id'=>"zone_clastic_p10_net$j", 'class'=>"span6 number zone_clastic_p10_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_por", array('id'=>"zone_clastic_p10_por$j", 'class'=>"span6 number zone_clastic_p10_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_satur", array('id'=>"zone_clastic_p10_satur$j", 'class'=>"span6 number zone_clastic_p10_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_thickness", array('id'=>"zone_clastic_p50_thickness$j", 'class'=>"span6 number zone_clastic_p50_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_gr", array('id'=>"zone_clastic_p50_gr$j", 'class'=>"span6 number zone_clastic_p50_gr tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">API</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_gr_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_sp", array('id'=>"zone_clastic_p50_sp$j", 'class'=>"span6 number zone_clastic_p50_sp tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">mV</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_sp_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_net", array('id'=>"zone_clastic_p50_net$j", 'class'=>"span6 number zone_clastic_p50_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_por", array('id'=>"zone_clastic_p50_por$j", 'class'=>"span6 number zone_clastic_p50_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_satur", array('id'=>"zone_clastic_p50_satur$j", 'class'=>"span6 number zone_clastic_p50_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_thickness", array('id'=>"zone_clastic_p90_thickness$j", 'class'=>"span6 number zone_clastic_p90_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_gr", array('id'=>"zone_clastic_p90_gr$j", 'class'=>"span6 number zone_clastic_p90_gr tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">API</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_gr_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_sp", array('id'=>"zone_clastic_p90_sp$j", 'class'=>"span6 number zone_clastic_p90_sp tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">mV</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_sp_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_net", array('id'=>"zone_clastic_p90_net$j", 'class'=>"span6 number zone_clastic_p90_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_por", array('id'=>"zone_clastic_p90_por$j", 'class'=>"span6 number zone_clastic_p90_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_satur", array('id'=>"zone_clastic_p90_satur$j", 'class'=>"span6 number zone_clastic_p90_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>HC Saturation (1-Sw)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_thickness", array('id'=>"zone_carbo_p10_thickness$j", 'class'=>"span6 number zone_carbo_p10_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_dtc", array('id'=>"zone_carbo_p10_dtc$j", 'class'=>"span6 number zone_carbo_p10_dtc tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">usec/ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_dtc_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_total", array('id'=>"zone_carbo_p10_total$j", 'class'=>"span6 number zone_carbo_p10_total tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_net", array('id'=>"zone_carbo_p10_net$j", 'class'=>"span6 number zone_carbo_p10_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_por", array('id'=>"zone_carbo_p10_por$j", 'class'=>"span6 number zone_carbo_p10_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_satur", array('id'=>"zone_carbo_p10_satur$j", 'class'=>"span6 number zone_carbo_p10_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_thickness", array('id'=>"zone_carbo_p50_thickness$j", 'class'=>"span6 number zone_carbo_p50_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_dtc", array('id'=>"zone_carbo_p50_dtc$j", 'class'=>"span6 number zone_carbo_p50_dtc tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">usec/ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_dtc_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_total", array('id'=>"zone_carbo_p50_total$j", 'class'=>"span6 number zone_carbo_p50_total tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_net", array('id'=>"zone_carbo_p50_net$j", 'class'=>"span6 number zone_carbo_p50_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_por", array('id'=>"zone_carbo_p50_por$j", 'class'=>"span6 number zone_carbo_p50_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_satur", array('id'=>"zone_carbo_p50_satur$j", 'class'=>"span6 number zone_carbo_p50_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_thickness", array('id'=>"zone_carbo_p90_thickness$j", 'class'=>"span6 number zone_carbo_p90_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_dtc", array('id'=>"zone_carbo_p90_dtc$j", 'class'=>"span6 number zone_carbo_p90_dtc tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">usec/ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_dtc_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_total", array('id'=>"zone_carbo_p90_total$j", 'class'=>"span6 number zone_carbo_p90_total tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_net", array('id'=>"zone_carbo_p90_net$j", 'class'=>"span6 number zone_carbo_p90_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_por", array('id'=>"zone_carbo_p90_por$j", 'class'=>"span6 number zone_carbo_p90_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_satur", array('id'=>"zone_carbo_p90_satur$j", 'class'=>"span6 number zone_carbo_p90_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="z2_11" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_date", array('id'=>"z2_$j", 'class'=>"m-wrap medium popovers disable-input", 'style'=>"max-width:130px;", 'data-trigger'=>"hover", 'data-container'=>"body", 'data-content'=>"If Year that only available, please choose 1-January for Day and Month, if not leave it blank.", 'data-original-title'=>"Sample Date"));?>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <span id="clearz2_<?=$j;?>" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_date_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="sampleat" type="text" style="max-width:160px;"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_sample", array('id'=>"sampleat$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasoilratio_" type="text" style="max-width:135px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_ratio", array('id'=>"gasoilratio_$j", 'class'=>"number", 'style'=>"max-width:105px;"));?>
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_ratio_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="separator_pressure" type="text" style="max-width:153px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_pressure", array('id'=>"separator_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="separator_temperature" type="text" style="max-width:163px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_temp", array('id'=>"separator_temperature$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">&#176;C</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_temp_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="tubing_pressure" type="text" style="max-width:153px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_tubing", array('id'=>"tubing_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_tubing_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="casing_pressure" type="text" style="max-width:153px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_casing", array('id'=>"casing_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_casing_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="sampleby" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_by", array('id'=>"sampleby$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_by_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="repost_avail" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_report", array('id'=>"repost_avail$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_report_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="hydro_finger" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_finger", array('id'=>"hydro_finger$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_finger_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 &#176;F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilgrav_60" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_grv_oil", array('id'=>"oilgrav_60$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_grv_oil_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 &#176;F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasgrav_60" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_grv_gas", array('id'=>"gasgrav_60$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_grv_gas_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 &#176;F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="condensategrav_60" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_grv_conden", array('id'=>"condensategrav_60$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_grv_conden_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="pvt_" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_pvt", array('id'=>"pvt_$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_pvt_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="sample_" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_quantity", array('id'=>"sample_quantity$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_quantity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="gasdev_fac" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_z", array('id'=>"gasdev_fac$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_z_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>Max</th>
                                                                                                                    <th>Mean</th>
                                                                                                                    <th>Min</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_oil_p10", array('id'=>"zone_fvf_oil_p10$j", 'class'=>"span6 number zone_fvf_oil_p10 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RBO/STB</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_oil_p10_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_oil_p50", array('id'=>"zone_fvf_oil_p50$j", 'class'=>"span6 number zone_fvf_oil_p50 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RBO/STB</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_oil_p50_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_oil_p90", array('id'=>"zone_fvf_oil_p90$j", 'class'=>"span6 number zone_fvf_oil_p90 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RBO/STB</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_oil_p90_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_gas_p10", array('id'=>"zone_fvf_gas_p10$j", 'class'=>"span6 number zone_fvf_gas_p10 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RCF/SCF</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_gas_p10_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_gas_p50", array('id'=>"zone_fvf_gas_p50$j", 'class'=>"span6 number zone_fvf_gas_p50 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RCF/SCF</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_gas_p50_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_gas_p90", array('id'=>"zone_fvf_gas_p90$j", 'class'=>"span6 number zone_fvf_gas_p90 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RCF/SCF</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_gas_p90_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilviscocity" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_viscocity", array('id'=>"oilviscocity$j", 'class'=>"number", 'style'=>"max-width:130px; "));?>
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_viscocity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php $b = 0;?>
                                    											<?php }?>
                                                                                <!-- Zone 2 -->
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <!-- End Number Zone -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                                        
                                                    </div>
                                                </div>
                                                
</span>
					</div>
				</div>
			</div>
		</div>
		<div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
			<?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/updatewelldiscovery', array('id'=>$id)), array(
	        	'type'=>'POST',
	        	'dataType'=>'json',
	        	'beforeSend'=>'function(data) {
	        		$(".tooltips").attr("data-original-title", "");
	        		
	        		$(".has-err").removeClass("has-err");
	        		$(".errorMessage").hide();
	        	
                    $("yt0").prop("disabled", true);
	        		if($("#prospect_type").val() == "") {
	        			$("#Well_prospect_type_em_").text("Prospect Type cannot be blank.");                                                    
						$("#Well_prospect_type_em_").show();
		        		$("#Well_prospect_type_em_").parent().addClass("has-err");
	        			$("#pesan").hide();
	        			data.abort();
	        			
	        		}
	        	}',
	        	'success'=>'js:function(data) {
	        		
	        		
	        		if(data.result === "success") {
						$(".close").addClass("redirect");
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
						$(".redirect").click( function () {
							var redirect = "' . Yii::app()->createUrl('/Kkks/createwell') . '";
							window.location=redirect;
						});
                        $("yt0").prop("disabled", false);
	        		
	        		} else {
	        			var myArray = JSON.parse(data.err);
	        			$.each(myArray, function(key, val) {
	        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
			        		{
	        					$("#updatewellpostdrill-form #"+key+"_em_").parent().addClass("has-err");
	        					$("#updatewellpostdrill-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
	        					
			        		} else {
	        					$("#updatewellpostdrill-form #"+key+"_em_").text(val);                                                    
								$("#updatewellpostdrill-form #"+key+"_em_").show();
		        				$("#updatewellpostdrill-form #"+key+"_em_").parent().addClass("has-err");
	        				}
	        				
	        			});
	        		
	        			$("#message").html(data.msg);
	        			$("#popup").modal("show");
                        $("yt0").prop("disabled", false);
	        		}
	        	}',
	        ),
	        array('class'=>'btn btn-inverse')
	        );?>
	    </div>
        <?php $this->endWidget();?>
        
        <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->
	</div>
</div>
                                                
<?php 
	Yii::app()->clientScript->registerScript('formationname', "
				
		disabledElement('wz_');
// 		jQuery('.tooltips').tooltip();
// 		jQuery('.popovers').popover();
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('.formationname').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.formation,
				});
				getData();
			},
		});
		
		function getData() {
            $.ajax({
				url : '" . Yii::app()->createUrl('/Kkks/GetDataWellDiscovery', array('id'=>$id)) ."',
				type: 'POST',
				data: '',
				dataType: 'json',
				success : function(data) {
					$('#wl_id').val(data.wl_id);  
                        $('#well_name').val(data.wl_name);                      
                        $('#lat_degree').val(data.lat_degree);
                        $('#lat_minute').val(data.lat_minute);
                        $('#lat_second').val(data.lat_second);
                        $('#lat_direction').val(data.lat_direction); 
                        $('#long_degree').val(data.long_degree);
                        $('#long_minute').val(data.long_minute);
                        $('#long_second').val(data.long_second);
//                         $('#long_direction').val(data.long_direction);
						$('#s2id_well_result_discovery').select2('val', data.wl_result);
                        $('#s2id_well_type1').select2('val', data.wl_type);
                        $('#s2id_onoffshore').select2('val', data.wl_shore); 
                        $('#s2id_terrain').select2('val', data.wl_terrain);
                        $('#s2id_well_status').select2('val', data.wl_status);
                        $('#s2id_targetedformationname').select2('val', data.wl_formation);
                        $('#dsw8_').val(data.wl_date_complete);
                        $('#ttd_1').val(data.wl_target_depth_tvd);
                        $('#ttd_2').val(data.wl_target_depth_md);
                        $('#actual_total_depth').val(data.wl_actual_depth); 
                        $('#tpprt_').val(data.wl_target_play);
                        $('#apprt_').val(data.wl_actual_play);                            		
                        $('#s2id_num_mdt').select2('val', data.wl_number_mdt);
                        $('#s2id_num_rft').select2('val', data.wl_number_rft);
                        $('#rip_').val(data.wl_res_pressure);
                        $('#lpr_').val(data.wl_last_pressure);
                        $('#pressure_gradient').val(data.wl_pressure_gradient);
                        $('#lrt_').val(data.wl_last_temp);       
                        $('#s2id_actual_well_integrity').select2('val', data.wl_integrity);
                        $('#s2id_availability_electrolog').select2('val', data.wl_electro_avail);
                        $('#wl_electro_list1').val(data.wl_electro_list1);
                        $('#wl_electro_list2').val(data.wl_electro_list2);
                        $('#wl_electro_list3').val(data.wl_electro_list3);
                        $('#wl_electro_list4').val(data.wl_electro_list4); 
                        $('#wl_electro_list5').val(data.wl_electro_list5);
                        $('#wl_electro_list6').val(data.wl_electro_list6);
                        $('#wl_electro_list7').val(data.wl_electro_list7);
                        $('#wl_electro_list8').val(data.wl_electro_list8);
                        $('#wl_electro_list9').val(data.wl_electro_list9);
                        $('#wl_electro_list10').val(data.wl_electro_list10);
                        $('#wl_electro_list11').val(data.wl_electro_list11);
                        $('#wl_electro_list12').val(data.wl_electro_list12); 
                        $('#wl_electro_list13').val(data.wl_electro_list13);
                        $('#wl_electro_list14').val(data.wl_electro_list14);
                        $('#wl_electro_list15').val(data.wl_electro_list15);
                        $('#wl_electro_list16').val(data.wl_electro_list16);
						        		
						if(data.total_wellzone == null) {
							var total = [{id:1,text:'1'},{id:2,text:'2'},{id:3,text:'3'},{id:4,text:'4'},{id:5,text:'5'}];
							$('#wz_').select2({
								placeholder: 'Pilih',
								allowClear: true,
								data : total,
							});
							$('#wz_').select2('val', data.total_wellzone);
							disabledElement('wz_');
						} else {
							switch(data.total_wellzone) {
								case 1 :
									var total = [{id:1,text:'1'},{id:2,text:'2'},{id:3,text:'3'},{id:4,text:'4'},{id:5,text:'5'}];
									$('#wz_').select2({
										placeholder: 'Pilih',
										data : total,
									});
									$('#wz_').select2('val', data.total_wellzone);
									disabledElement('wz_');
									break;
								case 2 :
									var total = [{id:2,text:'2'},{id:3,text:'3'},{id:4,text:'4'},{id:5,text:'5'}];
									$('#wz_').select2({
										placeholder: 'Pilih',
										data : total,
									});
									$('#wz_').select2('val', data.total_wellzone);
									disabledElement('wz_');
									break;
								case 3 :
                					console.log(data.total_wellzone);
									var total = [{id:3,text:'3'},{id:4,text:'4'},{id:5,text:'5'}];
									$('#wz_').select2({
										placeholder: 'Pilih',
										data : total,
									});
									$('#wz_').select2('val', data.total_wellzone);
									disabledElement('wz_');
									break;
								case 4 :
									var total = [{id:4,text:'4'},{id:5,text:'5'}];
									$('#wz_').select2({
										placeholder: 'Pilih',
										data : total,
									});
									$('#wz_').select2('val', data.total_wellzone);
									disabledElement('wz_');
									break;
								case 5 :
									var total = [{id:5,text:'5'}];
									$('#wz_').select2({
										placeholder: 'Pilih',
										data : total,
									});
									$('#wz_').select2('val', data.total_wellzone);
									disabledElement('wz_');
									break;
							}
						}
                                                    		
						$('#zone_id1').val(data.zone_id1);
                        $('#zonename1').val(data.zone_name1);
                        $('#s2id_welltest1').select2('val', data.zone_test1);
                        $('#z_1').val(data.zone_test_date1);
                        $('#s2id_zoneresult1').select2('val', data.zone_result1);
                        $('#zonearea1').val(data.zone_area1);
                        $('#zonethickness1').val(data.zone_thickness1);
                        $('#zoneinterval1').val(data.zone_depth1);
                        $('#perforation_interval1').val(data.zone_perforation1);
                        $('#s2id_welltest_type1').select2('val', data.zone_well_test1);
                        $('#welltest_total1').val(data.zone_well_duration1);
                        $('#initialflow1').val(data.zone_initial_flow1);
                        $('#initialshutin1').val(data.zone_initial_shutin1);
                        $('#tubingsize1').val(data.zone_tubing_size1);
                        $('#initialtemperature1').val(data.zone_initial_temp1);
                        $('#zone_initial_pressure1').val(data.zone_initial_pressure1);
                        $('#res_presure1').val(data.zone_pseudostate1);
                        $('#pressurewell_for1').val(data.zone_well_formation1);
                        $('#pressurewell_head1').val(data.zone_head1);
                        $('#res_pressure1').val(data.zone_pressure_wellbore1);
                        $('#avg_porpsity1').val(data.zone_average_por1);
                        $('#watercut1').val(data.zone_water_cut1);
                        $('#initialwater_sat1').val(data.zone_initial_water1);
                        $('#lowtest_gas1').val(data.zone_low_gas1);
                        $('#lowtest_oil1').val(data.zone_low_oil1);
                        $('#freewater1').val(data.zone_free_water1);
                        $('#gas_grav1').val(data.zone_grv_gas1);
                        $('#oil_grav1').val(data.zone_grv_grv_oil1);
                        $('#zone_wellbore_coefficient1').val(data.zone_wellbore_coefficient1);
                        $('#wellbore_time1').val(data.zone_wellbore_time1);
                        $('#rsbt_1').val(data.zone_res_shape1);
                        $('#oilchoke1').val(data.zone_prod_oil_choke1);
                        $('#oilflow1').val(data.zone_prod_oil_flow1);
                        $('#gaschoke1').val(data.zone_prod_gas_choke1);
                        $('#gasflow1').val(data.zone_prod_gas_flow1);
                        $('#gasoilratio1').val(data.zone_prod_gasoil_ratio1);
                        $('#zone_prod_conden_ratio1').val(data.zone_prod_conden_ratio1);
                        $('#cummulative_pro_gas1').val(data.zone_prod_cumm_gas1);
                        $('#cummulative_pro_oil1').val(data.zone_prod_cumm_oil1);
                        $('#ab_openflow11').val(data.zone_prod_aof_bbl1);
                        $('#ab_openflow21').val(data.zone_prod_aof_scf1);
                        $('#criticalrate11').val(data.zone_prod_critical_bbl1);
                        $('#criticalrate21').val(data.zone_prod_critical_scf1);
                        $('#pro_index11').val(data.zone_prod_index_bbl1);
                        $('#pro_index21').val(data.zone_prod_index_scf1);
                        $('#diffusity_fact1').val(data.zone_prod_diffusity1);
                        $('#permeability1').val(data.zone_prod_permeability1);
                        $('#iafit_1').val(data.zone_prod_infinite1);
                        $('#wellradius1').val(data.zone_prod_well_radius1);
                        $('#pfit_1').val(data.zone_prod_pseudostate1);
                        $('#res_bondradius1').val(data.zone_prod_res_radius1);
                        $('#deltaP1').val(data.zone_prod_delta1);
                        $('#wellbore_skin1').val(data.zone_prod_wellbore1);
                        $('#rock_compress1').val(data.zone_prod_compres_rock1);
                        $('#fluid_compress1').val(data.zone_prod_compres_fluid1);
                        $('#total_compress1').val(data.zone_prod_compres_total1);
                        $('#secd_porosity1').val(data.zone_prod_second1);
                        $('#I_1').val(data.zone_prod_lambda1);
                        $('#W_1').val(data.zone_prod_omega1);
                        $('#oilshow_read11').select2('val', data.zone_hc_oil_show1);
                        $('#oilshow_read21').val(data.zone_hc_oil_show_tools1);
                        $('#gasshow_read11').select2('val', data.zone_hc_gas_show1);
                        $('#gasshow_read21').val(data.zone_hc_gas_show_tools1);
                        $('#wellmak_watercut1').val(data.zone_hc_water_cut1);
                        $('#water_bearing11').val(data.zone_hc_water_gwc1);
                        $('#water_bearing21').val(data.zone_hc_water_gwd_tools1);
                        $('#water_bearing31').val(data.zone_hc_water_owc1);
                        $('#water_bearing41').val(data.zone_hc_water_owc_tools1);
                        $('#s2id_rock_sampling1').select2('val', data.zone_rock_method1);
                        $('#s2id_petro_analys1').select2('val', data.zone_rock_petro1);
                        $('#sample1').val(data.zone_rock_petro_sample1);
                        $('#ticsd_1').val(data.zone_rock_top_depth1);
                        $('#bicsd_1').val(data.zone_rock_bot_depth1);
                        $('#num_totalcare1').val(data.zone_rock_barrel1);
                        $('#corebarrel1').val(data.zone_rock_barrel_equal1);
                        $('#totalrec1').val(data.zone_rock_total_core1);
                        $('#precore1').val(data.zone_rock_preservative1);
                        $('#rca_11').val(data.zone_rock_routine1);
                        $('#rca_1').val(data.zone_rock_routine_sample1);
                        $('#scal_11').val(data.zone_rock_scal1);
                        $('#scal_21').val(data.zone_rock_scal_sample1);
                        $('#zone_clastic_p10_thickness1').val(data.zone_clastic_p10_thickness1);
                        $('#zone_clastic_p50_thickness1').val(data.zone_clastic_p50_thickness1);
                        $('#zone_clastic_p90_thickness1').val(data.zone_clastic_p90_thickness1);
                        $('#zone_clastic_p10_gr1').val(data.zone_clastic_p10_gr1);
                        $('#zone_clastic_p50_gr1').val(data.zone_clastic_p50_gr1);
                        $('#zone_clastic_p90_gr1').val(data.zone_clastic_p90_gr1);
                        $('#zone_clastic_p10_sp1').val(data.zone_clastic_p10_sp1);
                        $('#zone_clastic_p50_sp1').val(data.zone_clastic_p50_sp1);
                        $('#zone_clastic_p90_sp1').val(data.zone_clastic_p90_sp1);
                        $('#zone_clastic_p10_net1').val(data.zone_clastic_p10_net1);
                        $('#zone_clastic_p50_net1').val(data.zone_clastic_p50_net1);
                        $('#zone_clastic_p90_net1').val(data.zone_clastic_p90_net1);
                        $('#zone_clastic_p10_por1').val(data.zone_clastic_p10_por1);
                        $('#zone_clastic_p50_por1').val(data.zone_clastic_p50_por1);
                        $('#zone_clastic_p90_por1').val(data.zone_clastic_p90_por1);
                        $('#zone_clastic_p10_satur1').val(data.zone_clastic_p10_satur1);
                        $('#zone_clastic_p50_satur1').val(data.zone_clastic_p50_satur1);
                        $('#zone_clastic_p90_satur1').val(data.zone_clastic_p90_satur1);
                        $('#zone_carbo_p10_thickness1').val(data.zone_carbo_p10_thickness1);
                        $('#zone_carbo_p50_thickness1').val(data.zone_carbo_p50_thickness1);
                        $('#zone_carbo_p90_thickness1').val(data.zone_carbo_p90_thickness1);
                        $('#zone_carbo_p10_dtc1').val(data.zone_carbo_p10_dtc1);
                        $('#zone_carbo_p50_dtc1').val(data.zone_carbo_p50_dtc1);
                        $('#zone_carbo_p90_dtc1').val(data.zone_carbo_p90_dtc1);
                        $('#zone_carbo_p10_total1').val(data.zone_carbo_p10_total1);
                        $('#zone_carbo_p50_total1').val(data.zone_carbo_p50_total1);
                        $('#zone_carbo_p90_total1').val(data.zone_carbo_p90_total1);
                        $('#zone_carbo_p10_net1').val(data.zone_carbo_p10_net1);
                        $('#zone_carbo_p50_net1').val(data.zone_carbo_p50_net1);
                        $('#zone_carbo_p90_net1').val(data.zone_carbo_p90_net1);
                        $('#zone_carbo_p10_por1').val(data.zone_carbo_p10_por1);
                        $('#zone_carbo_p50_por1').val(data.zone_carbo_p50_por1);
                        $('#zone_carbo_p90_por1').val(data.zone_carbo_p90_por1);
                        $('#zone_carbo_p10_satur1').val(data.zone_carbo_p10_satur1);
                        $('#zone_carbo_p50_satur1').val(data.zone_carbo_p50_satur1);
                        $('#zone_carbo_p90_satur1').val(data.zone_carbo_p90_satur1);
                        $('#z2_1').val(data.zone_fluid_date1);
                        $('#sampleat1').val(data.zone_fluid_sample1);
                        $('#gasoilratio_1').val(data.zone_fluid_ratio1);
                        $('#separator_pressure1').val(data.zone_fluid_pressure1);
                        $('#separator_temperature1').val(data.zone_fluid_temp1);
                        $('#tubing_pressure1').val(data.zone_fluid_tubing1);
                        $('#casing_pressure1').val(data.zone_fluid_casing1);
                        $('#sampleby1').val(data.zone_fluid_by1);
                        $('#repost_avail1').val(data.zone_fluid_report1);
                        $('#hydro_finger1').val(data.zone_fluid_finger1);
                        $('#oilgrav_601').val(data.zone_fluid_grv_oil1);
                        $('#gasgrav_601').val(data.zone_fluid_grv_gas1);
                        $('#condensategrav_601').val(data.zone_fluid_grv_conden1);
                        $('#pvt_1').val(data.zone_fluid_pvt1);
                        $('#sample_1').val(data.zone_fluid_quantity1);
                        $('#gasdev_fac1').val(data.zone_fluid_z1);
                        $('#zone_fvf_oil_p101').val(data.zone_fvf_oil_p101);
                        $('#zone_fvf_oil_p501').val(data.zone_fvf_oil_p501);
                        $('#zone_fvf_oil_p901').val(data.zone_fvf_oil_p901);
                        $('#zone_fvf_gas_p101').val(data.zone_fvf_gas_p101);
                        $('#zone_fvf_gas_p501').val(data.zone_fvf_gas_p501);
                        $('#zone_fvf_gas_p901').val(data.zone_fvf_gas_p901);
                        $('#oilviscocity1').val(data.zone_viscocity1);
                                                    		
						$('#zone_id2').val(data.zone_id2);
                        $('#zonename2').val(data.zone_name2);
                        $('#s2id_welltest2').select2('val', data.zone_test2);
                        $('#z_2').val(data.zone_test_date2);
                        $('#s2id_zoneresult2').select2('val', data.zone_result2);
                        $('#zonearea2').val(data.zone_area2);
                        $('#zonethickness2').val(data.zone_thickness2);
                        $('#zoneinterval2').val(data.zone_depth2);
                        $('#perforation_interval2').val(data.zone_perforation2);
                        $('#s2id_welltest_type2').select2('val', data.zone_well_test2);
                        $('#welltest_total2').val(data.zone_well_duration2);
                        $('#initialflow2').val(data.zone_initial_flow2);
                        $('#initialshutin2').val(data.zone_initial_shutin2);
                        $('#tubingsize2').val(data.zone_tubing_size2);
                        $('#initialtemperature2').val(data.zone_initial_temp2);
                        $('#zone_initial_pressure2').val(data.zone_initial_pressure2);
                        $('#res_presure2').val(data.zone_pseudostate2);
                        $('#pressurewell_for2').val(data.zone_well_formation2);
                        $('#pressurewell_head2').val(data.zone_head2);
                        $('#res_pressure2').val(data.zone_pressure_wellbore2);
                        $('#avg_porpsity2').val(data.zone_average_por2);
                        $('#watercut2').val(data.zone_water_cut2);
                        $('#initialwater_sat2').val(data.zone_initial_water2);
                        $('#lowtest_gas2').val(data.zone_low_gas2);
                        $('#lowtest_oil2').val(data.zone_low_oil2);
                        $('#freewater2').val(data.zone_free_water2);
                        $('#gas_grav2').val(data.zone_grv_gas2);
                        $('#oil_grav2').val(data.zone_grv_grv_oil2);
                        $('#zone_wellbore_coefficient2').val(data.zone_wellbore_coefficient2);
                        $('#wellbore_time2').val(data.zone_wellbore_time2);
                        $('#rsbt_2').val(data.zone_res_shape2);
                        $('#oilchoke2').val(data.zone_prod_oil_choke2);
                        $('#oilflow2').val(data.zone_prod_oil_flow2);
                        $('#gaschoke2').val(data.zone_prod_gas_choke2);
                        $('#gasflow2').val(data.zone_prod_gas_flow2);
                        $('#gasoilratio2').val(data.zone_prod_gasoil_ratio2);
                        $('#zone_prod_conden_ratio2').val(data.zone_prod_conden_ratio2);
                        $('#cummulative_pro_gas2').val(data.zone_prod_cumm_gas2);
                        $('#cummulative_pro_oil2').val(data.zone_prod_cumm_oil2);
                        $('#ab_openflow12').val(data.zone_prod_aof_bbl2);
                        $('#ab_openflow22').val(data.zone_prod_aof_scf2);
                        $('#criticalrate12').val(data.zone_prod_critical_bbl2);
                        $('#criticalrate22').val(data.zone_prod_critical_scf2);
                        $('#pro_index12').val(data.zone_prod_index_bbl2);
                        $('#pro_index22').val(data.zone_prod_index_scf2);
                        $('#diffusity_fact2').val(data.zone_prod_diffusity2);
                        $('#permeability2').val(data.zone_prod_permeability2);
                        $('#iafit_2').val(data.zone_prod_infinite2);
                        $('#wellradius2').val(data.zone_prod_well_radius2);
                        $('#pfit_2').val(data.zone_prod_pseudostate2);
                        $('#res_bondradius2').val(data.zone_prod_res_radius2);
                        $('#deltaP2').val(data.zone_prod_delta2);
                        $('#wellbore_skin2').val(data.zone_prod_wellbore2);
                        $('#rock_compress2').val(data.zone_prod_compres_rock2);
                        $('#fluid_compress2').val(data.zone_prod_compres_fluid2);
                        $('#total_compress2').val(data.zone_prod_compres_total2);
                        $('#secd_porosity2').val(data.zone_prod_second2);
                        $('#I_2').val(data.zone_prod_lambda2);
                        $('#W_2').val(data.zone_prod_omega2);
                        $('#oilshow_read12').select2('val', data.zone_hc_oil_show2);
                        $('#oilshow_read22').val(data.zone_hc_oil_show_tools2);
                        $('#gasshow_read12').select2('val', data.zone_hc_gas_show2);
                        $('#gasshow_read22').val(data.zone_hc_gas_show_tools2);
                        $('#wellmak_watercut2').val(data.zone_hc_water_cut2);
                        $('#water_bearing12').val(data.zone_hc_water_gwc2);
                        $('#water_bearing22').val(data.zone_hc_water_gwd_tools2);
                        $('#water_bearing32').val(data.zone_hc_water_owc2);
                        $('#water_bearing42').val(data.zone_hc_water_owc_tools2);
                        $('#s2id_rock_sampling2').select2('val', data.zone_rock_method2);
                        $('#s2id_petro_analys2').select2('val', data.zone_rock_petro2);
                        $('#sample2').val(data.zone_rock_petro_sample2);
                        $('#ticsd_2').val(data.zone_rock_top_depth2);
                        $('#bicsd_2').val(data.zone_rock_bot_depth2);
                        $('#num_totalcare2').val(data.zone_rock_barrel2);
                        $('#corebarrel2').val(data.zone_rock_barrel_equal2);
                        $('#totalrec2').val(data.zone_rock_total_core2);
                        $('#precore2').val(data.zone_rock_preservative2);
                        $('#rca_12').val(data.zone_rock_routine2);
                        $('#rca_2').val(data.zone_rock_routine_sample2);
                        $('#scal_12').val(data.zone_rock_scal2);
                        $('#scal_22').val(data.zone_rock_scal_sample2);
                        $('#zone_clastic_p10_thickness2').val(data.zone_clastic_p10_thickness2);
                        $('#zone_clastic_p50_thickness2').val(data.zone_clastic_p50_thickness2);
                        $('#zone_clastic_p90_thickness2').val(data.zone_clastic_p90_thickness2);
                        $('#zone_clastic_p10_gr2').val(data.zone_clastic_p10_gr2);
                        $('#zone_clastic_p50_gr2').val(data.zone_clastic_p50_gr2);
                        $('#zone_clastic_p90_gr2').val(data.zone_clastic_p90_gr2);
                        $('#zone_clastic_p10_sp2').val(data.zone_clastic_p10_sp2);
                        $('#zone_clastic_p50_sp2').val(data.zone_clastic_p50_sp2);
                        $('#zone_clastic_p90_sp2').val(data.zone_clastic_p90_sp2);
                        $('#zone_clastic_p10_net2').val(data.zone_clastic_p10_net2);
                        $('#zone_clastic_p50_net2').val(data.zone_clastic_p50_net2);
                        $('#zone_clastic_p90_net2').val(data.zone_clastic_p90_net2);
                        $('#zone_clastic_p10_por2').val(data.zone_clastic_p10_por2);
                        $('#zone_clastic_p50_por2').val(data.zone_clastic_p50_por2);
                        $('#zone_clastic_p90_por2').val(data.zone_clastic_p90_por2);
                        $('#zone_clastic_p10_satur2').val(data.zone_clastic_p10_satur2);
                        $('#zone_clastic_p50_satur2').val(data.zone_clastic_p50_satur2);
                        $('#zone_clastic_p90_satur2').val(data.zone_clastic_p90_satur2);
                        $('#zone_carbo_p10_thickness2').val(data.zone_carbo_p10_thickness2);
                        $('#zone_carbo_p50_thickness2').val(data.zone_carbo_p50_thickness2);
                        $('#zone_carbo_p90_thickness2').val(data.zone_carbo_p90_thickness2);
                        $('#zone_carbo_p10_dtc2').val(data.zone_carbo_p10_dtc2);
                        $('#zone_carbo_p50_dtc2').val(data.zone_carbo_p50_dtc2);
                        $('#zone_carbo_p90_dtc2').val(data.zone_carbo_p90_dtc2);
                        $('#zone_carbo_p10_total2').val(data.zone_carbo_p10_total2);
                        $('#zone_carbo_p50_total2').val(data.zone_carbo_p50_total2);
                        $('#zone_carbo_p90_total2').val(data.zone_carbo_p90_total2);
                        $('#zone_carbo_p10_net2').val(data.zone_carbo_p10_net2);
                        $('#zone_carbo_p50_net2').val(data.zone_carbo_p50_net2);
                        $('#zone_carbo_p90_net2').val(data.zone_carbo_p90_net2);
                        $('#zone_carbo_p10_por2').val(data.zone_carbo_p10_por2);
                        $('#zone_carbo_p50_por2').val(data.zone_carbo_p50_por2);
                        $('#zone_carbo_p90_por2').val(data.zone_carbo_p90_por2);
                        $('#zone_carbo_p10_satur2').val(data.zone_carbo_p10_satur2);
                        $('#zone_carbo_p50_satur2').val(data.zone_carbo_p50_satur2);
                        $('#zone_carbo_p90_satur2').val(data.zone_carbo_p90_satur2);
                        $('#z2_2').val(data.zone_fluid_date2);
                        $('#sampleat2').val(data.zone_fluid_sample2);
                        $('#gasoilratio_2').val(data.zone_fluid_ratio2);
                        $('#separator_pressure2').val(data.zone_fluid_pressure2);
                        $('#separator_temperature2').val(data.zone_fluid_temp2);
                        $('#tubing_pressure2').val(data.zone_fluid_tubing2);
                        $('#casing_pressure2').val(data.zone_fluid_casing2);
                        $('#sampleby2').val(data.zone_fluid_by2);
                        $('#repost_avail2').val(data.zone_fluid_report2);
                        $('#hydro_finger2').val(data.zone_fluid_finger2);
                        $('#oilgrav_602').val(data.zone_fluid_grv_oil2);
                        $('#gasgrav_602').val(data.zone_fluid_grv_gas2);
                        $('#condensategrav_602').val(data.zone_fluid_grv_conden2);
                        $('#pvt_2').val(data.zone_fluid_pvt2);
                        $('#sample_2').val(data.zone_fluid_quantity2);
                        $('#gasdev_fac2').val(data.zone_fluid_z2);
                        $('#zone_fvf_oil_p102').val(data.zone_fvf_oil_p102);
                        $('#zone_fvf_oil_p502').val(data.zone_fvf_oil_p502);
                        $('#zone_fvf_oil_p902').val(data.zone_fvf_oil_p902);
                        $('#zone_fvf_gas_p102').val(data.zone_fvf_gas_p102);
                        $('#zone_fvf_gas_p502').val(data.zone_fvf_gas_p502);
                        $('#zone_fvf_gas_p902').val(data.zone_fvf_gas_p902);
                        $('#oilviscocity2').val(data.zone_viscocity2);
                                                    		
						$('#zone_id3').val(data.zone_id3);
                        $('#zonename3').val(data.zone_name3);
                        $('#s2id_welltest3').select2('val', data.zone_test3);
                        $('#z_3').val(data.zone_test_date3);
                        $('#s2id_zoneresult3').select2('val', data.zone_result3);
                        $('#zonearea3').val(data.zone_area3);
                        $('#zonethickness3').val(data.zone_thickness3);
                        $('#zoneinterval3').val(data.zone_depth3);
                        $('#perforation_interval3').val(data.zone_perforation3);
                        $('#s2id_welltest_type3').select2('val', data.zone_well_test3);
                        $('#welltest_total3').val(data.zone_well_duration3);
                        $('#initialflow3').val(data.zone_initial_flow3);
                        $('#initialshutin3').val(data.zone_initial_shutin3);
                        $('#tubingsize3').val(data.zone_tubing_size3);
                        $('#initialtemperature3').val(data.zone_initial_temp3);
                        $('#zone_initial_pressure3').val(data.zone_initial_pressure3);
                        $('#res_presure3').val(data.zone_pseudostate3);
                        $('#pressurewell_for3').val(data.zone_well_formation3);
                        $('#pressurewell_head3').val(data.zone_head3);
                        $('#res_pressure3').val(data.zone_pressure_wellbore3);
                        $('#avg_porpsity3').val(data.zone_average_por3);
                        $('#watercut3').val(data.zone_water_cut3);
                        $('#initialwater_sat3').val(data.zone_initial_water3);
                        $('#lowtest_gas3').val(data.zone_low_gas3);
                        $('#lowtest_oil3').val(data.zone_low_oil3);
                        $('#freewater3').val(data.zone_free_water3);
                        $('#gas_grav3').val(data.zone_grv_gas3);
                        $('#oil_grav3').val(data.zone_grv_grv_oil3);
                        $('#zone_wellbore_coefficient3').val(data.zone_wellbore_coefficient3);
                        $('#wellbore_time3').val(data.zone_wellbore_time3);
                        $('#rsbt_3').val(data.zone_res_shape3);
                        $('#oilchoke3').val(data.zone_prod_oil_choke3);
                        $('#oilflow3').val(data.zone_prod_oil_flow3);
                        $('#gaschoke3').val(data.zone_prod_gas_choke3);
                        $('#gasflow3').val(data.zone_prod_gas_flow3);
                        $('#gasoilratio3').val(data.zone_prod_gasoil_ratio3);
                        $('#zone_prod_conden_ratio3').val(data.zone_prod_conden_ratio3);
                        $('#cummulative_pro_gas3').val(data.zone_prod_cumm_gas3);
                        $('#cummulative_pro_oil3').val(data.zone_prod_cumm_oil3);
                        $('#ab_openflow13').val(data.zone_prod_aof_bbl3);
                        $('#ab_openflow23').val(data.zone_prod_aof_scf3);
                        $('#criticalrate13').val(data.zone_prod_critical_bbl3);
                        $('#criticalrate23').val(data.zone_prod_critical_scf3);
                        $('#pro_index13').val(data.zone_prod_index_bbl3);
                        $('#pro_index23').val(data.zone_prod_index_scf3);
                        $('#diffusity_fact3').val(data.zone_prod_diffusity3);
                        $('#permeability3').val(data.zone_prod_permeability3);
                        $('#iafit_3').val(data.zone_prod_infinite3);
                        $('#wellradius3').val(data.zone_prod_well_radius3);
                        $('#pfit_3').val(data.zone_prod_pseudostate3);
                        $('#res_bondradius3').val(data.zone_prod_res_radius3);
                        $('#deltaP3').val(data.zone_prod_delta3);
                        $('#wellbore_skin3').val(data.zone_prod_wellbore3);
                        $('#rock_compress3').val(data.zone_prod_compres_rock3);
                        $('#fluid_compress3').val(data.zone_prod_compres_fluid3);
                        $('#total_compress3').val(data.zone_prod_compres_total3);
                        $('#secd_porosity3').val(data.zone_prod_second3);
                        $('#I_3').val(data.zone_prod_lambda3);
                        $('#W_3').val(data.zone_prod_omega3);
                        $('#oilshow_read13').select2('val', data.zone_hc_oil_show3);
                        $('#oilshow_read23').val(data.zone_hc_oil_show_tools3);
                        $('#gasshow_read13').select2('val', data.zone_hc_gas_show3);
                        $('#gasshow_read23').val(data.zone_hc_gas_show_tools3);
                        $('#wellmak_watercut3').val(data.zone_hc_water_cut3);
                        $('#water_bearing13').val(data.zone_hc_water_gwc3);
                        $('#water_bearing23').val(data.zone_hc_water_gwd_tools3);
                        $('#water_bearing33').val(data.zone_hc_water_owc3);
                        $('#water_bearing43').val(data.zone_hc_water_owc_tools3);
                        $('#s2id_rock_sampling3').select2('val', data.zone_rock_method3);
                        $('#s2id_petro_analys3').select2('val', data.zone_rock_petro3);
                        $('#sample3').val(data.zone_rock_petro_sample3);
                        $('#ticsd_3').val(data.zone_rock_top_depth3);
                        $('#bicsd_3').val(data.zone_rock_bot_depth3);
                        $('#num_totalcare3').val(data.zone_rock_barrel3);
                        $('#corebarrel3').val(data.zone_rock_barrel_equal3);
                        $('#totalrec3').val(data.zone_rock_total_core3);
                        $('#precore3').val(data.zone_rock_preservative3);
                        $('#rca_13').val(data.zone_rock_routine3);
                        $('#rca_3').val(data.zone_rock_routine_sample3);
                        $('#scal_13').val(data.zone_rock_scal3);
                        $('#scal_23').val(data.zone_rock_scal_sample3);
                        $('#zone_clastic_p10_thickness3').val(data.zone_clastic_p10_thickness3);
                        $('#zone_clastic_p50_thickness3').val(data.zone_clastic_p50_thickness3);
                        $('#zone_clastic_p90_thickness3').val(data.zone_clastic_p90_thickness3);
                        $('#zone_clastic_p10_gr3').val(data.zone_clastic_p10_gr3);
                        $('#zone_clastic_p50_gr3').val(data.zone_clastic_p50_gr3);
                        $('#zone_clastic_p90_gr3').val(data.zone_clastic_p90_gr3);
                        $('#zone_clastic_p10_sp3').val(data.zone_clastic_p10_sp3);
                        $('#zone_clastic_p50_sp3').val(data.zone_clastic_p50_sp3);
                        $('#zone_clastic_p90_sp3').val(data.zone_clastic_p90_sp3);
                        $('#zone_clastic_p10_net3').val(data.zone_clastic_p10_net3);
                        $('#zone_clastic_p50_net3').val(data.zone_clastic_p50_net3);
                        $('#zone_clastic_p90_net3').val(data.zone_clastic_p90_net3);
                        $('#zone_clastic_p10_por3').val(data.zone_clastic_p10_por3);
                        $('#zone_clastic_p50_por3').val(data.zone_clastic_p50_por3);
                        $('#zone_clastic_p90_por3').val(data.zone_clastic_p90_por3);
                        $('#zone_clastic_p10_satur3').val(data.zone_clastic_p10_satur3);
                        $('#zone_clastic_p50_satur3').val(data.zone_clastic_p50_satur3);
                        $('#zone_clastic_p90_satur3').val(data.zone_clastic_p90_satur3);
                        $('#zone_carbo_p10_thickness3').val(data.zone_carbo_p10_thickness3);
                        $('#zone_carbo_p50_thickness3').val(data.zone_carbo_p50_thickness3);
                        $('#zone_carbo_p90_thickness3').val(data.zone_carbo_p90_thickness3);
                        $('#zone_carbo_p10_dtc3').val(data.zone_carbo_p10_dtc3);
                        $('#zone_carbo_p50_dtc3').val(data.zone_carbo_p50_dtc3);
                        $('#zone_carbo_p90_dtc3').val(data.zone_carbo_p90_dtc3);
                        $('#zone_carbo_p10_total3').val(data.zone_carbo_p10_total3);
                        $('#zone_carbo_p50_total3').val(data.zone_carbo_p50_total3);
                        $('#zone_carbo_p90_total3').val(data.zone_carbo_p90_total3);
                        $('#zone_carbo_p10_net3').val(data.zone_carbo_p10_net3);
                        $('#zone_carbo_p50_net3').val(data.zone_carbo_p50_net3);
                        $('#zone_carbo_p90_net3').val(data.zone_carbo_p90_net3);
                        $('#zone_carbo_p10_por3').val(data.zone_carbo_p10_por3);
                        $('#zone_carbo_p50_por3').val(data.zone_carbo_p50_por3);
                        $('#zone_carbo_p90_por3').val(data.zone_carbo_p90_por3);
                        $('#zone_carbo_p10_satur3').val(data.zone_carbo_p10_satur3);
                        $('#zone_carbo_p50_satur3').val(data.zone_carbo_p50_satur3);
                        $('#zone_carbo_p90_satur3').val(data.zone_carbo_p90_satur3);
                        $('#z2_3').val(data.zone_fluid_date3);
                        $('#sampleat3').val(data.zone_fluid_sample3);
                        $('#gasoilratio_3').val(data.zone_fluid_ratio3);
                        $('#separator_pressure3').val(data.zone_fluid_pressure3);
                        $('#separator_temperature3').val(data.zone_fluid_temp3);
                        $('#tubing_pressure3').val(data.zone_fluid_tubing3);
                        $('#casing_pressure3').val(data.zone_fluid_casing3);
                        $('#sampleby3').val(data.zone_fluid_by3);
                        $('#repost_avail3').val(data.zone_fluid_report3);
                        $('#hydro_finger3').val(data.zone_fluid_finger3);
                        $('#oilgrav_603').val(data.zone_fluid_grv_oil3);
                        $('#gasgrav_603').val(data.zone_fluid_grv_gas3);
                        $('#condensategrav_603').val(data.zone_fluid_grv_conden3);
                        $('#pvt_3').val(data.zone_fluid_pvt3);
                        $('#sample_3').val(data.zone_fluid_quantity3);
                        $('#gasdev_fac3').val(data.zone_fluid_z3);
                        $('#zone_fvf_oil_p103').val(data.zone_fvf_oil_p103);
                        $('#zone_fvf_oil_p503').val(data.zone_fvf_oil_p503);
                        $('#zone_fvf_oil_p903').val(data.zone_fvf_oil_p903);
                        $('#zone_fvf_gas_p103').val(data.zone_fvf_gas_p103);
                        $('#zone_fvf_gas_p503').val(data.zone_fvf_gas_p503);
                        $('#zone_fvf_gas_p903').val(data.zone_fvf_gas_p903);
                        $('#oilviscocity3').val(data.zone_viscocity3);
                                                    		
						$('#zone_id4').val(data.zone_id4);
                        $('#zonename4').val(data.zone_name4);
                        $('#s2id_welltest4').select2('val', data.zone_test4);
                        $('#z_4').val(data.zone_test_date4);
                        $('#s2id_zoneresult4').select2('val', data.zone_result4);
                        $('#zonearea4').val(data.zone_area4);
                        $('#zonethickness4').val(data.zone_thickness4);
                        $('#zoneinterval4').val(data.zone_depth4);
                        $('#perforation_interval4').val(data.zone_perforation4);
                        $('#s2id_welltest_type4').select2('val', data.zone_well_test4);
                        $('#welltest_total4').val(data.zone_well_duration4);
                        $('#initialflow4').val(data.zone_initial_flow4);
                        $('#initialshutin4').val(data.zone_initial_shutin4);
                        $('#tubingsize4').val(data.zone_tubing_size4);
                        $('#initialtemperature4').val(data.zone_initial_temp4);
                        $('#zone_initial_pressure4').val(data.zone_initial_pressure4);
                        $('#res_presure4').val(data.zone_pseudostate4);
                        $('#pressurewell_for4').val(data.zone_well_formation4);
                        $('#pressurewell_head4').val(data.zone_head4);
                        $('#res_pressure4').val(data.zone_pressure_wellbore4);
                        $('#avg_porpsity4').val(data.zone_average_por4);
                        $('#watercut4').val(data.zone_water_cut4);
                        $('#initialwater_sat4').val(data.zone_initial_water4);
                        $('#lowtest_gas4').val(data.zone_low_gas4);
                        $('#lowtest_oil4').val(data.zone_low_oil4);
                        $('#freewater4').val(data.zone_free_water4);
                        $('#gas_grav4').val(data.zone_grv_gas4);
                        $('#oil_grav4').val(data.zone_grv_grv_oil4);
                        $('#zone_wellbore_coefficient4').val(data.zone_wellbore_coefficient4);
                        $('#wellbore_time4').val(data.zone_wellbore_time4);
                        $('#rsbt_4').val(data.zone_res_shape4);
                        $('#oilchoke4').val(data.zone_prod_oil_choke4);
                        $('#oilflow4').val(data.zone_prod_oil_flow4);
                        $('#gaschoke4').val(data.zone_prod_gas_choke4);
                        $('#gasflow4').val(data.zone_prod_gas_flow4);
                        $('#gasoilratio4').val(data.zone_prod_gasoil_ratio4);
                        $('#zone_prod_conden_ratio4').val(data.zone_prod_conden_ratio4);
                        $('#cummulative_pro_gas4').val(data.zone_prod_cumm_gas4);
                        $('#cummulative_pro_oil4').val(data.zone_prod_cumm_oil4);
                        $('#ab_openflow14').val(data.zone_prod_aof_bbl4);
                        $('#ab_openflow24').val(data.zone_prod_aof_scf4);
                        $('#criticalrate14').val(data.zone_prod_critical_bbl4);
                        $('#criticalrate24').val(data.zone_prod_critical_scf4);
                        $('#pro_index14').val(data.zone_prod_index_bbl4);
                        $('#pro_index24').val(data.zone_prod_index_scf4);
                        $('#diffusity_fact4').val(data.zone_prod_diffusity4);
                        $('#permeability4').val(data.zone_prod_permeability4);
                        $('#iafit_4').val(data.zone_prod_infinite4);
                        $('#wellradius4').val(data.zone_prod_well_radius4);
                        $('#pfit_4').val(data.zone_prod_pseudostate4);
                        $('#res_bondradius4').val(data.zone_prod_res_radius4);
                        $('#deltaP4').val(data.zone_prod_delta4);
                        $('#wellbore_skin4').val(data.zone_prod_wellbore4);
                        $('#rock_compress4').val(data.zone_prod_compres_rock4);
                        $('#fluid_compress4').val(data.zone_prod_compres_fluid4);
                        $('#total_compress4').val(data.zone_prod_compres_total4);
                        $('#secd_porosity4').val(data.zone_prod_second4);
                        $('#I_4').val(data.zone_prod_lambda4);
                        $('#W_4').val(data.zone_prod_omega4);
                        $('#oilshow_read14').select2('val', data.zone_hc_oil_show4);
                        $('#oilshow_read24').val(data.zone_hc_oil_show_tools4);
                        $('#gasshow_read14').select2('val', data.zone_hc_gas_show4);
                        $('#gasshow_read24').val(data.zone_hc_gas_show_tools4);
                        $('#wellmak_watercut4').val(data.zone_hc_water_cut4);
                        $('#water_bearing14').val(data.zone_hc_water_gwc4);
                        $('#water_bearing24').val(data.zone_hc_water_gwd_tools4);
                        $('#water_bearing34').val(data.zone_hc_water_owc4);
                        $('#water_bearing44').val(data.zone_hc_water_owc_tools4);
                        $('#s2id_rock_sampling4').select2('val', data.zone_rock_method4);
                        $('#s2id_petro_analys4').select2('val', data.zone_rock_petro4);
                        $('#sample4').val(data.zone_rock_petro_sample4);
                        $('#ticsd_4').val(data.zone_rock_top_depth4);
                        $('#bicsd_4').val(data.zone_rock_bot_depth4);
                        $('#num_totalcare4').val(data.zone_rock_barrel4);
                        $('#corebarrel4').val(data.zone_rock_barrel_equal4);
                        $('#totalrec4').val(data.zone_rock_total_core4);
                        $('#precore4').val(data.zone_rock_preservative4);
                        $('#rca_14').val(data.zone_rock_routine4);
                        $('#rca_4').val(data.zone_rock_routine_sample4);
                        $('#scal_14').val(data.zone_rock_scal4);
                        $('#scal_24').val(data.zone_rock_scal_sample4);
                        $('#zone_clastic_p10_thickness4').val(data.zone_clastic_p10_thickness4);
                        $('#zone_clastic_p50_thickness4').val(data.zone_clastic_p50_thickness4);
                        $('#zone_clastic_p90_thickness4').val(data.zone_clastic_p90_thickness4);
                        $('#zone_clastic_p10_gr4').val(data.zone_clastic_p10_gr4);
                        $('#zone_clastic_p50_gr4').val(data.zone_clastic_p50_gr4);
                        $('#zone_clastic_p90_gr4').val(data.zone_clastic_p90_gr4);
                        $('#zone_clastic_p10_sp4').val(data.zone_clastic_p10_sp4);
                        $('#zone_clastic_p50_sp4').val(data.zone_clastic_p50_sp4);
                        $('#zone_clastic_p90_sp4').val(data.zone_clastic_p90_sp4);
                        $('#zone_clastic_p10_net4').val(data.zone_clastic_p10_net4);
                        $('#zone_clastic_p50_net4').val(data.zone_clastic_p50_net4);
                        $('#zone_clastic_p90_net4').val(data.zone_clastic_p90_net4);
                        $('#zone_clastic_p10_por4').val(data.zone_clastic_p10_por4);
                        $('#zone_clastic_p50_por4').val(data.zone_clastic_p50_por4);
                        $('#zone_clastic_p90_por4').val(data.zone_clastic_p90_por4);
                        $('#zone_clastic_p10_satur4').val(data.zone_clastic_p10_satur4);
                        $('#zone_clastic_p50_satur4').val(data.zone_clastic_p50_satur4);
                        $('#zone_clastic_p90_satur4').val(data.zone_clastic_p90_satur4);
                        $('#zone_carbo_p10_thickness4').val(data.zone_carbo_p10_thickness4);
                        $('#zone_carbo_p50_thickness4').val(data.zone_carbo_p50_thickness4);
                        $('#zone_carbo_p90_thickness4').val(data.zone_carbo_p90_thickness4);
                        $('#zone_carbo_p10_dtc4').val(data.zone_carbo_p10_dtc4);
                        $('#zone_carbo_p50_dtc4').val(data.zone_carbo_p50_dtc4);
                        $('#zone_carbo_p90_dtc4').val(data.zone_carbo_p90_dtc4);
                        $('#zone_carbo_p10_total4').val(data.zone_carbo_p10_total4);
                        $('#zone_carbo_p50_total4').val(data.zone_carbo_p50_total4);
                        $('#zone_carbo_p90_total4').val(data.zone_carbo_p90_total4);
                        $('#zone_carbo_p10_net4').val(data.zone_carbo_p10_net4);
                        $('#zone_carbo_p50_net4').val(data.zone_carbo_p50_net4);
                        $('#zone_carbo_p90_net4').val(data.zone_carbo_p90_net4);
                        $('#zone_carbo_p10_por4').val(data.zone_carbo_p10_por4);
                        $('#zone_carbo_p50_por4').val(data.zone_carbo_p50_por4);
                        $('#zone_carbo_p90_por4').val(data.zone_carbo_p90_por4);
                        $('#zone_carbo_p10_satur4').val(data.zone_carbo_p10_satur4);
                        $('#zone_carbo_p50_satur4').val(data.zone_carbo_p50_satur4);
                        $('#zone_carbo_p90_satur4').val(data.zone_carbo_p90_satur4);
                        $('#z2_4').val(data.zone_fluid_date4);
                        $('#sampleat4').val(data.zone_fluid_sample4);
                        $('#gasoilratio_4').val(data.zone_fluid_ratio4);
                        $('#separator_pressure4').val(data.zone_fluid_pressure4);
                        $('#separator_temperature4').val(data.zone_fluid_temp4);
                        $('#tubing_pressure4').val(data.zone_fluid_tubing4);
                        $('#casing_pressure4').val(data.zone_fluid_casing4);
                        $('#sampleby4').val(data.zone_fluid_by4);
                        $('#repost_avail4').val(data.zone_fluid_report4);
                        $('#hydro_finger4').val(data.zone_fluid_finger4);
                        $('#oilgrav_604').val(data.zone_fluid_grv_oil4);
                        $('#gasgrav_604').val(data.zone_fluid_grv_gas4);
                        $('#condensategrav_604').val(data.zone_fluid_grv_conden4);
                        $('#pvt_4').val(data.zone_fluid_pvt4);
                        $('#sample_4').val(data.zone_fluid_quantity4);
                        $('#gasdev_fac4').val(data.zone_fluid_z4);
                        $('#zone_fvf_oil_p104').val(data.zone_fvf_oil_p104);
                        $('#zone_fvf_oil_p504').val(data.zone_fvf_oil_p504);
                        $('#zone_fvf_oil_p904').val(data.zone_fvf_oil_p904);
                        $('#zone_fvf_gas_p104').val(data.zone_fvf_gas_p104);
                        $('#zone_fvf_gas_p504').val(data.zone_fvf_gas_p504);
                        $('#zone_fvf_gas_p904').val(data.zone_fvf_gas_p904);
                        $('#oilviscocity4').val(data.zone_viscocity4);
                                                    		
						$('#zone_id5').val(data.zone_id5);
                        $('#zonename5').val(data.zone_name5);
                        $('#s2id_welltest5').select2('val', data.zone_test5);
                        $('#z_5').val(data.zone_test_date5);
                        $('#s2id_zoneresult5').select2('val', data.zone_result5);
                        $('#zonearea5').val(data.zone_area5);
                        $('#zonethickness5').val(data.zone_thickness5);
                        $('#zoneinterval5').val(data.zone_depth5);
                        $('#perforation_interval5').val(data.zone_perforation5);
                        $('#s2id_welltest_type5').select2('val', data.zone_well_test5);
                        $('#welltest_total5').val(data.zone_well_duration5);
                        $('#initialflow5').val(data.zone_initial_flow5);
                        $('#initialshutin5').val(data.zone_initial_shutin5);
                        $('#tubingsize5').val(data.zone_tubing_size5);
                        $('#initialtemperature5').val(data.zone_initial_temp5);
                        $('#zone_initial_pressure5').val(data.zone_initial_pressure5);
                        $('#res_presure5').val(data.zone_pseudostate5);
                        $('#pressurewell_for5').val(data.zone_well_formation5);
                        $('#pressurewell_head5').val(data.zone_head5);
                        $('#res_pressure5').val(data.zone_pressure_wellbore5);
                        $('#avg_porpsity5').val(data.zone_average_por5);
                        $('#watercut5').val(data.zone_water_cut5);
                        $('#initialwater_sat5').val(data.zone_initial_water5);
                        $('#lowtest_gas5').val(data.zone_low_gas5);
                        $('#lowtest_oil5').val(data.zone_low_oil5);
                        $('#freewater5').val(data.zone_free_water5);
                        $('#gas_grav5').val(data.zone_grv_gas5);
                        $('#oil_grav5').val(data.zone_grv_grv_oil5);
                        $('#zone_wellbore_coefficient5').val(data.zone_wellbore_coefficient5);
                        $('#wellbore_time5').val(data.zone_wellbore_time5);
                        $('#rsbt_5').val(data.zone_res_shape5);
                        $('#oilchoke5').val(data.zone_prod_oil_choke5);
                        $('#oilflow5').val(data.zone_prod_oil_flow5);
                        $('#gaschoke5').val(data.zone_prod_gas_choke5);
                        $('#gasflow5').val(data.zone_prod_gas_flow5);
                        $('#gasoilratio5').val(data.zone_prod_gasoil_ratio5);
                        $('#zone_prod_conden_ratio5').val(data.zone_prod_conden_ratio5);
                        $('#cummulative_pro_gas5').val(data.zone_prod_cumm_gas5);
                        $('#cummulative_pro_oil5').val(data.zone_prod_cumm_oil5);
                        $('#ab_openflow15').val(data.zone_prod_aof_bbl5);
                        $('#ab_openflow25').val(data.zone_prod_aof_scf5);
                        $('#criticalrate15').val(data.zone_prod_critical_bbl5);
                        $('#criticalrate25').val(data.zone_prod_critical_scf5);
                        $('#pro_index15').val(data.zone_prod_index_bbl5);
                        $('#pro_index25').val(data.zone_prod_index_scf5);
                        $('#diffusity_fact5').val(data.zone_prod_diffusity5);
                        $('#permeability5').val(data.zone_prod_permeability5);
                        $('#iafit_5').val(data.zone_prod_infinite5);
                        $('#wellradius5').val(data.zone_prod_well_radius5);
                        $('#pfit_5').val(data.zone_prod_pseudostate5);
                        $('#res_bondradius5').val(data.zone_prod_res_radius5);
                        $('#deltaP5').val(data.zone_prod_delta5);
                        $('#wellbore_skin5').val(data.zone_prod_wellbore5);
                        $('#rock_compress5').val(data.zone_prod_compres_rock5);
                        $('#fluid_compress5').val(data.zone_prod_compres_fluid5);
                        $('#total_compress5').val(data.zone_prod_compres_total5);
                        $('#secd_porosity5').val(data.zone_prod_second5);
                        $('#I_5').val(data.zone_prod_lambda5);
                        $('#W_5').val(data.zone_prod_omega5);
                        $('#oilshow_read15').select2('val', data.zone_hc_oil_show5);
                        $('#oilshow_read25').val(data.zone_hc_oil_show_tools5);
                        $('#gasshow_read15').select2('val', data.zone_hc_gas_show5);
                        $('#gasshow_read25').val(data.zone_hc_gas_show_tools5);
                        $('#wellmak_watercut5').val(data.zone_hc_water_cut5);
                        $('#water_bearing15').val(data.zone_hc_water_gwc5);
                        $('#water_bearing25').val(data.zone_hc_water_gwd_tools5);
                        $('#water_bearing35').val(data.zone_hc_water_owc5);
                        $('#water_bearing45').val(data.zone_hc_water_owc_tools5);
                        $('#s2id_rock_sampling5').select2('val', data.zone_rock_method5);
                        $('#s2id_petro_analys5').select2('val', data.zone_rock_petro5);
                        $('#sample5').val(data.zone_rock_petro_sample5);
                        $('#ticsd_5').val(data.zone_rock_top_depth5);
                        $('#bicsd_5').val(data.zone_rock_bot_depth5);
                        $('#num_totalcare5').val(data.zone_rock_barrel5);
                        $('#corebarrel5').val(data.zone_rock_barrel_equal5);
                        $('#totalrec5').val(data.zone_rock_total_core5);
                        $('#precore5').val(data.zone_rock_preservative5);
                        $('#rca_15').val(data.zone_rock_routine5);
                        $('#rca_5').val(data.zone_rock_routine_sample5);
                        $('#scal_15').val(data.zone_rock_scal5);
                        $('#scal_25').val(data.zone_rock_scal_sample5);
                        $('#zone_clastic_p10_thickness5').val(data.zone_clastic_p10_thickness5);
                        $('#zone_clastic_p50_thickness5').val(data.zone_clastic_p50_thickness5);
                        $('#zone_clastic_p90_thickness5').val(data.zone_clastic_p90_thickness5);
                        $('#zone_clastic_p10_gr5').val(data.zone_clastic_p10_gr5);
                        $('#zone_clastic_p50_gr5').val(data.zone_clastic_p50_gr5);
                        $('#zone_clastic_p90_gr5').val(data.zone_clastic_p90_gr5);
                        $('#zone_clastic_p10_sp5').val(data.zone_clastic_p10_sp5);
                        $('#zone_clastic_p50_sp5').val(data.zone_clastic_p50_sp5);
                        $('#zone_clastic_p90_sp5').val(data.zone_clastic_p90_sp5);
                        $('#zone_clastic_p10_net5').val(data.zone_clastic_p10_net5);
                        $('#zone_clastic_p50_net5').val(data.zone_clastic_p50_net5);
                        $('#zone_clastic_p90_net5').val(data.zone_clastic_p90_net5);
                        $('#zone_clastic_p10_por5').val(data.zone_clastic_p10_por5);
                        $('#zone_clastic_p50_por5').val(data.zone_clastic_p50_por5);
                        $('#zone_clastic_p90_por5').val(data.zone_clastic_p90_por5);
                        $('#zone_clastic_p10_satur5').val(data.zone_clastic_p10_satur5);
                        $('#zone_clastic_p50_satur5').val(data.zone_clastic_p50_satur5);
                        $('#zone_clastic_p90_satur5').val(data.zone_clastic_p90_satur5);
                        $('#zone_carbo_p10_thickness5').val(data.zone_carbo_p10_thickness5);
                        $('#zone_carbo_p50_thickness5').val(data.zone_carbo_p50_thickness5);
                        $('#zone_carbo_p90_thickness5').val(data.zone_carbo_p90_thickness5);
                        $('#zone_carbo_p10_dtc5').val(data.zone_carbo_p10_dtc5);
                        $('#zone_carbo_p50_dtc5').val(data.zone_carbo_p50_dtc5);
                        $('#zone_carbo_p90_dtc5').val(data.zone_carbo_p90_dtc5);
                        $('#zone_carbo_p10_total5').val(data.zone_carbo_p10_total5);
                        $('#zone_carbo_p50_total5').val(data.zone_carbo_p50_total5);
                        $('#zone_carbo_p90_total5').val(data.zone_carbo_p90_total5);
                        $('#zone_carbo_p10_net5').val(data.zone_carbo_p10_net5);
                        $('#zone_carbo_p50_net5').val(data.zone_carbo_p50_net5);
                        $('#zone_carbo_p90_net5').val(data.zone_carbo_p90_net5);
                        $('#zone_carbo_p10_por5').val(data.zone_carbo_p10_por5);
                        $('#zone_carbo_p50_por5').val(data.zone_carbo_p50_por5);
                        $('#zone_carbo_p90_por5').val(data.zone_carbo_p90_por5);
                        $('#zone_carbo_p10_satur5').val(data.zone_carbo_p10_satur5);
                        $('#zone_carbo_p50_satur5').val(data.zone_carbo_p50_satur5);
                        $('#zone_carbo_p90_satur5').val(data.zone_carbo_p90_satur5);
                        $('#z2_5').val(data.zone_fluid_date5);
                        $('#sampleat5').val(data.zone_fluid_sample5);
                        $('#gasoilratio_5').val(data.zone_fluid_ratio5);
                        $('#separator_pressure5').val(data.zone_fluid_pressure5);
                        $('#separator_temperature5').val(data.zone_fluid_temp5);
                        $('#tubing_pressure5').val(data.zone_fluid_tubing5);
                        $('#casing_pressure5').val(data.zone_fluid_casing5);
                        $('#sampleby5').val(data.zone_fluid_by5);
                        $('#repost_avail5').val(data.zone_fluid_report5);
                        $('#hydro_finger5').val(data.zone_fluid_finger5);
                        $('#oilgrav_605').val(data.zone_fluid_grv_oil5);
                        $('#gasgrav_605').val(data.zone_fluid_grv_gas5);
                        $('#condensategrav_605').val(data.zone_fluid_grv_conden5);
                        $('#pvt_5').val(data.zone_fluid_pvt5);
                        $('#sample_5').val(data.zone_fluid_quantity5);
                        $('#gasdev_fac5').val(data.zone_fluid_z5);
                        $('#zone_fvf_oil_p105').val(data.zone_fvf_oil_p105);
                        $('#zone_fvf_oil_p505').val(data.zone_fvf_oil_p505);
                        $('#zone_fvf_oil_p905').val(data.zone_fvf_oil_p905);
                        $('#zone_fvf_gas_p105').val(data.zone_fvf_gas_p105);
                        $('#zone_fvf_gas_p505').val(data.zone_fvf_gas_p505);
                        $('#zone_fvf_gas_p905').val(data.zone_fvf_gas_p905);
                        $('#oilviscocity5').val(data.zone_viscocity5);
				},
			});
		}
	", CClientScript::POS_END);
	
	
?>