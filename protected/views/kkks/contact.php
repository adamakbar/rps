<?php
/* @var $this SiteController */

$this->pageTitle='Help';
$this->breadcrumbs=array(
	'Help',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->			    			
			<h3 class="page-title">Help</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Help</a><span class="divider-last">&nbsp;</span></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Contact</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<div class="contact-us">
							<div class="row-fluid">
								<div class="span12">
                                      <p>
                                      	<strong>Email (SKK Migas) :</strong> sumberdaya@skkmigas.go.id <br>
                                      	<strong>Email (LEMIGAS) :</strong> coachingclinicrps@lemigas.esdm.go.id <br>
                                      </p>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
			

