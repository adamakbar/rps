
<div class="GalleryEditor" id="<?php echo $this->id?>">
    <div class="gform">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'uploadimage-form',
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),	
		));?>
		
		<span class="btn btn-success fileinput-button">
				<i class="icon-plus icon-white"></i><span>Add Image</span>
		        <?php echo $form->fileField($mdlUploadImage, 'image-path', array('class'=>'afile', 'accept' => "image/*", 'multiple' => 'true')); ?>
		</span>
		
		<?php $this->endWidget(); ?>
	</div>
</div>

<div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
    	<h3 id="myModalLabel">Success</h3>
  	</div>
  	<div class="modal-body">
    	<p id="message">Your images has success upload</p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  	</div>
</div>

<?php 
$cs = Yii::app()->getClientScript();

$css = <<<EOD
    /* Photo Gallery */
    .GalleryEditor {
        border: 1px solid #DDD;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .GalleryEditor div.gform {
        padding-top: 4px;
        clear: left;
    }
    .GalleryEditor form{
        margin: 0;
    }

    .GalleryEditor .photo {
        position: relative;
        float: left;
        background-color: #fff;
        margin: 4px;
        height: 178px;
        width:140px;

        display: block;
        padding: 4px;
        line-height: 1;
        border: 1px solid #DDD;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .GalleryEditor .photo img {
        width: 140px;
        height: auto;
    }

    .GalleryEditor .photo a {
        padding-left: 8px;
    }

    .GalleryEditor .photo .actions {
        float: right;

        position: absolute;
        bottom: 4px;
        right: 4px;
    }


    .GalleryEditor hr{
        margin: 0 4px;
    }

    .GalleryEditor .fileinput-button {
        position: relative;
        overflow: hidden;
        margin-left: 8px;
        margin-top: 4px;
        margin-bottom: 4px;
    }
    .GalleryEditor .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        border: solid transparent;
        border-width: 0 0 100px 200px;
        opacity: 0;
        filter: alpha(opacity=0);
        -moz-transform: translate(-300px, 0) scale(4);
        direction: ltr;
        cursor: pointer;
    }

    /* modal styles*/
    .GalleryEditor .preview {
        overflow: hidden;
        width: 200px;
        height: 156px;
        margin-right: 10px;
        overflow: hidden;
        float: left;
    }

    .GalleryEditor .preview img {
        width: 200px;
    }

    .GalleryEditor .photo-editor {
        min-height:156px;
        margin-bottom: 4px;
        padding: 4px;
        border: 1px solid #DDD;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .photo-editor form {
        margin-bottom: 0;
    }

    .GalleryEditor .caption p{
        height: 40px;
        overflow: hidden;
    }

    /* fixed thumbnail sizes */
    .GalleryEditor.no-desc .photo{
        height: 138px;
    }
    .GalleryEditor.no-name .photo{
        height: 160px;
    }
    .GalleryEditor.no-name-no-desc .photo{
        height: 120px;
    }
    .GalleryEditor .image-preview{
        height: 88px;
        overflow: hidden;
    }
    /* item selection */
    .GalleryEditor .photo-select{
        position: absolute;
        bottom: 8px;
        left: 8px;
    }
    .GalleryEditor .photo.selected{
        background-color: #cef;
        border-color: blue;
    }
EOD;

$cs->registerCss($this->id . 'css', $css);
?>
<script type="text/javascript">
$(function() {

	var ajaxUploadUrl = '<?php echo  Yii::app()->createUrl('kkks/ajaxuploadimageplay', array('view'=>$view))?>';

	var $editorModal = $('#popup');
	
	if (typeof window.FormData == 'function') {
		$('.afile').attr('multiple', 'true').on('change', function (e) {
			e.preventDefault();
			var filesCount = this.files.length;
			var uploadedCount = 0;

			for(var i = 0; i < filesCount; i++) {
				var fd = new FormData();
				fd.append(this.name, this.files[i]);
				var xhr = new XMLHttpRequest();
				xhr.open('POST', ajaxUploadUrl, true);
				xhr.onload = function () {
					uploadedCount++;
					if (uploadedCount === filesCount) $editorModal.modal('show');
				};
				xhr.send(fd);
			}
		});
	}
});
</script>