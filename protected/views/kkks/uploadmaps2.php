<?php
/* @var $this SiteController */

$this->pageTitle='File Upload';
$this->breadcrumbs=array(
	'Upload',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">UPLOAD MAPS</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>File Upload</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
		<div class="row-fluid">
			<div class="span12">
				<div id="_list-data-well" class="widget">
					<div class="widget-title">
						<h4><i class="icon-cloud"></i>Upload Maps</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal">
					
							<div class="GalleryEditor" id="<?php echo $this->id?>">
					    		<div class="gform">
									<?php $form = $this->beginWidget('CActiveForm', array(
										'id'=>'uploadimage-form',
										'htmlOptions'=>array('enctype'=>'multipart/form-data'),	
									));?>
									<div class="control-group">
			                            <div class="alert alert-success">
			                                <button class="close" data-dismiss="alert">×</button>
			                                <p>For Working Area, Lead, or Prospect Map please using .shp file and other supported file with WGS 84 or Latitude/Longitude format, for other document report you can upload within "Other Document" category using this format .doc, .ppt, or .pdf</p>
			                            </div>
			                        </div>
									<div class="control-group">
							       		<label class="control-label">Category :</label>
							            <div class="controls">
							            <?php echo $form->textField($mdlUploadImage, 'upload_category', array('id'=>'map_category', 'class'=>'span3', 'style'=>'text-align: center;'));?>
							            <br><br>
							            <div id="error-category"></div>
							            </div>
							        </div>
									<div class="control-group">
										<div class="controls">
											<?php echo CHtml::textField('input-view', '', array('id'=>'input-view', 'class'=>'input-large')); ?>
							        		<span class="prettyFile">
											    <div class="btn btn-success fileinput-button">
							 					<i class=""></i><span id="label">Browse</span>
												<?php echo $form->fileField($mdlUploadImage, 'image-path', array('class'=>'afile', 'multiple' => 'true')); ?>
											    </div>
											</span>
										</div>
									</div>
									<div id="info"></div>
					        		<?php $this->endWidget();?>
					        	</div>
							</div>
						</span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
				<div id="_list-data-well" class="widget">
					<div class="widget-title">
						<h4><i class="icon-picture"></i>Gallery</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal">
							<div id="jQ-menu"></div>
						</span>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
    	<h3 id="myModalLabel">Success</h3>
  	</div>
  	<div class="modal-body">
    	<p id="message">Your images has success upload</p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  	</div>
</div>
<?php 
$cs = Yii::app()->getClientScript();

$css = <<<EOD
    /* Photo Gallery */
		
	.input-append { display: inline-block; vertica-align: middle; }
 
	.input-large {
	    border: 1px solid rgba(82, 168, 236, 0.8);
	    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(82, 168, 236, .6);
	    border-radius: 3px 0 0 3px;
	    font-size: 14px;
	    height: 20px;
	    color: #555;
	    padding: 4px 6px;
	    margin-right: -4px;
	    width: 210px;
	}

    .GalleryEditor div.gform {
        padding-top: 4px;
		padding-left: 4px;
        clear: left;
    }
    .GalleryEditor form{
        margin: 0;
    }


    .GalleryEditor .fileinput-button {
        position: relative;
        overflow: hidden;
        margin-left: 0;
        margin-top: 3px;
        margin-bottom: 4px;
		height: 9px;
		width: 50px;
		
    }
		
	.GalleryEditor #label {
		position: absolute;
        top: 20%;
		left: 0;
		text-align: center;
		width: 100%;
		height: 100%;
		vertical-align: middle;
	}	
	
    .GalleryEditor .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        border: solid transparent;
        border-width: 0 0 100px 200px;
        opacity: 0;
        filter: alpha(opacity=0);
        -moz-transform: translate(-300px, 0) scale(4);
        direction: ltr;
        cursor: pointer;
    }
		
	#jQ-menu ul {
	list-style-type: none;
	}
	
	#jQ-menu a, #jQ-menu li {
		color: gray;
		text-decoration: none;
		padding-bottom: 3px;
	}
	
	#jQ-menu ul {
		padding-left: 15px;
	}

EOD;

$cs->registerCss($this->id . 'css', $css);
?>
<script type="text/javascript">

$(document).ready(function() {
	$("a.gambar").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false
	});
	
	$('#input-view').val('No files selected');
	
	var ajaxUploadUrl = '<?php echo  Yii::app()->createUrl('kkks/ajaxrequestuploadmaps2')?>';
	
	$('.afile').on('change', function (e) {
		if($('#map_category').val() == '') {
			$("#error-category").empty();
			$("#error-category").append("Category cannot blank");
			$("#error-category").parent().addClass("has-err");
			$('#input-view').val('No files selected');
		} else {
			var formData = new FormData();
			var filesCount = this.files.length;
			var i, file, progress, size;

			$('#input-view').val(filesCount + ' files selected');
			$(".has-err").removeClass("has-err");
			$("#error-category").empty();

			for(i = 0; i < filesCount; i++){
				file = this.files[i];
				size = this.files[i].size;
				name = this.files[i].name;

				var regexAll = /[^\\]*\.(\w+)$/;
				var full = name.match(regexAll);
				var extension = full[1];

				/*
					\.(?:jpe?g|png|gif)$
					will match if the tested string ends in .jpeg, .png etc.
				*/
				var regexExt = /(?:zip|rar|exe)\b/; //will match if the tested string contains .jpeg, .png or one of the other alternatives.
				
				if(!regexExt.test(extension))
				{
					if((Math.round(size))<=(10240*1024)) //Limited size 1 MB
					{
						formData.append("file[]", file);  // adding file to formdata
						formData.append("UploadImage", $('#map_category').val());
						if(i==(filesCount-1))
						{
							$("#info").html("wait a moment to complete upload");
							$.ajax({
								url: ajaxUploadUrl,
								type: "POST",
								data: formData,
								dataType: 'json',
								processData: false,
								contentType: false,
								success: function(res){
									console.log(res);
									$("#info").empty();
									var myArray = JSON.parse(res.err);
				        			$.each(myArray, function(key, val) {
				        				$("#info").append(key + " : " + val + "<br>");
				        			});
				        			$("#jQ-menu").empty();
									loadImage();
								},
							});
						}
					} else {
						$("#info").html(name+" Size limit exceeded");
						return;
					}
				} else {
					$("#info").html(name+" File isn't Allowed");
					return;
				}
				
			}
		}
		return false;
	});
});


loadImage();

function loadImage() {
	var category_value = $('#map_category').val();
	var baseUrl = '<?php echo  Yii::app()->request->baseUrl;?>/js/jquery.color.js';
	var baseUrl2 = '<?php echo  Yii::app()->request->baseUrl;?>/js/jMenu.js';
	$.ajax({
		url : '<?php echo Yii::app()->createUrl('/Kkks/loadimage') ?>' ,
		data : {
			category : category_value,
		},
		type : 'POST',
		dataType : 'json',
		success : function(data) {
			$("#jQ-menu").append(data.imageView);
			$.getScript(baseUrl);
			$.getScript(baseUrl2);
		},
	});
}
</script>
