<div class="accordion">
                                                    <!-- Well General Data -->
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w_">
                                                                <strong>Well General Data</strong>
                                                            </a>
                                                        </div>
                                                        <div id="tabcol_w_" class="accordion-body collapse">
                                                            <div class="accordion-inner">
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Name :</label>
                                                                    <div class="controls wajib">
                                                                        <!-- <input id="well_name" type="text"  class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_name", array('id'=>"well_name", 'class'=>"span3 popovers", 'data-trigger'=>"hover", 'data-placement'=>"right", 'data-container'=>"body", 'data-content'=>"Please using unique and meaningful well name.", 'data-original-title'=>"Well Name"));?>
                                                                        <div id="Well_wl_name_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Latitude :</label>
                                                                    <div class="controls">
                                                                    	<table>
                                                                    		<tr>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_degree", array('id'=>"lat_degree", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
				                                                        					<span class="add-on">&#176;</span>
																						</div>
																						<div id="Well_lat_degree_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_minute", array('id'=>"lat_minute", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">'</span>
																						</div>
																						<div id="Well_lat_minute_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_second", array('id'=>"lat_second", 'class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">"</span>
																						</div>
																						<div id="Well_lat_second_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div>
																							<!-- <input type="text"  class="input-mini" placeholder="S/ N"/><span class="add-on"> -->
																							<?php echo CHtml::activeTextField($mdlWell, "lat_direction", array('id'=>"lat_direction", 'class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
																						</div>
																						<div id="Well_lat_direction_em_" class="errorMessage" style="display:none"></div>
																					</span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                                    			</td>
                                                                    		</tr>
                                                                    	</table>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Longitude :</label>
                                                                    <div class="controls">
                                                                    	<table>
                                                                    		<tr>
                                                                    			<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_degree", array('id'=>"long_degree", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
			                                                        						<span class="add-on">&#176;</span>
																						</div>
																						<div id="Well_long_degree_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="minute"/><span class="add-on">'</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_minute", array('id'=>"long_minute", 'class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">'</span>
																						</div>
																						<div id="Well_long_minute_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
                                                                    				<span class="wajib">
																						<div class=" input-append">
																							<!-- <div class=" input-append"><input type="text"  class="input-mini" placeholder="second"/><span class="add-on">"</span></div> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_second", array('id'=>"long_second", 'class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
				                                                        					<span class="add-on">"</span>
																						</div>
																						<div id="Well_long_second_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
                                                                    				<span class="wajib">
																						<div>
																							<!-- <input type="text"  class="input-mini" placeholder="E/ W"/><span class="add-on"> -->
																							<?php echo CHtml::activeTextField($mdlWell, "long_direction", array('value'=>'E', 'id'=>"long_direction", 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																						</div>
																						<div id="Well_long_direction_em_" class="errorMessage" style="display:none"></div>
																					</span>
																				</td>
																				<td>
																					<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
																				</td>
                                                                    		</tr>
                                                                    	</table>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Result :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="well_type" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_result", array('id'=>"well_result_discovery", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_result_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Type :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="well_type" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_type", array('id'=>"well_type1", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_type_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                <label class="control-label">Onshore or Offshore :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="onoffshore" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_shore", array('id'=>"onoffshore", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_shore_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Terrain :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="terrain" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_terrain", array('id'=>"terrain", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_terrain_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Well Status :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="well_status" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_status", array('id'=>"well_status", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_status_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Targeted Formation Name :</label>
                                                                    <div class="controls wajib">
                                                                        <!-- <input id="targeted_forname" class="span3" type="text"  style="text-align: center;"/> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_formation", array('id'=>"targetedformationname", 'class'=>"span3 formationname", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_formation_em_" class="errorMessage" style="display:none"></div>
                                                                        <p class="help-block"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Date Well Completed :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:165px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_date_complete", array('id'=>"dsw8_", 'class'=>"m-wrap medium popovers disable-input", 'style'=>"max-width:137px;", 'data-trigger'=>"hover", 'data-placement'=>"right", 'data-container'=>"body", 'data-content'=>"If Year that only available, please choose 1-January for Day and Month, if not leave it blank.", 'data-original-title'=>"Date Well Completed"));?>
                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                            <span id="cleardsw8_" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                                        </div>
                                                                        <div id="Well_wl_date_complete_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Targeted Total Depth :</label>
                                                                    <div class="controls">
                                                                    	<table>
                                                                    		<tr>
                                                                    			<td>
                                                                    				<span>
	                                                                    				<div class="input-prepend input-append">
	                                                                    					<span class="add-on">TVD</span>
				                                                                            <!-- <input id="ttd_1" type="text" style="max-width:155px"/> -->
				                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_depth_tvd", array('id'=>"ttd_1", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:126px;"));?>
				                                                                            <span class="add-on">ft</span>
				                                                                        </div>
				                                                                        <div id="Well_wl_target_depth_tvd_em_" class="errorMessage" style="display:none"></div>
			                                                                        </span>
                                                                    			</td>
                                                                    			<td>
                                                                    				<span style="margin-left: 24px">
	                                                                    				<div class="input-prepend input-append">
	                                                                    					<span class="add-on">MD</span>
				                                                                            <!-- <input id="ttd_2" type="text" style="max-width:155px; margin-left:24px;"/> -->
				                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_target_depth_md", array('id'=>"ttd_2", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:126px;"));?>
				                                                                            <span class="add-on">ft</span>
				                                                                        </div>
				                                                                        <div id="Well_wl_target_depth_md_em_" class="errorMessage" style="display:none"></div>
			                                                                        </span>
                                                                    			</td>
                                                                    		</tr>
                                                                    	</table>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Actual Total Depth :</label>
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                        	<span class="add-on">TVD</span>
                                                                            <!-- <input id="actual_total_depth" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_actual_depth", array('id'=>"actual_total_depth", 'class'=>"number", 'style'=>"max-width:126px"));?>
                                                                            <span class="add-on">ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_actual_depth_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                        	<span class="add-on">TVD</span>
	                                                                        <!-- <input id="tpprt_" type="text" style="max-width:155px"/> -->
	                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_target_play", array('id'=>"tpprt_", 'class'=>"number", 'style'=>"max-width:126px"));?>
	                                                                        <span class="add-on">ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_target_play_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                        	<span class="add-on">TVD</span>
                                                                            <!-- <input id="apprt_" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_actual_play", array('id'=>"apprt_", 'class'=>"number", 'style'=>"max-width:126px"));?>
                                                                            <span class="add-on">ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_actual_play_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of MDT Sample :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="num_mdt" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_number_mdt", array('id'=>"num_mdt", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_number_mdt_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of RFT Sample :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="num_rft" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_number_rft", array('id'=>"num_rft", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_number_rft_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Reservoir Initial Pressure :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="rip_" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_res_pressure", array('id'=>"rip_", 'class'=>"number", 'style'=>"max-width:126px;"));?>
                                                                            <span class="add-on">psig</span>
                                                                        </div>
                                                                        <div id="Well_wl_res_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Last Reservoir Pressure :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="lpr_" type="text" style="max-width:155px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_last_pressure", array('id'=>"lpr_", 'class'=>"number", 'style'=>"max-width:126px;"));?>
                                                                            <span class="add-on">psig</span>
                                                                        </div>
                                                                        <div id="Well_wl_last_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Pressure Gradient :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="pressure_gradient" type="text" style="max-width:145px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_pressure_gradient", array('id'=>"pressure_gradient", 'class'=>"number", 'style'=>"max-width:117px;"));?>
                                                                            <span class="add-on">psig/ft</span>
                                                                        </div>
                                                                        <div id="Well_wl_pressure_gradient_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Last Reservoir Temperature :</label>
                                                                    <div class="controls">
                                                                        <div class="input-append">
                                                                            <!-- <input id="lrt_" type="text" style="max-width:165px"/> -->
                                                                            <?php echo CHtml::activeTextField($mdlWell, "wl_last_temp", array('id'=>"lrt_", 'class'=>"number", 'style'=>"max-width:137px;"));?>
                                                                            <span class="add-on">&#176;C</span>
                                                                        </div>
                                                                        <div id="Well_wl_last_temp_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Actual Well Integrity :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="actual_well_integrity" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_integrity", array('id'=>"actual_well_integrity", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_integrity_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                                    <div class="controls">
                                                                        <!-- <input id="availability_electrolog" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_avail", array('id'=>"availability_electrolog", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_electro_avail_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">List of Electrolog Data :</label>
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list1", array('id'=>"wl_electro_list1", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list2", array('id'=>"wl_electro_list2", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list3", array('id'=>"wl_electro_list3", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list4", array('id'=>"wl_electro_list4", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list5", array('id'=>"wl_electro_list5", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list6", array('id'=>"wl_electro_list6", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list7", array('id'=>"wl_electro_list7", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list8", array('id'=>"wl_electro_list8", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list9", array('id'=>"wl_electro_list9", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list10", array('id'=>"wl_electro_list10", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list11", array('id'=>"wl_electro_list11", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list12", array('id'=>"wl_electro_list12", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <!-- <input class="span3" type="text" /> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_electro_list13", array('id'=>"wl_electro_list13", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list14", array('id'=>"wl_electro_list14", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list15", array('id'=>"wl_electro_list15", 'class'=>'span2 no-comma'));?>
																		<?php echo CHtml::activeTextField($mdlWell, "wl_electro_list16", array('id'=>"wl_electro_list16", 'class'=>'span2 no-comma'));?>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                                                   
                                                        </div>
                                                    </div>

                                                    <!-- Zone for Well 1-->
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w_2">
                                                                <strong>Zone Data</strong>
                                                            </a>
                                                        </div>
                                                        <div id="tabcol_w_2" class="accordion-body collapse">
                                                            <div class="accordion-inner">
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of Penetrated Production Zones :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <!-- <input id="wz_" class="span3" type="text" style="text-align: center;"/> -->
                                                                        <?php echo CHtml::activeTextField($mdlWell, "wl_total_zone", array('id'=>"wz_", 'class'=>"span3 wz", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <div id="Well_wl_total_zone_em_" class="errorMessage" style="display:none"></div>
                                                                    </div>
                                                                </div>
                                                                <!-- Notification -->
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <blockquote>
                                                                            <small>Detail of zone data can be filled at generated form below.</small>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                                <!-- Notification -->
                                                                <div class="row-fluid">
                                                                    <div class="span12">        
                                                                        <!-- Number Zone -->
                                                                        <div id="well_rumah_zone" class="tabbable tabbable-custom">
                                                                            <ul class="nav nav-tabs">
                                                                                <li id="li_well_zone_1" class="active"><a id="well_zone_1" href="#well_tab_z1" data-toggle="tab">Zone 1</a></li>
                                                                                <li id="li_well_zone_2"><a id="well_zone_2" href="#well_tab_z2" data-toggle="tab" class="hidden">Zone 2</a></li>
                                                                                <li id="li_well_zone_3"><a id="well_zone_3" href="#well_tab_z3" data-toggle="tab" class="hidden">Zone 3</a></li>
                                                                                <li id="li_well_zone_4"><a id="well_zone_4" href="#well_tab_z4" data-toggle="tab" class="hidden">Zone 4</a></li>
                                                                                <li id="li_well_zone_5"><a id="well_zone_5" href="#well_tab_z5" data-toggle="tab" class="hidden">Zone 5</a></li>
                                                                            </ul>
                                                                            <div class="tab-content">
                                                                                <!-- Zone 1 -->
                                                                                <?php $b = 0;?>
                                                                                <?php $ac = 'active';?>
                                                                                <?php for($j = 1; $j <= 5; $j++) { ?>
                                                                                <div class="tab-pane <?php if($j == 1) echo $ac;?>" id="well_tab_z<?=$j;?>">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b?>">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <!-- <input id="zonename" class="span3"  type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_name", array('id'=>"zonename$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_name_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <!-- <input id="z_welltest<?=$j?>" class="span3"  type="text" style="text-align: center;"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_test", array('id'=>"welltest$j", 'class'=>"span3", "style"=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_test_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="z1_3"  type="text" class="m-wrap medium" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_test_date", array('id'=>"z_$j", 'class'=>"m-wrap medium disable-input", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <span id="clearz_<?=$j;?>" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_test_date_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Result</label>
                                                                                                        <div class="controls">
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_result", array('id'=>"zoneresult$j", 'class'=>"span3", "style"=>"text-align: center; position: relative; display: inline-block;")) ;?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_result_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Area</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_area", array('id'=>"zonearea$j", 'class'=>'number', 'style'=>'max-width:130px;')) ;?>
                                                                                                                <span class="add-on">acre</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_area_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="zonethickness"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_thickness", array('id'=>"zonethickness$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="zoneinterval"  type="text" style="max-width:148px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_depth", array('id'=>"zoneinterval$j", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_depth_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="perforation_interval" type="text" style="max-width:148px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_perforation", array('id'=>"perforation_interval$j", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_perforation_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="welltest_type" class="span3" type="text" style="text-align: center;"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_well_test", array('id'=>"welltest_type$j", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_well_test_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="welltest_total" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_well_duration", array('id'=>"welltest_total$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_well_duration_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialflow" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_flow", array('id'=>"initialflow$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_flow_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialshutin" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_shutin", array('id'=>"initialshutin$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_shutin_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="tubingsize" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_tubing_size", array('id'=>"tubingsize$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">in</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_tubing_size_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialtemperature"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_temp", array('id'=>"initialtemperature$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">&#176;C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_temp_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="initialreservoir"  type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_pressure", array('id'=>"zone_initial_pressure$j", 'class'=>"popovers number", 'style'=>"max-width:120px;", 'data-trigger'=>"hover", 'data-placement'=>"right", 'data-container'=>"body", 'data-content'=>"Reservoir or well pressure from well analysis result (Bottom-hole Pressure).", 'data-original-title'=>"Initial Reservoir Pressure"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudo Steady State :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="res_presure" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_pseudostate", array('id'=>"res_presure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_pseudostate_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="pressurewell_for" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_well_formation", array('id'=>"pressurewell_for$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_well_formation_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="pressurewell_head" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_head", array('id'=>"pressurewell_head$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_head_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="res_pressure" type="text" style="max-width:150px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_pressure_wellbore", array('id'=>"res_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_pressure_wellbore_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="avg_porpsity"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_average_por", array('id'=>"avg_porpsity$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_average_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="watercut"  type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_water_cut", array('id'=>"watercut$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_water_cut_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                            <!-- <input id="initialwater_sat"  type="text" style="max-width:161px;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_initial_water", array('id'=>"initialwater_sat$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_initial_water_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="lowtest_gas" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_low_gas", array('id'=>"lowtest_gas$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_low_gas_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="lowtest_oil" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_low_oil", array('id'=>"lowtest_oil$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_low_oil_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="freewater" type="text" style="max-width:161px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_free_water", array('id'=>"freewater$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_free_water_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                        	<div class="input-append">
	                                                                                                            <!-- <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/> -->
	                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_grv_gas", array('id'=>"gas_grav$j", 'class'=>"popovers", 'style'=>"max-width:130px;", 'data-trigger'=>'hover', 'data-container'=>"body", 'data-content'=>"Ratio of gas density with air density", 'data-original-title'=>"Gas Gravity"));?>
                                                                                                            <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_grv_gas_em_" class="errorMessage" style="display:none"></div>                                                                                  
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                        	<div class="input-append">
	                                                                                                            <!-- <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/> -->
	                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_grv_grv_oil", array('id'=>"oil_grav$j", 'class'=>"popovers", 'style'=>"max-width:130px;", 'data-trigger'=>"hover", 'data-container'=>"body", 'data-content'=>"Ratio of oil density with air density (API units)", 'data-original-title'=>"Oil Gravity"));?>
                                                                                                            <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_grv_grv_oil_em_" class="errorMessage" style="display:none"></div>  
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellbore" type="text" style="max-width:138px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_wellbore_coefficient", array('id'=>"zone_wellbore_coefficient$j", 'class'=>"number", 'style'=>"max-width:105px;"));?>
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_wellbore_coefficient_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellbore_time" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_wellbore_time", array('id'=>"wellbore_time$j", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_wellbore_time_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="rsbt_" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_res_shape", array('id'=>"rsbt_$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_res_shape_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilchoke"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_oil_choke", array('id'=>"oilchoke$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_oil_choke_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilflow"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_oil_flow", array('id'=>"oilflow$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_oil_flow_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gaschoke"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_gas_choke", array('id'=>"gaschoke$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_gas_choke_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasflow"  type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_gas_flow", array('id'=>"gasflow$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_gas_flow_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasoilratio" type="text" style="max-width:139px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_gasoil_ratio", array('id'=>"gasoilratio$j", 'class'=>"number", 'style'=>"max-width:110px;"));?>
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_gasoil_ratio_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                        	<div class="input-append">
	                                                                                                            <!-- <input id="condensategas" class="span3" type="text"/> -->
	                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_conden_ratio", array('id'=>"zone_prod_conden_ratio$j", 'style'=>"max-width:130px;"));?>
	                                                                                                            <span class="add-on">bbl/scf</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_conden_ratio_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="cummulative_pro_gas" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_cumm_gas", array('id'=>"cummulative_pro_gas$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_cumm_gas_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="cummulative_pro_oil" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_cumm_oil", array('id'=>"cummulative_pro_oil$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_cumm_oil_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                            	<tr>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="ab_openflow1" type="text" style="max-width:149px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_aof_bbl", array('id'=>"ab_openflow1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px;"));?>
				                                                                                                                <span class="add-on">bbl/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_aof_bbl_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_aof_scf", array('id'=>"ab_openflow2$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px; margin-left:24px;"));?>
				                                                                                                                <span class="add-on">scf/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_aof_scf_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            	</tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                            	<tr>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="criticalrate1" type="text" style="max-width:149px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_critical_bbl", array('id'=>"criticalrate1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px;"));?>
				                                                                                                                <span class="add-on">bbl/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_critical_bbl_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_critical_scf", array('id'=>"criticalrate2$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px; margin-left:24px;"));?>
				                                                                                                                <span class="add-on">scf/d</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_critical_scf_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            	</tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                            	<tr>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="pro_index1" type="text" style="max-width:149px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_index_bbl", array('id'=>"pro_index1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px;"));?>
				                                                                                                                <span class="add-on">bbl/d/psi</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_index_bbl_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            		<td>
                                                                                                            			<span>
                                                                                                            				<div class="input-append">
				                                                                                                                <!-- <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" /> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_index_scf", array('id'=>"pro_index2$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:120px; margin-left:24px;"));?>
				                                                                                                                <span class="add-on">scf/d/psi</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_index_scf_em_" class="errorMessage" style="display:none"></div>
                                                                                                            			</span>
                                                                                                            		</td>
                                                                                                            	</tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="diffusity_fact" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_diffusity", array('id'=>"diffusity_fact$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_diffusity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="permeability" type="text" style="max-width:158px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_permeability", array('id'=>"permeability$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_permeability_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">How Long Infinite-acting period last :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="iafit_" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_infinite", array('id'=>"iafit_$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_infinite_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius :</label>
                                                                                                        <div class="controls wajib">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellradius"  type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_well_radius", array('id'=>"wellradius$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_well_radius_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">When the Pseudostate Condition is Reached :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="pfit_" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_pseudostate", array('id'=>"pfit_$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_pseudostate_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Radius of Investigation (Re)</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="res_bondradius"  type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_res_radius", array('id'=>"res_bondradius$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_res_radius_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="deltaP" type="text" style="max-width:155px;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_delta", array('id'=>"deltaP$j", 'class'=>"span3 number"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_delta_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="wellbore_skin" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_wellbore", array('id'=>"wellbore_skin$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_wellbore_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="rock_compress" type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_compres_rock", array('id'=>"rock_compress$j", 'class'=>"number", 'style'=>"max-width:115px;"));?>
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_compres_rock_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="fluid_compress" type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_compres_fluid", array('id'=>"fluid_compress$j", 'class'=>"number", 'style'=>"max-width:115px;"));?>
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_compres_fluid_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="total_compress" type="text" style="max-width:149px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_compres_total", array('id'=>"total_compress$j", 'class'=>"number", 'style'=>"max-width:115px;"));?>
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_compres_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="secd_porosity" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_second", array('id'=>"secd_porosity$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_wl_integrity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">&#955; :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="I_" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_lambda", array('id'=>"I_$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_second_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">&#969; :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="W_" type="text" style="max-width:165px;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_prod_omega", array('id'=>"W_$j", 'class'=>"span3 number"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_prod_omega_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="oilshow_read1"  class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_oil_show", array('id'=>"oilshow_read1$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_oil_show_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="oilshow_read2" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_oil_show_tools", array('id'=>"oilshow_read2$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_oil_show_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="gasshow_read1"  class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_gas_show", array('id'=>"gasshow_read1$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_gas_show_em_" class="errorMessage" style="display:none"></div>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="gasshow_read2" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_gas_show_tools", array('id'=>"gasshow_read2$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_gas_show_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="wellmak_watercut" type="text" style="max-width:165px"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_cut", array('id'=>"wellmak_watercut$j", 'class'=>"number", 'style'=>"max-width:130px"));?>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_water_cut_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div class="input-prepend input-append">
				                                                                                                                <span class="add-on">GWC</span>
				                                                                                                                <!-- <input id="water_bearing1" type="text" style="max-width:120px"/> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_gwc", array('id'=>"water_bearing1$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:95px"));?>
				                                                                                                                <span class="add-on">ft</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_water_gwc_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
	                                                                                                        					<!-- <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
	                                                                                                            				<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_gwd_tools", array('id'=>"water_bearing2$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width:165px; margin-left:24px;", 'placeholder'=>"By Tools Indication"));?>
                                                                                                            				</div>
                                                                                                            				<div id="Wellzone_<?php echo $j;?>_zone_hc_water_gwd_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div class="input-prepend input-append">
				                                                                                                                <span class="add-on">OWC</span>
				                                                                                                                <!-- <input id="water_bearing3" type="text" style="max-width:120px"/> -->
				                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_owc", array('id'=>"water_bearing3$j", 'class'=>"number tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"max-width:95px"));?>
				                                                                                                                <span class="add-on">ft</span>
				                                                                                                            </div>
				                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_hc_water_owc_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" /> -->
                                                                                                            					<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_hc_water_owc_tools", array('id'=>"water_bearing4$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width:165px; margin-left:24px;", 'placeholder'=>"By Tools Indication"));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_hc_water_owc_tools_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="rock_sampling" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_method", array('id'=>"rock_sampling$j", 'class'=>"span3", 'style'=>"text-align:center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_method_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="petro_analys" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_petro", array('id'=>"petro_analys$j", 'class'=>"span3", 'style'=>"text-align:center; position: relative; display: inline-block;"));?>
                                                                                                            <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_petro_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="sample" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_petro_sample", array('id'=>"sample$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_petro_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="ticsd_" type="text" style="max-width:145px;"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_top_depth", array('id'=>"ticsd_$j", 'class'=>"number", 'style'=>"max-width:110px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_top_depth_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                            	<span class="add-on">TVD</span>
                                                                                                                <!-- <input id="bicsd_" type="text" style="max-width:145px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_bot_depth", array('id'=>"bicsd_$j", 'class'=>"number", 'style'=>"max-width:110px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_bot_depth_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="num_totalcare" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_barrel", array('id'=>"num_totalcare$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_barrel_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="1corebarrel" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_barrel_equal", array('id'=>"corebarrel$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_barrel_equal_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="totalrec" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_total_core", array('id'=>"totalrec$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_total_core_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="precore" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_preservative", array('id'=>"precore$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_rock_preservative_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="rca_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                        						<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_routine", array('id'=>"rca_1$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px;"));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_routine_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>-->
                                                                                                            					<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_routine_sample", array('id'=>"rca_$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px; margin-left:24px;", 'placeholder'=>"Sample Quantity.."));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_routine_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                        	<table>
                                                                                                        		<tr>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="scal_1" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                        						<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_scal", array('id'=>"scal_1$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px;"));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_scal_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        			<td>
                                                                                                        				<span>
                                                                                                        					<div>
                                                                                                        						<!-- <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/> -->
                                                                                                            					<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_rock_scal_sample", array('id'=>"scal_2$j", 'class'=>"span3 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>"width: 177px; margin-left:24px;", 'placeholder'=>"Sample Quantity.."));?>
                                                                                                        					</div>
                                                                                                        					<div id="Wellzone_<?php echo $j;?>_zone_rock_scal_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        				</span>
                                                                                                        			</td>
                                                                                                        		</tr>
                                                                                                        	</table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>HC Saturation (1-Sw)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_thickness", array('id'=>"zone_clastic_p10_thickness$j", 'class'=>"span6 number zone_clastic_p10_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!--  <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_gr", array('id'=>"zone_clastic_p10_gr$j", 'class'=>"span6 number zone_clastic_p10_gr tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">API</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_gr_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_sp", array('id'=>"zone_clastic_p10_sp$j", 'class'=>"span6 number zone_clastic_p10_sp tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">mV</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_sp_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_net", array('id'=>"zone_clastic_p10_net$j", 'class'=>"span6 number zone_clastic_p10_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_por", array('id'=>"zone_clastic_p10_por$j", 'class'=>"span6 number zone_clastic_p10_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p10_satur", array('id'=>"zone_clastic_p10_satur$j", 'class'=>"span6 number zone_clastic_p10_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p10_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_thickness", array('id'=>"zone_clastic_p50_thickness$j", 'class'=>"span6 number zone_clastic_p50_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_gr", array('id'=>"zone_clastic_p50_gr$j", 'class'=>"span6 number zone_clastic_p50_gr tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">API</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_gr_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_sp", array('id'=>"zone_clastic_p50_sp$j", 'class'=>"span6 number zone_clastic_p50_sp tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">mV</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_sp_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_net", array('id'=>"zone_clastic_p50_net$j", 'class'=>"span6 number zone_clastic_p50_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_por", array('id'=>"zone_clastic_p50_por$j", 'class'=>"span6 number zone_clastic_p50_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p50_satur", array('id'=>"zone_clastic_p50_satur$j", 'class'=>"span6 number zone_clastic_p50_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p50_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_thickness", array('id'=>"zone_clastic_p90_thickness$j", 'class'=>"span6 number zone_clastic_p90_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_gr", array('id'=>"zone_clastic_p90_gr$j", 'class'=>"span6 number zone_clastic_p90_gr tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">API</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_gr_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_sp", array('id'=>"zone_clastic_p90_sp$j", 'class'=>"span6 number zone_clastic_p90_sp tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">mV</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_sp_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_net", array('id'=>"zone_clastic_p90_net$j", 'class'=>"span6 number zone_clastic_p90_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_por", array('id'=>"zone_clastic_p90_por$j", 'class'=>"span6 number zone_clastic_p90_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_clastic_p90_satur", array('id'=>"zone_clastic_p90_satur$j", 'class'=>"span6 number zone_clastic_p90_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_clastic_p90_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>HC Saturation (1-Sw)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_thickness", array('id'=>"zone_carbo_p10_thickness$j", 'class'=>"span6 number zone_carbo_p10_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_dtc", array('id'=>"zone_carbo_p10_dtc$j", 'class'=>"span6 number zone_carbo_p10_dtc tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">usec/ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_dtc_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_total", array('id'=>"zone_carbo_p10_total$j", 'class'=>"span6 number zone_carbo_p10_total tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_net", array('id'=>"zone_carbo_p10_net$j", 'class'=>"span6 number zone_carbo_p10_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_por", array('id'=>"zone_carbo_p10_por$j", 'class'=>"span6 number zone_carbo_p10_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p10_satur", array('id'=>"zone_carbo_p10_satur$j", 'class'=>"span6 number zone_carbo_p10_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p10_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_thickness", array('id'=>"zone_carbo_p50_thickness$j", 'class'=>"span6 number zone_carbo_p50_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_dtc", array('id'=>"zone_carbo_p50_dtc$j", 'class'=>"span6 number zone_carbo_p50_dtc tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">usec/ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_dtc_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_total", array('id'=>"zone_carbo_p50_total$j", 'class'=>"span6 number zone_carbo_p50_total tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_net", array('id'=>"zone_carbo_p50_net$j", 'class'=>"span6 number zone_carbo_p50_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_por", array('id'=>"zone_carbo_p50_por$j", 'class'=>"span6 number zone_carbo_p50_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p50_satur", array('id'=>"zone_carbo_p50_satur$j", 'class'=>"span6 number zone_carbo_p50_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p50_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_thickness", array('id'=>"zone_carbo_p90_thickness$j", 'class'=>"span6 number zone_carbo_p90_thickness tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_thickness_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_dtc", array('id'=>"zone_carbo_p90_dtc$j", 'class'=>"span6 number zone_carbo_p90_dtc tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">usec/ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_dtc_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_total", array('id'=>"zone_carbo_p90_total$j", 'class'=>"span6 number zone_carbo_p90_total tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">ft</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_total_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_net", array('id'=>"zone_carbo_p90_net$j", 'class'=>"span6 number zone_carbo_p90_net tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_net_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_por", array('id'=>"zone_carbo_p90_por$j", 'class'=>"span6 number zone_carbo_p90_por tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_por_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                    <td>
	                                                                                                                    <span>
	                                                                                                                    	<div class=" input-append">
	                                                                                                                    		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                    		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_carbo_p90_satur", array('id'=>"zone_carbo_p90_satur$j", 'class'=>"span6 number zone_carbo_p90_satur tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                    		<span class="add-on">%</span>
	                                                                                                                    	</div>
	                                                                                                                    	<div id="Wellzone_<?php echo $j;?>_zone_carbo_p90_satur_em_" class="errorMessage" style="display:none"></div>
                                                                                                                    	</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z<?=$j;?>_<?=++$b;?>">
                                                                                                    <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z<?=$j;?>_<?=$b;?>" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="z2_11" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_date", array('id'=>"z2_$j", 'class'=>"m-wrap medium popovers disable-input", 'style'=>"max-width:130px;", 'data-trigger'=>"hover", 'data-container'=>"body", 'data-content'=>"If Year that only available, please choose 1-January for Day and Month, if not leave it blank.", 'data-original-title'=>"Sample Date"));?>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <span id="clearz2_<?=$j;?>" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_date_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="sampleat" type="text" style="max-width:160px;"/> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_sample", array('id'=>"sampleat$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_sample_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasoilratio_" type="text" style="max-width:135px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_ratio", array('id'=>"gasoilratio_$j", 'class'=>"number", 'style'=>"max-width:105px;"));?>
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_ratio_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="separator_pressure" type="text" style="max-width:153px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_pressure", array('id'=>"separator_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_pressure_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="separator_temperature" type="text" style="max-width:163px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_temp", array('id'=>"separator_temperature$j", 'class'=>"number", 'style'=>"max-width:130px;"));?>
                                                                                                                <span class="add-on">&#176;C</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_temp_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="tubing_pressure" type="text" style="max-width:153px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_tubing", array('id'=>"tubing_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_tubing_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="casing_pressure" type="text" style="max-width:153px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_casing", array('id'=>"casing_pressure$j", 'class'=>"number", 'style'=>"max-width:120px;"));?>
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_casing_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="sampleby" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_by", array('id'=>"sampleby$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_by_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="repost_avail" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_report", array('id'=>"repost_avail$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_report_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="hydro_finger" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_finger", array('id'=>"hydro_finger$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_finger_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 &#176;F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilgrav_60" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_grv_oil", array('id'=>"oilgrav_60$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_grv_oil_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 &#176;F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="gasgrav_60" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_grv_gas", array('id'=>"gasgrav_60$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_grv_gas_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 &#176;F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="condensategrav_60" type="text" style="max-width:160px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_grv_conden", array('id'=>"condensategrav_60$j", 'class'=>"number", 'style'=>"max-width:125px;"));?>
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_grv_conden_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="pvt_" class="span3" type="text" style="text-align:center;" /> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_pvt", array('id'=>"pvt_$j", 'class'=>"span3"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_pvt_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="sample_" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_quantity", array('id'=>"sample_quantity$j", 'class'=>"span3 number"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_quantity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <!-- <input id="gasdev_fac" class="span3" type="text"/> -->
                                                                                                            <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fluid_z", array('id'=>"gasdev_fac$j", 'class'=>"span3 number"));?>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_fluid_z_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>Max</th>
                                                                                                                    <th>Mean</th>
                                                                                                                    <th>Min</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_oil_p10", array('id'=>"zone_fvf_oil_p10$j", 'class'=>"span6 number zone_fvf_oil_p10 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RBO/STB</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_oil_p10_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_oil_p50", array('id'=>"zone_fvf_oil_p50$j", 'class'=>"span6 number zone_fvf_oil_p50 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RBO/STB</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_oil_p50_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_oil_p90", array('id'=>"zone_fvf_oil_p90$j", 'class'=>"span6 number zone_fvf_oil_p90 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RBO/STB</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_oil_p90_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_gas_p10", array('id'=>"zone_fvf_gas_p10$j", 'class'=>"span6 number zone_fvf_gas_p10 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RCF/SCF</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_gas_p10_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_gas_p50", array('id'=>"zone_fvf_gas_p50$j", 'class'=>"span6 number zone_fvf_gas_p50 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RCF/SCF</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_gas_p50_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                                <td>
	                                                                                                                <span>
	                                                                                                                	<div class=" input-append">
	                                                                                                                		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                                                                                		<?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_fvf_gas_p90", array('id'=>"zone_fvf_gas_p90$j", 'class'=>"span6 number zone_fvf_gas_p90 tooltips", 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                                                                                		<span class="add-on">RCF/SCF</span>
	                                                                                                                	</div>
	                                                                                                                	<div id="Wellzone_<?php echo $j;?>_zone_fvf_gas_p90_em_" class="errorMessage" style="display:none"></div>
                                                                                                                	</span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <!-- <input id="oilviscocity" type="text" style="max-width:165px;" /> -->
                                                                                                                <?php echo CHtml::activeTextField($mdlWellZone, "[$j]zone_viscocity", array('id'=>"oilviscocity$j", 'class'=>"number", 'style'=>"max-width:130px; "));?>
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                            <div id="Wellzone_<?php echo $j;?>_zone_viscocity_em_" class="errorMessage" style="display:none"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php $b = 0;?>
                                    											<?php }?>
                                                                                <!-- Zone 2 -->
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <!-- End Number Zone -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                                        
                                                    </div>
                                                </div>
                                                
<?php 
	Yii::app()->clientScript->registerScript('formationname', "
		disabledElement('wz_');
		jQuery('.tooltips').tooltip();
		jQuery('.popovers').popover();
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('.formationname').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.formation,
				});
			},
		});
	", CClientScript::POS_END);
?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/data.js"></script>