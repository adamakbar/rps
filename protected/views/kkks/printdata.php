<?php
/* @var $this SiteController */

$this->pageTitle='Print';
$this->breadcrumbs=array('Print');

function massNumberFormat($data)
{
    /**
     * Key should have same name with intended value
     */
    foreach ($data as $key => $val) {
        if (strpos($key, 'gcf') !== false) {
            $data[$key] = number_format($val, 2, '.', '');
        } elseif (strpos($key, 'boi') !== false) {
            $data[$key] = number_format($val, 2, '.', '');
        } elseif (strpos($key, 'bgi') !== false) {
            if ($val <= 0.0001) {
                $data[$key] = number_format($val, 6, '.', '');
            } else {
                $data[$key] = number_format($val, 2, '.', '');
            }
        } else {
            if ($val < 1) {
                $data[$key] = number_format($val, 3, '.', '');
            } else {
                $data[$key] = number_format($val, 0, '', '');
            }
        }

        if ($data[$key] == '0'
            or $data[$key] == '1.22'
            or $data[$key] == '0.0063') {
            $data[$key] = '-';
        }
    }

    return $data;
}
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">PRINT DATA</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Summary</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Data Print</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- BEGIN PAGE CONTENT-->
<div id="page">
<?php ob_start(); ?>
<div>
<?php
if (isset($s['err'])) {
?>
  <center><h2><?php echo $s['err_con']; ?></h2></center>
<?php
} else {
    $ap = assets('images/logo1.png');
?>
<style type="text/css" scoped>
    .p-right {
        text-align: right;
    }

    * {overflow: visible;}
    @media screen {
        .tinker {
            display: none;
        }

        .earth {
            display: none;
        }

    }

    @media print {
        .tinker {
            top: 0px;
            page-break-before: always;
        }

        .earth {
            float: left;
        }

        .print-out {
            display: none;
        }
    }

    body {
        font-family: Tahoma;
    }

    .tinker .tinker-table {
        border: none !important;
    }

    .img-header {
        float: right;
        margin: -15px 0px 0px 1000px;
    }

    .table-data table, tr, th, td {
        font-size: 10px;
        border-collapse: collapse;
        padding: 5px;
        border: 1px solid black;
        page-break-inside: avoid !important;
    }

    .table-data h1 {
        text-align: center;
        font-size: 12px;
    }

    .table-data .first-blood {
        page-break-before: avoid;
        text-align: center;
        font-size: 12px;
    }

    .table-data h2 {
        font-size: 10px;
    }

    .vol-header h1 {
        text-align: center;
        font-size: 12px;
    }

</style>

<div class="tinker laser">
  <?php echo $s['wk_name']; ?> <br />
  <?php echo $s['kkks_name']; ?>
  <img class="img-header" src="<?php echo $ap; ?>" width="100" height="auto" />
  <hr style="width: 100%;" />
</div>

<div class="table-data">
  <h1 class="first-blood">A1 - Resource Inventory</h1>
  <table>
    <tr>
      <th>No.</th>
      <th>Basin</th>
      <th>Play</th>
      <th>Lead</th>
      <th>Drillable Prospect</th>
      <th>Post Drill Prospect</th>
      <th>Discovery Prospect</th>
    </tr>
<?php
    foreach ($s['a1'] as $key => $val) {
        $a1data = [
            'play' => $val['play'],
            'lead' => $val['lead'],
            'drillable' => $val['drillable'],
            'postdrill' => $val['postdrill'],
            'discovery' => $val['discovery'],
        ];
        $result = massNumberFormat($a1data);
        $val = array_merge($val, $result);
?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td class="p-right"><?php echo $val['play']; ?></td>
      <td class="p-right"><?php echo $val['lead']; ?></td>
      <td class="p-right"><?php echo $val['drillable']; ?></td>
      <td class="p-right"><?php echo $val['postdrill']; ?></td>
      <td class="p-right"><?php echo $val['discovery']; ?></td>
    </tr>

<?php } ?>
  </table>
</div>

<div class="earth">
    <p>Authorized by</p> <br />
    <p>.............</p>
</div>

<?php
    if ($s['data_avail']['well'] == 1) {
?>
<div class="table-data">
<div class="tinker laser">
  <?php echo $s['wk_name']; ?> <br />
  <?php echo $s['kkks_name']; ?>
  <img class="img-header" src="<?php echo $ap; ?>" width="100" height="auto" />
  <hr style="width: 100%;" />
</div>
  <h1>A2 - Well Recapitulation</h1>
  <table>
    <tr>
      <th>No.</th>
      <th>Well Name</th>
      <th>Prospect Stage</th>
      <th>Latitude</th>
      <th>Longitude</th>
      <th>Well Type</th>
      <th>Well Result</th>
      <th>Well Formation</th>
      <th>Completed Year</th>
    </tr>
<?php
    foreach ($s['a2'] as $key => $val) {
?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['wl_name']; ?></td>
      <td><?php echo $val['prospect_type']; ?></td>
      <td><?php echo $val['wl_latitude']; ?></td>
      <td><?php echo $val['wl_longitude']; ?></td>
      <td><?php echo $val['wl_type']; ?></td>
      <td><?php echo $val['wl_result']; ?></td>
      <td><?php echo $val['wl_formation']; ?></td>
      <td><?php echo $val['wl_year']; ?></td>
    </tr>

<?php } ?>
  </table>
<div class="earth">
    <p>Authorized by</p> <br />
    <p>.............</p>
</div>
  <p style="page-break-after: always;"></p>
</div>
<?php } ?>

<div class="vol-header">
<div class="tinker laser">
  <?php echo $s['wk_name']; ?> <br />
  <?php echo $s['kkks_name']; ?>
  <img class="img-header" src="<?php echo $ap; ?>" width="100" height="auto" />
  <hr style="width: 100%;" />
</div>
  <h1>A3 - Volumetric Recapitulation<h1>
</div>

<div class="table-data">
  <h2>Play</h2>
  <table>
    <tr>
      <th>No.</th>
      <th>Basin</th>
      <th>Play Name</th>
      <th>Source Rock</th>
      <th>Reservoir</th>
      <th>Trap</th>
      <th>Dynamic</th>
      <th>GCF (&#37;)</th>
    </tr>
<?php
    foreach ($s['a3']['play'] as $key => $val) {

        /**
         * Key for GCF changed to gcf_*
         */
        $a3play = [
            'gcf_sr' => $val['gcf_val']['sr'],
            'gcf_re' => $val['gcf_val']['re'],
            'gcf_tr' => $val['gcf_val']['tr'],
            'gcf_dn' => $val['gcf_val']['dn'],
            'gcf_rda' => $val['gcf_val']['rda'],
        ];
        $result = massNumberFormat($a3play);
?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $val['play_name']; ?></td>
      <td class="p-right"><?php echo $result['gcf_sr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_re']; ?></td>
      <td class="p-right"><?php echo $result['gcf_tr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_dn']; ?></td>
      <td class="p-right"><?php echo $result['gcf_rda']; ?></td>
    </tr>
<?php } ?>
  </table>
</div>

<?php
    if ($s['data_avail']['lead'] == 1) {
?>
<div class="table-data">
  <h2>Lead</h2>
  <table>
    <tr>
      <th rowspan="2">No.</th>
      <th rowspan="2">Basin</th>
      <th rowspan="2">Lead Name</th>
      <th colspan="3">Probability of 2D Exposure Area (Acre)</th>
      <th rowspan="2">Source Rock</th>
      <th rowspan="2">Reservoir</th>
      <th rowspan="2">Trap</th>
      <th rowspan="2">Dynamic</th>
      <th rowspan="2">GCF (&#37;)</th>
    </tr>
    <tr>
      <th>Low Estimate</th>
      <th>Best Estimate</th>
      <th>High Estimate</th>
    </tr>
<?php
    foreach ($s['a3']['lead'] as $key => $val) {
        $a3lead = [
            'low' => $val['low'],
            'best' => $val['best'],
            'high' => $val['high'],
            'gcf_sr' => $val['gcf_val']['sr'],
            'gcf_re' => $val['gcf_val']['re'],
            'gcf_tr' => $val['gcf_val']['tr'],
            'gcf_dn' => $val['gcf_val']['dn'],
            'gcf_rda' => $val['gcf_val']['rda'],
        ];
        $result = massNumberFormat($a3lead);
?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $val['lead_name']; ?></td>
      <td class="p-right"><?php echo $val['low']; ?></td>
      <td class="p-right"><?php echo $val['best']; ?></td>
      <td class="p-right"><?php echo $val['high']; ?></td>
      <td class="p-right"><?php echo $result['gcf_sr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_re']; ?></td>
      <td class="p-right"><?php echo $result['gcf_tr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_dn']; ?></td>
      <td class="p-right"><?php echo $result['gcf_rda']; ?></td>
    </td>

<?php } ?>
  </table>
</div>
<?php } ?>

<?php
    if ($s['data_avail']['drillable'] == 1) {
?>
<div class="table-data">
  <h2>Drillable Prospect</h2>
  <table>
    <tr>
      <th rowspan="2">No.</th>
      <th rowspan="2">Basin</th>
      <th rowspan="2">Prospect Name</th>
      <th colspan="3">Areal Closure (Acre)</th>
      <th colspan="3">Net Pay Thickness (Feet)</th>
      <th colspan="3">Porosity (&#37;)</th>
      <th colspan="3">1-S<sub>w</sub> (&#37;)</th>
      <th colspan="3">Oil Case<br/>OOIP (MMBO)</th>
      <th colspan="3">Gas Case<br/>OGIP (BCF)</th>
      <th rowspan="2">Source Rock</th>
      <th rowspan="2">Reservoir</th>
      <th rowspan="2">Trap</th>
      <th rowspan="2">Dynamic</th>
      <th rowspan="2">GCF (&#37;)</th>
    </tr>
    <tr>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
    </tr>
<?php
    /**
     * A3 Drillable, data manipulation.
     */
    foreach ($s['a3']['drillable'] as $key => $val) {
        $a3drillable = [
            'area_p90' => $val['area_p90'],
            'area_p50' => $val['area_p50'],
            'area_p10' => $val['area_p10'],
            'net_p90' => $val['net_p90'],
            'net_p50' => $val['net_p50'],
            'net_p10' => $val['net_p10'],
            'por_p90' => $val['por_p90'],
            'por_p50' => $val['por_p50'],
            'por_p10' => $val['por_p10'],
            'sat_p90' => $val['sat_p90'],
            'sat_p50' => $val['sat_p50'],
            'sat_p10' => $val['sat_p10'],
            'oil_p90' => $val['oil_p90'],
            'oil_p50' => $val['oil_p50'],
            'oil_p10' => $val['oil_p10'],
            'gas_p90' => $val['gas_p90'],
            'gas_p50' => $val['gas_p50'],
            'gas_p10' => $val['gas_p10'],
            'gcf_sr' => $val['gcf_val']['sr'],
            'gcf_re' => $val['gcf_val']['re'],
            'gcf_tr' => $val['gcf_val']['tr'],
            'gcf_dn' => $val['gcf_val']['dn'],
            'gcf_rda' => $val['gcf_val']['rda'],
        ];
        $result = massNumberFormat($a3drillable);
        $val = array_merge($val, $result);
?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $val['prospect_name']; ?></td>
      <td class="p-right"><?php echo $val['area_p90']; ?></td>
      <td class="p-right"><?php echo $val['area_p50']; ?></td>
      <td class="p-right"><?php echo $val['area_p10']; ?></td>
      <td class="p-right"><?php echo $val['net_p90']; ?></td>
      <td class="p-right"><?php echo $val['net_p50']; ?></td>
      <td class="p-right"><?php echo $val['net_p10']; ?></td>
      <td class="p-right"><?php echo $val['por_p90']; ?></td>
      <td class="p-right"><?php echo $val['por_p50']; ?></td>
      <td class="p-right"><?php echo $val['por_p10']; ?></td>
      <td class="p-right"><?php echo $val['sat_p90']; ?></td>
      <td class="p-right"><?php echo $val['sat_p50']; ?></td>
      <td class="p-right"><?php echo $val['sat_p10']; ?></td>
      <td class="p-right"><?php echo $val['oil_p90']; ?></td>
      <td class="p-right"><?php echo $val['oil_p50']; ?></td>
      <td class="p-right"><?php echo $val['oil_p10']; ?></td>
      <td class="p-right"><?php echo $val['gas_p90']; ?></td>
      <td class="p-right"><?php echo $val['gas_p50']; ?></td>
      <td class="p-right"><?php echo $val['gas_p10']; ?></td>
      <td class="p-right"><?php echo $result['gcf_sr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_re']; ?></td>
      <td class="p-right"><?php echo $result['gcf_tr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_dn']; ?></td>
      <td class="p-right"><?php echo $result['gcf_rda']; ?></td>
    </tr>
<?php } ?>
  </table>
</div>
<?php } ?>

<?php
    if ($s['data_avail']['postdrill'] == 1) {
?>
<div class="table-data">
  <h2>Post Drill Prospect</h2>
  <table>
    <tr>
      <th rowspan="2">No.</th>
      <th rowspan="2">Basin</th>
      <th rowspan="2">Prospect Name</th>
      <th colspan="3">Areal Closure (Acre)</th>
      <th colspan="3">Net Pay Thickness (Feet)</th>
      <th colspan="3">Porosity (&#37;)</th>
      <th colspan="3">1-S<sub>w</sub> (&#37;)</th>
      <th rowspan="2">Source Rock</th>
      <th rowspan="2">Reservoir</th>
      <th rowspan="2">Trap</th>
      <th rowspan="2">Dynamic</th>
      <th rowspan="2">GCF (&#37;)</th>
    </tr>
    <tr>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
    </tr>
<?php
    /**
     * A3 Postdrill, attribute.
     */
    foreach ($s['a3']['postdrill'] as $key => $val) {
        $a3postdrill = [
            'area_p90' => $val['area_p90'],
            'area_p50' => $val['area_p50'],
            'area_p10' => $val['area_p10'],
            'net_p90' => $val['net_p90'],
            'net_p50' => $val['net_p50'],
            'net_p10' => $val['net_p10'],
            'por_p90' => $val['por_p90'],
            'por_p50' => $val['por_p50'],
            'por_p10' => $val['por_p10'],
            'sat_p90' => $val['sat_p90'],
            'sat_p50' => $val['sat_p50'],
            'sat_p10' => $val['sat_p10'],
            'gcf_sr' => $val['gcf_val']['sr'],
            'gcf_re' => $val['gcf_val']['re'],
            'gcf_tr' => $val['gcf_val']['tr'],
            'gcf_dn' => $val['gcf_val']['dn'],
            'gcf_rda' => $val['gcf_val']['rda'],
        ];
        $result = massNumberFormat($a3postdrill);
        $val = array_merge($val, $result);

?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $val['prospect_name']; ?></td>
      <td class="p-right"><?php echo $val['area_p90']; ?></td>
      <td class="p-right"><?php echo $val['area_p50']; ?></td>
      <td class="p-right"><?php echo $val['area_p10']; ?></td>
      <td class="p-right"><?php echo $val['net_p90']; ?></td>
      <td class="p-right"><?php echo $val['net_p50']; ?></td>
      <td class="p-right"><?php echo $val['net_p10']; ?></td>
      <td class="p-right"><?php echo $val['por_p90']; ?></td>
      <td class="p-right"><?php echo $val['por_p50']; ?></td>
      <td class="p-right"><?php echo $val['por_p10']; ?></td>
      <td class="p-right"><?php echo $val['sat_p90']; ?></td>
      <td class="p-right"><?php echo $val['sat_p50']; ?></td>
      <td class="p-right"><?php echo $val['sat_p10']; ?></td>
      <td class="p-right"><?php echo $result['gcf_sr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_re']; ?></td>
      <td class="p-right"><?php echo $result['gcf_tr']; ?></td>
      <td class="p-right"><?php echo $result['gcf_dn']; ?></td>
      <td class="p-right"><?php echo $result['gcf_rda']; ?></td>
    </tr>
<?php } ?>
  </table>
</div>
<?php } ?>
<br />
<?php
    if ($s['data_avail']['postdrill'] == 1) {
?>
<div class="table-data">
  <table>
    <tr>
      <th rowspan="2">No.</th>
      <th rowspan="2">Basin</th>
      <th rowspan="2">Prospect Name</th>
      <th colspan="3">OOIP (MMBO)</th>
      <th colspan="3">B<sub>oi</sub><br/>(RBO/STB)</th>
      <th colspan="3">STOIP (MMSTB)</th>
      <th colspan="3">OGIP (BCF)</th>
      <th colspan="3">B<sub>gi</sub><br/>(RCF/SCF)</th>
      <th colspan="3">IGIP (BSCF)</th>
    </tr>
    <tr>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
    </tr>
<?php
    /**
     * A3 Postdrill, oil gas.
     */
    foreach ($s['a3']['postdrill'] as $key => $val) {
        $a3postdrill = [
            'mmbo_p90' => $val['mmbo_p90'],
            'mmbo_p50' => $val['mmbo_p50'],
            'mmbo_p10' => $val['mmbo_p10'],
            'boi_p90' => $val['boi_p90'],
            'boi_p50' => $val['boi_p50'],
            'boi_p10' => $val['boi_p10'],
            'mmstb_p90' => $val['mmstb_p90'],
            'mmstb_p50' => $val['mmstb_p50'],
            'mmstb_p10' => $val['mmstb_p10'],
            'bcf_p90' => $val['bcf_p90'],
            'bcf_p50' => $val['bcf_p50'],
            'bcf_p10' => $val['bcf_p10'],
            'bgi_p90' => $val['bgi_p90'],
            'bgi_p50' => $val['bgi_p50'],
            'bgi_p10' => $val['bgi_p10'],
            'bscf_p90' => $val['bscf_p90'],
            'bscf_p50' => $val['bscf_p50'],
            'bscf_p10' => $val['bscf_p10'],
        ];
        $result = massNumberFormat($a3postdrill);
        $val = array_merge($val, $result);
?>
    <tr>
      <td><?php echo $key+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $val['prospect_name']; ?></td>
      <td class="p-right"><?php echo $val['mmbo_p90']; ?></td>
      <td class="p-right"><?php echo $val['mmbo_p50']; ?></td>
      <td class="p-right"><?php echo $val['mmbo_p10']; ?></td>
      <td class="p-right"><?php echo $val['boi_p90']; ?></td>
      <td class="p-right"><?php echo $val['boi_p50']; ?></td>
      <td class="p-right"><?php echo $val['boi_p10']; ?></td>
      <td class="p-right"><?php echo $val['mmstb_p90']; ?></td>
      <td class="p-right"><?php echo $val['mmstb_p50']; ?></td>
      <td class="p-right"><?php echo $val['mmstb_p10']; ?></td>
      <td class="p-right"><?php echo $val['bcf_p90']; ?></td>
      <td class="p-right"><?php echo $val['bcf_p50']; ?></td>
      <td class="p-right"><?php echo $val['bcf_p10']; ?></td>
      <td class="p-right"><?php echo $val['bgi_p90']; ?></td>
      <td class="p-right"><?php echo $val['bgi_p50']; ?></td>
      <td class="p-right"><?php echo $val['bgi_p10']; ?></td>
      <td class="p-right"><?php echo $val['bscf_p90']; ?></td>
      <td class="p-right"><?php echo $val['bscf_p50']; ?></td>
      <td class="p-right"><?php echo $val['bscf_p10']; ?></td>
    </tr>
<?php } ?>
  </table>
</div>
<?php } ?>

<?php
    if ($s['data_avail']['discovery'] == 1) {
?>
<div class="table-data">
  <h2>Discovery Prospect</h2>
  <table>
    <tr>
      <th rowspan="2">No.</th>
      <th rowspan="2">Basin</th>
      <th rowspan="2">Prospect Name with Reservoir Zone</th>
      <th colspan="3">Areal Closure (Acre)</th>
      <th colspan="3">Net Pay Thickness (Feet)</th>
      <th colspan="3">Porosity (&#37;)</th>
      <th colspan="3">1-S<sub>w</sub> (&#37;)</th>
      <th rowspan="2">B<sub>oi</sub><br/>(RBO/STB)</th>
      <th rowspan="2">B<sub>gi</sub><br/>(RCF/SCF)</th>
      <th rowspan="2">Radius of Investigation (Re)<br/>(Feet)</th>
      <th rowspan="2">Well Name</th>
    </tr>
    <tr>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
    </tr>
<?php
    /**
     * A3 Discovery, attribute.
     * Two steps to manipulate, well first then well zone.
     */
    $dn = 0;
    foreach ($s['a3']['discovery'] as $key => $val) {
        foreach ($val['well'] as $wkey => $wval) {
            $znum = 1;
            $a3well = [
                'area_p90' => $val['area']['p90'],
                'area_p50' => $val['area']['p50'],
                'area_p10' => $val['area']['p10'],
                'net_p90' => $val['net']['p90'],
                'net_p50' => $val['net']['p50'],
                'net_p10' => $val['net']['p10'],
            ];
            $well = massNumberFormat($a3well);

            foreach ($wval['zone'] as $zkey => $zval) {
                $a3zone = [
                    'por_p90' => $zval['por_p90'],
                    'por_p50' => $zval['por_p50'],
                    'por_p10' => $zval['por_p10'],
                    'sat_p90' => $zval['sat_p90'],
                    'sat_p50' => $zval['sat_p50'],
                    'sat_p10' => $zval['sat_p10'],
                    'boi_p50' => $zval['boi_p50'],
                    'bgi_p50' => $zval['bgi_p50'],
                    'radius_re' => $zval['radius_re'],
                ];
                $zone = massNumberFormat($a3zone);

                $stnm = "{$val['structure_name']} @ {$zval['zone_name']}";
                $znum++;
?>
    <tr>
      <td><?php echo $dn+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $stnm; ?></td>
      <td class="p-right"><?php echo $well['area_p90']; ?></td>
      <td class="p-right"><?php echo $well['area_p50']; ?></td>
      <td class="p-right"><?php echo $well['area_p10']; ?></td>
      <td class="p-right"><?php echo $well['net_p90']; ?></td>
      <td class="p-right"><?php echo $well['net_p50']; ?></td>
      <td class="p-right"><?php echo $well['net_p10']; ?></td>
      <td class="p-right"><?php echo $zone['por_p90']; ?></td>
      <td class="p-right"><?php echo $zone['por_p50']; ?></td>
      <td class="p-right"><?php echo $zone['por_p10']; ?></td>
      <td class="p-right"><?php echo $zone['sat_p90']; ?></td>
      <td class="p-right"><?php echo $zone['sat_p50']; ?></td>
      <td class="p-right"><?php echo $zone['sat_p10']; ?></td>
      <td class="p-right"><?php echo $zone['boi_p50']; ?></td>
      <td class="p-right"><?php echo $zone['bgi_p50']; ?></td>
      <td class="p-right"><?php echo $zone['radius_re']; ?></td>
      <td><?php echo $wval['well_name']; ?></td>
    </tr>
<?php
    $dn++;
        }
    }
}
?>
  </table>
  <br />
  <table>
    <tr>
      <th rowspan="3">No.</th>
      <th rowspan="3">Basin</th>
      <th rowspan="3">Prospect Name</th>
      <th colspan="7">Volumetric Oil In Place</th>
      <th colspan="2" rowspan="2">Volumetric Oil for Well<br/>(MMSTB)</th>
      <th colspan="3" rowspan="2">Volumetric Structural Oil Remaining<br/>(MMSTB)</th>
    </tr>
    <tr>
      <th colspan="3">OOIP (MMBO)</th>
      <th rowspan="2">Average B<sub>oi</sub><br/>(RBO/STB)</th>
      <th colspan="3">STOIP (MMSTB)</th>
    </tr>
    <tr>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P1 (Total)</th>
      <th>2P (Total)</th>
      <th>CR1</th>
      <th>CR2</th>
      <th>CR3</th>
    </tr>
<?php
    /**
     * A3 Discovery, Oil.
     */
    $dn = 0;
    foreach ($s['a3']['discovery'] as $key => $val) {
        $wnum = 1;
        foreach ($val['well'] as $wkey => $wval) {
            $stnm = "{$val['structure_name']} @ {$wval['formation_name']}";
            $wnum++;
            $a3welloil = [
                'ooip_p90' => $wval['ooip_p90'],
                'ooip_p50' => $wval['ooip_p50'],
                'ooip_p10' => $wval['ooip_p10'],
                'boi' => $wval['avg_bo'],
                'stoip_p90' => $wval['stoip_p90'],
                'stoip_p50' => $wval['stoip_p50'],
                'stoip_p10' => $wval['stoip_p10'],
                'p1_oil' => $wval['p1_oil'],
                '2p_oil' => $wval['2p_oil'],
                'c1_oil' => $wval['c1_oil'],
                'c2_oil' => $wval['c2_oil'],
                'c3_oil' => $wval['c3_oil'],
            ];
            $a3welloil = massNumberFormat($a3welloil);

?>
    <tr>
      <td><?php echo $dn+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $stnm; ?></td>
      <td class="p-right"><?php echo $a3welloil['ooip_p90']; ?></td>
      <td class="p-right"><?php echo $a3welloil['ooip_p50']; ?></td>
      <td class="p-right"><?php echo $a3welloil['ooip_p10']; ?></td>
      <td class="p-right"><?php echo $a3welloil['boi']; ?></td>
      <td class="p-right"><?php echo $a3welloil['stoip_p90']; ?></td>
      <td class="p-right"><?php echo $a3welloil['stoip_p50']; ?></td>
      <td class="p-right"><?php echo $a3welloil['stoip_p10']; ?></td>
      <td class="p-right"><?php echo $a3welloil['p1_oil']; ?></td>
      <td class="p-right"><?php echo $a3welloil['2p_oil']; ?></td>
      <td class="p-right"><?php echo $a3welloil['c1_oil']; ?></td>
      <td class="p-right"><?php echo $a3welloil['c2_oil']; ?></td>
      <td class="p-right"><?php echo $a3welloil['c3_oil']; ?></td>
    </tr>
<?php
    $dn++;
        }
    }
?>
  </table>
  <br />
  <table>
    <tr>
      <th rowspan="3">No.</th>
      <th rowspan="3">Basin</th>
      <th rowspan="3">Prospect Name</th>
      <th colspan="7">Volumetric Gas In Place</th>
      <th colspan="2" rowspan="2">Volumetric Gas for Well<br/>(BSCF)</th>
      <th colspan="3" rowspan="2">Volumetric Structural Gas Remaining<br/>(BSCF)</th>
    </tr>
    <tr>
      <th colspan="3">OGIP (BCF)</th>
      <th rowspan="2">Average B<sub>gi</sub> <br/>(RCF/SCF)</th>
      <th colspan="3">IGIP (BSCF)</th>
    </tr>
    <tr>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P90</th>
      <th>P50</th>
      <th>P10</th>
      <th>P1 (Total)</th>
      <th>2P (Total)</th>
      <th>CR1</th>
      <th>CR2</th>
      <th>CR3</th>
    </tr>
<?php
    /**
     * A3 Discovery, Gas.
     */
    $dn = 0;
    foreach ($s['a3']['discovery'] as $key => $val) {
        $wnum = 1;
        foreach ($val['well'] as $wkey => $wval) {
            $stnm = "{$val['structure_name']} @ {$wval['formation_name']}";
            $wnum++;
            $a3wellgas = [
                'ogip_p90' => $wval['ogip_p90'],
                'ogip_p50' => $wval['ogip_p50'],
                'ogip_p10' => $wval['ogip_p10'],
                'bgi' => $wval['avg_bg'],
                'igip_p90' => $wval['igip_p90'],
                'igip_p50' => $wval['igip_p50'],
                'igip_p10' => $wval['igip_p10'],
                'p1_gas' => $wval['p1_gas'],
                '2p_gas' => $wval['2p_gas'],
                'c1_gas' => $wval['c1_gas'],
                'c2_gas' => $wval['c2_gas'],
                'c3_gas' => $wval['c3_gas'],
            ];
            $a3wellgas = massNumberFormat($a3wellgas);
?>
    <tr>
      <td><?php echo $dn+1; ?></td>
      <td><?php echo $val['basin_name']; ?></td>
      <td><?php echo $stnm; ?></td>
      <td class="p-right"><?php echo $a3wellgas['ogip_p90']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['ogip_p50']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['ogip_p10']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['bgi']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['igip_p90']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['igip_p50']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['igip_p10']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['p1_gas']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['2p_gas']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['c1_gas']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['c2_gas']; ?></td>
      <td class="p-right"><?php echo $a3wellgas['c3_gas']; ?></td>
    </tr>
<?php
    $dn++;
        }
    }
?>
  </table>
</div>
<?php } ?>
<div class="earth">
    <p>Authorized by</p><br />
    <p>.............</p>
</div>
</div>
<br />
<div class="print-out">
<center>
<form action="" method="post">
    <input type="submit" name="print-pdf" value="Print to PDF">
</form>
</center>
</div>

<?php
    }
    $_SESSION['pdfdata'] = ob_get_contents();
    ob_end_flush();
?>
</div>
</div>
</div>