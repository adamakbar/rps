<?php
/* @var $this SiteController */
$this->pageTitle = 'Discovery';
$this->breadcrumbs = array (
		'Discovery' 
);
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title">DISCOVERY PROSPECT</h3>
			<ul class="breadcrumb">
				<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i
						class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
				<li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
				<li><a href="#"><strong>Discovery</strong></a><span
					class="divider-last">&nbsp;</span></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
				<div id="_list-data-discovery" class="widget">
					<div class="widget-title">
						<h4>
							<i class="icon-table"></i> LIST DATA DISCOVERY
						</h4>
						<span class="tools"><a href="javascript:;"
							class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<!-- <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Prospect Discovery Name</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>SANDSTONE.DELTA SLOPE.TAF-Talang Akar/Upper Talang Akar/Pre-Talangakar.OLIGOCENE.STRUCTURAL TECTONIC EXTENSIONAL</td>
                                <td>14/06/2013</td>
                                <td><span class="label label-success">Original</span></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>REEF CARBONATE.REEFAL.BRF-Pre-Baturaja/Baturaja.MIOCENE.STRATIGRAPHIC DEPOSITIONAL REEF</td>
                                <td>23/09/2013</td>
                                <td><span class="label label-warning">Update</span></td>
                            <tr>
                                <td>3</td>
                                <td>PLATFORM CARBONATE.PLATFORM MARGIN.KJG-Kujung I/Kujung/Kujung 1 Reef/Kujung I of Kujung Fm.MIOCENE.STRUCTURAL TECTONIC COMPRESSIONAL INVERTED</td>
                                <td>30/12/2013</td>
                                <td><span class="label label-warning">Update</span></td>
                            </tr>
                            </tbody>
                        </table> -->
                        <?php $this->widget ( 'zii.widgets.grid.CGridView', array (
                        	'id'=>'discovery-grid',
							'dataProvider'=>$model->searchRelatedDiscovery(),
                        	'filter'=>$model,
                        	'htmlOptions'=>array('class'=>'table table-striped'),
							'columns' => array (
								array (
									'header'=>'No.',
									'name' => 'No.',
									'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
								),
								array(
									'header'=>'Basin',
                                    'filter'=>CHtml::activeTextField($model, 'basin_name'),
									'value'=>'$data->plays->basin->basin_name',
								),
								array (
									'header' => 'Prospect Name / Closure Name',
                                    'filter'=>CHtml::activeTextField($model, 'discovery_name'),
// 									'value'=>'CHtml::link($data->prospects->structure_name, array("GetPreviousDataDiscovery", "id"=>$data->prospects->prospect_id), array("class"=>"ajaxupdate"))',
									'value'=>'$data->prospects->getProspectName2($data->prospects->prospect_id)',
									'type'=>'raw',
								),
								array(
									'header'=>'Status',
									'value'=>'$data->prospects->prospect_submit_status',
								),
								array (
									'header'=>'Actions',
									'template' => '{edit} {delete }',
									'class' => 'CButtonColumn',
									'buttons' => array (
										'edit' => array (
											'options' => array (
												'class' => 'icon-edit' 
											),
											'url' => 'Yii::app()->createUrl("/kkks/updatediscovery", array("id"=>$data->prospects->prospect_id))' 
										),
										'delete '=>array(
											'options'=>array(
												'class'=>'icon-remove'
											),
											'url'=>'Yii::app()->createUrl("/kkks/deletediscovery", array("id"=>$data->prospects->prospect_id))',
											'click'=>"function() {
												$.fn.yiiGridView.update('discovery-grid', {  //change my-grid to your grid's name
											        type:'POST',
											        url:$(this).attr('href'),
											        success:function(data) {
											              $.fn.yiiGridView.update('discovery-grid'); //change my-grid to your grid's name
											        }
											    })
										    	return false;
										  	}
											",
										),
									) 
								) 
							) 
						));
						?>
                    </div>
				</div>
			</div>
		</div>       
        
        <?php $form = $this->beginWidget ( 'CActiveForm', array (
			'id' => 'creatediscovery-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnChange'=>false,
			),
        	'enableAjaxValidation'=>true,
//         	'action'=>$this->createUrl('/Kkks/createlead'),
		));
		?>
        <?php //echo $form->errorSummary(array($mdlProspect, $mdlDiscovery, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));?>
        <?php echo $form->hiddenField($mdlProspect, 'prospect_update_from', array('id'=>'prospect_update_from'));?>
		<?php echo $form->hiddenField($mdlProspect, 'prospect_submit_revision', array('id'=>'prospect_submit_revision'));?>
        <div id="pesan"></div>
        
        <div class="row-fluid">
			<div class="span12">
				<div id="1_well_discovery" class="widget">
					<div class="widget-title">
						<h4>
							<i class="icon-list"></i> WELL DATA AVAILABILITY
						</h4>
						<span class="tools"><a href="javascript:;"
							class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal"> <!-- Notification -->
							<div class="control-group">
                        		<label class="control-label">Well :</label>
                                <div class="controls wajib">
                                    <!-- <input id="w" class="span3" type="text" style="text-align: center;"/> -->
                                    <?php echo $form->textField($mdlProspect, 'well_name', array('id'=>"well_name", 'class'=>"span3", 'style'=>"text-align: center;"));?>
                                    <br><br>
                                    <div id="Prospect_well_name_em_" class="errorMessage" style="display:none">
                                    </div>
                                </div>
                        	</div>
						</span>
					</div>
				</div>
			</div>
		</div>
		
        <div class="row-fluid">
			<div class="span12">
				<!-- BEGIN GENERAL DATA-->
				<div class="widget">
					<div class="widget-title">
						<h4>
							<i class="icon-file"></i> GENERAL DATA
						</h4>
						<span class="tools"><a href="javascript:;"
							class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal">
							<div class="accordion" id="accordion1a">
								<!-- Begin Data Prospect Administration Data -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gen1"> <strong>Prospect Administration Data</strong>
										</a>
									</div>
									<div id="col_gen1" class="accordion-body collapse">
										<div class="accordion-inner">
                                          <div class="alert alert-success">
                                            Structure name must be unique and meaningful, name that contain "Lead",
                                            "Drillable", "Discovery" or other
                                            general term is not permitted.
                                          </div>
											<div class="control-group">
												<label class="control-label">Play Name :</label>
												<div class="controls wajib">
													<?php 
//                                                 		$mdlPlaySearch = Play::model();
// 	                                                	$model = new Play('search');
// 	                                                	$model->unsetAttributes();
	                                                	
// 	                                                	$criteria = new CDbCriteria;
// 	                                                	$criteria->select="max(b.play_submit_date)";
// 	                                                	$criteria->alias = 'b';
// 	                                                	$criteria->condition="b.play_update_from = t.play_update_from";
// 	                                                	$subQuery=$model->getCommandBuilder()->createFindCommand($model->getTableSchema(),$criteria)->getText();
	                                                	
// 	                                                	$mainCriteria = new CDbCriteria;
// 	                                                	$mainCriteria->with = array('gcfs');
// 	                                                	$mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
// 	                                                	$mainCriteria->addCondition('play_submit_date = (' . $subQuery . ')', 'AND');
// 	                                                	$mainCriteria->group = 'play_update_from';
                                                	
                                                	?>
													<!-- <input id="play_in" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php //echo $form->dropDownList($mdlProspect, 'play_id', CHtml::listData( Play::model()->findAll($mainCriteria), 'play_id', 'gcfs.playName' ), array('empty'=>'-- Select --', 'style'=>'text-align: center;', 'id'=>'plyName', 'class'=>'span5'));?>
                                                    <?php echo $form->textField($mdlProspect, 'play_id', array('style'=>'text-align: center; position: relative; display: inline-block;', 'id'=>'plyName', 'class'=>'span6'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'play_id');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Structure Name :</label>
												<div class="controls wajib">
													<table>
                                                		<tr>
                                                			<td>
                                                				<!-- <input id="d2" type="text" class="span6 popovers" data-trigger="hover" data-container="body" data-content="Postdrill Prospect should already in one structure and Postdrill Prospect name should unique within Working Area." data-original-title="Prospect Name"/> -->
			                                                    <?php echo $form->textField($mdlProspect, 'structure_name', array('id'=>'structure_name', 'class'=>'span5 popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'', 'data-original-title'=>'', 'style'=>'width: 375px;'));?>
			                                                    <?php echo $form->error($mdlProspect, 'structure_name');?>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
											</div>
											<div class="control-group">
                                            	<label class="control-label">Prospect Name / Closure Name :</label>
                                            	<div class="controls">
                                            		<div class="alert alert-success span12" style="width: 375px;"><span id="additional_lead_name" class="add-on"></span></div>
                                            	</div>
                                            </div>
											<div class="control-group">
												<label class="control-label">Clarified by :</label>
												<div class="controls wajib">
													<!-- <input id="_clarifiedby" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_clarified', array('id'=>'_clarifiedby', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_clarified');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Revised Prospect Name :</label>
												<div class="controls">
													<!-- <input id="_revisedprosname" class="span3" type="text"/> -->
                                                    <?php echo $form->textField($mdlDiscovery, 'dc_name_revise', array('id'=>'_revisedprosname', 'class'=>'span3'));?>
                                                    <?php echo $form->error($mdlDiscovery, 'dc_name_revise');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Prospect Name Revised Date :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="d9" type="text" class="m-wrap medium popovers" data-trigger="hover" style="max-width:172px;" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Revised Date"/> -->
                                                        <?php echo $form->textField($mdlDiscovery, 'dc_name_revise_date', array('id'=>'d9', 'class'=>'m-wrap medium popovers disable-input', 'data-trigger'=>'hover', 'style'=>'max-width:137px;', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Prospect Name Revised Date'));?>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
														<span id="cleard9" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
													</div>
													<?php echo $form->error($mdlDiscovery, 'dc_name_revise_date');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Prospect Name Initiation Date :</label>
												<div class="controls wajib">
													<div class=" input-append">
														<!-- <input id="d7" type="text" class="m-wrap medium popovers" data-trigger="hover" style="max-width:172px;" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Initiation Date"/> -->
                                                        <?php echo $form->textField($mdlProspect, 'prospect_date_initiate', array('id'=>'d7', 'class'=>'m-wrap medium popovers disable-input', 'data-trigger'=>'hover', 'style'=>'max-width:137px;', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Prospect Name Initiation Date'));?>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                        <span id="cleard7" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
													</div>
													<?php echo $form->error($mdlProspect, 'prospect_date_initiate');?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Prospect Administration Data -->
								<!-- Begin Geographical Prospect Area -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gen2_drill"> <strong>Geographical Prospect Area</strong>
										</a>
									</div>
									<div id="col_gen2_drill" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Center Latitude :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
		                                                        		<?php echo $form->textField($mdlProspect, 'center_lat_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
		                                                        		<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_degree');?>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
			                                                        	<?php echo $form->textField($mdlProspect, 'center_lat_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
			                                                        	<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_minute');?>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div>-->
			                                                        	<?php echo $form->textField($mdlProspect, 'center_lat_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
			                                                        	<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_lat_second');?>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"></span> -->
																		<?php echo $form->textField($mdlProspect, 'center_lat_direction', array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>		
																	</div>	
																	<?php echo $form->error($mdlProspect, 'center_lat_direction');?>																
																</span>
															</td>
															<td>
	                                                        	<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
	                                                        </td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Center Longitude :</label>
												<div class="controls wajib">
													<table>
														<tr>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
																	<div class=" input-append">
		                                                        		<?php echo $form->textField($mdlProspect, 'center_long_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
			                                                        	<span class="add-on">&#176;</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_degree');?>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
																	<div class=" input-append">
			                                                        	<?php echo $form->textField($mdlProspect, 'center_long_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
			                                                        	<span class="add-on">'</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_minute');?>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div> -->
																	<div class=" input-append">
			                                                        	<?php echo $form->textField($mdlProspect, 'center_long_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
			                                                        	<span class="add-on">"</span>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_second');?>
																</span>
															</td>
															<td>
																<span class="wajib">
																	<div>
																		<!-- <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on"></span> -->
																		<?php echo $form->textField($mdlProspect, 'center_long_direction', array('value'=>'E', 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
																	</div>
																	<?php echo $form->error($mdlProspect, 'center_long_direction');?>
																</span>
															</td>
															<td>
	                                                        	<div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
	                                                        </td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Geographical Prospect Area -->
								<!-- Begin Data Environment -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gen3"> <strong>Environment</strong>
										</a>
									</div>
									<div id="col_gen3" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Onshore or Offshore :</label>
												<div class="controls wajib">
													<!-- <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_shore', array('id'=>'env_onoffshore', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_shore');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Terrain :</label>
												<div class="controls wajib">
													<!-- <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_terrain', array('id'=>'env_terrain', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlProspect, 'prospect_terrain');?>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Environment -->
								<!-- Begin Data Adjacent Activity :  -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gen4"> <strong>Adjacent Activity</strong>
										</a>
									</div>
									<div id="col_gen4" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Nearby Field :</label>
												<div class="controls wajib">
													<p style="margin-bottom: 2px;">
													<!-- <input name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_near_field', array('id'=>'n_facility', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
													<?php echo $form->error($mdlProspect, 'prospect_near_field');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Nearby Infrastructure :</label>
												<div class="controls wajib">
													<p style="margin-bottom: 2px;">
													<!-- <input name="custom" type="text" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/> -->
                                                    <?php echo $form->textField($mdlProspect, 'prospect_near_infra_structure', array('id'=>'n_developmentwell', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
													<?php echo $form->error($mdlProspect, 'prospect_near_infra_structure');?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Adjacent Activity : -->
							</div>
						</span>
					</div>
				</div>
				<!-- END GENERAL DATA-->
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN OCCURANCE SURVEY DESCRIPTION-->
				<div class="widget">
					<div class="widget-title">
						<h4>
							<i class="icon-list"></i> SURVEY DATA AVAILABILITY
						</h4>
						<span class="tools"><a href="javascript:;"
							class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal"> <!-- Notification -->
							<div class="control-group">
								<div class="alert alert-success">
									<button class="close" data-dismiss="alert">×</button>
									<p>P10 = Probability to recover HC accumulation with certain volume or greater than expected is 10%</p>
                                	<p>P50 = Probability to recover HC accumulation with certain volume or greater than expected is 50%</p>
                                	<p>P90 = Probability to recover HC accumulation with certain volume or greater than expected is 90%</p>
								</div>
							</div> <!-- Notification -->
							<div class="accordion">
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlGfs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGfs','DlGfs_link','col_dav1')" /> -->
											<?php echo $form->checkBox($mdlGeological, 'is_checked', array('id'=>'DlGfs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlGfs","DlGfs_link","col_dav1")'));?>
											<a id="DlGfs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">Geological Field Survey</a>
										</div>
									</div>
									<div id="col_dav1" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year :</label>
												<div class="controls wajib">
													<!-- <input id="dav_gfs_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                	<?php echo $form->textField($mdlGeological, 'sgf_year_survey', array('id'=>'dav_gfs_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                	<?php echo $form->error($mdlGeological, 'sgf_year_survey');?>
                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Method :</label>
												<div class="controls">
													<!-- <input id="dav_gfs_surmethod" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlGeological, 'sgf_survey_method', array('id'=>'dav_gfs_surmethod', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlGeological, 'sgf_survey_method');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Total Coverage Area : </label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_gfs_tca" type="text" style="max-width:170px"/> -->
	                                                    <?php echo $form->textField($mdlGeological, 'sgf_coverage_area', array('id'=>'dav_gfs_tca', 'class'=>"number", 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">acre</span>
													</div>
													<?php echo $form->error($mdlGeological, "sgf_coverage_area");?>
												</div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Areal Closure Estimation</th>
															<th>Gross Sand Thickness</th>
															<th>Net to Gross</th>
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th><strong>P90</strong></th>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_area', array('id'=>'sgf_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p90_area');?>
																</div>
															</td>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_thickness', array('id'=>'sgf_p90_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p90_thickness');?>
																</div>
															</td>
															<td>
															<div class="wajib">
																<div class=" input-append">
																	<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p90_net', array('id'=>'sgf_p90_net', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">%</span>
																</div>
																<?php echo $form->error($mdlGeological, 'sgf_p90_net');?>
															</div>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p90_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p90_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p90_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p90_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th><strong>P50</strong></th>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_area', array('id'=>'sgf_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p50_area');?>
																</div>
															</td>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_thickness', array('id'=>'sgf_p50_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p50_thickness');?>
																</div>
															</td>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p50_net', array('id'=>'sgf_p50_net', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p50_net');?>
																</div>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p50_porosity" name="custom" -->
<!-- 																		class="span6" type="text" readonly /> <span -->
<!-- 																		class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p50_sw" name="custom" class="span6" -->
<!-- 																		type="text" readonly /> <span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p50_oil" name="custom" class="span6" -->
<!-- 																		type="text" readonly /> <span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p50_gas" name="custom" class="span6" -->
<!-- 																		type="text" readonly /> <span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th><strong>P10</strong></th>
															<td>
															<div class="wajib">
																<div class=" input-append">
																	<!-- <input id="#" name="custom" class="span6" type="text"/> -->
	                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_area', array('id'=>'sgf_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
	                                                        		<span class="add-on">acre</span>
																</div>
																<?php echo $form->error($mdlGeological, 'sgf_p10_area');?>
															</div>
															</td>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_thickness', array('id'=>'sgf_p10_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p10_thickness');?>
																</div>
															</td>
															<td>
																<div class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
		                                                        		<?php echo $form->textField($mdlGeological, 'sgf_p10_net', array('id'=>'sgf_p10_net', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
		                                                        		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlGeological, 'sgf_p10_net');?>
																</div>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p10_porosity" name="custom" -->
<!-- 																		class="span6" type="text" readonly /> <span -->
<!-- 																		class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p10_sw" name="custom" class="span6" -->
<!-- 																		type="text" readonly /> <span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p10_oil" name="custom" class="span6" -->
<!-- 																		type="text" readonly /> <span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgf_p10_gas" name="custom" class="span6" -->
<!-- 																		type="text" readonly /> <span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_gfs_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGeological, 'sgf_remark', array('id'=>'dav_gfs_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlDss2" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss2','DlDss2_link','col_dav2' )" /> -->
											<?php echo $form->checkBox($mdlSeismic2d, 'is_checked', array('id'=>'DlDss2', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlDss2","DlDss2_link","col_dav2")'));?>
											<a id="DlDss2_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2"> 2D Seismic Survey</a>
										</div>
									</div>
									<div id="col_dav2" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year:</label>
												<div class="controls wajib">
													<!-- <input id="dav_2dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                	<?php echo $form->textField($mdlSeismic2d, 's2d_year_survey', array('id'=>'dav_2dss_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                	<?php echo $form->error($mdlSeismic2d, 's2d_year_survey');?>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Number Of Vintage :</label>
												<div class="controls">
													<!-- <input id="dav_2dss_numvin" class="span3" style="text-align: center;" /> -->
                                              		<?php echo $form->textField($mdlSeismic2d, 's2d_vintage_number', array('id'=>'dav_2dss_numvin', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                              		<p style="margin-bottom: 30px; display: inline-block;"></p>
                                              		<?php echo $form->error($mdlSeismic2d, 's2d_vintage_number');?>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Total Crossline Number :</label>
												<div class="controls wajib">
													<!-- <input id="dav_2dss_tcn" type="text" class="span3"/> -->
	                                                <?php echo $form->textField($mdlSeismic2d, 's2d_total_crossline', array('id'=>'dav_2dss_tcn', 'class'=>'span3 number'));?>
	                                                <?php echo $form->error($mdlSeismic2d, 's2d_total_crossline');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Seismic Line Total Length :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_2dss_sltl" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_seismic_line', array('id'=>'dav_2dss_sltl', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">km</span>
													</div>
													<?php echo $form->error($mdlSeismic2d, 's2d_seismic_line');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Average Spacing Parallel Interval :</label>
												<div class="controls wajib">
													<div class=" input-append">
														<!-- <input id="dav_2dss_aspi" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_average_interval', array('id'=>'dav_2dss_aspi', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">km</span>
													</div>
													<?php echo $form->error($mdlSeismic2d, 's2d_average_interval');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Record Length :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_2dss_rl1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_record_length_ms', array('id'=>'dav_2dss_rl1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ms</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_record_length_ms');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_2dss_rl2" type="text" style="max-width:170px; margin-left:24px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_record_length_ft', array('id'=>'dav_2dss_rl2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_record_length_ft');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Year Of Latest Processing :</label>
												<div class="controls">
													<!-- <input id="dav_2dss_yearlatest" class="span3" type="text"/> -->
	                                                <?php echo $form->textField($mdlSeismic2d, 's2d_year_late_process', array('id'=>'dav_2dss_yearlatest', 'class'=>'span3 number-comma'));?>
	                                                <?php echo $form->error($mdlSeismic2d, 's2d_year_late_process');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Latest Processing Method :</label>
												<div class="controls">
													<!-- <input id="dav_2dss_lpm" class="span3" style="text-align: center;" /> -->
	                                                <?php echo $form->textField($mdlSeismic2d, 's2d_late_method', array('id'=>'dav_2dss_lpm', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic2d, 's2d_late_method');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Seismic Image Quality :</label>
												<div class="controls">
													<!-- <input id="dav_2dss_siq" class="span3" style="text-align: center;" /> -->
	                                                <?php echo $form->textField($mdlSeismic2d, 's2d_img_quality', array('id'=>'dav_2dss_siq', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic2d, 's2d_img_quality');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Top Reservoir Target Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<span class="add-on">MD</span>
																		<!-- <input id="dav_2dss_trtd1" type="text" style="max-width:157px"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_top_depth_ft', array('id'=>'dav_2dss_trtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_top_depth_ft');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_2dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_top_depth_ms', array('id'=>'dav_2dss_trtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
					                                                    <span class="add-on">ms</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_top_depth_ms');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Bottom Reservoir Target Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-prepend input-append">
																		<span class="add-on">MD</span>
																		<!-- <input id="dav_2dss_brtd1" type="text" style="max-width:157px"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_bot_depth_ft', array('id'=>'dav_2dss_brtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_bot_depth_ft');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_2dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_bot_depth_ms', array('id'=>'dav_2dss_brtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
					                                                    <span class="add-on">ms</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_bot_depth_ms');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Spill Point :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_2dss_dcsp" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_spill', array('id'=>'dav_2dss_dcsp', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlSeismic2d, 's2d_depth_spill');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Estimate OWC/GWC :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-append">
																		<!-- <input id="dav_2dss_owc1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate', array('id'=>'dav_2dss_owc1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_depth_estimate');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dlosd2m2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/> -->
                                                						<?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate_analog', array('id'=>'dlosd2m2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_depth_estimate_analog');?>
																</span>
															</td>
															<td>
																<span>
			                                                		<div class="input-append">
																		<!-- <input id="dav_2dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_estimate_spill', array('id'=>'dav_2dss_owc2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
					                                                    <span class="add-on">%</span>
				                                                    </div>
				                                                    <?php echo $form->error($mdlSeismic2d, 's2d_depth_estimate_spill');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Estiamte Oil or Gas :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_2dss_oilgas1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_depth_low', array('id'=>'dav_2dss_oilgas1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                               		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_depth_low');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dlosd2n2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/> -->
                                                						<?php echo $form->textField($mdlSeismic2d, 's2d_depth_low_analog', array('id'=>'dlosd2n2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_depth_low_analog');?>
																</span>
															</td>
															<td>
																<span>
				                                                	<div class=" input-append">
																		<!-- <input id="oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/> -->
				                                                    	<?php echo $form->textField($mdlSeismic2d, 's2d_depth_low_spill', array('id'=>'oilgas2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_depth_low_spill');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Formation Thickness :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_2dss_forthickness" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_formation_thickness', array('id'=>'dav_2dss_forthickness', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                	<span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlSeismic2d, 's2d_formation_thickness');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Gross Sand or Reservoir Thickness :</label>
												<div class="controls wajib">
													<div class=" input-append">
														<!-- <input id="dav_2dss_gross_resthickness" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic2d, 's2d_gross_thickness', array('id'=>'dav_2dss_gross_resthickness', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                	<span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlSeismic2d, 's2d_gross_thickness');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Availability Net Thickness Reservoir Pay :</label>
												<div class="controls">
													<!-- <input id="dav_2dss_antrip" class="span3" type="text" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic2d, 's2d_avail_pay', array('id'=>'dav_2dss_antrip', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic2d, 's2d_avail_pay');?>
	                                            </div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Pay Zone Thickness/ Net Pay</th>
															<th>Net Pay Thickness</th>
<!-- 															<th>Net Pay to Gross Sand</th> -->
															<th>Vsh Cut-off</th>
															<th>Porosity Cut-off</th>
															<th>Saturation Cut-Off</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_thickness', array('id'=>'s2d_net_p90_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_thickness');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_net_p90_net_to_gross" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_vsh', array('id'=>'s2d_net_p90_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_vsh');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_por', array('id'=>'s2d_net_p90_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_por');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p90_satur', array('id'=>'s2d_net_p90_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p90_satur');?>
																</span>
															</td>
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_thickness', array('id'=>'s2d_net_p50_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_thickness');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_net_p50_net_to_gross" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_vsh', array('id'=>'s2d_net_p50_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_vsh');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_por', array('id'=>'s2d_net_p50_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_por');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p50_satur', array('id'=>'s2d_net_p50_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p50_satur');?>
																</span>
															</td>
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_thickness', array('id'=>'s2d_net_p10_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_thickness');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_net_p10_net_to_gross" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_vsh', array('id'=>'s2d_net_p10_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_vsh');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_por', array('id'=>'s2d_net_p10_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_por');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_net_p10_satur', array('id'=>'s2d_net_p10_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_net_p10_satur');?>
																</span>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Areal Closure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
															<span class="wajib">
																<div class=" input-append">
																	<!-- <input id="#" name="custom" class="span6" type="text"/> -->
			                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p90_area', array('id'=>'s2d_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                		<span class="add-on">acre</span>
																</div>
																<?php echo $form->error($mdlSeismic2d, 's2d_vol_p90_area');?>
															</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p90_thickness" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p90_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p90_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p90_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p90_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p50_area', array('id'=>'s2d_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p50_thickness" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p50_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p50_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p50_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p50_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic2d, 's2d_vol_p10_area', array('id'=>'s2d_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlSeismic2d, 's2d_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p10_thickness" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p10_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p10_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p10_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s2d_vol_p10_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_2dss_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlSeismic2d, 's2d_remark', array('id'=>'dav_2dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlGras" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGras','DlGras_link','col_dav3' )" /> -->
											<?php echo $form->checkBox($mdlGravity, 'is_checked', array('id'=>'DlGras', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlGras","DlGras_link","col_dav3")'));?>
											<a id="DlGras_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3"> Gravity Survey</a>
										</div>
									</div>
									<div id="col_dav3" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year:</label>
												<div class="controls wajib">
													<!-- <input id="dav_grasur_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
	                                                <?php echo $form->textField($mdlGravity, 'sgv_year_survey', array('id'=>'dav_grasur_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition '));?>
	                                                <?php echo $form->error($mdlGravity, 'sgv_year_survey');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Methods :</label>
												<div class="controls">
													<!-- <input id="dav_grasur_surveymethods" class="span3" style="text-align: center;" /> -->
	                                                <?php echo $form->textField($mdlGravity, 'sgv_survey_method', array('id'=>'dav_grasur_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlGravity, 'sgv_survey_method');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Coverage Area :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_grasur_sca" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlGravity, 'sgv_coverage_area', array('id'=>'dav_grasur_sca', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">acre</span>
													</div>
													<?php echo $form->error($mdlGravity, 'sgv_coverage_area');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Survey Penetration Range:</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_grasur_dspr" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_range', array('id'=>'dav_grasur_dspr', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlGravity, 'sgv_depth_range');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Recorder Spacing Interval :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_grasur_rsi" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlGravity, 'sgv_spacing_interval', array('id'=>'dav_grasur_rsi', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlGravity, 'sgv_spacing_interval');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Spill Point :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_grasur_dcsp" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_spill', array('id'=>'dav_grasur_dcsp', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlGravity, 'sgv_depth_spill');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Estimate OWC/GWC :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_grasur_owc1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate', array('id'=>'dav_grasur_owc1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_depth_estimate');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dlosd3g2" class="span3" type="text" placeholder="By analog to"/> -->
	                                                					<?php echo $form->textField($mdlGravity, 'sgv_depth_estimate_analog', array('id'=>'dlosd3g2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_depth_estimate_analog');?>
																</span>
															</td>
															<td>
																<span>
					                                                <div class=" input-append">
																		<!-- <input id="dav_grasur_owc2" type="text" style="max-width:170px; margin-left:18px;"placeholder="Estimated From Spill Point"/> -->
					                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_estimate_spill', array('id'=>'dav_grasur_owc2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
					                                                    <span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_depth_estimate_spill');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Lowest Known Oil or Gas :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_grasur_oilgas1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_low', array('id'=>'dav_grasur_oilgas1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_depth_low');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dlosd3h2" class="span3" type="text" placeholder="By analog to"/> -->
	                                                					<?php echo $form->textField($mdlGravity, 'sgv_depth_low_analog', array('id'=>'dlosd3h2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_depth_low_analog');?>
																</span>
															</td>
															<td>
																<span>
					                                                <div class=" input-append">
																		<!-- <input id="dav_grasur_oilgas2" type="text" style="max-width:170px; margin-left:18px;" placeholder="Estimated from Spill Point"/> -->
					                                                    <?php echo $form->textField($mdlGravity, 'sgv_depth_low_spill', array('id'=>'dav_grasur_oilgas2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'Estimated from Spill Point'));?>
					                                                    <span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_depth_low_spill');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Reservoir Thickness :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_grasur_resthickness1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlGravity, 'sgv_res_thickness', array('id'=>'dav_grasur_resthickness1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_res_thickness');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dav_grasur_resthickness2" class="span3" type="text" placeholder="According to"/> -->
                                                						<?php echo $form->textField($mdlGravity, 'sgv_res_according', array('id'=>'dav_grasur_resthickness2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'According to'));?>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_res_according');?>
																</span>
															</td>
														</tr>
													</table>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Top Sand Seismic Depth :</label>
												<div class="controls">
	                                                <!-- <input id="dav_grasur_resthickness3" class="span3" type="text" placeholder="Top Sand Seismic Depth" /> -->
	                                                <?php echo $form->textField($mdlGravity, 'sgv_res_top_depth', array('id'=>'dav_grasur_resthickness3', 'class'=>'span3'));?>
	                                                <?php echo $form->error($mdlGravity, 'sgv_res_top_depth');?>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Bottom Sand Seismic Depth :</label>
												<div class="controls">
	                                                <!-- <input id="dav_grasur_resthickness4" class="span3" type="text" placeholder="Bottom Sand Seismic Depth"/> -->
	                                                <?php echo $form->textField($mdlGravity, 'sgv_res_bot_depth', array('id'=>'dav_grasur_resthickness4', 'class'=>'span3'));?>
	                                                <?php echo $form->error($mdlGravity, 'sgv_res_bot_depth');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Gross Sand Thickness :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_grasur_gst1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlGravity, 'sgv_gross_thickness', array('id'=>'dav_grasur_gst1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_gross_thickness');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dav_grasur_gst2" class="span3" type="text" placeholder="According to"/> -->
                                                						<?php echo $form->textField($mdlGravity, 'sgv_gross_according', array('id'=>'dav_grasur_gst2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'According to'));?>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_gross_according');?>
																</span>
															</td>
														</tr>
													</table>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">According to Well Name :</label>
												<div class="controls">
													<!-- <input id="dav_grasur_gst3" class="span6" type="text" placeholder="Well Name"/> -->
	                                                <?php echo $form->textField($mdlGravity, 'sgv_gross_well', array('id'=>'dav_grasur_gst3', 'class'=>'span6'));?>
	                                                <?php echo $form->error($mdlGravity, 'sgv_gross_well');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Net To Gross (Net Pay/Gross Sand) :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_grasur_netogross" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlGravity, 'sgv_net_gross', array('id'=>'dav_grasur_netogross', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">%</span>
													</div>
													<?php echo $form->error($mdlGravity, 'sgv_net_gross');?>
												</div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Areal Closure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p90_area', array('id'=>'sgv_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_vol_p90_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p90_thickness" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p90_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p90_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p90_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p90_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p50_area', array('id'=>'sgv_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p50_thickness" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p50_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p50_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p50_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p50_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" name="custom" class="span6" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlGravity, 'sgv_vol_p10_area', array('id'=>'sgv_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGravity, 'sgv_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p10_thickness" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p10_porosity" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p10_sw" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p10_oil" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgv_vol_p10_gas" name="custom" class="span6" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_grasur_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGravity, 'sgv_remark', array('id'=>'dav_grasur_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlGeos" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGeos','DlGeos_link','col_dav4' )" /> -->
											<?php echo $form->checkBox($mdlGeochemistry, 'is_checked', array('id'=>'DlGeos', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlGeos","DlGeos_link","col_dav4")'));?>
											<a id="DlGeos_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4"> Geochemistry Survey</a>
										</div>
									</div>
									<div id="col_dav4" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year :</label>
												<div class="controls wajib">
													<!-- <input id="dav_geo_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_year_survey', array('id'=>'dav_geo_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_year_survey');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Interval Samples Range :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_geo_isr" type="text" style="max-width:170px;"/>-->
	                                                    <?php echo $form->textField($mdlGeochemistry, 'sgc_sample_interval', array('id'=>'dav_geo_isr', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlGeochemistry, 'sgc_sample_interval');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of Sample Location :</label>
												<div class="controls">
													<!-- <input id="dav_geo_numsample" type="text" class="span3"/>-->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_sample', array('id'=>'dav_geo_numsample', 'class'=>'span3 number'));?>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_number_sample');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of Rocks Sample :</label>
												<div class="controls">
													<!-- <input id="dav_geo_numrock" type="text" class="span3"/>-->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_rock', array('id'=>'dav_geo_numrock', 'class'=>'span3 number'));?>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_number_rock');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of Fluid Sample :</label>
												<div class="controls">
													<!-- <input id="dav_geo_numfluid" type="text" class="span3"/>-->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_number_fluid', array('id'=>'dav_geo_numfluid', 'class'=>'span3 number'));?>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_number_fluid');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Availability HC Composition :</label>
												<div class="controls">
													<!-- <input id="dav_geo_ahc" class="span3" style="text-align: center;" />-->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_avail_hc', array('id'=>'dav_geo_ahc', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_avail_hc');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Hydrocarbon Composition : </label>
												<div class="controls">
													<!-- <input id="dav_geo_hycomp" type="text" class="span3"/>-->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_hc_composition', array('id'=>'dav_geo_hycomp', 'class'=>'span3'));?>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_hc_composition');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Laboratorium Evaluation & Report Year</label>
												<div class="controls">
													<!-- <input id="dav_geo_lery" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" placeholder="angka,koma"/> -->
	                                                <?php echo $form->textField($mdlGeochemistry, 'sgc_lab_report', array('id'=>'dav_geo_lery', 'class'=>'span3 number-comma popovers', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
	                                                <?php echo $form->error($mdlGeochemistry, 'sgc_lab_report');?>
	                                            </div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Areal Closure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p90_area', array('id'=>'sgc_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGeochemistry, 'sgc_vol_p90_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p90_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p90_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p90_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p90_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p90_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p50_area', array('id'=>'sgc_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGeochemistry, 'sgc_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p50_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p50_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p50_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p50_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p50_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlGeochemistry, 'sgc_vol_p10_area', array('id'=>'sgc_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlGeochemistry, 'sgc_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p10_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p10_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p10_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p10_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sgc_vol_p10_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_geo_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlGeochemistry, 'sgc_remark', array('id'=>'dav_geo_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlEs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlEs','DlEs_link','col_dav5' )" /> -->
											<?php echo $form->checkBox($mdlElectromagnetic, 'is_checked', array('id'=>'DlEs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlEs","DlEs_link","col_dav5")'));?> 
											<a id="DlEs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav5"> Electromagnetic Survey</a>
										</div>
									</div>
									<div id="col_dav5" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year:</label>
												<div class="controls wajib">
													<!-- <input id="dav_elec_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
	                                                <?php echo $form->textField($mdlElectromagnetic, 'sel_year_survey', array('id'=>'dav_elec_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
	                                                <?php echo $form->error($mdlElectromagnetic, 'sel_year_survey');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Methods :</label>
												<div class="controls">
													<!-- <input id="dav_elec_surveymethods" class="span3" style="text-align: center;" /> -->
	                                                <?php echo $form->textField($mdlElectromagnetic, 'sel_survey_method', array('id'=>'dav_elec_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlElectromagnetic, 'sel_survey_method');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Coverage Area :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_elec_sca" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_coverage_area', array('id'=>'dav_elec_sca', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">acre</span>
													</div>
													<?php echo $form->error($mdlElectromagnetic, 'sel_coverage_area');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Survey Penetration Range :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="dav_elec_dspr" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_depth_range', array('id'=>'dav_elec_dspr', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlElectromagnetic, 'sel_depth_range');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Recorder Spacing Interval :</label>
												<div class="controls">
													<div class="input-append">
														<!-- <input id="dav_elec_rsi" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlElectromagnetic, 'sel_spacing_interval', array('id'=>'dav_elec_rsi', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlElectromagnetic, 'sel_spacing_interval');?>
												</div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Area Closure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation(1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p90_area', array('id'=>'sel_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlElectromagnetic, 'sel_vol_p90_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p90_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p90_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p90_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p90_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p90_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p50_area', array('id'=>'sel_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlElectromagnetic, 'sel_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p50_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p50_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p50_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p50_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p50_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlElectromagnetic, 'sel_vol_p10_area', array('id'=>'sel_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlElectromagnetic, 'sel_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p10_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p10_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p10_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p10_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sel_vol_p10_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_elec_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlElectromagnetic, 'sel_remark', array('id'=>'dav_elec_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlRs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlRs','DlRs_link','col_dav6' )" /> -->
											<?php echo $form->checkBox($mdlResistivity, 'is_checked', array('id'=>'DlRs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlRs","DlRs_link","col_dav6")'));?>
											<a id="DlRs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav6"> Resistivity Survey</a>
										</div>
									</div>
									<div id="col_dav6" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year:</label>
												<div class="controls wajib">
													<!-- <input id="dav_res_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
	                                                <?php echo $form->textField($mdlResistivity, 'rst_year_survey', array('id'=>'dav_res_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
	                                                <?php echo $form->error($mdlResistivity, 'rst_year_survey');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Methods :</label>
												<div class="controls">
													<!-- <input id="dav_res_surveymethods" class="span3" style="text-align: center;" /> -->
	                                                <?php echo $form->textField($mdlResistivity, 'rst_survey_method', array('id'=>'dav_res_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlResistivity, 'rst_survey_method');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Survey Coverage Area :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_res_sca" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlResistivity, 'rst_coverage_area', array('id'=>'dav_res_sca', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">acre</span>
													</div>
													<?php echo $form->error($mdlResistivity, 'rst_coverage_area');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Survey Penetration Range :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_res_dspr" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlResistivity, 'rst_depth_range', array('id'=>'dav_res_dspr', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlResistivity, 'rst_depth_range');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Recorder Spacing Interval :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_res_rsi" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlResistivity, 'rst_spacing_interval', array('id'=>'dav_res_rsi', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlResistivity, 'rst_spacing_interval');?>
												</div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Area Closure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p90_area', array('id'=>'rst_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlResistivity, 'rst_vol_p90_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p90_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p90_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p90_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p90_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p90_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p50_area', array('id'=>'rst_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlResistivity, 'rst_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p50_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p50_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p50_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p50_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p50_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlResistivity, 'rst_vol_p10_area', array('id'=>'rst_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlResistivity, 'rst_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p10_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p10_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p10_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p10_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="rst_vol_p10_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_res_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlResistivity, 'rst_remark', array('id'=>'dav_res_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlOs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlOs','DlOs_link','col_dav7' )" /> -->
											<?php echo $form->checkBox($mdlOther, 'is_checked', array('id'=>'DlOs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlOs","DlOs_link","col_dav7")'));?>
											<a id="DlOs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav7"> Other Survey</a>
										</div>
									</div>
									<div id="col_dav7" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year :</label>
												<div class="controls wajib">
													<!-- <input id="dav_other_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
	                                                <?php echo $form->textField($mdlOther, 'sor_year_survey', array('id'=>'dav_other_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
	                                                <?php echo $form->error($mdlOther, 'sor_year_survey');?>
	                                            </div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Area Clossure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p90_area', array('id'=>'sor_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlOther, 'sor_vol_p90_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p90_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p90_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p90_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p90_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p90_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p50_area', array('id'=>'sor_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlOther, 'sor_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p50_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p50_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p50_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p50_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p50_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
			                                                    		<?php echo $form->textField($mdlOther, 'sor_vol_p10_area', array('id'=>'sor_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
			                                                    		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlOther, 'sor_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p10_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p10_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p10_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p10_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="sor_vol_p10_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_other_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlOther, 'sor_remark', array('id'=>'dav_other_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<div class="accordion-heading">
											<div class="geser"></div>
											<!-- <input id="DlDss3" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss3','DlDss3_link','col_dav8' )" /> -->
											<?php echo $form->checkBox($mdlSeismic3d, 'is_checked', array('id'=>'DlDss3', 'class'=>'cekbok', 'onclick'=>'toggleStatus("DlDss3","DlDss3_link","col_dav8")'));?>
											<a id="DlDss3_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav8"> Survey Seismic 3D</a>
										</div>
									</div>
									<div id="col_dav8" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Acquisition Year :</label>
												<div class="controls wajib">
													<!-- <input id="dav_3dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_year_survey', array('id'=>'dav_3dss_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_year_survey');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Number of 3D Vintage :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_numvin" class="span3" style="text-align: center;" /> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_vintage_number', array('id'=>'dav_3dss_numvin', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_vintage_number');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Bin Size :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_3dss_binsize" type="text" class="span3" /> -->
                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_bin_size', array('id'=>'dav_3dss_binsize', 'class'=>'number', 'style'=>'max-width:130px;'));?>
                                                		<span class="add-on">km<sup>2</sup></span>
                                                	</div>
                                                	<?php echo $form->error($mdlSeismic3d, 's3d_bin_size');?>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Total Coverage Area : </label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_3dss_tca" type="text" style="max-width:160px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_coverage_area', array('id'=>'dav_3dss_tca', 'class'=>'number', 'style'=>'max-width:130px;'))?>
	                                                    <span class="add-on">km<sup>2</sup></span>
													</div>
													<?php echo $form->error($mdlSeismic3d, 's3d_coverage_area');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Dominant Frequency at Reservoir Target:</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_3dss_dfrt1" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_frequency', array('id'=>'dav_3dss_dfrt1', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">hz</span>
													</div>
													<?php echo $form->error($mdlSeismic3d, 's3d_frequency');?>
                                            	</div>
											</div>
											<div class="control-group">
												<label class="control-label">Lateral Seismic Resolution :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_dfrt2" type="text" class="span3" placeholder="Lateral Seismic Resolution"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_frequency_lateral', array('id'=>'dav_3dss_dfrt2', 'class'=>'span3'));?>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_frequency_lateral');?>   
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Vertical Seismic Resolution :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_dfrt3" type="text" class="span3" placeholder="Vertical Seismic Resolution"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_frequency_vertical', array('id'=>'dav_3dss_dfrt3', 'class'=>'span3'));?>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_frequency_vertical');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Latest Processing Year:</label>
												<div class="controls">
													<!-- <input id="dav_3dss_lpy" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Latest Processing Year"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_year_late_process', array('id'=>'dav_3dss_lpy', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Latest Processing Year'));?>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_year_late_process');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Latest Proccessing Method :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_lpm" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_late_method', array('id'=>'dav_3dss_lpm', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_late_method');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Seismic Image Quality :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_siq" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_img_quality', array('id'=>'dav_3dss_siq', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_img_quality');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Top Reservoir Target Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-prepend input-append">
																		<span class="add-on">MD</span>
																		<!-- <input id="dav_3dss_trtd1" type="text" style="max-width:155px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ft', array('id'=>'dav_3dss_trtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px;'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_top_depth_ft');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_3dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_top_depth_ms', array('id'=>'dav_3dss_trtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
					                                                    <span class="add-on">ms</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_top_depth_ms');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Bottom Reservoir Target Depth :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class="input-prepend input-append">
																		<span class="add-on">MD</span>
																		<!-- <input id="dav_3dss_brtd1" type="text" style="max-width:155px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_bot_depth_ft', array('id'=>'dav_3dss_brtd1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:125px;'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_bot_depth_ft');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_3dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_bot_depth_ms', array('id'=>'dav_3dss_brtd2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px; margin-left:24px;'));?>
					                                                    <span class="add-on">ms</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_bot_depth_ms');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Spill Point :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_3dss_dcsp" type="text" style="max-width:170px;"/> -->
	                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_spill', array('id'=>'dav_3dss_dcsp', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlSeismic3d, 's3d_depth_spill');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Ability to Distinguish Wave Propagation by Offset :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_adwpo" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_offset', array('id'=>'dav_3dss_adwpo', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_ability_offset');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Ability to Resolve Reservoir Rock Property :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_ar3p" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_rock', array('id'=>'dav_3dss_ar3p', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_ability_rock');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Ability to Resolve Reservoir Fluid Property :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_arrfp" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_ability_fluid', array('id'=>'dav_3dss_arrfp', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_ability_fluid');?>
	                                            </div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Estimate OWC/GWC :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_3dss_owc1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate', array('id'=>'dav_3dss_owc1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_depth_estimate');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dlosd8n2" class="span3" type="text" placeholder="By analog to" /> -->
	                                                					<?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate_analog', array('id'=>'dlosd8n2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_depth_estimate_analog');?>
																</span>
															</td>
															<td>
																<span>
					                                                <div class=" input-append">
																		<!-- <input id="dav_3dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" /> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_estimate_spill', array('id'=>'dav_3dss_owc2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
					                                                    <span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_depth_estimate_spill');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Depth Contour Lowest Known Oil/Gas :</label>
												<div class="controls">
													<table>
														<tr>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="dav_3dss_oilgas1" type="text" style="max-width:170px;"/> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low', array('id'=>'dav_3dss_oilgas1', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:137px'));?>
					                                                    <span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_depth_low');?>
																</span>
															</td>
															<td>
																<span>
																	<div>
																		<!-- <input id="dlosd8o2" class="span3" type="text" placeholder="By analog to" /> -->
	                                                					<?php echo $form->textField($mdlSeismic3d, 's3d_depth_low_analog', array('id'=>'dlosd8o2', 'class'=>'span3 tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'width: 177px; margin-left:24px;', 'placeholder'=>'By analog to'));?>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_depth_low_analog');?>
																</span>
															</td>
															<td>
																<span>
					                                                <div class=" input-append">
																		<!-- <input id="dav_3dss_oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" /> -->
					                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_depth_low_spill', array('id'=>'dav_3dss_oilgas2', 'class'=>'number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'style'=>'max-width:170px; margin-left:24px;', 'placeholder'=>'Estimated From Spill Point'));?>
					                                                    <span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_depth_low_spill');?>
																</span>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Formation Thickness :</label>
												<div class="controls">
													<div class=" input-append">
														<!-- <input id="dav_3dss_forthickness" type="text" style="max-width:170px;" /> -->
	                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_formation_thickness', array('id'=>'dav_3dss_forthickness', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlSeismic3d, 's3d_formation_thickness');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Grass Sand or Reservoir Thickness :</label>
												<div class="controls wajib">
													<div class=" input-append">
														<!-- <input id="dav_3dss_grass_resthickness" type="text" style="max-width:170px;" /> -->
	                                                    <?php echo $form->textField($mdlSeismic3d, 's3d_gross_thickness', array('id'=>'dav_3dss_grass_resthickness', 'class'=>'number', 'style'=>'max-width:137px'));?>
	                                                    <span class="add-on">ft</span>
													</div>
													<?php echo $form->error($mdlSeismic3d, 's3d_gross_thickness');?>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Availability Net Thickness Reservoir Pay :</label>
												<div class="controls">
													<!-- <input id="dav_3dss_antrip" class="span3" style="text-align: center;"/> -->
	                                                <?php echo $form->textField($mdlSeismic3d, 's3d_avail_pay', array('id'=>'dav_3dss_antrip', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
	                                                <p style="margin-bottom: 30px; display: inline-block;"></p>
	                                                <?php echo $form->error($mdlSeismic3d, 's3d_avail_pay');?>
	                                            </div>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Pay Zone Thickness / Net Pay</th>
															<th>Thickness Net Pay</th>
<!-- 															<th>NTG(Net Pay to Gross Sand)</th> -->
															<th>Vsh Cut Off</th>
															<th>Porosity Cut Off</th>
															<th>Saturation Cut Off</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_thickness', array('id'=>'s3d_net_p90_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_thickness');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_net_p90_net_to_gross" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_vsh', array('id'=>'s3d_net_p90_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_vsh');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_por', array('id'=>'s3d_net_p90_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_por');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p90_satur', array('id'=>'s3d_net_p90_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p90_satur');?>
																</span>
															</td>
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_thickness', array('id'=>'s3d_net_p50_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_thickness');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_net_p50_net_to_gross" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_vsh', array('id'=>'s3d_net_p50_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_vsh');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_por', array('id'=>'s3d_net_p50_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_por');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/>  -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p50_satur', array('id'=>'s3d_net_p50_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p50_satur');?>
																</span>
															</td>
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_thickness', array('id'=>'s3d_net_p10_thickness', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">ft</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_thickness');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_net_p10_net_to_gross" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_vsh', array('id'=>'s3d_net_p10_vsh', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_vsh');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_por', array('id'=>'s3d_net_p10_por', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_por');?>
																</span>
															</td>
															<td>
																<span>
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_net_p10_satur', array('id'=>'s3d_net_p10_satur', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">%</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_net_p10_satur');?>
																</span>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>Volumetric Estimation</th>
															<th>Areal Closure Estimation</th>
<!-- 															<th>Net Pay Thickness</th> -->
<!-- 															<th>Porosity</th> -->
<!-- 															<th>HC Saturation (1-Sw)</th> -->
<!-- 															<th>Oil Case</th> -->
<!-- 															<th>Gas Case</th> -->
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>P90</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p90_area', array('id'=>'s3d_vol_p90_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_vol_p90_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p90_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p90_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p90_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p90_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p90_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P50</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p50_area', array('id'=>'s3d_vol_p50_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_vol_p50_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p50_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p50_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p50_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p50_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p50_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
														<tr>
															<th>P10</th>
															<td>
																<span class="wajib">
																	<div class=" input-append">
																		<!-- <input id="#" class="span6" name="custom" type="text"/> -->
				                                                		<?php echo $form->textField($mdlSeismic3d, 's3d_vol_p10_area', array('id'=>'s3d_vol_p10_area', 'class'=>'span6 number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body'));?>
				                                                		<span class="add-on">acre</span>
																	</div>
																	<?php echo $form->error($mdlSeismic3d, 's3d_vol_p10_area');?>
																</span>
															</td>
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p10_thickness" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">ft</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p10_porosity" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p10_sw" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">%</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p10_oil" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">MMBO</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<td> -->
<!-- 																<div class=" input-append"> -->
<!-- 																	<input id="s3d_vol_p10_gas" class="span6" name="custom" type="text" readonly />  -->
<!-- 																	<span class="add-on">BCF</span> -->
<!-- 																</div> -->
<!-- 															</td> -->
														</tr>
													</tbody>
												</table>
											</div>
											<div class="control-group">
												<label class="control-label">Remarks :</label>
												<div class="controls">
													<!-- <textarea id="dav_3dss_remarks" class="span3" rows="2"></textarea> -->
                                                <?php echo $form->textArea($mdlSeismic3d, 's3d_remark', array('id'=>'dav_3dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</span>
					</div>
					<!-- END OCCURANCE SURVEY DESCRIPTION-->
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN LEAD GCF -->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-list"></i> DISCOVERY PROSPECT GEOLOGICAL CHANCE FACTOR</h4>
						<span class="tools"><a href="javascript:;"
							class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<span action="#" class="form-horizontal"> <!-- Notification -->
							<div class="control-group">
								<div class="alert alert-success">
									<button class="close" data-dismiss="alert">×</button>
									<strong>Discovery Prospect Geological Chance Factor</strong>
									<p>When Play chosen this Geological Chance Factor automatically filled with Play Geological Chance Factor, change according to newest Geological Change Factor or leave as it is.</p>
								</div>
							</div> <!-- Notification -->
							<div class="accordion">
								<!-- Begin Data Source Rock -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gcf1"> <span class="add-on"><i
												class="icon-list-alt"></i></span><strong> Source Rock</strong>
										</a>
									</div>
									<div id="col_gcf1" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Source Rock :</label>
												<div class="controls wajib">
													<!-- <input id="gcfsrock" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_sr', array('id'=>'gcfsrock', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_sr');?>
                                                    <?php echo $form->hiddenField($mdlGcf, 'gcf_id_reference', array('id'=>'gcf_id_reference'));?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Source Age :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input id="gcfsrock1" class="span3" style="text-align: center;" /> -->
                                                   						<?php echo $form->textField($mdlGcf, 'gcf_sr_age_system', array('id'=>'gcfsrock1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                   						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                   						<?php echo $form->error($mdlGcf, 'gcf_sr_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input id="gcfsrock1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_age_serie', array('id'=>'gcfsrock1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Source Formation Name or Equivalent :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input id="gcfsrock2" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_sr_formation', array('id'=>'sourceformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_sr_formation');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<?php echo $form->textField($mdlGcf, 'gcf_sr_formation_serie', array('id'=>'gcf_sr_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlGcf, 'gcf_sr_formation_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Type of Kerogen :</label>
												<div class="controls">
													<!-- <input id="gcfsrock3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_kerogen', array('id'=>'gcfsrock3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_ker" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_kerogen');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Capacity (TOC) :</label>
												<div class="controls">
													<!-- <input id="gcfsrock4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_toc', array('id'=>'gcfsrock4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_toc" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_toc');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Heat Flow Unit (HFU) :</label>
												<div class="controls">
													<!-- <input id="gcfsrock5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_hfu', array('id'=>'gcfsrock5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_hfu" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_hfu');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Distribution :</label>
												<div class="controls">
													<!-- <input id="gcfsrock6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_distribution', array('id'=>'gcfsrock6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_distribution');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Continuity :</label>
												<div class="controls">
													<!-- <input id="gcfsrock7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_continuity', array('id'=>'gcfsrock7', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_continuity');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Maturity :</label>
												<div class="controls">
													<!-- <input id="gcfsrock8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_maturity', array('id'=>'gcfsrock8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_mat" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_maturity');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Present of Other Source Rock :</label>
												<div class="controls">
													<!-- <input id="gcfsrock9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_otr', array('id'=>'gcfsrock9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_otr" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_otr');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Remark for Source Rock:</label>
												<div class="controls">
													<!-- <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_sr_remark', array('id'=>'gcfsrock10', 'class'=>'span3', 'row'=>'2', 'style'=>'text-align: left;'));?>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Source Rock -->
								<!-- Begin Data Reservoir -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gcf2"> <span class="add-on"><i
												class="icon-list-alt"></i></span><strong> Reservoir</strong>
										</a>
									</div>
									<div id="col_gcf2" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Reservoir :</label>
												<div class="controls wajib">
													<!-- <input type="hidden" id="gcfres" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_res', array('id'=>'gcfres', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_res');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Reservoir Age :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_age_system', array('id'=>'gcfres1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_age_serie', array('id'=>'gcfres1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Reservoir Formation Name or Equivalent :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_res_formation', array('id'=>'reservoirformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_res_formation');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<?php echo $form->textField($mdlGcf, 'gcf_res_formation_serie', array('id'=>'gcf_res_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlGcf, 'gcf_res_formation_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
													
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Depositional Setting :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_set', array('id'=>'gcfres3', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_set');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Depositional Environment :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_env', array('id'=>'gcfres4', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_env');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Distribution :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_distribution', array('id'=>'gcfres5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_dis" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_distribution');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Continuity :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_continuity', array('id'=>'gcfres6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_continuity');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Lithology :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_lithology', array('id'=>'gcfres7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_lit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_lithology');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Porosity Type :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_type', array('id'=>'gcfres8', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_type');?>
                                                </div>
											</div>
											<!-- <div id="prim_pro" class="control-group"> -->
                                                <!-- <label class="control-label">Average Primary Porosity Reservoir :</label> -->
                                                <!-- <div class="controls"> -->
                                                    <!-- <p style="margin-bottom: 2px;"> -->
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php //echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3', 'style'=>'text-align: center; max-width: 155px; position: relative; display: inline-block;'));?>
                                                    <!-- <span class="addkm">%</span> -->
                                                    <!-- </p> -->
                                                    <?php //echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                <!-- </div> -->
                                            <!-- </div> -->
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Average Primary Porosity Reservoir % :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_pri" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_secondary', array('id'=>'gcfres10', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_sec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_secondary');?>
                                                </div>
                                            </div>
											<div class="control-group">
												<label class="control-label">Remark for Reservoir :</label>
												<div class="controls">
													<!-- <textarea id="gcfres11" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_res_remark', array('id'=>'gcfres11', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Reservoir -->
								<!-- Begin Data Trap -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gcf3"> <span class="add-on"><i
												class="icon-list-alt"></i></span><strong> Trap</strong>
										</a>
									</div>
									<div id="col_gcf3" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Trap :</strong></label>
												<div class="controls wajib">
													<!-- <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_trap', array('id'=>'gcftrap', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_trap');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sealing Age :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_system', array('id'=>'gcftrap1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_serie', array('id'=>'gcftrap1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sealing Formation Name or Equivalent :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" /> -->
                                               							<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation', array('id'=>'sealingformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                               							<p style="margin-bottom: 30px; display: inline-block;"></p>
                                               							<?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation_serie', array('id'=>'gcf_trap_seal_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
																		<p style="margin-bottom: 30px; display: inline-block;"></p>
																		<?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sealing Distribution :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_distribution', array('id'=>'gcftrap3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_sdi" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_distribution');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sealing Continuity :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_continuity', array('id'=>'gcftrap4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_scn" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_continuity');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Sealing Type :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcftrap5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_type', array('id'=>'gcftrap5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_stp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_type');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Trapping Age :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_age_system', array('id'=>'gcftrap6', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_trap_age_serie', array('id'=>'gcftrap6a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_trap_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Trapping Geometry :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_geometry', array('id'=>'gcftrap7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_geo" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_geometry');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Trapping Type :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_type', array('id'=>'gcftrap8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_trp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_type');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Closure Type :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_closure', array('id'=>'gcftrap9', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_closure');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Remark for Trap:</label>
												<div class="controls">
													<!-- <textarea id="gcftrap10"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_trap_remark', array('id'=>'gcftrap10', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Trap -->
								<!-- Begin Data Dynamic -->
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse"
											href="#col_gcf4"> <span class="add-on"><i
												class="icon-list-alt"></i></span><strong> Dynamic</strong>
										</a>
									</div>
									<div id="col_gcf4" class="accordion-body collapse">
										<div class="accordion-inner">
											<div class="control-group">
												<label class="control-label">Dynamic :</strong></label>
												<div class="controls wajib">
													<!-- <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_dyn', array('id'=>'gcfdyn', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_dyn');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Authenticate Migration :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration', array('id'=>'gcfdyn1', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_migration');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Trap Position due to Kitchen :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_kitchen', array('id'=>'gcfdyn2', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_kit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_kitchen');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Tectonic Order to Establish
													Petroleum System :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_petroleum', array('id'=>'gcfdyn3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_tec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_petroleum');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Tectonic Regime (Earliest) :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" /> -->
                                                						<?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_system', array('id'=>'gcfdyn4', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                						<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                						<?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_serie', array('id'=>'gcfdyn4a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Tectonic Regime (Latest) :</label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_system', array('id'=>'gcfdyn5', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_serie', array('id'=>'gcfdyn5a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Preservation/Segregation Post
													Entrapment :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_preservation', array('id'=>'gcfdyn6', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_prv" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_preservation');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Migration Pathways :</label>
												<div class="controls">
													<!-- <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_pathways', array('id'=>'gcfdyn7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_mig" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_pathways');?>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Estimation Migration Age : </label>
												<div class="controls">
													<table>
														<tr>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_system', array('id'=>'gcfdyn8', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_system');?>
																	</div>
																</span>
															</td>
															<td style="vertical-align: top; max-width: 180px;">
																<span>
																	<div>
																		<!-- <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" /> -->
                                                    					<?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_serie', array('id'=>'gcfdyn8a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                    					<p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    					<?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_serie');?>
																	</div>
																</span>
															</td>
														</tr>
													</table>
                                                </div>
											</div>
											<div class="control-group">
												<label class="control-label">Remark for Dynamic:</label>
												<div class="controls">
													<!-- <textarea id="gcfdyn9"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_dyn_remark', array('id'=>'gcfdyn9', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Data Dynamic -->
							</div>
						</span>
					</div>
				</div>
				<!-- BEGIN LEAD GCF -->
			</div>
		</div>
        <?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
        <div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
        <?php echo CHtml::ajaxSubmitButton('SAVE', CHtml::normalizeUrl(array('/kkks/creatediscovery', 'render'=>true)), array(
        	'type'=>'POST',
        	'dataType'=>'json',
        	'beforeSend'=>'function(data) {
                $("yt0").prop("disabled", true);
        	}',
        	'success'=>'js:function(data) {
        		$(".tooltips").attr("data-original-title", "");
        		
        		$(".has-err").removeClass("has-err");
        		$(".errorMessage").hide();
        		
        		if(data.result === "success") {
        			$(".close").addClass("redirect");
        			$("#message").html(data.msg);
        			$("#popup").modal("show");
        			$("#pesan").hide();
        			$.fn.yiiGridView.update("discovery-grid");
        			$(".redirect").click( function () {
						var redirect = "' . Yii::app()->createUrl('/Kkks/creatediscovery') . '";
						window.location=redirect;
					});
                    $("yt0").prop("disabled", false);
        		} else {
        			var myArray = JSON.parse(data.err);
        			$.each(myArray, function(key, val) {
        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
		        		{
        					$("#creatediscovery-form #"+key+"_em_").parent().addClass("has-err");
        					$("#creatediscovery-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
        					
		        		} else {
        					$("#creatediscovery-form #"+key+"_em_").text(val);                                                    
							$("#creatediscovery-form #"+key+"_em_").show();
	        				$("#creatediscovery-form #"+key+"_em_").parent().addClass("has-err");
        				}
        				
        				
//         				console.log($("#creatediscovery-form #"+key+"_em_").parents().eq(1)[0].nodeName);
//         				console.log($("#creatediscovery-form #"+key+"_em_").parent().children(":input").attr("id"));
        			});
        		
        			$("#message").html(data.msg);
        			$("#popup").modal("show");
                    $("yt0").prop("disabled", false);
        		}
        	}',
        ),
        array('class'=>'btn btn-inverse')
        );?>
        </div>
	    <?php $this->endWidget();?>
	    
	    <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->
    </div>
</div>

<?php
Yii::app ()->clientScript->registerScript ( 'ambil-gcf', "
		var blank_lithology = '',
			blank_depos_env = '',
			blank_formation_serie = '',
			blank_age_serie = '',
        	blank_trap_type = '';

		function generate_prospect_name() {
			$('#gcfres7').select2('val') == 'Unknown' ? blank_lithology = '' : blank_lithology = $('#gcfres7').select2('val');
			$('#gcfres4').select2('val') == 'Others' ? blank_depos_env = '' : blank_depos_env = $('#gcfres4').select2('val');
			$('#gcf_res_formation_serie').select2('val') == 'Not Available' ? blank_formation_serie = '' : blank_formation_serie = $('#gcf_res_formation_serie').select2('val');
            $('#gcfres1a').select2('val') == 'Not Available' ? blank_age_serie = '' : blank_age_serie = $('#gcfres1a').select2('val');
            $('#gcftrap8').select2('val') == 'Unknown' ? blank_trap_type = '' : blank_trap_type = $('#gcftrap8').select2('val');

			$('#additional_lead_name').text($('#structure_name').val() + '; ' + blank_lithology + ' - ' + blank_formation_serie + ' ' + $('#reservoirformationname').select2('val') + ' - ' + blank_age_serie + ' ' + $('#gcfres1').select2('val') + ' - ' + blank_depos_env + ' - ' + blank_trap_type);
		};
		
		$('#structure_name').on('keyup', function() {
			generate_prospect_name();
		});

    function gcfAttr() {
        var attrGcf = [
            '#gcfsrock3', '#gcfsrock4', '#gcfsrock5', '#gcfsrock8', '#gcfsrock9',
            '#gcfres5', '#gcfres7', '#gcfres10', '#gcfres9',
            '#gcftrap3', '#gcftrap4', '#gcftrap5', '#gcftrap7', '#gcftrap8',
            '#gcfdyn2', '#gcfdyn3', '#gcfdyn6', '#gcfdyn7',
        ];

        for(var loop = 0; loop < attrGcf.length; loop++) {
            $.ajax({
                url: '" . Yii::app()->createUrl('/Kkks/getnilai') . "',
                type: 'POST',
                data: {
                    name: $(attrGcf[loop]).attr('id'),
                    choosen: $(attrGcf[loop]).val(),
                    from: 'discovery',
                    category: $(gcfCategory).val() //category proven atau analog
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
		
		$('#plyName').on('change', function() {
			var value = $('#plyName').val();
			if(value != '') {
				$.ajax({
					url : '" . Yii::app()->createUrl('/Kkks/getGcf' ) . "',
					data : {
						play_id : value,
					},
					type : 'POST',
					dataType : 'json',
					success : function(data) {
						$('#gcf_id_reference').val(data.gcf_id);
						$('#gcfsrock').select2('val', data.gcf_is_sr);
						$('#gcfsrock1').select2('val', data.gcf_sr_age_system);
						$('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
						$('#sourceformationname').select2('val', data.gcf_sr_formation);
                		$('#gcf_sr_formation_serie').select2('val', data.gcf_sr_formation_serie);
						$('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
						$('#gcfsrock4').select2('val', data.gcf_sr_toc);
						$('#gcfsrock5').select2('val', data.gcf_sr_hfu);
						$('#gcfsrock6').select2('val', data.gcf_sr_distribution);
						$('#gcfsrock7').select2('val', data.gcf_sr_continuity);
						$('#gcfsrock8').select2('val', data.gcf_sr_maturity);
						$('#gcfsrock9').select2('val', data.gcf_sr_otr);
						$('#gcfsrock10').val(data.gcf_sr_remark);
						$('#gcfres').select2('val', data.gcf_is_res);
						$('#gcfres1').select2('val', data.gcf_res_age_system);
						$('#gcfres1a').select2('val', data.gcf_res_age_serie);
						$('#reservoirformationname').select2('val', data.gcf_res_formation);
                		$('#gcf_res_formation_serie').select2('val', data.gcf_res_formation_serie);
						$('#gcfres3').select2('val', data.gcf_res_depos_set);
						$('#gcfres4').select2('val', data.gcf_res_depos_env);
						$('#gcfres5').select2('val', data.gcf_res_distribution);
						$('#gcfres6').select2('val', data.gcf_res_continuity);		
						$('#gcfres7').select2('val', data.gcf_res_lithology);
						$('#gcfres8').select2('val', data.gcf_res_por_type);
						$('#gcfres9').select2('val', data.gcf_res_por_primary);
						$('#gcfres10').select2('val', data.gcf_res_por_secondary);
						$('#gcfres11').val(data.gcf_res_remark);
						$('#gcftrap').select2('val', data.gcf_is_trap);
						$('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
						$('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
						$('#sealingformationname').select2('val', data.gcf_trap_seal_formation);
                		$('#gcf_trap_seal_formation_serie').select2('val', data.gcf_trap_seal_formation_serie);
						$('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
						$('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
						$('#gcftrap5').select2('val', data.gcf_trap_seal_type);
						$('#gcftrap6').select2('val', data.gcf_trap_age_system);
						$('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
						$('#gcftrap7').select2('val', data.gcf_trap_geometry);
						$('#gcftrap8').select2('val', data.gcf_trap_type);
						$('#gcftrap9').select2('val', data.gcf_trap_closure);
						$('#gcftrap10').val(data.gcf_trap_remark);
						$('#gcfdyn').select2('val', data.gcf_is_dyn);
						$('#gcfdyn1').select2('val', data.gcf_dyn_migration);
						$('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
						$('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
						$('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
						$('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
						$('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
						$('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
						$('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
						$('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
						$('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
						$('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
						$('#gcfdyn9').val(data.gcf_dyn_remark);
    					disabledElement('gcfsrock');
						disabledElement('gcfres');
						disabledElement('gcftrap');
						disabledElement('gcfdyn');
						disabledElement('gcfres8');
						disable('gcfsrock');
						disable('gcfres');
						disable('gcftrap');
						generate_prospect_name();
            gcfAttr();
					},
				});
			} else {
    			$('#gcfsrock').select2('val', '');
    			disabledElement('gcfsrock');
    			disable('gcfsrock');
    			$('#gcfsrock10').val('');
    		
    			$('#gcfres').select2('val', '');
    			disabledElement('gcfres');
                disable('gcfres');
    			$('#gcfres11').val('');
    			$('#gcfres1').select2('val', '');
                $('#gcfres1a').select2('val', '');
    			$('#reservoirformationname').select2('val', '');
                $('#gcf_res_formation_serie').select2('val', '');
    			$('#gcfres4').select2('val', '');
    			$('#gcfres7').select2('val', '');
    		
    			$('#gcftrap').select2('val', '');
    			disabledElement('gcftrap');
                disable('gcftrap');
    			$('#gcftrap10').val('');
    			$('#gcftrap8').select2('val', '');
    			
    			$('#gcfdyn').select2('val', '');
    			disabledElement('gcfdyn');
    			$('#gcfdyn9').val('');
                disabledElement('gcfres8');
                generate_prospect_name();
                gcfAttr();
    		}
		});
	" );
?>

<?php 
Yii::app()->clientScript->registerScript('coldiscovery', "
		
// 	function calculateClasticThickness() {
// 		var array = [];
// 		var atribut = $(this).attr('id');
		
		
// // 		$('.zone_clastic_p10_thickness').each( function() {
// // 			array.push($(this).attr('id'));
// // 		});
		
// // 		console.log(array);
		
// 		var atribut_ke = atribut.substr(atribut.length -2, 2);
// 		var atribut_semua_zone = atribut.substr(atribut.length -2, 1);
// 		var total_clastic;
// 		for(var i = 1; i <= $('#w').val(); i++) {
// 			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				
// // 				console.log($('#zone_clastic_p10_thickness' + i + j));
// 				total_clastic = total_clastic + $('#zone_clastic_p10_thickness' + i + j).val();
// 				console.log(total_clastic);
// 			}
// 		}
// 		var atribut_ke = atribut.substr(atribut.length -2, 2);
// 		var atribut_semua_zone = atribut.substr(atribut.length -2, 1);
		
// 		var zone_clastic_p10_thickness = ($('#zone_clastic_p10_thickness' + atribut_ke).val() != '') ? parseFloat($('#zone_clastic_p10_thickness' + atribut_ke).val()) : 0;
// 		var zone_clastic_p50_thickness = ($('#zone_clastic_p50_thickness' + atribut_ke).val() != '') ? parseFloat($('#zone_clastic_p50_thickness' + atribut_ke).val()) : 0;
// 		var zone_clastic_p90_thickness = ($('#zone_clastic_p90_thickness' + atribut_ke).val() != '') ? parseFloat($('#zone_clastic_p90_thickness' + atribut_ke).val()) : 0;
// 		var average = (zone_clastic_p10_thickness + zone_clastic_p50_thickness + zone_clastic_p90_thickness) / 3;
// 		var estimate = (zone_clastic_p90_thickness - average) / 3;
// 		$('#average_clastic_thickness' + atribut_ke).val(average.toFixed(2));
// 		$('#estimate_clastic_thickness' + atribut_ke).val(estimate.toFixed(2));
// 		console.log(average.toFixed(2));
// 		console.log(estimate.toFixed(2));
// 	}
		
// 	$('.zone_clastic_p10_thickness').bind('change keyup', calculateClasticThickness).change();
		
	$('#w, .wz').bind('change', function() {
		calculateClasticThickness();
		calculateClasticGr();
		calculateClasticSp();
		calculateClasticNet();
		calculateClasticPor();
		calculateClasticSatur();
		calculateCarbonateThickness();
		calculateCarbonateDtc();
		calculateCarbonateTotal();
		calculateCarbonateNet();
		calculateCarbonatePor();
		calculateCarbonateSatur();
		calculateFluidVolumeOil();
		calculateFluidVolumeGas();
		setSurveyPor90();
	}).change();
		
	$('.zone_clastic_p10_thickness, .zone_clastic_p50_thickness, .zone_clastic_p90_thickness').bind('change keyup', function() {
		calculateClasticThickness();
	}).change();
		
	$('.zone_clastic_p10_gr, .zone_clastic_p50_gr, .zone_clastic_p90_gr').bind('change keyup', function() {
		calculateClasticGr();
	}).change();
		
	$('.zone_clastic_p10_sp, .zone_clastic_p50_sp, .zone_clastic_p90_sp').bind('change keyup', function() {
		calculateClasticSp();
	}).change();
		
	$('.zone_clastic_p10_net, .zone_clastic_p50_net, .zone_clastic_p90_net').bind('change keyup', function() {
		calculateClasticNet();
	}).change();
		
	$('.zone_clastic_p10_por, .zone_clastic_p50_por, .zone_clastic_p90_por').bind('change keyup', function() {
		calculateClasticPor();
	}).change();
		
	$('.zone_clastic_p10_satur, .zone_clastic_p50_satur, .zone_clastic_p90_satur').bind('change keyup', function() {
		calculateClasticSatur();
	}).change();
		
	$('.zone_carbo_p10_thickness, .zone_carbo_p50_thickness, .zone_carbo_p90_thickness').bind('change keyup', function() {
		calculateCarbonateThickness();
	}).change();
		
	$('.zone_carbo_p10_dtc, .zone_carbo_p50_dtc, .zone_carbo_p90_dtc').bind('change keyup', function() {
		calculateCarbonateDtc();
	}).change();
		
	$('.zone_carbo_p10_total, .zone_carbo_p50_total, .zone_carbo_p90_total').bind('change keyup', function() {
		calculateCarbonateTotal();
	}).change();
		
	$('.zone_carbo_p10_net, .zone_carbo_p50_net, .zone_carbo_p90_net').bind('change keyup', function() {
		calculateCarbonateNet();
	}).change();
		
	$('.zone_carbo_p10_por, .zone_carbo_p50_por, .zone_carbo_p90_por').bind('change keyup', function() {
		calculateCarbonatePor();
	}).change();
		
	$('.zone_carbo_p10_satur, .zone_carbo_p50_satur, .zone_carbo_p90_satur').bind('change keyup', function() {
		calculateCarbonateSatur();
	}).change();
		
	$('.zone_fvf_oil_p10, .zone_fvf_oil_p50, .zone_fvf_oil_p90').bind('change keyup', function() {
		calculateFluidVolumeOil();
	}).change();
		
	$('.zone_fvf_gas_p10, .zone_fvf_gas_p50, .zone_fvf_gas_p90').bind('change keyup', function() {
		calculateFluidVolumeGas();
	}).change();
		
	$('#zone_clastic_p90_por11, #zone_carbo_p90_por11').bind('change keyup', function() {
		setSurveyPor90();
		calculateSurveyP90();
	}).change();
		
	$('#zone_clastic_p50_por11, #zone_carbo_p50_por11').bind('change keyup', function() {
		setSurveyPor50();
		calculateSurveyP50();
	}).change();
		
	$('#zone_clastic_p10_por11, #zone_carbo_p10_por11').bind('change keyup', function() {
		setSurveyPor10();
		calculateSurveyP10();
	}).change();
		
	$('#zone_clastic_p90_satur11, #zone_carbo_p90_satur11').bind('change keyup', function() {
		setSurveySatur90();
		calculateSurveyP90();
	}).change();
		
	$('#zone_clastic_p50_satur11, #zone_carbo_p50_satur11').bind('change keyup', function() {
		setSurveySatur50();
		calculateSurveyP50();
	}).change();
		
	$('#zone_clastic_p10_satur11, #zone_carbo_p10_satur11').bind('change keyup', function() {
		setSurveySatur10();
		calculateSurveyP10();
	}).change();
		
	$('#s2d_net_p90_thickness, #s2d_net_p50_thickness, #s2d_net_p10_thickness, #dav_2dss_gross_resthickness').bind('change keyup', function() {
		calculate2dNetToGross();
		setThicknessP90();
		setThicknessP50();
		setThicknessP10();
		calculateSurveyP90();
		calculateSurveyP50();
		calculateSurveyP10();
	}).change();
	
	$('#s3d_net_p90_thickness, #s3d_net_p50_thickness, #s3d_net_p10_thickness, #dav_3dss_grass_resthickness').bind('change keyup', function() {
		calculate3dNetToGross();
		setThicknessP90();
		setThicknessP50();
		setThicknessP10();
		calculateSurveyP90();
		calculateSurveyP50();
		calculateSurveyP10();
	}).change();
		
	$('#sgf_p90_area, #sgf_p90_thickness, #sgf_p90_net').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#sgf_p50_area, #sgf_p50_thickness, #sgf_p50_net').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#sgf_p10_area, #sgf_p10_thickness, #sgf_p10_net').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	$('#s2d_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#s2d_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#s2d_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	$('#sgv_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#sgv_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#sgv_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	$('#sgc_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#sgc_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#sgc_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
	
	$('#sel_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#sel_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#sel_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	$('#rst_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#rst_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#rst_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	$('#sor_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#sor_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#sor_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	$('#s3d_vol_p90_area').bind('change keyup', function() {
		calculateSurveyP90();
	}).change();
		
	$('#s3d_vol_p50_area').bind('change keyup', function() {
		calculateSurveyP50();
	}).change();
		
	$('#s3d_vol_p10_area').bind('change keyup', function() {
		calculateSurveyP10();
	}).change();
		
	function calculateClasticThickness()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_clastic_p10_thickness = ($('#zone_clastic_p10_thickness' + i + j).val() != '') ? parseFloat($('#zone_clastic_p10_thickness' + i + j).val()) : 0;
				var zone_clastic_p50_thickness = ($('#zone_clastic_p50_thickness' + i + j).val() != '') ? parseFloat($('#zone_clastic_p50_thickness' + i + j).val()) : 0;
				var zone_clastic_p90_thickness = ($('#zone_clastic_p90_thickness' + i + j).val() != '') ? parseFloat($('#zone_clastic_p90_thickness' + i + j).val()) : 0;
				var average = (zone_clastic_p10_thickness + zone_clastic_p50_thickness + zone_clastic_p90_thickness) / 3;
				var estimate = (zone_clastic_p90_thickness - average) / 3;
				$('#average_clastic_thickness' + i + j).val(average.toFixed(2));
				$('#estimate_clastic_thickness' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateClasticGr()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_clastic_p10_gr = ($('#zone_clastic_p10_gr' + i + j).val() != '') ? parseFloat($('#zone_clastic_p10_gr' + i + j).val()) : 0;
				var zone_clastic_p50_gr = ($('#zone_clastic_p50_gr' + i + j).val() != '') ? parseFloat($('#zone_clastic_p50_gr' + i + j).val()) : 0;
				var zone_clastic_p90_gr = ($('#zone_clastic_p90_gr' + i + j).val() != '') ? parseFloat($('#zone_clastic_p90_gr' + i + j).val()) : 0;
				var average = (zone_clastic_p10_gr + zone_clastic_p50_gr + zone_clastic_p90_gr) / 3;
				var estimate = (zone_clastic_p90_gr - average) / 3;
				$('#average_clastic_gr' + i + j).val(average.toFixed(2));
				$('#estimate_clastic_gr' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateClasticSp()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_clastic_p10_sp = ($('#zone_clastic_p10_sp' + i + j).val() != '') ? parseFloat($('#zone_clastic_p10_sp' + i + j).val()) : 0;
				var zone_clastic_p50_sp = ($('#zone_clastic_p50_sp' + i + j).val() != '') ? parseFloat($('#zone_clastic_p50_sp' + i + j).val()) : 0;
				var zone_clastic_p90_sp = ($('#zone_clastic_p90_sp' + i + j).val() != '') ? parseFloat($('#zone_clastic_p90_sp' + i + j).val()) : 0;
				var average = (zone_clastic_p10_sp + zone_clastic_p50_sp + zone_clastic_p90_sp) / 3;
				var estimate = (zone_clastic_p90_sp - average) / 3;
				$('#average_clastic_sp' + i + j).val(average.toFixed(2));
				$('#estimate_clastic_sp' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateClasticNet()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_clastic_p10_net = ($('#zone_clastic_p10_net' + i + j).val() != '') ? parseFloat($('#zone_clastic_p10_net' + i + j).val()) : 0;
				var zone_clastic_p50_net = ($('#zone_clastic_p50_net' + i + j).val() != '') ? parseFloat($('#zone_clastic_p50_net' + i + j).val()) : 0;
				var zone_clastic_p90_net = ($('#zone_clastic_p90_net' + i + j).val() != '') ? parseFloat($('#zone_clastic_p90_net' + i + j).val()) : 0;
				var average = (zone_clastic_p10_net + zone_clastic_p50_net + zone_clastic_p90_net) / 3;
				var estimate = (zone_clastic_p90_net - average) / 3;
				$('#average_clastic_net' + i + j).val(average.toFixed(2));
				$('#estimate_clastic_net' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateClasticPor()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_clastic_p10_por = ($('#zone_clastic_p10_por' + i + j).val() != '') ? parseFloat($('#zone_clastic_p10_por' + i + j).val()) : 0;
				var zone_clastic_p50_por = ($('#zone_clastic_p50_por' + i + j).val() != '') ? parseFloat($('#zone_clastic_p50_por' + i + j).val()) : 0;
				var zone_clastic_p90_por = ($('#zone_clastic_p90_por' + i + j).val() != '') ? parseFloat($('#zone_clastic_p90_por' + i + j).val()) : 0;
				var average = (zone_clastic_p10_por + zone_clastic_p90_por + zone_clastic_p90_por) / 3;
				var estimate = (zone_clastic_p90_por - average) / 3;
				$('#average_clastic_por' + i + j).val(average.toFixed(2));
				$('#estimate_clastic_por' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateClasticSatur()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_clastic_p10_satur = ($('#zone_clastic_p10_satur' + i + j).val() != '') ? parseFloat($('#zone_clastic_p10_satur' + i + j).val()) : 0;
				var zone_clastic_p50_satur = ($('#zone_clastic_p50_satur' + i + j).val() != '') ? parseFloat($('#zone_clastic_p50_satur' + i + j).val()) : 0;
				var zone_clastic_p90_satur = ($('#zone_clastic_p90_satur' + i + j).val() != '') ? parseFloat($('#zone_clastic_p90_satur' + i + j).val()) : 0;
				var average = (zone_clastic_p10_satur + zone_clastic_p50_satur + zone_clastic_p90_satur) / 3;
				var estimate = (zone_clastic_p90_satur - average) / 3;
				$('#average_clastic_satur' + i + j).val(average.toFixed(2));
				$('#estimate_clastic_satur' + i + j).val(estimate.toFixed(2));
			}
		}
	}
	
	function calculateCarbonateThickness()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_carbo_p10_thickness = ($('#zone_carbo_p10_thickness' + i + j).val() != '') ? parseFloat($('#zone_carbo_p10_thickness' + i + j).val()) : 0;
				var zone_carbo_p50_thickness = ($('#zone_carbo_p50_thickness' + i + j).val() != '') ? parseFloat($('#zone_carbo_p50_thickness' + i + j).val()) : 0;
				var zone_carbo_p90_thickness = ($('#zone_carbo_p90_thickness' + i + j).val() != '') ? parseFloat($('#zone_carbo_p90_thickness' + i + j).val()) : 0;
				var average = (zone_carbo_p10_thickness + zone_carbo_p50_thickness + zone_carbo_p90_thickness) / 3;
				var estimate = (zone_carbo_p90_thickness - average) / 3;
				$('#average_carbo_thickness' + i + j).val(average.toFixed(2));
				$('#estimate_carbo_thickness' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateCarbonateDtc()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_carbo_p10_dtc = ($('#zone_carbo_p10_dtc' + i + j).val() != '') ? parseFloat($('#zone_carbo_p10_dtc' + i + j).val()) : 0;
				var zone_carbo_p50_dtc = ($('#zone_carbo_p50_dtc' + i + j).val() != '') ? parseFloat($('#zone_carbo_p50_dtc' + i + j).val()) : 0;
				var zone_carbo_p90_dtc = ($('#zone_carbo_p90_dtc' + i + j).val() != '') ? parseFloat($('#zone_carbo_p90_dtc' + i + j).val()) : 0;
				var average = (zone_carbo_p10_dtc + zone_carbo_p50_dtc + zone_carbo_p90_dtc) / 3;
				var estimate = (zone_carbo_p90_dtc - average) / 3;
				$('#average_carbo_dtc' + i + j).val(average.toFixed(2));
				$('#estimate_carbo_dtc' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateCarbonateTotal()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_carbo_p10_total = ($('#zone_carbo_p10_total' + i + j).val() != '') ? parseFloat($('#zone_carbo_p10_total' + i + j).val()) : 0;
				var zone_carbo_p50_total = ($('#zone_carbo_p50_total' + i + j).val() != '') ? parseFloat($('#zone_carbo_p50_total' + i + j).val()) : 0;
				var zone_carbo_p90_total = ($('#zone_carbo_p90_total' + i + j).val() != '') ? parseFloat($('#zone_carbo_p90_total' + i + j).val()) : 0;
				var average = (zone_carbo_p10_total + zone_carbo_p50_total + zone_carbo_p90_total) / 3;
				var estimate = (zone_carbo_p90_total - average) / 3;
				$('#average_carbo_total' + i + j).val(average.toFixed(2));
				$('#estimate_carbo_total' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateCarbonateNet()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_carbo_p10_net = ($('#zone_carbo_p10_net' + i + j).val() != '') ? parseFloat($('#zone_carbo_p10_net' + i + j).val()) : 0;
				var zone_carbo_p50_net = ($('#zone_carbo_p50_net' + i + j).val() != '') ? parseFloat($('#zone_carbo_p50_net' + i + j).val()) : 0;
				var zone_carbo_p90_net = ($('#zone_carbo_p90_net' + i + j).val() != '') ? parseFloat($('#zone_carbo_p90_net' + i + j).val()) : 0;
				var average = (zone_carbo_p10_net + zone_carbo_p50_net + zone_carbo_p90_net) / 3;
				var estimate = (zone_carbo_p90_net - average) / 3;
				$('#average_carbo_net' + i + j).val(average.toFixed(2));
				$('#estimate_carbo_net' + i + j).val(estimate.toFixed(2));
			}
		}
	}
	
	function calculateCarbonatePor()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_carbo_p10_por = ($('#zone_carbo_p10_por' + i + j).val() != '') ? parseFloat($('#zone_carbo_p10_por' + i + j).val()) : 0;
				var zone_carbo_p50_por = ($('#zone_carbo_p50_por' + i + j).val() != '') ? parseFloat($('#zone_carbo_p50_por' + i + j).val()) : 0;
				var zone_carbo_p90_por = ($('#zone_carbo_p90_por' + i + j).val() != '') ? parseFloat($('#zone_carbo_p90_por' + i + j).val()) : 0;
				var average = (zone_carbo_p10_por + zone_carbo_p50_por + zone_carbo_p90_por) / 3;
				var estimate = (zone_carbo_p90_por - average) / 3;
				$('#average_carbo_por' + i + j).val(average.toFixed(2));
				$('#estimate_carbo_por' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateCarbonateSatur()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_carbo_p10_satur = ($('#zone_carbo_p10_satur' + i + j).val() != '') ? parseFloat($('#zone_carbo_p10_satur' + i + j).val()) : 0;
				var zone_carbo_p50_satur = ($('#zone_carbo_p50_satur' + i + j).val() != '') ? parseFloat($('#zone_carbo_p50_satur' + i + j).val()) : 0;
				var zone_carbo_p90_satur = ($('#zone_carbo_p90_satur' + i + j).val() != '') ? parseFloat($('#zone_carbo_p90_satur' + i + j).val()) : 0;
				var average = (zone_carbo_p10_satur + zone_carbo_p50_satur + zone_carbo_p90_satur) / 3;
				var estimate = (zone_carbo_p90_satur - average) / 3;
				$('#average_carbo_satur' + i + j).val(average.toFixed(2));
				$('#estimate_carbo_satur' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateFluidVolumeOil()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_fvf_oil_p10 = ($('#zone_fvf_oil_p10' + i + j).val() != '') ? parseFloat($('#zone_fvf_oil_p10' + i + j).val()) : 0;
				var zone_fvf_oil_p50 = ($('#zone_fvf_oil_p50' + i + j).val() != '') ? parseFloat($('#zone_fvf_oil_p50' + i + j).val()) : 0;
				var zone_fvf_oil_p90 = ($('#zone_fvf_oil_p90' + i + j).val() != '') ? parseFloat($('#zone_fvf_oil_p90' + i + j).val()) : 0;
				var average = (zone_fvf_oil_p10 + zone_fvf_oil_p50 + zone_fvf_oil_p90) / 3;
				var estimate = (zone_fvf_oil_p90 - average) / 3;
				$('#average_initial_oil' + i + j).val(average.toFixed(2));
				$('#estimate_initial_oil' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function calculateFluidVolumeGas()
	{
		for(var i = 1; i <= $('#w').val(); i++) {
			for(var j = 1; j <= $('#wz_' + i).val(); j++) {
				var zone_fvf_gas_p10 = ($('#zone_fvf_gas_p10' + i + j).val() != '') ? parseFloat($('#zone_fvf_gas_p10' + i + j).val()) : 0;
				var zone_fvf_gas_p50 = ($('#zone_fvf_gas_p50' + i + j).val() != '') ? parseFloat($('#zone_fvf_gas_p50' + i + j).val()) : 0;
				var zone_fvf_gas_p90 = ($('#zone_fvf_gas_p90' + i + j).val() != '') ? parseFloat($('#zone_fvf_gas_p90' + i + j).val()) : 0;
				var average = (zone_fvf_gas_p10 + zone_fvf_gas_p50 + zone_fvf_gas_p90) / 3;
				var estimate = (zone_fvf_gas_p90 - average) / 3;
				$('#average_initial_gas' + i + j).val(average.toFixed(2));
				$('#estimate_initial_gas' + i + j).val(estimate.toFixed(2));
			}
		}
	}
		
	function setSurveyPor90()
	{
		var zone_clastic_p90_por11 = ($('#zone_clastic_p90_por11').val() != '') ? parseFloat($('#zone_clastic_p90_por11').val()) : 0;
		var zone_carbo_p90_por11 = ($('#zone_carbo_p90_por11').val() != '') ? parseFloat($('#zone_carbo_p90_por11').val()) : 0;
		var p90_porosity = (zone_clastic_p90_por11 == 0) ?  zone_carbo_p90_por11 : zone_clastic_p90_por11;
		$('#sgf_p90_porosity').val(p90_porosity);
		$('#s2d_vol_p90_porosity').val(p90_porosity);
		$('#sgv_vol_p90_porosity').val(p90_porosity);
		$('#sgc_vol_p90_porosity').val(p90_porosity);
		$('#sel_vol_p90_porosity').val(p90_porosity);
		$('#rst_vol_p90_porosity').val(p90_porosity);
		$('#sor_vol_p90_porosity').val(p90_porosity);
		$('#s3d_vol_p90_porosity').val(p90_porosity);
	}
		
	function setSurveyPor50()
	{
		var zone_clastic_p50_por11 = ($('#zone_clastic_p50_por11').val() != '') ? parseFloat($('#zone_clastic_p50_por11').val()) : 0;
		var zone_carbo_p50_por11 = ($('#zone_carbo_p50_por11').val() != '') ? parseFloat($('#zone_carbo_p50_por11').val()) : 0;
		var p50_porosity = (zone_clastic_p50_por11 == 0) ?  zone_carbo_p50_por11 : zone_clastic_p50_por11;
		$('#sgf_p50_porosity').val(p50_porosity);
		$('#s2d_vol_p50_porosity').val(p50_porosity);
		$('#sgv_vol_p50_porosity').val(p50_porosity);
		$('#sgc_vol_p50_porosity').val(p50_porosity);
		$('#sel_vol_p50_porosity').val(p50_porosity);
		$('#rst_vol_p50_porosity').val(p50_porosity);
		$('#sor_vol_p50_porosity').val(p50_porosity);
		$('#s3d_vol_p50_porosity').val(p50_porosity);
	}
		
	function setSurveyPor10()
	{
		var zone_clastic_p10_por11 = ($('#zone_clastic_p10_por11').val() != '') ? parseFloat($('#zone_clastic_p10_por11').val()) : 0;
		var zone_carbo_p10_por11 = ($('#zone_carbo_p10_por11').val() != '') ? parseFloat($('#zone_carbo_p10_por11').val()) : 0;
		var p10_porosity = (zone_clastic_p10_por11 == 0) ?  zone_carbo_p10_por11 : zone_clastic_p10_por11;
		$('#sgf_p10_porosity').val(p10_porosity);
		$('#s2d_vol_p10_porosity').val(p10_porosity);
		$('#sgv_vol_p10_porosity').val(p10_porosity);
		$('#sgc_vol_p10_porosity').val(p10_porosity);
		$('#sel_vol_p10_porosity').val(p10_porosity);
		$('#rst_vol_p10_porosity').val(p10_porosity);
		$('#sor_vol_p10_porosity').val(p10_porosity);
		$('#s3d_vol_p10_porosity').val(p10_porosity);
	}
		
	function setSurveySatur90()
	{
		var zone_clastic_p90_satur11 = ($('#zone_clastic_p90_satur11').val() != '') ? parseFloat($('#zone_clastic_p90_satur11').val()) : 0;
		var zone_carbo_p90_satur11 = ($('#zone_carbo_p90_satur11').val() != '') ? parseFloat($('#zone_carbo_p90_satur11').val()) : 0;
		var p90_saturation = (zone_clastic_p90_satur11 == 0) ?  zone_carbo_p90_satur11 : zone_clastic_p90_satur11;
		$('#sgf_p90_sw').val(p90_saturation);
		$('#s2d_vol_p90_sw').val(p90_saturation);
		$('#sgv_vol_p90_sw').val(p90_saturation);
		$('#sgc_vol_p90_sw').val(p90_saturation);
		$('#sel_vol_p90_sw').val(p90_saturation);
		$('#rst_vol_p90_sw').val(p90_saturation);
		$('#sor_vol_p90_sw').val(p90_saturation);
		$('#s3d_vol_p90_sw').val(p90_saturation);
	}
		
	function setSurveySatur50()
	{
		var zone_clastic_p50_satur11 = ($('#zone_clastic_p50_satur11').val() != '') ? parseFloat($('#zone_clastic_p50_satur11').val()) : 0;
		var zone_carbo_p50_satur11 = ($('#zone_carbo_p50_satur11').val() != '') ? parseFloat($('#zone_carbo_p50_satur11').val()) : 0;
		var p50_saturation = (zone_clastic_p50_satur11 == 0) ?  zone_carbo_p50_satur11 : zone_clastic_p50_satur11;
		$('#sgf_p50_sw').val(p50_saturation);
		$('#s2d_vol_p50_sw').val(p50_saturation);
		$('#sgv_vol_p50_sw').val(p50_saturation);
		$('#sgc_vol_p50_sw').val(p50_saturation);
		$('#sel_vol_p50_sw').val(p50_saturation);
		$('#rst_vol_p50_sw').val(p50_saturation);
		$('#sor_vol_p50_sw').val(p50_saturation);
		$('#s3d_vol_p50_sw').val(p50_saturation);
	}
		
	function setSurveySatur10()
	{
		var zone_clastic_p10_satur11 = ($('#zone_clastic_p10_satur11').val() != '') ? parseFloat($('#zone_clastic_p10_satur11').val()) : 0;
		var zone_carbo_p10_satur11 = ($('#zone_carbo_p10_satur11').val() != '') ? parseFloat($('#zone_carbo_p10_satur11').val()) : 0;
		var p10_saturation = (zone_clastic_p10_satur11 == 0) ?  zone_carbo_p10_satur11 : zone_clastic_p10_satur11;
		$('#sgf_p10_sw').val(p10_saturation);
		$('#s2d_vol_p10_sw').val(p10_saturation);
		$('#sgv_vol_p10_sw').val(p10_saturation);
		$('#sgc_vol_p10_sw').val(p10_saturation);
		$('#sel_vol_p10_sw').val(p10_saturation);
		$('#rst_vol_p10_sw').val(p10_saturation);
		$('#sor_vol_p10_sw').val(p10_saturation);
		$('#s3d_vol_p10_sw').val(p10_saturation);
	}
		
	function calculateSurveyP90()
	{
		var sgf_p90_area = ($('#sgf_p90_area').val() != '') ? parseFloat($('#sgf_p90_area').val()) : 0;
		var sgf_p90_thickness = ($('#sgf_p90_thickness').val() != '') ? parseFloat($('#sgf_p90_thickness').val()) : 0;
		var sgf_p90_net = ($('#sgf_p90_net').val() != '') ? parseFloat($('#sgf_p90_net').val()) : 0;
		var sgf_p90_porosity = ($('#sgf_p90_porosity').val() != '') ? parseFloat($('#sgf_p90_porosity').val()) : 0;
		var sgf_p90_sw = ($('#sgf_p90_sw').val() != '') ? parseFloat($('#sgf_p90_sw').val()) : 0;
		var sgf_p90_oil = 7758 * sgf_p90_area * (sgf_p90_thickness * (sgf_p90_net / 100)) * (sgf_p90_porosity / 100) * (sgf_p90_sw / 100) / 1000000;
		var sgf_p90_gas = 43560 * sgf_p90_area * (sgf_p90_thickness * (sgf_p90_net / 100)) * (sgf_p90_porosity / 100) * (sgf_p90_sw / 100) / 1000000000; 
		$('#sgf_p90_oil').val(sgf_p90_oil.toFixed(2));
		$('#sgf_p90_gas').val(sgf_p90_gas.toFixed(2));
		
		var s2d_vol_p90_area = ($('#s2d_vol_p90_area').val() != '') ? parseFloat($('#s2d_vol_p90_area').val()) : 0;
		var s2d_vol_p90_thickness = ($('#s2d_vol_p90_thickness').val() != '') ? parseFloat($('#s2d_vol_p90_thickness').val()) : 0;
		var s2d_vol_p90_porosity = ($('#s2d_vol_p90_porosity').val() != '') ? parseFloat($('#s2d_vol_p90_porosity').val()) : 0;
		var s2d_vol_p90_sw = ($('#s2d_vol_p90_sw').val() != '') ? parseFloat($('#s2d_vol_p90_sw').val()) : 0;
		var s2d_vol_p90_oil = 7758 * s2d_vol_p90_area * (s2d_vol_p90_thickness) * (s2d_vol_p90_porosity / 100) * (s2d_vol_p90_sw / 100) / 1000000;
		var s2d_vol_p90_gas = 43560 * s2d_vol_p90_area * (s2d_vol_p90_thickness) * (s2d_vol_p90_porosity / 100) * (s2d_vol_p90_sw / 100) / 1000000000; 
		$('#s2d_vol_p90_oil').val(s2d_vol_p90_oil.toFixed(2));
		$('#s2d_vol_p90_gas').val(s2d_vol_p90_gas.toFixed(2));
		
		var sgv_vol_p90_area = ($('#sgv_vol_p90_area').val() != '') ? parseFloat($('#sgv_vol_p90_area').val()) : 0;
		var sgv_vol_p90_thickness = ($('#sgv_vol_p90_thickness').val() != '') ? parseFloat($('#sgv_vol_p90_thickness').val()) : 0;
		var sgv_vol_p90_porosity = ($('#sgv_vol_p90_porosity').val() != '') ? parseFloat($('#sgv_vol_p90_porosity').val()) : 0;
		var sgv_vol_p90_sw = ($('#sgv_vol_p90_sw').val() != '') ? parseFloat($('#sgv_vol_p90_sw').val()) : 0;
		var sgv_vol_p90_oil = 7758 * sgv_vol_p90_area * (sgv_vol_p90_thickness) * (sgv_vol_p90_porosity / 100) * (sgv_vol_p90_sw / 100) / 1000000;
		var sgv_vol_p90_gas = 43560 * sgv_vol_p90_area * (sgv_vol_p90_thickness) * (sgv_vol_p90_porosity / 100) * (sgv_vol_p90_sw / 100) / 1000000000; 
		$('#sgv_vol_p90_oil').val(sgv_vol_p90_oil.toFixed(2));
		$('#sgv_vol_p90_gas').val(sgv_vol_p90_gas.toFixed(2));
		
		var sgc_vol_p90_area = ($('#sgc_vol_p90_area').val() != '') ? parseFloat($('#sgc_vol_p90_area').val()) : 0;
		var sgc_vol_p90_thickness = ($('#sgc_vol_p90_thickness').val() != '') ? parseFloat($('#sgc_vol_p90_thickness').val()) : 0;
		var sgc_vol_p90_porosity = ($('#sgc_vol_p90_porosity').val() != '') ? parseFloat($('#sgc_vol_p90_porosity').val()) : 0;
		var sgc_vol_p90_sw = ($('#sgc_vol_p90_sw').val() != '') ? parseFloat($('#sgc_vol_p90_sw').val()) : 0;
		var sgc_vol_p90_oil = 7758 * sgc_vol_p90_area * (sgc_vol_p90_thickness) * (sgc_vol_p90_porosity / 100) * (sgc_vol_p90_sw / 100) / 1000000;
		var sgc_vol_p90_gas = 43560 * sgc_vol_p90_area * (sgc_vol_p90_thickness) * (sgc_vol_p90_porosity / 100) * (sgc_vol_p90_sw / 100) / 1000000000; 
		$('#sgc_vol_p90_oil').val(sgc_vol_p90_oil.toFixed(2));
		$('#sgc_vol_p90_gas').val(sgc_vol_p90_gas.toFixed(2));
		
		var sel_vol_p90_area = ($('#sel_vol_p90_area').val() != '') ? parseFloat($('#sel_vol_p90_area').val()) : 0;
		var sel_vol_p90_thickness = ($('#sel_vol_p90_thickness').val() != '') ? parseFloat($('#sel_vol_p90_thickness').val()) : 0;
		var sel_vol_p90_porosity = ($('#sel_vol_p90_porosity').val() != '') ? parseFloat($('#sel_vol_p90_porosity').val()) : 0;
		var sel_vol_p90_sw = ($('#sel_vol_p90_sw').val() != '') ? parseFloat($('#sel_vol_p90_sw').val()) : 0;
		var sel_vol_p90_oil = 7758 * sel_vol_p90_area * (sel_vol_p90_thickness) * (sel_vol_p90_porosity / 100) * (sel_vol_p90_sw / 100) / 1000000;
		var sel_vol_p90_gas = 43560 * sel_vol_p90_area * (sel_vol_p90_thickness) * (sel_vol_p90_porosity / 100) * (sel_vol_p90_sw / 100) / 1000000000; 
		$('#sel_vol_p90_oil').val(sel_vol_p90_oil.toFixed(2));
		$('#sel_vol_p90_gas').val(sel_vol_p90_gas.toFixed(2));
		
		var rst_vol_p90_area = ($('#rst_vol_p90_area').val() != '') ? parseFloat($('#rst_vol_p90_area').val()) : 0;
		var rst_vol_p90_thickness = ($('#rst_vol_p90_thickness').val() != '') ? parseFloat($('#rst_vol_p90_thickness').val()) : 0;
		var rst_vol_p90_porosity = ($('#rst_vol_p90_porosity').val() != '') ? parseFloat($('#rst_vol_p90_porosity').val()) : 0;
		var rst_vol_p90_sw = ($('#rst_vol_p90_sw').val() != '') ? parseFloat($('#rst_vol_p90_sw').val()) : 0;
		var rst_vol_p90_oil = 7758 * rst_vol_p90_area * (rst_vol_p90_thickness) * (rst_vol_p90_porosity / 100) * (rst_vol_p90_sw / 100) / 1000000;
		var rst_vol_p90_gas = 43560 * rst_vol_p90_area * (rst_vol_p90_thickness) * (rst_vol_p90_porosity / 100) * (rst_vol_p90_sw / 100) / 1000000000; 
		$('#rst_vol_p90_oil').val(rst_vol_p90_oil.toFixed(2));
		$('#rst_vol_p90_gas').val(rst_vol_p90_gas.toFixed(2));
		
		var sor_vol_p90_area = ($('#sor_vol_p90_area').val() != '') ? parseFloat($('#sor_vol_p90_area').val()) : 0;
		var sor_vol_p90_thickness = ($('#sor_vol_p90_thickness').val() != '') ? parseFloat($('#sor_vol_p90_thickness').val()) : 0;
		var sor_vol_p90_porosity = ($('#sor_vol_p90_porosity').val() != '') ? parseFloat($('#sor_vol_p90_porosity').val()) : 0;
		var sor_vol_p90_sw = ($('#sor_vol_p90_sw').val() != '') ? parseFloat($('#sor_vol_p90_sw').val()) : 0;
		var sor_vol_p90_oil = 7758 * sor_vol_p90_area * (sor_vol_p90_thickness) * (sor_vol_p90_porosity / 100) * (sor_vol_p90_sw / 100) / 1000000;
		var sor_vol_p90_gas = 43560 * sor_vol_p90_area * (sor_vol_p90_thickness) * (sor_vol_p90_porosity / 100) * (sor_vol_p90_sw / 100) / 1000000000; 
		$('#sor_vol_p90_oil').val(sor_vol_p90_oil.toFixed(2));
		$('#sor_vol_p90_gas').val(sor_vol_p90_gas.toFixed(2));
		
		var s3d_vol_p90_area = ($('#s3d_vol_p90_area').val() != '') ? parseFloat($('#s3d_vol_p90_area').val()) : 0;
		var s3d_vol_p90_thickness = ($('#s3d_vol_p90_thickness').val() != '') ? parseFloat($('#s3d_vol_p90_thickness').val()) : 0;
		var s3d_vol_p90_porosity = ($('#s3d_vol_p90_porosity').val() != '') ? parseFloat($('#s3d_vol_p90_porosity').val()) : 0;
		var s3d_vol_p90_sw = ($('#s3d_vol_p90_sw').val() != '') ? parseFloat($('#s3d_vol_p90_sw').val()) : 0;
		var s3d_vol_p90_oil = 7758 * s3d_vol_p90_area * (s3d_vol_p90_thickness) * (s3d_vol_p90_porosity / 100) * (s3d_vol_p90_sw / 100) / 1000000;
		var s3d_vol_p90_gas = 43560 * s3d_vol_p90_area * (s3d_vol_p90_thickness) * (s3d_vol_p90_porosity / 100) * (s3d_vol_p90_sw / 100) / 1000000000; 
		$('#s3d_vol_p90_oil').val(s3d_vol_p90_oil.toFixed(2));
		$('#s3d_vol_p90_gas').val(s3d_vol_p90_gas.toFixed(2));
	}
		
	function calculateSurveyP50()
	{
		var sgf_p50_area = ($('#sgf_p50_area').val() != '') ? parseFloat($('#sgf_p50_area').val()) : 0;
		var sgf_p50_thickness = ($('#sgf_p50_thickness').val() != '') ? parseFloat($('#sgf_p50_thickness').val()) : 0;
		var sgf_p50_net = ($('#sgf_p50_net').val() != '') ? parseFloat($('#sgf_p50_net').val()) : 0;
		var sgf_p50_porosity = ($('#sgf_p50_porosity').val() != '') ? parseFloat($('#sgf_p50_porosity').val()) : 0;
		var sgf_p50_sw = ($('#sgf_p50_sw').val() != '') ? parseFloat($('#sgf_p50_sw').val()) : 0;
		var sgf_p50_oil = 7758 * sgf_p50_area * (sgf_p50_thickness * (sgf_p50_net / 100)) * (sgf_p50_porosity / 100) * (sgf_p50_sw / 100) / 1000000;
		var sgf_p50_gas = 43560 * sgf_p50_area * (sgf_p50_thickness * (sgf_p50_net / 100)) * (sgf_p50_porosity / 100) * (sgf_p50_sw / 100) / 1000000000; 
		$('#sgf_p50_oil').val(sgf_p50_oil.toFixed(2));
		$('#sgf_p50_gas').val(sgf_p50_gas.toFixed(2));
		
		var s2d_vol_p50_area = ($('#s2d_vol_p50_area').val() != '') ? parseFloat($('#s2d_vol_p50_area').val()) : 0;
		var s2d_vol_p50_thickness = ($('#s2d_vol_p50_thickness').val() != '') ? parseFloat($('#s2d_vol_p50_thickness').val()) : 0;
		var s2d_vol_p50_porosity = ($('#s2d_vol_p50_porosity').val() != '') ? parseFloat($('#s2d_vol_p50_porosity').val()) : 0;
		var s2d_vol_p50_sw = ($('#s2d_vol_p50_sw').val() != '') ? parseFloat($('#s2d_vol_p50_sw').val()) : 0;
		var s2d_vol_p50_oil = 7758 * s2d_vol_p50_area * (s2d_vol_p50_thickness) * (s2d_vol_p50_porosity / 100) * (s2d_vol_p50_sw / 100) / 1000000;
		var s2d_vol_p50_gas = 43560 * s2d_vol_p50_area * (s2d_vol_p50_thickness) * (s2d_vol_p50_porosity / 100) * (s2d_vol_p50_sw / 100) / 1000000000; 
		$('#s2d_vol_p50_oil').val(s2d_vol_p50_oil.toFixed(2));
		$('#s2d_vol_p50_gas').val(s2d_vol_p50_gas.toFixed(2));
		
		var sgv_vol_p50_area = ($('#sgv_vol_p50_area').val() != '') ? parseFloat($('#sgv_vol_p50_area').val()) : 0;
		var sgv_vol_p50_thickness = ($('#sgv_vol_p50_thickness').val() != '') ? parseFloat($('#sgv_vol_p50_thickness').val()) : 0;
		var sgv_vol_p50_porosity = ($('#sgv_vol_p50_porosity').val() != '') ? parseFloat($('#sgv_vol_p50_porosity').val()) : 0;
		var sgv_vol_p50_sw = ($('#sgv_vol_p50_sw').val() != '') ? parseFloat($('#sgv_vol_p50_sw').val()) : 0;
		var sgv_vol_p50_oil = 7758 * sgv_vol_p50_area * (sgv_vol_p50_thickness) * (sgv_vol_p50_porosity / 100) * (sgv_vol_p50_sw / 100) / 1000000;
		var sgv_vol_p50_gas = 43560 * sgv_vol_p50_area * (sgv_vol_p50_thickness) * (sgv_vol_p50_porosity / 100) * (sgv_vol_p50_sw / 100) / 1000000000; 
		$('#sgv_vol_p50_oil').val(sgv_vol_p50_oil.toFixed(2));
		$('#sgv_vol_p50_gas').val(sgv_vol_p50_gas.toFixed(2));
		
		var sgc_vol_p50_area = ($('#sgc_vol_p50_area').val() != '') ? parseFloat($('#sgc_vol_p50_area').val()) : 0;
		var sgc_vol_p50_thickness = ($('#sgc_vol_p50_thickness').val() != '') ? parseFloat($('#sgc_vol_p50_thickness').val()) : 0;
		var sgc_vol_p50_porosity = ($('#sgc_vol_p50_porosity').val() != '') ? parseFloat($('#sgc_vol_p50_porosity').val()) : 0;
		var sgc_vol_p50_sw = ($('#sgc_vol_p50_sw').val() != '') ? parseFloat($('#sgc_vol_p50_sw').val()) : 0;
		var sgc_vol_p50_oil = 7758 * sgc_vol_p50_area * (sgc_vol_p50_thickness) * (sgc_vol_p50_porosity / 100) * (sgc_vol_p50_sw / 100) / 1000000;
		var sgc_vol_p50_gas = 43560 * sgc_vol_p50_area * (sgc_vol_p50_thickness) * (sgc_vol_p50_porosity / 100) * (sgc_vol_p50_sw / 100) / 1000000000; 
		$('#sgc_vol_p50_oil').val(sgc_vol_p50_oil.toFixed(2));
		$('#sgc_vol_p50_gas').val(sgc_vol_p50_gas.toFixed(2));
		
		var sel_vol_p50_area = ($('#sel_vol_p50_area').val() != '') ? parseFloat($('#sel_vol_p50_area').val()) : 0;
		var sel_vol_p50_thickness = ($('#sel_vol_p50_thickness').val() != '') ? parseFloat($('#sel_vol_p50_thickness').val()) : 0;
		var sel_vol_p50_porosity = ($('#sel_vol_p50_porosity').val() != '') ? parseFloat($('#sel_vol_p50_porosity').val()) : 0;
		var sel_vol_p50_sw = ($('#sel_vol_p50_sw').val() != '') ? parseFloat($('#sel_vol_p50_sw').val()) : 0;
		var sel_vol_p50_oil = 7758 * sel_vol_p50_area * (sel_vol_p50_thickness) * (sel_vol_p50_porosity / 100) * (sel_vol_p50_sw / 100) / 1000000;
		var sel_vol_p50_gas = 43560 * sel_vol_p50_area * (sel_vol_p50_thickness) * (sel_vol_p50_porosity / 100) * (sel_vol_p50_sw / 100) / 1000000000; 
		$('#sel_vol_p50_oil').val(sel_vol_p50_oil.toFixed(2));
		$('#sel_vol_p50_gas').val(sel_vol_p50_gas.toFixed(2));
		
		var rst_vol_p50_area = ($('#rst_vol_p50_area').val() != '') ? parseFloat($('#rst_vol_p50_area').val()) : 0;
		var rst_vol_p50_thickness = ($('#rst_vol_p50_thickness').val() != '') ? parseFloat($('#rst_vol_p50_thickness').val()) : 0;
		var rst_vol_p50_porosity = ($('#rst_vol_p50_porosity').val() != '') ? parseFloat($('#rst_vol_p50_porosity').val()) : 0;
		var rst_vol_p50_sw = ($('#rst_vol_p50_sw').val() != '') ? parseFloat($('#rst_vol_p50_sw').val()) : 0;
		var rst_vol_p50_oil = 7758 * rst_vol_p50_area * (rst_vol_p50_thickness) * (rst_vol_p50_porosity / 100) * (rst_vol_p50_sw / 100) / 1000000;
		var rst_vol_p50_gas = 43560 * rst_vol_p50_area * (rst_vol_p50_thickness) * (rst_vol_p50_porosity / 100) * (rst_vol_p50_sw / 100) / 1000000000; 
		$('#rst_vol_p50_oil').val(rst_vol_p50_oil.toFixed(2));
		$('#rst_vol_p50_gas').val(rst_vol_p50_gas.toFixed(2));
		
		var sor_vol_p50_area = ($('#sor_vol_p50_area').val() != '') ? parseFloat($('#sor_vol_p50_area').val()) : 0;
		var sor_vol_p50_thickness = ($('#sor_vol_p50_thickness').val() != '') ? parseFloat($('#sor_vol_p50_thickness').val()) : 0;
		var sor_vol_p50_porosity = ($('#sor_vol_p50_porosity').val() != '') ? parseFloat($('#sor_vol_p50_porosity').val()) : 0;
		var sor_vol_p50_sw = ($('#sor_vol_p50_sw').val() != '') ? parseFloat($('#sor_vol_p50_sw').val()) : 0;
		var sor_vol_p50_oil = 7758 * sor_vol_p50_area * (sor_vol_p50_thickness) * (sor_vol_p50_porosity / 100) * (sor_vol_p50_sw / 100) / 1000000;
		var sor_vol_p50_gas = 43560 * sor_vol_p50_area * (sor_vol_p50_thickness) * (sor_vol_p50_porosity / 100) * (sor_vol_p50_sw / 100) / 1000000000; 
		$('#sor_vol_p50_oil').val(sor_vol_p50_oil.toFixed(2));
		$('#sor_vol_p50_gas').val(sor_vol_p50_gas.toFixed(2));
		
		var s3d_vol_p50_area = ($('#s3d_vol_p50_area').val() != '') ? parseFloat($('#s3d_vol_p50_area').val()) : 0;
		var s3d_vol_p50_thickness = ($('#s3d_vol_p50_thickness').val() != '') ? parseFloat($('#s3d_vol_p50_thickness').val()) : 0;
		var s3d_vol_p50_porosity = ($('#s3d_vol_p50_porosity').val() != '') ? parseFloat($('#s3d_vol_p50_porosity').val()) : 0;
		var s3d_vol_p50_sw = ($('#s3d_vol_p50_sw').val() != '') ? parseFloat($('#s3d_vol_p50_sw').val()) : 0;
		var s3d_vol_p50_oil = 7758 * s3d_vol_p50_area * (s3d_vol_p50_thickness) * (s3d_vol_p50_porosity / 100) * (s3d_vol_p50_sw / 100) / 1000000;
		var s3d_vol_p50_gas = 43560 * s3d_vol_p50_area * (s3d_vol_p50_thickness) * (s3d_vol_p50_porosity / 100) * (s3d_vol_p50_sw / 100) / 1000000000; 
		$('#s3d_vol_p50_oil').val(s3d_vol_p50_oil.toFixed(2));
		$('#s3d_vol_p50_gas').val(s3d_vol_p50_gas.toFixed(2));
	}
		
	function calculateSurveyP10()
	{
		var sgf_p10_area = ($('#sgf_p10_area').val() != '') ? parseFloat($('#sgf_p10_area').val()) : 0;
		var sgf_p10_thickness = ($('#sgf_p10_thickness').val() != '') ? parseFloat($('#sgf_p10_thickness').val()) : 0;
		var sgf_p10_net = ($('#sgf_p10_net').val() != '') ? parseFloat($('#sgf_p10_net').val()) : 0;
		var sgf_p10_porosity = ($('#sgf_p10_porosity').val() != '') ? parseFloat($('#sgf_p10_porosity').val()) : 0;
		var sgf_p10_sw = ($('#sgf_p10_sw').val() != '') ? parseFloat($('#sgf_p10_sw').val()) : 0;
		var sgf_p10_oil = 7758 * sgf_p10_area * (sgf_p10_thickness * (sgf_p10_net / 100)) * (sgf_p10_porosity / 100) * (sgf_p10_sw / 100) / 1000000;
		var sgf_p10_gas = 43560 * sgf_p10_area * (sgf_p10_thickness * (sgf_p10_net / 100)) * (sgf_p10_porosity / 100) * (sgf_p10_sw / 100) / 1000000000; 
		$('#sgf_p10_oil').val(sgf_p10_oil.toFixed(2));
		$('#sgf_p10_gas').val(sgf_p10_gas.toFixed(2));
		
		var s2d_vol_p10_area = ($('#s2d_vol_p10_area').val() != '') ? parseFloat($('#s2d_vol_p10_area').val()) : 0;
		var s2d_vol_p10_thickness = ($('#s2d_vol_p10_thickness').val() != '') ? parseFloat($('#s2d_vol_p10_thickness').val()) : 0;
		var s2d_vol_p10_porosity = ($('#s2d_vol_p10_porosity').val() != '') ? parseFloat($('#s2d_vol_p10_porosity').val()) : 0;
		var s2d_vol_p10_sw = ($('#s2d_vol_p10_sw').val() != '') ? parseFloat($('#s2d_vol_p10_sw').val()) : 0;
		var s2d_vol_p10_oil = 7758 * s2d_vol_p10_area * (s2d_vol_p10_thickness) * (s2d_vol_p10_porosity / 100) * (s2d_vol_p10_sw / 100) / 1000000;
		var s2d_vol_p10_gas = 43560 * s2d_vol_p10_area * (s2d_vol_p10_thickness) * (s2d_vol_p10_porosity / 100) * (s2d_vol_p10_sw / 100) / 1000000000; 
		$('#s2d_vol_p10_oil').val(s2d_vol_p10_oil.toFixed(2));
		$('#s2d_vol_p10_gas').val(s2d_vol_p10_gas.toFixed(2));
		
		var sgv_vol_p10_area = ($('#sgv_vol_p10_area').val() != '') ? parseFloat($('#sgv_vol_p10_area').val()) : 0;
		var sgv_vol_p10_thickness = ($('#sgv_vol_p10_thickness').val() != '') ? parseFloat($('#sgv_vol_p10_thickness').val()) : 0;
		var sgv_vol_p10_porosity = ($('#sgv_vol_p10_porosity').val() != '') ? parseFloat($('#sgv_vol_p10_porosity').val()) : 0;
		var sgv_vol_p10_sw = ($('#sgv_vol_p10_sw').val() != '') ? parseFloat($('#sgv_vol_p10_sw').val()) : 0;
		var sgv_vol_p10_oil = 7758 * sgv_vol_p10_area * (sgv_vol_p10_thickness) * (sgv_vol_p10_porosity / 100) * (sgv_vol_p10_sw / 100) / 1000000;
		var sgv_vol_p10_gas = 43560 * sgv_vol_p10_area * (sgv_vol_p10_thickness) * (sgv_vol_p10_porosity / 100) * (sgv_vol_p10_sw / 100) / 1000000000; 
		$('#sgv_vol_p10_oil').val(sgv_vol_p10_oil.toFixed(2));
		$('#sgv_vol_p10_gas').val(sgv_vol_p10_gas.toFixed(2));
		
		var sgc_vol_p10_area = ($('#sgc_vol_p10_area').val() != '') ? parseFloat($('#sgc_vol_p10_area').val()) : 0;
		var sgc_vol_p10_thickness = ($('#sgc_vol_p10_thickness').val() != '') ? parseFloat($('#sgc_vol_p10_thickness').val()) : 0;
		var sgc_vol_p10_porosity = ($('#sgc_vol_p10_porosity').val() != '') ? parseFloat($('#sgc_vol_p10_porosity').val()) : 0;
		var sgc_vol_p10_sw = ($('#sgc_vol_p10_sw').val() != '') ? parseFloat($('#sgc_vol_p10_sw').val()) : 0;
		var sgc_vol_p10_oil = 7758 * sgc_vol_p10_area * (sgc_vol_p10_thickness) * (sgc_vol_p10_porosity / 100) * (sgc_vol_p10_sw / 100) / 1000000;
		var sgc_vol_p10_gas = 43560 * sgc_vol_p10_area * (sgc_vol_p10_thickness) * (sgc_vol_p10_porosity / 100) * (sgc_vol_p10_sw / 100) / 1000000000; 
		$('#sgc_vol_p10_oil').val(sgc_vol_p10_oil.toFixed(2));
		$('#sgc_vol_p10_gas').val(sgc_vol_p10_gas.toFixed(2));
		
		var sel_vol_p10_area = ($('#sel_vol_p10_area').val() != '') ? parseFloat($('#sel_vol_p10_area').val()) : 0;
		var sel_vol_p10_thickness = ($('#sel_vol_p10_thickness').val() != '') ? parseFloat($('#sel_vol_p10_thickness').val()) : 0;
		var sel_vol_p10_porosity = ($('#sel_vol_p10_porosity').val() != '') ? parseFloat($('#sel_vol_p10_porosity').val()) : 0;
		var sel_vol_p10_sw = ($('#sel_vol_p10_sw').val() != '') ? parseFloat($('#sel_vol_p10_sw').val()) : 0;
		var sel_vol_p10_oil = 7758 * sel_vol_p10_area * (sel_vol_p10_thickness) * (sel_vol_p10_porosity / 100) * (sel_vol_p10_sw / 100) / 1000000;
		var sel_vol_p10_gas = 43560 * sel_vol_p10_area * (sel_vol_p10_thickness) * (sel_vol_p10_porosity / 100) * (sel_vol_p10_sw / 100) / 1000000000; 
		$('#sel_vol_p10_oil').val(sel_vol_p10_oil.toFixed(2));
		$('#sel_vol_p10_gas').val(sel_vol_p10_gas.toFixed(2));
		
		var rst_vol_p10_area = ($('#rst_vol_p10_area').val() != '') ? parseFloat($('#rst_vol_p10_area').val()) : 0;
		var rst_vol_p10_thickness = ($('#rst_vol_p10_thickness').val() != '') ? parseFloat($('#rst_vol_p10_thickness').val()) : 0;
		var rst_vol_p10_porosity = ($('#rst_vol_p10_porosity').val() != '') ? parseFloat($('#rst_vol_p10_porosity').val()) : 0;
		var rst_vol_p10_sw = ($('#rst_vol_p10_sw').val() != '') ? parseFloat($('#rst_vol_p10_sw').val()) : 0;
		var rst_vol_p10_oil = 7758 * rst_vol_p10_area * (rst_vol_p10_thickness) * (rst_vol_p10_porosity / 100) * (rst_vol_p10_sw / 100) / 1000000;
		var rst_vol_p10_gas = 43560 * rst_vol_p10_area * (rst_vol_p10_thickness) * (rst_vol_p10_porosity / 100) * (rst_vol_p10_sw / 100) / 1000000000; 
		$('#rst_vol_p10_oil').val(rst_vol_p10_oil.toFixed(2));
		$('#rst_vol_p10_gas').val(rst_vol_p10_gas.toFixed(2));
		
		var sor_vol_p10_area = ($('#sor_vol_p10_area').val() != '') ? parseFloat($('#sor_vol_p10_area').val()) : 0;
		var sor_vol_p10_thickness = ($('#sor_vol_p10_thickness').val() != '') ? parseFloat($('#sor_vol_p10_thickness').val()) : 0;
		var sor_vol_p10_porosity = ($('#sor_vol_p10_porosity').val() != '') ? parseFloat($('#sor_vol_p10_porosity').val()) : 0;
		var sor_vol_p10_sw = ($('#sor_vol_p10_sw').val() != '') ? parseFloat($('#sor_vol_p10_sw').val()) : 0;
		var sor_vol_p10_oil = 7758 * sor_vol_p10_area * (sor_vol_p10_thickness) * (sor_vol_p10_porosity / 100) * (sor_vol_p10_sw / 100) / 1000000;
		var sor_vol_p10_gas = 43560 * sor_vol_p10_area * (sor_vol_p10_thickness) * (sor_vol_p10_porosity / 100) * (sor_vol_p10_sw / 100) / 1000000000; 
		$('#sor_vol_p10_oil').val(sor_vol_p10_oil.toFixed(2));
		$('#sor_vol_p10_gas').val(sor_vol_p10_gas.toFixed(2));
		
		var s3d_vol_p10_area = ($('#s3d_vol_p10_area').val() != '') ? parseFloat($('#s3d_vol_p10_area').val()) : 0;
		var s3d_vol_p10_thickness = ($('#s3d_vol_p10_thickness').val() != '') ? parseFloat($('#s3d_vol_p10_thickness').val()) : 0;
		var s3d_vol_p10_porosity = ($('#s3d_vol_p10_porosity').val() != '') ? parseFloat($('#s3d_vol_p10_porosity').val()) : 0;
		var s3d_vol_p10_sw = ($('#s3d_vol_p10_sw').val() != '') ? parseFloat($('#s3d_vol_p10_sw').val()) : 0;
		var s3d_vol_p10_oil = 7758 * s3d_vol_p10_area * (s3d_vol_p10_thickness) * (s3d_vol_p10_porosity / 100) * (s3d_vol_p10_sw / 100) / 1000000;
		var s3d_vol_p10_gas = 43560 * s3d_vol_p10_area * (s3d_vol_p10_thickness) * (s3d_vol_p10_porosity / 100) * (s3d_vol_p10_sw / 100) / 1000000000; 
		$('#s3d_vol_p10_oil').val(s3d_vol_p10_oil.toFixed(2));
		$('#s3d_vol_p10_gas').val(s3d_vol_p10_gas.toFixed(2));
	}
		
	function calculate2dNetToGross()
	{
		var dav_2dss_gross_resthickness = ($('#dav_2dss_gross_resthickness').val() != '') ? parseFloat($('#dav_2dss_gross_resthickness').val()) : 0;
		var s2d_net_p90_thickness = ($('#s2d_net_p90_thickness').val() != '') ? parseFloat($('#s2d_net_p90_thickness').val()) : 0;
		var s2d_net_p50_thickness = ($('#s2d_net_p50_thickness').val() != '') ? parseFloat($('#s2d_net_p50_thickness').val()) : 0;
		var s2d_net_p10_thickness = ($('#s2d_net_p10_thickness').val() != '') ? parseFloat($('#s2d_net_p10_thickness').val()) : 0;
		var s2d_net_p90_net_to_gross = s2d_net_p90_thickness / dav_2dss_gross_resthickness;
		var s2d_net_p50_net_to_gross = s2d_net_p50_thickness / dav_2dss_gross_resthickness;
		var s2d_net_p10_net_to_gross = s2d_net_p10_thickness / dav_2dss_gross_resthickness;
		$('#s2d_net_p90_net_to_gross').val(s2d_net_p90_net_to_gross.toFixed(2));
		$('#s2d_net_p50_net_to_gross').val(s2d_net_p50_net_to_gross.toFixed(2));
		$('#s2d_net_p10_net_to_gross').val(s2d_net_p10_net_to_gross.toFixed(2));
	}

	function calculate3dNetToGross()
	{
		var dav_3dss_grass_resthickness = ($('#dav_3dss_grass_resthickness').val() != '') ? parseFloat($('#dav_3dss_grass_resthickness').val()) : 0;
		var s3d_net_p90_thickness = ($('#s3d_net_p90_thickness').val() != '') ? parseFloat($('#s3d_net_p90_thickness').val()) : 0;
		var s3d_net_p50_thickness = ($('#s3d_net_p50_thickness').val() != '') ? parseFloat($('#s3d_net_p50_thickness').val()) : 0;
		var s3d_net_p10_thickness = ($('#s3d_net_p10_thickness').val() != '') ? parseFloat($('#s3d_net_p10_thickness').val()) : 0;
		var s3d_net_p90_net_to_gross = s3d_net_p90_thickness / dav_3dss_grass_resthickness;
		var s3d_net_p50_net_to_gross = s3d_net_p50_thickness / dav_3dss_grass_resthickness;
		var s3d_net_p10_net_to_gross = s3d_net_p10_thickness / dav_3dss_grass_resthickness;
		$('#s3d_net_p90_net_to_gross').val(s3d_net_p90_net_to_gross.toFixed(2));
		$('#s3d_net_p50_net_to_gross').val(s3d_net_p50_net_to_gross.toFixed(2));
		$('#s3d_net_p10_net_to_gross').val(s3d_net_p10_net_to_gross.toFixed(2));
	}
		
	function setThicknessP90()
	{
		var s2d_net_p90_thickness = ($('#s2d_net_p90_thickness').val() != '') ? parseFloat($('#s2d_net_p90_thickness').val()) : 0;
		var s3d_net_p90_thickness = ($('#s3d_net_p90_thickness').val() != '') ? parseFloat($('#s3d_net_p90_thickness').val()) : 0;
		var p90_thickness = (s3d_net_p90_thickness == 0) ?  s2d_net_p90_thickness : s3d_net_p90_thickness;
		
		$('#s2d_vol_p90_thickness').val(s2d_net_p90_thickness);
		$('#s3d_vol_p90_thickness').val(s3d_net_p90_thickness);
		
		$('#sgv_vol_p90_thickness').val(p90_thickness);
		$('#sgc_vol_p90_thickness').val(p90_thickness);
		$('#sel_vol_p90_thickness').val(p90_thickness);
		$('#rst_vol_p90_thickness').val(p90_thickness);
		$('#sor_vol_p90_thickness').val(p90_thickness);
	}
		
	function setThicknessP50()
	{
		var s2d_net_p50_thickness = ($('#s2d_net_p50_thickness').val() != '') ? parseFloat($('#s2d_net_p50_thickness').val()) : 0;
		var s3d_net_p50_thickness = ($('#s3d_net_p50_thickness').val() != '') ? parseFloat($('#s3d_net_p50_thickness').val()) : 0;
		var p50_thickness = (s3d_net_p50_thickness == 0) ?  s2d_net_p50_thickness : s3d_net_p50_thickness;
		
		$('#s2d_vol_p50_thickness').val(s2d_net_p50_thickness);
		$('#s3d_vol_p50_thickness').val(s3d_net_p50_thickness);
		
		$('#sgv_vol_p50_thickness').val(p50_thickness);
		$('#sgc_vol_p50_thickness').val(p50_thickness);
		$('#sel_vol_p50_thickness').val(p50_thickness);
		$('#rst_vol_p50_thickness').val(p50_thickness);
		$('#sor_vol_p50_thickness').val(p50_thickness);
	}
		
	function setThicknessP10()
	{
		var s2d_net_p10_thickness = ($('#s2d_net_p10_thickness').val() != '') ? parseFloat($('#s2d_net_p10_thickness').val()) : 0;
		var s3d_net_p10_thickness = ($('#s3d_net_p10_thickness').val() != '') ? parseFloat($('#s3d_net_p10_thickness').val()) : 0;
		var p10_thickness = (s3d_net_p10_thickness == 0) ?  s2d_net_p10_thickness : s3d_net_p10_thickness;
		
		$('#s2d_vol_p10_thickness').val(s2d_net_p10_thickness);
		$('#s3d_vol_p10_thickness').val(s3d_net_p10_thickness);
		
		$('#sgv_vol_p10_thickness').val(p10_thickness);
		$('#sgc_vol_p10_thickness').val(p10_thickness);
		$('#sel_vol_p10_thickness').val(p10_thickness);
		$('#rst_vol_p10_thickness').val(p10_thickness);
		$('#sor_vol_p10_thickness').val(p10_thickness);
	}
	
");
?>

<?php 
	Yii::app()->clientScript->registerScript('formationname', "
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/playname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('#plyName').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.play,
					escapeMarkup: function(m) {
                        // Do not escape HTML in the select options text
                        return m;
                    },
				});
			},
		});	
		
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/wellnamediscovery') ."',
			type: 'POST',
			data: {
				prospect_id : '',
				prospect_type : 'Discovery',
			},
			dataType: 'json',
			success : function(data) {
				$('#well_name').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.wellname,
					multiple : true
				});
			},
		});
		
		$.ajax({
			url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
			type: 'POST',
			data: '',
			dataType: 'json',
			success : function(data) {
				$('.formationname').select2({
					placeholder: 'Pilih',
					allowClear: true,
					data : data.formation,
				});
		
				disable('gcfsrock');
				disable('gcfres');
				disable('gcftrap');
			},
		});	
		
		function disable(id) {
        	switch(id) {
				case 'gcfsrock' :
					if($('#' + id).val() == '') {
                		mati(['#sourceformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#sourceformationname']);
                	}
                	break;
                case 'gcfres' :
                		mati2(['#reservoirformationname']);
                	break;
                case 'gcftrap' :
					if($('#' + id).val() == '') {
                		mati(['#sealingformationname']);
                	} else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                		nyala(['#sealingformationname']);
                	}
                	break;
            }
        }
                		
        function mati(target) {
        	$(target).select2(\"readonly\", true);
            $(target).select2(\"val\", \"\");
		}
		
		function mati2(target) {
			$(target).select2(\"readonly\", true);
		}
                		
        function nyala(target) {
             $(target).select2(\"enable\", true);
             $(target).select2(\"readonly\", false);
		}
                		
        $('#gcfsrock').on('change', function(e) {
			disable('gcfsrock');
		});
        $('#gcfres').on('change', function(e) {
			disable('gcfres');
		});
        $('#gcftrap').on('change', function(e) {
			disable('gcftrap');
		});
	");
	
	Yii::app()->clientScript->registerScript('ajaxupdate', "
		$('#discovery-grid a.ajaxupdate').live('click', function() {
            $.ajax({
				url : $(this).attr('href'),
				type: 'POST',
				data: '',
				dataType: 'json',
				success : function(data) {
// 					if(data.error === false) {
                        if(data.prospect_update_from != '') $('#prospect_update_from').val(data.prospect_update_from);
                        if(data.prospect_submit_revision != '') $('#prospect_submit_revision').val(data.prospect_submit_revision);
						$('#structure_name').val(data.structure_name);
                        $('#plyName').val(data.play_id);
                        $('#s2id__clarifiedby').select2('val', data.prospect_clarified);
                        $('#_seismicyear').val(data.prospect_year_seismic);
                        $('#s2id__processingtype').select2('val', data.prospect_processing_type);        		
                        $('#_latestyearpro').val(data.prospect_year_late);
                        $('#_revisedprosname').val(data.dc_name_revise);
                        $('#d9').val(data.dc_name_revise_date);
                        $('#d7').val(data.prospect_date_initiate);
                        $('#Prospect_center_lat_degree').val(data.center_lat_degree);
                        $('#Prospect_center_lat_minute').val(data.center_lat_minute);
                        $('#Prospect_center_lat_second').val(data.center_lat_second);
                        $('#Prospect_center_lat_direction').val(data.center_lat_direction); 
                        $('#Prospect_center_long_degree').val(data.center_long_degree);
                        $('#Prospect_center_long_minute').val(data.center_long_minute);
                        $('#Prospect_center_long_second').val(data.center_long_second);
                        $('#Prospect_center_long_direction').val(data.center_long_direction);
                        $('#s2id_env_onoffshore').select2('val', data.prospect_shore);
                        $('#s2id_env_terrain').select2('val', data.prospect_terrain);        		
                        $('#n_facility').select2('val', data.prospect_near_field);
                        $('#n_developmentwell').select2('val', data.prospect_near_infra_structure);
                        $('#s2id_w').select2('val', data.dc_total_well); 
                            		
                        $('#well_name1').val(data.wdc_name1);                      
                        $('#lat_degree1').val(data.lat_degree1);
                        $('#lat_minute1').val(data.lat_minute1);
                        $('#lat_second1').val(data.lat_second1);
                        $('#lat_direction1').val(data.lat_direction1); 
                        $('#long_degree1').val(data.long_degree1);
                        $('#long_minute1').val(data.long_minute1);
                        $('#long_second1').val(data.long_second1);
                        $('#long_direction1').val(data.long_direction1); 
                        $('#s2id_well_type1').select2('val', data.wdc_type1);
                        $('#s2id_onoffshore1').select2('val', data.wdc_shore1); 
                        $('#s2id_terrain1').select2('val', data.wdc_terrain1);
                        $('#s2id_well_status1').select2('val', data.wdc_status1);
                        $('#s2id_targetedformationname1').select2('val', data.wdc_formation1);
                        $('#dsw8_1').val(data.wdc_date_complete1);
                        $('#ttd_11').val(data.wdc_target_depth_tvd1);
                        $('#ttd_21').val(data.wdc_target_depth_md1);
                        $('#actual_total_depth1').val(data.wdc_actual_depth1); 
                        $('#tpprt_1').val(data.wdc_target_play1);
                        $('#apprt_1').val(data.wdc_actual_play1);                            		
                        $('#s2id_num_mdt1').select2('val', data.wdc_number_mdt1);
                        $('#s2id_num_rft1').select2('val', data.wdc_number_rft1);
                        $('#rip_1').val(data.wdc_res_pressure1);
                        $('#lpr_1').val(data.wdc_last_pressure1);
                        $('#pressure_gradient1').val(data.wdc_pressure_gradient1);
                        $('#lrt_1').val(data.wdc_last_temp1);       
                        $('#s2id_actual_well_integrity1').select2('val', data.wdc_integrity1);
                        $('#s2id_availability_electrolog1').select2('val', data.wdc_electro_avail1);
                        $('#wdc_electro_list11').val(data.wdc_electro_list11);
                        $('#wdc_electro_list21').val(data.wdc_electro_list21);
                        $('#wdc_electro_list31').val(data.wdc_electro_list31);
                        $('#wdc_electro_list41').val(data.wdc_electro_list41); 
                        $('#wdc_electro_list51').val(data.wdc_electro_list51);
                        $('#wdc_electro_list61').val(data.wdc_electro_list61);
                        $('#wdc_electro_list71').val(data.wdc_electro_list71);
                        $('#wdc_electro_list81').val(data.wdc_electro_list81);
                        $('#wdc_electro_list91').val(data.wdc_electro_list91);
                        $('#wdc_electro_list101').val(data.wdc_electro_list101);
                        $('#wdc_electro_list111').val(data.wdc_electro_list111);
                        $('#wdc_electro_list121').val(data.wdc_electro_list121); 
                        $('#wdc_electro_list131').val(data.wdc_electro_list131);
                        $('#wdc_electro_list141').val(data.wdc_electro_list141);
                        $('#wdc_electro_list151').val(data.wdc_electro_list151);
                        $('#wdc_electro_list161').val(data.wdc_electro_list161);
                        $('#s2id_wz_1').select2('val', data.wdc_total_zone1);
                            		
                        $('#well_name2').val(data.wdc_name2);                      
                        $('#lat_degree2').val(data.lat_degree2);
                        $('#lat_minute2').val(data.lat_minute2);
                        $('#lat_second2').val(data.lat_second2);
                        $('#lat_direction2').val(data.lat_direction2); 
                        $('#long_degree2').val(data.long_degree2);
                        $('#long_minute2').val(data.long_minute2);
                        $('#long_second2').val(data.long_second2);
                        $('#long_direction2').val(data.long_direction2); 
                        $('#s2id_well_type2').select2('val', data.wdc_type2);
                        $('#s2id_onoffshore2').select2('val', data.wdc_shore2); 
                        $('#s2id_terrain2').select2('val', data.wdc_terrain2);
                        $('#s2id_well_status2').select2('val', data.wdc_status2);
                        $('#s2id_targetedformationname2').select2('val', data.wdc_formation2);
                        $('#dsw8_2').val(data.wdc_date_complete2);
                        $('#ttd_12').val(data.wdc_target_depth_tvd2);
                        $('#ttd_22').val(data.wdc_target_depth_md2);
                        $('#actual_total_depth2').val(data.wdc_actual_depth2); 
                        $('#tpprt_2').val(data.wdc_target_play2);
                        $('#apprt_2').val(data.wdc_actual_play2);                            		
                        $('#s2id_num_mdt2').select2('val', data.wdc_number_mdt2);
                        $('#s2id_num_rft2').select2('val', data.wdc_number_rft2);
                        $('#rip_2').val(data.wdc_res_pressure2);
                        $('#lpr_2').val(data.wdc_last_pressure2);
                        $('#pressure_gradient2').val(data.wdc_pressure_gradient2);
                        $('#lrt_2').val(data.wdc_last_temp2);       
                        $('#s2id_actual_well_integrity2').select2('val', data.wdc_integrity2);
                        $('#s2id_availability_electrolog2').select2('val', data.wdc_electro_avail2);
                        $('#wdc_electro_list12').val(data.wdc_electro_list12);
                        $('#wdc_electro_list22').val(data.wdc_electro_list22);
                        $('#wdc_electro_list32').val(data.wdc_electro_list32);
                        $('#wdc_electro_list42').val(data.wdc_electro_list42); 
                        $('#wdc_electro_list52').val(data.wdc_electro_list52);
                        $('#wdc_electro_list62').val(data.wdc_electro_list62);
                        $('#wdc_electro_list72').val(data.wdc_electro_list72);
                        $('#wdc_electro_list82').val(data.wdc_electro_list82);
                        $('#wdc_electro_list92').val(data.wdc_electro_list92);
                        $('#wdc_electro_list102').val(data.wdc_electro_list102);
                        $('#wdc_electro_list112').val(data.wdc_electro_list112);
                        $('#wdc_electro_list122').val(data.wdc_electro_list122); 
                        $('#wdc_electro_list132').val(data.wdc_electro_list132);
                        $('#wdc_electro_list142').val(data.wdc_electro_list142);
                        $('#wdc_electro_list152').val(data.wdc_electro_list152);
                        $('#wdc_electro_list162').val(data.wdc_electro_list162);
                        $('#s2id_wz_2').select2('val', data.wdc_total_zone2);
                            		
                        $('#well_name3').val(data.wdc_name3);                      
                        $('#lat_degree3').val(data.lat_degree3);
                        $('#lat_minute3').val(data.lat_minute3);
                        $('#lat_second3').val(data.lat_second3);
                        $('#lat_direction3').val(data.lat_direction3); 
                        $('#long_degree3').val(data.long_degree3);
                        $('#long_minute3').val(data.long_minute3);
                        $('#long_second3').val(data.long_second3);
                        $('#long_direction3').val(data.long_direction3); 
                        $('#s2id_well_type').select2('val', data.wdc_type3);
                        $('#s2id_onoffshore3').select2('val', data.wdc_shore3); 
                        $('#s2id_terrain3').select2('val', data.wdc_terrain3);
                        $('#s2id_well_status3').select2('val', data.wdc_status3);
                        $('#s2id_targetedformationname3').select2('val', data.wdc_formation3);
                        $('#dsw8_3').val(data.wdc_date_complete3);
                        $('#ttd_13').val(data.wdc_target_depth_tvd3);
                        $('#ttd_23').val(data.wdc_target_depth_md3);
                        $('#actual_total_depth3').val(data.wdc_actual_depth3); 
                        $('#tpprt_3').val(data.wdc_target_play3);
                        $('#apprt_3').val(data.wdc_actual_play3);
                        $('#s2id_num_mdt3').select2('val', data.wdc_number_mdt3);
                        $('#s2id_num_rft3').select2('val', data.wdc_number_rft3);
                        $('#rip_3').val(data.wdc_res_pressure3);
                        $('#lpr_3').val(data.wdc_last_pressure3);
                        $('#pressure_gradient3').val(data.wdc_pressure_gradient3);
                        $('#lrt_3').val(data.wdc_last_temp3);
                        $('#s2id_actual_well_integrity3').select2('val', data.wdc_integrity3);
                        $('#s2id_availability_electrolog3').select2('val', data.wdc_electro_avail3);
                        $('#wdc_electro_list13').val(data.wdc_electro_list13);
                        $('#wdc_electro_list23').val(data.wdc_electro_list23);
                        $('#wdc_electro_list33').val(data.wdc_electro_list33);
                        $('#wdc_electro_list43').val(data.wdc_electro_list43); 
                        $('#wdc_electro_list53').val(data.wdc_electro_list53);
                        $('#wdc_electro_list63').val(data.wdc_electro_list63);
                        $('#wdc_electro_list73').val(data.wdc_electro_list73);
                        $('#wdc_electro_list83').val(data.wdc_electro_list83);
                        $('#wdc_electro_list93').val(data.wdc_electro_list93);
                        $('#wdc_electro_list103').val(data.wdc_electro_list103);
                        $('#wdc_electro_list113').val(data.wdc_electro_list113);
                        $('#wdc_electro_list123').val(data.wdc_electro_list123); 
                        $('#wdc_electro_list133').val(data.wdc_electro_list133);
                        $('#wdc_electro_list143').val(data.wdc_electro_list143);
                        $('#wdc_electro_list153').val(data.wdc_electro_list153);
                        $('#wdc_electro_list163').val(data.wdc_electro_list163);
                        $('#s2id_wz_3').select2('val', data.wdc_total_zone3);
                            		
                        $('#well_name4').val(data.wdc_name4);                      
                        $('#lat_degree4').val(data.lat_degree4);
                        $('#lat_minute4').val(data.lat_minute4);
                        $('#lat_second4').val(data.lat_second4);
                        $('#lat_direction4').val(data.lat_direction4); 
                        $('#long_degree4').val(data.long_degree4);
                        $('#long_minute4').val(data.long_minute4);
                        $('#long_second4').val(data.long_second4);
                        $('#long_direction4').val(data.long_direction4);
                        $('#s2id_well_type4').select2('val', data.wdc_type4);
                        $('#s2id_onoffshore4').select2('val', data.wdc_shore4); 
                        $('#s2id_terrain4').select2('val', data.wdc_terrain4);
                        $('#s2id_well_status4').select2('val', data.wdc_status4);
                        $('#s2id_targetedformationname4').select2('val', data.wdc_formation4);
                        $('#dsw8_4').val(data.wdc_date_complete4);
                        $('#ttd_14').val(data.wdc_target_depth_tvd4);
                        $('#ttd_24').val(data.wdc_target_depth_md4);
                        $('#actual_total_depth4').val(data.wdc_actual_depth4); 
                        $('#tpprt_4').val(data.wdc_target_play4);
                        $('#apprt_4').val(data.wdc_actual_play4);              		
                        $('#s2id_num_mdt4').select2('val', data.wdc_number_mdt4);
                        $('#s2id_num_rft4').select2('val', data.wdc_number_rft4);
                        $('#rip_4').val(data.wdc_res_pressure4);
                        $('#lpr_4').val(data.wdc_last_pressure4);
                        $('#pressure_gradient4').val(data.wdc_pressure_gradient4);
                        $('#lrt_4').val(data.wdc_last_temp4);
                        $('#s2id_actual_well_integrity4').select2('val', data.wdc_integrity4);
                        $('#s2id_availability_electrolog4').select2('val', data.wdc_electro_avail4);
                        $('#wdc_electro_list14').val(data.wdc_electro_list14);
                        $('#wdc_electro_list24').val(data.wdc_electro_list24);
                        $('#wdc_electro_list34').val(data.wdc_electro_list34);
                        $('#wdc_electro_list44').val(data.wdc_electro_list44); 
                        $('#wdc_electro_list54').val(data.wdc_electro_list54);
                        $('#wdc_electro_list64').val(data.wdc_electro_list64);
                        $('#wdc_electro_list74').val(data.wdc_electro_list74);
                        $('#wdc_electro_list84').val(data.wdc_electro_list84);
                        $('#wdc_electro_list94').val(data.wdc_electro_list94);
                        $('#wdc_electro_list104').val(data.wdc_electro_list104);
                        $('#wdc_electro_list114').val(data.wdc_electro_list114);
                        $('#wdc_electro_list124').val(data.wdc_electro_list124); 
                        $('#wdc_electro_list134').val(data.wdc_electro_list134);
                        $('#wdc_electro_list144').val(data.wdc_electro_list144);
                        $('#wdc_electro_list154').val(data.wdc_electro_list154);
                        $('#wdc_electro_list164').val(data.wdc_electro_list164);
                        $('#s2id_wz_4').select2('val', data.wdc_total_zone4);
                            		
                        $('#zonename11').val(data.zone_name11);
                        $('#s2id_welltest11').select2('val', data.zone_test11);
                        $('#z_11').val(data.zone_test_date11);
                        $('#zonethickness11').val(data.zone_thickness11);
                        $('#zoneinterval11').val(data.zone_depth11);
                        $('#perforation_interval11').val(data.zone_perforation11);
                        $('#s2id_welltest_type11').select2('val', data.zone_well_test11);
                        $('#welltest_total11').val(data.zone_well_duration11);
                        $('#initialflow11').val(data.zone_initial_flow11);
                        $('#initialshutin11').val(data.zone_initial_shutin11);
                        $('#tubingsize11').val(data.zone_tubing_size11);
                        $('#initialtemperature11').val(data.zone_initial_temp11);
                        $('#zone_initial_pressure11').val(data.zone_initial_pressure11);
                        $('#res_presure11').val(data.zone_pseudostate11);
                        $('#pressurewell_for11').val(data.zone_well_formation11);
                        $('#pressurewell_head11').val(data.zone_head11);
                        $('#res_pressure11').val(data.zone_pressure_wellbore11);
                        $('#avg_porpsity11').val(data.zone_average_por11);
                        $('#watercut11').val(data.zone_water_cut11);
                        $('#initialwater_sat11').val(data.zone_initial_water11);
                        $('#lowtest_gas11').val(data.zone_low_gas11);
                        $('#lowtest_oil11').val(data.zone_low_oil11);
                        $('#freewater11').val(data.zone_free_water11);
                        $('#gas_grav11').val(data.zone_grv_gas11);
                        $('#oil_grav11').val(data.zone_grv_grv_oil11);
                        $('#zone_wellbore_coefficient11').val(data.zone_wellbore_coefficient11);
                        $('#wellbore_time11').val(data.zone_wellbore_time11);
                        $('#rsbt_11').val(data.zone_res_shape11);
                        $('#oilchoke11').val(data.zone_prod_oil_choke11);
                        $('#oilflow11').val(data.zone_prod_oil_flow11);
                        $('#gaschoke11').val(data.zone_prod_gas_choke11);
                        $('#gasflow11').val(data.zone_prod_gas_flow11);
                        $('#gasoilratio11').val(data.zone_prod_gasoil_ratio11);
                        $('#zone_prod_conden_ratio11').val(data.zone_prod_conden_ratio11);
                        $('#cummulative_pro_gas11').val(data.zone_prod_cumm_gas11);
                        $('#cummulative_pro_oil11').val(data.zone_prod_cumm_oil11);
                        $('#ab_openflow111').val(data.zone_prod_aof_bbl11);
                        $('#ab_openflow211').val(data.zone_prod_aof_scf11);
                        $('#criticalrate111').val(data.zone_prod_critical_bbl11);
                        $('#criticalrate211').val(data.zone_prod_critical_scf11);
                        $('#pro_index111').val(data.zone_prod_index_bbl11);
                        $('#pro_index211').val(data.zone_prod_index_scf11);
                        $('#diffusity_fact11').val(data.zone_prod_diffusity11);
                        $('#permeability11').val(data.zone_prod_permeability11);
                        $('#iafit_11').val(data.zone_prod_infinite11);
                        $('#wellradius11').val(data.zone_prod_well_radius11);
                        $('#pfit_11').val(data.zone_prod_pseudostate11);
                        $('#res_bondradius11').val(data.zone_prod_res_radius11);
                        $('#deltaP11').val(data.zone_prod_delta11);
                        $('#wellbore_skin11').val(data.zone_prod_wellbore11);
                        $('#rock_compress11').val(data.zone_prod_compres_rock11);
                        $('#fluid_compress11').val(data.zone_prod_compres_fluid11);
                        $('#total_compress11').val(data.zone_prod_compres_total11);
                        $('#secd_porosity11').val(data.zone_prod_second11);
                        $('#I_11').val(data.zone_prod_lambda11);
                        $('#W_11').val(data.zone_prod_omega11);
                        $('#oilshow_read111').val(data.zone_hc_oil_show11);
                        $('#oilshow_read211').val(data.zone_hc_oil_show_tools11);
                        $('#gasshow_read111').val(data.zone_hc_gas_show11);
                        $('#gasshow_read211').val(data.zone_hc_gas_show_tools11);
                        $('#wellmak_watercut11').val(data.zone_hc_water_cut11);
                        $('#water_bearing111').val(data.zone_hc_water_gwc11);
                        $('#water_bearing211').val(data.zone_hc_water_gwd_tools11);
                        $('#water_bearing311').val(data.zone_hc_water_owc11);
                        $('#water_bearing411').val(data.zone_hc_water_owc_tools11);
                        $('#s2id_rock_sampling11').select2('val', data.zone_rock_method11);
                        $('#s2id_petro_analys11').select2('val', data.zone_rock_petro11);
                        $('#sample11').val(data.zone_rock_petro_sample11);
                        $('#ticsd_11').val(data.zone_rock_top_depth11);
                        $('#bicsd_11').val(data.zone_rock_bot_depth11);
                        $('#num_totalcare11').val(data.zone_rock_barrel11);
                        $('#corebarrel11').val(data.zone_rock_barrel_equal11);
                        $('#totalrec11').val(data.zone_rock_total_core11);
                        $('#precore11').val(data.zone_rock_preservative11);
                        $('#rca_111').val(data.zone_rock_routine11);
                        $('#rca_11').val(data.zone_rock_routine_sample11);
                        $('#scal_111').val(data.zone_rock_scal11);
                        $('#scal_211').val(data.zone_rock_scal_sample11);
                        $('#zone_clastic_p10_thickness11').val(data.zone_clastic_p10_thickness11);
                        $('#zone_clastic_p50_thickness11').val(data.zone_clastic_p50_thickness11);
                        $('#zone_clastic_p90_thickness11').val(data.zone_clastic_p90_thickness11);
                        $('#zone_clastic_p10_gr11').val(data.zone_clastic_p10_gr11);
                        $('#zone_clastic_p50_gr11').val(data.zone_clastic_p50_gr11);
                        $('#zone_clastic_p90_gr11').val(data.zone_clastic_p90_gr11);
                        $('#zone_clastic_p10_sp11').val(data.zone_clastic_p10_sp11);
                        $('#zone_clastic_p50_sp11').val(data.zone_clastic_p50_sp11);
                        $('#zone_clastic_p90_sp11').val(data.zone_clastic_p90_sp11);
                        $('#zone_clastic_p10_net11').val(data.zone_clastic_p10_net11);
                        $('#zone_clastic_p50_net11').val(data.zone_clastic_p50_net11);
                        $('#zone_clastic_p90_net11').val(data.zone_clastic_p90_net11);
                        $('#zone_clastic_p10_por11').val(data.zone_clastic_p10_por11);
                        $('#zone_clastic_p50_por11').val(data.zone_clastic_p50_por11);
                        $('#zone_clastic_p90_por11').val(data.zone_clastic_p90_por11);
                        $('#zone_clastic_p10_satur11').val(data.zone_clastic_p10_satur11);
                        $('#zone_clastic_p50_satur11').val(data.zone_clastic_p50_satur11);
                        $('#zone_clastic_p90_satur11').val(data.zone_clastic_p90_satur11);
                        $('#zone_carbo_p10_thickness11').val(data.zone_carbo_p10_thickness11);
                        $('#zone_carbo_p50_thickness11').val(data.zone_carbo_p50_thickness11);
                        $('#zone_carbo_p90_thickness11').val(data.zone_carbo_p90_thickness11);
                        $('#zone_carbo_p10_dtc11').val(data.zone_carbo_p10_dtc11);
                        $('#zone_carbo_p50_dtc11').val(data.zone_carbo_p50_dtc11);
                        $('#zone_carbo_p90_dtc11').val(data.zone_carbo_p90_dtc11);
                        $('#zone_carbo_p10_total11').val(data.zone_carbo_p10_total11);
                        $('#zone_carbo_p50_total11').val(data.zone_carbo_p50_total11);
                        $('#zone_carbo_p90_total11').val(data.zone_carbo_p90_total11);
                        $('#zone_carbo_p10_net11').val(data.zone_carbo_p10_net11);
                        $('#zone_carbo_p50_net11').val(data.zone_carbo_p50_net11);
                        $('#zone_carbo_p90_net11').val(data.zone_carbo_p90_net11);
                        $('#zone_carbo_p10_por11').val(data.zone_carbo_p10_por11);
                        $('#zone_carbo_p50_por11').val(data.zone_carbo_p50_por11);
                        $('#zone_carbo_p90_por11').val(data.zone_carbo_p90_por11);
                        $('#zone_carbo_p10_satur11').val(data.zone_carbo_p10_satur11);
                        $('#zone_carbo_p50_satur11').val(data.zone_carbo_p50_satur11);
                        $('#zone_carbo_p90_satur11').val(data.zone_carbo_p90_satur11);
                        $('#z2_11').val(data.zone_fluid_date11);
                        $('#sampleat11').val(data.zone_fluid_sample11);
                        $('#gasoilratio_11').val(data.zone_fluid_ratio11);
                        $('#separator_pressure11').val(data.zone_fluid_pressure11);
                        $('#separator_temperature11').val(data.zone_fluid_temp11);
                        $('#tubing_pressure11').val(data.zone_fluid_tubing11);
                        $('#casing_pressure11').val(data.zone_fluid_casing11);
                        $('#sampleby11').val(data.zone_fluid_by11);
                        $('#repost_avail11').val(data.zone_fluid_report11);
                        $('#hydro_finger11').val(data.zone_fluid_finger11);
                        $('#oilgrav_6011').val(data.zone_fluid_grv_oil11);
                        $('#gasgrav_6011').val(data.zone_fluid_grv_gas11);
                        $('#condensategrav_6011').val(data.zone_fluid_grv_conden11);
                        $('#pvt_11').val(data.zone_fluid_pvt11);
                        $('#sample_11').val(data.zone_fluid_quantity11);
                        $('#gasdev_fac11').val(data.zone_fluid_z11);
                        $('#zone_fvf_oil_p1011').val(data.zone_fvf_oil_p1011);
                        $('#zone_fvf_oil_p5011').val(data.zone_fvf_oil_p5011);
                        $('#zone_fvf_oil_p9011').val(data.zone_fvf_oil_p9011);
                        $('#zone_fvf_gas_p1011').val(data.zone_fvf_gas_p1011);
                        $('#zone_fvf_gas_p5011').val(data.zone_fvf_gas_p5011);
                        $('#zone_fvf_gas_p9011').val(data.zone_fvf_gas_p9011);
                        $('#oilviscocity11').val(data.zone_viscocity11);
                            		
                        $('#zonename12').val(data.zone_name12);
                        $('#s2id_welltest12').select2('val', data.zone_test12);
                        $('#z_12').val(data.zone_test_date12);
                        $('#zonethickness12').val(data.zone_thickness12);
                        $('#zoneinterval12').val(data.zone_depth12);
                        $('#perforation_interval12').val(data.zone_perforation12);
                        $('#s2id_welltest_type12').select2('val', data.zone_well_test12);
                        $('#welltest_total12').val(data.zone_well_duration12);
                        $('#initialflow12').val(data.zone_initial_flow12);
                        $('#initialshutin12').val(data.zone_initial_shutin12);
                        $('#tubingsize12').val(data.zone_tubing_size12);
                        $('#initialtemperature12').val(data.zone_initial_temp12);
                        $('#zone_initial_pressure12').val(data.zone_initial_pressure12);
                        $('#res_presure12').val(data.zone_pseudostate12);
                        $('#pressurewell_for12').val(data.zone_well_formation12);
                        $('#pressurewell_head12').val(data.zone_head12);
                        $('#res_pressure12').val(data.zone_pressure_wellbore12);
                        $('#avg_porpsity12').val(data.zone_average_por12);
                        $('#watercut12').val(data.zone_water_cut12);
                        $('#initialwater_sat12').val(data.zone_initial_water12);
                        $('#lowtest_gas12').val(data.zone_low_gas12);
                        $('#lowtest_oil12').val(data.zone_low_oil12);
                        $('#freewater12').val(data.zone_free_water12);
                        $('#gas_grav12').val(data.zone_grv_gas12);
                        $('#oil_grav12').val(data.zone_grv_grv_oil12);
                        $('#zone_wellbore_coefficient12').val(data.zone_wellbore_coefficient12);
                        $('#wellbore_time12').val(data.zone_wellbore_time12);
                        $('#rsbt_12').val(data.zone_res_shape12);
                        $('#oilchoke12').val(data.zone_prod_oil_choke12);
                        $('#oilflow12').val(data.zone_prod_oil_flow12);
                        $('#gaschoke12').val(data.zone_prod_gas_choke12);
                        $('#gasflow12').val(data.zone_prod_gas_flow12);
                        $('#gasoilratio12').val(data.zone_prod_gasoil_ratio12);
                        $('#zone_prod_conden_ratio12').val(data.zone_prod_conden_ratio12);
                        $('#cummulative_pro_gas12').val(data.zone_prod_cumm_gas12);
                        $('#cummulative_pro_oil12').val(data.zone_prod_cumm_oil12);
                        $('#ab_openflow112').val(data.zone_prod_aof_bbl12);
                        $('#ab_openflow212').val(data.zone_prod_aof_scf12);
                        $('#criticalrate112').val(data.zone_prod_critical_bbl12);
                        $('#criticalrate212').val(data.zone_prod_critical_scf12);
                        $('#pro_index112').val(data.zone_prod_index_bbl12);
                        $('#pro_index212').val(data.zone_prod_index_scf12);
                        $('#diffusity_fact12').val(data.zone_prod_diffusity12);
                        $('#permeability12').val(data.zone_prod_permeability12);
                        $('#iafit_12').val(data.zone_prod_infinite12);
                        $('#wellradius12').val(data.zone_prod_well_radius12);
                        $('#pfit_12').val(data.zone_prod_pseudostate12);
                        $('#res_bondradius12').val(data.zone_prod_res_radius12);
                        $('#deltaP12').val(data.zone_prod_delta12);
                        $('#wellbore_skin12').val(data.zone_prod_wellbore12);
                        $('#rock_compress12').val(data.zone_prod_compres_rock12);
                        $('#fluid_compress12').val(data.zone_prod_compres_fluid12);
                        $('#total_compress12').val(data.zone_prod_compres_total12);
                        $('#secd_porosity12').val(data.zone_prod_second12);
                        $('#I_12').val(data.zone_prod_lambda12);
                        $('#W_12').val(data.zone_prod_omega12);
                        $('#oilshow_read112').val(data.zone_hc_oil_show12);
                        $('#oilshow_read212').val(data.zone_hc_oil_show_tools12);
                        $('#gasshow_read112').val(data.zone_hc_gas_show12);
                        $('#gasshow_read212').val(data.zone_hc_gas_show_tools12);
                        $('#wellmak_watercut12').val(data.zone_hc_water_cut12);
                        $('#water_bearing112').val(data.zone_hc_water_gwc12);
                        $('#water_bearing212').val(data.zone_hc_water_gwd_tools12);
                        $('#water_bearing312').val(data.zone_hc_water_owc12);
                        $('#water_bearing412').val(data.zone_hc_water_owc_tools12);
                        $('#s2id_rock_sampling12').select2('val', data.zone_rock_method12);
                        $('#s2id_petro_analys12').select2('val', data.zone_rock_petro12);
                        $('#sample12').val(data.zone_rock_petro_sample12);
                        $('#ticsd_12').val(data.zone_rock_top_depth12);
                        $('#bicsd_12').val(data.zone_rock_bot_depth12);
                        $('#num_totalcare12').val(data.zone_rock_barrel12);
                        $('#corebarrel12').val(data.zone_rock_barrel_equal12);
                        $('#totalrec12').val(data.zone_rock_total_core12);
                        $('#precore12').val(data.zone_rock_preservative12);
                        $('#rca_112').val(data.zone_rock_routine12);
                        $('#rca_12').val(data.zone_rock_routine_sample12);
                        $('#scal_112').val(data.zone_rock_scal12);
                        $('#scal_212').val(data.zone_rock_scal_sample12);
                        $('#zone_clastic_p10_thickness12').val(data.zone_clastic_p10_thickness12);
                        $('#zone_clastic_p50_thickness12').val(data.zone_clastic_p50_thickness12);
                        $('#zone_clastic_p90_thickness12').val(data.zone_clastic_p90_thickness12);
                        $('#zone_clastic_p10_gr12').val(data.zone_clastic_p10_gr12);
                        $('#zone_clastic_p50_gr12').val(data.zone_clastic_p50_gr12);
                        $('#zone_clastic_p90_gr12').val(data.zone_clastic_p90_gr12);
                        $('#zone_clastic_p10_sp12').val(data.zone_clastic_p10_sp12);
                        $('#zone_clastic_p50_sp12').val(data.zone_clastic_p50_sp12);
                        $('#zone_clastic_p90_sp12').val(data.zone_clastic_p90_sp12);
                        $('#zone_clastic_p10_net12').val(data.zone_clastic_p10_net12);
                        $('#zone_clastic_p50_net12').val(data.zone_clastic_p50_net12);
                        $('#zone_clastic_p90_net12').val(data.zone_clastic_p90_net12);
                        $('#zone_clastic_p10_por12').val(data.zone_clastic_p10_por12);
                        $('#zone_clastic_p50_por12').val(data.zone_clastic_p50_por12);
                        $('#zone_clastic_p90_por12').val(data.zone_clastic_p90_por12);
                        $('#zone_clastic_p10_satur12').val(data.zone_clastic_p10_satur12);
                        $('#zone_clastic_p50_satur12').val(data.zone_clastic_p50_satur12);
                        $('#zone_clastic_p90_satur12').val(data.zone_clastic_p90_satur12);
                        $('#zone_carbo_p10_thickness12').val(data.zone_carbo_p10_thickness12);
                        $('#zone_carbo_p50_thickness12').val(data.zone_carbo_p50_thickness12);
                        $('#zone_carbo_p90_thickness12').val(data.zone_carbo_p90_thickness12);
                        $('#zone_carbo_p10_dtc12').val(data.zone_carbo_p10_dtc12);
                        $('#zone_carbo_p50_dtc12').val(data.zone_carbo_p50_dtc12);
                        $('#zone_carbo_p90_dtc12').val(data.zone_carbo_p90_dtc12);
                        $('#zone_carbo_p10_total12').val(data.zone_carbo_p10_total12);
                        $('#zone_carbo_p50_total12').val(data.zone_carbo_p50_total12);
                        $('#zone_carbo_p90_total12').val(data.zone_carbo_p90_total12);
                        $('#zone_carbo_p10_net12').val(data.zone_carbo_p10_net12);
                        $('#zone_carbo_p50_net12').val(data.zone_carbo_p50_net12);
                        $('#zone_carbo_p90_net12').val(data.zone_carbo_p90_net12);
                        $('#zone_carbo_p10_por12').val(data.zone_carbo_p10_por12);
                        $('#zone_carbo_p50_por12').val(data.zone_carbo_p50_por12);
                        $('#zone_carbo_p90_por12').val(data.zone_carbo_p90_por12);
                        $('#zone_carbo_p10_satur12').val(data.zone_carbo_p10_satur12);
                        $('#zone_carbo_p50_satur12').val(data.zone_carbo_p50_satur12);
                        $('#zone_carbo_p90_satur12').val(data.zone_carbo_p90_satur12);
                        $('#z2_12').val(data.zone_fluid_date12);
                        $('#sampleat12').val(data.zone_fluid_sample12);
                        $('#gasoilratio_12').val(data.zone_fluid_ratio12);
                        $('#separator_pressure12').val(data.zone_fluid_pressure12);
                        $('#separator_temperature12').val(data.zone_fluid_temp12);
                        $('#tubing_pressure12').val(data.zone_fluid_tubing12);
                        $('#casing_pressure12').val(data.zone_fluid_casing12);
                        $('#sampleby12').val(data.zone_fluid_by12);
                        $('#repost_avail12').val(data.zone_fluid_report12);
                        $('#hydro_finger12').val(data.zone_fluid_finger12);
                        $('#oilgrav_6012').val(data.zone_fluid_grv_oil12);
                        $('#gasgrav_6012').val(data.zone_fluid_grv_gas12);
                        $('#condensategrav_6012').val(data.zone_fluid_grv_conden12);
                        $('#pvt_12').val(data.zone_fluid_pvt12);
                        $('#sample_12').val(data.zone_fluid_quantity12);
                        $('#gasdev_fac12').val(data.zone_fluid_z12);
                        $('#zone_fvf_oil_p1012').val(data.zone_fvf_oil_p1012);
                        $('#zone_fvf_oil_p5012').val(data.zone_fvf_oil_p5012);
                        $('#zone_fvf_oil_p9012').val(data.zone_fvf_oil_p9012);
                        $('#zone_fvf_gas_p1012').val(data.zone_fvf_gas_p1012);
                        $('#zone_fvf_gas_p5012').val(data.zone_fvf_gas_p5012);
                        $('#zone_fvf_gas_p9012').val(data.zone_fvf_gas_p9012);
                        $('#oilviscocity12').val(data.zone_viscocity12);
                            		
                        $('#zonename13').val(data.zone_name13);
                        $('#s2id_welltest13').select2('val', data.zone_test13);
                        $('#z_13').val(data.zone_test_date13);
                        $('#zonethickness13').val(data.zone_thickness13);
                        $('#zoneinterval13').val(data.zone_depth13);
                        $('#perforation_interval13').val(data.zone_perforation13);
                        $('#s2id_welltest_type13').select2('val', data.zone_well_test13);
                        $('#welltest_total13').val(data.zone_well_duration13);
                        $('#initialflow13').val(data.zone_initial_flow13);
                        $('#initialshutin13').val(data.zone_initial_shutin13);
                        $('#tubingsize13').val(data.zone_tubing_size13);
                        $('#initialtemperature13').val(data.zone_initial_temp13);
                        $('#zone_initial_pressure13').val(data.zone_initial_pressure13);
                        $('#res_presure13').val(data.zone_pseudostate13);
                        $('#pressurewell_for13').val(data.zone_well_formation13);
                        $('#pressurewell_head13').val(data.zone_head13);
                        $('#res_pressure13').val(data.zone_pressure_wellbore13);
                        $('#avg_porpsity13').val(data.zone_average_por13);
                        $('#watercut13').val(data.zone_water_cut13);
                        $('#initialwater_sat13').val(data.zone_initial_water13);
                        $('#lowtest_gas13').val(data.zone_low_gas13);
                        $('#lowtest_oil13').val(data.zone_low_oil13);
                        $('#freewater13').val(data.zone_free_water13);
                        $('#gas_grav13').val(data.zone_grv_gas13);
                        $('#oil_grav13').val(data.zone_grv_grv_oil13);
                        $('#zone_wellbore_coefficient13').val(data.zone_wellbore_coefficient13);
                        $('#wellbore_time13').val(data.zone_wellbore_time13);
                        $('#rsbt_13').val(data.zone_res_shape13);
                        $('#oilchoke13').val(data.zone_prod_oil_choke13);
                        $('#oilflow13').val(data.zone_prod_oil_flow13);
                        $('#gaschoke13').val(data.zone_prod_gas_choke13);
                        $('#gasflow13').val(data.zone_prod_gas_flow13);
                        $('#gasoilratio13').val(data.zone_prod_gasoil_ratio13);
                        $('#zone_prod_conden_ratio13').val(data.zone_prod_conden_ratio13);
                        $('#cummulative_pro_gas13').val(data.zone_prod_cumm_gas13);
                        $('#cummulative_pro_oil13').val(data.zone_prod_cumm_oil13);
                        $('#ab_openflow113').val(data.zone_prod_aof_bbl13);
                        $('#ab_openflow213').val(data.zone_prod_aof_scf13);
                        $('#criticalrate113').val(data.zone_prod_critical_bbl13);
                        $('#criticalrate213').val(data.zone_prod_critical_scf13);
                        $('#pro_index113').val(data.zone_prod_index_bbl13);
                        $('#pro_index213').val(data.zone_prod_index_scf13);
                        $('#diffusity_fact13').val(data.zone_prod_diffusity13);
                        $('#permeability13').val(data.zone_prod_permeability13);
                        $('#iafit_13').val(data.zone_prod_infinite13);
                        $('#wellradius13').val(data.zone_prod_well_radius13);
                        $('#pfit_13').val(data.zone_prod_pseudostate13);
                        $('#res_bondradius13').val(data.zone_prod_res_radius13);
                        $('#deltaP13').val(data.zone_prod_delta13);
                        $('#wellbore_skin13').val(data.zone_prod_wellbore13);
                        $('#rock_compress13').val(data.zone_prod_compres_rock13);
                        $('#fluid_compress13').val(data.zone_prod_compres_fluid13);
                        $('#total_compress13').val(data.zone_prod_compres_total13);
                        $('#secd_porosity13').val(data.zone_prod_second13);
                        $('#I_13').val(data.zone_prod_lambda13);
                        $('#W_13').val(data.zone_prod_omega13);
                        $('#oilshow_read113').val(data.zone_hc_oil_show13);
                        $('#oilshow_read213').val(data.zone_hc_oil_show_tools13);
                        $('#gasshow_read113').val(data.zone_hc_gas_show13);
                        $('#gasshow_read213').val(data.zone_hc_gas_show_tools13);
                        $('#wellmak_watercut13').val(data.zone_hc_water_cut13);
                        $('#water_bearing113').val(data.zone_hc_water_gwc13);
                        $('#water_bearing213').val(data.zone_hc_water_gwd_tools13);
                        $('#water_bearing313').val(data.zone_hc_water_owc13);
                        $('#water_bearing413').val(data.zone_hc_water_owc_tools13);
                        $('#s2id_rock_sampling13').select2('val', data.zone_rock_method13);
                        $('#s2id_petro_analys13').select2('val', data.zone_rock_petro13);
                        $('#sample13').val(data.zone_rock_petro_sample13);
                        $('#ticsd_13').val(data.zone_rock_top_depth13);
                        $('#bicsd_13').val(data.zone_rock_bot_depth13);
                        $('#num_totalcare13').val(data.zone_rock_barrel13);
                        $('#corebarrel13').val(data.zone_rock_barrel_equal13);
                        $('#totalrec13').val(data.zone_rock_total_core13);
                        $('#precore13').val(data.zone_rock_preservative13);
                        $('#rca_113').val(data.zone_rock_routine13);
                        $('#rca_13').val(data.zone_rock_routine_sample13);
                        $('#scal_113').val(data.zone_rock_scal13);
                        $('#scal_213').val(data.zone_rock_scal_sample13);
                        $('#zone_clastic_p10_thickness13').val(data.zone_clastic_p10_thickness13);
                        $('#zone_clastic_p50_thickness13').val(data.zone_clastic_p50_thickness13);
                        $('#zone_clastic_p90_thickness13').val(data.zone_clastic_p90_thickness13);
                        $('#zone_clastic_p10_gr13').val(data.zone_clastic_p10_gr13);
                        $('#zone_clastic_p50_gr13').val(data.zone_clastic_p50_gr13);
                        $('#zone_clastic_p90_gr13').val(data.zone_clastic_p90_gr13);
                        $('#zone_clastic_p10_sp13').val(data.zone_clastic_p10_sp13);
                        $('#zone_clastic_p50_sp13').val(data.zone_clastic_p50_sp13);
                        $('#zone_clastic_p90_sp13').val(data.zone_clastic_p90_sp13);
                        $('#zone_clastic_p10_net13').val(data.zone_clastic_p10_net13);
                        $('#zone_clastic_p50_net13').val(data.zone_clastic_p50_net13);
                        $('#zone_clastic_p90_net13').val(data.zone_clastic_p90_net13);
                        $('#zone_clastic_p10_por13').val(data.zone_clastic_p10_por13);
                        $('#zone_clastic_p50_por13').val(data.zone_clastic_p50_por13);
                        $('#zone_clastic_p90_por13').val(data.zone_clastic_p90_por13);
                        $('#zone_clastic_p10_satur13').val(data.zone_clastic_p10_satur13);
                        $('#zone_clastic_p50_satur13').val(data.zone_clastic_p50_satur13);
                        $('#zone_clastic_p90_satur13').val(data.zone_clastic_p90_satur13);
                        $('#zone_carbo_p10_thickness13').val(data.zone_carbo_p10_thickness13);
                        $('#zone_carbo_p50_thickness13').val(data.zone_carbo_p50_thickness13);
                        $('#zone_carbo_p90_thickness13').val(data.zone_carbo_p90_thickness13);
                        $('#zone_carbo_p10_dtc13').val(data.zone_carbo_p10_dtc13);
                        $('#zone_carbo_p50_dtc13').val(data.zone_carbo_p50_dtc13);
                        $('#zone_carbo_p90_dtc13').val(data.zone_carbo_p90_dtc13);
                        $('#zone_carbo_p10_total13').val(data.zone_carbo_p10_total13);
                        $('#zone_carbo_p50_total13').val(data.zone_carbo_p50_total13);
                        $('#zone_carbo_p90_total13').val(data.zone_carbo_p90_total13);
                        $('#zone_carbo_p10_net13').val(data.zone_carbo_p10_net13);
                        $('#zone_carbo_p50_net13').val(data.zone_carbo_p50_net13);
                        $('#zone_carbo_p90_net13').val(data.zone_carbo_p90_net13);
                        $('#zone_carbo_p10_por13').val(data.zone_carbo_p10_por13);
                        $('#zone_carbo_p50_por13').val(data.zone_carbo_p50_por13);
                        $('#zone_carbo_p90_por13').val(data.zone_carbo_p90_por13);
                        $('#zone_carbo_p10_satur13').val(data.zone_carbo_p10_satur13);
                        $('#zone_carbo_p50_satur13').val(data.zone_carbo_p50_satur13);
                        $('#zone_carbo_p90_satur13').val(data.zone_carbo_p90_satur13);
                        $('#z2_13').val(data.zone_fluid_date13);
                        $('#sampleat13').val(data.zone_fluid_sample13);
                        $('#gasoilratio_13').val(data.zone_fluid_ratio13);
                        $('#separator_pressure13').val(data.zone_fluid_pressure13);
                        $('#separator_temperature13').val(data.zone_fluid_temp13);
                        $('#tubing_pressure13').val(data.zone_fluid_tubing13);
                        $('#casing_pressure13').val(data.zone_fluid_casing13);
                        $('#sampleby13').val(data.zone_fluid_by13);
                        $('#repost_avail13').val(data.zone_fluid_report13);
                        $('#hydro_finger13').val(data.zone_fluid_finger13);
                        $('#oilgrav_6013').val(data.zone_fluid_grv_oil13);
                        $('#gasgrav_6013').val(data.zone_fluid_grv_gas13);
                        $('#condensategrav_6013').val(data.zone_fluid_grv_conden13);
                        $('#pvt_13').val(data.zone_fluid_pvt13);
                        $('#sample_13').val(data.zone_fluid_quantity13);
                        $('#gasdev_fac13').val(data.zone_fluid_z13);
                        $('#zone_fvf_oil_p1013').val(data.zone_fvf_oil_p1013);
                        $('#zone_fvf_oil_p5013').val(data.zone_fvf_oil_p5013);
                        $('#zone_fvf_oil_p9013').val(data.zone_fvf_oil_p9013);
                        $('#zone_fvf_gas_p1013').val(data.zone_fvf_gas_p1013);
                        $('#zone_fvf_gas_p5013').val(data.zone_fvf_gas_p5013);
                        $('#zone_fvf_gas_p9013').val(data.zone_fvf_gas_p9013);
                        $('#oilviscocity13').val(data.zone_viscocity13);
                            		
                        $('#zonename14').val(data.zone_name14);
                        $('#s2id_welltest14').select2('val', data.zone_test14);
                        $('#z_14').val(data.zone_test_date14);
                        $('#zonethickness14').val(data.zone_thickness14);
                        $('#zoneinterval14').val(data.zone_depth14);
                        $('#perforation_interval14').val(data.zone_perforation14);
                        $('#s2id_welltest_type14').select2('val', data.zone_well_test14);
                        $('#welltest_total14').val(data.zone_well_duration14);
                        $('#initialflow14').val(data.zone_initial_flow14);
                        $('#initialshutin14').val(data.zone_initial_shutin14);
                        $('#tubingsize14').val(data.zone_tubing_size14);
                        $('#initialtemperature14').val(data.zone_initial_temp14);
                        $('#zone_initial_pressure14').val(data.zone_initial_pressure14);
                        $('#res_presure14').val(data.zone_pseudostate14);
                        $('#pressurewell_for14').val(data.zone_well_formation14);
                        $('#pressurewell_head14').val(data.zone_head14);
                        $('#res_pressure14').val(data.zone_pressure_wellbore14);
                        $('#avg_porpsity14').val(data.zone_average_por14);
                        $('#watercut14').val(data.zone_water_cut14);
                        $('#initialwater_sat14').val(data.zone_initial_water14);
                        $('#lowtest_gas14').val(data.zone_low_gas14);
                        $('#lowtest_oil14').val(data.zone_low_oil14);
                        $('#freewater14').val(data.zone_free_water14);
                        $('#gas_grav14').val(data.zone_grv_gas14);
                        $('#oil_grav14').val(data.zone_grv_grv_oil14);
                        $('#zone_wellbore_coefficient14').val(data.zone_wellbore_coefficient14);
                        $('#wellbore_time14').val(data.zone_wellbore_time14);
                        $('#rsbt_14').val(data.zone_res_shape14);
                        $('#oilchoke14').val(data.zone_prod_oil_choke14);
                        $('#oilflow14').val(data.zone_prod_oil_flow14);
                        $('#gaschoke14').val(data.zone_prod_gas_choke14);
                        $('#gasflow14').val(data.zone_prod_gas_flow14);
                        $('#gasoilratio14').val(data.zone_prod_gasoil_ratio14);
                        $('#zone_prod_conden_ratio14').val(data.zone_prod_conden_ratio14);
                        $('#cummulative_pro_gas14').val(data.zone_prod_cumm_gas14);
                        $('#cummulative_pro_oil14').val(data.zone_prod_cumm_oil14);
                        $('#ab_openflow114').val(data.zone_prod_aof_bbl14);
                        $('#ab_openflow214').val(data.zone_prod_aof_scf14);
                        $('#criticalrate114').val(data.zone_prod_critical_bbl14);
                        $('#criticalrate214').val(data.zone_prod_critical_scf14);
                        $('#pro_index114').val(data.zone_prod_index_bbl14);
                        $('#pro_index214').val(data.zone_prod_index_scf14);
                        $('#diffusity_fact14').val(data.zone_prod_diffusity14);
                        $('#permeability14').val(data.zone_prod_permeability14);
                        $('#iafit_14').val(data.zone_prod_infinite14);
                        $('#wellradius14').val(data.zone_prod_well_radius14);
                        $('#pfit_14').val(data.zone_prod_pseudostate14);
                        $('#res_bondradius14').val(data.zone_prod_res_radius14);
                        $('#deltaP14').val(data.zone_prod_delta14);
                        $('#wellbore_skin14').val(data.zone_prod_wellbore14);
                        $('#rock_compress14').val(data.zone_prod_compres_rock14);
                        $('#fluid_compress14').val(data.zone_prod_compres_fluid14);
                        $('#total_compress14').val(data.zone_prod_compres_total14);
                        $('#secd_porosity14').val(data.zone_prod_second14);
                        $('#I_14').val(data.zone_prod_lambda14);
                        $('#W_14').val(data.zone_prod_omega14);
                        $('#oilshow_read114').val(data.zone_hc_oil_show14);
                        $('#oilshow_read214').val(data.zone_hc_oil_show_tools14);
                        $('#gasshow_read114').val(data.zone_hc_gas_show14);
                        $('#gasshow_read214').val(data.zone_hc_gas_show_tools14);
                        $('#wellmak_watercut14').val(data.zone_hc_water_cut14);
                        $('#water_bearing114').val(data.zone_hc_water_gwc14);
                        $('#water_bearing214').val(data.zone_hc_water_gwd_tools14);
                        $('#water_bearing314').val(data.zone_hc_water_owc14);
                        $('#water_bearing414').val(data.zone_hc_water_owc_tools14);
                        $('#s2id_rock_sampling14').select2('val', data.zone_rock_method14);
                        $('#s2id_petro_analys14').select2('val', data.zone_rock_petro14);
                        $('#sample14').val(data.zone_rock_petro_sample14);
                        $('#ticsd_14').val(data.zone_rock_top_depth14);
                        $('#bicsd_14').val(data.zone_rock_bot_depth14);
                        $('#num_totalcare14').val(data.zone_rock_barrel14);
                        $('#corebarrel14').val(data.zone_rock_barrel_equal14);
                        $('#totalrec14').val(data.zone_rock_total_core14);
                        $('#precore14').val(data.zone_rock_preservative14);
                        $('#rca_114').val(data.zone_rock_routine14);
                        $('#rca_14').val(data.zone_rock_routine_sample14);
                        $('#scal_114').val(data.zone_rock_scal14);
                        $('#scal_214').val(data.zone_rock_scal_sample14);
                        $('#zone_clastic_p10_thickness14').val(data.zone_clastic_p10_thickness14);
                        $('#zone_clastic_p50_thickness14').val(data.zone_clastic_p50_thickness14);
                        $('#zone_clastic_p90_thickness14').val(data.zone_clastic_p90_thickness14);
                        $('#zone_clastic_p10_gr14').val(data.zone_clastic_p10_gr14);
                        $('#zone_clastic_p50_gr14').val(data.zone_clastic_p50_gr14);
                        $('#zone_clastic_p90_gr14').val(data.zone_clastic_p90_gr14);
                        $('#zone_clastic_p10_sp14').val(data.zone_clastic_p10_sp14);
                        $('#zone_clastic_p50_sp14').val(data.zone_clastic_p50_sp14);
                        $('#zone_clastic_p90_sp14').val(data.zone_clastic_p90_sp14);
                        $('#zone_clastic_p10_net14').val(data.zone_clastic_p10_net14);
                        $('#zone_clastic_p50_net14').val(data.zone_clastic_p50_net14);
                        $('#zone_clastic_p90_net14').val(data.zone_clastic_p90_net14);
                        $('#zone_clastic_p10_por14').val(data.zone_clastic_p10_por14);
                        $('#zone_clastic_p50_por14').val(data.zone_clastic_p50_por14);
                        $('#zone_clastic_p90_por14').val(data.zone_clastic_p90_por14);
                        $('#zone_clastic_p10_satur14').val(data.zone_clastic_p10_satur14);
                        $('#zone_clastic_p50_satur14').val(data.zone_clastic_p50_satur14);
                        $('#zone_clastic_p90_satur14').val(data.zone_clastic_p90_satur14);
                        $('#zone_carbo_p10_thickness14').val(data.zone_carbo_p10_thickness14);
                        $('#zone_carbo_p50_thickness14').val(data.zone_carbo_p50_thickness14);
                        $('#zone_carbo_p90_thickness14').val(data.zone_carbo_p90_thickness14);
                        $('#zone_carbo_p10_dtc14').val(data.zone_carbo_p10_dtc14);
                        $('#zone_carbo_p50_dtc14').val(data.zone_carbo_p50_dtc14);
                        $('#zone_carbo_p90_dtc14').val(data.zone_carbo_p90_dtc14);
                        $('#zone_carbo_p10_total14').val(data.zone_carbo_p10_total14);
                        $('#zone_carbo_p50_total14').val(data.zone_carbo_p50_total14);
                        $('#zone_carbo_p90_total14').val(data.zone_carbo_p90_total14);
                        $('#zone_carbo_p10_net14').val(data.zone_carbo_p10_net14);
                        $('#zone_carbo_p50_net14').val(data.zone_carbo_p50_net14);
                        $('#zone_carbo_p90_net14').val(data.zone_carbo_p90_net14);
                        $('#zone_carbo_p10_por14').val(data.zone_carbo_p10_por14);
                        $('#zone_carbo_p50_por14').val(data.zone_carbo_p50_por14);
                        $('#zone_carbo_p90_por14').val(data.zone_carbo_p90_por14);
                        $('#zone_carbo_p10_satur14').val(data.zone_carbo_p10_satur14);
                        $('#zone_carbo_p50_satur14').val(data.zone_carbo_p50_satur14);
                        $('#zone_carbo_p90_satur14').val(data.zone_carbo_p90_satur14);
                        $('#z2_14').val(data.zone_fluid_date14);
                        $('#sampleat14').val(data.zone_fluid_sample14);
                        $('#gasoilratio_14').val(data.zone_fluid_ratio14);
                        $('#separator_pressure14').val(data.zone_fluid_pressure14);
                        $('#separator_temperature14').val(data.zone_fluid_temp14);
                        $('#tubing_pressure14').val(data.zone_fluid_tubing14);
                        $('#casing_pressure14').val(data.zone_fluid_casing14);
                        $('#sampleby14').val(data.zone_fluid_by14);
                        $('#repost_avail14').val(data.zone_fluid_report14);
                        $('#hydro_finger14').val(data.zone_fluid_finger14);
                        $('#oilgrav_6014').val(data.zone_fluid_grv_oil14);
                        $('#gasgrav_6014').val(data.zone_fluid_grv_gas14);
                        $('#condensategrav_6014').val(data.zone_fluid_grv_conden14);
                        $('#pvt_14').val(data.zone_fluid_pvt14);
                        $('#sample_14').val(data.zone_fluid_quantity14);
                        $('#gasdev_fac14').val(data.zone_fluid_z14);
                        $('#zone_fvf_oil_p1014').val(data.zone_fvf_oil_p1014);
                        $('#zone_fvf_oil_p5014').val(data.zone_fvf_oil_p5014);
                        $('#zone_fvf_oil_p9014').val(data.zone_fvf_oil_p9014);
                        $('#zone_fvf_gas_p1014').val(data.zone_fvf_gas_p1014);
                        $('#zone_fvf_gas_p5014').val(data.zone_fvf_gas_p5014);
                        $('#zone_fvf_gas_p9014').val(data.zone_fvf_gas_p9014);
                        $('#oilviscocity14').val(data.zone_viscocity14);
                            		
                        $('#zonename15').val(data.zone_name15);
                        $('#s2id_welltest15').select2('val', data.zone_test15);
                        $('#z_15').val(data.zone_test_date15);
                        $('#zonethickness15').val(data.zone_thickness15);
                        $('#zoneinterval15').val(data.zone_depth15);
                        $('#perforation_interval15').val(data.zone_perforation15);
                        $('#s2id_welltest_type15').select2('val', data.zone_well_test15);
                        $('#welltest_total15').val(data.zone_well_duration15);
                        $('#initialflow15').val(data.zone_initial_flow15);
                        $('#initialshutin15').val(data.zone_initial_shutin15);
                        $('#tubingsize15').val(data.zone_tubing_size15);
                        $('#initialtemperature15').val(data.zone_initial_temp15);
                        $('#zone_initial_pressure15').val(data.zone_initial_pressure15);
                        $('#res_presure15').val(data.zone_pseudostate15);
                        $('#pressurewell_for15').val(data.zone_well_formation15);
                        $('#pressurewell_head15').val(data.zone_head15);
                        $('#res_pressure15').val(data.zone_pressure_wellbore15);
                        $('#avg_porpsity15').val(data.zone_average_por15);
                        $('#watercut15').val(data.zone_water_cut15);
                        $('#initialwater_sat15').val(data.zone_initial_water15);
                        $('#lowtest_gas15').val(data.zone_low_gas15);
                        $('#lowtest_oil15').val(data.zone_low_oil15);
                        $('#freewater15').val(data.zone_free_water15);
                        $('#gas_grav15').val(data.zone_grv_gas15);
                        $('#oil_grav15').val(data.zone_grv_grv_oil15);
                        $('#zone_wellbore_coefficient15').val(data.zone_wellbore_coefficient15);
                        $('#wellbore_time15').val(data.zone_wellbore_time15);
                        $('#rsbt_15').val(data.zone_res_shape15);
                        $('#oilchoke15').val(data.zone_prod_oil_choke15);
                        $('#oilflow15').val(data.zone_prod_oil_flow15);
                        $('#gaschoke15').val(data.zone_prod_gas_choke15);
                        $('#gasflow15').val(data.zone_prod_gas_flow15);
                        $('#gasoilratio15').val(data.zone_prod_gasoil_ratio15);
                        $('#zone_prod_conden_ratio15').val(data.zone_prod_conden_ratio15);
                        $('#cummulative_pro_gas15').val(data.zone_prod_cumm_gas15);
                        $('#cummulative_pro_oil15').val(data.zone_prod_cumm_oil15);
                        $('#ab_openflow115').val(data.zone_prod_aof_bbl15);
                        $('#ab_openflow215').val(data.zone_prod_aof_scf15);
                        $('#criticalrate115').val(data.zone_prod_critical_bbl15);
                        $('#criticalrate215').val(data.zone_prod_critical_scf15);
                        $('#pro_index115').val(data.zone_prod_index_bbl15);
                        $('#pro_index215').val(data.zone_prod_index_scf15);
                        $('#diffusity_fact15').val(data.zone_prod_diffusity15);
                        $('#permeability15').val(data.zone_prod_permeability15);
                        $('#iafit_15').val(data.zone_prod_infinite15);
                        $('#wellradius15').val(data.zone_prod_well_radius15);
                        $('#pfit_15').val(data.zone_prod_pseudostate15);
                        $('#res_bondradius15').val(data.zone_prod_res_radius15);
                        $('#deltaP15').val(data.zone_prod_delta15);
                        $('#wellbore_skin15').val(data.zone_prod_wellbore15);
                        $('#rock_compress15').val(data.zone_prod_compres_rock15);
                        $('#fluid_compress15').val(data.zone_prod_compres_fluid15);
                        $('#total_compress15').val(data.zone_prod_compres_total15);
                        $('#secd_porosity15').val(data.zone_prod_second15);
                        $('#I_15').val(data.zone_prod_lambda15);
                        $('#W_15').val(data.zone_prod_omega15);
                        $('#oilshow_read115').val(data.zone_hc_oil_show15);
                        $('#oilshow_read215').val(data.zone_hc_oil_show_tools15);
                        $('#gasshow_read115').val(data.zone_hc_gas_show15);
                        $('#gasshow_read215').val(data.zone_hc_gas_show_tools15);
                        $('#wellmak_watercut15').val(data.zone_hc_water_cut15);
                        $('#water_bearing115').val(data.zone_hc_water_gwc15);
                        $('#water_bearing215').val(data.zone_hc_water_gwd_tools15);
                        $('#water_bearing315').val(data.zone_hc_water_owc15);
                        $('#water_bearing415').val(data.zone_hc_water_owc_tools15);
                        $('#s2id_rock_sampling15').select2('val', data.zone_rock_method15);
                        $('#s2id_petro_analys15').select2('val', data.zone_rock_petro15);
                        $('#sample15').val(data.zone_rock_petro_sample15);
                        $('#ticsd_15').val(data.zone_rock_top_depth15);
                        $('#bicsd_15').val(data.zone_rock_bot_depth15);
                        $('#num_totalcare15').val(data.zone_rock_barrel15);
                        $('#corebarrel15').val(data.zone_rock_barrel_equal15);
                        $('#totalrec15').val(data.zone_rock_total_core15);
                        $('#precore15').val(data.zone_rock_preservative15);
                        $('#rca_115').val(data.zone_rock_routine15);
                        $('#rca_15').val(data.zone_rock_routine_sample15);
                        $('#scal_115').val(data.zone_rock_scal15);
                        $('#scal_215').val(data.zone_rock_scal_sample15);
                        $('#zone_clastic_p10_thickness15').val(data.zone_clastic_p10_thickness15);
                        $('#zone_clastic_p50_thickness15').val(data.zone_clastic_p50_thickness15);
                        $('#zone_clastic_p90_thickness15').val(data.zone_clastic_p90_thickness15);
                        $('#zone_clastic_p10_gr15').val(data.zone_clastic_p10_gr15);
                        $('#zone_clastic_p50_gr15').val(data.zone_clastic_p50_gr15);
                        $('#zone_clastic_p90_gr15').val(data.zone_clastic_p90_gr15);
                        $('#zone_clastic_p10_sp15').val(data.zone_clastic_p10_sp15);
                        $('#zone_clastic_p50_sp15').val(data.zone_clastic_p50_sp15);
                        $('#zone_clastic_p90_sp15').val(data.zone_clastic_p90_sp15);
                        $('#zone_clastic_p10_net15').val(data.zone_clastic_p10_net15);
                        $('#zone_clastic_p50_net15').val(data.zone_clastic_p50_net15);
                        $('#zone_clastic_p90_net15').val(data.zone_clastic_p90_net15);
                        $('#zone_clastic_p10_por15').val(data.zone_clastic_p10_por15);
                        $('#zone_clastic_p50_por15').val(data.zone_clastic_p50_por15);
                        $('#zone_clastic_p90_por15').val(data.zone_clastic_p90_por15);
                        $('#zone_clastic_p10_satur15').val(data.zone_clastic_p10_satur15);
                        $('#zone_clastic_p50_satur15').val(data.zone_clastic_p50_satur15);
                        $('#zone_clastic_p90_satur15').val(data.zone_clastic_p90_satur15);
                        $('#zone_carbo_p10_thickness15').val(data.zone_carbo_p10_thickness15);
                        $('#zone_carbo_p50_thickness15').val(data.zone_carbo_p50_thickness15);
                        $('#zone_carbo_p90_thickness15').val(data.zone_carbo_p90_thickness15);
                        $('#zone_carbo_p10_dtc15').val(data.zone_carbo_p10_dtc15);
                        $('#zone_carbo_p50_dtc15').val(data.zone_carbo_p50_dtc15);
                        $('#zone_carbo_p90_dtc15').val(data.zone_carbo_p90_dtc15);
                        $('#zone_carbo_p10_total15').val(data.zone_carbo_p10_total15);
                        $('#zone_carbo_p50_total15').val(data.zone_carbo_p50_total15);
                        $('#zone_carbo_p90_total15').val(data.zone_carbo_p90_total15);
                        $('#zone_carbo_p10_net15').val(data.zone_carbo_p10_net15);
                        $('#zone_carbo_p50_net15').val(data.zone_carbo_p50_net15);
                        $('#zone_carbo_p90_net15').val(data.zone_carbo_p90_net15);
                        $('#zone_carbo_p10_por15').val(data.zone_carbo_p10_por15);
                        $('#zone_carbo_p50_por15').val(data.zone_carbo_p50_por15);
                        $('#zone_carbo_p90_por15').val(data.zone_carbo_p90_por15);
                        $('#zone_carbo_p10_satur15').val(data.zone_carbo_p10_satur15);
                        $('#zone_carbo_p50_satur15').val(data.zone_carbo_p50_satur15);
                        $('#zone_carbo_p90_satur15').val(data.zone_carbo_p90_satur15);
                        $('#z2_15').val(data.zone_fluid_date15);
                        $('#sampleat15').val(data.zone_fluid_sample15);
                        $('#gasoilratio_15').val(data.zone_fluid_ratio15);
                        $('#separator_pressure15').val(data.zone_fluid_pressure15);
                        $('#separator_temperature15').val(data.zone_fluid_temp15);
                        $('#tubing_pressure15').val(data.zone_fluid_tubing15);
                        $('#casing_pressure15').val(data.zone_fluid_casing15);
                        $('#sampleby15').val(data.zone_fluid_by15);
                        $('#repost_avail15').val(data.zone_fluid_report15);
                        $('#hydro_finger15').val(data.zone_fluid_finger15);
                        $('#oilgrav_6015').val(data.zone_fluid_grv_oil15);
                        $('#gasgrav_6015').val(data.zone_fluid_grv_gas15);
                        $('#condensategrav_6015').val(data.zone_fluid_grv_conden15);
                        $('#pvt_15').val(data.zone_fluid_pvt15);
                        $('#sample_15').val(data.zone_fluid_quantity15);
                        $('#gasdev_fac15').val(data.zone_fluid_z15);
                        $('#zone_fvf_oil_p1015').val(data.zone_fvf_oil_p1015);
                        $('#zone_fvf_oil_p5015').val(data.zone_fvf_oil_p5015);
                        $('#zone_fvf_oil_p9015').val(data.zone_fvf_oil_p9015);
                        $('#zone_fvf_gas_p1015').val(data.zone_fvf_gas_p1015);
                        $('#zone_fvf_gas_p5015').val(data.zone_fvf_gas_p5015);
                        $('#zone_fvf_gas_p9015').val(data.zone_fvf_gas_p9015);
                        $('#oilviscocity15').val(data.zone_viscocity15);
                            		
                        $('#zonename21').val(data.zone_name21);
                        $('#s2id_welltest21').select2('val', data.zone_test21);
                        $('#z_21').val(data.zone_test_date21);
                        $('#zonethickness21').val(data.zone_thickness21);
                        $('#zoneinterval21').val(data.zone_depth21);
                        $('#perforation_interval21').val(data.zone_perforation21);
                        $('#s2id_welltest_type21').select2('val', data.zone_well_test21);
                        $('#welltest_total21').val(data.zone_well_duration21);
                        $('#initialflow21').val(data.zone_initial_flow21);
                        $('#initialshutin21').val(data.zone_initial_shutin21);
                        $('#tubingsize21').val(data.zone_tubing_size21);
                        $('#initialtemperature21').val(data.zone_initial_temp21);
                        $('#zone_initial_pressure21').val(data.zone_initial_pressure21);
                        $('#res_presure21').val(data.zone_pseudostate21);
                        $('#pressurewell_for21').val(data.zone_well_formation21);
                        $('#pressurewell_head21').val(data.zone_head21);
                        $('#res_pressure21').val(data.zone_pressure_wellbore21);
                        $('#avg_porpsity21').val(data.zone_average_por21);
                        $('#watercut21').val(data.zone_water_cut21);
                        $('#initialwater_sat21').val(data.zone_initial_water21);
                        $('#lowtest_gas21').val(data.zone_low_gas21);
                        $('#lowtest_oil21').val(data.zone_low_oil21);
                        $('#freewater21').val(data.zone_free_water21);
                        $('#gas_grav21').val(data.zone_grv_gas21);
                        $('#oil_grav21').val(data.zone_grv_grv_oil21);
                        $('#zone_wellbore_coefficient21').val(data.zone_wellbore_coefficient21);
                        $('#wellbore_time21').val(data.zone_wellbore_time21);
                        $('#rsbt_21').val(data.zone_res_shape21);
                        $('#oilchoke21').val(data.zone_prod_oil_choke21);
                        $('#oilflow21').val(data.zone_prod_oil_flow21);
                        $('#gaschoke21').val(data.zone_prod_gas_choke21);
                        $('#gasflow21').val(data.zone_prod_gas_flow21);
                        $('#gasoilratio21').val(data.zone_prod_gasoil_ratio21);
                        $('#zone_prod_conden_ratio21').val(data.zone_prod_conden_ratio21);
                        $('#cummulative_pro_gas21').val(data.zone_prod_cumm_gas21);
                        $('#cummulative_pro_oil21').val(data.zone_prod_cumm_oil21);
                        $('#ab_openflow121').val(data.zone_prod_aof_bbl21);
                        $('#ab_openflow221').val(data.zone_prod_aof_scf21);
                        $('#criticalrate121').val(data.zone_prod_critical_bbl21);
                        $('#criticalrate221').val(data.zone_prod_critical_scf21);
                        $('#pro_index121').val(data.zone_prod_index_bbl21);
                        $('#pro_index221').val(data.zone_prod_index_scf21);
                        $('#diffusity_fact21').val(data.zone_prod_diffusity21);
                        $('#permeability21').val(data.zone_prod_permeability21);
                        $('#iafit_21').val(data.zone_prod_infinite21);
                        $('#wellradius21').val(data.zone_prod_well_radius21);
                        $('#pfit_21').val(data.zone_prod_pseudostate21);
                        $('#res_bondradius21').val(data.zone_prod_res_radius21);
                        $('#deltaP21').val(data.zone_prod_delta21);
                        $('#wellbore_skin21').val(data.zone_prod_wellbore21);
                        $('#rock_compress21').val(data.zone_prod_compres_rock21);
                        $('#fluid_compress21').val(data.zone_prod_compres_fluid21);
                        $('#total_compress21').val(data.zone_prod_compres_total21);
                        $('#secd_porosity21').val(data.zone_prod_second21);
                        $('#I_21').val(data.zone_prod_lambda21);
                        $('#W_21').val(data.zone_prod_omega21);
                        $('#oilshow_read121').val(data.zone_hc_oil_show21);
                        $('#oilshow_read221').val(data.zone_hc_oil_show_tools21);
                        $('#gasshow_read121').val(data.zone_hc_gas_show21);
                        $('#gasshow_read221').val(data.zone_hc_gas_show_tools21);
                        $('#wellmak_watercut21').val(data.zone_hc_water_cut21);
                        $('#water_bearing121').val(data.zone_hc_water_gwc21);
                        $('#water_bearing221').val(data.zone_hc_water_gwd_tools21);
                        $('#water_bearing321').val(data.zone_hc_water_owc21);
                        $('#water_bearing421').val(data.zone_hc_water_owc_tools21);
                        $('#s2id_rock_sampling21').select2('val', data.zone_rock_method21);
                        $('#s2id_petro_analys21').select2('val', data.zone_rock_petro21);
                        $('#sample21').val(data.zone_rock_petro_sample21);
                        $('#ticsd_21').val(data.zone_rock_top_depth21);
                        $('#bicsd_21').val(data.zone_rock_bot_depth21);
                        $('#num_totalcare21').val(data.zone_rock_barrel21);
                        $('#corebarrel21').val(data.zone_rock_barrel_equal21);
                        $('#totalrec21').val(data.zone_rock_total_core21);
                        $('#precore21').val(data.zone_rock_preservative21);
                        $('#rca_121').val(data.zone_rock_routine21);
                        $('#rca_21').val(data.zone_rock_routine_sample21);
                        $('#scal_121').val(data.zone_rock_scal21);
                        $('#scal_221').val(data.zone_rock_scal_sample21);
                        $('#zone_clastic_p10_thickness21').val(data.zone_clastic_p10_thickness21);
                        $('#zone_clastic_p50_thickness21').val(data.zone_clastic_p50_thickness21);
                        $('#zone_clastic_p90_thickness21').val(data.zone_clastic_p90_thickness21);
                        $('#zone_clastic_p10_gr21').val(data.zone_clastic_p10_gr21);
                        $('#zone_clastic_p50_gr21').val(data.zone_clastic_p50_gr21);
                        $('#zone_clastic_p90_gr21').val(data.zone_clastic_p90_gr21);
                        $('#zone_clastic_p10_sp21').val(data.zone_clastic_p10_sp21);
                        $('#zone_clastic_p50_sp21').val(data.zone_clastic_p50_sp21);
                        $('#zone_clastic_p90_sp21').val(data.zone_clastic_p90_sp21);
                        $('#zone_clastic_p10_net21').val(data.zone_clastic_p10_net21);
                        $('#zone_clastic_p50_net21').val(data.zone_clastic_p50_net21);
                        $('#zone_clastic_p90_net21').val(data.zone_clastic_p90_net21);
                        $('#zone_clastic_p10_por21').val(data.zone_clastic_p10_por21);
                        $('#zone_clastic_p50_por21').val(data.zone_clastic_p50_por21);
                        $('#zone_clastic_p90_por21').val(data.zone_clastic_p90_por21);
                        $('#zone_clastic_p10_satur21').val(data.zone_clastic_p10_satur21);
                        $('#zone_clastic_p50_satur21').val(data.zone_clastic_p50_satur21);
                        $('#zone_clastic_p90_satur21').val(data.zone_clastic_p90_satur21);
                        $('#zone_carbo_p10_thickness21').val(data.zone_carbo_p10_thickness21);
                        $('#zone_carbo_p50_thickness21').val(data.zone_carbo_p50_thickness21);
                        $('#zone_carbo_p90_thickness21').val(data.zone_carbo_p90_thickness21);
                        $('#zone_carbo_p10_dtc21').val(data.zone_carbo_p10_dtc21);
                        $('#zone_carbo_p50_dtc21').val(data.zone_carbo_p50_dtc21);
                        $('#zone_carbo_p90_dtc21').val(data.zone_carbo_p90_dtc21);
                        $('#zone_carbo_p10_total21').val(data.zone_carbo_p10_total21);
                        $('#zone_carbo_p50_total21').val(data.zone_carbo_p50_total21);
                        $('#zone_carbo_p90_total21').val(data.zone_carbo_p90_total21);
                        $('#zone_carbo_p10_net21').val(data.zone_carbo_p10_net21);
                        $('#zone_carbo_p50_net21').val(data.zone_carbo_p50_net21);
                        $('#zone_carbo_p90_net21').val(data.zone_carbo_p90_net21);
                        $('#zone_carbo_p10_por21').val(data.zone_carbo_p10_por21);
                        $('#zone_carbo_p50_por21').val(data.zone_carbo_p50_por21);
                        $('#zone_carbo_p90_por21').val(data.zone_carbo_p90_por21);
                        $('#zone_carbo_p10_satur21').val(data.zone_carbo_p10_satur21);
                        $('#zone_carbo_p50_satur21').val(data.zone_carbo_p50_satur21);
                        $('#zone_carbo_p90_satur21').val(data.zone_carbo_p90_satur21);
                        $('#z2_21').val(data.zone_fluid_date21);
                        $('#sampleat21').val(data.zone_fluid_sample21);
                        $('#gasoilratio_21').val(data.zone_fluid_ratio21);
                        $('#separator_pressure21').val(data.zone_fluid_pressure21);
                        $('#separator_temperature21').val(data.zone_fluid_temp21);
                        $('#tubing_pressure21').val(data.zone_fluid_tubing21);
                        $('#casing_pressure21').val(data.zone_fluid_casing21);
                        $('#sampleby21').val(data.zone_fluid_by21);
                        $('#repost_avail21').val(data.zone_fluid_report21);
                        $('#hydro_finger21').val(data.zone_fluid_finger21);
                        $('#oilgrav_6021').val(data.zone_fluid_grv_oil21);
                        $('#gasgrav_6021').val(data.zone_fluid_grv_gas21);
                        $('#condensategrav_6021').val(data.zone_fluid_grv_conden21);
                        $('#pvt_21').val(data.zone_fluid_pvt21);
                        $('#sample_21').val(data.zone_fluid_quantity21);
                        $('#gasdev_fac21').val(data.zone_fluid_z21);
                        $('#zone_fvf_oil_p1021').val(data.zone_fvf_oil_p1021);
                        $('#zone_fvf_oil_p5021').val(data.zone_fvf_oil_p5021);
                        $('#zone_fvf_oil_p9021').val(data.zone_fvf_oil_p9021);
                        $('#zone_fvf_gas_p1021').val(data.zone_fvf_gas_p1021);
                        $('#zone_fvf_gas_p5021').val(data.zone_fvf_gas_p5021);
                        $('#zone_fvf_gas_p9021').val(data.zone_fvf_gas_p9021);
                        $('#oilviscocity21').val(data.zone_viscocity21);
                            		
                        $('#zonename22').val(data.zone_name22);
                        $('#s2id_welltest22').select2('val', data.zone_test22);
                        $('#z_22').val(data.zone_test_date22);
                        $('#zonethickness22').val(data.zone_thickness22);
                        $('#zoneinterval22').val(data.zone_depth22);
                        $('#perforation_interval22').val(data.zone_perforation22);
                        $('#s2id_welltest_type22').select2('val', data.zone_well_test22);
                        $('#welltest_total22').val(data.zone_well_duration22);
                        $('#initialflow22').val(data.zone_initial_flow22);
                        $('#initialshutin22').val(data.zone_initial_shutin22);
                        $('#tubingsize22').val(data.zone_tubing_size22);
                        $('#initialtemperature22').val(data.zone_initial_temp22);
                        $('#zone_initial_pressure22').val(data.zone_initial_pressure22);
                        $('#res_presure22').val(data.zone_pseudostate22);
                        $('#pressurewell_for22').val(data.zone_well_formation22);
                        $('#pressurewell_head22').val(data.zone_head22);
                        $('#res_pressure22').val(data.zone_pressure_wellbore22);
                        $('#avg_porpsity22').val(data.zone_average_por22);
                        $('#watercut22').val(data.zone_water_cut22);
                        $('#initialwater_sat22').val(data.zone_initial_water22);
                        $('#lowtest_gas22').val(data.zone_low_gas22);
                        $('#lowtest_oil22').val(data.zone_low_oil22);
                        $('#freewater22').val(data.zone_free_water22);
                        $('#gas_grav22').val(data.zone_grv_gas22);
                        $('#oil_grav22').val(data.zone_grv_grv_oil22);
                        $('#zone_wellbore_coefficient22').val(data.zone_wellbore_coefficient22);
                        $('#wellbore_time22').val(data.zone_wellbore_time22);
                        $('#rsbt_22').val(data.zone_res_shape22);
                        $('#oilchoke22').val(data.zone_prod_oil_choke22);
                        $('#oilflow22').val(data.zone_prod_oil_flow22);
                        $('#gaschoke22').val(data.zone_prod_gas_choke22);
                        $('#gasflow22').val(data.zone_prod_gas_flow22);
                        $('#gasoilratio22').val(data.zone_prod_gasoil_ratio22);
                        $('#zone_prod_conden_ratio22').val(data.zone_prod_conden_ratio22);
                        $('#cummulative_pro_gas22').val(data.zone_prod_cumm_gas22);
                        $('#cummulative_pro_oil22').val(data.zone_prod_cumm_oil22);
                        $('#ab_openflow122').val(data.zone_prod_aof_bbl22);
                        $('#ab_openflow222').val(data.zone_prod_aof_scf22);
                        $('#criticalrate122').val(data.zone_prod_critical_bbl22);
                        $('#criticalrate222').val(data.zone_prod_critical_scf22);
                        $('#pro_index122').val(data.zone_prod_index_bbl22);
                        $('#pro_index222').val(data.zone_prod_index_scf22);
                        $('#diffusity_fact22').val(data.zone_prod_diffusity22);
                        $('#permeability22').val(data.zone_prod_permeability22);
                        $('#iafit_22').val(data.zone_prod_infinite22);
                        $('#wellradius22').val(data.zone_prod_well_radius22);
                        $('#pfit_22').val(data.zone_prod_pseudostate22);
                        $('#res_bondradius22').val(data.zone_prod_res_radius22);
                        $('#deltaP22').val(data.zone_prod_delta22);
                        $('#wellbore_skin22').val(data.zone_prod_wellbore22);
                        $('#rock_compress22').val(data.zone_prod_compres_rock22);
                        $('#fluid_compress22').val(data.zone_prod_compres_fluid22);
                        $('#total_compress22').val(data.zone_prod_compres_total22);
                        $('#secd_porosity22').val(data.zone_prod_second22);
                        $('#I_22').val(data.zone_prod_lambda22);
                        $('#W_22').val(data.zone_prod_omega22);
                        $('#oilshow_read122').val(data.zone_hc_oil_show22);
                        $('#oilshow_read222').val(data.zone_hc_oil_show_tools22);
                        $('#gasshow_read122').val(data.zone_hc_gas_show22);
                        $('#gasshow_read222').val(data.zone_hc_gas_show_tools22);
                        $('#wellmak_watercut22').val(data.zone_hc_water_cut22);
                        $('#water_bearing122').val(data.zone_hc_water_gwc22);
                        $('#water_bearing222').val(data.zone_hc_water_gwd_tools22);
                        $('#water_bearing322').val(data.zone_hc_water_owc22);
                        $('#water_bearing422').val(data.zone_hc_water_owc_tools22);
                        $('#s2id_rock_sampling22').select2('val', data.zone_rock_method22);
                        $('#s2id_petro_analys22').select2('val', data.zone_rock_petro22);
                        $('#sample22').val(data.zone_rock_petro_sample22);
                        $('#ticsd_22').val(data.zone_rock_top_depth22);
                        $('#bicsd_22').val(data.zone_rock_bot_depth22);
                        $('#num_totalcare22').val(data.zone_rock_barrel22);
                        $('#corebarrel22').val(data.zone_rock_barrel_equal22);
                        $('#totalrec22').val(data.zone_rock_total_core22);
                        $('#precore22').val(data.zone_rock_preservative22);
                        $('#rca_122').val(data.zone_rock_routine22);
                        $('#rca_22').val(data.zone_rock_routine_sample22);
                        $('#scal_122').val(data.zone_rock_scal22);
                        $('#scal_222').val(data.zone_rock_scal_sample22);
                        $('#zone_clastic_p10_thickness22').val(data.zone_clastic_p10_thickness22);
                        $('#zone_clastic_p50_thickness22').val(data.zone_clastic_p50_thickness22);
                        $('#zone_clastic_p90_thickness22').val(data.zone_clastic_p90_thickness22);
                        $('#zone_clastic_p10_gr22').val(data.zone_clastic_p10_gr22);
                        $('#zone_clastic_p50_gr22').val(data.zone_clastic_p50_gr22);
                        $('#zone_clastic_p90_gr22').val(data.zone_clastic_p90_gr22);
                        $('#zone_clastic_p10_sp22').val(data.zone_clastic_p10_sp22);
                        $('#zone_clastic_p50_sp22').val(data.zone_clastic_p50_sp22);
                        $('#zone_clastic_p90_sp22').val(data.zone_clastic_p90_sp22);
                        $('#zone_clastic_p10_net22').val(data.zone_clastic_p10_net22);
                        $('#zone_clastic_p50_net22').val(data.zone_clastic_p50_net22);
                        $('#zone_clastic_p90_net22').val(data.zone_clastic_p90_net22);
                        $('#zone_clastic_p10_por22').val(data.zone_clastic_p10_por22);
                        $('#zone_clastic_p50_por22').val(data.zone_clastic_p50_por22);
                        $('#zone_clastic_p90_por22').val(data.zone_clastic_p90_por22);
                        $('#zone_clastic_p10_satur22').val(data.zone_clastic_p10_satur22);
                        $('#zone_clastic_p50_satur22').val(data.zone_clastic_p50_satur22);
                        $('#zone_clastic_p90_satur22').val(data.zone_clastic_p90_satur22);
                        $('#zone_carbo_p10_thickness22').val(data.zone_carbo_p10_thickness22);
                        $('#zone_carbo_p50_thickness22').val(data.zone_carbo_p50_thickness22);
                        $('#zone_carbo_p90_thickness22').val(data.zone_carbo_p90_thickness22);
                        $('#zone_carbo_p10_dtc22').val(data.zone_carbo_p10_dtc22);
                        $('#zone_carbo_p50_dtc22').val(data.zone_carbo_p50_dtc22);
                        $('#zone_carbo_p90_dtc22').val(data.zone_carbo_p90_dtc22);
                        $('#zone_carbo_p10_total22').val(data.zone_carbo_p10_total22);
                        $('#zone_carbo_p50_total22').val(data.zone_carbo_p50_total22);
                        $('#zone_carbo_p90_total22').val(data.zone_carbo_p90_total22);
                        $('#zone_carbo_p10_net22').val(data.zone_carbo_p10_net22);
                        $('#zone_carbo_p50_net22').val(data.zone_carbo_p50_net22);
                        $('#zone_carbo_p90_net22').val(data.zone_carbo_p90_net22);
                        $('#zone_carbo_p10_por22').val(data.zone_carbo_p10_por22);
                        $('#zone_carbo_p50_por22').val(data.zone_carbo_p50_por22);
                        $('#zone_carbo_p90_por22').val(data.zone_carbo_p90_por22);
                        $('#zone_carbo_p10_satur22').val(data.zone_carbo_p10_satur22);
                        $('#zone_carbo_p50_satur22').val(data.zone_carbo_p50_satur22);
                        $('#zone_carbo_p90_satur22').val(data.zone_carbo_p90_satur22);
                        $('#z2_22').val(data.zone_fluid_date22);
                        $('#sampleat22').val(data.zone_fluid_sample22);
                        $('#gasoilratio_22').val(data.zone_fluid_ratio22);
                        $('#separator_pressure22').val(data.zone_fluid_pressure22);
                        $('#separator_temperature22').val(data.zone_fluid_temp22);
                        $('#tubing_pressure22').val(data.zone_fluid_tubing22);
                        $('#casing_pressure22').val(data.zone_fluid_casing22);
                        $('#sampleby22').val(data.zone_fluid_by22);
                        $('#repost_avail22').val(data.zone_fluid_report22);
                        $('#hydro_finger22').val(data.zone_fluid_finger22);
                        $('#oilgrav_6022').val(data.zone_fluid_grv_oil22);
                        $('#gasgrav_6022').val(data.zone_fluid_grv_gas22);
                        $('#condensategrav_6022').val(data.zone_fluid_grv_conden22);
                        $('#pvt_22').val(data.zone_fluid_pvt22);
                        $('#sample_22').val(data.zone_fluid_quantity22);
                        $('#gasdev_fac22').val(data.zone_fluid_z22);
                        $('#zone_fvf_oil_p1022').val(data.zone_fvf_oil_p1022);
                        $('#zone_fvf_oil_p5022').val(data.zone_fvf_oil_p5022);
                        $('#zone_fvf_oil_p9022').val(data.zone_fvf_oil_p9022);
                        $('#zone_fvf_gas_p1022').val(data.zone_fvf_gas_p1022);
                        $('#zone_fvf_gas_p5022').val(data.zone_fvf_gas_p5022);
                        $('#zone_fvf_gas_p9022').val(data.zone_fvf_gas_p9022);
                        $('#oilviscocity22').val(data.zone_viscocity22);
                            		
                        $('#zonename23').val(data.zone_name23);
                        $('#s2id_welltest23').select2('val', data.zone_test23);
                        $('#z_23').val(data.zone_test_date23);
                        $('#zonethickness23').val(data.zone_thickness23);
                        $('#zoneinterval23').val(data.zone_depth23);
                        $('#perforation_interval23').val(data.zone_perforation23);
                        $('#s2id_welltest_type23').select2('val', data.zone_well_test23);
                        $('#welltest_total23').val(data.zone_well_duration23);
                        $('#initialflow23').val(data.zone_initial_flow23);
                        $('#initialshutin23').val(data.zone_initial_shutin23);
                        $('#tubingsize23').val(data.zone_tubing_size23);
                        $('#initialtemperature23').val(data.zone_initial_temp23);
                        $('#zone_initial_pressure23').val(data.zone_initial_pressure23);
                        $('#res_presure23').val(data.zone_pseudostate23);
                        $('#pressurewell_for23').val(data.zone_well_formation23);
                        $('#pressurewell_head23').val(data.zone_head23);
                        $('#res_pressure23').val(data.zone_pressure_wellbore23);
                        $('#avg_porpsity23').val(data.zone_average_por23);
                        $('#watercut23').val(data.zone_water_cut23);
                        $('#initialwater_sat23').val(data.zone_initial_water23);
                        $('#lowtest_gas23').val(data.zone_low_gas23);
                        $('#lowtest_oil23').val(data.zone_low_oil23);
                        $('#freewater23').val(data.zone_free_water23);
                        $('#gas_grav23').val(data.zone_grv_gas23);
                        $('#oil_grav23').val(data.zone_grv_grv_oil23);
                        $('#zone_wellbore_coefficient23').val(data.zone_wellbore_coefficient23);
                        $('#wellbore_time23').val(data.zone_wellbore_time23);
                        $('#rsbt_23').val(data.zone_res_shape23);
                        $('#oilchoke23').val(data.zone_prod_oil_choke23);
                        $('#oilflow23').val(data.zone_prod_oil_flow23);
                        $('#gaschoke23').val(data.zone_prod_gas_choke23);
                        $('#gasflow23').val(data.zone_prod_gas_flow23);
                        $('#gasoilratio23').val(data.zone_prod_gasoil_ratio23);
                        $('#zone_prod_conden_ratio23').val(data.zone_prod_conden_ratio23);
                        $('#cummulative_pro_gas23').val(data.zone_prod_cumm_gas23);
                        $('#cummulative_pro_oil23').val(data.zone_prod_cumm_oil23);
                        $('#ab_openflow123').val(data.zone_prod_aof_bbl23);
                        $('#ab_openflow223').val(data.zone_prod_aof_scf23);
                        $('#criticalrate123').val(data.zone_prod_critical_bbl23);
                        $('#criticalrate223').val(data.zone_prod_critical_scf23);
                        $('#pro_index123').val(data.zone_prod_index_bbl23);
                        $('#pro_index223').val(data.zone_prod_index_scf23);
                        $('#diffusity_fact23').val(data.zone_prod_diffusity23);
                        $('#permeability23').val(data.zone_prod_permeability23);
                        $('#iafit_23').val(data.zone_prod_infinite23);
                        $('#wellradius23').val(data.zone_prod_well_radius23);
                        $('#pfit_23').val(data.zone_prod_pseudostate23);
                        $('#res_bondradius23').val(data.zone_prod_res_radius23);
                        $('#deltaP23').val(data.zone_prod_delta23);
                        $('#wellbore_skin23').val(data.zone_prod_wellbore23);
                        $('#rock_compress23').val(data.zone_prod_compres_rock23);
                        $('#fluid_compress23').val(data.zone_prod_compres_fluid23);
                        $('#total_compress23').val(data.zone_prod_compres_total23);
                        $('#secd_porosity23').val(data.zone_prod_second23);
                        $('#I_23').val(data.zone_prod_lambda23);
                        $('#W_23').val(data.zone_prod_omega23);
                        $('#oilshow_read123').val(data.zone_hc_oil_show23);
                        $('#oilshow_read223').val(data.zone_hc_oil_show_tools23);
                        $('#gasshow_read123').val(data.zone_hc_gas_show23);
                        $('#gasshow_read223').val(data.zone_hc_gas_show_tools23);
                        $('#wellmak_watercut23').val(data.zone_hc_water_cut23);
                        $('#water_bearing123').val(data.zone_hc_water_gwc23);
                        $('#water_bearing223').val(data.zone_hc_water_gwd_tools23);
                        $('#water_bearing323').val(data.zone_hc_water_owc23);
                        $('#water_bearing423').val(data.zone_hc_water_owc_tools23);
                        $('#s2id_rock_sampling23').select2('val', data.zone_rock_method23);
                        $('#s2id_petro_analys23').select2('val', data.zone_rock_petro23);
                        $('#sample23').val(data.zone_rock_petro_sample23);
                        $('#ticsd_23').val(data.zone_rock_top_depth23);
                        $('#bicsd_23').val(data.zone_rock_bot_depth23);
                        $('#num_totalcare23').val(data.zone_rock_barrel23);
                        $('#corebarrel23').val(data.zone_rock_barrel_equal23);
                        $('#totalrec23').val(data.zone_rock_total_core23);
                        $('#precore23').val(data.zone_rock_preservative23);
                        $('#rca_123').val(data.zone_rock_routine23);
                        $('#rca_23').val(data.zone_rock_routine_sample23);
                        $('#scal_123').val(data.zone_rock_scal23);
                        $('#scal_223').val(data.zone_rock_scal_sample23);
                        $('#zone_clastic_p10_thickness23').val(data.zone_clastic_p10_thickness23);
                        $('#zone_clastic_p50_thickness23').val(data.zone_clastic_p50_thickness23);
                        $('#zone_clastic_p90_thickness23').val(data.zone_clastic_p90_thickness23);
                        $('#zone_clastic_p10_gr23').val(data.zone_clastic_p10_gr23);
                        $('#zone_clastic_p50_gr23').val(data.zone_clastic_p50_gr23);
                        $('#zone_clastic_p90_gr23').val(data.zone_clastic_p90_gr23);
                        $('#zone_clastic_p10_sp23').val(data.zone_clastic_p10_sp23);
                        $('#zone_clastic_p50_sp23').val(data.zone_clastic_p50_sp23);
                        $('#zone_clastic_p90_sp23').val(data.zone_clastic_p90_sp23);
                        $('#zone_clastic_p10_net23').val(data.zone_clastic_p10_net23);
                        $('#zone_clastic_p50_net23').val(data.zone_clastic_p50_net23);
                        $('#zone_clastic_p90_net23').val(data.zone_clastic_p90_net23);
                        $('#zone_clastic_p10_por23').val(data.zone_clastic_p10_por23);
                        $('#zone_clastic_p50_por23').val(data.zone_clastic_p50_por23);
                        $('#zone_clastic_p90_por23').val(data.zone_clastic_p90_por23);
                        $('#zone_clastic_p10_satur23').val(data.zone_clastic_p10_satur23);
                        $('#zone_clastic_p50_satur23').val(data.zone_clastic_p50_satur23);
                        $('#zone_clastic_p90_satur23').val(data.zone_clastic_p90_satur23);
                        $('#zone_carbo_p10_thickness23').val(data.zone_carbo_p10_thickness23);
                        $('#zone_carbo_p50_thickness23').val(data.zone_carbo_p50_thickness23);
                        $('#zone_carbo_p90_thickness23').val(data.zone_carbo_p90_thickness23);
                        $('#zone_carbo_p10_dtc23').val(data.zone_carbo_p10_dtc23);
                        $('#zone_carbo_p50_dtc23').val(data.zone_carbo_p50_dtc23);
                        $('#zone_carbo_p90_dtc23').val(data.zone_carbo_p90_dtc23);
                        $('#zone_carbo_p10_total23').val(data.zone_carbo_p10_total23);
                        $('#zone_carbo_p50_total23').val(data.zone_carbo_p50_total23);
                        $('#zone_carbo_p90_total23').val(data.zone_carbo_p90_total23);
                        $('#zone_carbo_p10_net23').val(data.zone_carbo_p10_net23);
                        $('#zone_carbo_p50_net23').val(data.zone_carbo_p50_net23);
                        $('#zone_carbo_p90_net23').val(data.zone_carbo_p90_net23);
                        $('#zone_carbo_p10_por23').val(data.zone_carbo_p10_por23);
                        $('#zone_carbo_p50_por23').val(data.zone_carbo_p50_por23);
                        $('#zone_carbo_p90_por23').val(data.zone_carbo_p90_por23);
                        $('#zone_carbo_p10_satur23').val(data.zone_carbo_p10_satur23);
                        $('#zone_carbo_p50_satur23').val(data.zone_carbo_p50_satur23);
                        $('#zone_carbo_p90_satur23').val(data.zone_carbo_p90_satur23);
                        $('#z2_23').val(data.zone_fluid_date23);
                        $('#sampleat23').val(data.zone_fluid_sample23);
                        $('#gasoilratio_23').val(data.zone_fluid_ratio23);
                        $('#separator_pressure23').val(data.zone_fluid_pressure23);
                        $('#separator_temperature23').val(data.zone_fluid_temp23);
                        $('#tubing_pressure23').val(data.zone_fluid_tubing23);
                        $('#casing_pressure23').val(data.zone_fluid_casing23);
                        $('#sampleby23').val(data.zone_fluid_by23);
                        $('#repost_avail23').val(data.zone_fluid_report23);
                        $('#hydro_finger23').val(data.zone_fluid_finger23);
                        $('#oilgrav_6023').val(data.zone_fluid_grv_oil23);
                        $('#gasgrav_6023').val(data.zone_fluid_grv_gas23);
                        $('#condensategrav_6023').val(data.zone_fluid_grv_conden23);
                        $('#pvt_23').val(data.zone_fluid_pvt23);
                        $('#sample_23').val(data.zone_fluid_quantity23);
                        $('#gasdev_fac23').val(data.zone_fluid_z23);
                        $('#zone_fvf_oil_p1023').val(data.zone_fvf_oil_p1023);
                        $('#zone_fvf_oil_p5023').val(data.zone_fvf_oil_p5023);
                        $('#zone_fvf_oil_p9023').val(data.zone_fvf_oil_p9023);
                        $('#zone_fvf_gas_p1023').val(data.zone_fvf_gas_p1023);
                        $('#zone_fvf_gas_p5023').val(data.zone_fvf_gas_p5023);
                        $('#zone_fvf_gas_p9023').val(data.zone_fvf_gas_p9023);
                        $('#oilviscocity23').val(data.zone_viscocity23);
                        
                        $('#zonename24').val(data.zone_name24);
                        $('#s2id_welltest24').select2('val', data.zone_test24);
                        $('#z_24').val(data.zone_test_date24);
                        $('#zonethickness24').val(data.zone_thickness24);
                        $('#zoneinterval24').val(data.zone_depth24);
                        $('#perforation_interval24').val(data.zone_perforation24);
                        $('#s2id_welltest_type24').select2('val', data.zone_well_test24);
                        $('#welltest_total24').val(data.zone_well_duration24);
                        $('#initialflow24').val(data.zone_initial_flow24);
                        $('#initialshutin24').val(data.zone_initial_shutin24);
                        $('#tubingsize24').val(data.zone_tubing_size24);
                        $('#initialtemperature24').val(data.zone_initial_temp24);
                        $('#zone_initial_pressure24').val(data.zone_initial_pressure24);
                        $('#res_presure24').val(data.zone_pseudostate24);
                        $('#pressurewell_for24').val(data.zone_well_formation24);
                        $('#pressurewell_head24').val(data.zone_head24);
                        $('#res_pressure24').val(data.zone_pressure_wellbore24);
                        $('#avg_porpsity24').val(data.zone_average_por24);
                        $('#watercut24').val(data.zone_water_cut24);
                        $('#initialwater_sat24').val(data.zone_initial_water24);
                        $('#lowtest_gas24').val(data.zone_low_gas24);
                        $('#lowtest_oil24').val(data.zone_low_oil24);
                        $('#freewater24').val(data.zone_free_water24);
                        $('#gas_grav24').val(data.zone_grv_gas24);
                        $('#oil_grav24').val(data.zone_grv_grv_oil24);
                        $('#zone_wellbore_coefficient24').val(data.zone_wellbore_coefficient24);
                        $('#wellbore_time24').val(data.zone_wellbore_time24);
                        $('#rsbt_24').val(data.zone_res_shape24);
                        $('#oilchoke24').val(data.zone_prod_oil_choke24);
                        $('#oilflow24').val(data.zone_prod_oil_flow24);
                        $('#gaschoke24').val(data.zone_prod_gas_choke24);
                        $('#gasflow24').val(data.zone_prod_gas_flow24);
                        $('#gasoilratio24').val(data.zone_prod_gasoil_ratio24);
                        $('#zone_prod_conden_ratio24').val(data.zone_prod_conden_ratio24);
                        $('#cummulative_pro_gas24').val(data.zone_prod_cumm_gas24);
                        $('#cummulative_pro_oil24').val(data.zone_prod_cumm_oil24);
                        $('#ab_openflow124').val(data.zone_prod_aof_bbl24);
                        $('#ab_openflow224').val(data.zone_prod_aof_scf24);
                        $('#criticalrate124').val(data.zone_prod_critical_bbl24);
                        $('#criticalrate224').val(data.zone_prod_critical_scf24);
                        $('#pro_index124').val(data.zone_prod_index_bbl24);
                        $('#pro_index224').val(data.zone_prod_index_scf24);
                        $('#diffusity_fact24').val(data.zone_prod_diffusity24);
                        $('#permeability24').val(data.zone_prod_permeability24);
                        $('#iafit_24').val(data.zone_prod_infinite24);
                        $('#wellradius24').val(data.zone_prod_well_radius24);
                        $('#pfit_24').val(data.zone_prod_pseudostate24);
                        $('#res_bondradius24').val(data.zone_prod_res_radius24);
                        $('#deltaP24').val(data.zone_prod_delta24);
                        $('#wellbore_skin24').val(data.zone_prod_wellbore24);
                        $('#rock_compress24').val(data.zone_prod_compres_rock24);
                        $('#fluid_compress24').val(data.zone_prod_compres_fluid24);
                        $('#total_compress24').val(data.zone_prod_compres_total24);
                        $('#secd_porosity24').val(data.zone_prod_second24);
                        $('#I_24').val(data.zone_prod_lambda24);
                        $('#W_24').val(data.zone_prod_omega24);
                        $('#oilshow_read124').val(data.zone_hc_oil_show24);
                        $('#oilshow_read224').val(data.zone_hc_oil_show_tools24);
                        $('#gasshow_read124').val(data.zone_hc_gas_show24);
                        $('#gasshow_read224').val(data.zone_hc_gas_show_tools24);
                        $('#wellmak_watercut24').val(data.zone_hc_water_cut24);
                        $('#water_bearing124').val(data.zone_hc_water_gwc24);
                        $('#water_bearing224').val(data.zone_hc_water_gwd_tools24);
                        $('#water_bearing324').val(data.zone_hc_water_owc24);
                        $('#water_bearing424').val(data.zone_hc_water_owc_tools24);
                        $('#s2id_rock_sampling24').select2('val', data.zone_rock_method24);
                        $('#s2id_petro_analys24').select2('val', data.zone_rock_petro24);
                        $('#sample24').val(data.zone_rock_petro_sample24);
                        $('#ticsd_24').val(data.zone_rock_top_depth24);
                        $('#bicsd_24').val(data.zone_rock_bot_depth24);
                        $('#num_totalcare24').val(data.zone_rock_barrel24);
                        $('#corebarrel24').val(data.zone_rock_barrel_equal24);
                        $('#totalrec24').val(data.zone_rock_total_core24);
                        $('#precore24').val(data.zone_rock_preservative24);
                        $('#rca_124').val(data.zone_rock_routine24);
                        $('#rca_24').val(data.zone_rock_routine_sample24);
                        $('#scal_124').val(data.zone_rock_scal24);
                        $('#scal_224').val(data.zone_rock_scal_sample24);
                        $('#zone_clastic_p10_thickness24').val(data.zone_clastic_p10_thickness24);
                        $('#zone_clastic_p50_thickness24').val(data.zone_clastic_p50_thickness24);
                        $('#zone_clastic_p90_thickness24').val(data.zone_clastic_p90_thickness24);
                        $('#zone_clastic_p10_gr24').val(data.zone_clastic_p10_gr24);
                        $('#zone_clastic_p50_gr24').val(data.zone_clastic_p50_gr24);
                        $('#zone_clastic_p90_gr24').val(data.zone_clastic_p90_gr24);
                        $('#zone_clastic_p10_sp24').val(data.zone_clastic_p10_sp24);
                        $('#zone_clastic_p50_sp24').val(data.zone_clastic_p50_sp24);
                        $('#zone_clastic_p90_sp24').val(data.zone_clastic_p90_sp24);
                        $('#zone_clastic_p10_net24').val(data.zone_clastic_p10_net24);
                        $('#zone_clastic_p50_net24').val(data.zone_clastic_p50_net24);
                        $('#zone_clastic_p90_net24').val(data.zone_clastic_p90_net24);
                        $('#zone_clastic_p10_por24').val(data.zone_clastic_p10_por24);
                        $('#zone_clastic_p50_por24').val(data.zone_clastic_p50_por24);
                        $('#zone_clastic_p90_por24').val(data.zone_clastic_p90_por24);
                        $('#zone_clastic_p10_satur24').val(data.zone_clastic_p10_satur24);
                        $('#zone_clastic_p50_satur24').val(data.zone_clastic_p50_satur24);
                        $('#zone_clastic_p90_satur24').val(data.zone_clastic_p90_satur24);
                        $('#zone_carbo_p10_thickness24').val(data.zone_carbo_p10_thickness24);
                        $('#zone_carbo_p50_thickness24').val(data.zone_carbo_p50_thickness24);
                        $('#zone_carbo_p90_thickness24').val(data.zone_carbo_p90_thickness24);
                        $('#zone_carbo_p10_dtc24').val(data.zone_carbo_p10_dtc24);
                        $('#zone_carbo_p50_dtc24').val(data.zone_carbo_p50_dtc24);
                        $('#zone_carbo_p90_dtc24').val(data.zone_carbo_p90_dtc24);
                        $('#zone_carbo_p10_total24').val(data.zone_carbo_p10_total24);
                        $('#zone_carbo_p50_total24').val(data.zone_carbo_p50_total24);
                        $('#zone_carbo_p90_total24').val(data.zone_carbo_p90_total24);
                        $('#zone_carbo_p10_net24').val(data.zone_carbo_p10_net24);
                        $('#zone_carbo_p50_net24').val(data.zone_carbo_p50_net24);
                        $('#zone_carbo_p90_net24').val(data.zone_carbo_p90_net24);
                        $('#zone_carbo_p10_por24').val(data.zone_carbo_p10_por24);
                        $('#zone_carbo_p50_por24').val(data.zone_carbo_p50_por24);
                        $('#zone_carbo_p90_por24').val(data.zone_carbo_p90_por24);
                        $('#zone_carbo_p10_satur24').val(data.zone_carbo_p10_satur24);
                        $('#zone_carbo_p50_satur24').val(data.zone_carbo_p50_satur24);
                        $('#zone_carbo_p90_satur24').val(data.zone_carbo_p90_satur24);
                        $('#z2_24').val(data.zone_fluid_date24);
                        $('#sampleat24').val(data.zone_fluid_sample24);
                        $('#gasoilratio_24').val(data.zone_fluid_ratio24);
                        $('#separator_pressure24').val(data.zone_fluid_pressure24);
                        $('#separator_temperature24').val(data.zone_fluid_temp24);
                        $('#tubing_pressure24').val(data.zone_fluid_tubing24);
                        $('#casing_pressure24').val(data.zone_fluid_casing24);
                        $('#sampleby24').val(data.zone_fluid_by24);
                        $('#repost_avail24').val(data.zone_fluid_report24);
                        $('#hydro_finger24').val(data.zone_fluid_finger24);
                        $('#oilgrav_6024').val(data.zone_fluid_grv_oil24);
                        $('#gasgrav_6024').val(data.zone_fluid_grv_gas24);
                        $('#condensategrav_6024').val(data.zone_fluid_grv_conden24);
                        $('#pvt_24').val(data.zone_fluid_pvt24);
                        $('#sample_24').val(data.zone_fluid_quantity24);
                        $('#gasdev_fac24').val(data.zone_fluid_z24);
                        $('#zone_fvf_oil_p1024').val(data.zone_fvf_oil_p1024);
                        $('#zone_fvf_oil_p5024').val(data.zone_fvf_oil_p5024);
                        $('#zone_fvf_oil_p9024').val(data.zone_fvf_oil_p9024);
                        $('#zone_fvf_gas_p1024').val(data.zone_fvf_gas_p1024);
                        $('#zone_fvf_gas_p5024').val(data.zone_fvf_gas_p5024);
                        $('#zone_fvf_gas_p9024').val(data.zone_fvf_gas_p9024);
                        $('#oilviscocity24').val(data.zone_viscocity24);
                            		
                        $('#zonename25').val(data.zone_name25);
                        $('#s2id_welltest25').select2('val', data.zone_test25);
                        $('#z_25').val(data.zone_test_date25);
                        $('#zonethickness25').val(data.zone_thickness25);
                        $('#zoneinterval25').val(data.zone_depth25);
                        $('#perforation_interval25').val(data.zone_perforation25);
                        $('#s2id_welltest_type25').select2('val', data.zone_well_test25);
                        $('#welltest_total25').val(data.zone_well_duration25);
                        $('#initialflow25').val(data.zone_initial_flow25);
                        $('#initialshutin25').val(data.zone_initial_shutin25);
                        $('#tubingsize25').val(data.zone_tubing_size25);
                        $('#initialtemperature25').val(data.zone_initial_temp25);
                        $('#zone_initial_pressure25').val(data.zone_initial_pressure25);
                        $('#res_presure25').val(data.zone_pseudostate25);
                        $('#pressurewell_for25').val(data.zone_well_formation25);
                        $('#pressurewell_head25').val(data.zone_head25);
                        $('#res_pressure25').val(data.zone_pressure_wellbore25);
                        $('#avg_porpsity25').val(data.zone_average_por25);
                        $('#watercut25').val(data.zone_water_cut25);
                        $('#initialwater_sat25').val(data.zone_initial_water25);
                        $('#lowtest_gas25').val(data.zone_low_gas25);
                        $('#lowtest_oil25').val(data.zone_low_oil25);
                        $('#freewater25').val(data.zone_free_water25);
                        $('#gas_grav25').val(data.zone_grv_gas25);
                        $('#oil_grav25').val(data.zone_grv_grv_oil25);
                        $('#zone_wellbore_coefficient25').val(data.zone_wellbore_coefficient25);
                        $('#wellbore_time25').val(data.zone_wellbore_time25);
                        $('#rsbt_25').val(data.zone_res_shape25);
                        $('#oilchoke25').val(data.zone_prod_oil_choke25);
                        $('#oilflow25').val(data.zone_prod_oil_flow25);
                        $('#gaschoke25').val(data.zone_prod_gas_choke25);
                        $('#gasflow25').val(data.zone_prod_gas_flow25);
                        $('#gasoilratio25').val(data.zone_prod_gasoil_ratio25);
                        $('#zone_prod_conden_ratio25').val(data.zone_prod_conden_ratio25);
                        $('#cummulative_pro_gas25').val(data.zone_prod_cumm_gas25);
                        $('#cummulative_pro_oil25').val(data.zone_prod_cumm_oil25);
                        $('#ab_openflow125').val(data.zone_prod_aof_bbl25);
                        $('#ab_openflow225').val(data.zone_prod_aof_scf25);
                        $('#criticalrate125').val(data.zone_prod_critical_bbl25);
                        $('#criticalrate225').val(data.zone_prod_critical_scf25);
                        $('#pro_index125').val(data.zone_prod_index_bbl25);
                        $('#pro_index225').val(data.zone_prod_index_scf25);
                        $('#diffusity_fact25').val(data.zone_prod_diffusity25);
                        $('#permeability25').val(data.zone_prod_permeability25);
                        $('#iafit_25').val(data.zone_prod_infinite25);
                        $('#wellradius25').val(data.zone_prod_well_radius25);
                        $('#pfit_25').val(data.zone_prod_pseudostate25);
                        $('#res_bondradius25').val(data.zone_prod_res_radius25);
                        $('#deltaP25').val(data.zone_prod_delta25);
                        $('#wellbore_skin25').val(data.zone_prod_wellbore25);
                        $('#rock_compress25').val(data.zone_prod_compres_rock25);
                        $('#fluid_compress25').val(data.zone_prod_compres_fluid25);
                        $('#total_compress25').val(data.zone_prod_compres_total25);
                        $('#secd_porosity25').val(data.zone_prod_second25);
                        $('#I_25').val(data.zone_prod_lambda25);
                        $('#W_25').val(data.zone_prod_omega25);
                        $('#oilshow_read125').val(data.zone_hc_oil_show25);
                        $('#oilshow_read225').val(data.zone_hc_oil_show_tools25);
                        $('#gasshow_read125').val(data.zone_hc_gas_show25);
                        $('#gasshow_read225').val(data.zone_hc_gas_show_tools25);
                        $('#wellmak_watercut25').val(data.zone_hc_water_cut25);
                        $('#water_bearing125').val(data.zone_hc_water_gwc25);
                        $('#water_bearing225').val(data.zone_hc_water_gwd_tools25);
                        $('#water_bearing325').val(data.zone_hc_water_owc25);
                        $('#water_bearing425').val(data.zone_hc_water_owc_tools25);
                        $('#s2id_rock_sampling25').select2('val', data.zone_rock_method25);
                        $('#s2id_petro_analys25').select2('val', data.zone_rock_petro25);
                        $('#sample25').val(data.zone_rock_petro_sample25);
                        $('#ticsd_25').val(data.zone_rock_top_depth25);
                        $('#bicsd_25').val(data.zone_rock_bot_depth25);
                        $('#num_totalcare25').val(data.zone_rock_barrel25);
                        $('#corebarrel25').val(data.zone_rock_barrel_equal25);
                        $('#totalrec25').val(data.zone_rock_total_core25);
                        $('#precore25').val(data.zone_rock_preservative25);
                        $('#rca_125').val(data.zone_rock_routine25);
                        $('#rca_25').val(data.zone_rock_routine_sample25);
                        $('#scal_125').val(data.zone_rock_scal25);
                        $('#scal_225').val(data.zone_rock_scal_sample25);
                        $('#zone_clastic_p10_thickness25').val(data.zone_clastic_p10_thickness25);
                        $('#zone_clastic_p50_thickness25').val(data.zone_clastic_p50_thickness25);
                        $('#zone_clastic_p90_thickness25').val(data.zone_clastic_p90_thickness25);
                        $('#zone_clastic_p10_gr25').val(data.zone_clastic_p10_gr25);
                        $('#zone_clastic_p50_gr25').val(data.zone_clastic_p50_gr25);
                        $('#zone_clastic_p90_gr25').val(data.zone_clastic_p90_gr25);
                        $('#zone_clastic_p10_sp25').val(data.zone_clastic_p10_sp25);
                        $('#zone_clastic_p50_sp25').val(data.zone_clastic_p50_sp25);
                        $('#zone_clastic_p90_sp25').val(data.zone_clastic_p90_sp25);
                        $('#zone_clastic_p10_net25').val(data.zone_clastic_p10_net25);
                        $('#zone_clastic_p50_net25').val(data.zone_clastic_p50_net25);
                        $('#zone_clastic_p90_net25').val(data.zone_clastic_p90_net25);
                        $('#zone_clastic_p10_por25').val(data.zone_clastic_p10_por25);
                        $('#zone_clastic_p50_por25').val(data.zone_clastic_p50_por25);
                        $('#zone_clastic_p90_por25').val(data.zone_clastic_p90_por25);
                        $('#zone_clastic_p10_satur25').val(data.zone_clastic_p10_satur25);
                        $('#zone_clastic_p50_satur25').val(data.zone_clastic_p50_satur25);
                        $('#zone_clastic_p90_satur25').val(data.zone_clastic_p90_satur25);
                        $('#zone_carbo_p10_thickness25').val(data.zone_carbo_p10_thickness25);
                        $('#zone_carbo_p50_thickness25').val(data.zone_carbo_p50_thickness25);
                        $('#zone_carbo_p90_thickness25').val(data.zone_carbo_p90_thickness25);
                        $('#zone_carbo_p10_dtc25').val(data.zone_carbo_p10_dtc25);
                        $('#zone_carbo_p50_dtc25').val(data.zone_carbo_p50_dtc25);
                        $('#zone_carbo_p90_dtc25').val(data.zone_carbo_p90_dtc25);
                        $('#zone_carbo_p10_total25').val(data.zone_carbo_p10_total25);
                        $('#zone_carbo_p50_total25').val(data.zone_carbo_p50_total25);
                        $('#zone_carbo_p90_total25').val(data.zone_carbo_p90_total25);
                        $('#zone_carbo_p10_net25').val(data.zone_carbo_p10_net25);
                        $('#zone_carbo_p50_net25').val(data.zone_carbo_p50_net25);
                        $('#zone_carbo_p90_net25').val(data.zone_carbo_p90_net25);
                        $('#zone_carbo_p10_por25').val(data.zone_carbo_p10_por25);
                        $('#zone_carbo_p50_por25').val(data.zone_carbo_p50_por25);
                        $('#zone_carbo_p90_por25').val(data.zone_carbo_p90_por25);
                        $('#zone_carbo_p10_satur25').val(data.zone_carbo_p10_satur25);
                        $('#zone_carbo_p50_satur25').val(data.zone_carbo_p50_satur25);
                        $('#zone_carbo_p90_satur25').val(data.zone_carbo_p90_satur25);
                        $('#z2_25').val(data.zone_fluid_date25);
                        $('#sampleat25').val(data.zone_fluid_sample25);
                        $('#gasoilratio_25').val(data.zone_fluid_ratio25);
                        $('#separator_pressure25').val(data.zone_fluid_pressure25);
                        $('#separator_temperature25').val(data.zone_fluid_temp25);
                        $('#tubing_pressure25').val(data.zone_fluid_tubing25);
                        $('#casing_pressure25').val(data.zone_fluid_casing25);
                        $('#sampleby25').val(data.zone_fluid_by25);
                        $('#repost_avail25').val(data.zone_fluid_report25);
                        $('#hydro_finger25').val(data.zone_fluid_finger25);
                        $('#oilgrav_6025').val(data.zone_fluid_grv_oil25);
                        $('#gasgrav_6025').val(data.zone_fluid_grv_gas25);
                        $('#condensategrav_6025').val(data.zone_fluid_grv_conden25);
                        $('#pvt_25').val(data.zone_fluid_pvt25);
                        $('#sample_25').val(data.zone_fluid_quantity25);
                        $('#gasdev_fac25').val(data.zone_fluid_z25);
                        $('#zone_fvf_oil_p1025').val(data.zone_fvf_oil_p1025);
                        $('#zone_fvf_oil_p5025').val(data.zone_fvf_oil_p5025);
                        $('#zone_fvf_oil_p9025').val(data.zone_fvf_oil_p9025);
                        $('#zone_fvf_gas_p1025').val(data.zone_fvf_gas_p1025);
                        $('#zone_fvf_gas_p5025').val(data.zone_fvf_gas_p5025);
                        $('#zone_fvf_gas_p9025').val(data.zone_fvf_gas_p9025);
                        $('#oilviscocity25').val(data.zone_viscocity25);
                            		
                        $('#zonename31').val(data.zone_name31);
                        $('#s2id_welltest31').select2('val', data.zone_test31);
                        $('#z_31').val(data.zone_test_date31);
                        $('#zonethickness31').val(data.zone_thickness31);
                        $('#zoneinterval31').val(data.zone_depth31);
                        $('#perforation_interval31').val(data.zone_perforation31);
                        $('#s2id_welltest_type31').select2('val', data.zone_well_test31);
                        $('#welltest_total31').val(data.zone_well_duration31);
                        $('#initialflow31').val(data.zone_initial_flow31);
                        $('#initialshutin31').val(data.zone_initial_shutin31);
                        $('#tubingsize31').val(data.zone_tubing_size31);
                        $('#initialtemperature31').val(data.zone_initial_temp31);
                        $('#zone_initial_pressure31').val(data.zone_initial_pressure31);
                        $('#res_presure31').val(data.zone_pseudostate31);
                        $('#pressurewell_for31').val(data.zone_well_formation31);
                        $('#pressurewell_head31').val(data.zone_head31);
                        $('#res_pressure31').val(data.zone_pressure_wellbore31);
                        $('#avg_porpsity31').val(data.zone_average_por31);
                        $('#watercut31').val(data.zone_water_cut31);
                        $('#initialwater_sat31').val(data.zone_initial_water31);
                        $('#lowtest_gas31').val(data.zone_low_gas31);
                        $('#lowtest_oil31').val(data.zone_low_oil31);
                        $('#freewater31').val(data.zone_free_water31);
                        $('#gas_grav31').val(data.zone_grv_gas31);
                        $('#oil_grav31').val(data.zone_grv_grv_oil31);
                        $('#zone_wellbore_coefficient31').val(data.zone_wellbore_coefficient31);
                        $('#wellbore_time31').val(data.zone_wellbore_time31);
                        $('#rsbt_31').val(data.zone_res_shape31);
                        $('#oilchoke31').val(data.zone_prod_oil_choke31);
                        $('#oilflow31').val(data.zone_prod_oil_flow31);
                        $('#gaschoke31').val(data.zone_prod_gas_choke31);
                        $('#gasflow31').val(data.zone_prod_gas_flow31);
                        $('#gasoilratio31').val(data.zone_prod_gasoil_ratio31);
                        $('#zone_prod_conden_ratio31').val(data.zone_prod_conden_ratio31);
                        $('#cummulative_pro_gas31').val(data.zone_prod_cumm_gas31);
                        $('#cummulative_pro_oil31').val(data.zone_prod_cumm_oil31);
                        $('#ab_openflow131').val(data.zone_prod_aof_bbl31);
                        $('#ab_openflow231').val(data.zone_prod_aof_scf31);
                        $('#criticalrate131').val(data.zone_prod_critical_bbl31);
                        $('#criticalrate231').val(data.zone_prod_critical_scf31);
                        $('#pro_index131').val(data.zone_prod_index_bbl31);
                        $('#pro_index231').val(data.zone_prod_index_scf31);
                        $('#diffusity_fact31').val(data.zone_prod_diffusity31);
                        $('#permeability31').val(data.zone_prod_permeability31);
                        $('#iafit_31').val(data.zone_prod_infinite31);
                        $('#wellradius31').val(data.zone_prod_well_radius31);
                        $('#pfit_31').val(data.zone_prod_pseudostate31);
                        $('#res_bondradius31').val(data.zone_prod_res_radius31);
                        $('#deltaP31').val(data.zone_prod_delta31);
                        $('#wellbore_skin31').val(data.zone_prod_wellbore31);
                        $('#rock_compress31').val(data.zone_prod_compres_rock31);
                        $('#fluid_compress31').val(data.zone_prod_compres_fluid31);
                        $('#total_compress31').val(data.zone_prod_compres_total31);
                        $('#secd_porosity31').val(data.zone_prod_second31);
                        $('#I_31').val(data.zone_prod_lambda31);
                        $('#W_31').val(data.zone_prod_omega31);
                        $('#oilshow_read131').val(data.zone_hc_oil_show31);
                        $('#oilshow_read231').val(data.zone_hc_oil_show_tools31);
                        $('#gasshow_read131').val(data.zone_hc_gas_show31);
                        $('#gasshow_read231').val(data.zone_hc_gas_show_tools31);
                        $('#wellmak_watercut31').val(data.zone_hc_water_cut31);
                        $('#water_bearing131').val(data.zone_hc_water_gwc31);
                        $('#water_bearing231').val(data.zone_hc_water_gwd_tools31);
                        $('#water_bearing331').val(data.zone_hc_water_owc31);
                        $('#water_bearing431').val(data.zone_hc_water_owc_tools31);
                        $('#s2id_rock_sampling31').select2('val', data.zone_rock_method31);
                        $('#s2id_petro_analys31').select2('val', data.zone_rock_petro31);
                        $('#sample31').val(data.zone_rock_petro_sample31);
                        $('#ticsd_31').val(data.zone_rock_top_depth31);
                        $('#bicsd_31').val(data.zone_rock_bot_depth31);
                        $('#num_totalcare31').val(data.zone_rock_barrel31);
                        $('#corebarrel31').val(data.zone_rock_barrel_equal31);
                        $('#totalrec31').val(data.zone_rock_total_core31);
                        $('#precore31').val(data.zone_rock_preservative31);
                        $('#rca_131').val(data.zone_rock_routine31);
                        $('#rca_31').val(data.zone_rock_routine_sample31);
                        $('#scal_131').val(data.zone_rock_scal31);
                        $('#scal_231').val(data.zone_rock_scal_sample31);
                        $('#zone_clastic_p10_thickness31').val(data.zone_clastic_p10_thickness31);
                        $('#zone_clastic_p50_thickness31').val(data.zone_clastic_p50_thickness31);
                        $('#zone_clastic_p90_thickness31').val(data.zone_clastic_p90_thickness31);
                        $('#zone_clastic_p10_gr31').val(data.zone_clastic_p10_gr31);
                        $('#zone_clastic_p50_gr31').val(data.zone_clastic_p50_gr31);
                        $('#zone_clastic_p90_gr31').val(data.zone_clastic_p90_gr31);
                        $('#zone_clastic_p10_sp31').val(data.zone_clastic_p10_sp31);
                        $('#zone_clastic_p50_sp31').val(data.zone_clastic_p50_sp31);
                        $('#zone_clastic_p90_sp31').val(data.zone_clastic_p90_sp31);
                        $('#zone_clastic_p10_net31').val(data.zone_clastic_p10_net31);
                        $('#zone_clastic_p50_net31').val(data.zone_clastic_p50_net31);
                        $('#zone_clastic_p90_net31').val(data.zone_clastic_p90_net31);
                        $('#zone_clastic_p10_por31').val(data.zone_clastic_p10_por31);
                        $('#zone_clastic_p50_por31').val(data.zone_clastic_p50_por31);
                        $('#zone_clastic_p90_por31').val(data.zone_clastic_p90_por31);
                        $('#zone_clastic_p10_satur31').val(data.zone_clastic_p10_satur31);
                        $('#zone_clastic_p50_satur31').val(data.zone_clastic_p50_satur31);
                        $('#zone_clastic_p90_satur31').val(data.zone_clastic_p90_satur31);
                        $('#zone_carbo_p10_thickness31').val(data.zone_carbo_p10_thickness31);
                        $('#zone_carbo_p50_thickness31').val(data.zone_carbo_p50_thickness31);
                        $('#zone_carbo_p90_thickness31').val(data.zone_carbo_p90_thickness31);
                        $('#zone_carbo_p10_dtc31').val(data.zone_carbo_p10_dtc31);
                        $('#zone_carbo_p50_dtc31').val(data.zone_carbo_p50_dtc31);
                        $('#zone_carbo_p90_dtc31').val(data.zone_carbo_p90_dtc31);
                        $('#zone_carbo_p10_total31').val(data.zone_carbo_p10_total31);
                        $('#zone_carbo_p50_total31').val(data.zone_carbo_p50_total31);
                        $('#zone_carbo_p90_total31').val(data.zone_carbo_p90_total31);
                        $('#zone_carbo_p10_net31').val(data.zone_carbo_p10_net31);
                        $('#zone_carbo_p50_net31').val(data.zone_carbo_p50_net31);
                        $('#zone_carbo_p90_net31').val(data.zone_carbo_p90_net31);
                        $('#zone_carbo_p10_por31').val(data.zone_carbo_p10_por31);
                        $('#zone_carbo_p50_por31').val(data.zone_carbo_p50_por31);
                        $('#zone_carbo_p90_por31').val(data.zone_carbo_p90_por31);
                        $('#zone_carbo_p10_satur31').val(data.zone_carbo_p10_satur31);
                        $('#zone_carbo_p50_satur31').val(data.zone_carbo_p50_satur31);
                        $('#zone_carbo_p90_satur31').val(data.zone_carbo_p90_satur31);
                        $('#z2_31').val(data.zone_fluid_date31);
                        $('#sampleat31').val(data.zone_fluid_sample31);
                        $('#gasoilratio_31').val(data.zone_fluid_ratio31);
                        $('#separator_pressure31').val(data.zone_fluid_pressure31);
                        $('#separator_temperature31').val(data.zone_fluid_temp31);
                        $('#tubing_pressure31').val(data.zone_fluid_tubing31);
                        $('#casing_pressure31').val(data.zone_fluid_casing31);
                        $('#sampleby31').val(data.zone_fluid_by31);
                        $('#repost_avail31').val(data.zone_fluid_report31);
                        $('#hydro_finger31').val(data.zone_fluid_finger31);
                        $('#oilgrav_6031').val(data.zone_fluid_grv_oil31);
                        $('#gasgrav_6031').val(data.zone_fluid_grv_gas31);
                        $('#condensategrav_6031').val(data.zone_fluid_grv_conden31);
                        $('#pvt_31').val(data.zone_fluid_pvt31);
                        $('#sample_31').val(data.zone_fluid_quantity31);
                        $('#gasdev_fac31').val(data.zone_fluid_z31);
                        $('#zone_fvf_oil_p1031').val(data.zone_fvf_oil_p1031);
                        $('#zone_fvf_oil_p5031').val(data.zone_fvf_oil_p5031);
                        $('#zone_fvf_oil_p9031').val(data.zone_fvf_oil_p9031);
                        $('#zone_fvf_gas_p1031').val(data.zone_fvf_gas_p1031);
                        $('#zone_fvf_gas_p5031').val(data.zone_fvf_gas_p5031);
                        $('#zone_fvf_gas_p9031').val(data.zone_fvf_gas_p9031);
                        $('#oilviscocity31').val(data.zone_viscocity31);
                            		
                        $('#zonename32').val(data.zone_name32);
                        $('#s2id_welltest32').select2('val', data.zone_test32);
                        $('#z_32').val(data.zone_test_date32);
                        $('#zonethickness32').val(data.zone_thickness32);
                        $('#zoneinterval32').val(data.zone_depth32);
                        $('#perforation_interval32').val(data.zone_perforation32);
                        $('#s2id_welltest_type32').select2('val', data.zone_well_test32);
                        $('#welltest_total32').val(data.zone_well_duration32);
                        $('#initialflow32').val(data.zone_initial_flow32);
                        $('#initialshutin32').val(data.zone_initial_shutin32);
                        $('#tubingsize32').val(data.zone_tubing_size32);
                        $('#initialtemperature32').val(data.zone_initial_temp32);
                        $('#zone_initial_pressure32').val(data.zone_initial_pressure32);
                        $('#res_presure32').val(data.zone_pseudostate32);
                        $('#pressurewell_for32').val(data.zone_well_formation32);
                        $('#pressurewell_head32').val(data.zone_head32);
                        $('#res_pressure32').val(data.zone_pressure_wellbore32);
                        $('#avg_porpsity32').val(data.zone_average_por32);
                        $('#watercut32').val(data.zone_water_cut32);
                        $('#initialwater_sat32').val(data.zone_initial_water32);
                        $('#lowtest_gas32').val(data.zone_low_gas32);
                        $('#lowtest_oil32').val(data.zone_low_oil32);
                        $('#freewater32').val(data.zone_free_water32);
                        $('#gas_grav32').val(data.zone_grv_gas32);
                        $('#oil_grav32').val(data.zone_grv_grv_oil32);
                        $('#zone_wellbore_coefficient32').val(data.zone_wellbore_coefficient32);
                        $('#wellbore_time32').val(data.zone_wellbore_time32);
                        $('#rsbt_32').val(data.zone_res_shape32);
                        $('#oilchoke32').val(data.zone_prod_oil_choke32);
                        $('#oilflow32').val(data.zone_prod_oil_flow32);
                        $('#gaschoke32').val(data.zone_prod_gas_choke32);
                        $('#gasflow32').val(data.zone_prod_gas_flow32);
                        $('#gasoilratio32').val(data.zone_prod_gasoil_ratio32);
                        $('#zone_prod_conden_ratio32').val(data.zone_prod_conden_ratio32);
                        $('#cummulative_pro_gas32').val(data.zone_prod_cumm_gas32);
                        $('#cummulative_pro_oil32').val(data.zone_prod_cumm_oil32);
                        $('#ab_openflow132').val(data.zone_prod_aof_bbl32);
                        $('#ab_openflow232').val(data.zone_prod_aof_scf32);
                        $('#criticalrate132').val(data.zone_prod_critical_bbl32);
                        $('#criticalrate232').val(data.zone_prod_critical_scf32);
                        $('#pro_index132').val(data.zone_prod_index_bbl32);
                        $('#pro_index232').val(data.zone_prod_index_scf32);
                        $('#diffusity_fact32').val(data.zone_prod_diffusity32);
                        $('#permeability32').val(data.zone_prod_permeability32);
                        $('#iafit_32').val(data.zone_prod_infinite32);
                        $('#wellradius32').val(data.zone_prod_well_radius32);
                        $('#pfit_32').val(data.zone_prod_pseudostate32);
                        $('#res_bondradius32').val(data.zone_prod_res_radius32);
                        $('#deltaP32').val(data.zone_prod_delta32);
                        $('#wellbore_skin32').val(data.zone_prod_wellbore32);
                        $('#rock_compress32').val(data.zone_prod_compres_rock32);
                        $('#fluid_compress32').val(data.zone_prod_compres_fluid32);
                        $('#total_compress32').val(data.zone_prod_compres_total32);
                        $('#secd_porosity32').val(data.zone_prod_second32);
                        $('#I_32').val(data.zone_prod_lambda32);
                        $('#W_32').val(data.zone_prod_omega32);
                        $('#oilshow_read132').val(data.zone_hc_oil_show32);
                        $('#oilshow_read232').val(data.zone_hc_oil_show_tools32);
                        $('#gasshow_read132').val(data.zone_hc_gas_show32);
                        $('#gasshow_read232').val(data.zone_hc_gas_show_tools32);
                        $('#wellmak_watercut32').val(data.zone_hc_water_cut32);
                        $('#water_bearing132').val(data.zone_hc_water_gwc32);
                        $('#water_bearing232').val(data.zone_hc_water_gwd_tools32);
                        $('#water_bearing332').val(data.zone_hc_water_owc32);
                        $('#water_bearing432').val(data.zone_hc_water_owc_tools32);
                        $('#s2id_rock_sampling32').select2('val', data.zone_rock_method32);
                        $('#s2id_petro_analys32').select2('val', data.zone_rock_petro32);
                        $('#sample32').val(data.zone_rock_petro_sample32);
                        $('#ticsd_32').val(data.zone_rock_top_depth32);
                        $('#bicsd_32').val(data.zone_rock_bot_depth32);
                        $('#num_totalcare32').val(data.zone_rock_barrel32);
                        $('#corebarrel32').val(data.zone_rock_barrel_equal32);
                        $('#totalrec32').val(data.zone_rock_total_core32);
                        $('#precore32').val(data.zone_rock_preservative32);
                        $('#rca_132').val(data.zone_rock_routine32);
                        $('#rca_32').val(data.zone_rock_routine_sample32);
                        $('#scal_132').val(data.zone_rock_scal32);
                        $('#scal_232').val(data.zone_rock_scal_sample32);
                        $('#zone_clastic_p10_thickness32').val(data.zone_clastic_p10_thickness32);
                        $('#zone_clastic_p50_thickness32').val(data.zone_clastic_p50_thickness32);
                        $('#zone_clastic_p90_thickness32').val(data.zone_clastic_p90_thickness32);
                        $('#zone_clastic_p10_gr32').val(data.zone_clastic_p10_gr32);
                        $('#zone_clastic_p50_gr32').val(data.zone_clastic_p50_gr32);
                        $('#zone_clastic_p90_gr32').val(data.zone_clastic_p90_gr32);
                        $('#zone_clastic_p10_sp32').val(data.zone_clastic_p10_sp32);
                        $('#zone_clastic_p50_sp32').val(data.zone_clastic_p50_sp32);
                        $('#zone_clastic_p90_sp32').val(data.zone_clastic_p90_sp32);
                        $('#zone_clastic_p10_net32').val(data.zone_clastic_p10_net32);
                        $('#zone_clastic_p50_net32').val(data.zone_clastic_p50_net32);
                        $('#zone_clastic_p90_net32').val(data.zone_clastic_p90_net32);
                        $('#zone_clastic_p10_por32').val(data.zone_clastic_p10_por32);
                        $('#zone_clastic_p50_por32').val(data.zone_clastic_p50_por32);
                        $('#zone_clastic_p90_por32').val(data.zone_clastic_p90_por32);
                        $('#zone_clastic_p10_satur32').val(data.zone_clastic_p10_satur32);
                        $('#zone_clastic_p50_satur32').val(data.zone_clastic_p50_satur32);
                        $('#zone_clastic_p90_satur32').val(data.zone_clastic_p90_satur32);
                        $('#zone_carbo_p10_thickness32').val(data.zone_carbo_p10_thickness32);
                        $('#zone_carbo_p50_thickness32').val(data.zone_carbo_p50_thickness32);
                        $('#zone_carbo_p90_thickness32').val(data.zone_carbo_p90_thickness32);
                        $('#zone_carbo_p10_dtc32').val(data.zone_carbo_p10_dtc32);
                        $('#zone_carbo_p50_dtc32').val(data.zone_carbo_p50_dtc32);
                        $('#zone_carbo_p90_dtc32').val(data.zone_carbo_p90_dtc32);
                        $('#zone_carbo_p10_total32').val(data.zone_carbo_p10_total32);
                        $('#zone_carbo_p50_total32').val(data.zone_carbo_p50_total32);
                        $('#zone_carbo_p90_total32').val(data.zone_carbo_p90_total32);
                        $('#zone_carbo_p10_net32').val(data.zone_carbo_p10_net32);
                        $('#zone_carbo_p50_net32').val(data.zone_carbo_p50_net32);
                        $('#zone_carbo_p90_net32').val(data.zone_carbo_p90_net32);
                        $('#zone_carbo_p10_por32').val(data.zone_carbo_p10_por32);
                        $('#zone_carbo_p50_por32').val(data.zone_carbo_p50_por32);
                        $('#zone_carbo_p90_por32').val(data.zone_carbo_p90_por32);
                        $('#zone_carbo_p10_satur32').val(data.zone_carbo_p10_satur32);
                        $('#zone_carbo_p50_satur32').val(data.zone_carbo_p50_satur32);
                        $('#zone_carbo_p90_satur32').val(data.zone_carbo_p90_satur32);
                        $('#z2_32').val(data.zone_fluid_date32);
                        $('#sampleat32').val(data.zone_fluid_sample32);
                        $('#gasoilratio_32').val(data.zone_fluid_ratio32);
                        $('#separator_pressure32').val(data.zone_fluid_pressure32);
                        $('#separator_temperature32').val(data.zone_fluid_temp32);
                        $('#tubing_pressure32').val(data.zone_fluid_tubing32);
                        $('#casing_pressure32').val(data.zone_fluid_casing32);
                        $('#sampleby32').val(data.zone_fluid_by32);
                        $('#repost_avail32').val(data.zone_fluid_report32);
                        $('#hydro_finger32').val(data.zone_fluid_finger32);
                        $('#oilgrav_6032').val(data.zone_fluid_grv_oil32);
                        $('#gasgrav_6032').val(data.zone_fluid_grv_gas32);
                        $('#condensategrav_6032').val(data.zone_fluid_grv_conden32);
                        $('#pvt_32').val(data.zone_fluid_pvt32);
                        $('#sample_32').val(data.zone_fluid_quantity32);
                        $('#gasdev_fac32').val(data.zone_fluid_z32);
                        $('#zone_fvf_oil_p1032').val(data.zone_fvf_oil_p1032);
                        $('#zone_fvf_oil_p5032').val(data.zone_fvf_oil_p5032);
                        $('#zone_fvf_oil_p9032').val(data.zone_fvf_oil_p9032);
                        $('#zone_fvf_gas_p1032').val(data.zone_fvf_gas_p1032);
                        $('#zone_fvf_gas_p5032').val(data.zone_fvf_gas_p5032);
                        $('#zone_fvf_gas_p9032').val(data.zone_fvf_gas_p9032);
                        $('#oilviscocity32').val(data.zone_viscocity32);
                            		
                        $('#zonename33').val(data.zone_name33);
                        $('#s2id_welltest33').select2('val', data.zone_test33);
                        $('#z_33').val(data.zone_test_date33);
                        $('#zonethickness33').val(data.zone_thickness33);
                        $('#zoneinterval33').val(data.zone_depth33);
                        $('#perforation_interval33').val(data.zone_perforation33);
                        $('#s2id_welltest_type33').select2('val', data.zone_well_test33);
                        $('#welltest_total33').val(data.zone_well_duration33);
                        $('#initialflow33').val(data.zone_initial_flow33);
                        $('#initialshutin33').val(data.zone_initial_shutin33);
                        $('#tubingsize33').val(data.zone_tubing_size33);
                        $('#initialtemperature33').val(data.zone_initial_temp33);
                        $('#zone_initial_pressure33').val(data.zone_initial_pressure33);
                        $('#res_presure33').val(data.zone_pseudostate33);
                        $('#pressurewell_for33').val(data.zone_well_formation33);
                        $('#pressurewell_head33').val(data.zone_head33);
                        $('#res_pressure33').val(data.zone_pressure_wellbore33);
                        $('#avg_porpsity33').val(data.zone_average_por33);
                        $('#watercut33').val(data.zone_water_cut33);
                        $('#initialwater_sat33').val(data.zone_initial_water33);
                        $('#lowtest_gas33').val(data.zone_low_gas33);
                        $('#lowtest_oil33').val(data.zone_low_oil33);
                        $('#freewater33').val(data.zone_free_water33);
                        $('#gas_grav33').val(data.zone_grv_gas33);
                        $('#oil_grav33').val(data.zone_grv_grv_oil33);
                        $('#zone_wellbore_coefficient33').val(data.zone_wellbore_coefficient33);
                        $('#wellbore_time33').val(data.zone_wellbore_time33);
                        $('#rsbt_33').val(data.zone_res_shape33);
                        $('#oilchoke33').val(data.zone_prod_oil_choke33);
                        $('#oilflow33').val(data.zone_prod_oil_flow33);
                        $('#gaschoke33').val(data.zone_prod_gas_choke33);
                        $('#gasflow33').val(data.zone_prod_gas_flow33);
                        $('#gasoilratio33').val(data.zone_prod_gasoil_ratio33);
                        $('#zone_prod_conden_ratio33').val(data.zone_prod_conden_ratio33);
                        $('#cummulative_pro_gas33').val(data.zone_prod_cumm_gas33);
                        $('#cummulative_pro_oil33').val(data.zone_prod_cumm_oil33);
                        $('#ab_openflow133').val(data.zone_prod_aof_bbl33);
                        $('#ab_openflow233').val(data.zone_prod_aof_scf33);
                        $('#criticalrate133').val(data.zone_prod_critical_bbl33);
                        $('#criticalrate233').val(data.zone_prod_critical_scf33);
                        $('#pro_index133').val(data.zone_prod_index_bbl33);
                        $('#pro_index233').val(data.zone_prod_index_scf33);
                        $('#diffusity_fact33').val(data.zone_prod_diffusity33);
                        $('#permeability33').val(data.zone_prod_permeability33);
                        $('#iafit_33').val(data.zone_prod_infinite33);
                        $('#wellradius33').val(data.zone_prod_well_radius33);
                        $('#pfit_33').val(data.zone_prod_pseudostate33);
                        $('#res_bondradius33').val(data.zone_prod_res_radius33);
                        $('#deltaP33').val(data.zone_prod_delta33);
                        $('#wellbore_skin33').val(data.zone_prod_wellbore33);
                        $('#rock_compress33').val(data.zone_prod_compres_rock33);
                        $('#fluid_compress33').val(data.zone_prod_compres_fluid33);
                        $('#total_compress33').val(data.zone_prod_compres_total33);
                        $('#secd_porosity33').val(data.zone_prod_second33);
                        $('#I_33').val(data.zone_prod_lambda33);
                        $('#W_33').val(data.zone_prod_omega33);
                        $('#oilshow_read133').val(data.zone_hc_oil_show33);
                        $('#oilshow_read233').val(data.zone_hc_oil_show_tools33);
                        $('#gasshow_read133').val(data.zone_hc_gas_show33);
                        $('#gasshow_read233').val(data.zone_hc_gas_show_tools33);
                        $('#wellmak_watercut33').val(data.zone_hc_water_cut33);
                        $('#water_bearing133').val(data.zone_hc_water_gwc33);
                        $('#water_bearing233').val(data.zone_hc_water_gwd_tools33);
                        $('#water_bearing333').val(data.zone_hc_water_owc33);
                        $('#water_bearing433').val(data.zone_hc_water_owc_tools33);
                        $('#s2id_rock_sampling33').select2('val', data.zone_rock_method33);
                        $('#s2id_petro_analys33').select2('val', data.zone_rock_petro33);
                        $('#sample33').val(data.zone_rock_petro_sample33);
                        $('#ticsd_33').val(data.zone_rock_top_depth33);
                        $('#bicsd_33').val(data.zone_rock_bot_depth33);
                        $('#num_totalcare33').val(data.zone_rock_barrel33);
                        $('#corebarrel33').val(data.zone_rock_barrel_equal33);
                        $('#totalrec33').val(data.zone_rock_total_core33);
                        $('#precore33').val(data.zone_rock_preservative33);
                        $('#rca_133').val(data.zone_rock_routine33);
                        $('#rca_33').val(data.zone_rock_routine_sample33);
                        $('#scal_133').val(data.zone_rock_scal33);
                        $('#scal_233').val(data.zone_rock_scal_sample33);
                        $('#zone_clastic_p10_thickness33').val(data.zone_clastic_p10_thickness33);
                        $('#zone_clastic_p50_thickness33').val(data.zone_clastic_p50_thickness33);
                        $('#zone_clastic_p90_thickness33').val(data.zone_clastic_p90_thickness33);
                        $('#zone_clastic_p10_gr33').val(data.zone_clastic_p10_gr33);
                        $('#zone_clastic_p50_gr33').val(data.zone_clastic_p50_gr33);
                        $('#zone_clastic_p90_gr33').val(data.zone_clastic_p90_gr33);
                        $('#zone_clastic_p10_sp33').val(data.zone_clastic_p10_sp33);
                        $('#zone_clastic_p50_sp33').val(data.zone_clastic_p50_sp33);
                        $('#zone_clastic_p90_sp33').val(data.zone_clastic_p90_sp33);
                        $('#zone_clastic_p10_net33').val(data.zone_clastic_p10_net33);
                        $('#zone_clastic_p50_net33').val(data.zone_clastic_p50_net33);
                        $('#zone_clastic_p90_net33').val(data.zone_clastic_p90_net33);
                        $('#zone_clastic_p10_por33').val(data.zone_clastic_p10_por33);
                        $('#zone_clastic_p50_por33').val(data.zone_clastic_p50_por33);
                        $('#zone_clastic_p90_por33').val(data.zone_clastic_p90_por33);
                        $('#zone_clastic_p10_satur33').val(data.zone_clastic_p10_satur33);
                        $('#zone_clastic_p50_satur33').val(data.zone_clastic_p50_satur33);
                        $('#zone_clastic_p90_satur33').val(data.zone_clastic_p90_satur33);
                        $('#zone_carbo_p10_thickness33').val(data.zone_carbo_p10_thickness33);
                        $('#zone_carbo_p50_thickness33').val(data.zone_carbo_p50_thickness33);
                        $('#zone_carbo_p90_thickness33').val(data.zone_carbo_p90_thickness33);
                        $('#zone_carbo_p10_dtc33').val(data.zone_carbo_p10_dtc33);
                        $('#zone_carbo_p50_dtc33').val(data.zone_carbo_p50_dtc33);
                        $('#zone_carbo_p90_dtc33').val(data.zone_carbo_p90_dtc33);
                        $('#zone_carbo_p10_total33').val(data.zone_carbo_p10_total33);
                        $('#zone_carbo_p50_total33').val(data.zone_carbo_p50_total33);
                        $('#zone_carbo_p90_total33').val(data.zone_carbo_p90_total33);
                        $('#zone_carbo_p10_net33').val(data.zone_carbo_p10_net33);
                        $('#zone_carbo_p50_net33').val(data.zone_carbo_p50_net33);
                        $('#zone_carbo_p90_net33').val(data.zone_carbo_p90_net33);
                        $('#zone_carbo_p10_por33').val(data.zone_carbo_p10_por33);
                        $('#zone_carbo_p50_por33').val(data.zone_carbo_p50_por33);
                        $('#zone_carbo_p90_por33').val(data.zone_carbo_p90_por33);
                        $('#zone_carbo_p10_satur33').val(data.zone_carbo_p10_satur33);
                        $('#zone_carbo_p50_satur33').val(data.zone_carbo_p50_satur33);
                        $('#zone_carbo_p90_satur33').val(data.zone_carbo_p90_satur33);
                        $('#z2_33').val(data.zone_fluid_date33);
                        $('#sampleat33').val(data.zone_fluid_sample33);
                        $('#gasoilratio_33').val(data.zone_fluid_ratio33);
                        $('#separator_pressure33').val(data.zone_fluid_pressure33);
                        $('#separator_temperature33').val(data.zone_fluid_temp33);
                        $('#tubing_pressure33').val(data.zone_fluid_tubing33);
                        $('#casing_pressure33').val(data.zone_fluid_casing33);
                        $('#sampleby33').val(data.zone_fluid_by33);
                        $('#repost_avail33').val(data.zone_fluid_report33);
                        $('#hydro_finger33').val(data.zone_fluid_finger33);
                        $('#oilgrav_6033').val(data.zone_fluid_grv_oil33);
                        $('#gasgrav_6033').val(data.zone_fluid_grv_gas33);
                        $('#condensategrav_6033').val(data.zone_fluid_grv_conden33);
                        $('#pvt_33').val(data.zone_fluid_pvt33);
                        $('#sample_33').val(data.zone_fluid_quantity33);
                        $('#gasdev_fac33').val(data.zone_fluid_z33);
                        $('#zone_fvf_oil_p1033').val(data.zone_fvf_oil_p1033);
                        $('#zone_fvf_oil_p5033').val(data.zone_fvf_oil_p5033);
                        $('#zone_fvf_oil_p9033').val(data.zone_fvf_oil_p9033);
                        $('#zone_fvf_gas_p1033').val(data.zone_fvf_gas_p1033);
                        $('#zone_fvf_gas_p5033').val(data.zone_fvf_gas_p5033);
                        $('#zone_fvf_gas_p9033').val(data.zone_fvf_gas_p9033);
                        $('#oilviscocity33').val(data.zone_viscocity33);
                            		
                        $('#zonename34').val(data.zone_name34);
                        $('#s2id_welltest34').select2('val', data.zone_test34);
                        $('#z_34').val(data.zone_test_date34);
                        $('#zonethickness34').val(data.zone_thickness34);
                        $('#zoneinterval34').val(data.zone_depth34);
                        $('#perforation_interval34').val(data.zone_perforation34);
                        $('#s2id_welltest_type34').select2('val', data.zone_well_test34);
                        $('#welltest_total34').val(data.zone_well_duration34);
                        $('#initialflow34').val(data.zone_initial_flow34);
                        $('#initialshutin34').val(data.zone_initial_shutin34);
                        $('#tubingsize34').val(data.zone_tubing_size34);
                        $('#initialtemperature34').val(data.zone_initial_temp34);
                        $('#zone_initial_pressure34').val(data.zone_initial_pressure34);
                        $('#res_presure34').val(data.zone_pseudostate34);
                        $('#pressurewell_for34').val(data.zone_well_formation34);
                        $('#pressurewell_head34').val(data.zone_head34);
                        $('#res_pressure34').val(data.zone_pressure_wellbore34);
                        $('#avg_porpsity34').val(data.zone_average_por34);
                        $('#watercut34').val(data.zone_water_cut34);
                        $('#initialwater_sat34').val(data.zone_initial_water34);
                        $('#lowtest_gas34').val(data.zone_low_gas34);
                        $('#lowtest_oil34').val(data.zone_low_oil34);
                        $('#freewater34').val(data.zone_free_water34);
                        $('#gas_grav34').val(data.zone_grv_gas34);
                        $('#oil_grav34').val(data.zone_grv_grv_oil34);
                        $('#zone_wellbore_coefficient34').val(data.zone_wellbore_coefficient34);
                        $('#wellbore_time34').val(data.zone_wellbore_time34);
                        $('#rsbt_34').val(data.zone_res_shape34);
                        $('#oilchoke34').val(data.zone_prod_oil_choke34);
                        $('#oilflow34').val(data.zone_prod_oil_flow34);
                        $('#gaschoke34').val(data.zone_prod_gas_choke34);
                        $('#gasflow34').val(data.zone_prod_gas_flow34);
                        $('#gasoilratio34').val(data.zone_prod_gasoil_ratio34);
                        $('#zone_prod_conden_ratio34').val(data.zone_prod_conden_ratio34);
                        $('#cummulative_pro_gas34').val(data.zone_prod_cumm_gas34);
                        $('#cummulative_pro_oil34').val(data.zone_prod_cumm_oil34);
                        $('#ab_openflow134').val(data.zone_prod_aof_bbl34);
                        $('#ab_openflow234').val(data.zone_prod_aof_scf34);
                        $('#criticalrate134').val(data.zone_prod_critical_bbl34);
                        $('#criticalrate234').val(data.zone_prod_critical_scf34);
                        $('#pro_index134').val(data.zone_prod_index_bbl34);
                        $('#pro_index234').val(data.zone_prod_index_scf34);
                        $('#diffusity_fact34').val(data.zone_prod_diffusity34);
                        $('#permeability34').val(data.zone_prod_permeability34);
                        $('#iafit_34').val(data.zone_prod_infinite34);
                        $('#wellradius34').val(data.zone_prod_well_radius34);
                        $('#pfit_34').val(data.zone_prod_pseudostate34);
                        $('#res_bondradius34').val(data.zone_prod_res_radius34);
                        $('#deltaP34').val(data.zone_prod_delta34);
                        $('#wellbore_skin34').val(data.zone_prod_wellbore34);
                        $('#rock_compress34').val(data.zone_prod_compres_rock34);
                        $('#fluid_compress34').val(data.zone_prod_compres_fluid34);
                        $('#total_compress34').val(data.zone_prod_compres_total34);
                        $('#secd_porosity34').val(data.zone_prod_second34);
                        $('#I_34').val(data.zone_prod_lambda34);
                        $('#W_34').val(data.zone_prod_omega34);
                        $('#oilshow_read134').val(data.zone_hc_oil_show34);
                        $('#oilshow_read234').val(data.zone_hc_oil_show_tools34);
                        $('#gasshow_read134').val(data.zone_hc_gas_show34);
                        $('#gasshow_read234').val(data.zone_hc_gas_show_tools34);
                        $('#wellmak_watercut34').val(data.zone_hc_water_cut34);
                        $('#water_bearing134').val(data.zone_hc_water_gwc34);
                        $('#water_bearing234').val(data.zone_hc_water_gwd_tools34);
                        $('#water_bearing334').val(data.zone_hc_water_owc34);
                        $('#water_bearing434').val(data.zone_hc_water_owc_tools34);
                        $('#s2id_rock_sampling34').select2('val', data.zone_rock_method34);
                        $('#s2id_petro_analys34').select2('val', data.zone_rock_petro34);
                        $('#sample34').val(data.zone_rock_petro_sample34);
                        $('#ticsd_34').val(data.zone_rock_top_depth34);
                        $('#bicsd_34').val(data.zone_rock_bot_depth34);
                        $('#num_totalcare34').val(data.zone_rock_barrel34);
                        $('#corebarrel34').val(data.zone_rock_barrel_equal34);
                        $('#totalrec34').val(data.zone_rock_total_core34);
                        $('#precore34').val(data.zone_rock_preservative34);
                        $('#rca_134').val(data.zone_rock_routine34);
                        $('#rca_34').val(data.zone_rock_routine_sample34);
                        $('#scal_134').val(data.zone_rock_scal34);
                        $('#scal_234').val(data.zone_rock_scal_sample34);
                        $('#zone_clastic_p10_thickness34').val(data.zone_clastic_p10_thickness34);
                        $('#zone_clastic_p50_thickness34').val(data.zone_clastic_p50_thickness34);
                        $('#zone_clastic_p90_thickness34').val(data.zone_clastic_p90_thickness34);
                        $('#zone_clastic_p10_gr34').val(data.zone_clastic_p10_gr34);
                        $('#zone_clastic_p50_gr34').val(data.zone_clastic_p50_gr34);
                        $('#zone_clastic_p90_gr34').val(data.zone_clastic_p90_gr34);
                        $('#zone_clastic_p10_sp34').val(data.zone_clastic_p10_sp34);
                        $('#zone_clastic_p50_sp34').val(data.zone_clastic_p50_sp34);
                        $('#zone_clastic_p90_sp34').val(data.zone_clastic_p90_sp34);
                        $('#zone_clastic_p10_net34').val(data.zone_clastic_p10_net34);
                        $('#zone_clastic_p50_net34').val(data.zone_clastic_p50_net34);
                        $('#zone_clastic_p90_net34').val(data.zone_clastic_p90_net34);
                        $('#zone_clastic_p10_por34').val(data.zone_clastic_p10_por34);
                        $('#zone_clastic_p50_por34').val(data.zone_clastic_p50_por34);
                        $('#zone_clastic_p90_por34').val(data.zone_clastic_p90_por34);
                        $('#zone_clastic_p10_satur34').val(data.zone_clastic_p10_satur34);
                        $('#zone_clastic_p50_satur34').val(data.zone_clastic_p50_satur34);
                        $('#zone_clastic_p90_satur34').val(data.zone_clastic_p90_satur34);
                        $('#zone_carbo_p10_thickness34').val(data.zone_carbo_p10_thickness34);
                        $('#zone_carbo_p50_thickness34').val(data.zone_carbo_p50_thickness34);
                        $('#zone_carbo_p90_thickness34').val(data.zone_carbo_p90_thickness34);
                        $('#zone_carbo_p10_dtc34').val(data.zone_carbo_p10_dtc34);
                        $('#zone_carbo_p50_dtc34').val(data.zone_carbo_p50_dtc34);
                        $('#zone_carbo_p90_dtc34').val(data.zone_carbo_p90_dtc34);
                        $('#zone_carbo_p10_total34').val(data.zone_carbo_p10_total34);
                        $('#zone_carbo_p50_total34').val(data.zone_carbo_p50_total34);
                        $('#zone_carbo_p90_total34').val(data.zone_carbo_p90_total34);
                        $('#zone_carbo_p10_net34').val(data.zone_carbo_p10_net34);
                        $('#zone_carbo_p50_net34').val(data.zone_carbo_p50_net34);
                        $('#zone_carbo_p90_net34').val(data.zone_carbo_p90_net34);
                        $('#zone_carbo_p10_por34').val(data.zone_carbo_p10_por34);
                        $('#zone_carbo_p50_por34').val(data.zone_carbo_p50_por34);
                        $('#zone_carbo_p90_por34').val(data.zone_carbo_p90_por34);
                        $('#zone_carbo_p10_satur34').val(data.zone_carbo_p10_satur34);
                        $('#zone_carbo_p50_satur34').val(data.zone_carbo_p50_satur34);
                        $('#zone_carbo_p90_satur34').val(data.zone_carbo_p90_satur34);
                        $('#z2_34').val(data.zone_fluid_date34);
                        $('#sampleat34').val(data.zone_fluid_sample34);
                        $('#gasoilratio_34').val(data.zone_fluid_ratio34);
                        $('#separator_pressure34').val(data.zone_fluid_pressure34);
                        $('#separator_temperature34').val(data.zone_fluid_temp34);
                        $('#tubing_pressure34').val(data.zone_fluid_tubing34);
                        $('#casing_pressure34').val(data.zone_fluid_casing34);
                        $('#sampleby34').val(data.zone_fluid_by34);
                        $('#repost_avail34').val(data.zone_fluid_report34);
                        $('#hydro_finger34').val(data.zone_fluid_finger34);
                        $('#oilgrav_6034').val(data.zone_fluid_grv_oil34);
                        $('#gasgrav_6034').val(data.zone_fluid_grv_gas34);
                        $('#condensategrav_6034').val(data.zone_fluid_grv_conden34);
                        $('#pvt_34').val(data.zone_fluid_pvt34);
                        $('#sample_34').val(data.zone_fluid_quantity34);
                        $('#gasdev_fac34').val(data.zone_fluid_z34);
                        $('#zone_fvf_oil_p1034').val(data.zone_fvf_oil_p1034);
                        $('#zone_fvf_oil_p5034').val(data.zone_fvf_oil_p5034);
                        $('#zone_fvf_oil_p9034').val(data.zone_fvf_oil_p9034);
                        $('#zone_fvf_gas_p1034').val(data.zone_fvf_gas_p1034);
                        $('#zone_fvf_gas_p5034').val(data.zone_fvf_gas_p5034);
                        $('#zone_fvf_gas_p9034').val(data.zone_fvf_gas_p9034);
                        $('#oilviscocity34').val(data.zone_viscocity34);
                            		
                        $('#zonename35').val(data.zone_name35);
                        $('#s2id_welltest35').select2('val', data.zone_test35);
                        $('#z_35').val(data.zone_test_date35);
                        $('#zonethickness35').val(data.zone_thickness35);
                        $('#zoneinterval35').val(data.zone_depth35);
                        $('#perforation_interval35').val(data.zone_perforation35);
                        $('#s2id_welltest_type35').select2('val', data.zone_well_test35);
                        $('#welltest_total35').val(data.zone_well_duration35);
                        $('#initialflow35').val(data.zone_initial_flow35);
                        $('#initialshutin35').val(data.zone_initial_shutin35);
                        $('#tubingsize35').val(data.zone_tubing_size35);
                        $('#initialtemperature35').val(data.zone_initial_temp35);
                        $('#zone_initial_pressure35').val(data.zone_initial_pressure35);
                        $('#res_presure35').val(data.zone_pseudostate35);
                        $('#pressurewell_for35').val(data.zone_well_formation35);
                        $('#pressurewell_head35').val(data.zone_head35);
                        $('#res_pressure35').val(data.zone_pressure_wellbore35);
                        $('#avg_porpsity35').val(data.zone_average_por35);
                        $('#watercut35').val(data.zone_water_cut35);
                        $('#initialwater_sat35').val(data.zone_initial_water35);
                        $('#lowtest_gas35').val(data.zone_low_gas35);
                        $('#lowtest_oil35').val(data.zone_low_oil35);
                        $('#freewater35').val(data.zone_free_water35);
                        $('#gas_grav35').val(data.zone_grv_gas35);
                        $('#oil_grav35').val(data.zone_grv_grv_oil35);
                        $('#zone_wellbore_coefficient35').val(data.zone_wellbore_coefficient35);
                        $('#wellbore_time35').val(data.zone_wellbore_time35);
                        $('#rsbt_35').val(data.zone_res_shape35);
                        $('#oilchoke35').val(data.zone_prod_oil_choke35);
                        $('#oilflow35').val(data.zone_prod_oil_flow35);
                        $('#gaschoke35').val(data.zone_prod_gas_choke35);
                        $('#gasflow35').val(data.zone_prod_gas_flow35);
                        $('#gasoilratio35').val(data.zone_prod_gasoil_ratio35);
                        $('#zone_prod_conden_ratio35').val(data.zone_prod_conden_ratio35);
                        $('#cummulative_pro_gas35').val(data.zone_prod_cumm_gas35);
                        $('#cummulative_pro_oil35').val(data.zone_prod_cumm_oil35);
                        $('#ab_openflow135').val(data.zone_prod_aof_bbl35);
                        $('#ab_openflow235').val(data.zone_prod_aof_scf35);
                        $('#criticalrate135').val(data.zone_prod_critical_bbl35);
                        $('#criticalrate235').val(data.zone_prod_critical_scf35);
                        $('#pro_index135').val(data.zone_prod_index_bbl35);
                        $('#pro_index235').val(data.zone_prod_index_scf35);
                        $('#diffusity_fact35').val(data.zone_prod_diffusity35);
                        $('#permeability35').val(data.zone_prod_permeability35);
                        $('#iafit_35').val(data.zone_prod_infinite35);
                        $('#wellradius35').val(data.zone_prod_well_radius35);
                        $('#pfit_35').val(data.zone_prod_pseudostate35);
                        $('#res_bondradius35').val(data.zone_prod_res_radius35);
                        $('#deltaP35').val(data.zone_prod_delta35);
                        $('#wellbore_skin35').val(data.zone_prod_wellbore35);
                        $('#rock_compress35').val(data.zone_prod_compres_rock35);
                        $('#fluid_compress35').val(data.zone_prod_compres_fluid35);
                        $('#total_compress35').val(data.zone_prod_compres_total35);
                        $('#secd_porosity35').val(data.zone_prod_second35);
                        $('#I_35').val(data.zone_prod_lambda35);
                        $('#W_35').val(data.zone_prod_omega35);
                        $('#oilshow_read135').val(data.zone_hc_oil_show35);
                        $('#oilshow_read235').val(data.zone_hc_oil_show_tools35);
                        $('#gasshow_read135').val(data.zone_hc_gas_show35);
                        $('#gasshow_read235').val(data.zone_hc_gas_show_tools35);
                        $('#wellmak_watercut35').val(data.zone_hc_water_cut35);
                        $('#water_bearing135').val(data.zone_hc_water_gwc35);
                        $('#water_bearing235').val(data.zone_hc_water_gwd_tools35);
                        $('#water_bearing335').val(data.zone_hc_water_owc35);
                        $('#water_bearing435').val(data.zone_hc_water_owc_tools35);
                        $('#s2id_rock_sampling35').select2('val', data.zone_rock_method35);
                        $('#s2id_petro_analys35').select2('val', data.zone_rock_petro35);
                        $('#sample35').val(data.zone_rock_petro_sample35);
                        $('#ticsd_35').val(data.zone_rock_top_depth35);
                        $('#bicsd_35').val(data.zone_rock_bot_depth35);
                        $('#num_totalcare35').val(data.zone_rock_barrel35);
                        $('#corebarrel35').val(data.zone_rock_barrel_equal35);
                        $('#totalrec35').val(data.zone_rock_total_core35);
                        $('#precore35').val(data.zone_rock_preservative35);
                        $('#rca_135').val(data.zone_rock_routine35);
                        $('#rca_35').val(data.zone_rock_routine_sample35);
                        $('#scal_135').val(data.zone_rock_scal35);
                        $('#scal_235').val(data.zone_rock_scal_sample35);
                        $('#zone_clastic_p10_thickness35').val(data.zone_clastic_p10_thickness35);
                        $('#zone_clastic_p50_thickness35').val(data.zone_clastic_p50_thickness35);
                        $('#zone_clastic_p90_thickness35').val(data.zone_clastic_p90_thickness35);
                        $('#zone_clastic_p10_gr35').val(data.zone_clastic_p10_gr35);
                        $('#zone_clastic_p50_gr35').val(data.zone_clastic_p50_gr35);
                        $('#zone_clastic_p90_gr35').val(data.zone_clastic_p90_gr35);
                        $('#zone_clastic_p10_sp35').val(data.zone_clastic_p10_sp35);
                        $('#zone_clastic_p50_sp35').val(data.zone_clastic_p50_sp35);
                        $('#zone_clastic_p90_sp35').val(data.zone_clastic_p90_sp35);
                        $('#zone_clastic_p10_net35').val(data.zone_clastic_p10_net35);
                        $('#zone_clastic_p50_net35').val(data.zone_clastic_p50_net35);
                        $('#zone_clastic_p90_net35').val(data.zone_clastic_p90_net35);
                        $('#zone_clastic_p10_por35').val(data.zone_clastic_p10_por35);
                        $('#zone_clastic_p50_por35').val(data.zone_clastic_p50_por35);
                        $('#zone_clastic_p90_por35').val(data.zone_clastic_p90_por35);
                        $('#zone_clastic_p10_satur35').val(data.zone_clastic_p10_satur35);
                        $('#zone_clastic_p50_satur35').val(data.zone_clastic_p50_satur35);
                        $('#zone_clastic_p90_satur35').val(data.zone_clastic_p90_satur35);
                        $('#zone_carbo_p10_thickness35').val(data.zone_carbo_p10_thickness35);
                        $('#zone_carbo_p50_thickness35').val(data.zone_carbo_p50_thickness35);
                        $('#zone_carbo_p90_thickness35').val(data.zone_carbo_p90_thickness35);
                        $('#zone_carbo_p10_dtc35').val(data.zone_carbo_p10_dtc35);
                        $('#zone_carbo_p50_dtc35').val(data.zone_carbo_p50_dtc35);
                        $('#zone_carbo_p90_dtc35').val(data.zone_carbo_p90_dtc35);
                        $('#zone_carbo_p10_total35').val(data.zone_carbo_p10_total35);
                        $('#zone_carbo_p50_total35').val(data.zone_carbo_p50_total35);
                        $('#zone_carbo_p90_total35').val(data.zone_carbo_p90_total35);
                        $('#zone_carbo_p10_net35').val(data.zone_carbo_p10_net35);
                        $('#zone_carbo_p50_net35').val(data.zone_carbo_p50_net35);
                        $('#zone_carbo_p90_net35').val(data.zone_carbo_p90_net35);
                        $('#zone_carbo_p10_por35').val(data.zone_carbo_p10_por35);
                        $('#zone_carbo_p50_por35').val(data.zone_carbo_p50_por35);
                        $('#zone_carbo_p90_por35').val(data.zone_carbo_p90_por35);
                        $('#zone_carbo_p10_satur35').val(data.zone_carbo_p10_satur35);
                        $('#zone_carbo_p50_satur35').val(data.zone_carbo_p50_satur35);
                        $('#zone_carbo_p90_satur35').val(data.zone_carbo_p90_satur35);
                        $('#z2_35').val(data.zone_fluid_date35);
                        $('#sampleat35').val(data.zone_fluid_sample35);
                        $('#gasoilratio_35').val(data.zone_fluid_ratio35);
                        $('#separator_pressure35').val(data.zone_fluid_pressure35);
                        $('#separator_temperature35').val(data.zone_fluid_temp35);
                        $('#tubing_pressure35').val(data.zone_fluid_tubing35);
                        $('#casing_pressure35').val(data.zone_fluid_casing35);
                        $('#sampleby35').val(data.zone_fluid_by35);
                        $('#repost_avail35').val(data.zone_fluid_report35);
                        $('#hydro_finger35').val(data.zone_fluid_finger35);
                        $('#oilgrav_6035').val(data.zone_fluid_grv_oil35);
                        $('#gasgrav_6035').val(data.zone_fluid_grv_gas35);
                        $('#condensategrav_6035').val(data.zone_fluid_grv_conden35);
                        $('#pvt_35').val(data.zone_fluid_pvt35);
                        $('#sample_35').val(data.zone_fluid_quantity35);
                        $('#gasdev_fac35').val(data.zone_fluid_z35);
                        $('#zone_fvf_oil_p1035').val(data.zone_fvf_oil_p1035);
                        $('#zone_fvf_oil_p5035').val(data.zone_fvf_oil_p5035);
                        $('#zone_fvf_oil_p9035').val(data.zone_fvf_oil_p9035);
                        $('#zone_fvf_gas_p1035').val(data.zone_fvf_gas_p1035);
                        $('#zone_fvf_gas_p5035').val(data.zone_fvf_gas_p5035);
                        $('#zone_fvf_gas_p9035').val(data.zone_fvf_gas_p9035);
                        $('#oilviscocity35').val(data.zone_viscocity35);
                            		
                        $('#zonename41').val(data.zone_name41);
                        $('#s2id_welltest41').select2('val', data.zone_test41);
                        $('#z_41').val(data.zone_test_date41);
                        $('#zonethickness41').val(data.zone_thickness41);
                        $('#zoneinterval41').val(data.zone_depth41);
                        $('#perforation_interval41').val(data.zone_perforation41);
                        $('#s2id_welltest_type41').select2('val', data.zone_well_test41);
                        $('#welltest_total41').val(data.zone_well_duration41);
                        $('#initialflow41').val(data.zone_initial_flow41);
                        $('#initialshutin41').val(data.zone_initial_shutin41);
                        $('#tubingsize41').val(data.zone_tubing_size41);
                        $('#initialtemperature41').val(data.zone_initial_temp41);
                        $('#zone_initial_pressure41').val(data.zone_initial_pressure41);
                        $('#res_presure41').val(data.zone_pseudostate41);
                        $('#pressurewell_for41').val(data.zone_well_formation41);
                        $('#pressurewell_head41').val(data.zone_head41);
                        $('#res_pressure41').val(data.zone_pressure_wellbore41);
                        $('#avg_porpsity41').val(data.zone_average_por41);
                        $('#watercut41').val(data.zone_water_cut41);
                        $('#initialwater_sat41').val(data.zone_initial_water41);
                        $('#lowtest_gas41').val(data.zone_low_gas41);
                        $('#lowtest_oil41').val(data.zone_low_oil41);
                        $('#freewater41').val(data.zone_free_water41);
                        $('#gas_grav41').val(data.zone_grv_gas41);
                        $('#oil_grav41').val(data.zone_grv_grv_oil41);
                        $('#zone_wellbore_coefficient41').val(data.zone_wellbore_coefficient41);
                        $('#wellbore_time41').val(data.zone_wellbore_time41);
                        $('#rsbt_41').val(data.zone_res_shape41);
                        $('#oilchoke41').val(data.zone_prod_oil_choke41);
                        $('#oilflow41').val(data.zone_prod_oil_flow41);
                        $('#gaschoke41').val(data.zone_prod_gas_choke41);
                        $('#gasflow41').val(data.zone_prod_gas_flow41);
                        $('#gasoilratio41').val(data.zone_prod_gasoil_ratio41);
                        $('#zone_prod_conden_ratio41').val(data.zone_prod_conden_ratio41);
                        $('#cummulative_pro_gas41').val(data.zone_prod_cumm_gas41);
                        $('#cummulative_pro_oil41').val(data.zone_prod_cumm_oil41);
                        $('#ab_openflow141').val(data.zone_prod_aof_bbl41);
                        $('#ab_openflow241').val(data.zone_prod_aof_scf41);
                        $('#criticalrate141').val(data.zone_prod_critical_bbl41);
                        $('#criticalrate241').val(data.zone_prod_critical_scf41);
                        $('#pro_index141').val(data.zone_prod_index_bbl41);
                        $('#pro_index241').val(data.zone_prod_index_scf41);
                        $('#diffusity_fact41').val(data.zone_prod_diffusity41);
                        $('#permeability41').val(data.zone_prod_permeability41);
                        $('#iafit_41').val(data.zone_prod_infinite41);
                        $('#wellradius41').val(data.zone_prod_well_radius41);
                        $('#pfit_41').val(data.zone_prod_pseudostate41);
                        $('#res_bondradius41').val(data.zone_prod_res_radius41);
                        $('#deltaP41').val(data.zone_prod_delta41);
                        $('#wellbore_skin41').val(data.zone_prod_wellbore41);
                        $('#rock_compress41').val(data.zone_prod_compres_rock41);
                        $('#fluid_compress41').val(data.zone_prod_compres_fluid41);
                        $('#total_compress41').val(data.zone_prod_compres_total41);
                        $('#secd_porosity41').val(data.zone_prod_second41);
                        $('#I_41').val(data.zone_prod_lambda41);
                        $('#W_41').val(data.zone_prod_omega41);
                        $('#oilshow_read141').val(data.zone_hc_oil_show41);
                        $('#oilshow_read241').val(data.zone_hc_oil_show_tools41);
                        $('#gasshow_read141').val(data.zone_hc_gas_show41);
                        $('#gasshow_read241').val(data.zone_hc_gas_show_tools41);
                        $('#wellmak_watercut41').val(data.zone_hc_water_cut41);
                        $('#water_bearing141').val(data.zone_hc_water_gwc41);
                        $('#water_bearing241').val(data.zone_hc_water_gwd_tools41);
                        $('#water_bearing341').val(data.zone_hc_water_owc41);
                        $('#water_bearing441').val(data.zone_hc_water_owc_tools41);
                        $('#s2id_rock_sampling41').select2('val', data.zone_rock_method41);
                        $('#s2id_petro_analys41').select2('val', data.zone_rock_petro41);
                        $('#sample41').val(data.zone_rock_petro_sample41);
                        $('#ticsd_41').val(data.zone_rock_top_depth41);
                        $('#bicsd_41').val(data.zone_rock_bot_depth41);
                        $('#num_totalcare41').val(data.zone_rock_barrel41);
                        $('#corebarrel41').val(data.zone_rock_barrel_equal41);
                        $('#totalrec41').val(data.zone_rock_total_core41);
                        $('#precore41').val(data.zone_rock_preservative41);
                        $('#rca_141').val(data.zone_rock_routine41);
                        $('#rca_41').val(data.zone_rock_routine_sample41);
                        $('#scal_141').val(data.zone_rock_scal41);
                        $('#scal_241').val(data.zone_rock_scal_sample41);
                        $('#zone_clastic_p10_thickness41').val(data.zone_clastic_p10_thickness41);
                        $('#zone_clastic_p50_thickness41').val(data.zone_clastic_p50_thickness41);
                        $('#zone_clastic_p90_thickness41').val(data.zone_clastic_p90_thickness41);
                        $('#zone_clastic_p10_gr41').val(data.zone_clastic_p10_gr41);
                        $('#zone_clastic_p50_gr41').val(data.zone_clastic_p50_gr41);
                        $('#zone_clastic_p90_gr41').val(data.zone_clastic_p90_gr41);
                        $('#zone_clastic_p10_sp41').val(data.zone_clastic_p10_sp41);
                        $('#zone_clastic_p50_sp41').val(data.zone_clastic_p50_sp41);
                        $('#zone_clastic_p90_sp41').val(data.zone_clastic_p90_sp41);
                        $('#zone_clastic_p10_net41').val(data.zone_clastic_p10_net41);
                        $('#zone_clastic_p50_net41').val(data.zone_clastic_p50_net41);
                        $('#zone_clastic_p90_net41').val(data.zone_clastic_p90_net41);
                        $('#zone_clastic_p10_por41').val(data.zone_clastic_p10_por41);
                        $('#zone_clastic_p50_por41').val(data.zone_clastic_p50_por41);
                        $('#zone_clastic_p90_por41').val(data.zone_clastic_p90_por41);
                        $('#zone_clastic_p10_satur41').val(data.zone_clastic_p10_satur41);
                        $('#zone_clastic_p50_satur41').val(data.zone_clastic_p50_satur41);
                        $('#zone_clastic_p90_satur41').val(data.zone_clastic_p90_satur41);
                        $('#zone_carbo_p10_thickness41').val(data.zone_carbo_p10_thickness41);
                        $('#zone_carbo_p50_thickness41').val(data.zone_carbo_p50_thickness41);
                        $('#zone_carbo_p90_thickness41').val(data.zone_carbo_p90_thickness41);
                        $('#zone_carbo_p10_dtc41').val(data.zone_carbo_p10_dtc41);
                        $('#zone_carbo_p50_dtc41').val(data.zone_carbo_p50_dtc41);
                        $('#zone_carbo_p90_dtc41').val(data.zone_carbo_p90_dtc41);
                        $('#zone_carbo_p10_total41').val(data.zone_carbo_p10_total41);
                        $('#zone_carbo_p50_total41').val(data.zone_carbo_p50_total41);
                        $('#zone_carbo_p90_total41').val(data.zone_carbo_p90_total41);
                        $('#zone_carbo_p10_net41').val(data.zone_carbo_p10_net41);
                        $('#zone_carbo_p50_net41').val(data.zone_carbo_p50_net41);
                        $('#zone_carbo_p90_net41').val(data.zone_carbo_p90_net41);
                        $('#zone_carbo_p10_por41').val(data.zone_carbo_p10_por41);
                        $('#zone_carbo_p50_por41').val(data.zone_carbo_p50_por41);
                        $('#zone_carbo_p90_por41').val(data.zone_carbo_p90_por41);
                        $('#zone_carbo_p10_satur41').val(data.zone_carbo_p10_satur41);
                        $('#zone_carbo_p50_satur41').val(data.zone_carbo_p50_satur41);
                        $('#zone_carbo_p90_satur41').val(data.zone_carbo_p90_satur41);
                        $('#z2_41').val(data.zone_fluid_date41);
                        $('#sampleat41').val(data.zone_fluid_sample41);
                        $('#gasoilratio_41').val(data.zone_fluid_ratio41);
                        $('#separator_pressure41').val(data.zone_fluid_pressure41);
                        $('#separator_temperature41').val(data.zone_fluid_temp41);
                        $('#tubing_pressure41').val(data.zone_fluid_tubing41);
                        $('#casing_pressure41').val(data.zone_fluid_casing41);
                        $('#sampleby41').val(data.zone_fluid_by41);
                        $('#repost_avail41').val(data.zone_fluid_report41);
                        $('#hydro_finger41').val(data.zone_fluid_finger41);
                        $('#oilgrav_6041').val(data.zone_fluid_grv_oil41);
                        $('#gasgrav_6041').val(data.zone_fluid_grv_gas41);
                        $('#condensategrav_6041').val(data.zone_fluid_grv_conden41);
                        $('#pvt_41').val(data.zone_fluid_pvt41);
                        $('#sample_41').val(data.zone_fluid_quantity41);
                        $('#gasdev_fac41').val(data.zone_fluid_z41);
                        $('#zone_fvf_oil_p1041').val(data.zone_fvf_oil_p1041);
                        $('#zone_fvf_oil_p5041').val(data.zone_fvf_oil_p5041);
                        $('#zone_fvf_oil_p9041').val(data.zone_fvf_oil_p9041);
                        $('#zone_fvf_gas_p1041').val(data.zone_fvf_gas_p1041);
                        $('#zone_fvf_gas_p5041').val(data.zone_fvf_gas_p5041);
                        $('#zone_fvf_gas_p9041').val(data.zone_fvf_gas_p9041);
                        $('#oilviscocity41').val(data.zone_viscocity41);
                            		
                        $('#zonename42').val(data.zone_name42);
                        $('#s2id_welltest42').select2('val', data.zone_test42);
                        $('#z_42').val(data.zone_test_date42);
                        $('#zonethickness42').val(data.zone_thickness42);
                        $('#zoneinterval42').val(data.zone_depth42);
                        $('#perforation_interval42').val(data.zone_perforation42);
                        $('#s2id_welltest_type42').select2('val', data.zone_well_test42);
                        $('#welltest_total42').val(data.zone_well_duration42);
                        $('#initialflow42').val(data.zone_initial_flow42);
                        $('#initialshutin42').val(data.zone_initial_shutin42);
                        $('#tubingsize42').val(data.zone_tubing_size42);
                        $('#initialtemperature42').val(data.zone_initial_temp42);
                        $('#zone_initial_pressure42').val(data.zone_initial_pressure42);
                        $('#res_presure42').val(data.zone_pseudostate42);
                        $('#pressurewell_for42').val(data.zone_well_formation42);
                        $('#pressurewell_head42').val(data.zone_head42);
                        $('#res_pressure42').val(data.zone_pressure_wellbore42);
                        $('#avg_porpsity42').val(data.zone_average_por42);
                        $('#watercut42').val(data.zone_water_cut42);
                        $('#initialwater_sat42').val(data.zone_initial_water42);
                        $('#lowtest_gas42').val(data.zone_low_gas42);
                        $('#lowtest_oil42').val(data.zone_low_oil42);
                        $('#freewater42').val(data.zone_free_water42);
                        $('#gas_grav42').val(data.zone_grv_gas42);
                        $('#oil_grav42').val(data.zone_grv_grv_oil42);
                        $('#zone_wellbore_coefficient42').val(data.zone_wellbore_coefficient42);
                        $('#wellbore_time42').val(data.zone_wellbore_time42);
                        $('#rsbt_42').val(data.zone_res_shape42);
                        $('#oilchoke42').val(data.zone_prod_oil_choke42);
                        $('#oilflow42').val(data.zone_prod_oil_flow42);
                        $('#gaschoke42').val(data.zone_prod_gas_choke42);
                        $('#gasflow42').val(data.zone_prod_gas_flow42);
                        $('#gasoilratio42').val(data.zone_prod_gasoil_ratio42);
                        $('#zone_prod_conden_ratio42').val(data.zone_prod_conden_ratio42);
                        $('#cummulative_pro_gas42').val(data.zone_prod_cumm_gas42);
                        $('#cummulative_pro_oil42').val(data.zone_prod_cumm_oil42);
                        $('#ab_openflow142').val(data.zone_prod_aof_bbl42);
                        $('#ab_openflow242').val(data.zone_prod_aof_scf42);
                        $('#criticalrate142').val(data.zone_prod_critical_bbl42);
                        $('#criticalrate242').val(data.zone_prod_critical_scf42);
                        $('#pro_index142').val(data.zone_prod_index_bbl42);
                        $('#pro_index242').val(data.zone_prod_index_scf42);
                        $('#diffusity_fact42').val(data.zone_prod_diffusity42);
                        $('#permeability42').val(data.zone_prod_permeability42);
                        $('#iafit_42').val(data.zone_prod_infinite42);
                        $('#wellradius42').val(data.zone_prod_well_radius42);
                        $('#pfit_42').val(data.zone_prod_pseudostate42);
                        $('#res_bondradius42').val(data.zone_prod_res_radius42);
                        $('#deltaP42').val(data.zone_prod_delta42);
                        $('#wellbore_skin42').val(data.zone_prod_wellbore42);
                        $('#rock_compress42').val(data.zone_prod_compres_rock42);
                        $('#fluid_compress42').val(data.zone_prod_compres_fluid42);
                        $('#total_compress42').val(data.zone_prod_compres_total42);
                        $('#secd_porosity42').val(data.zone_prod_second42);
                        $('#I_42').val(data.zone_prod_lambda42);
                        $('#W_42').val(data.zone_prod_omega42);
                        $('#oilshow_read142').val(data.zone_hc_oil_show42);
                        $('#oilshow_read242').val(data.zone_hc_oil_show_tools42);
                        $('#gasshow_read142').val(data.zone_hc_gas_show42);
                        $('#gasshow_read242').val(data.zone_hc_gas_show_tools42);
                        $('#wellmak_watercut42').val(data.zone_hc_water_cut42);
                        $('#water_bearing142').val(data.zone_hc_water_gwc42);
                        $('#water_bearing242').val(data.zone_hc_water_gwd_tools42);
                        $('#water_bearing342').val(data.zone_hc_water_owc42);
                        $('#water_bearing442').val(data.zone_hc_water_owc_tools42);
                        $('#s2id_rock_sampling42').select2('val', data.zone_rock_method42);
                        $('#s2id_petro_analys42').select2('val', data.zone_rock_petro42);
                        $('#sample42').val(data.zone_rock_petro_sample42);
                        $('#ticsd_42').val(data.zone_rock_top_depth42);
                        $('#bicsd_42').val(data.zone_rock_bot_depth42);
                        $('#num_totalcare42').val(data.zone_rock_barrel42);
                        $('#corebarrel42').val(data.zone_rock_barrel_equal42);
                        $('#totalrec42').val(data.zone_rock_total_core42);
                        $('#precore42').val(data.zone_rock_preservative42);
                        $('#rca_142').val(data.zone_rock_routine42);
                        $('#rca_42').val(data.zone_rock_routine_sample42);
                        $('#scal_142').val(data.zone_rock_scal42);
                        $('#scal_242').val(data.zone_rock_scal_sample42);
                        $('#zone_clastic_p10_thickness42').val(data.zone_clastic_p10_thickness42);
                        $('#zone_clastic_p50_thickness42').val(data.zone_clastic_p50_thickness42);
                        $('#zone_clastic_p90_thickness42').val(data.zone_clastic_p90_thickness42);
                        $('#zone_clastic_p10_gr42').val(data.zone_clastic_p10_gr42);
                        $('#zone_clastic_p50_gr42').val(data.zone_clastic_p50_gr42);
                        $('#zone_clastic_p90_gr42').val(data.zone_clastic_p90_gr42);
                        $('#zone_clastic_p10_sp42').val(data.zone_clastic_p10_sp42);
                        $('#zone_clastic_p50_sp42').val(data.zone_clastic_p50_sp42);
                        $('#zone_clastic_p90_sp42').val(data.zone_clastic_p90_sp42);
                        $('#zone_clastic_p10_net42').val(data.zone_clastic_p10_net42);
                        $('#zone_clastic_p50_net42').val(data.zone_clastic_p50_net42);
                        $('#zone_clastic_p90_net42').val(data.zone_clastic_p90_net42);
                        $('#zone_clastic_p10_por42').val(data.zone_clastic_p10_por42);
                        $('#zone_clastic_p50_por42').val(data.zone_clastic_p50_por42);
                        $('#zone_clastic_p90_por42').val(data.zone_clastic_p90_por42);
                        $('#zone_clastic_p10_satur42').val(data.zone_clastic_p10_satur42);
                        $('#zone_clastic_p50_satur42').val(data.zone_clastic_p50_satur42);
                        $('#zone_clastic_p90_satur42').val(data.zone_clastic_p90_satur42);
                        $('#zone_carbo_p10_thickness42').val(data.zone_carbo_p10_thickness42);
                        $('#zone_carbo_p50_thickness42').val(data.zone_carbo_p50_thickness42);
                        $('#zone_carbo_p90_thickness42').val(data.zone_carbo_p90_thickness42);
                        $('#zone_carbo_p10_dtc42').val(data.zone_carbo_p10_dtc42);
                        $('#zone_carbo_p50_dtc42').val(data.zone_carbo_p50_dtc42);
                        $('#zone_carbo_p90_dtc42').val(data.zone_carbo_p90_dtc42);
                        $('#zone_carbo_p10_total42').val(data.zone_carbo_p10_total42);
                        $('#zone_carbo_p50_total42').val(data.zone_carbo_p50_total42);
                        $('#zone_carbo_p90_total42').val(data.zone_carbo_p90_total42);
                        $('#zone_carbo_p10_net42').val(data.zone_carbo_p10_net42);
                        $('#zone_carbo_p50_net42').val(data.zone_carbo_p50_net42);
                        $('#zone_carbo_p90_net42').val(data.zone_carbo_p90_net42);
                        $('#zone_carbo_p10_por42').val(data.zone_carbo_p10_por42);
                        $('#zone_carbo_p50_por42').val(data.zone_carbo_p50_por42);
                        $('#zone_carbo_p90_por42').val(data.zone_carbo_p90_por42);
                        $('#zone_carbo_p10_satur42').val(data.zone_carbo_p10_satur42);
                        $('#zone_carbo_p50_satur42').val(data.zone_carbo_p50_satur42);
                        $('#zone_carbo_p90_satur42').val(data.zone_carbo_p90_satur42);
                        $('#z2_42').val(data.zone_fluid_date42);
                        $('#sampleat42').val(data.zone_fluid_sample42);
                        $('#gasoilratio_42').val(data.zone_fluid_ratio42);
                        $('#separator_pressure42').val(data.zone_fluid_pressure42);
                        $('#separator_temperature42').val(data.zone_fluid_temp42);
                        $('#tubing_pressure42').val(data.zone_fluid_tubing42);
                        $('#casing_pressure42').val(data.zone_fluid_casing42);
                        $('#sampleby42').val(data.zone_fluid_by42);
                        $('#repost_avail42').val(data.zone_fluid_report42);
                        $('#hydro_finger42').val(data.zone_fluid_finger42);
                        $('#oilgrav_6042').val(data.zone_fluid_grv_oil42);
                        $('#gasgrav_6042').val(data.zone_fluid_grv_gas42);
                        $('#condensategrav_6042').val(data.zone_fluid_grv_conden42);
                        $('#pvt_42').val(data.zone_fluid_pvt42);
                        $('#sample_42').val(data.zone_fluid_quantity42);
                        $('#gasdev_fac42').val(data.zone_fluid_z42);
                        $('#zone_fvf_oil_p1042').val(data.zone_fvf_oil_p1042);
                        $('#zone_fvf_oil_p5042').val(data.zone_fvf_oil_p5042);
                        $('#zone_fvf_oil_p9042').val(data.zone_fvf_oil_p9042);
                        $('#zone_fvf_gas_p1042').val(data.zone_fvf_gas_p1042);
                        $('#zone_fvf_gas_p5042').val(data.zone_fvf_gas_p5042);
                        $('#zone_fvf_gas_p9042').val(data.zone_fvf_gas_p9042);
                        $('#oilviscocity42').val(data.zone_viscocity42);
                            		
                        $('#zonename43').val(data.zone_name43);
                        $('#s2id_welltest43').select2('val', data.zone_test43);
                        $('#z_43').val(data.zone_test_date43);
                        $('#zonethickness43').val(data.zone_thickness43);
                        $('#zoneinterval43').val(data.zone_depth43);
                        $('#perforation_interval43').val(data.zone_perforation43);
                        $('#s2id_welltest_type43').select2('val', data.zone_well_test43);
                        $('#welltest_total43').val(data.zone_well_duration43);
                        $('#initialflow43').val(data.zone_initial_flow43);
                        $('#initialshutin43').val(data.zone_initial_shutin43);
                        $('#tubingsize43').val(data.zone_tubing_size43);
                        $('#initialtemperature43').val(data.zone_initial_temp43);
                        $('#zone_initial_pressure43').val(data.zone_initial_pressure43);
                        $('#res_presure43').val(data.zone_pseudostate43);
                        $('#pressurewell_for43').val(data.zone_well_formation43);
                        $('#pressurewell_head43').val(data.zone_head43);
                        $('#res_pressure43').val(data.zone_pressure_wellbore43);
                        $('#avg_porpsity43').val(data.zone_average_por43);
                        $('#watercut43').val(data.zone_water_cut43);
                        $('#initialwater_sat43').val(data.zone_initial_water43);
                        $('#lowtest_gas43').val(data.zone_low_gas43);
                        $('#lowtest_oil43').val(data.zone_low_oil43);
                        $('#freewater43').val(data.zone_free_water43);
                        $('#gas_grav43').val(data.zone_grv_gas43);
                        $('#oil_grav43').val(data.zone_grv_grv_oil43);
                        $('#zone_wellbore_coefficient43').val(data.zone_wellbore_coefficient43);
                        $('#wellbore_time43').val(data.zone_wellbore_time43);
                        $('#rsbt_43').val(data.zone_res_shape43);
                        $('#oilchoke43').val(data.zone_prod_oil_choke43);
                        $('#oilflow43').val(data.zone_prod_oil_flow43);
                        $('#gaschoke43').val(data.zone_prod_gas_choke43);
                        $('#gasflow43').val(data.zone_prod_gas_flow43);
                        $('#gasoilratio43').val(data.zone_prod_gasoil_ratio43);
                        $('#zone_prod_conden_ratio43').val(data.zone_prod_conden_ratio43);
                        $('#cummulative_pro_gas43').val(data.zone_prod_cumm_gas43);
                        $('#cummulative_pro_oil43').val(data.zone_prod_cumm_oil43);
                        $('#ab_openflow143').val(data.zone_prod_aof_bbl43);
                        $('#ab_openflow243').val(data.zone_prod_aof_scf43);
                        $('#criticalrate143').val(data.zone_prod_critical_bbl43);
                        $('#criticalrate243').val(data.zone_prod_critical_scf43);
                        $('#pro_index143').val(data.zone_prod_index_bbl43);
                        $('#pro_index243').val(data.zone_prod_index_scf43);
                        $('#diffusity_fact43').val(data.zone_prod_diffusity43);
                        $('#permeability43').val(data.zone_prod_permeability43);
                        $('#iafit_43').val(data.zone_prod_infinite43);
                        $('#wellradius43').val(data.zone_prod_well_radius43);
                        $('#pfit_43').val(data.zone_prod_pseudostate43);
                        $('#res_bondradius43').val(data.zone_prod_res_radius43);
                        $('#deltaP43').val(data.zone_prod_delta43);
                        $('#wellbore_skin43').val(data.zone_prod_wellbore43);
                        $('#rock_compress43').val(data.zone_prod_compres_rock43);
                        $('#fluid_compress43').val(data.zone_prod_compres_fluid43);
                        $('#total_compress43').val(data.zone_prod_compres_total43);
                        $('#secd_porosity43').val(data.zone_prod_second43);
                        $('#I_43').val(data.zone_prod_lambda43);
                        $('#W_43').val(data.zone_prod_omega43);
                        $('#oilshow_read143').val(data.zone_hc_oil_show43);
                        $('#oilshow_read243').val(data.zone_hc_oil_show_tools43);
                        $('#gasshow_read143').val(data.zone_hc_gas_show43);
                        $('#gasshow_read243').val(data.zone_hc_gas_show_tools43);
                        $('#wellmak_watercut43').val(data.zone_hc_water_cut43);
                        $('#water_bearing143').val(data.zone_hc_water_gwc43);
                        $('#water_bearing243').val(data.zone_hc_water_gwd_tools43);
                        $('#water_bearing343').val(data.zone_hc_water_owc43);
                        $('#water_bearing443').val(data.zone_hc_water_owc_tools43);
                        $('#s2id_rock_sampling43').select2('val', data.zone_rock_method43);
                        $('#s2id_petro_analys43').select2('val', data.zone_rock_petro43);
                        $('#sample43').val(data.zone_rock_petro_sample43);
                        $('#ticsd_43').val(data.zone_rock_top_depth43);
                        $('#bicsd_43').val(data.zone_rock_bot_depth43);
                        $('#num_totalcare43').val(data.zone_rock_barrel43);
                        $('#corebarrel43').val(data.zone_rock_barrel_equal43);
                        $('#totalrec43').val(data.zone_rock_total_core43);
                        $('#precore43').val(data.zone_rock_preservative43);
                        $('#rca_143').val(data.zone_rock_routine43);
                        $('#rca_43').val(data.zone_rock_routine_sample43);
                        $('#scal_143').val(data.zone_rock_scal43);
                        $('#scal_243').val(data.zone_rock_scal_sample43);
                        $('#zone_clastic_p10_thickness43').val(data.zone_clastic_p10_thickness43);
                        $('#zone_clastic_p50_thickness43').val(data.zone_clastic_p50_thickness43);
                        $('#zone_clastic_p90_thickness43').val(data.zone_clastic_p90_thickness43);
                        $('#zone_clastic_p10_gr43').val(data.zone_clastic_p10_gr43);
                        $('#zone_clastic_p50_gr43').val(data.zone_clastic_p50_gr43);
                        $('#zone_clastic_p90_gr43').val(data.zone_clastic_p90_gr43);
                        $('#zone_clastic_p10_sp43').val(data.zone_clastic_p10_sp43);
                        $('#zone_clastic_p50_sp43').val(data.zone_clastic_p50_sp43);
                        $('#zone_clastic_p90_sp43').val(data.zone_clastic_p90_sp43);
                        $('#zone_clastic_p10_net43').val(data.zone_clastic_p10_net43);
                        $('#zone_clastic_p50_net43').val(data.zone_clastic_p50_net43);
                        $('#zone_clastic_p90_net43').val(data.zone_clastic_p90_net43);
                        $('#zone_clastic_p10_por43').val(data.zone_clastic_p10_por43);
                        $('#zone_clastic_p50_por43').val(data.zone_clastic_p50_por43);
                        $('#zone_clastic_p90_por43').val(data.zone_clastic_p90_por43);
                        $('#zone_clastic_p10_satur43').val(data.zone_clastic_p10_satur43);
                        $('#zone_clastic_p50_satur43').val(data.zone_clastic_p50_satur43);
                        $('#zone_clastic_p90_satur43').val(data.zone_clastic_p90_satur43);
                        $('#zone_carbo_p10_thickness43').val(data.zone_carbo_p10_thickness43);
                        $('#zone_carbo_p50_thickness43').val(data.zone_carbo_p50_thickness43);
                        $('#zone_carbo_p90_thickness43').val(data.zone_carbo_p90_thickness43);
                        $('#zone_carbo_p10_dtc43').val(data.zone_carbo_p10_dtc43);
                        $('#zone_carbo_p50_dtc43').val(data.zone_carbo_p50_dtc43);
                        $('#zone_carbo_p90_dtc43').val(data.zone_carbo_p90_dtc43);
                        $('#zone_carbo_p10_total43').val(data.zone_carbo_p10_total43);
                        $('#zone_carbo_p50_total43').val(data.zone_carbo_p50_total43);
                        $('#zone_carbo_p90_total43').val(data.zone_carbo_p90_total43);
                        $('#zone_carbo_p10_net43').val(data.zone_carbo_p10_net43);
                        $('#zone_carbo_p50_net43').val(data.zone_carbo_p50_net43);
                        $('#zone_carbo_p90_net43').val(data.zone_carbo_p90_net43);
                        $('#zone_carbo_p10_por43').val(data.zone_carbo_p10_por43);
                        $('#zone_carbo_p50_por43').val(data.zone_carbo_p50_por43);
                        $('#zone_carbo_p90_por43').val(data.zone_carbo_p90_por43);
                        $('#zone_carbo_p10_satur43').val(data.zone_carbo_p10_satur43);
                        $('#zone_carbo_p50_satur43').val(data.zone_carbo_p50_satur43);
                        $('#zone_carbo_p90_satur43').val(data.zone_carbo_p90_satur43);
                        $('#z2_43').val(data.zone_fluid_date43);
                        $('#sampleat43').val(data.zone_fluid_sample43);
                        $('#gasoilratio_43').val(data.zone_fluid_ratio43);
                        $('#separator_pressure43').val(data.zone_fluid_pressure43);
                        $('#separator_temperature43').val(data.zone_fluid_temp43);
                        $('#tubing_pressure43').val(data.zone_fluid_tubing43);
                        $('#casing_pressure43').val(data.zone_fluid_casing43);
                        $('#sampleby43').val(data.zone_fluid_by43);
                        $('#repost_avail43').val(data.zone_fluid_report43);
                        $('#hydro_finger43').val(data.zone_fluid_finger43);
                        $('#oilgrav_6043').val(data.zone_fluid_grv_oil43);
                        $('#gasgrav_6043').val(data.zone_fluid_grv_gas43);
                        $('#condensategrav_6043').val(data.zone_fluid_grv_conden43);
                        $('#pvt_43').val(data.zone_fluid_pvt43);
                        $('#sample_43').val(data.zone_fluid_quantity43);
                        $('#gasdev_fac43').val(data.zone_fluid_z43);
                        $('#zone_fvf_oil_p1043').val(data.zone_fvf_oil_p1043);
                        $('#zone_fvf_oil_p5043').val(data.zone_fvf_oil_p5043);
                        $('#zone_fvf_oil_p9043').val(data.zone_fvf_oil_p9043);
                        $('#zone_fvf_gas_p1043').val(data.zone_fvf_gas_p1043);
                        $('#zone_fvf_gas_p5043').val(data.zone_fvf_gas_p5043);
                        $('#zone_fvf_gas_p9043').val(data.zone_fvf_gas_p9043);
                        $('#oilviscocity43').val(data.zone_viscocity43);
                            		
                        $('#zonename44').val(data.zone_name44);
                        $('#s2id_welltest44').select2('val', data.zone_test44);
                        $('#z_44').val(data.zone_test_date44);
                        $('#zonethickness44').val(data.zone_thickness44);
                        $('#zoneinterval44').val(data.zone_depth44);
                        $('#perforation_interval44').val(data.zone_perforation44);
                        $('#s2id_welltest_type44').select2('val', data.zone_well_test44);
                        $('#welltest_total44').val(data.zone_well_duration44);
                        $('#initialflow44').val(data.zone_initial_flow44);
                        $('#initialshutin44').val(data.zone_initial_shutin44);
                        $('#tubingsize44').val(data.zone_tubing_size44);
                        $('#initialtemperature44').val(data.zone_initial_temp44);
                        $('#zone_initial_pressure44').val(data.zone_initial_pressure44);
                        $('#res_presure44').val(data.zone_pseudostate44);
                        $('#pressurewell_for44').val(data.zone_well_formation44);
                        $('#pressurewell_head44').val(data.zone_head44);
                        $('#res_pressure44').val(data.zone_pressure_wellbore44);
                        $('#avg_porpsity44').val(data.zone_average_por44);
                        $('#watercut44').val(data.zone_water_cut44);
                        $('#initialwater_sat44').val(data.zone_initial_water44);
                        $('#lowtest_gas44').val(data.zone_low_gas44);
                        $('#lowtest_oil44').val(data.zone_low_oil44);
                        $('#freewater44').val(data.zone_free_water44);
                        $('#gas_grav44').val(data.zone_grv_gas44);
                        $('#oil_grav44').val(data.zone_grv_grv_oil44);
                        $('#zone_wellbore_coefficient44').val(data.zone_wellbore_coefficient44);
                        $('#wellbore_time44').val(data.zone_wellbore_time44);
                        $('#rsbt_44').val(data.zone_res_shape44);
                        $('#oilchoke44').val(data.zone_prod_oil_choke44);
                        $('#oilflow44').val(data.zone_prod_oil_flow44);
                        $('#gaschoke44').val(data.zone_prod_gas_choke44);
                        $('#gasflow44').val(data.zone_prod_gas_flow44);
                        $('#gasoilratio44').val(data.zone_prod_gasoil_ratio44);
                        $('#zone_prod_conden_ratio44').val(data.zone_prod_conden_ratio44);
                        $('#cummulative_pro_gas44').val(data.zone_prod_cumm_gas44);
                        $('#cummulative_pro_oil44').val(data.zone_prod_cumm_oil44);
                        $('#ab_openflow144').val(data.zone_prod_aof_bbl44);
                        $('#ab_openflow244').val(data.zone_prod_aof_scf44);
                        $('#criticalrate144').val(data.zone_prod_critical_bbl44);
                        $('#criticalrate244').val(data.zone_prod_critical_scf44);
                        $('#pro_index144').val(data.zone_prod_index_bbl44);
                        $('#pro_index244').val(data.zone_prod_index_scf44);
                        $('#diffusity_fact44').val(data.zone_prod_diffusity44);
                        $('#permeability44').val(data.zone_prod_permeability44);
                        $('#iafit_44').val(data.zone_prod_infinite44);
                        $('#wellradius44').val(data.zone_prod_well_radius44);
                        $('#pfit_44').val(data.zone_prod_pseudostate44);
                        $('#res_bondradius44').val(data.zone_prod_res_radius44);
                        $('#deltaP44').val(data.zone_prod_delta44);
                        $('#wellbore_skin44').val(data.zone_prod_wellbore44);
                        $('#rock_compress44').val(data.zone_prod_compres_rock44);
                        $('#fluid_compress44').val(data.zone_prod_compres_fluid44);
                        $('#total_compress44').val(data.zone_prod_compres_total44);
                        $('#secd_porosity44').val(data.zone_prod_second44);
                        $('#I_44').val(data.zone_prod_lambda44);
                        $('#W_44').val(data.zone_prod_omega44);
                        $('#oilshow_read144').val(data.zone_hc_oil_show44);
                        $('#oilshow_read244').val(data.zone_hc_oil_show_tools44);
                        $('#gasshow_read144').val(data.zone_hc_gas_show44);
                        $('#gasshow_read244').val(data.zone_hc_gas_show_tools44);
                        $('#wellmak_watercut44').val(data.zone_hc_water_cut44);
                        $('#water_bearing144').val(data.zone_hc_water_gwc44);
                        $('#water_bearing244').val(data.zone_hc_water_gwd_tools44);
                        $('#water_bearing344').val(data.zone_hc_water_owc44);
                        $('#water_bearing444').val(data.zone_hc_water_owc_tools44);
                        $('#s2id_rock_sampling44').select2('val', data.zone_rock_method44);
                        $('#s2id_petro_analys44').select2('val', data.zone_rock_petro44);
                        $('#sample44').val(data.zone_rock_petro_sample44);
                        $('#ticsd_44').val(data.zone_rock_top_depth44);
                        $('#bicsd_44').val(data.zone_rock_bot_depth44);
                        $('#num_totalcare44').val(data.zone_rock_barrel44);
                        $('#corebarrel44').val(data.zone_rock_barrel_equal44);
                        $('#totalrec44').val(data.zone_rock_total_core44);
                        $('#precore44').val(data.zone_rock_preservative44);
                        $('#rca_144').val(data.zone_rock_routine44);
                        $('#rca_44').val(data.zone_rock_routine_sample44);
                        $('#scal_144').val(data.zone_rock_scal44);
                        $('#scal_244').val(data.zone_rock_scal_sample44);
                        $('#zone_clastic_p10_thickness44').val(data.zone_clastic_p10_thickness44);
                        $('#zone_clastic_p50_thickness44').val(data.zone_clastic_p50_thickness44);
                        $('#zone_clastic_p90_thickness44').val(data.zone_clastic_p90_thickness44);
                        $('#zone_clastic_p10_gr44').val(data.zone_clastic_p10_gr44);
                        $('#zone_clastic_p50_gr44').val(data.zone_clastic_p50_gr44);
                        $('#zone_clastic_p90_gr44').val(data.zone_clastic_p90_gr44);
                        $('#zone_clastic_p10_sp44').val(data.zone_clastic_p10_sp44);
                        $('#zone_clastic_p50_sp44').val(data.zone_clastic_p50_sp44);
                        $('#zone_clastic_p90_sp44').val(data.zone_clastic_p90_sp44);
                        $('#zone_clastic_p10_net44').val(data.zone_clastic_p10_net44);
                        $('#zone_clastic_p50_net44').val(data.zone_clastic_p50_net44);
                        $('#zone_clastic_p90_net44').val(data.zone_clastic_p90_net44);
                        $('#zone_clastic_p10_por44').val(data.zone_clastic_p10_por44);
                        $('#zone_clastic_p50_por44').val(data.zone_clastic_p50_por44);
                        $('#zone_clastic_p90_por44').val(data.zone_clastic_p90_por44);
                        $('#zone_clastic_p10_satur44').val(data.zone_clastic_p10_satur44);
                        $('#zone_clastic_p50_satur44').val(data.zone_clastic_p50_satur44);
                        $('#zone_clastic_p90_satur44').val(data.zone_clastic_p90_satur44);
                        $('#zone_carbo_p10_thickness44').val(data.zone_carbo_p10_thickness44);
                        $('#zone_carbo_p50_thickness44').val(data.zone_carbo_p50_thickness44);
                        $('#zone_carbo_p90_thickness44').val(data.zone_carbo_p90_thickness44);
                        $('#zone_carbo_p10_dtc44').val(data.zone_carbo_p10_dtc44);
                        $('#zone_carbo_p50_dtc44').val(data.zone_carbo_p50_dtc44);
                        $('#zone_carbo_p90_dtc44').val(data.zone_carbo_p90_dtc44);
                        $('#zone_carbo_p10_total44').val(data.zone_carbo_p10_total44);
                        $('#zone_carbo_p50_total44').val(data.zone_carbo_p50_total44);
                        $('#zone_carbo_p90_total44').val(data.zone_carbo_p90_total44);
                        $('#zone_carbo_p10_net44').val(data.zone_carbo_p10_net44);
                        $('#zone_carbo_p50_net44').val(data.zone_carbo_p50_net44);
                        $('#zone_carbo_p90_net44').val(data.zone_carbo_p90_net44);
                        $('#zone_carbo_p10_por44').val(data.zone_carbo_p10_por44);
                        $('#zone_carbo_p50_por44').val(data.zone_carbo_p50_por44);
                        $('#zone_carbo_p90_por44').val(data.zone_carbo_p90_por44);
                        $('#zone_carbo_p10_satur44').val(data.zone_carbo_p10_satur44);
                        $('#zone_carbo_p50_satur44').val(data.zone_carbo_p50_satur44);
                        $('#zone_carbo_p90_satur44').val(data.zone_carbo_p90_satur44);
                        $('#z2_44').val(data.zone_fluid_date44);
                        $('#sampleat44').val(data.zone_fluid_sample44);
                        $('#gasoilratio_44').val(data.zone_fluid_ratio44);
                        $('#separator_pressure44').val(data.zone_fluid_pressure44);
                        $('#separator_temperature44').val(data.zone_fluid_temp44);
                        $('#tubing_pressure44').val(data.zone_fluid_tubing44);
                        $('#casing_pressure44').val(data.zone_fluid_casing44);
                        $('#sampleby44').val(data.zone_fluid_by44);
                        $('#repost_avail44').val(data.zone_fluid_report44);
                        $('#hydro_finger44').val(data.zone_fluid_finger44);
                        $('#oilgrav_6044').val(data.zone_fluid_grv_oil44);
                        $('#gasgrav_6044').val(data.zone_fluid_grv_gas44);
                        $('#condensategrav_6044').val(data.zone_fluid_grv_conden44);
                        $('#pvt_44').val(data.zone_fluid_pvt44);
                        $('#sample_44').val(data.zone_fluid_quantity44);
                        $('#gasdev_fac44').val(data.zone_fluid_z44);
                        $('#zone_fvf_oil_p1044').val(data.zone_fvf_oil_p1044);
                        $('#zone_fvf_oil_p5044').val(data.zone_fvf_oil_p5044);
                        $('#zone_fvf_oil_p9044').val(data.zone_fvf_oil_p9044);
                        $('#zone_fvf_gas_p1044').val(data.zone_fvf_gas_p1044);
                        $('#zone_fvf_gas_p5044').val(data.zone_fvf_gas_p5044);
                        $('#zone_fvf_gas_p9044').val(data.zone_fvf_gas_p9044);
                        $('#oilviscocity44').val(data.zone_viscocity44);
                            		
                        $('#zonename45').val(data.zone_name45);
                        $('#s2id_welltest45').select2('val', data.zone_test45);
                        $('#z_45').val(data.zone_test_date45);
                        $('#zonethickness45').val(data.zone_thickness45);
                        $('#zoneinterval45').val(data.zone_depth45);
                        $('#perforation_interval45').val(data.zone_perforation45);
                        $('#s2id_welltest_type45').select2('val', data.zone_well_test45);
                        $('#welltest_total45').val(data.zone_well_duration45);
                        $('#initialflow45').val(data.zone_initial_flow45);
                        $('#initialshutin45').val(data.zone_initial_shutin45);
                        $('#tubingsize45').val(data.zone_tubing_size45);
                        $('#initialtemperature45').val(data.zone_initial_temp45);
                        $('#zone_initial_pressure45').val(data.zone_initial_pressure45);
                        $('#res_presure45').val(data.zone_pseudostate45);
                        $('#pressurewell_for45').val(data.zone_well_formation45);
                        $('#pressurewell_head45').val(data.zone_head45);
                        $('#res_pressure45').val(data.zone_pressure_wellbore45);
                        $('#avg_porpsity45').val(data.zone_average_por45);
                        $('#watercut45').val(data.zone_water_cut45);
                        $('#initialwater_sat45').val(data.zone_initial_water45);
                        $('#lowtest_gas45').val(data.zone_low_gas45);
                        $('#lowtest_oil45').val(data.zone_low_oil45);
                        $('#freewater45').val(data.zone_free_water45);
                        $('#gas_grav45').val(data.zone_grv_gas45);
                        $('#oil_grav45').val(data.zone_grv_grv_oil45);
                        $('#zone_wellbore_coefficient45').val(data.zone_wellbore_coefficient45);
                        $('#wellbore_time45').val(data.zone_wellbore_time45);
                        $('#rsbt_45').val(data.zone_res_shape45);
                        $('#oilchoke45').val(data.zone_prod_oil_choke45);
                        $('#oilflow45').val(data.zone_prod_oil_flow45);
                        $('#gaschoke45').val(data.zone_prod_gas_choke45);
                        $('#gasflow45').val(data.zone_prod_gas_flow45);
                        $('#gasoilratio45').val(data.zone_prod_gasoil_ratio45);
                        $('#zone_prod_conden_ratio45').val(data.zone_prod_conden_ratio45);
                        $('#cummulative_pro_gas45').val(data.zone_prod_cumm_gas45);
                        $('#cummulative_pro_oil45').val(data.zone_prod_cumm_oil45);
                        $('#ab_openflow145').val(data.zone_prod_aof_bbl45);
                        $('#ab_openflow245').val(data.zone_prod_aof_scf45);
                        $('#criticalrate145').val(data.zone_prod_critical_bbl45);
                        $('#criticalrate245').val(data.zone_prod_critical_scf45);
                        $('#pro_index145').val(data.zone_prod_index_bbl45);
                        $('#pro_index245').val(data.zone_prod_index_scf45);
                        $('#diffusity_fact45').val(data.zone_prod_diffusity45);
                        $('#permeability45').val(data.zone_prod_permeability45);
                        $('#iafit_45').val(data.zone_prod_infinite45);
                        $('#wellradius45').val(data.zone_prod_well_radius45);
                        $('#pfit_45').val(data.zone_prod_pseudostate45);
                        $('#res_bondradius45').val(data.zone_prod_res_radius45);
                        $('#deltaP45').val(data.zone_prod_delta45);
                        $('#wellbore_skin45').val(data.zone_prod_wellbore45);
                        $('#rock_compress45').val(data.zone_prod_compres_rock45);
                        $('#fluid_compress45').val(data.zone_prod_compres_fluid45);
                        $('#total_compress45').val(data.zone_prod_compres_total45);
                        $('#secd_porosity45').val(data.zone_prod_second45);
                        $('#I_45').val(data.zone_prod_lambda45);
                        $('#W_45').val(data.zone_prod_omega45);
                        $('#oilshow_read145').val(data.zone_hc_oil_show45);
                        $('#oilshow_read245').val(data.zone_hc_oil_show_tools45);
                        $('#gasshow_read145').val(data.zone_hc_gas_show45);
                        $('#gasshow_read245').val(data.zone_hc_gas_show_tools45);
                        $('#wellmak_watercut45').val(data.zone_hc_water_cut45);
                        $('#water_bearing145').val(data.zone_hc_water_gwc45);
                        $('#water_bearing245').val(data.zone_hc_water_gwd_tools45);
                        $('#water_bearing345').val(data.zone_hc_water_owc45);
                        $('#water_bearing445').val(data.zone_hc_water_owc_tools45);
                        $('#s2id_rock_sampling45').select2('val', data.zone_rock_method45);
                        $('#s2id_petro_analys45').select2('val', data.zone_rock_petro45);
                        $('#sample45').val(data.zone_rock_petro_sample45);
                        $('#ticsd_45').val(data.zone_rock_top_depth45);
                        $('#bicsd_45').val(data.zone_rock_bot_depth45);
                        $('#num_totalcare45').val(data.zone_rock_barrel45);
                        $('#corebarrel45').val(data.zone_rock_barrel_equal45);
                        $('#totalrec45').val(data.zone_rock_total_core45);
                        $('#precore45').val(data.zone_rock_preservative45);
                        $('#rca_145').val(data.zone_rock_routine45);
                        $('#rca_45').val(data.zone_rock_routine_sample45);
                        $('#scal_145').val(data.zone_rock_scal45);
                        $('#scal_245').val(data.zone_rock_scal_sample45);
                        $('#zone_clastic_p10_thickness45').val(data.zone_clastic_p10_thickness45);
                        $('#zone_clastic_p50_thickness45').val(data.zone_clastic_p50_thickness45);
                        $('#zone_clastic_p90_thickness45').val(data.zone_clastic_p90_thickness45);
                        $('#zone_clastic_p10_gr45').val(data.zone_clastic_p10_gr45);
                        $('#zone_clastic_p50_gr45').val(data.zone_clastic_p50_gr45);
                        $('#zone_clastic_p90_gr45').val(data.zone_clastic_p90_gr45);
                        $('#zone_clastic_p10_sp45').val(data.zone_clastic_p10_sp45);
                        $('#zone_clastic_p50_sp45').val(data.zone_clastic_p50_sp45);
                        $('#zone_clastic_p90_sp45').val(data.zone_clastic_p90_sp45);
                        $('#zone_clastic_p10_net45').val(data.zone_clastic_p10_net45);
                        $('#zone_clastic_p50_net45').val(data.zone_clastic_p50_net45);
                        $('#zone_clastic_p90_net45').val(data.zone_clastic_p90_net45);
                        $('#zone_clastic_p10_por45').val(data.zone_clastic_p10_por45);
                        $('#zone_clastic_p50_por45').val(data.zone_clastic_p50_por45);
                        $('#zone_clastic_p90_por45').val(data.zone_clastic_p90_por45);
                        $('#zone_clastic_p10_satur45').val(data.zone_clastic_p10_satur45);
                        $('#zone_clastic_p50_satur45').val(data.zone_clastic_p50_satur45);
                        $('#zone_clastic_p90_satur45').val(data.zone_clastic_p90_satur45);
                        $('#zone_carbo_p10_thickness45').val(data.zone_carbo_p10_thickness45);
                        $('#zone_carbo_p50_thickness45').val(data.zone_carbo_p50_thickness45);
                        $('#zone_carbo_p90_thickness45').val(data.zone_carbo_p90_thickness45);
                        $('#zone_carbo_p10_dtc45').val(data.zone_carbo_p10_dtc45);
                        $('#zone_carbo_p50_dtc45').val(data.zone_carbo_p50_dtc45);
                        $('#zone_carbo_p90_dtc45').val(data.zone_carbo_p90_dtc45);
                        $('#zone_carbo_p10_total45').val(data.zone_carbo_p10_total45);
                        $('#zone_carbo_p50_total45').val(data.zone_carbo_p50_total45);
                        $('#zone_carbo_p90_total45').val(data.zone_carbo_p90_total45);
                        $('#zone_carbo_p10_net45').val(data.zone_carbo_p10_net45);
                        $('#zone_carbo_p50_net45').val(data.zone_carbo_p50_net45);
                        $('#zone_carbo_p90_net45').val(data.zone_carbo_p90_net45);
                        $('#zone_carbo_p10_por45').val(data.zone_carbo_p10_por45);
                        $('#zone_carbo_p50_por45').val(data.zone_carbo_p50_por45);
                        $('#zone_carbo_p90_por45').val(data.zone_carbo_p90_por45);
                        $('#zone_carbo_p10_satur45').val(data.zone_carbo_p10_satur45);
                        $('#zone_carbo_p50_satur45').val(data.zone_carbo_p50_satur45);
                        $('#zone_carbo_p90_satur45').val(data.zone_carbo_p90_satur45);
                        $('#z2_45').val(data.zone_fluid_date45);
                        $('#sampleat45').val(data.zone_fluid_sample45);
                        $('#gasoilratio_45').val(data.zone_fluid_ratio45);
                        $('#separator_pressure45').val(data.zone_fluid_pressure45);
                        $('#separator_temperature45').val(data.zone_fluid_temp45);
                        $('#tubing_pressure45').val(data.zone_fluid_tubing45);
                        $('#casing_pressure45').val(data.zone_fluid_casing45);
                        $('#sampleby45').val(data.zone_fluid_by45);
                        $('#repost_avail45').val(data.zone_fluid_report45);
                        $('#hydro_finger45').val(data.zone_fluid_finger45);
                        $('#oilgrav_6045').val(data.zone_fluid_grv_oil45);
                        $('#gasgrav_6045').val(data.zone_fluid_grv_gas45);
                        $('#condensategrav_6045').val(data.zone_fluid_grv_conden45);
                        $('#pvt_45').val(data.zone_fluid_pvt45);
                        $('#sample_45').val(data.zone_fluid_quantity45);
                        $('#gasdev_fac45').val(data.zone_fluid_z45);
                        $('#zone_fvf_oil_p1045').val(data.zone_fvf_oil_p1045);
                        $('#zone_fvf_oil_p5045').val(data.zone_fvf_oil_p5045);
                        $('#zone_fvf_oil_p9045').val(data.zone_fvf_oil_p9045);
                        $('#zone_fvf_gas_p1045').val(data.zone_fvf_gas_p1045);
                        $('#zone_fvf_gas_p5045').val(data.zone_fvf_gas_p5045);
                        $('#zone_fvf_gas_p9045').val(data.zone_fvf_gas_p9045);
                        $('#oilviscocity45').val(data.zone_viscocity45);
                                		
                        if(data.sgf_is_check != null) {
                        	$('#DlGfs').parent().addClass('checked');
							$('#DlGfs').prop('checked', true);
							toggleStatus('DlGfs','DlGfs_link','col_dav1');
                        } else {
                            $('#DlGfs').parent().removeClass('checked');
                           	$('#DlGfs').prop('checked', false);
                           	$('#col_dav1').css('height', '0px');
							$('#DlGfs_link').css('color', 'gray');
							$('#col_dav1').find('input,select,textarea,button').prop('disabled', true);
                        }	
                        $('#dav_gfs_aqyear').val(data.sgf_year_survey);
                        $('#s2id_dav_gfs_surmethod').select2('val', data.sgf_survey_method);
                        $('#dav_gfs_tca').val(data.sgf_coverage_area);
                        $('#sgf_p90_area').val(data.sgf_p90_area);
                        $('#sgf_p90_thickness').val(data.sgf_p90_thickness);
                        $('#sgf_p90_net').val(data.sgf_p90_net);
                        $('#sgf_p50_area').val(data.sgf_p50_area);
                        $('#sgf_p50_thickness').val(data.sgf_p50_thickness);
                        $('#sgf_p50_net').val(data.sgf_p50_net);
                        $('#sgf_p10_area').val(data.sgf_p10_area);
                        $('#sgf_p10_thickness').val(data.sgf_p10_thickness);
                        $('#sgf_p10_net').val(data.sgf_p10_net);
                        $('#dav_gfs_remarks').val(data.sgf_remark);
						if(data.s2d_is_check != null) {
                            $('#DlDss2').parent().addClass('checked');
							$('#DlDss2').prop('checked', true);
							toggleStatus('DlDss2','DlDss2_link','col_dav2');
                        } else {
                            $('#DlDss2').parent().removeClass('checked');
                           	$('#DlDss2').prop('checked', false);
                           	$('#col_dav2').css('height', '0px');
							$('#DlDss2_link').css('color', 'gray');
							$('#col_dav2').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_2dss_aqyear').val(data.s2d_year_survey);
                        $('#s2id_dav_2dss_numvin').select2('val', data.s2d_vintage_number);
                        $('#dav_2dss_tcn').val(data.s2d_total_crossline);
                        $('#dav_2dss_sltl').val(data.s2d_seismic_line);
                        $('#dav_2dss_aspi').val(data.s2d_average_interval);
                        $('#dav_2dss_rl1').val(data.s2d_record_length_ms);
                        $('#dav_2dss_rl2').val(data.s2d_record_length_ft);
                        $('#dav_2dss_yearlatest').val(data.s2d_year_late_process);
                        $('#s2id_dav_2dss_lpm').select2('val', data.s2d_late_method);
                        $('#s2id_dav_2dss_siq').select2('val', data.s2d_img_quality);
                        $('#dav_2dss_trtd1').val(data.s2d_top_depth_ft);
                        $('#dav_2dss_trtd2').val(data.s2d_top_depth_ms);
                        $('#dav_2dss_brtd1').val(data.s2d_bot_depth_ft);
                        $('#dav_2dss_brtd2').val(data.s2d_bot_depth_ms);
                        $('#dav_2dss_dcsp').val(data.s2d_depth_spill);
                        $('#dav_2dss_owc1').val(data.s2d_depth_estimate);
                        $('#dlosd2m2').val(data.s2d_depth_estimate_analog);
                        $('#dav_2dss_owc2').val(data.s2d_depth_estimate_spill);
                        $('#dav_2dss_oilgas1').val(data.s2d_depth_low);
                        $('#dlosd2n2').val(data.s2d_depth_low_analog);
                        $('#oilgas2').val(data.s2d_depth_low_spill);
                        $('#dav_2dss_forthickness').val(data.s2d_formation_thickness);
                        $('#dav_2dss_gross_resthickness').val(data.s2d_gross_thickness);
                        $('#s2id_dav_2dss_antrip').select2('val', data.s2d_avail_pay);
                        $('#s2d_net_p90_thickness').val(data.s2d_net_p90_thickness);
                        $('#s2d_net_p90_vsh').val(data.s2d_net_p90_vsh);
                        $('#s2d_net_p90_por').val(data.s2d_net_p90_por);
                        $('#s2d_net_p90_satur').val(data.s2d_net_p90_satur);
                        $('#s2d_net_p50_thickness').val(data.s2d_net_p50_thickness);
                        $('#s2d_net_p50_vsh').val(data.s2d_net_p50_vsh);
                        $('#s2d_net_p50_por').val(data.s2d_net_p50_por);
                        $('#s2d_net_p50_satur').val(data.s2d_net_p50_satur);
                        $('#s2d_net_p10_thickness').val(data.s2d_net_p10_thickness);
                        $('#s2d_net_p10_vsh').val(data.s2d_net_p10_vsh);
                        $('#s2d_net_p10_por').val(data.s2d_net_p10_por);
                        $('#s2d_net_p10_satur').val(data.s2d_net_p10_satur);
                        $('#s2d_vol_p90_area').val(data.s2d_vol_p90_area);
                        $('#s2d_vol_p50_area').val(data.s2d_vol_p50_area);
                        $('#s2d_vol_p10_area').val(data.s2d_vol_p10_area);
                        $('#dav_2dss_remarks').val(data.s2d_remark);
                        if(data.sgv_is_check != null) {
                            $('#DlGras').parent().addClass('checked');
							$('#DlGras').prop('checked', true);
							toggleStatus('DlGras','DlGras_link','col_dav3');
                        } else {
                            $('#DlGras').parent().removeClass('checked');
                           	$('#DlGras').prop('checked', false);
                           	$('#col_dav3').css('height', '0px');
							$('#DlGras_link').css('color', 'gray');
							$('#col_dav3').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_grasur_aqyear').val(data.sgv_year_survey);
                        $('#s2id_dav_grasur_surveymethods').select2('val', data.sgv_survey_method);
                        $('#dav_grasur_sca').val(data.sgv_coverage_area);
                        $('#dav_grasur_dspr').val(data.sgv_depth_range);
                        $('#dav_grasur_rsi').val(data.sgv_spacing_interval);
                        $('#dav_grasur_dcsp').val(data.sgv_depth_spill);
                        $('#dav_grasur_owc1').val(data.sgv_depth_estimate);
                        $('#dlosd3g2').val(data.sgv_depth_estimate_analog);
                        $('#dav_grasur_owc2').val(data.sgv_depth_estimate_spill);
                        $('#dav_grasur_oilgas1').val(data.sgv_depth_low);
                        $('#dlosd3h2').val(data.sgv_depth_low_analog);
                        $('#dav_grasur_oilgas2').val(data.sgv_depth_low_spill);
                        $('#dav_grasur_resthickness1').val(data.sgv_res_thickness);
                        $('#dav_grasur_resthickness2').val(data.sgv_res_according);
                        $('#dav_grasur_resthickness3').val(data.sgv_res_top_depth);
                        $('#dav_grasur_resthickness4').val(data.sgv_res_bot_depth);
                        $('#dav_grasur_gst1').val(data.sgv_gross_thickness);
                        $('#dav_grasur_gst2').val(data.sgv_gross_according);
                        $('#dav_grasur_gst3').val(data.sgv_gross_well);
                        $('#dav_grasur_netogross').val(data.sgv_net_gross);
                        $('#sgv_vol_p90_area').val(data.sgv_vol_p90_area);
                        $('#sgv_vol_p50_area').val(data.sgv_vol_p50_area);
                        $('#sgv_vol_p10_area').val(data.sgv_vol_p10_area);
                        $('#dav_grasur_remarks').val(data.sgv_remark);
                        if(data.sgc_is_check != null) {
                            $('#DlGeos').parent().addClass('checked');
							$('#DlGeos').prop('checked', true);
							toggleStatus('DlGeos','DlGeos_link','col_dav4');
                        } else {
                            $('#DlGeos').parent().removeClass('checked');
                           	$('#DlGeos').prop('checked', false);
                           	$('#col_dav4').css('height', '0px');
							$('#DlGeos_link').css('color', 'gray');
							$('#col_dav4').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_geo_aqyear').val(data.sgc_year_survey);
                        $('#dav_geo_isr').val(data.sgc_sample_interval);
                        $('#dav_geo_numsample').val(data.sgc_number_sample);
                        $('#dav_geo_numrock').val(data.sgc_number_rock);
                        $('#dav_geo_numfluid').val(data.sgc_number_fluid);
                        $('#s2id_dav_geo_ahc').select2('val', data.sgc_avail_hc);
                        $('#dav_geo_hycomp').val(data.sgc_hc_composition);
                        $('#dav_geo_lery').val(data.sgc_lab_report);
                        $('#sgc_vol_p90_area').val(data.sgc_vol_p90_area);
                        $('#sgc_vol_p50_area').val(data.sgc_vol_p50_area);
                        $('#sgc_vol_p10_area').val(data.sgc_vol_p10_area);
                        $('#dav_geo_remarks').val(data.sgc_remark);
                        if(data.sel_is_check != null) {
                            $('#DlEs').parent().addClass('checked');
							$('#DlEs').prop('checked', true);
							toggleStatus('DlEs','DlEs_link','col_dav5');
                        } else {
                            $('#DlEs').parent().removeClass('checked');
                           	$('#DlEs').prop('checked', false);
                           	$('#col_dav5').css('height', '0px');
							$('#DlEs_link').css('color', 'gray');
							$('#col_dav5').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_elec_aqyear').val(data.sel_year_survey);
                        $('#s2id_dav_elec_surveymethods').select2('val', data.sel_survey_method);
                        $('#dav_elec_sca').val(data.sel_coverage_area);
                        $('#dav_elec_dspr').val(data.sel_depth_range);
                        $('#dav_elec_rsi').val(data.sel_spacing_interval);
                        $('#sel_vol_p90_area').val(data.sel_vol_p90_area);
                        $('#sel_vol_p50_area').val(data.sel_vol_p50_area);
                        $('#sel_vol_p10_area').val(data.sel_vol_p10_area);
                        $('#dav_elec_remarks').val(data.sel_remark);
                        if(data.rst_is_check != null) {
                            $('#DlRs').parent().addClass('checked');
							$('#DlRs').prop('checked', true);
							toggleStatus('DlRs','DlRs_link','col_dav6');
                        } else {
                            $('#DlRs').parent().removeClass('checked');
                           	$('#DlRs').prop('checked', false);
                           	$('#col_dav6').css('height', '0px');
							$('#DlRs_link').css('color', 'gray');
							$('#col_dav6').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_res_aqyear').val(data.rst_year_survey);
                        $('#s2id_dav_res_surveymethods').select2('val', data.rst_survey_method);
                        $('#dav_res_sca').val(data.rst_coverage_area);
                        $('#dav_res_dspr').val(data.rst_depth_range);
                        $('#dav_res_rsi').val(data.rst_spacing_interval);
                        $('#rst_vol_p90_area').val(data.rst_vol_p90_area);
                        $('#rst_vol_p50_area').val(data.rst_vol_p50_area);
                        $('#rst_vol_p10_area').val(data.rst_vol_p10_area);
                        $('#dav_res_remarks').val(data.rst_remark);
                        if(data.sor_is_check != null) {
                            $('#DlOs').parent().addClass('checked');
							$('#DlOs').prop('checked', true);
							toggleStatus('DlOs','DlOs_link','col_dav7');
                        } else {
                            $('#DlOs').parent().removeClass('checked');
                           	$('#DlOs').prop('checked', false);
                           	$('#col_dav7').css('height', '0px');
							$('#DlOs_link').css('color', 'gray');
							$('#col_dav7').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_other_aqyear').val(data.sor_year_survey);
                        $('#sor_vol_p90_area').val(data.sor_vol_p90_area);
                        $('#sor_vol_p50_area').val(data.sor_vol_p50_area);
                        $('#sor_vol_p10_area').val(data.sor_vol_p10_area);
                        $('#dav_other_remarks').val(data.sor_remark);
                        if(data.s3d_is_check != null) {
                            $('#DlDss3').parent().addClass('checked');
							$('#DlDss3').prop('checked', true);
							toggleStatus('DlDss3','DlDss3_link','col_dav8');
                        } else {
                            $('#DlDss3').parent().removeClass('checked');
                           	$('#DlDss3').prop('checked', false);
                           	$('#col_dav8').css('height', '0px');
							$('#DlDss3_link').css('color', 'gray');
							$('#col_dav8').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_3dss_aqyear').val(data.s3d_year_survey);
                        $('#s2id_dav_3dss_numvin').select2('val', data.s3d_vintage_number);
                        $('#dav_3dss_binsize').val(data.s3d_bin_size);
                        $('#dav_3dss_tca').val(data.s3d_coverage_area);
                        $('#dav_3dss_dfrt1').val(data.s3d_frequency);
                        $('#dav_3dss_dfrt2').val(data.s3d_frequency_lateral);
                        $('#dav_3dss_dfrt3').val(data.s3d_frequency_vertical);
                        $('#dav_3dss_lpy').val(data.s3d_year_late_process);
                        $('#s2id_dav_3dss_lpm').select2('val', data.s3d_late_method);
                        $('#s2id_dav_3dss_siq').select2('val', data.s3d_img_quality);
                        $('#dav_3dss_trtd1').val(data.s3d_top_depth_ft);
                        $('#dav_3dss_trtd2').val(data.s3d_top_depth_ms);
                        $('#dav_3dss_brtd1').val(data.s3d_bot_depth_ft);
                        $('#dav_3dss_brtd2').val(data.s3d_top_depth_ms);
                        $('#dav_3dss_dcsp').val(data.s3d_depth_spill);
                        $('#s2id_dav_3dss_adwpo').select2('val', data.s3d_ability_offset);
                        $('#s2id_dav_3dss_ar3p').select2('val', data.s3d_ability_rock);
                        $('#s2id_dav_3dss_arrfp').select2('val', data.s3d_ability_fluid);
                        $('#dav_3dss_owc1').val(data.s3d_depth_estimate);
                        $('#dlosd8n2').val(data.s3d_depth_estimate_analog);
                        $('#dav_3dss_owc2').val(data.s3d_depth_estimate_spill);
                        $('#dav_3dss_oilgas1').val(data.s3d_depth_low);
                        $('#dlosd8o2').val(data.s3d_depth_low_analog);
                        $('#dav_3dss_oilgas2').val(data.s3d_depth_low_spill);
                        $('#dav_3dss_forthickness').val(data.s3d_formation_thickness);
                        $('#dav_3dss_grass_resthickness').val(data.s3d_gross_thickness);
                        $('#s2id_dav_3dss_antrip').select2('val', data.s3d_avail_pay);
                        $('#s3d_net_p90_thickness').val(data.s3d_net_p90_thickness);
                        $('#s3d_net_p90_vsh').val(data.s3d_net_p90_vsh);
                        $('#s3d_net_p90_por').val(data.s3d_net_p90_por);
                        $('#s3d_net_p90_satur').val(data.s3d_net_p90_satur);
                        $('#s3d_net_p50_thickness').val(data.s3d_net_p50_thickness);
                        $('#s3d_net_p50_vsh').val(data.s3d_net_p50_vsh);
                        $('#s3d_net_p50_por').val(data.s3d_net_p50_por);
                        $('#s3d_net_p50_satur').val(data.s3d_net_p50_satur);
                        $('#s3d_net_p10_thickness').val(data.s3d_net_p10_thickness);
                        $('#s3d_net_p10_vsh').val(data.s3d_net_p10_vsh);
                        $('#s3d_net_p10_por').val(data.s3d_net_p10_por);
                        $('#s3d_net_p10_satur').val(data.s3d_net_p10_satur);
                        $('#s3d_vol_p90_area').val(data.s3d_vol_p90_area);
                        $('#s3d_vol_p50_area').val(data.s3d_vol_p50_area);
                        $('#s3d_vol_p10_area').val(data.s3d_vol_p10_area);
                        $('#dav_3dss_remarks').val(data.s3d_remark);
                        $('#gcfsrock').select2('val', data.gcf_is_sr);
						$('#gcfsrock1').select2('val', data.gcf_sr_age_system);
						$('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
						$('#sourceformationname').select2('val', data.gcf_sr_formation_name_pre);
						$('#gcf_sr_formation_name_post').select2('val', data.gcf_sr_formation_name_post);
						$('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
						$('#gcfsrock4').select2('val', data.gcf_sr_toc);
						$('#gcfsrock5').select2('val', data.gcf_sr_hfu);
						$('#gcfsrock6').select2('val', data.gcf_sr_distribution);
						$('#gcfsrock7').select2('val', data.gcf_sr_continuity);
						$('#gcfsrock8').select2('val', data.gcf_sr_maturity);
						$('#gcfsrock9').select2('val', data.gcf_sr_otr);
						$('#gcfsrock10').val(data.gcf_sr_remark);
						$('#gcfres').select2('val', data.gcf_is_res);
						$('#gcfres1').select2('val', data.gcf_res_age_system);
						$('#gcfres1a').select2('val', data.gcf_res_age_serie);
						$('#reservoirformationname').select2('val', data.gcf_res_formation_name_pre);
						$('#gcf_res_formation_name_post').select2('val', data.gcf_res_formation_name_post);
						$('#gcfres3').select2('val', data.gcf_res_depos_set);
						$('#gcfres4').select2('val', data.gcf_res_depos_env);
						$('#gcfres5').select2('val', data.gcf_res_distribution);
						$('#gcfres6').select2('val', data.gcf_res_continuity);
						$('#gcfres7').select2('val', data.gcf_res_lithology);
						$('#gcfres8').select2('val', data.gcf_res_por_type);
						$('#gcfres9').select2('val', data.gcf_res_por_primary);
						$('#gcfres10').select2('val', data.gcf_res_por_secondary);
						$('#gcfres11').val(data.gcf_res_remark);
						$('#gcftrap').select2('val', data.gcf_is_trap);
						$('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
						$('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
						$('#sealingformationname').select2('val', data.gcf_trap_formation_name_pre);
						$('#gcf_trap_formation_name_post').select2('val', data.gcf_trap_formation_name_post);
						$('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
						$('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
						$('#gcftrap5').select2('val', data.gcf_trap_seal_type);
						$('#gcftrap6').select2('val', data.gcf_trap_age_system);
						$('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
						$('#gcftrap7').select2('val', data.gcf_trap_geometry);
						$('#gcftrap8').select2('val', data.gcf_trap_type);
						$('#gcftrap9').select2('val', data.gcf_trap_closure);
						$('#gcftrap10').val(data.gcf_trap_remark);
						$('#gcfdyn').select2('val', data.gcf_is_dyn);
						$('#gcfdyn1').select2('val', data.gcf_dyn_migration);
						$('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
						$('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
						$('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
						$('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
						$('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
						$('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
						$('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
						$('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
						$('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
						$('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
						$('#gcfdyn9').val(data.gcf_dyn_remark);
// 					}
	
                    disabledElement('w');
                    disabledElement('wz_1');
					disabledElement('wz_2');
					disabledElement('wz_3');
					disabledElement('wz_4');
					disabledElement('gcfsrock');
					disable('gcfsrock');
					disabledElement('gcfres');
					disable('gcfres');
					disabledElement('gcftrap');
					disable('gcftrap');
					disabledElement('gcfdyn');
                    disabledElement('gcfres8');
                   	generate_prospect_name();
                                		

					$('.zone_clastic_p10_thickness, .zone_clastic_p50_thickness, .zone_clastic_p90_thickness').bind('change keyup', function() {
						calculateClasticThickness();
					}).change();
						
					$('.zone_clastic_p10_gr, .zone_clastic_p50_gr, .zone_clastic_p90_gr').bind('change keyup', function() {
						calculateClasticGr();
					}).change();
						
					$('.zone_clastic_p10_sp, .zone_clastic_p50_sp, .zone_clastic_p90_sp').bind('change keyup', function() {
						calculateClasticSp();
					}).change();
						
					$('.zone_clastic_p10_net, .zone_clastic_p50_net, .zone_clastic_p90_net').bind('change keyup', function() {
						calculateClasticNet();
					}).change();
						
					$('.zone_clastic_p10_por, .zone_clastic_p50_por, .zone_clastic_p90_por').bind('change keyup', function() {
						calculateClasticPor();
					}).change();
						
					$('.zone_clastic_p10_satur, .zone_clastic_p50_satur, .zone_clastic_p90_satur').bind('change keyup', function() {
						calculateClasticSatur();
					}).change();
						
					$('.zone_carbo_p10_thickness, .zone_carbo_p50_thickness, .zone_carbo_p90_thickness').bind('change keyup', function() {
						calculateCarbonateThickness();
					}).change();
						
					$('.zone_carbo_p10_dtc, .zone_carbo_p50_dtc, .zone_carbo_p90_dtc').bind('change keyup', function() {
						calculateCarbonateDtc();
					}).change();
						
					$('.zone_carbo_p10_total, .zone_carbo_p50_total, .zone_carbo_p90_total').bind('change keyup', function() {
						calculateCarbonateTotal();
					}).change();
						
					$('.zone_carbo_p10_net, .zone_carbo_p50_net, .zone_carbo_p90_net').bind('change keyup', function() {
						calculateCarbonateNet();
					}).change();
						
					$('.zone_carbo_p10_por, .zone_carbo_p50_por, .zone_carbo_p90_por').bind('change keyup', function() {
						calculateCarbonatePor();
					}).change();
						
					$('.zone_carbo_p10_satur, .zone_carbo_p50_satur, .zone_carbo_p90_satur').bind('change keyup', function() {
						calculateCarbonateSatur();
					}).change();
						
					$('.zone_fvf_oil_p10, .zone_fvf_oil_p50, .zone_fvf_oil_p90').bind('change keyup', function() {
						calculateFluidVolumeOil();
					}).change();
						
					$('.zone_fvf_gas_p10, .zone_fvf_gas_p50, .zone_fvf_gas_p90').bind('change keyup', function() {
						calculateFluidVolumeGas();
					}).change();
						
					$('#zone_clastic_p90_por11, #zone_carbo_p90_por11').bind('change keyup', function() {
						setSurveyPor90();
						calculateSurveyP90();
					}).change();
						
					$('#zone_clastic_p50_por11, #zone_carbo_p50_por11').bind('change keyup', function() {
						setSurveyPor50();
						calculateSurveyP50();
					}).change();
						
					$('#zone_clastic_p10_por11, #zone_carbo_p10_por11').bind('change keyup', function() {
						setSurveyPor10();
						calculateSurveyP10();
					}).change();
						
					$('#zone_clastic_p90_satur11, #zone_carbo_p90_satur11').bind('change keyup', function() {
						setSurveySatur90();
						calculateSurveyP90();
					}).change();
						
					$('#zone_clastic_p50_satur11, #zone_carbo_p50_satur11').bind('change keyup', function() {
						setSurveySatur50();
						calculateSurveyP50();
					}).change();
						
					$('#zone_clastic_p10_satur11, #zone_carbo_p10_satur11').bind('change keyup', function() {
						setSurveySatur10();
						calculateSurveyP10();
					}).change();
						
					$('#s2d_net_p90_thickness, #s2d_net_p50_thickness, #s2d_net_p10_thickness, #dav_2dss_gross_resthickness').bind('change keyup', function() {
						calculate2dNetToGross();
						setThicknessP90();
						setThicknessP50();
						setThicknessP10();
						calculateSurveyP90();
						calculateSurveyP50();
						calculateSurveyP10();
					}).change();
					
					$('#s3d_net_p90_thickness, #s3d_net_p50_thickness, #s3d_net_p10_thickness, #dav_3dss_grass_resthickness').bind('change keyup', function() {
						calculate3dNetToGross();
						setThicknessP90();
						setThicknessP50();
						setThicknessP10();
						calculateSurveyP90();
						calculateSurveyP50();
						calculateSurveyP10();
					}).change();
						
					$('#sgf_p90_area, #sgf_p90_thickness, #sgf_p90_net').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#sgf_p50_area, #sgf_p50_thickness, #sgf_p50_net').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#sgf_p10_area, #sgf_p10_thickness, #sgf_p10_net').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
						
					$('#s2d_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#s2d_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#s2d_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
						
					$('#sgv_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#sgv_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#sgv_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
						
					$('#sgc_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#sgc_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#sgc_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
					
					$('#sel_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#sel_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#sel_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
						
					$('#rst_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#rst_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#rst_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
						
					$('#sor_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#sor_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#sor_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
						
					$('#s3d_vol_p90_area').bind('change keyup', function() {
						calculateSurveyP90();
					}).change();
						
					$('#s3d_vol_p50_area').bind('change keyup', function() {
						calculateSurveyP50();
					}).change();
						
					$('#s3d_vol_p10_area').bind('change keyup', function() {
						calculateSurveyP10();
					}).change();
                            		
//                     rock_clastic();
//                     rock_carbonate();
// 					fluid_volume();
//                     fluid_or_carbonate_p90_por1();
// 					fluid_or_carbonate_p50_por1();
// 					fluid_or_carbonate_p10_por1();
// 					fluid_or_carbonate_p90_satur1();
// 					fluid_or_carbonate_p50_satur1();
// 					fluid_or_carbonate_p10_satur1();
// 					geological_p90_oil_gas();
// 					geological_p50_oil_gas();
//                     geological_p10_oil_gas();
// 					all_net_pay();
// 					s2d_p90_oil_gas();
// 					s2d_p50_oil_gas();
// 					s2d_p10_oil_gas();
// 					gravity_p90_oil_gas();
// 					gravity_p50_oil_gas();
// 					gravity_p10_oil_gas();
// 					geochemistry_p90_oil_gas();
// 					geochemistry_p50_oil_gas();
// 					geochemistry_p10_oil_gas();
// 					electromagnetic_p90_oil_gas();
// 					electromagnetic_p50_oil_gas();
// 					electromagnetic_p10_oil_gas();
// 					resistivity_p90_oil_gas();
// 					resistivity_p50_oil_gas();
// 					resistivity_p10_oil_gas();
// 					other_p90_oil_gas();
// 					other_p50_oil_gas();
// 					other_p10_oil_gas();
// 					s3d_p90_oil_gas();
// 					s3d_p50_oil_gas();
//                     s3d_p10_oil_gas();
				},
			});
			return false;
		});
	");
?>

<script type="text/javascript">
/* Memanggil nilai gcf untuk ditampilkan */
var sr = ['gcfsrock3', 'gcfsrock4', 'gcfsrock5', 'gcfsrock8', 'gcfsrock9'];
var res = ['gcfres5', 'gcfres7', 'gcfres9', 'gcfres10'];
var trp = ['gcftrap3', 'gcftrap4', 'gcftrap5', 'gcftrap7', 'gcftrap8'];
var dyn = ['gcfdyn2', 'gcfdyn3', 'gcfdyn6', 'gcfdyn7'];

var gcfCategory = ''; 

$('.gcf').each(function() {
    $(this).bind('change', function() {
        if($.inArray($(this).attr("id"), sr) != -1)
            gcfCategory = '#gcfsrock';
        else if($.inArray($(this).attr("id"), res) != -1)
            gcfCategory = '#gcfres';
        else if($.inArray($(this).attr("id"), trp) != -1)
            gcfCategory = '#gcftrap';
        else
            gcfCategory = '#gcfdyn';

        $.ajax({
            url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
            type: 'POST',
            data: {
                "name": $(this).attr("id"),
                "choosen": $(this).val(),
                "from": "discovery",
                "category": $(gcfCategory).val() //category proven atau analog
            },
            dataType: 'json',
            error: function(xhr) {
                console.log(xhr.status);
            },
            success: function(e) {
                $(e.state).text(e.value);
            }
        });
    }).change();
});

$('#gcfsrock').bind('change', function() {
    if($("#gcfsrock").val() != '') {
        for(var loop = 0; loop < sr.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": sr[loop],
                    "choosen": $('#'+sr[loop]).val(),
                    "from": "discovery",
                    "category": $("#gcfsrock").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#gcfres').bind('change', function() {
    if($('#gcfres').val() != '') {
        for(var loop = 0; loop < res.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": res[loop],
                    "choosen": $('#'+res[loop]).val(),
                    "from": "discovery",
                    "category": $("#gcfres").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#gcftrap').bind('change', function() {
  if($('#gcftrap').val() != '') {
    for(var loop = 0; loop < trp.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": trp[loop],
          "choosen": $('#'+trp[loop]).val(),
          "from": "discovery",
          "category": $("#gcftrap").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();

$('#gcfdyn').bind('change', function() {
  if($('#gcfdyn').val() != '') {
    for(var loop = 0; loop < dyn.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": dyn[loop],
          "choosen": $('#'+dyn[loop]).val(),
          "from": "discovery",
          "category": $("#gcfdyn").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();
/* end Memanggil nilai gcf untuk ditampilkan */
</script>

<?php 
$cs  = $cs = Yii::app()->getClientScript();

$css = <<<EOD
    table.items {
		width: 100%;
	}

	#discovery-grid table td, #discovery-grid table th {
		text-align: left;
	}

EOD;

$cs->registerCss($this->id . 'css', $css);
?>
