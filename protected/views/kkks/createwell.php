<?php
/* @var $this SiteController */

$this->pageTitle='Well';
$this->breadcrumbs=array(
	'Well',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">WELL</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Well</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="well_page">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN LIST DATA Well -->
				<div id="_list-data-well" class="widget">
					<div class="widget-title">
						<h4><i class="icon-table"></i> LIST DATA WELL</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'well-grid',
							'dataProvider'=>$wellDataProvider,
							'htmlOptions'=>array('class'=>'table table-striped'),
							'columns'=>array(
								array(
									'header'=>'No.',
									'name'=>'No.',
									'value'=>'$row+1',
								),
								array(
									'name'=>'Type',
									'value'=>'$data->prospect_type',
								),
								array(
									'name'  => 'Well Name',
									'value' => '$data->wl_name',
								),
								array(
									'name'=>'Total Zone',
									'value'=>'Wellzone::model()->totalZone($data->wl_id)',
									'type'=>'raw'
								),
								array(
									'template'=>'{edit}',
									'class'=>'CButtonColumn',
									'buttons'=>array(
										'edit'=>array(
											'options'=>array(
													'class'=>'icon-edit',
											),
											'url'=>'($data->prospect_type == "Postdrill") ? Yii::app()->createUrl("/kkks/updatewellpostdrill", array("id"=>$data->wl_id)) : Yii::app()->createUrl("/kkks/updatewelldiscovery", array("id"=>$data->wl_id))',
										),
									),
								),
							),
						));?>
						
					</div>
				</div>
				<!-- END LIST DATA WELL -->
			</div>
		</div>
		
		<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'createwell-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnChange'=>false,
				),
				'enableAjaxValidation'=>true,
		));?>
		<div id="pesan"></div>
		<div class="row-fluid">
			<div class="span12">
				<div class="widget">
					<div class="widget-title">
                        <h4><i class="icon-list"></i> WELL DATA AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                    	<span action="#" class="form-horizontal">
                    		<div class="control-group">
	                            <div class="alert alert-success">
	                                <button class="close" data-dismiss="alert">×</button>
	                                <p>P10 = Probability to recover HC accumulation with certain volume or greater than expected is 10%</p>
	                                <p>P50 = Probability to recover HC accumulation with certain volume or greater than expected is 50%</p>
	                                <p>P90 = Probability to recover HC accumulation with certain volume or greater than expected is 90%</p>
	                            </div>
	                        </div>
                        	<div class="control-group">
                        		<label class="control-label">Prospect Type :</label>
                                <div class="controls wajib">
                                    <!-- <input id="w" class="span3" type="text" style="text-align: center;"/> -->
                                    <?php echo $form->textField($mdlWell, 'prospect_type', array('id'=>"prospect_type", 'class'=>"span3", 'style'=>"text-align: center; position: relative; display: inline-block;"));?>
                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                    <div id="Well_prospect_type_em_" class="errorMessage" style="display:none">
                                    </div>
                                </div>
                        	</div>	                    	
	                        <!-- render partial will be here -->
                        	<div id="render-partial">
	                        	
							</div>
							<!-- render partial until here -->
						</span>
                    </div>
				</div>
			</div>
		</div>
        
        <?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
        <div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
        <?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/createwell'), array(
        	'type'=>'POST',
        	'dataType'=>'json',
        	'beforeSend'=>'function(data) {
        		$(".tooltips").attr("data-original-title", "");
        		
        		$(".has-err").removeClass("has-err");
        		$(".errorMessage").hide();
        	
                $("#yt0").prop("disabled", true);
        		if($("#prospect_type").val() == "") {
        			$("#Well_prospect_type_em_").text("Prospect Type cannot be blank.");                                                    
					$("#Well_prospect_type_em_").show();
	        		$("#Well_prospect_type_em_").parent().addClass("has-err");
        			$("#pesan").hide();
        			data.abort();
        			
        		}
        	}',
        	'success'=>'js:function(data) {
        		
        		
        		if(data.result === "success") {
        			$(".close").addClass("redirect");
        			$("#message").html(data.msg);
        			$("#popup").modal("show");
        			$("#pesan").hide();
        			$.fn.yiiGridView.update("well-grid");
        			$(".redirect").click( function () {
						var redirect = "' . Yii::app()->createUrl('/Kkks/createwell') . '";
						window.location=redirect;
					});
                    $("#yt0").prop("disabled", false);
        		} else {
        			var myArray = JSON.parse(data.err);
        			$.each(myArray, function(key, val) {
        				if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
		        		{
        					$("#createwell-form #"+key+"_em_").parent().addClass("has-err");
        					$("#createwell-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
        					
		        		} else {
        					$("#createwell-form #"+key+"_em_").text(val);                                                    
							$("#createwell-form #"+key+"_em_").show();
	        				$("#createwell-form #"+key+"_em_").parent().addClass("has-err");
        				}
        				
        			});
        		
        			$("#message").html(data.msg);
        			$("#popup").modal("show");
                    $("#yt0").prop("disabled", false);
        		}
        	}',
        ),
        array('class'=>'btn btn-inverse')
        );?>
        </div>
        <?php //echo CHtml::button('Get', array('onclick'=>'{getPreviousDataPlay();}'));?>
        <?php $this->endWidget();?>
        
        <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
		  	</div>
		  	<div class="modal-body">
		    	<p id="message"></p>
		  	</div>
		</div>
		<!-- end popup submit -->
	</div>
	<!-- END PAGE CONTENT-->
</div>

<script type="text/javascript">

$('#prospect_type').on('change', function() {
	$('#render-partial').empty();
	if($('#prospect_type').val() != '') {
		changeWell();
	}
});

function changeWell()
{
	var prospect_type_value = $('#prospect_type').val();
		$.ajax({
			url : '<?php echo Yii::app()->createUrl('/Kkks/ajaxrequestwell') ?>' ,
			data : {
				prospect_type : prospect_type_value,
			},
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				$("#render-partial").html(data.div);
				
			},
		});
}
</script>

<?php 
$cs  = $cs = Yii::app()->getClientScript();

$css = <<<EOD
    table.items {
		width: 100%;
	}

EOD;

$cs->registerCss($this->id . 'css', $css);
?>
