<?php
/* @var $this SiteController */

$this->pageTitle='Report Form & Instruction Manual';
$this->breadcrumbs=array(
    'Form &amp; Manual',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE &amp; BREADCRUMB-->                           
            <h3 class="page-title">Report Form &amp; Instruction Manual</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Form & Manual</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- BEGIN PAGE CONTENT-->
<div id="page">
  <div class="row-fluid">
      <div class="span12">
        <div id="_list-data-well" class="widget">
          <div class="widget-body">
            <span action="#" class="form-horizontal">
              <center><h3>Daftar File Format Pelaporan & Manual Pengisian Dinas Pengkajian Eksplorasi</h3></center>              
              
              <?php $doc_path = Yii::app()->request->baseUrl . '/mandoc/'; ?>

              <div class="accordion" id="accordion1">
                <div class="accordion-group">
                  <div class="accordion-heading">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#col_gen1">
                          <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Revitalisasi Pelaporan Sumberdaya</strong>
                      </a>
                  </div>
                  <div id="col_gen1" class="accordion-body in collapse">
                    <div class="accordion-inner">
                      <a href="<?php echo $doc_path . 'rps/manual_rps_2014.pdf'; ?>"><button class="btn btn-block" style="text-align: left;">Manual RPS 2014</button></a><br/>
                      <a href="<?php echo $doc_path . 'rps/keterangan_umum_rps.pdf'; ?>"><button class="btn btn-block" style="text-align: left;">Keterangan Umum RPS</button></a>
                    </div>
                  </div>
                </div>

                <div class="accordion-group">
                  <div class="accordion-heading">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#col_gen2">
                          <span class="add-on"><i class="icon-list-alt"></i></span><strong> Form Usulan Study (Study Plan)</strong>
                      </a>
                  </div>
                  <div id="col_gen2" class="accordion-body collapse">
                    <div class="accordion-inner">
                      <a href="<?php echo $doc_path . 'study_plan/Template_PE-WK_XXX-Tahun_XXX-AFE_Number.xlsx'; ?>"><button class="btn btn-block" style="text-align: left;"> Template Project Evaluation (PE)</button></a>
                    </div>
                  </div>
                </div>

                <div class="accordion-group">
                  <div class="accordion-heading">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#col_gen3">
                          <span class="add-on"><i class="icon-list-alt"></i></span><strong> Form IT GGR</strong>
                      </a>
                  </div>
                  <div id="col_gen3" class="accordion-body collapse">
                    <div class="accordion-inner">
                      <a href="<?php echo $doc_path . 'it_ggr/Template_IT_GGR_Software_Plan_(Blank).xlsx'; ?>"><button class="btn btn-block" style="text-align: left;"> Template IT GGR Software Plan (Blank Form)</button></a><br/>
                      <a href="<?php echo $doc_path . 'it_ggr/Template_IT_GGR_Software_Plan_(Contoh).xlsx'; ?>"><button class="btn btn-block" style="text-align: left;"> Template IT GGR Software Plan (Contoh Pengisian)</button></a>
                    </div>
                  </div>
                </div>

                <div class="accordion-group">
                  <div class="accordion-heading">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#col_gen4">
                          <span class="add-on"><i class="icon-list-alt"></i></span><strong> Form Monitoring Study &amp; IT GGR</strong>
                      </a>
                  </div>
                  <div id="col_gen4" class="accordion-body collapse">
                    <div class="accordion-inner">
                      <a href="<?php echo $doc_path . 'monitoring/Template_Monitoring_Studi_EKS-[KKKS Name]-[WK Name].pptx'; ?>"><button class="btn btn-block" style="text-align: left;"> Template Monitoring Studi EKS</button></a><br/> 
                      <a href="<?php echo $doc_path . 'monitoring/Template_Monitoring_Studi_EPT-[KKKS Name]-[WK Name].pptx'; ?>"><button class="btn btn-block" style="text-align: left;"> Template Monitoring Studi EPT</button></a>
                    </div>
                  </div>
                </div>

                <div class="accordion-group">
                  <div class="accordion-heading">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#col_gen5">
                          <span class="add-on"><i class="icon-list-alt"></i></span><strong> Manual &amp; Presentasi</strong>
                      </a>
                  </div>
                  <div id="col_gen5" class="accordion-body collapse">
                    <div class="accordion-inner">
                      <a href="<?php echo $doc_path . 'manual_presentasi/Manual_Pengisian_Monitoring_Studi.pdf'; ?>">
                        <button class="btn btn-block" style="text-align: left;">Manual Pengisian Monitoring Studi</button>
                      </a><br/>

                      <a href="<?php echo $doc_path . 'manual_presentasi/Sosialisasi_Pelaporan_Studi.pdf'; ?>">
                        <button class="btn btn-block" style="text-align: left;">Sosialisai Pelaporan Studi</button>
                      </a><br/>

                      <a href="<?php echo $doc_path . 'manual_presentasi/Sosialisasi_Studi_Plan-IT_GGR_PLAN-Monitoring_Studi-Subsurface_Closed_Out_P3.pdf'; ?>"> 
                        <button class="btn btn-block" style="text-align: left;">Sosialisasi Studi Plan &amp; IT GGR Plan &amp; Monitoring Studi Plan &amp; Subsurface Closed Out</button>
                      </a><br/>
                      
                      <a href="<?php echo $doc_path . 'manual_presentasi/Sosialisasi_RPS_2014.pdf'; ?>"> 
                        <button class="btn btn-block" style="text-align: left;">Sosialisasi RPS 2014</button>
                      </a><br/>

                      <a href="<?php echo $doc_path . 'manual_presentasi/Sosialisasi_RPS_2015.pdf'; ?>"> 
                        <button class="btn btn-block" style="text-align: left;">Sosialisasi RPS 2015</button>
                      </a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </span>
        </div>
      </div>
    </div>
</div>