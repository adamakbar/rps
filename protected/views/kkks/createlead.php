<?php
/* @var $this SiteController */

$this->pageTitle='Lead';
$this->breadcrumbs=array(
    'Lead',
);
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">                    
            <h3 class="page-title">LEAD</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Resources</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Lead</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
        </div>
    </div>
    <!-- BEGIN PAGE CONTENT-->
    <div id="page">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN LIST DATA LEAD -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-table"></i> LIST DATA LEAD</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <?php $this->widget('zii.widgets.grid.CGridView', array(
                            'id'=>'lead-grid',
                            'dataProvider'=>$model->searchRelated(),
                            'filter'=>$model,
                            'htmlOptions'=>array('class'=>'table table-striped'),
                            'columns'=>array(
                                array(
                                    'header'=>'No.',
                                    'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                                ),
                                array(
                                    'header'=>'Basin',
                                    'filter'=>CHtml::activeTextField($model, 'basin_name'),
                                    'value'=>'$data->plays->basin->basin_name',
                                ),
                                array(
                                    'header'=>'Lead Name / Closure Name',
                                    'filter'=>CHtml::activeTextField($model, 'lead_name'),
                                    //'value'=>'CHtml::link($data->structure_name, array("GetPreviousDataLead", "id"=>$data->lead_id), array("class"=>"ajaxupdate"))',
                                    'value'=>'$data->getLeadName($data->gcf_id)',
                                    'type'=>'raw',
                                ),
                                array(
                                    'header'=>'Status',
                                    'value'=>'$data->lead_submit_status',
                                ),
                                array(
                                    'header'=>'Actions',
                                    'template'=>'{edit} {delete }',
                                    'class'=>'CButtonColumn',
                                    'buttons'=>array(
                                        'edit'=>array(
                                            'options'=>array(
                                                'class'=>'icon-edit',
                                            ),
                                            'url'=>'Yii::app()->createUrl("/kkks/updatelead", array("id"=>$data->lead_id))',
                                        ),
										'delete '=>array(
											'options'=>array(
												'class'=>'icon-remove'
											),
											'url'=>'Yii::app()->createUrl("/kkks/deletelead", array("id"=>$data->lead_id))',
											'click'=>"function() {
												$.fn.yiiGridView.update('lead-grid', {  //change my-grid to your grid's name
											        type:'POST',
											        url:$(this).attr('href'),
											        success:function(data) {
											              $.fn.yiiGridView.update('lead-grid'); //change my-grid to your grid's name
											        }
											    })
											    return false;
											  }
											",
										),
                                    ),
                                ),
                            ),
                        ))?>
                    </div>
                </div>
                <!-- END LIST DATA LEAD -->
            </div>
        </div>
        
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'createlead-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange'=>false,
            ),
            'enableAjaxValidation'=>true,
        ));?>
        <div id="pesan"></div>
        <?php //echo $form->errorSummary(array($mdlLead, $mdlLeadGeological, $mdlLead2d, $mdlLeadGravity, $mdlLeadGeochemistry, $mdlLeadElectromagnetic, $mdlLeadResistivity, $mdlLeadOther, $mdlGcf));?>
        <?php echo $form->hiddenField($mdlLead, 'lead_update_from', array('id'=>'lead_update_from'));?>
        <?php echo $form->hiddenField($mdlLead, 'lead_submit_revision', array('id'=>'lead_submit_revision'));?>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> GENERAL DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <div class="accordion" id="accordion1a">
                                <!-- Begin Lead Administration Data -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Lead Administration Data</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                          <div class="alert alert-success">
                                            Structure name must be unique and meaningful, name that contain "Lead",
                                            "Drillable", "Discovery" or other
                                            general term is not permitted.
                                          </div>
                                            <div class="control-group">
                                                <label class="control-label">Play Name :</label>
                                                <div class="controls wajib">
                                                    <?php 
//                                                      $mdlPlaySearch = Play::model();
//                                                      $model = new Play('search');
//                                                      $model->unsetAttributes();
                                                        
//                                                      $criteria = new CDbCriteria;
//                                                      $criteria->select="max(b.play_submit_date)";
//                                                      $criteria->alias = 'b';
//                                                      $criteria->condition="b.play_update_from = t.play_update_from";
//                                                      $subQuery=$model->getCommandBuilder()->createFindCommand($model->getTableSchema(),$criteria)->getText();
                                                        
//                                                      $mainCriteria = new CDbCriteria;
//                                                      $mainCriteria->with = array('gcfs');
//                                                      $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
//                                                      $mainCriteria->addCondition('play_submit_date = (' . $subQuery . ')', 'AND');
//                                                      $mainCriteria->group = 'play_update_from';
                                                    
                                                    ?>
                                                    <!-- <input type="hidden" id="play_in" class="span6" style="text-align: center;"/> -->
                                                    <?php //echo $form->dropDownList($mdlLead, 'play_id', CHtml::listData( Play::model()->findAll($mainCriteria), 'play_id', 'gcfs.playName' ), array('empty'=>'-- Select --', 'style'=>'text-align: center;', 'id'=>'plyName', 'class'=>'span6'));?>
                                                    <?php echo $form->textField($mdlLead, 'play_id', array('style'=>'text-align: center; position: relative; display: inline-block;', 'id'=>'plyName', 'class'=>'span6'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead, 'play_id');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Structure Name :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <!-- <input id="structure_name" type="text" class="span6"/> -->
                                                                <?php echo $form->textField($mdlLead, 'structure_name', array('id'=>'structure_name', 'class'=>'span6', 'style'=>'width: 375px;'));?>
                                                                <?php echo $form->error($mdlLead, 'structure_name');?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lead Name / Closure Name :</label>
                                                <div class="controls">
                                                    <div class="alert alert-success span12" style="width: 375px;">
                                                        <span id="additional_lead_name" class="add-on"></span>
                                                    </div>
                                                </div>
                                            </div>                     
                                            <div class="control-group">
                                                <label class="control-label">Clarified by :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="_clarifiedby" class="span3" type="text" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlLead, 'lead_clarified', array('id'=>'_clarifiedby', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead, 'lead_clarified');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lead Name Initiation Date :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input id="l5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Lead Name Initiation Date"/> -->
                                                        <?php echo $form->textField($mdlLead, 'lead_date_initiate', array('id'=>'l5', 'class'=>'m-wrap medium popovers disable-input', 'style'=>'max-width:138px;', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'If Year that only available, please choose 1-January for Day and Month, if not leave it blank.', 'data-original-title'=>'Lead Name Initiation Date'));?>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                        <span id="clearl5" class="add-on" style="cursor: pointer;" onclick="clearDate(this.id)"><i class="icon-remove"></i></span>
                                                    </div>
                                                    <?php echo $form->error($mdlLead, 'lead_date_initiate');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Lead Administration Data -->
                                <!-- Begin Geographical Lead Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2_lead">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geographical Lead Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2_lead" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info!</strong> For one Lead structure, please fill with exact same coordinate or with difference of 25'' with other Lead of one structure
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <label class="control-label">Center Latitude :</label>
                                                <div class="controls wajib">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <span class="wajib">
                                                                    <div class=" input-append">
                                                                        <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
                                                                        <?php echo $form->textField($mdlLead, 'center_lat_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                                        <span class="add-on">&#176;</span>
                                                                    </div>
                                                                    <?php echo $form->error($mdlLead, 'center_lat_degree');?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span class="wajib">
                                                                    <div class=" input-append">
                                                                        <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
                                                                        <?php echo $form->textField($mdlLead, 'center_lat_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
                                                                        <span class="add-on">'</span>
                                                                    </div>
                                                                    <?php echo $form->error($mdlLead, 'center_lat_minute');?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span class="wajib">
                                                                    <div class=" input-append">
                                                                        <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div> -->
                                                                        <?php echo $form->textField($mdlLead, 'center_lat_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
                                                                        <span class="add-on">"</span>
                                                                    </div>
                                                                    <?php echo $form->error($mdlLead, 'center_lat_second');?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span class="wajib">
                                                                    <div>
                                                                        <!-- <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"></span> -->
                                                                        <?php echo $form->textField($mdlLead, 'center_lat_direction', array('class'=>'input-mini directionlat tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'S/ N', 'style'=>'margin-left: 24px;'));?>
                                                                    </div>
                                                                    <?php echo $form->error($mdlLead, 'center_lat_direction');?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-group">
                                                    <label class="control-label">Center Longitude :</label>
                                                    <div class="controls wajib">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <span class="wajib">
                                                                        <div class=" input-append">
                                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div> -->
                                                                            <?php echo $form->textField($mdlLead, 'center_long_degree', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'degree'));?>
                                                                            <span class="add-on">&#176;</span>
                                                                        </div>
                                                                        <?php echo $form->error($mdlLead, 'center_long_degree');?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="wajib">
                                                                        <div class=" input-append">
                                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div> -->
                                                                            <?php echo $form->textField($mdlLead, 'center_long_minute', array('class'=>'input-mini number-only tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'minute', 'style'=>'margin-left: 24px;'));?>
                                                                            <span class="add-on">'</span>
                                                                        </div>
                                                                        <?php echo $form->error($mdlLead, 'center_long_minute');?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="wajib">
                                                                        <div class=" input-append">
                                                                            <!-- <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div> -->
                                                                            <?php echo $form->textField($mdlLead, 'center_long_second', array('class'=>'input-mini number tooltips', 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'second', 'style'=>'margin-left: 24px;'));?>
                                                                            <span class="add-on">"</span>
                                                                        </div>
                                                                        <?php echo $form->error($mdlLead, 'center_long_second');?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <span class="wajib">
                                                                        <div>
                                                                            <!-- <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on"></span> -->
                                                                            <?php echo $form->textField($mdlLead, 'center_long_direction', array('value'=>'E', 'class'=>'input-mini directionlong tooltips', 'readonly'=>true, 'data-trigger'=>'hover', 'data-original-title'=>'', 'data-container'=>'body', 'placeholder'=>'E/ W', 'style'=>'margin-left: 24px;'));?>
                                                                        </div>
                                                                        <?php echo $form->error($mdlLead, 'center_long_direction');?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <div class=" input-append" style='margin-left: 24px;'><span class="add-on">Datum WGS '84</span></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Geographical Lead Area -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Onshore or Offshore :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLead, 'lead_shore', array('id'=>'env_onoffshore', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead, 'lead_shore');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Terrain :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLead, 'lead_terrain', array('id'=>'env_terrain', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead, 'lead_terrain');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Field :</label>
                                                <div class="controls wajib">
                                                    <p style="margin-bottom: 2px;">
                                                    <!-- <input id="n_facility" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/> -->
                                                    <?php echo $form->textField($mdlLead, 'lead_near_field', array('id'=>'n_facility', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlLead, 'lead_near_field');?>
                                                </div>
                                            </div>                    
                                            <div class="control-group">
                                                <label class="control-label">Nearby Infra Structure:</label>
                                                <div class="controls wajib">
                                                    <p style="margin-bottom: 2px;">
                                                    <!-- <input id="n_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/> -->
                                                    <?php echo $form->textField($mdlLead, 'lead_near_infra_structure', array('id'=>'n_developmentwell', 'class'=>'span3', 'style'=>'text-align: center; max-width:148px; position: relative; display: inline-block;'));?>
                                                    <span class="addkm">Km</span>
                                                    </p>
                                                    <?php echo $form->error($mdlLead, 'lead_near_infra_structure');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN DATA AVAILABILITY -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-paper-clip"></i> SURVEY DATA AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <div class="control-group">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert">×</button>
                                <p>Low Estimate = Probability of HC accumulation spread out with certain extents or greater than expected is 10%</p>
                                <p>Best Estimate = Probability of HC accumulation spread out with certain extents or greater than expected is 50%</p>
                                <p>High Estimate = Probability of HC accumulation spread out with certain extents or greater than expected is 90%</p>
                            </div>
                        </div>
                        <span action="#" class="form-horizontal">                                                       
                            <div class="accordion">
                                <div class="accordion-group">
                                    <div class="accordion-heading"><div class="geser" ></div>
                                        <!-- <input id="LGfs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LGfs','LGfs_link','col_dav1')" /> -->
                                        <?php echo $form->checkBox($mdlLeadGeological, 'is_checked', array('id'=>'LGfs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LGfs","LGfs_link","col_dav1")'));?> 
                                        <a id="LGfs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">Geological Field Survey</a>
                                    </div>
                                    <div id="col_dav1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_gfs_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadGeological, 'lsgf_year_survey', array('id'=>'dav_gfs_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadGeological, 'lsgf_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Method :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_gfs_surmethod" class="span3" style="text-align: center;"/> -->
                                                    <?php echo $form->textField($mdlLeadGeological, 'lsgf_survey_method', array('id'=>'dav_gfs_surmethod', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLeadGeological, 'lsgf_survey_method');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Total Coverage Area : </label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_gfs_tca" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeological, 'lsgf_coverage_area', array('id'=>'dav_gfs_tca', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeological, 'lsgf_coverage_area');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_gfs_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeological, 'lsgf_low_estimate', array('id'=>'dav_gfs_lowes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeological, 'lsgf_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_gfs_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeological, 'lsgf_best_estimate', array('id'=>'dav_gfs_bestes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeological, 'lsgf_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_gfs_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeological, 'lsgf_high_estimate', array('id'=>'dav_gfs_highes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeological, 'lsgf_high_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong> </strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_gfs_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLeadGeological, 'lsgf_remark', array('id'=>'dav_gfs_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <!-- <input id="LDss" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LDss','LDss_link','col_dav2' )"/> --> 
                                            <?php echo $form->checkBox($mdlLead2d, 'is_checked', array('id'=>'LDss', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LDss","LDss_link","col_dav2")'));?>
                                            <a id="LDss_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2"> 2D Seismic Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav2" class="accordion-body collapse" >
                                        <div class="accordion-inner">                                   
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year:</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_2dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLead2d, 'ls2d_year_survey', array('id'=>'dav_2dss_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Number Of Vintage :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_2dss_numvin" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLead2d, 'ls2d_vintage_number', array('id'=>'dav_2dss_numvin', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_vintage_number');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Total Crossline Number :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_2dss_tcn" type="text" class="span3"/> -->
                                                    <?php echo $form->textField($mdlLead2d, 'ls2d_total_crossline', array('id'=>'dav_2dss_tcn', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_total_crossline');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Total Coverage Area :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_2dss_tca" type="text" style="max-width:168px"/> -->
                                                        <?php echo $form->textField($mdlLead2d, 'ls2d_total_coverage', array('id'=>'dav_2dss_tca', 'class'=>'number', 'style'=>'max-width:130px'));?>
                                                        <span class="add-on">km<sup>2</sup></span>
                                                    </div>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_total_coverage');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Average Spacing Parallel Interval :</label>
                                                <div class="controls wajib">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_2dss_aspi" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLead2d, 'ls2d_average_interval', array('id'=>'dav_2dss_aspi', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">km</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_average_interval');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Latest Processing Year :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_2dss_lpy" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Only latest processing year" data-original-title="Latest Processing Year"/> -->
                                                    <?php echo $form->textField($mdlLead2d, 'ls2d_year_late_process', array('id'=>'dav_2dss_lpy', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Only latest processing year', 'data-original-title'=>'Latest Processing Year'));?>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_year_late_process');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Latest Processing Method :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_2dss_lpm" style="text-align: center;" class="span3"/> -->
                                                    <?php echo $form->textField($mdlLead2d, 'ls2d_late_method', array('id'=>'dav_2dss_lpm', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_late_method');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Seismic Image Quality :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_2dss_siq" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLead2d, 'ls2d_img_quality', array('id'=>'dav_2dss_siq', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_img_quality');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_2dss_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLead2d, 'ls2d_low_estimate', array('id'=>'dav_2dss_lowes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_2dss_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLead2d, 'ls2d_best_estimate', array('id'=>'dav_2dss_bestes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_2dss_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLead2d, 'ls2d_hight_estimate', array('id'=>'dav_2dss_highes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLead2d, 'ls2d_hight_estimate');?>
                                                </div>
                                            </div>                                           
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_2dss_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLead2d, 'ls2d_remark', array('id'=>'dav_2dss_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <!-- <input id="LGras" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LGras','LGras_link','col_dav3' )"/> -->
                                            <?php echo $form->checkBox($mdlLeadGravity, 'is_checked', array('id'=>'LGras', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LGras","LGras_link","col_dav3")'));?> 
                                            <a id="LGras_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3"> Gravity Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year:</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_grasur_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadGravity, 'lsgv_year_survey', array('id'=>'dav_grasur_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Methods :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_grasur_surveymethods" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLeadGravity, 'lsgv_survey_method', array('id'=>'dav_grasur_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_survey_method');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Coverage Area :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_grasur_sca" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadGravity, 'lsgv_coverage_area', array('id'=>'dav_grasur_sca', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_coverage_area');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depth Survey Penetration Range:</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_grasur_dspr" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadGravity, 'lsgv_range_penetration', array('id'=>'dav_grasur_dspr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_range_penetration');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Recorder Spacing Interval :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_grasur_rsi" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadGravity, 'lsgv_spacing_interval', array('id'=>'dav_grasur_rsi', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_spacing_interval');?>
                                                </div>
                                            </div>                                            
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_grasur_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGravity, 'lsgv_low_estimate', array('id'=>'dav_grasur_lowes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_grasur_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGravity, 'lsgv_best_estimate', array('id'=>'dav_grasur_bestes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_grasur_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGravity, 'lsgv_high_estimate', array('id'=>'dav_grasur_highes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGravity, 'lsgv_high_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_grasur_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLeadGravity, 'lsgv_remark', array('id'=>'dav_grasur_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                              
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <!-- <input id="LGeos" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LGeos','LGeos_link','col_dav4' )"/> -->
                                            <?php echo $form->checkBox($mdlLeadGeochemistry, 'is_checked', array('id'=>'LGeos', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LGeos","LGeos_link","col_dav4")'));?> 
                                            <a id="LGeos_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4"> Geochemistry Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_geo_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_year_survey', array('id'=>'dav_geo_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Interval Samples Range :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_geo_isr" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_range_interval', array('id'=>'dav_geo_isr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_range_interval');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Number of Sample Location :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_geo_numsample" type="text" class="span3"/> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_number_sample', array('id'=>'dav_geo_numsample', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_number_sample');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Number of Rocks Sample :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_geo_numrock" type="text" class="span3"/> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_number_rock', array('id'=>'dav_geo_numrock', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_number_rock');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Number of Fluid Sample :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_geo_numfluid" type="text" class="span3"/> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_number_fluid', array('id'=>'dav_geo_numfluid', 'class'=>'span3 number'));?>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_number_fluid');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Availability HC Composition :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_geo_ahc" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_hc_availability', array('id'=>'dav_geo_ahc', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_hc_availability');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Hydrocarbon Composition : </label>
                                                <div class="controls">
                                                    <!-- <input id="dav_geo_hycomp" type="text" class="span3"/> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_hc_composition', array('id'=>'dav_geo_hycomp', 'class'=>'span3'));?>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_hc_composition');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Laboratorium Evaluation & Report Year</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_geo_lery" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_year_report', array('id'=>'dav_geo_lery', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_year_report');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_geo_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_low_estimate', array('id'=>'dav_geo_lowes', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_geo_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_best_estimate', array('id'=>'dav_geo_bestes', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_geo_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadGeochemistry, 'lsgc_high_estimate', array('id'=>'dav_geo_highes', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadGeochemistry, 'lsgc_high_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_geo_m" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLeadGeochemistry,'lsgc_remark', array('id'=>'dav_geo_m', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <!-- <input id="LEs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LEs','LEs_link','col_dav5' )"/> -->
                                            <?php echo $form->checkBox($mdlLeadElectromagnetic, 'is_checked', array('id'=>'LEs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LEs","LEs_link","col_dav5")'));?> 
                                            <a id="LEs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav5"> Electromagnetic Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav5" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year:</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_elec_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_year_survey', array('id'=>'dav_elec_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Methods :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_elec_surveymethods" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_survey_method', array('id'=>'dav_elec_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_survey_method');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Coverage Area :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_elec_sca" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_coverage_area', array('id'=>'dav_elec_sca', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_coverage_area');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depth Survey Penetration Range :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_elec_dspr" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_range_penetration', array('id'=>'dav_elec_dspr', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_range_penetration');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Recorder Spacing Interval :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_elec_rsi" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_spacing_interval', array('id'=>'dav_elec_rsi', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_spacing_interval');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_elec_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_low_estimate', array('id'=>'dav_elec_lowes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_elec_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_best_estimate', array('id'=>'dav_elec_bestes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_elec_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadElectromagnetic, 'lsel_high_estimate', array('id'=>'dav_elec_highes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadElectromagnetic, 'lsel_high_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_elec_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLeadElectromagnetic, 'lsel_remark', array('id'=>'dav_elec_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <!-- <input id="LRs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LRs','LRs_link','col_dav6' )"/> -->
                                            <?php echo $form->checkBox($mdlLeadResistivity, 'is_checked', array('id'=>'LRs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LRs","LRs_link","col_dav6")'));?> 
                                            <a id="LRs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav6"> Resistivity Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav6" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year:</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_res_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadResistivity, 'lsrt_year_survey', array('id'=>'dav_res_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Methods :</label>
                                                <div class="controls">
                                                    <!-- <input id="dav_res_surveymethods" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlLeadResistivity, 'lsrt_survey_method', array('id'=>'dav_res_surveymethods', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_survey_method');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Survey Coverage Area :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_res_sca" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadResistivity, 'lsrt_coverage_area', array('id'=>'dav_res_sca', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_coverage_area');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depth Survey Penetration Range :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_res_dspr" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadResistivity, 'lsrt_range_penetration', array('id'=>'dav_res_dspr', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_range_penetration');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Recorder Spacing Interval :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_res_rsi" type="text" style="max-width:170px;"/> -->
                                                        <?php echo $form->textField($mdlLeadResistivity, 'lsrt_spacing_interval', array('id'=>'dav_res_rsi', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">ft</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_spacing_interval');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_res_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadResistivity, 'lsrt_low_estimate', array('id'=>'dav_res_lowes', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_res_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadResistivity, 'lsrt_best_estimate', array('id'=>'dav_res_bestes', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_res_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadResistivity, 'lsrt_high_estimate', array('id'=>'dav_res_highes', 'class'=>'number', 'style'=>'max-width:137px;'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadResistivity, 'lsrt_high_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_res_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLeadResistivity, 'lsrt_remark', array('id'=>'dav_res_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <!-- <input id="LOs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LOs','LOs_link','col_dav7' )"/> -->
                                            <?php echo $form->checkBox($mdlLeadOther, 'is_checked', array('id'=>'LOs', 'class'=>'cekbok', 'onclick'=>'toggleStatus("LOs","LOs_link","col_dav7")'));?> 
                                            <a id="LOs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav7"> Other/ Others Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav7" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info!</strong> Please provide more information about other survey in remark.
                                                </div>
                                            </div>
                                            <!-- Notification --> 
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year:</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="dav_other_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/> -->
                                                    <?php echo $form->textField($mdlLeadOther, 'lsor_year_survey', array('id'=>'dav_other_aqyear', 'class'=>'span3 popovers number-comma', 'data-trigger'=>'hover', 'data-container'=>'body', 'data-content'=>'Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012.', 'data-original-title'=>'Acquisition Year'));?>
                                                    <?php echo $form->error($mdlLeadOther, 'lsor_year_survey');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <!-- <input id="dav_other_lowes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadOther, 'lsor_low_estimate', array('id'=>'dav_other_lowes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadOther, 'lsor_low_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_other_bestes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadOther, 'lsor_best_estimate', array('id'=>'dav_other_bestes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadOther, 'lsor_best_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <!-- <input id="dav_other_highes" name="custom" type="text" style="max-width:170px"/> -->
                                                        <?php echo $form->textField($mdlLeadOther, 'lsor_high_estimate', array('id'=>'dav_other_highes', 'class'=>'number', 'style'=>'max-width:137px'));?>
                                                        <span class="add-on">acre</span>
                                                    </div>
                                                    <?php echo $form->error($mdlLeadOther, 'lsor_high_estimate');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="dav_other_remarks" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlLeadOther, 'lsor_remark', array('id'=>'dav_other_remarks', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                              
                           </div>                        
                        </span>
                    </div>
                </div>
                <!-- END DATA AVAILABILITY -->                
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN LEAD GCF -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> DESCRIPTION LEAD GEOLOGICAL CHANCE FACTOR</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <span action="#" class="form-horizontal">
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">×</button>
                                    <strong>Lead Geological Chance Factor</strong>
                                    <p>When Play chosen this Geological Chance Factor automatically filled with Play Geological Chance Factor, Change according to newest Geological Chance Factor or leave as it is.</p>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="accordion"> 
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Source Rock :</label>
                                                <div class="controls wajib">
                                                    <!-- <input id="gcfsrock" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_sr', array('id'=>'gcfsrock', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_sr');?>
                                                    <?php echo $form->hiddenField($mdlGcf, 'gcf_id_reference', array('id'=>'gcf_id_reference'));?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input id="gcfsrock1" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_sr_age_system', array('id'=>'gcfsrock1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_sr_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input id="gcfsrock1a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_sr_age_serie', array('id'=>'gcfsrock1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_sr_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input id="gcfsrock2" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_sr_formation', array('id'=>'sourceformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_sr_formation');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_sr_formation_serie', array('id'=>'gcf_sr_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_sr_formation_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_kerogen', array('id'=>'gcfsrock3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_ker" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_kerogen');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_toc', array('id'=>'gcfsrock4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_toc" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_toc');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_hfu', array('id'=>'gcfsrock5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_hfu" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_hfu');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_distribution', array('id'=>'gcfsrock6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_continuity', array('id'=>'gcfsrock7', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_maturity', array('id'=>'gcfsrock8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_mat" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_maturity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <!-- <input id="gcfsrock9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_sr_otr', array('id'=>'gcfsrock9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="sr_otr" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_sr_otr');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Source Rock:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_sr_remark', array('id'=>'gcfsrock10', 'class'=>'span3', 'row'=>'2', 'style'=>'text-align: left;'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Reservoir :</label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfres" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_res', array('id'=>'gcfres', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_res');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_res_age_system', array('id'=>'gcfres1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_res_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_res_age_serie', array('id'=>'gcfres1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_res_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_res_formation', array('id'=>'reservoirformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_res_formation');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div class="wajib">
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_res_formation_serie', array('id'=>'gcf_res_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_res_formation_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_set', array('id'=>'gcfres3', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_set');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_depos_env', array('id'=>'gcfres4', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_depos_env');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_distribution', array('id'=>'gcfres5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_dis" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_continuity', array('id'=>'gcfres6', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_lithology', array('id'=>'gcfres7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_lit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_lithology');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_type', array('id'=>'gcfres8', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_type');?>
                                                </div>
                                            </div>
                                            <!-- <div id="prim_pro" class="control-group"> -->
                                                <!-- <label class="control-label">Average Primary Porosity Reservoir :</label> -->
                                                <!-- <div class="controls"> -->
                                                    <!-- <p style="margin-bottom: 2px;"> -->
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php //echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3', 'style'=>'text-align: center; max-width: 155px; position: relative; display: inline-block;'));?>
                                                    <!-- <span class="addkm">%</span> -->
                                                    <!-- </p> -->
                                                    <?php //echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                <!-- </div> -->
                                            <!-- </div> -->
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Average Primary Porosity Reservoir % :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_primary', array('id'=>'gcfres9', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_pri" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_primary');?>
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_res_por_secondary', array('id'=>'gcfres10', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="re_sec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_res_por_secondary');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Reservoir :</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfres11" class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_res_remark', array('id'=>'gcfres11', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Trap :</strong></label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_trap', array('id'=>'gcftrap', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_trap');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_system', array('id'=>'gcftrap1', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_age_serie', array('id'=>'gcftrap1a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_trap_seal_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name or Equivalent :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation', array('id'=>'sealingformationname', 'class'=>'span3 formationname', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_formation_serie', array('id'=>'gcf_trap_seal_formation_serie', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_trap_seal_formation_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_distribution', array('id'=>'gcftrap3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_sdi" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_distribution');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_continuity', array('id'=>'gcftrap4', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_scn" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_continuity');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap5" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_seal_type', array('id'=>'gcftrap5', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_stp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_seal_type');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_trap_age_system', array('id'=>'gcftrap6', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_trap_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_trap_age_serie', array('id'=>'gcftrap6a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_trap_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_geometry', array('id'=>'gcftrap7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_geo" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_geometry');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_type', array('id'=>'gcftrap8', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="tr_trp" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_type');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_trap_closure', array('id'=>'gcftrap9', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_trap_closure');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Trap:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcftrap10"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_trap_remark', array('id'=>'gcftrap10', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Dynamic :</strong></label>
                                                <div class="controls wajib">
                                                    <!-- <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_is_dyn', array('id'=>'gcfdyn', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_is_dyn');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration', array('id'=>'gcfdyn1', 'class'=>'span3', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_migration');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_kitchen', array('id'=>'gcfdyn2', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_kit" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_kitchen');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_petroleum', array('id'=>'gcfdyn3', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_tec" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_petroleum');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_system', array('id'=>'gcfdyn4', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_dyn_early_age_serie', array('id'=>'gcfdyn4a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_dyn_early_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_system', array('id'=>'gcfdyn5', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_dyn_late_age_serie', array('id'=>'gcfdyn5a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_dyn_late_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_preservation', array('id'=>'gcfdyn6', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_prv" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_preservation');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <!-- <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" /> -->
                                                    <?php echo $form->textField($mdlGcf, 'gcf_dyn_pathways', array('id'=>'gcfdyn7', 'class'=>'span3 gcf', 'style'=>'text-align: center; position: relative; display: inline-block;'));?>
                                                    <div id="dn_mig" class="span7 pullright">0.5</div>
                                                    <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                    <?php echo $form->error($mdlGcf, 'gcf_dyn_pathways');?>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                    <table>
                                                        <tr>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_system', array('id'=>'gcfdyn8', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_system');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                            <td style="vertical-align: top; max-width: 180px;">
                                                                <span>
                                                                    <div>
                                                                        <!-- <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" /> -->
                                                                        <?php echo $form->textField($mdlGcf, 'gcf_dyn_migration_age_serie', array('id'=>'gcfdyn8a', 'class'=>'span3', 'style'=>'width: 175px; text-align: center; position: relative; display: inline-block;'));?>
                                                                        <p style="margin-bottom: 30px; display: inline-block;"></p>
                                                                        <?php echo $form->error($mdlGcf, 'gcf_dyn_migration_age_serie');?>
                                                                    </div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Dynamic:</label>
                                                <div class="controls">
                                                    <!-- <textarea id="gcfdyn9"class="span3" rows="2"></textarea> -->
                                                    <?php echo $form->textArea($mdlGcf, 'gcf_dyn_remark', array('id'=>'gcfdyn9', 'class'=>'span3', 'row'=>'2'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- BEGIN LEAD GCF -->
            </div>
        </div>
        <?php //echo CHtml::submitButton('SAVE', array('id'=>'tombol-save', 'class'=>'btn btn-inverse', 'style'=>'overflow: visible; width: auto; height: auto;'));?>
        <div id="tombol-save" style="overflow: visible; width: auto; height: auto;">
        <?php echo CHtml::ajaxSubmitButton('SAVE', $this->createUrl('/kkks/createlead'), array(
            'type'=>'POST',
            'dataType'=>'json',
            'beforeSend'=>'function(data) {
                $("#yt0").prop("disabled", true);
            }',
            'success'=>'js:function(data) {
                $(".tooltips").attr("data-original-title", "");
                
                $(".has-err").removeClass("has-err");
                $(".errorMessage").hide();
                
                if(data.result === "success") {
                    $(".close").addClass("redirect");
                    $("#message").html(data.msg);
                    $("#popup").modal("show");
                    $("#pesan").hide();
                    $.fn.yiiGridView.update("lead-grid");
                    $(".redirect").click( function () {
                        var redirect = "' . Yii::app()->createUrl('/Kkks/createlead') . '";
                        window.location=redirect;
                    });
                    $("#yt0").prop("disabled", false);
                } else {
                    var myArray = JSON.parse(data.err);
                    $.each(myArray, function(key, val) {
                        if($("#"+key+"_em_").parents().eq(1)[0].nodeName == "TD")
                        {
                            $("#createlead-form #"+key+"_em_").parent().addClass("has-err");
                            $("#createlead-form #"+key+"_em_").parent().children().children(":input").attr("data-original-title", val);
                            
                        } else {
                            $("#createlead-form #"+key+"_em_").text(val);                                                    
                            $("#createlead-form #"+key+"_em_").show();
                            $("#createlead-form #"+key+"_em_").parent().addClass("has-err");
                        }
                        
                    });
                
                    $("#message").html(data.msg);
                    $("#popup").modal("show");
                    $("#yt0").prop("disabled", false);
                }
            }',
        ),
        array('class'=>'btn btn-inverse')
        );?>
        
        </div>
        <?php //echo CHtml::button('Get', array('onclick'=>'{getPreviousDataLead();}'));?>
        <?php $this->endWidget();?>
        
        <!-- popup submit -->
        <div id="popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#10006;</button>
		    	<h3 id="myModalLabel"><i class="icon-info-sign"></i> Information</h3>
            </div>
            <div class="modal-body">
                <p id="message"></p>
            </div>
        </div>
        <!-- end popup submit -->
    </div>
</div>
<?php 
    Yii::app()->clientScript->registerScript('ambil-gcf', "
        var blank_lithology = '',
            blank_depos_env = '',
            blank_formation_serie = '',
            blank_age_serie = '',
            blank_trap_type = '';

        function generate_prospect_name() {
            $('#gcfres7').select2('val') == 'Unknown' ? blank_lithology = '' : blank_lithology = $('#gcfres7').select2('val');
            $('#gcfres4').select2('val') == 'Others' ? blank_depos_env = '' :  blank_depos_env = $('#gcfres4').select2('val');
            $('#gcf_res_formation_serie').select2('val') == 'Not Available' ? blank_formation_serie = '' : blank_formation_serie = $('#gcf_res_formation_serie').select2('val');
            $('#gcfres1a').select2('val') == 'Not Available' ? blank_age_serie = '' : blank_age_serie = $('#gcfres1a').select2('val');
            $('#gcftrap8').select2('val') == 'Unknown' ? blank_trap_type = '' : blank_trap_type = $('#gcftrap8').select2('val');

            $('#additional_lead_name').text($('#structure_name').val() + '; ' + blank_lithology + ' - ' + blank_formation_serie + ' ' + $('#reservoirformationname').select2('val') + ' - ' + blank_age_serie + ' ' + $('#gcfres1').select2('val') + blank_depos_env + ' - ' + blank_trap_type);
        };
        
        $('#structure_name').on('keyup', function() {
            generate_prospect_name();
        });

        function gcfAttr() {
            var attrGcf = [
                '#gcfsrock3', '#gcfsrock4', '#gcfsrock5', '#gcfsrock8', '#gcfsrock9',
                '#gcfres5', '#gcfres7', '#gcfres10', '#gcfres9',
                '#gcftrap3', '#gcftrap4', '#gcftrap5', '#gcftrap7', '#gcftrap8',
                '#gcfdyn2', '#gcfdyn3', '#gcfdyn6', '#gcfdyn7',
            ];

            for(var loop = 0; loop < attrGcf.length; loop++) {
                $.ajax({
                    url: '" . Yii::app()->createUrl('/Kkks/getnilai') . "',
                    type: 'POST',
                    data: {
                        name: $(attrGcf[loop]).attr('id'),
                        choosen: $(attrGcf[loop]).val(),
                        from: 'lead',
                        category: $(gcfCategory).val() //category proven atau analog
                    },
                    dataType: 'json',
                    error: function(xhr) {
                        console.log(xhr.status);
                    },
                    success: function(e) {
                        $(e.state).text(e.value);
                    }
                });
            }
        }
        
        $('#plyName').on('change', function() {
            var value = $('#plyName').val();

            if(value != '') {
                $.ajax({
                    url : '" . Yii::app()->createUrl('/Kkks/getGcf') . "',
                    data : {
                        play_id : value,
                    },
                    type : 'POST',
                    dataType : 'json',
                    success : function(data) {
    					$('#gcf_id_reference').val(data.gcf_id);
                        $('#gcfsrock').select2('val', data.gcf_is_sr);
                        $('#gcfsrock1').select2('val', data.gcf_sr_age_system);
                        $('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
                        $('#sourceformationname').select2('val', data.gcf_sr_formation);
                        $('#gcf_sr_formation_serie').select2('val', data.gcf_sr_formation_serie);
                        $('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
                        $('#gcfsrock4').select2('val', data.gcf_sr_toc);
                        $('#gcfsrock5').select2('val', data.gcf_sr_hfu);
                        $('#gcfsrock6').select2('val', data.gcf_sr_distribution);
                        $('#gcfsrock7').select2('val', data.gcf_sr_continuity);
                        $('#gcfsrock8').select2('val', data.gcf_sr_maturity);
                        $('#gcfsrock9').select2('val', data.gcf_sr_otr);
                        $('#gcfsrock10').val(data.gcf_sr_remark);
                        $('#gcfres').select2('val', data.gcf_is_res);
                        $('#gcfres1').select2('val', data.gcf_res_age_system);
                        $('#gcfres1a').select2('val', data.gcf_res_age_serie);
                        $('#reservoirformationname').select2('val', data.gcf_res_formation);
                        $('#gcf_res_formation_serie').select2('val', data.gcf_res_formation_serie);
                        $('#gcfres3').select2('val', data.gcf_res_depos_set);
                        $('#gcfres4').select2('val', data.gcf_res_depos_env);
                        $('#gcfres5').select2('val', data.gcf_res_distribution);
                        $('#gcfres6').select2('val', data.gcf_res_continuity);      
                        $('#gcfres7').select2('val', data.gcf_res_lithology);
                        $('#gcfres8').select2('val', data.gcf_res_por_type);
                        $('#gcfres9').select2('val', data.gcf_res_por_primary);
                        $('#gcfres10').select2('val', data.gcf_res_por_secondary);
                        $('#gcfres11').val(data.gcf_res_remark);
                        $('#gcftrap').select2('val', data.gcf_is_trap);
                        $('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
                        $('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
                        $('#sealingformationname').select2('val', data.gcf_trap_seal_formation);
                        $('#gcf_trap_seal_formation_serie').select2('val', data.gcf_trap_seal_formation_serie);
                        $('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
                        $('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
                        $('#gcftrap5').select2('val', data.gcf_trap_seal_type);
                        $('#gcftrap6').select2('val', data.gcf_trap_age_system);
                        $('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
                        $('#gcftrap7').select2('val', data.gcf_trap_geometry);
                        $('#gcftrap8').select2('val', data.gcf_trap_type);
                        $('#gcftrap9').select2('val', data.gcf_trap_closure);
                        $('#gcftrap10').val(data.gcf_trap_remark);
                        $('#gcfdyn').select2('val', data.gcf_is_dyn);
                        $('#gcfdyn1').select2('val', data.gcf_dyn_migration);
                        $('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
                        $('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
                        $('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
                        $('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
                        $('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
                        $('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
                        $('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
                        $('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
                        $('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
                        $('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
                        $('#gcfdyn9').val(data.gcf_dyn_remark);
                        disabledElement('gcfsrock');
                        disabledElement('gcfres');
                        disabledElement('gcftrap');
                        disabledElement('gcfdyn');
                        disabledElement('gcfres8');
                        disable('gcfsrock');
                        disable('gcfres');
                        disable('gcftrap');
                        generate_prospect_name();
                        gcfAttr();
                    },
                });

            } else {
    			$('#gcfsrock').select2('val', '');
    			disabledElement('gcfsrock');
    			disable('gcfsrock');
    			$('#gcfsrock10').val('');
    		
    			$('#gcfres').select2('val', '');
    			disabledElement('gcfres');
                disable('gcfres');
    			$('#gcfres11').val('');
    			$('#gcfres1').select2('val', '');
                $('#gcfres1a').select2('val', '');
    			$('#reservoirformationname').select2('val', '');
                $('#gcf_res_formation_serie').select2('val', '');
    			$('#gcfres4').select2('val', '');
    			$('#gcfres7').select2('val', '');
    		
    			$('#gcftrap').select2('val', '');
    			disabledElement('gcftrap');
                disable('gcftrap');
    			$('#gcftrap10').val('');
    			$('#gcftrap8').select2('val', '');
    			
    			$('#gcfdyn').select2('val', '');
    			disabledElement('gcfdyn');
    			$('#gcfdyn9').val('');
                disabledElement('gcfres8');
                generate_prospect_name();
                gcfAttr();
    		}
        });
    ");
?>

<script type="text/javascript">
/* Memanggil nilai gcf untuk ditampilkan */
var sr = ['gcfsrock3', 'gcfsrock4', 'gcfsrock5', 'gcfsrock8', 'gcfsrock9'];
var res = ['gcfres5', 'gcfres7', 'gcfres9', 'gcfres10'];
var trp = ['gcftrap3', 'gcftrap4', 'gcftrap5', 'gcftrap7', 'gcftrap8'];
var dyn = ['gcfdyn2', 'gcfdyn3', 'gcfdyn6', 'gcfdyn7'];

var gcfCategory = ''; 

$('.gcf').each(function() {
    $(this).bind('change', function() {
        if($.inArray($(this).attr("id"), sr) != -1)
            gcfCategory = '#gcfsrock';
        else if($.inArray($(this).attr("id"), res) != -1)
            gcfCategory = '#gcfres';
        else if($.inArray($(this).attr("id"), trp) != -1)
            gcfCategory = '#gcftrap';
        else
            gcfCategory = '#gcfdyn';

        $.ajax({
            url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
            type: 'POST',
            data: {
                "name": $(this).attr("id"),
                "choosen": $(this).val(),
                "from": "lead",
                "category": $(gcfCategory).val() //category proven atau analog
            },
            dataType: 'json',
            error: function(xhr) {
                console.log(xhr.status);
            },
            success: function(e) {
                $(e.state).text(e.value);
            }
        });
    }).change();
});

$('#gcfsrock').bind('change', function() {
    if($("#gcfsrock").val() != '') {
        for(var loop = 0; loop < sr.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": sr[loop],
                    "choosen": $('#'+sr[loop]).val(),
                    "from": "lead",
                    "category": $("#gcfsrock").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#gcfres').bind('change', function() {
    if($('#gcfres').val() != '') {
        for(var loop = 0; loop < res.length; loop++) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
                type: 'POST',
                data: {
                    "name": res[loop],
                    "choosen": $('#'+res[loop]).val(),
                    "from": "lead",
                    "category": $("#gcfres").val()
                },
                dataType: 'json',
                error: function(xhr) {
                    console.log(xhr.status);
                },
                success: function(e) {
                    $(e.state).text(e.value);
                }
            });
        }
    }
}).change();

$('#gcftrap').bind('change', function() {
  if($('#gcftrap').val() != '') {
    for(var loop = 0; loop < trp.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": trp[loop],
          "choosen": $('#'+trp[loop]).val(),
          "from": "lead",
          "category": $("#gcftrap").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();

$('#gcfdyn').bind('change', function() {
  if($('#gcfdyn').val() != '') {
    for(var loop = 0; loop < dyn.length; loop++) {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl("/kkks/getnilai")?>',
        type: 'POST',
        data: {
          "name": dyn[loop],
          "choosen": $('#'+dyn[loop]).val(),
          "from": "lead",
          "category": $("#gcfdyn").val()
        },
        dataType: 'json',
        error: function(xhr) {
          console.log(xhr.status);
        },
        success: function(e) {
          $(e.state).text(e.value);
        }
      });
    }
  }
}).change();
/* end Memanggil nilai gcf untuk ditampilkan */

function getPreviousDataLead()
{
    <?php echo CHtml::ajax(array(
        'url'=>Yii::app()->createUrl('Kkks/getpreviousdatalead'),
        'data'=>'',
        'type'=>'POST',
        'dataType'=>'json',
        'success'=>"js:function(data) {
            if(data.error === false) {
                $('#structure_name').val(data.structure_name);
                $('#" . CHtml::activeId($mdlLead, 'play_id') . "').val(data.play_id);
                $('#_clarifiedby').select2('val', data.lead_clarified);
                $('#_yearofstudy').val(data.lead_year_study);
                $('#l5').val(data.lead_date_initiate);
                $('#" . CHtml::activeId($mdlLead, 'center_lat_degree') . "').val(data.center_lat_degree);
                $('#" . CHtml::activeId($mdlLead, 'center_lat_minute') . "').val(data.center_lat_minute);
                $('#" . CHtml::activeId($mdlLead, 'center_lat_second') . "').val(data.center_lat_second);
                $('#" . CHtml::activeId($mdlLead, 'center_lat_direction') . "').val(data.center_lat_direction);
                $('#" . CHtml::activeId($mdlLead, 'center_long_degree') . "').val(data.center_long_degree);
                $('#" . CHtml::activeId($mdlLead, 'center_long_minute') . "').val(data.center_long_minute);
                $('#" . CHtml::activeId($mdlLead, 'center_long_second') . "').val(data.center_long_second);
                $('#" . CHtml::activeId($mdlLead, 'center_long_direction') . "').val(data.center_long_direction);
                $('#env_onoffshore').select2('val', data.lead_shore);
                $('#env_terrain').select2('val', data.lead_terrain);
                $('#n_facility').val(data.lead_near_field);
                $('#n_developmentwell').val(data.lead_near_infra_structure);
                $('#dav_gfs_aqyear').val(data.lsgf_year_survey);
                $('#dav_gfs_surmethod').select2('val', data.lsgf_survey_method);
                $('#dav_gfs_tca').val(data.lsgf_coverage_area);
                $('#dav_gfs_lowes').val(data.lsgf_low_estimate);
                $('#dav_gfs_bestes').val(data.lsgf_best_estimate);
                $('#dav_gfs_highes').val(data.lsgf_high_estimate);
                $('#dav_gfs_remarks').val(data.lsgf_remark);
                $('#dav_2dss_aqyear').val(data.ls2d_year_survey);
                $('#dav_2dss_numvin').select2('val', data.ls2d_vintage_number);
                $('#dav_2dss_tcn').val(data.ls2d_total_crossline);
                $('#dav_2dss_tca').val(data.ls2d_total_coverage);
                $('#dav_2dss_aspi').val(data.ls2d_average_interval);
                $('#dav_2dss_lpy').val(data.ls2d_year_late_process);
                $('#dav_2dss_lpm').select2('val', data.ls2d_late_method);
                $('#dav_2dss_siq').select2('val', data.ls2d_img_quality);
                $('#dav_2dss_lowes').val(data.ls2d_low_estimate);
                $('#dav_2dss_bestes').val(data.ls2d_best_estimate);
                $('#dav_2dss_highes').val(data.ls2d_hight_estimate);
                $('#dav_2dss_remarks').val(data.ls2d_remark);   
                $('#dav_grasur_aqyear').val(data.lsgv_year_survey);
                $('#dav_grasur_surveymethods').select2('val', data.lsgv_survey_method);
                $('#dav_grasur_sca').val(data.lsgv_coverage_area);
                $('#dav_grasur_dspr').val(data.lsgv_range_penetration);
                $('#dav_grasur_rsi').val(data.lsgv_spacing_interval);
                $('#dav_grasur_lowes').val(data.lsgv_low_estimate);
                $('#dav_grasur_bestes').val(data.lsgv_best_estimate);
                $('#dav_grasur_highes').val(data.lsgv_high_estimate);
                $('#dav_grasur_remarks').val(data.lsgv_remark);
                $('#dav_geo_aqyear').val(data.lsgc_year_survey);
                $('#dav_geo_isr').val(data.lsgc_range_interval);
                $('#dav_geo_numsample').val(data.lsgc_number_sample);
                $('#dav_geo_numrock').val(data.lsgc_number_rock);
                $('#dav_geo_numfluid').val(data.lsgc_number_fluid);
                $('#dav_geo_ahc').select2('val', data.lsgc_hc_availability);
                $('#dav_geo_hycomp').val(data.lsgc_hc_composition);
                $('#dav_geo_lery').val(data.lsgc_year_report);
                $('#dav_geo_lowes').val(data.lsgc_low_estimate);
                $('#dav_geo_bestes').val(data.lsgc_best_estimate);
                $('#dav_geo_highes').val(data.lsgc_high_estimate);
                $('#dav_geo_m').val(data.lsgc_remark);
                $('#dav_elec_aqyear').val(data.lsel_year_survey);
                $('#dav_elec_surveymethods').select2('val', data.lsel_survey_method);
                $('#dav_elec_sca').val(data.lsel_coverage_area);
                $('#dav_elec_dspr').val(data.lsel_range_penetration);
                $('#dav_elec_rsi').val(data.lsel_spacing_interval);
                $('#dav_elec_lowes').val(data.lsel_low_estimate);
                $('#dav_elec_bestes').val(data.lsel_best_estimate);
                $('#dav_elec_highes').val(data.lsel_high_estimate);
                $('#dav_elec_remarks').val(data.lsel_remark);
                $('#dav_res_aqyear').val(data.lsrt_year_survey);
                $('#dav_res_surveymethods').select2('val', data.lsrt_survey_method);
                $('#dav_res_sca').val(data.lsrt_coverage_area);
                $('#dav_res_dspr').val(data.lsrt_range_penetration);
                $('#dav_res_rsi').val(data.lsrt_spacing_interval);
                $('#dav_res_lowes').val(data.lsrt_low_estimate);
                $('#dav_res_bestes').val(data.lsrt_best_estimate);
                $('#dav_res_highes').val(data.lsrt_high_estimate);
                $('#dav_res_remarks').val(data.lsrt_remark);
                $('#dav_other_aqyear').val(data.lsor_year_survey);
                $('#dav_other_lowes').val(data.lsor_low_estimate);
                $('#dav_other_bestes').val(data.lsor_best_estimate);
                $('#dav_other_highes').val(data.lsor_high_estimate);
                $('#dav_other_remarks').val(data.lsor_remark);
                $('#gcfsrock').select2('val', data.gcf_is_sr);
                $('#gcfsrock1').select2('val', data.gcf_sr_age_system);
                $('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
                $('#gcfsrock2').select2('val', data.gcf_sr_formation);
                $('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
                $('#gcfsrock4').select2('val', data.gcf_sr_toc);
                $('#gcfsrock5').select2('val', data.gcf_sr_hfu);
                $('#gcfsrock6').select2('val', data.gcf_sr_distribution);
                $('#gcfsrock7').select2('val', data.gcf_sr_continuity);
                $('#gcfsrock8').select2('val', data.gcf_sr_maturity);
                $('#gcfsrock9').select2('val', data.gcf_sr_otr);
                $('#gcfsrock10').val(data.gcf_sr_remark);
                $('#gcfres').select2('val', data.gcf_is_res);
                $('#gcfres1').select2('val', data.gcf_res_age_system);
                $('#gcfres1a').select2('val', data.gcf_res_age_serie);
                $('#gcfres2').select2('val', data.gcf_res_formation);
                $('#gcfres3').select2('val', data.gcf_res_depos_set);
                $('#gcfres4').select2('val', data.gcf_res_depos_env);
                $('#gcfres5').select2('val', data.gcf_res_distribution);
                $('#gcfres6').select2('val', data.gcf_res_continuity);      
                $('#gcfres7').select2('val', data.gcf_res_lithology);
                $('#gcfres8').select2('val', data.gcf_res_por_type);
                $('#gcfres9').select2('val', data.gcf_res_por_primary);
                $('#gcfres10').select2('val', data.gcf_res_por_secondary);
                $('#gcfres11').val(data.gcf_res_remark);
                $('#gcftrap').select2('val', data.gcf_is_trap);
                $('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
                $('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
                $('#gcftrap2').select2('val', data.gcf_trap_seal_formation);
                $('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
                $('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
                $('#gcftrap5').select2('val', data.gcf_trap_seal_type);
                $('#gcftrap6').select2('val', data.gcf_trap_age_system);
                $('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
                $('#gcftrap7').select2('val', data.gcf_trap_geometry);
                $('#gcftrap8').select2('val', data.gcf_trap_type);
                $('#gcftrap9').select2('val', data.gcf_trap_closure);
                $('#gcftrap10').val(data.gcf_trap_remark);
                $('#gcfdyn').select2('val', data.gcf_is_dyn);
                $('#gcfdyn1').select2('val', data.gcf_dyn_migration);
                $('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
                $('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
                $('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
                $('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
                $('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
                $('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
                $('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
                $('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
                $('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
                $('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
                $('#gcfdyn9').val(data.gcf_dyn_remark);
            }
        }",
    ));?>
}
</script>
    
<?php 
    Yii::app()->clientScript->registerScript('formationname', "
        $.ajax({
            url : '" . Yii::app()->createUrl('/Kkks/playname') ."',
            type: 'POST',
            data: '',
            dataType: 'json',
            success : function(data) {
                $('#plyName').select2({
                    placeholder: 'Pilih',
                    allowClear: true,
                    data : data.play,
                    escapeMarkup: function(m) {
                        // Do not escape HTML in the select options text
                        return m;
                    },
                });
            },
        }); 
        
        $.ajax({
            url : '" . Yii::app()->createUrl('/Kkks/formationname') ."',
            type: 'POST',
            data: '',
            dataType: 'json',
            success : function(data) {
                $('.formationname').select2({
                    placeholder: 'Pilih',
                    allowClear: true,
                    data : data.formation,
                });
        
                disable('gcfsrock');
                disable('gcfres');
                disable('gcftrap');
            },
        }); 
        
        function disable(id) {
            switch(id) {
                case 'gcfsrock' :
                    if($('#' + id).val() == '') {
                        mati(['#sourceformationname']);
                    } else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                        nyala(['#sourceformationname']);
                    }
                    break;
                case 'gcfres' :
                        mati2(['#reservoirformationname']);
                    break;
                case 'gcftrap' :
                    if($('#' + id).val() == '') {
                        mati(['#sealingformationname']);
                    } else if($('#' + id).val() == 'Analog' || $('#' + id).val() == 'Proven') {
                        nyala(['#sealingformationname']);
                    }
                    break;
            }
        }
        
        function mati(target) {
            $(target).select2(\"readonly\", true);
            $(target).select2(\"val\", \"\");
        }
        
        function mati2(target) {
            $(target).select2(\"readonly\", true);
        }
                        
        function nyala(target) {
             $(target).select2(\"enable\", true);
             $(target).select2(\"readonly\", false);
        }
                        
        $('#gcfsrock').on('change', function(e) {
            disable('gcfsrock');
        });
        $('#gcfres').on('change', function(e) {
            disable('gcfres');
        });
        $('#gcftrap').on('change', function(e) {
            disable('gcftrap');
        });  
    ");
    
    Yii::app()->clientScript->registerScript('ajaxupdate', "
        $('#lead-grid a.ajaxupdate').live('click', function() {
            $.ajax({
                url : $(this).attr('href'),
                type: 'POST',
                data: '',
                dataType: 'json',
                success : function(data) {
                    if(data.error === false) {
                        if(data.lead_update_from != '') $('#lead_update_from').val(data.lead_update_from);
                        if(data.lead_submit_revision != '') $('#lead_submit_revision').val(data.lead_submit_revision);
                        $('#structure_name').val(data.structure_name);
                        $('#plyName').val(data.play_id);
                        $('#_clarifiedby').select2('val', data.lead_clarified);
                        $('#_yearofstudy').val(data.lead_year_study);
                        $('#l5').val(data.lead_date_initiate);
                        $('#" . CHtml::activeId($mdlLead, 'center_lat_degree') . "').val(data.center_lat_degree);
                        $('#" . CHtml::activeId($mdlLead, 'center_lat_minute') . "').val(data.center_lat_minute);
                        $('#" . CHtml::activeId($mdlLead, 'center_lat_second') . "').val(data.center_lat_second);
                        $('#" . CHtml::activeId($mdlLead, 'center_lat_direction') . "').val(data.center_lat_direction);
                        $('#" . CHtml::activeId($mdlLead, 'center_long_degree') . "').val(data.center_long_degree);
                        $('#" . CHtml::activeId($mdlLead, 'center_long_minute') . "').val(data.center_long_minute);
                        $('#" . CHtml::activeId($mdlLead, 'center_long_second') . "').val(data.center_long_second);
                        $('#" . CHtml::activeId($mdlLead, 'center_long_direction') . "').val(data.center_long_direction);
                        $('#env_onoffshore').select2('val', data.lead_shore);
                        $('#env_terrain').select2('val', data.lead_terrain);
                        $('#n_facility').val(data.lead_near_field);
                        $('#n_developmentwell').val(data.lead_near_infra_structure);
                        
                        if(data.lsgf_is_check != null) {
                            $('#LGfs').parent().addClass('checked');
                            $('#LGfs').prop('checked', true);
                            toggleStatus('LGfs','LGfs_link','col_dav1');
                        } else {
                            $('#LGfs').parent().removeClass('checked');
                            $('#LGfs').prop('checked', false);
                            $('#col_dav1').css('height', '0px');
                            $('#LGfs_link').css('color', 'gray');
                            $('#col_dav1').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_gfs_aqyear').val(data.lsgf_year_survey);
                        $('#dav_gfs_surmethod').select2('val', data.lsgf_survey_method);
                        $('#dav_gfs_tca').val(data.lsgf_coverage_area);
                        $('#dav_gfs_lowes').val(data.lsgf_low_estimate);
                        $('#dav_gfs_bestes').val(data.lsgf_best_estimate);
                        $('#dav_gfs_highes').val(data.lsgf_high_estimate);
                        $('#dav_gfs_remarks').val(data.lsgf_remark);
                        if(data.ls2d_is_check != null) {
                            $('#LDss').parent().addClass('checked');
                            $('#LDss').prop('checked', true);
                            toggleStatus('LDss','LDss_link','col_dav2');
                        } else {
                            $('#LDss').parent().removeClass('checked');
                            $('#LDss').prop('checked', false);
                            $('#col_dav2').css('height', '0px');
                            $('#LDss_link').css('color', 'gray');
                            $('#col_dav2').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_2dss_aqyear').val(data.ls2d_year_survey);
                        $('#dav_2dss_numvin').select2('val', data.ls2d_vintage_number);
                        $('#dav_2dss_tcn').val(data.ls2d_total_crossline);
                        $('#dav_2dss_tca').val(data.ls2d_total_coverage);
                        $('#dav_2dss_aspi').val(data.ls2d_average_interval);
                        $('#dav_2dss_lpy').val(data.ls2d_year_late_process);
                        $('#dav_2dss_lpm').select2('val', data.ls2d_late_method);
                        $('#dav_2dss_siq').select2('val', data.ls2d_img_quality);
                        $('#dav_2dss_lowes').val(data.ls2d_low_estimate);
                        $('#dav_2dss_bestes').val(data.ls2d_best_estimate);
                        $('#dav_2dss_highes').val(data.ls2d_hight_estimate);
                        $('#dav_2dss_remarks').val(data.ls2d_remark);
                        if(data.lsgv_is_check != null) {
                            $('#LGras').parent().addClass('checked');
                            $('#LGras').prop('checked', true);
                            toggleStatus('LGras','LGras_link','col_dav3');
                        } else {
                            $('#LGras').parent().removeClass('checked');
                            $('#LGras').prop('checked', false);
                            $('#col_dav3').css('height', '0px');
                            $('#LGras_link').css('color', 'gray');
                            $('#col_dav3').find('input,select,textarea,button').prop('disabled', true);
                        }   
                        $('#dav_grasur_aqyear').val(data.lsgv_year_survey);
                        $('#dav_grasur_surveymethods').select2('val', data.lsgv_survey_method);
                        $('#dav_grasur_sca').val(data.lsgv_coverage_area);
                        $('#dav_grasur_dspr').val(data.lsgv_range_penetration);
                        $('#dav_grasur_rsi').val(data.lsgv_spacing_interval);
                        $('#dav_grasur_lowes').val(data.lsgv_low_estimate);
                        $('#dav_grasur_bestes').val(data.lsgv_best_estimate);
                        $('#dav_grasur_highes').val(data.lsgv_high_estimate);
                        $('#dav_grasur_remarks').val(data.lsgv_remark);
                        if(data.lsgc_is_check != null) {
                            $('#LGeos').parent().addClass('checked');
                            $('#LGeos').prop('checked', true);
                            toggleStatus('LGeos','LGeos_link','col_dav4');
                        } else {
                            $('#LGeos').parent().removeClass('checked');
                            $('#LGeos').prop('checked', false);
                            $('#col_dav4').css('height', '0px');
                            $('#LGeos_link').css('color', 'gray');
                            $('#col_dav4').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_geo_aqyear').val(data.lsgc_year_survey);
                        $('#dav_geo_isr').val(data.lsgc_range_interval);
                        $('#dav_geo_numsample').val(data.lsgc_number_sample);
                        $('#dav_geo_numrock').val(data.lsgc_number_rock);
                        $('#dav_geo_numfluid').val(data.lsgc_number_fluid);
                        $('#dav_geo_ahc').select2('val', data.lsgc_hc_availability);
                        $('#dav_geo_hycomp').val(data.lsgc_hc_composition);
                        $('#dav_geo_lery').val(data.lsgc_year_report);
                        $('#dav_geo_lowes').val(data.lsgc_low_estimate);
                        $('#dav_geo_bestes').val(data.lsgc_best_estimate);
                        $('#dav_geo_highes').val(data.lsgc_high_estimate);
                        $('#dav_geo_m').val(data.lsgc_remark);
                        if(data.lsel_is_check != null) {
                            $('#LEs').parent().addClass('checked');
                            $('#LEs').prop('checked', true);
                            toggleStatus('LEs','LEs_link','col_dav5');
                        } else {
                            $('#LEs').parent().removeClass('checked');
                            $('#LEs').prop('checked', false);
                            $('#col_dav5').css('height', '0px');
                            $('#LEs_link').css('color', 'gray');
                            $('#col_dav5').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_elec_aqyear').val(data.lsel_year_survey);
                        $('#dav_elec_surveymethods').select2('val', data.lsel_survey_method);
                        $('#dav_elec_sca').val(data.lsel_coverage_area);
                        $('#dav_elec_dspr').val(data.lsel_range_penetration);
                        $('#dav_elec_rsi').val(data.lsel_spacing_interval);
                        $('#dav_elec_lowes').val(data.lsel_low_estimate);
                        $('#dav_elec_bestes').val(data.lsel_best_estimate);
                        $('#dav_elec_highes').val(data.lsel_high_estimate);
                        $('#dav_elec_remarks').val(data.lsel_remark);
                        if(data.lsrt_is_check != null) {
                            $('#LRs').parent().addClass('checked');
                            $('#LRs').prop('checked', true);
                            toggleStatus('LRs','LRs_link','col_dav6');
                        } else {
                            $('#LRs').parent().removeClass('checked');
                            $('#LRs').prop('checked', false);
                            $('#col_dav6').css('height', '0px');
                            $('#LRs_link').css('color', 'gray');
                            $('#col_dav6').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_res_aqyear').val(data.lsrt_year_survey);
                        $('#dav_res_surveymethods').select2('val', data.lsrt_survey_method);
                        $('#dav_res_sca').val(data.lsrt_coverage_area);
                        $('#dav_res_dspr').val(data.lsrt_range_penetration);
                        $('#dav_res_rsi').val(data.lsrt_spacing_interval);
                        $('#dav_res_lowes').val(data.lsrt_low_estimate);
                        $('#dav_res_bestes').val(data.lsrt_best_estimate);
                        $('#dav_res_highes').val(data.lsrt_high_estimate);
                        $('#dav_res_remarks').val(data.lsrt_remark);
                        if(data.lsor_is_check != null) {
                            $('#LOs').parent().addClass('checked');
                            $('#LOs').prop('checked', true);
                            toggleStatus('LOs','LOs_link','col_dav7');
                        } else {
                            $('#LOs').parent().removeClass('checked');
                            $('#LOs').prop('checked', false);
                            $('#col_dav7').css('height', '0px');
                            $('#LOs_link').css('color', 'gray');
                            $('#col_dav7').find('input,select,textarea,button').prop('disabled', true);
                        }
                        $('#dav_other_aqyear').val(data.lsor_year_survey);
                        $('#dav_other_lowes').val(data.lsor_low_estimate);
                        $('#dav_other_bestes').val(data.lsor_best_estimate);
                        $('#dav_other_highes').val(data.lsor_high_estimate);
                        $('#dav_other_remarks').val(data.lsor_remark);
                        $('#gcfsrock').select2('val', data.gcf_is_sr);
                        $('#gcfsrock1').select2('val', data.gcf_sr_age_system);
                        $('#gcfsrock1a').select2('val', data.gcf_sr_age_serie);
                        $('#sourceformationname').select2('val', data.gcf_sr_formation_name_pre);
                        $('#gcf_sr_formation_name_post').select2('val', data.gcf_sr_formation_name_post);
                        $('#gcfsrock3').select2('val', data.gcf_sr_kerogen);
                        $('#gcfsrock4').select2('val', data.gcf_sr_toc);
                        $('#gcfsrock5').select2('val', data.gcf_sr_hfu);
                        $('#gcfsrock6').select2('val', data.gcf_sr_distribution);
                        $('#gcfsrock7').select2('val', data.gcf_sr_continuity);
                        $('#gcfsrock8').select2('val', data.gcf_sr_maturity);
                        $('#gcfsrock9').select2('val', data.gcf_sr_otr);
                        $('#gcfsrock10').val(data.gcf_sr_remark);
                        $('#gcfres').select2('val', data.gcf_is_res);
                        $('#gcfres1').select2('val', data.gcf_res_age_system);
                        $('#gcfres1a').select2('val', data.gcf_res_age_serie);
                        $('#reservoirformationname').select2('val', data.gcf_res_formation_name_pre);
                        $('#gcf_res_formation_name_post').select2('val', data.gcf_res_formation_name_post);
                        $('#gcfres3').select2('val', data.gcf_res_depos_set);
                        $('#gcfres4').select2('val', data.gcf_res_depos_env);
                        $('#gcfres5').select2('val', data.gcf_res_distribution);
                        $('#gcfres6').select2('val', data.gcf_res_continuity);      
                        $('#gcfres7').select2('val', data.gcf_res_lithology);
                        $('#gcfres8').select2('val', data.gcf_res_por_type);
                        $('#gcfres9').select2('val', data.gcf_res_por_primary);
                        $('#gcfres10').select2('val', data.gcf_res_por_secondary);
                        $('#gcfres11').val(data.gcf_res_remark);
                        $('#gcftrap').select2('val', data.gcf_is_trap);
                        $('#gcftrap1').select2('val', data.gcf_trap_seal_age_system);
                        $('#gcftrap1a').select2('val', data.gcf_trap_seal_age_serie);
                        $('#sealingformationname').select2('val', data.gcf_trap_formation_name_pre);
                        $('#gcf_trap_formation_name_post').select2('val', data.gcf_trap_formation_name_post);
                        $('#gcftrap3').select2('val', data.gcf_trap_seal_distribution);
                        $('#gcftrap4').select2('val', data.gcf_trap_seal_continuity);
                        $('#gcftrap5').select2('val', data.gcf_trap_seal_type);
                        $('#gcftrap6').select2('val', data.gcf_trap_age_system);
                        $('#gcftrap6a').select2('val', data.gcf_trap_age_serie);
                        $('#gcftrap7').select2('val', data.gcf_trap_geometry);
                        $('#gcftrap8').select2('val', data.gcf_trap_type);
                        $('#gcftrap9').select2('val', data.gcf_trap_closure);
                        $('#gcftrap10').val(data.gcf_trap_remark);
                        $('#gcfdyn').select2('val', data.gcf_is_dyn);
                        $('#gcfdyn1').select2('val', data.gcf_dyn_migration);
                        $('#gcfdyn2').select2('val', data.gcf_dyn_kitchen);
                        $('#gcfdyn3').select2('val', data.gcf_dyn_petroleum);
                        $('#gcfdyn4').select2('val', data.gcf_dyn_early_age_system);
                        $('#gcfdyn4a').select2('val', data.gcf_dyn_early_age_serie);
                        $('#gcfdyn5').select2('val', data.gcf_dyn_late_age_system);
                        $('#gcfdyn5a').select2('val', data.gcf_dyn_late_age_serie);
                        $('#gcfdyn6').select2('val', data.gcf_dyn_preservation);
                        $('#gcfdyn7').select2('val', data.gcf_dyn_pathways);
                        $('#gcfdyn8').select2('val', data.gcf_dyn_migration_age_system);
                        $('#gcfdyn8a').select2('val', data.gcf_dyn_migration_age_serie);
                        $('#gcfdyn9').val(data.gcf_dyn_remark);
                    }
                    
                                    
                    
                    disabledElement('gcfsrock');
                    disable('gcfsrock');
                    disabledElement('gcfres');
                    disable('gcfres');
                    disabledElement('gcftrap');
                    disable('gcftrap');
                    disabledElement('gcfdyn');
                    disabledElement('gcfres8');
                    generate_prospect_name();
                },
            });
                    return false;
        });
    ");
?>

<?php 
$cs  = $cs = Yii::app()->getClientScript();

$css = <<<EOD
    table.items {
        width: 100%;
    }

    #lead-grid table td, #lead-grid table th {
        text-align: left;
    }

EOD;

$cs->registerCss($this->id . 'css', $css);
?>
