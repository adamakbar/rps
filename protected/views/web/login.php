<?php
/* @var $this SiteController */

$this->pageTitle='Login';
$this->breadcrumbs=array(
  'Login',
);
?>

<!-- BEGIN BODY -->
<body id="login-body">
  <div class="login-header">
      <!-- BEGIN LOGO -->
      <div id="logo" class="center">
          <img src="images/logo1.png" alt="logo" class="center" />
          <div style="width:250px; margin-left:-70px; text-align:center; padding:10px; font-size: 12px;"><b>Revitalisasi Pelaporan Sumberdaya</b></div>
      </div>
      <!-- END LOGO -->
  </div>

  <?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
      'validationSubmit'=>true,
    ),
  ))?>
  <!-- BEGIN LOGIN -->
  <div id="login">
    <!-- BEGIN LOGIN FORM -->
    <form id="loginform" class="form-vertical no-padding no-margin" action="index.html">

      <div class="control-wrap">
        <div class="control-group">
              <div class="controls">
                  <div class="input-prepend">
                      <span class="add-on"><i class="fa fa-user"></i></span><?php echo $form->textField($model, 'username');?>
                  </div>
              </div>
          </div>
          <?php echo $form->error($model, 'username');?>
          <div class="control-group">
              <div class="controls">
                  <div class="input-prepend">
                      <span class="add-on"><i class="fa fa-key"></i></span><?php echo $form->passwordField($model, 'password');?>
                  </div>
              </div>
          </div>
          <?php echo $form->error($model, 'password');?>
      </div>
    <?php echo CHtml::submitButton('Login', array('id'=>'login-btn', 'class'=>'btn btn-block login-btn'));?>
    </form>
    <!-- END LOGIN FORM -->
  </div>
  <!-- END LOGIN -->
  <?php $this->endWidget();?>
</body>
<!-- END BODY -->
