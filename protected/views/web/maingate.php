<?php
/* @var $this SiteController */

if (preg_match("/MSIE/", $_SERVER['HTTP_USER_AGENT'])) {
    header("Location: " . Yii::app()->createUrl("/web/noie"));
    die();
} else {
    header("Location: " . Yii::app()->createUrl("/web/login"));
}
?>
