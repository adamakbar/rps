<?php
/* @var $this SiteController */

$this->pageTitle='AddUser';
$this->breadcrumbs=array(
	'AddUser',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">Add User<small> Registration for new user</small></h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>New User</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN GENERAL DATA-->
				<div id="_gen_profile" class="widget">
					<div class="widget-title">
						<h4><i class="icon-user"></i> ADD USER</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<form action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Name :</label>
                                <div class="controls">
                                    <input class="span3" type="text" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Username :</label>
                                <div class="controls">
                                    <input class="span3" type="text" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Password :</label>
                                <div class="controls">
                                    <input class="span3" type="text" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">E-mail :</label>
                                <div class="controls">
                                    <div class="input-icon left">
                                        <i class="icon-envelope"></i>
                                        <input style="max-width:162px;" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->			
