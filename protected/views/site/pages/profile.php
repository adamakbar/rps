<?php
/* @var $this SiteController */

$this->pageTitle='Profile';
$this->breadcrumbs=array(
	'Profile',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">Profile<small> Working Area Detail</small></h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Profile</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-user"></i> PARTICIPANT SHARE</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Contractor Name :</label>
                                <div class="controls">
                                    <input id="k3s_" class="span5" style="text-align: center;" />
                                </div>
                            </div>
                            <div id="wajib" class="control-group">
                                <label class="control-label">Participant Interest :</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input name="custom" type="text" style="max-width:180px;" required="" />
                                        <span class="add-on">%</span>
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                            </div>
                            <button class="btn" type="button sumbit" style="margin-bottom:15px;" center="">Add Participant</button>
                        </form>

                        <div class="">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Contractor Name</th>
                                    <th>Participant Interest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="">
                                    <td>Eni</td>
                                    <td>70</td>
                                </tr>
                                <tr class="">
                                    <td>Chevron</td>
                                    <td>30</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div id="_gen_profile" class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> PROFILE DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Participant :</label>
                                <div class="controls">
                                    <input class="span3" type="text" readonly="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Operator :</label>
                                <div class="controls">
                                    <input id="e_cov_operator" class="span3" style="text-align: center;"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">PSC Sign Date :</label>
                                <div class="controls">
                                    <div class=" input-append">
                                        <input id="date_cov1" type="text" class="m-wrap medium" style="max-width:180px;"/>
                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">PSC End Date :</label>
                                <div class="controls">
                                    <div class=" input-append">
                                        <input id="date_cov2" type="text" class="m-wrap medium" style="max-width:180px;"/>
                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Authorized By :</label>
                                <div class="controls">
                                    <input class="span3" type="text" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">E-mail :</label>
                                <div class="controls">
                                    <div class="input-icon left">
                                        <i class="icon-envelope"></i>
                                        <input style="max-width:162px;" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->			
