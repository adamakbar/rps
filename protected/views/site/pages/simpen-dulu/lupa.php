<div class="row-fluid ">
            <div class="span12">
                <div class="widget widget-tabs">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i>DATA WELL AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                        <div class="tabbable portlet-tabs">
                            <ul class="nav nav-tabs">
                                <li><a href="#well_tab4" data-toggle="tab">Well 4</a></li>
                                <li><a href="#well_tab3" data-toggle="tab">Well 3</a></li>
                                <li><a href="#well_tab2" data-toggle="tab">Well 2</a></li>
                                <li><a href="#well_tab1" data-toggle="tab">Well 1</a></li>
                                <li class="active"><a href="#well_tab0" data-toggle="tab">Number of Well</a></li>                                
                            </ul>
                            <div class="tab-content">
                                <!-- Select Number of Well-->
                                <div class="tab-pane active" id="well_tab0">
                                    <!-- Notification -->
                                    <div class="control-group">
                                        <div class="alert alert-info">
                                            <button class="close" data-dismiss="alert">×</button>
                                            <strong>Well Availability.</strong> 
                                            <p>Only well that drilled this year should submitted in this form, any other well like not drilled this year or non-active well can be submitted in Well A2 form. Detail of well data can be filled at generated form below.</p>
                                        </div>
                                    </div>
                                    <!-- Notification -->                                    
                                    <div class="control-group">
                                        <label class="control-label">Number of Well :</label>
                                        <div class="controls">
                                            <input id="dw" class="span3" type="text" style="text-align: center;"/>
                                        </div>
                                    </div>
                                </div>
                                <!-- Well #1-->
                                <div class="tab-pane" id="well_tab1">
                                    <form action="#" class="form-horizontal">
                                        <div class="accordion">
                                            <!-- Well General Data -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc1_welltab1">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="acc1_welltab1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="1dw1" type="text" class="span3"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>Please using unique and meaningful well name.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Center Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="S/ N"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Center Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="E/ W"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div class="controls">
                                                                <input id="1dw2" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div class="controls">
                                                                <input id="1dw5" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Onshore or Offshore :</label>
                                                            <div class="controls">
                                                                <input id="1dw5a" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div class="controls">
                                                                <input id="1dw5b" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="1dw6" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="1dw7" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <input id="1dw8a" class="span3" type="text" />
                                                                <div class="input-append">
                                                                    <input id="1dw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="1dw9" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="1dw10" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw11" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="1dw12" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="1dw13" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw14" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw15" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw16" type="text" style="max-width:137px"/><span class="add-on">psig/feet</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw17a" type="text" style="max-width:175px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="1dw18" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="1dw19" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input id="1dw20a" class="span3" type="text" />
                                                                <input id="1dw20b" class="span3" type="text" />
                                                                <input id="1dw20c" class="span3" type="text" />
                                                                <input id="1dw20d" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input id="1dw21a" class="span3" type="text" />
                                                                <input id="1dw21b" class="span3" type="text" />
                                                                <input id="1dw21c" class="span3" type="text" />
                                                                <input id="1dw21d" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input id="1dw22a" class="span3" type="text" />
                                                                <input id="1dw22b" class="span3" type="text" />
                                                                <input id="1dw22c" class="span3" type="text" />
                                                                <input id="1dw22d" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input id="1dw23a" class="span3" type="text" />
                                                                <input id="1dw23b" class="span3" type="text" />
                                                                <input id="1dw23c" class="span3" type="text" />
                                                                <input id="1dw23d" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Hydrocarbon Indication -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc2_welltab1">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="acc2_welltab1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Indication :</label>
                                                            <div class="controls">
                                                                <input id="1dw24" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="1dw25a" class="span3" style="text-align: center;" />
                                                                <input id="1dw25b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="1dw26a" class="span3" style="text-align: center;" />
                                                                <input id="1dw26b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw27" type="text" style="max-width:170px"/><span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="1dw27a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="1dw27b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="1dw28a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="1dw28b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Rock Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc3_welltab1">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc3_welltab1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rocks Sampling Methods :</label>
                                                            <div class="controls">
                                                                <input id="1dw31" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analisys :</label>
                                                            <div class="controls">
                                                                <input id="1dw32a" class="span3" style="text-align: center;" />
                                                                <input id="1dw32b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Corring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw33" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Corring Sample Depth:</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw34" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw35a" type="text" style="max-width:173px"/><span class="add-on">bbl</span>
                                                                    <input id="1dw35b" type="text" style="max-width:175px; margin-left:24px;" placeholder="1 Core Barels is Equal to"/><span class="add-on">ft</span>
                                                                </div>                                
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw36" type="text" style="max-width:175px;">
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw37" type="text" style="max-width:175px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analisys :</label>
                                                            <div class="controls">
                                                                <input id="1dw38a" class="span3" style="text-align: center;" />
                                                                <input id="1dw38b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analisys :</label>
                                                            <div class="controls">
                                                                <input id="1dw39a" class="span3" style="text-align: center;" />
                                                                <input id="1dw39b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input id="1dw40a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw40b" class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw40c" class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw40d" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw40f" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw40g" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input id="1dw41a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw41b" class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw41c" class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw41d" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw41f" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw41g" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input id="1dw42a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw42b" class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw42c" class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw42d" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw42f" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw42g" class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input id="1dw43a" class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw43b" class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw43c" class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw43d" class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw43f" class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw43g" class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input id="1dw44a" class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw44b" class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw44c" class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw44d" class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw44f" class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw44g" class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>                                      
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="control-group">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input id="1dw45a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw45b" class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input id="1dw45c" class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input id="1dw45d" class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw45e" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw45f" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input id="1dw46a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw46b" class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input id="1dw46c" class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input id="1dw46d" class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw46e" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw46f" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input id="1dw47a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw47b" class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input id="1dw47c" class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input id="1dw47d" class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw47e" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw47f" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input id="1dw48a" class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw48b" class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input id="1dw48c" class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input id="1dw48d" class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw48e" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw48f" class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input id="1dw49a" class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw49b" class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input id="1dw49c" class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input id="1dw49d" class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw49e" class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw49f" class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input id="1dw50a" class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw50b" class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input id="1dw50c" class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input id="1dw50d" class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw50e" class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw50f" class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>                                                                 
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Fluid Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc4_welltab1">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc4_welltab1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Date of Sampled :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw53" type="text" class="m-wrap medium popovers" style="max-width:170px;" data-trigger="hover" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw54" type="text" style="max-width:170px"/><span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw55" type="text" style="max-width:143px"/><span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw56a" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw56b" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw57" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw58" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="1dw59" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availabilities :</label>
                                                            <div class="controls">
                                                                <input id="1dw60" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="1dw62" class="span3" type="text" />
                                                            </div>
                                                        </div>                                    
                                                        <div class="control-group">
                                                            <label class="control-label">Oil gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw63" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw64" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dw65" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analisys :</label>
                                                            <div class="controls">
                                                                <input id="1dw66a" class="span3" style="text-align: center;" />
                                                                <input id="1dw66b" class="span3" type="text" placeholder="Sample Quantity" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z):</label>
                                                            <div class="controls">
                                                                <input id="1dw67" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Initial Formation Volume Factor</th>
                                                                    <th>P10 (Max)</th>
                                                                    <th>P50 (Mean)</th>
                                                                    <th>P90 (Min)</th>
                                                                    <th>Estimate Forecast</th>
                                                                    <th>Average</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil ( Boi )</th>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas ( Bgi )</th>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input id="1dw70a" type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                </tr>                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <input id="1dw76" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this well :</label>
                                            <div class="controls">
                                                <textarea id="remark_1" class="span6" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Well #2-->
                                <div class="tab-pane" id="well_tab2">
                                    <form action="#" class="form-horizontal">
                                        <div class="accordion">
                                            <!-- Well General Data -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc1_welltab2">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="acc1_welltab2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="2dw1" type="text" class="span3"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>Please using unique and meaningful well name.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="S/ N"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="E/ W"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div class="controls">
                                                                <input id="2dw2" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div class="controls">
                                                                <input id="2dw5" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Onshore or Offshore :</label>
                                                            <div class="controls">
                                                                <input id="2dw5a" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="2dw6" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="2dw7" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <input id="2dw8a" class="span3" type="text" />
                                                                <div class="input-append">
                                                                    <input id="2dw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="2dw9" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="2dw10" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw11" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="2dw12" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="2dw13" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw14" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw15" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw16" type="text" style="max-width:137px"/><span class="add-on">psig/feet</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw17a" type="text" style="max-width:175px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="2dw18" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="2dw19" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Hydrocarbon Indication -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc2_welltab2">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="acc2_welltab2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Indication :</label>
                                                            <div class="controls">
                                                                <input id="2dw24" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="2dw25a" class="span3" style="text-align: center;" />
                                                                <input id="2dw25b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="2dw26a" class="span3" style="text-align: center;" />
                                                                <input id="2dw26b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw27" type="text" style="max-width:170px"/><span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="2dw27a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="2dw27b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="2dw28a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="2dw28b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Rock Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc3_welltab2">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc3_welltab2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rocks Sampling Methods :</label>
                                                            <div class="controls">
                                                                <input id="2dw31" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analisys :</label>
                                                            <div class="controls">
                                                                <input id="2dw32a" class="span3" style="text-align: center;" />
                                                                <input id="2dw32b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Corring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw33" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Corring Sample Depth:</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw34" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw35a" type="text" style="max-width:173px"/><span class="add-on">bbl</span>
                                                                    <input id="2dw35b" type="text" style="max-width:175px; margin-left:24px;" placeholder="1 Core Barels is Equal to"/><span class="add-on">ft</span>
                                                                </div>                                
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw36" type="text" style="max-width:175px;">
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw37" type="text" style="max-width:175px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analisys :</label>
                                                            <div class="controls">
                                                                <input id="2dw38a" class="span3" style="text-align: center;" />
                                                                <input id="2dw38b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analisys :</label>
                                                            <div class="controls">
                                                                <input id="2dw39a" class="span3" style="text-align: center;" />
                                                                <input id="2dw39b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>                                      
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="control-group">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>                                                                 
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Fluid Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc4_welltab2">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc4_welltab2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Date of Sampled :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw53" type="text" class="m-wrap medium popovers" style="max-width:170px;" data-trigger="hover" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw54" type="text" style="max-width:170px"/><span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw55" type="text" style="max-width:143px"/><span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw56a" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw56b" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw57" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw58" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="2dw59" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availabilities :</label>
                                                            <div class="controls">
                                                                <input id="2dw60" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="2dw62" class="span3" type="text" />
                                                            </div>
                                                        </div>                                    
                                                        <div class="control-group">
                                                            <label class="control-label">Oil gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw63" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw64" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dw65" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analisys :</label>
                                                            <div class="controls">
                                                                <input id="2dw66a" class="span3" style="text-align: center;" />
                                                                <input id="2dw66b" class="span3" type="text" placeholder="Sample Quantity" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z):</label>
                                                            <div class="controls">
                                                                <input id="2dw67" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Initial Formation Volume Factor</th>
                                                                    <th>P10 (Max)</th>
                                                                    <th>P50 (Mean)</th>
                                                                    <th>P90 (Min)</th>
                                                                    <th>Estimate Forecast</th>
                                                                    <th>Average</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil ( Boi )</th>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas ( Bgi )</th>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                </tr>                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <input id="2dw76" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this well :</label>
                                            <div class="controls">
                                                <textarea id="remark_2" class="span6" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Well #3-->
                                <div class="tab-pane" id="well_tab3">
                                    <form action="#" class="form-horizontal">
                                        <div class="accordion">
                                            <!-- Well General Data -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc1_welltab3">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="acc1_welltab3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="3dw1" type="text" class="span3"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>Please using unique and meaningful well name.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="S/ N"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="E/ W"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div class="controls">
                                                                <input id="3dw2" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div class="controls">
                                                                <input id="3dw5" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Onshore or Offshore :</label>
                                                            <div class="controls">
                                                                <input id="3dw5a" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="3dw6" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="3dw7" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <input id="3dw8a" class="span3" type="text" />
                                                                <div class="input-append">
                                                                    <input id="3dw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="3dw9" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="3dw10" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw11" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="3dw12" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="3dw13" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw14" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw15" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw16" type="text" style="max-width:137px"/><span class="add-on">psig/feet</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw17a" type="text" style="max-width:175px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="3dw18" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="3dw19" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Hydrocarbon Indication -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc2_welltab3">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="acc2_welltab3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Indication :</label>
                                                            <div class="controls">
                                                                <input id="3dw24" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="3dw25a" class="span3" style="text-align: center;" />
                                                                <input id="3dw25b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="3dw26a" class="span3" style="text-align: center;" />
                                                                <input id="3dw26b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw27" type="text" style="max-width:170px"/><span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="3dw27a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="3dw27b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="3dw28a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="3dw28b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Rock Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc3_welltab3">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc3_welltab3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rocks Sampling Methods :</label>
                                                            <div class="controls">
                                                                <input id="3dw31" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analisys :</label>
                                                            <div class="controls">
                                                                <input id="3dw32a" class="span3" style="text-align: center;" />
                                                                <input id="3dw32b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Corring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw33" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Corring Sample Depth:</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw34" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw35a" type="text" style="max-width:173px"/><span class="add-on">bbl</span>
                                                                    <input id="3dw35b" type="text" style="max-width:175px; margin-left:24px;" placeholder="1 Core Barels is Equal to"/><span class="add-on">ft</span>
                                                                </div>                                
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw36" type="text" style="max-width:175px;">
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw37" type="text" style="max-width:175px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analisys :</label>
                                                            <div class="controls">
                                                                <input id="3dw38a" class="span3" style="text-align: center;" />
                                                                <input id="3dw38b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analisys :</label>
                                                            <div class="controls">
                                                                <input id="3dw39a" class="span3" style="text-align: center;" />
                                                                <input id="3dw39b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>                                      
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="control-group">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>                                                                 
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Fluid Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc4_welltab3">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc4_welltab3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Date of Sampled :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw53" type="text" class="m-wrap medium popovers" style="max-width:170px;" data-trigger="hover" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw54" type="text" style="max-width:170px"/><span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw55" type="text" style="max-width:143px"/><span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw56a" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw56b" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw57" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw58" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="3dw59" class="span3" type="text" />
                                                            </div>3                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availabilities :</label>
                                                            <div class="controls">
                                                                <input id="3dw60" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="3dw62" class="span3" type="text" />
                                                            </div>
                                                        </div>                                    
                                                        <div class="control-group">
                                                            <label class="control-label">Oil gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw63" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw64" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dw65" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analisys :</label>
                                                            <div class="controls">
                                                                <input id="3dw66a" class="span3" style="text-align: center;" />
                                                                <input id="3dw66b" class="span3" type="text" placeholder="Sample Quantity" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z):</label>
                                                            <div class="controls">
                                                                <input id="3dw67" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Initial Formation Volume Factor</th>
                                                                    <th>P10 (Max)</th>
                                                                    <th>P50 (Mean)</th>
                                                                    <th>P90 (Min)</th>
                                                                    <th>Estimate Forecast</th>
                                                                    <th>Average</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil ( Boi )</th>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas ( Bgi )</th>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                </tr>                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <input id="3dw76" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this well :</label>
                                            <div class="controls">
                                                <textarea id="remark_3" class="span6" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Well #4-->
                                <div class="tab-pane" id="well_tab4">
                                    <form action="#" class="form-horizontal">
                                        <div class="accordion">
                                            <!-- Well General Data -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc1_welltab4">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="acc1_welltab4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="4dw1" type="text" class="span3"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>Please using unique and meaningful well name.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="S/ N"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                                <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="second"/><span class="add-on">"</span></div></div>
                                                                <input type="text" class="input-mini tooltips" data-original-title="can't be empty" data-placement="right" placeholder="E/ W"/><span class="add-on"></span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div class="controls">
                                                                <input id="4dw2" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div class="controls">
                                                                <input id="4dw5" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Onshore or Offshore :</label>
                                                            <div class="controls">
                                                                <input id="4dw5a" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="4dw6" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="4dw7" class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <blockquote>
                                                                    <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <input id="3dw8a" class="span3" type="text" />
                                                                <div class="input-append">
                                                                    <input id="4dw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="4dw9" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="4dw10" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play/Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw11" type="text" style="max-width:160px;"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="4dw12" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="4dw13" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw14" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw15" type="text" style="max-width:165px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw16" type="text" style="max-width:137px"/><span class="add-on">psig/feet</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw17a" type="text" style="max-width:175px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="4dw18" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="4dw19" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Hydrocarbon Indication -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc2_welltab4">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="acc2_welltab4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Indication :</label>
                                                            <div class="controls">
                                                                <input id="4dw24" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="4dw25a" class="span3" style="text-align: center;" />
                                                                <input id="4dw25b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="4dw26a" class="span3" style="text-align: center;" />
                                                                <input id="4dw26b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw27" type="text" style="max-width:170px"/><span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="4dw27a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="4dw27b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Depth of Water Bearing Level :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="4dw28a" type="text" style="max-width:124px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="4dw28b" class="span3" type="text" placeholder="By Tools Indication"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Rock Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc3_welltab4">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc3_welltab4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rocks Sampling Methods :</label>
                                                            <div class="controls">
                                                                <input id="4dw31" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analisys :</label>
                                                            <div class="controls">
                                                                <input id="4dw32a" class="span3" style="text-align: center;" />
                                                                <input id="4dw32b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Corring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw33" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Corring Sample Depth:</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw34" type="text" style="max-width:155px"/><span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw35a" type="text" style="max-width:173px"/><span class="add-on">bbl</span>
                                                                    <input id="4dw35b" type="text" style="max-width:175px; margin-left:24px;" placeholder="1 Core Barels is Equal to"/><span class="add-on">ft</span>
                                                                </div>                                
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw36" type="text" style="max-width:175px;">
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw37" type="text" style="max-width:175px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analisys :</label>
                                                            <div class="controls">
                                                                <input id="4dw38a" class="span3" style="text-align: center;" />
                                                                <input id="4dw38b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analisys :</label>
                                                            <div class="controls">
                                                                <input id="4dw39a" class="span3" style="text-align: center;" />
                                                                <input id="4dw39b" class="span3" type="text" placeholder="Number Quantity of Sample/s" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">API</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">mV</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                </tr>                                      
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="control-group">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                    <th>Gross Reservoir Thickness</th>
                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                    <th>Net to Gross</th>
                                                                    <th>Reservoir Porosity</th>
                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P10(Max)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P50(Mean)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>P90(Min)</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC"/><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron"/></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text"/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Estimate Forecast</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Average</th>
                                                                    <td><div class="input-append"><input class="span7" type="text" readonly="" /><span class="add-on">ft</span></div></td>
                                                                    <td><div class="input-append"><input class="span7" type="text" placeholder="DTC" readonly="" /><span class="add-on">usec/ft</span></div></td>
                                                                    <td><input class="span12" type="text" placeholder="Density-Neutron" readonly="" /></div></td>
                                                                    <td><div class="input-append"><input class="span5" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly="" /><span class="add-on">%</span></div></td>
                                                                    <td><div class="input-append"><input class="span8" type="text" readonly=""/><span class="add-on">%</span></div></td>
                                                                </tr>                                                                 
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Fluid Property (by Sampling) -->
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#acc4_welltab4">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="acc4_welltab4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Date of Sampled :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw53" type="text" class="m-wrap medium popovers" style="max-width:170px;" data-trigger="hover" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed" />
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw54" type="text" style="max-width:170px"/><span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw55" type="text" style="max-width:143px"/><span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw56a" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw56b" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw57" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw58" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="4dw59" class="span3" type="text" />
                                                            </div>3                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availabilities :</label>
                                                            <div class="controls">
                                                                <input id="4dw60" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="4dw62" class="span3" type="text" />
                                                            </div>
                                                        </div>                                    
                                                        <div class="control-group">
                                                            <label class="control-label">Oil gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw63" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw64" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60<sup>o</sup> F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dw65" type="text" style="max-width:165px"/><span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analisys :</label>
                                                            <div class="controls">
                                                                <input id="4dw66a" class="span3" style="text-align: center;" />
                                                                <input id="4dw66b" class="span3" type="text" placeholder="Sample Quantity" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z):</label>
                                                            <div class="controls">
                                                                <input id="4dw67" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Initial Formation Volume Factor</th>
                                                                    <th>P10 (Max)</th>
                                                                    <th>P50 (Mean)</th>
                                                                    <th>P90 (Min)</th>
                                                                    <th>Estimate Forecast</th>
                                                                    <th>Average</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil ( Boi )</th>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">bbl/stb</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas ( Bgi )</th>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                    <td><div class="input-append"><input type="text" style="max-width:70px"/><span class="add-on">scf/rcf</span></div></td>
                                                                </tr>                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <input id="4dw76" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this well :</label>
                                            <div class="controls">
                                                <textarea id="remark_4" class="span6" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>









        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">
                                                    <strong>Number of Well :</strong>
                                                </label>
                                                <div class="controls">
                                                    <input id="w" class="span3" type="text" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="controls">
                                                    <blockquote>
                                                        <small>Only discovery well and drilled this year should be submitted in this form, any other well like not drilled this year or non-active well can be submitted in Well A2 form. Detail of well data can be filled at generated form below.</small>
                                                    </blockquote>
                                                </div>
                                            </div>
                                            <!-- Notification -->

                                            <!-- Number Well -->
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div id="rumah_well" class="tabbable tabbable-custom">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a id="well_1" href="#tab_w1" data-toggle="tab">Well 1</a></li>
                                                            <li><a id="well_2" href="#tab_w2" data-toggle="tab" class="hidden">Well 2</a></li>
                                                            <li><a id="well_3" href="#tab_w3" data-toggle="tab" class="hidden">Well 3</a></li>
                                                            <li><a id="well_4" href="#tab_w4" data-toggle="tab" class="hidden">Well 4</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!-- Well 1 -->
                                                            <div class="tab-pane active" id="tab_w1">
                                                                <div class="accordion">
                                                                    <div class="accordion-group">
                                                                        <div class="accordion-heading">
                                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w1_1">
                                                                                <strong>Well General Data</strong>
                                                                            </a>
                                                                        </div>
                                                                        <div id="tabcol_w1_1" class="accordion-body collapse">
                                                                            <div class="accordion-inner">
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw1" type="text" class="span3"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>Please using unique and meaningful well name.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Latitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Longitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Type :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw5" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                <label class="control-label">Onshore or Offshore :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw5a" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Terrain :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw2" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Status :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw6" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Formation Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw7" class="span3" type="text" style="text-align: center;"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Date Well Completed :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw8" type="text" class="m-wrap medium" style="max-width:162px;"/>
                                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw8a" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="1dsw9" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="1dsw10" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw11" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of MDT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw12" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of RFT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw13" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Reservoir Initial Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw14" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw15" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Pressure Gradient :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw16" type="text" style="max-width:142px"/><span class="add-on">psig/ft</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Temperature :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="1dsw17a" type="text" style="max-width:164px"/><span class="add-on"><sup>o</sup>C</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Well Integrity :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw18" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                                                    <div class="controls">
                                                                                        <input id="1dsw19" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">List of Electrolog Data :</label>
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                    <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                 </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Remarks for this well :</label>
                                                                                    <div class="controls">
                                                                                        <textarea id="remark_1" class="span3" rows="2"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Well 2 -->
                                                            <div class="tab-pane" id="tab_w2">
                                                                <div class="accordion">
                                                                    <div class="accordion-group">
                                                                        <div class="accordion-heading">
                                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w2_2">
                                                                                <strong>Well General Data</strong>
                                                                            </a>
                                                                        </div>
                                                                        <div id="tabcol_w2_2" class="accordion-body collapse">
                                                                            <div class="accordion-inner">
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw1" type="text" class="span3"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>Please using unique and meaningful well name.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Latitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Longitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Type :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw5" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                <label class="control-label">Onshore or Offshore :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw5a" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Terrain :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw2" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Status :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw6" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Formation Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw7" class="span3" type="text" style="text-align: center;"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Date Well Completed :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw8" type="text" class="m-wrap medium" style="max-width:162px;"/>
                                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw8a" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="2dsw9" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="2dsw10" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw11" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of MDT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw12" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of RFT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw13" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Reservoir Initial Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw14" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw15" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Pressure Gradient :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw16" type="text" style="max-width:142px"/><span class="add-on">psig/ft</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Temperature :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="2dsw17a" type="text" style="max-width:164px"/><span class="add-on"><sup>o</sup>C</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Well Integrity :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw18" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                                                    <div class="controls">
                                                                                        <input id="2dsw19" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">List of Electrolog Data :</label>
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                 </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Remarks for this well :</label>
                                                                                    <div class="controls">
                                                                                        <textarea id="remark_2" class="span3" rows="2"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Well 3 -->
                                                            <div class="tab-pane" id="tab_w3">
                                                                <div class="accordion">
                                                                    <div class="accordion-group">
                                                                        <div class="accordion-heading">
                                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w3_3">
                                                                                <strong>Well General Data</strong>
                                                                            </a>
                                                                        </div>
                                                                        <div id="tabcol_w3_3" class="accordion-body collapse">
                                                                            <div class="accordion-inner">
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw1" type="text" class="span3"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>Please using unique and meaningful well name.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Latitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Longitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Type :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw5" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                <label class="control-label">Onshore or Offshore :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw5a" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Terrain :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw2" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Status :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw6" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Formation Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw7" class="span3" type="text" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Date Well Completed :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw8" type="text" class="m-wrap medium" style="max-width:162px;"/>
                                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw8a" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="3dsw9" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="3dsw10" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw11" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of MDT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw12" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of RFT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw13" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Reservoir Initial Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw14" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw15" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Pressure Gradient :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw16" type="text" style="max-width:142px"/><span class="add-on">psig/ft</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Temperature :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="3dsw17a" type="text" style="max-width:164px"/><span class="add-on"><sup>o</sup>C</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Well Integrity :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw18" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                                                    <div class="controls">
                                                                                        <input id="3dsw19" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">List of Electrolog Data :</label>
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                 </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Remarks for this well :</label>
                                                                                    <div class="controls">
                                                                                        <textarea id="remark_3" class="span3" rows="2"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Well 4 -->
                                                            <div class="tab-pane" id="tab_w4">
                                                                <div class="accordion">
                                                                    <div class="accordion-group">
                                                                        <div class="accordion-heading">
                                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w4_4">
                                                                                <strong>Well General Data</strong>
                                                                            </a>
                                                                        </div>
                                                                        <div id="tabcol_w4_4" class="accordion-body collapse">
                                                                            <div class="accordion-inner">
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw1" type="text" class="span3"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>Please using unique and meaningful well name.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Latitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Longitude :</label>
                                                                                    <div class="controls">
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                                        <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Type :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw5" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                <label class="control-label">Onshore or Offshore :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw5a" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Terrain :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw2" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Well Status :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw6" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Formation Name :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw7" class="span3" type="text" style="text-align: center;"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Date Well Completed :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw8" type="text" class="m-wrap medium" style="max-width:162px;"/>
                                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <blockquote>
                                                                                            <small>If Year that only available, please choose 1-January for Day and Month, if not leave it blank.</small>
                                                                                        </blockquote>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw8a" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw8b" type="text" style="max-width:180px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Total Depth :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="4dsw9" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                        <input id="4dsw10" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw11" type="text" style="max-width:155px"/><span class="add-on">TVD</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of MDT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw12" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Number of RFT Sample :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw13" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Reservoir Initial Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw14" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Pressure :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw15" type="text" style="max-width:155px"/><span class="add-on">psig</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Pressure Gradient :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw16" type="text" style="max-width:142px"/><span class="add-on">psig/ft</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Last Reservoir Temperature :</label>
                                                                                    <div class="controls">
                                                                                        <div class="input-append">
                                                                                            <input id="4dsw17a" type="text" style="max-width:164px"/><span class="add-on"><sup>o</sup>C</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Actual Well Integrity :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw18" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                                                    <div class="controls">
                                                                                        <input id="4dsw19" class="span3" style="text-align: center;" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">List of Electrolog Data :</label>
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <div class="controls">
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                        <input class="span3" type="text" />
                                                                                 </div>
                                                                                </div>
                                                                                <div class="control-group">
                                                                                    <label class="control-label">Remarks for this well :</label>
                                                                                    <div class="controls">
                                                                                        <textarea id="remark_4" class="span3" rows="2"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>