<?php
/* @var $this SiteController */

$this->pageTitle='Play';
$this->breadcrumbs=array(
	'Play',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">PLAY<small>Prosentase Kehadiran Hidrokarbon</small></h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Input RPS</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Play</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN LIST DATA PLAY -->
				<div id="_list-data-play" class="widget">
					<div class="widget-title">
						<h4><i class="icon-table"></i> LIST DATA PLAY</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-hover">
							<thead>
                            <tr>
                                <th>No</th>
								<th>Play Name</th>
								<th>Date</th>
								<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1</td>
								<td id="notice_play" >Sandstone/ Marine Shoreface/ Sentolo/ Miocene/ Early/ Structural Tectonic Extensional</td>
								<td>14/06/2013</td>
								<td><strong>Original</strong></td>
							</tr>
                            <tr>
                                <td>2</td>
                                <td><a href="#_gen_data_play" class="nyekrol">Coal/ Delta Slope/ Semarung/ Cretaceous/ Middle/ Structural Tectonic Extensional</a></td>
                                <td>23/09/2013</td>
                                <td><strong>Update</strong></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><a href="#_gen_data_play" class="nyekrol">Siltstone/ Bathyal/ Jungkong-Jungkong/ Pliocene/ Early/ Structural Tectonic Extensional</a></td>
                                <td>30/12/2013</td>
                                <td><strong>Update</strong></td>
                            </tr>        
							</tbody>
						</table>
					</div>
				</div>
				<!-- END LIST DATA PLAY -->
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN GENERAL DATA-->
				<div id="_gen_data_play" class="widget">
					<div class="widget-title">
						<h4><i class="icon-file"></i> GENERAL DATA</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<form id="play_form" action="#" class="form-horizontal">
                            <div class="accordion">
                                <!-- Begin Data Geographical Working Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geographical Working Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info!</strong> Estimated Boundary Coordinate of Play or Working Area (Datum WGS '84).
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <label class="control-label">Boundary Latitude Top Left :</label>
                                                <div class="controls">
                                                    <table>
                                                    <tr class="">
                                                    <td>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                        <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                    </td>
                                                    </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Boundary Latitude Bottom Right :</label>
                                                <div class="controls">
                                                    <table>
                                                    <tr class="">
                                                    <td>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                        <input type="text" class="input-mini" placeholder="S/ N"/>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Boundary Longitude Top Left :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr class="">
                                                        <td>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                        <input type="text" class="input-mini" placeholder="E/ W"/></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Boundary Longitude Bottom Right :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr class="">
                                                        <td>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                        <div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                        <input type="text" class="input-mini" placeholder="E/ W"/></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Basin :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="_basin"class="span5" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Province :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="_province"class="span5" style="text-align: center;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Geographical Working Area -->
                                <!-- Begin Data Geological -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geological</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">By Analog to :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="_byanalogto" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distance to Analog :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="_distancetoanalog" class="span3" style="text-align: center; max-width:183px;"/>
                                                    <span class="addkm">Km</span>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Exploration Methods :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="_explorationmethods" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Geological -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Onshore or Offshore :</label>
                                                <div id="wajib"class="controls">
                                                    <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Terrain :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Facility :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required id="n_facility" name="custom" type="text" style="max-width:165px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>                    
                                            <div class="control-group">
                                                <label class="control-label">Nearby Development Well :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required id="n_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Exploration Development Well :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required id="n_exploration_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Estimated range to the nearest exploration development well, if more than 100 Km, leave it blank." data-original-title="Nearby Exploration Development Well"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span12">
        		<!-- BEGIN DATA AVAILABILITY-->
        		<div id="_data_availability_play" class="widget">
        			<div class="widget-title">
        				<h4><i class="icon-paper-clip"></i> DATA AVAILABILITY</h4>
        				<span class="tools">
        					<a href="javascript:;" class="icon-chevron-down"></a>
        				</span>
        			</div>
        			<div class="widget-body">
        				<form action="#" class="form-horizontal">
                            <div class="accordion">
                                <!-- Begin Data Occurence Sample Description -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Occurence Sample Description</strong>
                                        </a>
                                    </div>
                                    <div id="col_dav1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Support Data :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="_supportdata" style="text-align: center;" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Proven if available data based on actual proven data, Analog if available data based on other analog data." data-original-title="Nearby Development Well"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <blockquote>
                                                        <small>Proven if available data based on actual proven data, Analog if available data based on other analog data.</small>
                                                    </blockquote>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Outcrop Place Distance :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="_outcrop" type="text" style="max-width:165px;"/>
                                                        <span class="add-on">Km</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Occurence Sample Description -->
                                <!-- Begin Data Occurrence Seismic 2D Data  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Occurrence Seismic 2D Data </strong>
                                        </a>
                                    </div>
                                    <div id="col_dav2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year :</label>
                                                <div class="controls">
                                                    <input id="osd_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Number of Seismic Crossline :</label>
                                                <div class="controls">
                                                    <input id="osd_numseismic" class="span3" type="text"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distance Seismic Intervall of Parallel Line :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="osd_dsipl" class="span3" style="text-align: center; max-width:183px;"/>
                                                    <span class="addkm">Km</span>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Seismic Image Quality :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="osd_siq" class="span3" style="text-align: center;"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Occurrence Seismic 2D Data  -->
                                <!-- Begin Data Occurrence Other Survey  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Occurrence Other Survey</strong>
                                        </a>
                                    </div>
                                    <div id="col_dav3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Geochemistry :</label>
                                                <div class="controls">
                                                    <input id="oos_1" type="text"class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Is Geochemistry data available? Please fill with any data about it." data-original-title="Geochemistry"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Geochemistry Number of Sample :</label>
                                                <div class="controls">
                                                    <input id="oos_2" type="text" class="span3" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Geochemistry Survey Range Depth :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="oos_3" type="text" style="max-width:172px;"/>
                                                        <span class="add-on">ft</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Gravity :</label>
                                                <div class="controls">
                                                    <input id="oos_4" type="text"class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content=" Is Gravity data available? Please fill with any data about it." data-original-title="Gravity"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Gravity Survey Acreage :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="oos_5" type="text" style=" max-width:170px;"/>
                                                        <span class="add-on">ac</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Gravity Survey Range Depth :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="oos_5" type="text" style="max-width:172px;"/>
                                                        <span class="add-on">ft</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Electromagnetic :</label>
                                                <div class="controls">
                                                    <input id="oos_6" type="text" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content=" Is Electromagnetic data available? Please fill with any data about it." data-original-title="Electromagnetic" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Electromagnetic Survey Acreage :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="oos_7" type="text" style="max-width:170px;"/>
                                                        <span class="add-on">ac</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Electromagnetic Survey Range Depth :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="oos_8" type="text" style="max-width:172px;"/>
                                                        <span class="add-on">ft</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Resistivity :</label>
                                                <div class="controls">
                                                    <input id="oos_9" type="text" class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Is Resistivity data available? Please fill with any data about it." data-original-title="Resistivity"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Resistivity Survey Acreage :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="oos_10" type="text" style="max-width:170px;"/>
                                                        <span class="add-on">ac</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Occurrence Other Survey -->
                                <!-- Begin Data Geology Regional Map :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geology Regional Map</strong>
                                        </a>
                                    </div>
                                    <div id="col_dav4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Scale :</label>
                                                <div class="controls">
                                                    <input id="pgrm1" class="span3" type="text"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Author by :</label>
                                             <div class="controls">
                                                    <input id="pgrm2" class="span3" type="text"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Year of Published :</label>
                                                <div class="controls">
                                                    <input id="pgrm3" class="span3" type="text"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Geology Regional Map :  -->
                            </div>
                            <div class="control-group">
                            	<label class="control-label">Data Availability Remark :</label>
                            	<div class="controls">
                            		<textarea id="rmk1" class="span3" row="2" style="text-align: left;" ></textarea>
                            	</div>
                            </div>
        				</form>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="row-fluid">
        	<div class="span12">
        		<!-- BEGIN PLAY GEOLOGICAL CHANCE FACTOR-->
        		<div id="_gcf_play" class="widget">
        			<div class="widget-title">
        				<h4><i class="icon-hdd"></i> PLAY GEOLOGICAL CHANCE FACTOR</h4>
        				<span class="tools">
        					<a href="javascript:;" class="icon-chevron-down"></a>
        				</span>
        			</div>
        			<div class="widget-body">
        				<form action="#" class="form-horizontal">
                            <div class="accordion">
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Source Rock :</label>
                                                <div id="wajib"class="controls">
                                                    <input type="hidden" id="pgcfsrock" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcfsrock1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfsrock9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Source Rock :</label>
                                                <div class="controls">
                                                    <textarea id="pgcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Reservoir :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="pgcfres" class="span3" style="text-align: center;" />                                            
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcfres1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Contuinity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Primary Porosity (%) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfres10" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Reservoir :</label>
                                                <div class="controls">
                                                    <textarea id="pgcfres11" class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Trap :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="pgcftrap" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcftrap1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap6" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcftrap6a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Trap :</label>
                                                <div class="controls">
                                                    <textarea id="pgcftrap10"class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Dynamic :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="pgcfdyn" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn1" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn4" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcfdyn4a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn5" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcfdyn5a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcfdyn8" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="pgcfdyn8a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Dynamic :</label>
                                                <div class="controls">
                                                    <textarea id="pgcfdyn9"class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
        				</form>
        			</div>
        		</div>
        	</div>
        </div>	
	</div>
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->			
