<div class="row-fluid">
            <div class="span12">
            <!-- BEGIN GENERAL DATA-->
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-file"></i> GENERAL DATA</h4>
                    <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                </div>
                <div class="widget-body">
                    <form action="#" class="form-horizontal">
                        <!-- Begin General Data -->
                        <div class="control-group">
                            <label class="control-label">Play :</label>
                            <div class="controls">
                                <input id="ds1" class="span5" style="text-align: center;"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Prospect Name :</label>
                            <div class="controls">
                                <input id="ds2" type="text" class="span5  popovers" data-trigger="hover" data-content="Sandstone - Marine Shelf - Talang Akar - Middle - Miocene - Structural Tectonic Extentional" data-original-title="Wajib Diisi. Contoh :" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Name Determined by Survey :</label>
                            <div class="controls">
                                <input id="ds3" class="span3" style="text-align: center;"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Year of Seismic :</label>
                            <div class="controls">
                                <div class=" input-append">
                                    <input id="ds4" type="text" class="m-wrap medium" style="max-width:180px"/>
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Processing Type :</label>
                            <div class="controls">
                                <input id="ds5" style="text-align: center;" class="span3"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Latest Year of Processing :</label>
                            <div class="controls">
                                <div class=" input-append">
                                    <input id="ds6" type="text" class="m-wrap medium" style="max-width:180px"/>
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Date of Initiate Prospect Name :</label>
                            <div class="controls">
                                <div class=" input-append">
                                    <input id="ds7" type="text" class="m-wrap medium" style="max-width:180px"/>
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Revised Prospect Name :</label>
                            <div class="controls">
                                <input id="ds8" type="text" class="span3"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Date of Revised Prospect Name :</label>
                            <div class="controls">
                                <div class=" input-append">
                                    <input id="ds9" type="text" class="m-wrap medium" style="max-width:180px"/>
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Number of Drilled Well at Prospect :</label>
                            <div class="controls">
                                <input id="ds10" type="text" class="span3"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Number of Dry Well at Prospect :</label>
                            <div class="controls">
                                <input id="ds11" type="text" class="span3"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Number of Tested Production Well at Prospect :</label>
                            <div class="controls">
                                <input id="ds12" type="text" class="span3"/>
                            </div>
                        </div>
                        <!-- Begin Geographical Structure Area -->
                        <p><strong>Geographical Structure Area</strong></p>
                        <div class="control-group">
                            <label class="control-label">Center Latitude :</label>
                            <div class="controls">
                                <table>
                                    <tr class="">
                                    <td>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="degree"/>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="minute"/>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="second"/>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="S/N"/>
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Center Longitude :</label>
                            <div class="controls">
                                <table>
                                    <tr class="">
                                    <td>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="degree"/>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="minute"/>
                                        <input type="text" class="input-mini tooltips" data-original-title="Wajib" data-placement="right" placeholder="second"/>
                                        <input type="text" class="input-mini" placeholder="E" disabled/>
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Geographic Area :</label>
                            <div class="controls">
                                <input id="dsenv1" class="span3" style="text-align: center;"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Terrain :</label>
                            <div class="controls">
                                <input id="dsenv2" class="span3" style="text-align: center;"/>
                            </div>
                        </div>
                        <!-- End Geographical Structure Area -->   
                        <!-- Begin Geographic Well Area-->
                        <p><strong>Geographic Well Area</strong></p> 
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                <th colspan="2">Name of Well</th>
                                <th colspan="2">Well Position Coordinate</th>
                                <th>Geographic Well</th>
                                <th>Depth of Sea Bottom Level</th>
                                <th>Terrain</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>    
                                <td rowspan="2">Name of Well #1</td>
                                <td rowspan="2"><input id="#" class="span9" type="text"/></td>
                                <td>Center Latitude</td>
                                <td>
                                    <input id="#" class="span2" type="text" placeholder="degree"/>
                                    <input id="#" class="span2" type="text" placeholder="minute"/>
                                    <input id="#" class="span2" type="text" placeholder="second"/>
                                    <input id="#" class="span2" type="text" placeholder="S/N"/>
                                </td>
                                <td rowspan="2"><input id="gwa1a" class="span9" style="text-align: center;"/></td>
                                <td rowspan="2"><input id="#" class="span9" type="text"/></td>
                                <td rowspan="2"><input id="gwa1b" class="span9" style="text-align: center;"/></td>
                                </tr>
                                <tr>
                                <td>Center Longitude</td>
                                <td>
                                    <input id="#" class="span2" type="text" placeholder="degree"/>
                                    <input id="#" class="span2" type="text" placeholder="minute"/>
                                    <input id="#" class="span2" type="text" placeholder="second"/>
                                    <input id="#" class="span2" type="text" placeholder="E" disabled="" />
                                </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Geographic Well Area -->
                        <!-- Begin Data Adjacent Activity-->
                        <p><strong>Adjacent Activity</strong></p>
                        <div class="control-group">
                            <label class="control-label">Nearby Existing Field :</label>
                            <div class="controls">
                                <input id="dsadj2" class="span3" type="text" style="text-align: center; max-width:190px;"/>
                                <span class="addkm">Km</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Nearby Oil & Gas Infrastructure :  </label>
                            <div class="controls">
                                <input id="dsadj2" class="span3" type="text" style="text-align: center; max-width:190px;"/>
                                <span class="addkm">Km</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Nearby Existing Infrastructure :</label>
                            <div class="controls">
                                <input id="dsadj1" class="span3" type="text" style="text-align: center; max-width:190px;"/>
                                <span class="addkm">Km</span>
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label">Nearby Tested Production Well :</label>
                            <div class="controls">
                                <input id="dsadj3" class="span3" type="text" style="text-align: center; max-width:190px;"/>
                                <span class="addkm">Km</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Nearby Discovery Exploration Well :</label>
                            <div class="controls">
                                <input id="dsadj3" class="span3" type="text" style="text-align: center; max-width:190px;"/>
                                <span class="addkm">Km</span>
                            </div>
                        </div>
                        <!-- End Data Adjacent Activity :  -->
                    </form>
                </div>
            </div>
            <!-- END GENERAL DATA-->
        </div>
    </div>
    
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN LEAD GCF -->
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-list"></i> DESCRIPTION PROSPECT DISCOVERY GEOLOGICAL CHANCE FACTOR</h4>
                    <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                </div>
                <div class="widget-body">
                    <form action="#" class="form-horizontal">
                        <!-- Notification -->
                            <div class="control-group">
                                <div class="alert alert-block alert-info fade in">
                                    <h4 class="alert-heading"><strong>Info!</strong></h4>
                                    <p>This Geological Chance Factor (GCF) Best On GCF Play</p>
                                </div>
                            </div>
                            <!-- Notification --> 
                        <!-- Begin Data Source Rock -->
                        <p><strong>Source Rock</strong></p>
                            <div class="control-group">
                                <label class="control-label">Source Rock :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Source/s Age :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock1" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcfsrock1a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Source Formation Name :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock2" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Type of Kerogen :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock3" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Source Rock Capacity (TOC) :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock4" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock5" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Source Rock Distribution :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock6" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Continuity :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock7" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Maturity :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock8" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Present of Other Source Rock :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfsrock9" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Remarks :</label>
                                <div class="controls">
                                    <textarea id="dlgcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea>
                                </div>
                            </div>
                            <!-- End Data Source Rock -->
                            <!-- Begin Data Reservoir -->
                            <p><strong>Reservoir</strong></p>
                            <div class="control-group">
                                <label class="control-label">Reservoir :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reservoir Age :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres1" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcfres1a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reservoir Formation Name :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres2" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Depositional Setting :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres3" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Depositional Environment :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres4" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reservoir Distribution :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres5" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Source Rock Distribution :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres6" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reservoir Contuinity :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres7" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reservoir Lithology :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres8" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Porosity Type :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres9" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Primary Porosity (%) :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres10" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Secondary Porosity :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfres11" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Remarks :</label>
                                <div class="controls">
                                    <textarea id="dlgcfres12" class="span3" rows="2"></textarea>
                                </div>
                            </div>
                            <!-- End Data Reservoir -->
                            <!-- Begin Data Trap -->
                            <p><strong>Trap</strong></p>
                            <div class="control-group">
                                <label class="control-label">Trap :</strong></label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sealing Age :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap1" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcftrap1a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Name Of Sealing Formation/Zone :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap2" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sealing Distribution :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap3" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sealing Continuity :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap4" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap5" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sealing Type :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap6" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Trapping Age :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap7" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcftrap7a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Trapping Geometry :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap8" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Trapping Type :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap9" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Closure Type :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcftrap10" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Remarks :</label>
                                <div class="controls">
                                    <textarea id="dlgcftrap11"class="span3" rows="2"></textarea>
                                </div>
                            </div>
                            <!-- End Data Trap -->
                            <!-- Begin Data Dynamic -->
                            <p><strong>Dynamic</strong></p>
                            <div class="control-group">
                                <label class="control-label">Dynamic :</strong></label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Authenticate Migration :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn1" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Trap Position due to Kitchen :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn2" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn3" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn4" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcfdyn4a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn5" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcfdyn5a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn6" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Migration Pathways :</label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn7" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estimation Migration Age : </label>
                                <div class="controls">
                                    <input type="hidden" id="dlgcfdyn8" class="span3" style="text-align: center;" />
                                    <input type="hidden" id="dlgcfdyn8a" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Remarks :</label>
                                <div class="controls">
                                    <textarea id="dlgcfdyn9"class="span3" rows="2"></textarea>
                                </div>
                            </div>
                            <!-- End Data Dynamic -->
                        </form>

                </div>
            </div>
        </div>
    </div>
        