<?php
/* @var $this SiteController */

$this->pageTitle='Drill';
$this->breadcrumbs=array(
	'Drill',
);
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">		    			
			<h3 class="page-title">PROSPECT POST DRILL</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Input RPS</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Drill</strong></a><span class="divider-last">&nbsp;</span></li>
			</ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-table"></i> LIST DATA PROSPECT DRILL</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Prospect Drill Name</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>SANDSTONE.DELTA SLOPE.TAF-Talang Akar/Upper Talang Akar/Pre-Talangakar.OLIGOCENE.STRUCTURAL TECTONIC EXTENSIONAL</td>
                                <td>14/06/2013</td>
                                <td><strong>Original</strong></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>REEF CARBONATE.REEFAL.BRF-Pre-Baturaja/Baturaja.MIOCENE.STRATIGRAPHIC DEPOSITIONAL REEF</td>
                                <td>23/09/2013</td>
                                <td><strong>Update</strong></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>PLATFORM CARBONATE.PLATFORM MARGIN.KJG-Kujung I/Kujung/Kujung 1 Reef/Kujung I of Kujung Fm.MIOCENE.STRUCTURAL TECTONIC COMPRESSIONAL INVERTED</td>
                                <td>30/12/2013</td>
                                <td><strong>Update</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> GENERAL DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                            <div class="accordion" id="accordion1a">
                                <!-- Begin Data Prospect Administration Data -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <strong>Prospect Administration Data</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Prospect Name :</label>
                                                <div class="controls">
                                                    <input id="d2" type="text" required class="span6 popovers" data-trigger="hover" data-container="body" data-content="Postdrill Prospect should already in one structure and Postdrill Prospect name should unique within Working Area." data-original-title="Prospect Name"/>
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Play Name :</label>
                                                <div id="wajib" class="controls">
                                                    <input id="play_in" class="span3" type="text" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Clarified by :</label>
                                                <div class="controls">
                                                    <input id="_clarifiedby" class="span3" type="text" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Seismic Year :</label>
                                                <div class="controls">
                                                    <input id="_seismicyear" type="text" class="span3 popovers" data-trigger="hover" data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012" data-original-title="Seismic Year" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Processing Type :</label>
                                                <div class="controls">
                                                    <input id="_processingtype" class="span3" type="text" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Latest Year of Processing :</label>
                                                <div class="controls">
                                                    <input id="_latestyearpro" type="text" class="span3 popovers" data-trigger="hover" data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012" data-original-title="Latest Year of Processing" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Revised Prospect Name :</label>
                                                <div class="controls">
                                                    <input id="_revisedprosname" class="span3" type="text"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Prospect Name Revised Date :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="d9" type="text" class="m-wrap medium popovers" data-trigger="hover" style="max-width:172px;" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Revised Date"/>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Prospect Name Initiation Date :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="d7" type="text" class="m-wrap medium popovers" data-trigger="hover" style="max-width:172px;" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Prospect Name Initiation Date"/>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Prospect Administration Data -->
                                <!-- Begin Geographical Prospect Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2_wajib">
                                            <strong>Geographical Prospect Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2_wajib" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Center Latitude :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr class="">
                                                        <td>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div>
                                                            <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"></span>
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Center Longitude :</label>
                                                <div class="controls">
                                                    <table>
                                                        <tr class="">
                                                        <td>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div>
                                                            <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on"></span>
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Geographical Prospect Area -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <strong>Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Onshore or Offshore :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Terrain :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4">
                                            <strong>Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Facility :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Development Well :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required name="custom" type="text" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Discovery Exploration Well :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required name="custom" type="text" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest exploration development well, if more than 100 km, leave it blank." data-original-title="Nearby Discovery Exploration Well"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Oil & Gas Infrastructure :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest Oil & Gas Infrastructure, if more thatn 100 km, leave it blank." data-original-title="Nearby Oil & Gas Infrastructure"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div id="1_well_discovery" class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> WELL DATA AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div  class="widget-body">
                        <form action="#" class="form-horizontal">
                            <div id="wajib" class="control-group">
                                <label class="control-label"><strong>Number of Well :</strong></label>
                                <div class="controls">
                                    <input id="w" class="span3" type="text" style="text-align: center;"/>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="controls">
                                    <blockquote>
                                        <small>Only well that drilled this year should submitted in this form, any other well like not drilled this year or non-active well can be submitted in Well A2 form. Detail of well data can be filled at generated form below.</small>
                                    </blockquote>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div id="rumah_well" class="tabbable tabbable-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a id="well_1" href="#tab_w1" data-toggle="tab">Well 1</a></li>
                                    <li><a id="well_2" href="#tab_w2" data-toggle="tab" class="hidden">Well 2</a></li>
                                    <li><a id="well_3" href="#tab_w3" data-toggle="tab" class="hidden">Well 3</a></li>
                                    <li><a id="well_4" href="#tab_w4" data-toggle="tab" class="hidden">Well 4</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- Well 1 -->
                                    <div class="tab-pane active" id="tab_w1">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w1_1">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w1_1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="well_name" type="text" required class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_category" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_type" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="onoffshore" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="terrain" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="well_status" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="targeted_forname" class="span3" type="text" required style="text-align: center;"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ttd_1" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="actual_total_depth" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="tpprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="apprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_mdt" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_rft" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="rip_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lpr_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pressure_gradient" type="text" style="max-width:147px"/><span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lrt_" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="actual_well_integrity" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="availability_electrolog" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w1_2">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w1_2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="wellmaking_watercut" type="text" style="max-width:172px"/>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="waterbearing_levdept1" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="waterbearing_levdept3" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w1_3">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w1_3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <input id="rocksampling" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <input id="petrograp_analys" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ticsd_" type="text" style="max-width:152px;"/>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="bitcsd_" type="text" style="max-width:152px;" />
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="num_totalcarebarrel" type="text" style="max-width:170px;" />
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1corebarrel" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="trcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <input id="scal_data1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w1_4">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w1_4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="z1_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="sample_at" type="text" style="max-width:172px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gas_oil_ratio" type="text" style="max-width:145px;" />
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_temperature" type="text" style="max-width:173px;" />
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="tubing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="casing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="sampleby" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <input id="report_avail" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="hydro_finger" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oilgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gasgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="condensate_gravity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample_" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <input id="gas_initialZ" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oil_viscocity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this Well:</label>
                                            <div class="controls">
                                                <textarea id="w1_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Well 2 -->
                                    <div class="tab-pane" id="tab_w2">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w2_1">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w2_1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="well_name" type="text" required class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_category2" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_type2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="onoffshore2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="terrain2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="well_status2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="targeted_forname2" class="span3" type="text" required style="text-align: center;"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="2dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ttd_1" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="actual_total_depth" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="tpprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="apprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_mdt2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_rft2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="rip_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lpr_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pressure_gradient" type="text" style="max-width:147px"/><span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lrt_" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="actual_well_integrity2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="availability_electrolog2" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w2_2">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w2_2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="wellmaking_watercut" type="text" style="max-width:172px"/>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="waterbearing_levdept1" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="waterbearing_levdept3" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w2_3">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w2_3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <input id="rocksampling2" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <input id="petrograp_analys2" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ticsd_" type="text" style="max-width:152px;"/>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="bitcsd_" type="text" style="max-width:152px;" />
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="num_totalcarebarrel" type="text" style="max-width:170px;" />
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1corebarrel" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="trcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <input id="scal_data1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w2_4">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w2_4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="z2_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="sample_at" type="text" style="max-width:172px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gas_oil_ratio" type="text" style="max-width:145px;" />
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_temperature" type="text" style="max-width:173px;" />
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="tubing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="casing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="sampleby" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <input id="report_avail2" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="hydro_finger" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oilgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gasgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="condensate_gravity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample_" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <input id="gas_initialZ" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oil_viscocity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this Well:</label>
                                            <div class="controls">
                                                <textarea id="w2_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Well 3 -->
                                    <div class="tab-pane" id="tab_w3">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w3_1">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w3_1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="well_name" type="text" required class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_category" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_type3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="onoffshore3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="terrain3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="well_status3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="targeted_forname3" class="span3" type="text" required style="text-align: center;"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="3dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ttd_1" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="actual_total_depth" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="tpprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="apprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_mdt3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_rft3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="rip_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lpr_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pressure_gradient" type="text" style="max-width:147px"/><span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lrt_" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="actual_well_integrity3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="availability_electrolog3" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w3_2">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w3_2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="wellmaking_watercut" type="text" style="max-width:172px"/>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="waterbearing_levdept1" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="waterbearing_levdept3" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w3_3">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w3_3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <input id="rocksampling3" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <input id="petrograp_analys3" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ticsd_" type="text" style="max-width:152px;"/>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="bitcsd_" type="text" style="max-width:152px;" />
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="num_totalcarebarrel" type="text" style="max-width:170px;" />
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1corebarrel" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="trcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <input id="scal_data1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w3_4">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w3_4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="z3_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="sample_at" type="text" style="max-width:172px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gas_oil_ratio" type="text" style="max-width:145px;" />
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_temperature" type="text" style="max-width:173px;" />
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="tubing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="casing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="sampleby" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <input id="report_avail3" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="hydro_finger" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oilgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gasgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="condensate_gravity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample_" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <input id="gas_initialZ" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oil_viscocity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this Well:</label>
                                            <div class="controls">
                                                <textarea id="w3_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Well 4 -->
                                    <div class="tab-pane" id="tab_w4">
                                        <div class="accordion">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w4_1">
                                                        <strong>Well General Data</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w4_1" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Well Name :</label>
                                                            <div class="controls">
                                                                <input id="well_name" type="text" required class="span3 popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Please using unique and meaningful well name." data-original-title="Well Name"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Latitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="S/ N"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Longitude :</label>
                                                            <div class="controls">
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                                                <div class=" input-append"><input type="text" required class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                                                <input type="text" required class="input-mini" placeholder="E/ W"/><span class="add-on">
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Category :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_category" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Type :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="well_type4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                        <label class="control-label">Onshore or Offshore :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="onoffshore4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Terrain :</label>
                                                            <div id="wajib" class="controls">
                                                                <input id="terrain4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Status :</label>
                                                            <div class="controls">
                                                                <input id="well_status4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Formation Name :</label>
                                                            <div class="controls">
                                                                <input id="targeted_forname4" class="span3" type="text" required style="text-align: center;"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Date Well Completed :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="4dsw8" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover" data-placement="right" data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Date Well Completed"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ttd_1" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                                <div class="input-append">
                                                                    <input id="ttd_2" type="text" style="max-width:160px; margin-left:24px;"/><span class="add-on">MID</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Total Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="actual_total_depth" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Targeted Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                <input id="tpprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Penetrated Play or Reservoir Target :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="apprt_" type="text" style="max-width:160px"/><span class="add-on">TVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of MDT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_mdt4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of RFT Sample :</label>
                                                            <div class="controls">
                                                                <input id="num_rft4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reservoir Initial Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="rip_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lpr_" type="text" style="max-width:160px"/><span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pressure Gradient :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pressure_gradient" type="text" style="max-width:147px"/><span class="add-on">psig/ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Last Reservoir Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="lrt_" type="text" style="max-width:170px"/><span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Actual Well Integrity :</label>
                                                            <div class="controls">
                                                                <input id="actual_well_integrity4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Availability of Electrolog Data Acquisition :</label>
                                                            <div class="controls">
                                                                <input id="availability_electrolog4" class="span3" style="text-align: center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">List of Electrolog Data :</label>
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                             </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                                <input class="span3" type="text" />
                                                            </div>
                                                        </div>
                                                     </div>                                                                        
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w4_2">
                                                        <strong>Hydrocarbon Indication</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w4_2" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                                <input id="oilshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Show or Reading :</label>
                                                            <div class="controls">
                                                                <input id="gasshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">By Tools Indication :</label>
                                                            <div class="controls">
                                                                <input id="oilshow_reading1" required class="span3" type="text"/>
                                                                <p class="help-block"></p>
                                                                <input id="gasshow_reading2" class="span3" type="text"/>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Well Making Water Cut :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="wellmaking_watercut" type="text" style="max-width:172px"/>
                                                                    <span class="add-on">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">GWC</span><input id="waterbearing_levdept1" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>                                                            
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Water Bearing Level Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-prepend input-append">
                                                                    <span class="add-on">OWC</span><input id="waterbearing_levdept3" type="text" style="max-width:127px"/><span class="add-on">ft</span>
                                                                </div>
                                                                <input id="waterbearing_levdept4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                            </div>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w4_3">
                                                        <strong>Rock Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w4_3" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Rock Sampling Method :</label>
                                                            <div class="controls">
                                                                <input id="rocksampling4" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Petrography Analysis :</label>
                                                            <div class="controls">
                                                                <input id="petrograp_analys4" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="ticsd_" type="text" style="max-width:152px;"/>
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="bitcsd_" type="text" style="max-width:152px;" />
                                                                    <span class="add-on">ftTVD</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Number of Total Core Barrels :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="num_totalcarebarrel" type="text" style="max-width:170px;" />
                                                                    <span class="add-on">bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">1 Core Barrel Equal to :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="1corebarrel" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Total Recoverable Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="trcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Preservative Core Data :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="pcd_" type="text" style="max-width:173px;" />
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Routine Core Analysis :</label>
                                                            <div class="controls">
                                                                <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="rca_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">SCAL Data Analysis :</label>
                                                            <div class="controls">
                                                                <input id="scal_data1" class="span3" type="text" style="text-align:center;" />
                                                                <input id="scal_data2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Reservoir Vshale Content (GR Log)</th>
                                                                        <th>Reservoir Vshale Content (SP Log)</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                        <th>Gross Reservoir Thickness</th>
                                                                        <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                        <th>Thickness Reservoir Total Pore</th>
                                                                        <th>Net to Gross</th>
                                                                        <th>Reservoir Porosity</th>
                                                                        <th>Reservoir Saturation (Cut)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>P10 (Max)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P50 (Mean)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>P90 (Min)</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Estimated Forecast</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Average</th>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                            </blockquote>
                                                        </div>
                                                    </div>                                                                      
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w4_4">
                                                        <strong>Fluid Property (by Sampling)</strong>
                                                    </a>
                                                </div>
                                                <div id="tabcol_w4_4" class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Date :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="z4_1_5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Sample Date"/>
                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled at :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="sample_at" type="text" style="max-width:172px;"/>
                                                                    <span class="add-on">ft</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Oil Ratio :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gas_oil_ratio" type="text" style="max-width:145px;" />
                                                                    <span class="add-on">scf/bbl</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Separator Temperature :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="separator_temperature" type="text" style="max-width:173px;" />
                                                                    <span class="add-on"><sup>o</sup>C</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Tubing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="tubing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Casing Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="casing_pressure" type="text" style="max-width:162px;" />
                                                                    <span class="add-on">psig</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sampled by :</label>
                                                            <div class="controls">
                                                                <input id="sampleby" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Reports Availability :</label>
                                                            <div class="controls">
                                                                <input id="report_avail4" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                            <div class="controls">
                                                                <input id="hydro_finger" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oilgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="gasgravity_60" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="condensate_gravity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">API</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">PVT Analysis :</label>
                                                            <div class="controls">
                                                                <input id="pvt_analysis" class="span3" type="text" style="text-align:center;" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Sample Quantity :</label>
                                                            <div class="controls">
                                                                <input id="sample_" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                            <div class="controls">
                                                                <input id="gas_initialZ" class="span3" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Initial Formation Volume Factor</th>
                                                                        <th>P10 (Max)</th>
                                                                        <th>P50 (Mean)</th>
                                                                        <th>P90 (Min)</th>
                                                                        <th>Estimated Forecast</th>
                                                                        <th>Average</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <th>Oil (Boi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Gas (Bgi)</th>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <blockquote>
                                                                <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                            <div class="controls">
                                                                <div class="input-append">
                                                                    <input id="oil_viscocity" type="text" style="max-width:165px;" />
                                                                    <span class="add-on">cP</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks for this Well:</label>
                                            <div class="controls">
                                                <textarea id="w4_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> SURVEY DATA AVAILABILITY</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">      
                            <div class="accordion">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlGfs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGfs','DlGfs_link','col_dav1')" /> 
                                            <a id="DlGfs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">Geological Field Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls">
                                                <input id="dav_gfs_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Method :</label>
                                            <div class="controls">
                                                <input id="dav_gfs_surmethod" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Coverage Area : </label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_gfs_tca" type="text" style="max-width:170px"/>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Volumetric Estimation</th>
                                                        <th>Areal Closure Estimation</th>
                                                        <th>Gross Sand Thickness</th>
                                                        <th>Net to Gross</th>
                                                        <th>Porosity</th>
                                                        <th>Oil Saturation (1-Sw)</th>
                                                        <th>Oil Case</th>
                                                        <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th><strong>P90</strong></th>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                        <th><strong>P50</strong></th>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                        <th><strong>P10</strong></th>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                        <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_gfs_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlDss2" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss2','DlDss2_link','col_dav2' )"/> 
                                            <a id="DlDss2_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2"> 2D Seismic Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls">
                                                <input id="dav_2dss_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number Of Vintage :</label>
                                            <div class="controls">
                                                <input id="dav_2dss_numvin" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Crossline Number :</label>
                                            <div class="controls">
                                                <input id="dav_2dss_tcn" type="text" class="span3"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Line Total Length :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_sltl" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Average Spacing Parallel Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_aspi" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Record Length :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_rl1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ms</span>
                                                </div>
                                                <div class=" input-append">
                                                    <input id="dav_2dss_rl2" type="text" style="max-width:170px; margin-left:24px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Year Of Latest Processing :</label>
                                            <div class="controls">
                                                <input id="dav_2dss_yearlatest" class="span3" type="text"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Processing Method :</label>
                                            <div class="controls">
                                                <input id="dav_2dss_lpm" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Image Quality :</label>
                                            <div class="controls">
                                                <input id="dav_2dss_siq" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Top Reserovir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_trtd1" type="text" style="max-width:157px"/>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <input id="dav_2dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Bottom Reservoir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_brtd1" type="text" style="max-width:157px"/>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <input id="dav_2dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_dcsp" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <input id="dav_2dss_owc1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <input id="dlosd2m2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/>
                                                <div class="input-append">
                                                    <input id="dav_2dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estiamte Oil or Gas :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_oilgas1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <input id="dlosd2n2" class="span3" type="text" style="margin-left:24px;" placeholder="By analog to"/>
                                                <div class=" input-append">
                                                    <input id="oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point"/>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Formation Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_forthickness" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Gross Sand or Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_2dss_gross_resthickness" required type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability Net Thickness Reservoir Pay :</label>
                                            <div class="controls">
                                                <input id="dav_2dss_antrip" class="span3" type="text" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Pay Zone Thickness/ Net Pay</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Net Pay to Gross Sand</th>
                                                <th>Vsh Cut-off</th>
                                                <th>Porosity Cut-off</th>
                                                <th>Saturation Cut-Off</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Oil Saturation (1-Sw)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_2dss_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlGras" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGras','DlGras_link','col_dav3' )"/> 
                                            <a id="DlGras_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3"> Gravity Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls">
                                                <input id="dav_grasur_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <input id="dav_grasur_surveymethods" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_sca" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range:</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_dspr" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_rsi" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_dcsp" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_owc1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <input id="dlosd3g2" class="span3" type="text" placeholder="By analog to"/>
                                                <div class=" input-append">
                                                    <input id="dav_grasur_owc2" type="text" style="max-width:170px; margin-left:18px;"placeholder="Estimated From Spill Point"/>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil or Gas</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_oilgas1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <input id="dlosd3h2" class="span3" type="text" placeholder="By analog to"/>
                                                <div class=" input-append">
                                                    <input id="dav_grasur_oilgas2" type="text" style="max-width:170px; margin-left:18px;" placeholder="Estimated from Spill Point"/>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_resthickness1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                                <input id="dav_grasur_resthickness2" class="span3" type="text" placeholder="According to"/>
                                                <input id="dav_grasur_resthickness3" class="span3" type="text" placeholder="Top Sand Seismic Depth" />
                                                <input id="dav_grasur_resthickness4" class="span3" type="text" placeholder="Bottom Sand Seismic Depth"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Gross Sand Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_gst1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">%</span>
                                                </div>
                                                <input id="dav_grasur_gst2" class="span3" type="text" placeholder="According to"/>
                                                <input id="dav_grasur_gst3" class="span6" type="text" placeholder="Well Name"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Net To Gross (Net Pay/Gross Sand) :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_grasur_netogross" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Areal Closure Estimation</th>
                                                    <th>Net Pay Thickness</th>
                                                    <th>Porosity</th>
                                                    <th>Oil Saturation (1-Sw)</th>
                                                    <th>Oil Case</th>
                                                    <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_grasur_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlGeos" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlGeos','DlGeos_link','col_dav4' )"/> 
                                            <a id="DlGeos_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4"> Geochemistry Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls">
                                                <input id="dav_geo_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Interval Samples Range :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_geo_isr" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Sample Location :</label>
                                            <div class="controls">
                                                <input id="dav_geo_numsample" type="text" class="span3"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Rocks Sample :</label>
                                            <div class="controls">
                                                <input id="dav_geo_numrock" type="text" class="span3"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of Fluid Sample :</label>
                                            <div class="controls">
                                                <input id="dav_geo_numfluid" type="text" class="span3"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability HC Composition :</label>
                                            <div class="controls">
                                                <input id="dav_geo_ahc" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Hydrocarbon Composition : </label>
                                            <div class="controls">
                                                <input id="dav_geo_hycomp" type="text" class="span3"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Laboratorium Evaluation & Report Year</label>
                                            <div class="controls">
                                                <input id="dav_geo_lery" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" placeholder="angka,koma"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Oil Saturation (1-Sw)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_geo_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlEs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlEs','DlEs_link','col_dav5' )"/> 
                                            <a id="DlEs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav5"> Electromagnetic Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav5" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls">
                                                <input id="dav_elec_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <input id="dav_elec_surveymethods" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_elec_sca" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <input id="dav_elec_dspr" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class="input-append">
                                                    <input id="dav_elec_rsi" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Area Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Oil Saturation(1-Sw)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_elec_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlRs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlRs','DlRs_link','col_dav6' )"/> 
                                            <a id="DlRs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav6"> Resistivity Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav6" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year:</label>
                                            <div class="controls">
                                                <input id="dav_res_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Methods :</label>
                                            <div class="controls">
                                                <input id="dav_res_surveymethods" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Survey Coverage Area :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_res_sca" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ac</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Survey Penetration Range :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_res_dspr" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Recorder Spacing Interval :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_res_rsi" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Area Closure Estimation</th>
                                                    <th>Net Pay Thickness</th>
                                                    <th>Porosity</th>
                                                    <th>Oil Saturation (1-Sw)</th>
                                                    <th>Oil Case</th>
                                                    <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_res_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlOs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlOs','DlOs_link','col_dav7' )"/> 
                                         <a id="DlOs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav7"> Other Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav7" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls">
                                                <input id="dav_other_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <th>Volumetric Estimation</th>
                                                    <th>Area Clossure Estimation</th>
                                                    <th>Net Pay Thickness</th>
                                                    <th>Porosity</th>
                                                    <th>Oil Saturation (1-Sw)</th>
                                                    <th>Oil Case</th>
                                                    <th>Gas Case</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <th>P90</th>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                    <th>P50</th>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                    <tr>
                                                    <th>P10</th>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                    <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_other_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="DlDss3" class="cekbok" type="checkbox" value="" onclick="toggleStatus('DlDss3','DlDss3_link','col_dav8' )"/> 
                                            <a id="DlDss3_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav8"> Survey Seismic 3D</a>
                                        </div>
                                    </div>
                                    <div id="col_dav8" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                        <div class="control-group">
                                            <label class="control-label">Acquisition Year :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_aqyear" name="custom-wajib" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Number of 3D Vintage :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_numvin" class="span3" style="text-align: center;" />
                                            </div>
                                        </div>                                        
                                        <div class="control-group">
                                            <label class="control-label">Bin Size :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_binsize" type="text" class="span3" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Total Coverage Area : </label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_tca" type="text" style="max-width:160px;"/>
                                                    <span class="add-on">km<sup>2</sup></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Dominant Frequency at Reservoir Target:</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_dfrt1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">hz</span>
                                                </div>
                                                <input id="dav_3dss_dfrt2" type="text" class="span3" placeholder="Lateral Seismic Resolution"/>
                                                <input id="dav_3dss_dfrt3" type="text" class="span3" placeholder="Vertical Seismic Resolution"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Processing Year:</label>
                                            <div class="controls">
                                                <input id="dav_3dss_lpy" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Latest Processing Year"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Latest Proccessing Method :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_lpm" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Seismic Image Quality :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_siq" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Top Reservoir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_trtd1" type="text" style="max-width:155px;"/>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <input id="dav_3dss_trtd2" type="text" style="max-width:170px; margin-left:24px;"/>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Bottom Reservoir Target Depth :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_brtd1" type="text" style="max-width:155px;"/>
                                                    <span class="add-on">ftMD</span>
                                                </div>
                                                <div class=" input-append">
                                                    <input id="dav_3dss_brtd2" type="text" style="max-width:170px; margin-left:24px;"/>
                                                    <span class="add-on">ms</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Spill Point :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_dcsp" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Distinguish Wave Propagation by Offset :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_adwpo" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Resolve Reservoir Rock Property :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_ar3p" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Ability to Resolve Reservoir Fluid Property :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_arrfp" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Estimate OWC/GWC :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_owc1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>  
                                                <input id="dlosd8n2" class="span3" type="text" placeholder="By analog to" />
                                                <div class=" input-append">
                                                    <input id="dav_3dss_owc2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" />
                                                    <span class="add-on">%</span>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Depth Contour Lowest Known Oil/Gas :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_oilgas1" type="text" style="max-width:170px;"/>
                                                    <span class="add-on">ft</span>
                                                </div>  
                                                <input id="dlosd8o2" class="span3" type="text" placeholder="By analog to" />
                                                <div class=" input-append">
                                                    <input id="dav_3dss_oilgas2" type="text" style="max-width:170px; margin-left:24px;" placeholder="Estimated From Spill Point" />
                                                    <span class="add-on">%</span>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Formation Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_forthickness" type="text" style="max-width:170px;" />
                                                    <span class="add-on">ft</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Grass Sand or Reservoir Thickness :</label>
                                            <div class="controls">
                                                <div class=" input-append">
                                                    <input id="dav_3dss_grass_resthickness" required type="text" style="max-width:170px;" />
                                                    <span class="add-on">ft</span>
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Availability Net Thickness Reservoir Pay :</label>
                                            <div class="controls">
                                                <input id="dav_3dss_antrip" class="span3" style="text-align: center;"/>
                                            </div>
                                        </div>  
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Pay Zone Thickness / Net Pay</th>
                                                <th>Thickness Net Pay</th>
                                                <th>NTG(Net Pay to Gross Sand)</th>
                                                <th>Porosity Cut Off</th>
                                                <th>Vsh Cut Off</th>
                                                <th>Saturation Cut Off</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">%</i></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                <th>Volumetric Estimation</th>
                                                <th>Areal Closure Estimation</th>
                                                <th>Net Pay Thickness</th>
                                                <th>Porosity</th>
                                                <th>Oil Saturation (1-Sw)</th>
                                                <th>Oil Case</th>
                                                <th>Gas Case</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                <th>P90</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P50</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                <tr>
                                                <th>P10</th>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text"/><span class="add-on">ac</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">ft</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">%</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">MMBO</i></span></td>
                                                <td><div class=" input-append"><input id="#" class="span6" name="custom" type="text" readonly="" /><span class="add-on">BCF</i></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Remarks :</label>
                                            <div class="controls">
                                                <textarea id="dav_3dss_remarks" class="span3" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>               
                </div>
            </div>

        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN LEAD GCF -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> DESCRIPTION PROSPECT DRILLABLE GEOLOGICAL CHANCE FACTOR</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="alert alert-info">
                                    <button class="close" data-dismiss="alert">×</button>
                                    <strong>Drill Prospect Geological Chance Factor</strong>
                                    <p>When play chosen this GCF automatically filled with Play GCF, change according to newest GCF or leave as it is.</p>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="accordion"> 
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Source Rock :</label>
                                                <div id="wajib" class="controls">
                                                    <input id="gcfsrock" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock1" class="span3" style="text-align: center;" />
                                                    <input id="gcfsrock1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks for Source Rock:</label>
                                                <div class="controls">
                                                    <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Reservoir :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="gcfres" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Contuinity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Primary Porosity (%) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks for Reservoir :</label>
                                                <div class="controls">
                                                    <textarea id="gcfres11" class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Trap :</strong></label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name :</label>
                                                <div class="controls">
                                                <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks for Trap:</label>
                                                <div class="controls">
                                                    <textarea id="gcftrap10"class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Dynamic :</strong></label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remarks for Dynamic:</label>
                                                <div class="controls">
                                                    <textarea id="gcfdyn9"class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


