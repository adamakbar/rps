<?php
/* @var $this SiteController */

$this->pageTitle='Help';
$this->breadcrumbs=array(
	'Help',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->			    			
			<h3 class="page-title">HELP</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Help</a><span class="divider-last">&nbsp;</span></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Contact Us</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<div class="contact-us">
							<h3>Our Contacts</h3>
							<div class="row-fluid">
								<div class="span4">
									<h4>Location</h4>
									<p>SKK Migas <br>
										House 01, Road 02, Sector 03<br>
										South Jakarta, Jakarta 12345<br>
										Phone: +62 1 000000<br>
										Fax : 1234 5678 909</p>
								</div>
								<div class="span4">
                                      <h4>Online</h4>
                                      <p>
                                      	<strong>Email :</strong> info@lemigas.com <br>
                                      	<strong>Support :</strong> support@lemigas.com<br>
                                      	<strong>Live Chat :</strong> live@lemigas.com<br>
                                      	<strong>Fax :</strong> 1234 5678 909
                                      </p>
                                </div>
                                <div class="span4">
                                	<h4>Social Network</h4>
                                	<p>
                                		<strong>Facebook :</strong> www.facebook.com/lemigas<br>
                                		<strong>Twitter :</strong> www.twitter.com/lemigas<br>
                                		<strong>Google + :</strong> www.googleplux.com/lemigas<br>
                                	</p>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
			

