<?php
/* @var $this SiteController */

$this->pageTitle='WellInventory';
$this->breadcrumbs=array(
	'WellInventory',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <h3 class="page-title">WELL INVENTORY</h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Input RPS</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Well Inventory</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="play_page">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN GENERAL DATA-->
				<div id="_gen_data_play" class="widget">
					<div class="widget-title">
						<h4><i class="icon-file"></i> WELL INVENTORY</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<form action="#" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Well Name :</label>
                                <div class="controls">
                                    <input id="_wellname" class="span3" type="text" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Well Category :</label>
                                <div class="controls">
                                    <input id="wellcategory_" class="span3" style="text-align: center;" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Drilling Year :</label>
                                <div class="controls">
                                    <input id="_drillingyear" class="span3" type="text" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Center Latitude :</label>
                                <div class="controls">
                                    <table>
                                        <tr class="">
                                        <td>
                                        <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                        <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                        <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                        <input type="text" name="wajib" class="input-mini" placeholder="S/ N"/></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Center Longitude :</label>
                                <div class="controls">
                                    <table>
                                        <tr class="">
                                        <td>
                                        <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div>
                                        <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div>
                                        <div class=" input-append"><input type="text" name="wajib" class="input-mini" placeholder="second"/><span class="add-on">"</span></div>
                                        <input type="text" name="wajib" class="input-mini" placeholder="E/ W"/></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                                <table table id="rd_penny" class="table table-striped table-bordered dataTable" aria-describedby="sample_1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="no-sort">No</th>
                                        <th>Well Name</th>
                                        <th>Category</th>
                                        <th >Drilling Year</th>
                                        <th>Center Latitude</th>
                                        <th>Center Longitude</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="">
                                        <td class="no-sort">2</td>
                                        <td>Well Name dua</td>
                                        <td>Postdrill Not Conclusive</td>
                                        <td>2013</td>
                                        <td>04<sup>o</sup> 12' 19.58 S</td>
                                        <td>104<sup>o</sup> 27' 11.39 E</td>
                                    </tr>
                                    <tr class="">
                                        <td class="no-sort" >3</td>
                                        <td>Well Name tiga</td>
                                        <td>Postdrill Wet</td>
                                        <td>2011</td>
                                        <td>04<sup>o</sup> 12' 19.58 N</td>
                                        <td>104<sup>o</sup> 27' 11.39 E</td>
                                    </tr>
                                    <tr class="">
                                        <td class="no-sort">4</td>
                                        <td>Well Name empat</td>
                                        <td>Postdrill Dry</td>
                                        <td>2012</td>
                                        <td>04<sup>o</sup> 12' 19.58 N</td>
                                        <td>104<sup>o</sup> 27' 11.39 E</td>
                                    </tr>
                                    <tr class="">
                                        <td class="no-sort">5</td>
                                        <td>Well Name lima</td>
                                        <td>Postdrill Wet</td>
                                        <td>2013</td>
                                        <td>04<sup>o</sup> 12' 19.58 S</td>
                                        <td>104<sup>o</sup> 27' 11.39 E</td>
                                    </tr>
                                    <tr class="">
                                        <td class="no-sort">6</td>
                                        <td>Well Name enam</td>
                                        <td>Postdrill Wet</td>
                                        <td>2014</td>
                                        <td>04<sup>o</sup> 12' 19.58 N</td>
                                        <td>104<sup>o</sup> 27' 11.39 E</td>
                                    </tr>
                                    </tbody>
                                </table>
                        </form>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>            
	</div>
	<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->			
