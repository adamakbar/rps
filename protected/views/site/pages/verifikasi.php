<?php
/* @var $this SiteController */

$this->pageTitle='Verifikasi';
$this->breadcrumbs=array(
	'Verifikasi',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->			    			
			<h3 class="page-title">VERIFIKASI</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Data Summary</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Verifikasi</strong></a><span class="divider-last">&nbsp;</span></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div class="widget">
					<div class="widget-body">
						<p style="text-align: center;">"Jika Anda sudah yakin dengan data yang Anda berikan, 
						Silahkan menekan tombol dibawah ini untuk keabsahan data yang Anda kirim."</p>
						<div class="text-center">
							<button type="submit" class="btn">VERIFIKASI</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
			

