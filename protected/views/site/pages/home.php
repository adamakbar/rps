<?php
/* @var $this SiteController */

$this->pageTitle='Home';
  


?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
        <div class="row-fluid">
            <div class="span12"></div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="alert alert-block alert-warning fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4 class="alert-heading">Warning!</h4>
                    <p>
                        Anda belum mengisi Working Area Profile.
                    </p>
                </div>
                <div class="alert alert-block alert-info fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4 class="alert-heading">Info!</h4>
                    <p>
                        Lakukan verifikasi data dengan memilih menu Data Summary > Verifikasi.
                    </p>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN BAGAN-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-picture"></i> BAGAN REVITALISASI PELAPORAN DAN SUMBERDAYA</h4>
                    </div>
                    <div class="widget-body">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/rtor.png" alt="rtor" />
                    </div>
                </div>
                <!-- END BAGAN-->
                
            </div>            
        </div>
    </div>
</div>
 
