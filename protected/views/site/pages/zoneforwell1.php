<!-- Zone for Well 1-->
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#tabcol_w1_2">
                                                                <strong>Zone Data</strong>
                                                            </a>
                                                        </div>
                                                        <div id="tabcol_w1_2" class="accordion-body collapse">
                                                            <div class="accordion-inner">
                                                                <div class="control-group">
                                                                    <label class="control-label">Number of Penetrated Production Zones :</label>
                                                                    <div id="wajib" class="controls">
                                                                        <input id="wz" class="span3" type="text" style="text-align: center;"/>
                                                                    </div>
                                                                </div>
                                                                <!-- Notification -->
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <blockquote>
                                                                            <small>Detail of zone data can be filled at generated form below.</small>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                                <!-- Notification -->
                                                                <div class="row-fluid">
                                                                    <div class="span12">        
                                                                        <!-- Number Zone -->
                                                                        <div id="rumah_zone" class="tabbable tabbable-custom">
                                                                            <ul class="nav nav-tabs">
                                                                                <li class="active"><a id="zone_1" href="#tab_z1" data-toggle="tab">Zone 1</a></li>
                                                                                <li><a id="zone_2" href="#tab_z2" data-toggle="tab" class="hidden">Zone 2</a></li>
                                                                                <li><a id="zone_3" href="#tab_z3" data-toggle="tab" class="hidden">Zone 3</a></li>
                                                                                <li><a id="zone_4" href="#tab_z4" data-toggle="tab" class="hidden">Zone 4</a></li>
                                                                                <li><a id="zone_5" href="#tab_z5" data-toggle="tab" class="hidden">Zone 5</a></li>
                                                                            </ul>
                                                                            <div class="tab-content">
                                                                                <!-- Zone 1 -->
                                                                                <div class="tab-pane active" id="tab_z1">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z1_1">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z1_1" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="zonename" class="span3" required type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest" class="span3" required type="text" style="text-align: center;"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z1_3" required type="text" class="m-wrap medium" style="max-width:161px;" />
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zonethickness" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zoneinterval" required type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="perforation_interval" type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest_type" class="span3" type="text" style="text-align: center;"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="welltest_total" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialflow" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialshutin" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubingsize" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">in</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialtemperature" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialreservoir" required type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudostate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_presure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_for" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_head" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_pressure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="avg_porpsity" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="watercut" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                            <input id="initialwater_sat" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_gas" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_oil" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="freewater" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/>                                                                                        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore" type="text" style="max-width:138px;" />
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore_time" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rsbt_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z1_2">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z1_2" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilchoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gaschoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio" type="text" style="max-width:139px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="condensategas" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_gas" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_oil" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="diffusity_fact" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="permeability" type="text" style="max-width:158px;" />
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Infinite-acting Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="iafit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pseudostate Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pfit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir or Boundary Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_bondradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="deltaP" type="text" style="max-width:155px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="wellbore_skin" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="rock_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="fluid_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="total_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="secd_porosity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">I :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="I_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">W :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="W_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z1_3">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z1_3" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellmak_watercut" type="text" style="max-width:165px"/>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">GWC</span><input id="water_bearing1" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">OWC</span><input id="water_bearing3" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z1_4">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z1_4" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rock_sampling" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="petro_analys" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ticsd_" type="text" style="max-width:145px;"/>
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="bicsd_" type="text" style="max-width:145px;" />
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="num_totalcare" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="1corebarrel" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="totalrec" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="precore" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="scal_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z1_5">
                                                                                                    <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z1_5" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z1_1_5" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="sampleat" type="text" style="max-width:160px;"/>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio_" type="text" style="max-width:135px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_temperature" type="text" style="max-width:163px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="casing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sampleby" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="repost_avail" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="hydro_finger" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="condensategrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="pvt_" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasdev_fac" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <th>Average</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom"  class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilviscocity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="control-group">
                                                                                        <label class="control-label">Remark This Zone :</label>
                                                                                        <div class="controls">
                                                                                            <textarea id="well1_rmk_z1" class="span3" row="2" style="text-align: left;" ></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Zone 2 -->
                                                                                <div class="tab-pane" id="tab_z2">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z2_1">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z2_1" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="zonename" class="span3" required type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest2" class="span3" required type="text" style="text-align: center;"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z2_3" required type="text" class="m-wrap medium" style="max-width:161px;" />
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zonethickness" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zoneinterval" required type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="perforation_interval" type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest_type2" class="span3" type="text" style="text-align: center;"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="welltest_total" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialflow" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialshutin" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubingsize" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">in</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialtemperature" required type="text" style="max-width:161px;" />
                                                                                                             <span class="add-on"><sup>o</sup>C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialreservoir" required type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudostate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_presure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_for" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_head" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_pressure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="avg_porpsity" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="watercut" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialwater_sat" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_gas" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_oil" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="freewater" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/>                                                                                        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore" type="text" style="max-width:138px;" />
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore_time" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rsbt_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z2_2">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z2_2" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilchoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gaschoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio" type="text" style="max-width:139px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="condensategas" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_gas" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_oil" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="diffusity_fact" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="permeability" type="text" style="max-width:158px;" />
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Infinite-acting Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="iafit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pseudostate Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pfit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir or Boundary Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_bondradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="deltaP" type="text" style="max-width:155px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="wellbore_skin" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="rock_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="fluid_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="total_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="secd_porosity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">I :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="I_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">W :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="W_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z2_3">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z2_3" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellmak_watercut" type="text" style="max-width:165px"/>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">GWC</span><input id="water_bearing1" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">OWC</span><input id="water_bearing3" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z2_4">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z2_4" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rock_sampling2" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="petro_analys2" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ticsd_" type="text" style="max-width:145px;"/>
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="bicsd_" type="text" style="max-width:145px;" />
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="num_totalcare" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="1corebarrel" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="totalrec" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="precore" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="scal_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z2_5">
                                                                                                <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z2_5" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z2_1_5" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="sampleat" type="text" style="max-width:160px;"/>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio_" type="text" style="max-width:135px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_temperature" type="text" style="max-width:163px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="casing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sampleby" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="repost_avail" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="hydro_finger" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="condensategrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="pvt_" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasdev_fac" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <th>Average</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom"  class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilviscocity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="control-group">
                                                                                        <label class="control-label">Remark This Zone :</label>
                                                                                        <div class="controls">
                                                                                            <textarea id="well1_rmk_z2" class="span3" row="2" style="text-align: left;" ></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Zone 3 -->
                                                                                <div class="tab-pane" id="tab_z3">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z3_1">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z3_1" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="zonename" class="span3" required type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest3" class="span3" required type="text" style="text-align: center;"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z3_3" required type="text" class="m-wrap medium" style="max-width:161px;" />
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zonethickness" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zoneinterval" required type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="perforation_interval" type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest_type3" class="span3" type="text" style="text-align: center;"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="welltest_total" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialflow" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialshutin" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubingsize" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">in</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialtemperature" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialreservoir" required type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudostate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_presure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_for" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_head" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_pressure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="avg_porpsity" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="watercut" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialwater_sat" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_gas" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_oil" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="freewater" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/>                                                                                        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore" type="text" style="max-width:138px;" />
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore_time" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rsbt_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z3_2">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z3_2" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilchoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gaschoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio" type="text" style="max-width:139px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="condensategas" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_gas" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_oil" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="diffusity_fact" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="permeability" type="text" style="max-width:158px;" />
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Infinite-acting Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="iafit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pseudostate Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pfit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir or Boundary Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_bondradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="deltaP" type="text" style="max-width:155px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="wellbore_skin" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="rock_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="fluid_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="total_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="secd_porosity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">I :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="I_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">W :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="W_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z3_3">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z3_3" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellmak_watercut" type="text" style="max-width:165px"/>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">GWC</span><input id="water_bearing1" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">OWC</span><input id="water_bearing3" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z3_4">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z3_4" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rock_sampling3" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="petro_analys3" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ticsd_" type="text" style="max-width:145px;"/>
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="bicsd_" type="text" style="max-width:145px;" />
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="num_totalcare" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="1corebarrel" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="totalrec" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="precore" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="scal_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>P10 (Max)</th>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                               <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>P50 (Mean)</th>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>P90 (Min)</th>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Estimated Forecast</th>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Average</th>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>P10 (Max)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>P50 (Mean)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>P90 (Min)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Estimated Forecast</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Average</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z3_5">
                                                                                                    <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z3_5" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z3_1_5" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="sampleat" type="text" style="max-width:160px;"/>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio_" type="text" style="max-width:135px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_temperature" type="text" style="max-width:163px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="casing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sampleby" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="repost_avail" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="hydro_finger" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="condensategrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="pvt_" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasdev_fac" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <th>Average</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom"  class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilviscocity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="control-group">
                                                                                        <label class="control-label">Remark This Zone :</label>
                                                                                        <div class="controls">
                                                                                            <textarea id="well1_rmk_z3" class="span3" row="2" style="text-align: left;" ></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Zone 4 -->
                                                                                <div class="tab-pane" id="tab_z4">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z4_1">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z4_1" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="zonename" class="span3" required type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest4" class="span3" required type="text" style="text-align: center;"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z4_3" required type="text" class="m-wrap medium" style="max-width:161px;" />
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zonethickness" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zoneinterval" required type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="perforation_interval" type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest_type4" class="span3" type="text" style="text-align: center;"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="welltest_total" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialflow" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialshutin" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubingsize" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">in</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialtemperature" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialreservoir" required type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudostate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_presure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_for" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_head" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_pressure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="avg_porpsity" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="watercut" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                             <input id="initialwater_sat" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_gas" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_oil" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="freewater" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/>                                                                                        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/>                                                                                        
                                                                                                            
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore" type="text" style="max-width:138px;" />
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore_time" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rsbt_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z4_2">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z4_2" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilchoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gaschoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio" type="text" style="max-width:139px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="condensategas" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_gas" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_oil" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="diffusity_fact" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="permeability" type="text" style="max-width:158px;" />
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Infinite-acting Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="iafit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pseudostate Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pfit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir or Boundary Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_bondradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="deltaP" type="text" style="max-width:155px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="wellbore_skin" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="rock_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="fluid_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="total_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="secd_porosity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">I :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="I_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">W :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="W_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well_z4_3">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well_z4_3" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellmak_watercut" type="text" style="max-width:165px"/>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">GWC</span><input id="water_bearing1" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">OWC</span><input id="water_bearing3" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z4_4">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z4_4" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rock_sampling4" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="petro_analys4" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ticsd_" type="text" style="max-width:145px;"/>
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="bicsd_" type="text" style="max-width:145px;" />
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="num_totalcare" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="1corebarrel" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="totalrec" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="precore" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="scal_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z4_5">
                                                                                                    <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z4_5" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z4_1_5" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="sampleat" type="text" style="max-width:160px;"/>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio_" type="text" style="max-width:135px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_temperature" type="text" style="max-width:163px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="casing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sampleby" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="repost_avail" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="hydro_finger" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="condensategrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="pvt_" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasdev_fac" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <th>Average</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom"  class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilviscocity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="control-group">
                                                                                        <label class="control-label">Remark This Zone :</label>
                                                                                        <div class="controls">
                                                                                            <textarea id="well1_rmk_z4" class="span3" row="2" style="text-align: left;" ></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Zone 5 -->
                                                                                <div class="tab-pane" id="tab_z5">
                                                                                    <div class="accordion">
                                                                                        <!-- Zone General Data -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z5_1">
                                                                                                    <strong>Zone General Data</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z5_1" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Name :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="zonename" class="span3" required type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest5" class="span3" required type="text" style="text-align: center;"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z5_3" required type="text" class="m-wrap medium" style="max-width:161px;" />
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Thickness :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zonethickness" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Zone Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="zoneinterval" required type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Perforation Interval Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="perforation_interval" type="text" style="max-width:148px;" />
                                                                                                                <span class="add-on">ftMD</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="welltest_type5" class="span3" type="text" style="text-align: center;"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Test Total Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="welltest_total" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Flow Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialflow" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Shutin Duration :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialshutin" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">h</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Size :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubingsize" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">in</i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialtemperature" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="initialreservoir" required type="text" style="max-width:150px;" class="popovers" data-trigger="hover" data-placement="right" data-container="body" data-content="Reservoir or well pressure from well analysis result (Bottom-hole Pressure)." data-original-title="Initial Reservoir Pressure"/>
                                                                                                                <span class="add-on">psig</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Pseudostate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_presure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Formation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_for" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pressure Well Head :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pressurewell_head" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Pressure Wellbore storage :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_pressure" type="text" style="max-width:150px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Average Porosity :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="avg_porpsity" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="watercut" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Initial Water Saturation :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                             <input id="initialwater_sat" required type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_gas" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Lowest Tested Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="lowtest_oil" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Free Water Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="freewater" type="text" style="max-width:161px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gas_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of gas density with air density" data-original-title="Gas Gravity"/>                                                                                        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oil_grav" class="span3 popovers" type="text" style="text-align: center;" data-trigger="hover"  data-container="body" data-content="Ratio of oil density with air density (API units)" data-original-title="Oil Gravity"/>                                                                                        
                                                                                                            
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Coefficient :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore" type="text" style="max-width:138px;" />
                                                                                                                <span class="add-on">bbl/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Storage Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellbore_time" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir Shape or Boundary Type :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rsbt_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Production Rate -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z5_2">
                                                                                                    <strong>Production Rate</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z5_2" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilchoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Choke :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gaschoke" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Flow Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasflow" required type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio" type="text" style="max-width:139px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label"> Condensate Gas Ratio:</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="condensategas" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Gas :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_gas" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">scf</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Cummulative Production Oil :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="cummulative_pro_oil" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">stb</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Absolute Open Flow :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="ab_openflow2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Critical Rate :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="criticalrate2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Production Index :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index1" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">bbl/d</span>
                                                                                                            </div>
                                                                                                            <div class="input-append">
                                                                                                                <input id="pro_index2" type="text" style="max-width:149px; margin-left:24px;" />
                                                                                                                <span class="add-on">scf/d</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Diffusity Factor :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="diffusity_fact" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Permeability :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="permeability" type="text" style="max-width:158px;" />
                                                                                                                <span class="add-on">mD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Infinite-acting Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="iafit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Pseudostate Final Investigation Time :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="pfit_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">h</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reservoir or Boundary Radius :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="res_bondradius" required type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                                <p class="help-block"></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Delta P Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="deltaP" type="text" style="max-width:155px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Wellbore Skin :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="wellbore_skin" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="rock_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Fluid Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="fluid_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Compressibility :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="total_compress" type="text" style="max-width:149px;" />
                                                                                                                <span class="add-on">1/psi</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Secondary Porosity Parameter :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="secd_porosity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">I :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="I_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">W :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="W_" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Hydrocarbon Indication -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z5_3">
                                                                                                    <strong>Hydrocarbon Indication</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z5_3" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="oilshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Show or Reading :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read1" required class="span3" type="text"/>
                                                                                                            <p class="help-block"></p>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">By Tools Indication :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasshow_read2" class="span3" type="text"/>
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Well Making Water Cut :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="wellmak_watercut" type="text" style="max-width:165px"/>
                                                                                                                <span class="add-on">%</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">GWC</span><input id="water_bearing1" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing2" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>                                                            
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Water Bearing Level Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-prepend input-append">
                                                                                                                <span class="add-on">OWC</span><input id="water_bearing3" type="text" style="max-width:120px"/><span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                            <input id="water_bearing4" class="span3" type="text" style="margin-left:24px;" placeholder="By Tools Indication" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Rock Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z5_4">
                                                                                                    <strong>Rock Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z5_4" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Rock Sampling Method :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rock_sampling5" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Petrography Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="petro_analys5" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Top Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="ticsd_" type="text" style="max-width:145px;"/>
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Bottom Interval Coring Sample Depth :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="bicsd_" type="text" style="max-width:145px;" />
                                                                                                                <span class="add-on">ftTVD</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Number of Total Core Barrels :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="num_totalcare" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">1 Core Barrel Equal to :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="1corebarrel" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Total Recoverable Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="totalrec" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Preservative Core Data :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="precore" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Routine Core Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="rca_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="rca_" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">SCAL Data Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="scal_1" class="span3" type="text" style="text-align:center;" />
                                                                                                            <input id="scal_2" class="span3" type="text" placeholder="Sample Quantity.."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Clastic Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Reservoir Vshale Content (GR Log)</th>
                                                                                                                    <th>Reservoir Vshale Content (SP Log)</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">API</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">mV</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of reservoir net thickness with reservoir gross thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Carbonate Reservoir Property (by Electrolog)</th>
                                                                                                                    <th>Gross Reservoir Thickness</th>
                                                                                                                    <th>Thickness Reservoir Pore Throat Connectivity (DTC)</th>
                                                                                                                    <th>Thickness Reservoir Total Pore</th>
                                                                                                                    <th>Net to Gross</th>
                                                                                                                    <th>Reservoir Porosity</th>
                                                                                                                    <th>Reservoir Saturation (Cut)</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th>Average</th>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">usec/ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">ft</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                    <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"readonly=""/><span class="add-on">%</i></span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Ratio of porous reservoir thickness with reservoir thickness</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- Fluid Property (by Sampling) -->
                                                                                        <div class="accordion-group">
                                                                                            <div class="accordion-heading">
                                                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#well1_z5_5">
                                                                                                    <strong>Fluid Property (by Sampling)</strong>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div id="well1_z5_5" class="accordion-body collapse">
                                                                                                <div class="accordion-inner">
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Date :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="z5_1_5" type="text" class="m-wrap medium popovers" style="max-width:160px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Sample Date"/>
                                                                                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled at :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="sampleat" type="text" style="max-width:160px;"/>
                                                                                                                <span class="add-on">ft</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Oil Ratio :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasoilratio_" type="text" style="max-width:135px;" />
                                                                                                                <span class="add-on">scf/bbl</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Separator Temperature :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="separator_temperature" type="text" style="max-width:163px;" />
                                                                                                                <span class="add-on"><sup>o</sup>C</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Tubing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="tubing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Casing Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="casing_pressure" type="text" style="max-width:153px;" />
                                                                                                                <span class="add-on">psig</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sampled by :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sampleby" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Reports Availability :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="repost_avail" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Hydrocarbon Fingerprint Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="hydro_finger" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="gasgrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Condensate Gravity at 60 °F :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="condensategrav_60" type="text" style="max-width:160px;" />
                                                                                                                <span class="add-on">API</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">PVT Analysis :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="pvt_" class="span3" type="text" style="text-align:center;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Sample Quantity :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="sample_" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Gas Deviation Factor (Initial Z) :</label>
                                                                                                        <div class="controls">
                                                                                                            <input id="gasdev_fac" class="span3" type="text"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <table class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Initial Formation Volume Factor</th>
                                                                                                                    <th>P10 (Max)</th>
                                                                                                                    <th>P50 (Mean)</th>
                                                                                                                    <th>P90 (Min)</th>
                                                                                                                    <th>Estimated Forecast</th>
                                                                                                                    <th>Average</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <th>Oil (Boi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th>Gas (Bgi)</th>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text"/><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom"  class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                                <td><div class=" input-append"><input id="#" name="custom" class="span6" type="text" readonly="" /><span class="add-on">bbl/stb</i></span></td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <blockquote>
                                                                                                            <small>Please fill Bgi, Boi, or Both based on Hydrocarbon Indication, fill with 0 at Bgi if only Boi available or vice versa.</small>
                                                                                                        </blockquote>
                                                                                                    </div>
                                                                                                    <div class="control-group">
                                                                                                        <label class="control-label">Oil Viscocity at Initial Reservoir Pressure :</label>
                                                                                                        <div class="controls">
                                                                                                            <div class="input-append">
                                                                                                                <input id="oilviscocity" type="text" style="max-width:165px;" />
                                                                                                                <span class="add-on">cP</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="control-group">
                                                                                        <label class="control-label">Remark This Zone :</label>
                                                                                        <div class="controls">
                                                                                            <textarea id="well1_rmk_z5" class="span3" row="2" style="text-align: left;" ></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- End Number Zone -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                                        
                                                    </div>