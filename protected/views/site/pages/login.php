<?php
/* @var $this SiteController */

$this->pageTitle='Login';
$this->breadcrumbs=array(
	'Login',
); 
?>

<!-- BEGIN BODY -->
<body id="login-body">
  <div class="login-header">
      <!-- BEGIN LOGO -->
      <div id="logo" class="center">
          <img src="images/logo1.png" alt="logo" class="center" />
      </div>
      <!-- END LOGO -->
  </div>

  <!-- BEGIN LOGIN -->
  <div id="login">
    <!-- BEGIN LOGIN FORM -->
    <form id="loginform" class="form-vertical no-padding no-margin" action="<?php echo Yii::app()->createUrl('/site/page', array('view'=>'home'));?>">     
      <div class="control-wrap">
        <div class="control-group">
              <div class="controls">
                  <div class="input-prepend">
                      <span class="add-on"><i class="icon-user"></i></span><input id="input-username" type="text" placeholder="Username" />
                  </div>
              </div>
          </div>
          <div class="control-group">
              <div class="controls">
                  <div class="input-prepend">
                      <span class="add-on"><i class="icon-key"></i></span><input id="input-password" type="password" placeholder="Password" />
                  </div>
              </div>
          </div>
      </div>
      <input type="submit" class="btn btn-block login-btn" value="Login"/>
    </form>
    <!-- END LOGIN FORM -->
  </div>
  <!-- END LOGIN -->  
</body>
<!-- END BODY -->
