<?php
/* @var $this SiteController */

$this->pageTitle='Maps';
$this->breadcrumbs=array(
	'Maps',
); 
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->			    			
			<h3 class="page-title">UPLOAD MAPS</h3>
			<ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Upload Maps</a><span class="divider-last">&nbsp;</span></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
		<div class="row-fluid">
			<div class="span12">
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-globe"></i> Drop Maps</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body form">
						<form action="http://thevectorlab.net/adminlab/assets/dropzone/upload.php" class="dropzone" id="my-awesome-dropzone">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN GALLERY MANAGER PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-camera-retro"></i>Gallery</h4>
					<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					</span>							
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="span4">
							<div class="thumbnail">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo1.jpg">
										<div class="zoom">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo1.jpg" alt="Photossssssssss" />
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="thumbnail">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo2.jpg">
										<div class="zoom">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo2.jpg" alt="Photo" />
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="thumbnail">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo3.jpg">
										<div class="zoom">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo3.jpg" alt="Photo" />
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="space10"></div>
					<div class="row-fluid">
						<div class="span4">
							<div class="thumbnail">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo4.jpg">
										<div class="zoom">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo4.jpg" alt="Photo" />
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="thumbnail">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo5.jpg">
										<div class="zoom">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo5.jpg" alt="Photo" />										
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="thumbnail">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo6.jpg">
										<div class="zoom">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/photo6.jpg" alt="Photo" />
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="space10"></div>
					<!-- END GALLERY MANAGER LISTING-->
				</div>
			</div>
			<!-- END GALLERY MANAGER PORTLET-->
		</div>
	</div>	
</div>
			

