<?php
/* @var $this SiteController */

$this->pageTitle='Lead';
$this->breadcrumbs=array(
	'Lead',
);
?>

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">	    			
			<h3 class="page-title">LEAD<small> Peluang Pelamparan Hidrokaron</small></h3>
            <ul class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                <li><a href="#">Input RPS</a> <span class="divider">&nbsp;</span></li>
                <li><a href="#"><strong>Lead</strong></a><span class="divider-last">&nbsp;</span></li>
            </ul>
		</div>
	</div>
	<!-- BEGIN PAGE CONTENT-->
	<div id="page">
        <div class="row-fluid">
			<div class="span12">
				<!-- BEGIN LIST DATA LEAD -->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-table"></i> LIST DATA LEAD</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Lead Name</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>SANDSTONE.DELTA SLOPE.TAF-Talang Akar/Upper Talang Akar/Pre-Talangakar.OLIGOCENE.STRUCTURAL TECTONIC EXTENSIONAL</td>
                                <td>14/06/2013</td>
                                <td><strong>Original</strong></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><a href="#_gen_data_lead" class="nyekrol">REEF CARBONATE.REEFAL.BRF-Pre-Baturaja/Baturaja.MIOCENE.STRATIGRAPHIC DEPOSITIONAL REEF</a></td>
                                <td>23/09/2013</td>
                                <td><strong>Update</strong></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>PLATFORM CARBONATE.PLATFORM MARGIN.KJG-Kujung I/Kujung/Kujung 1 Reef/Kujung I of Kujung Fm.MIOCENE.STRUCTURAL TECTONIC COMPRESSIONAL INVERTED</td>
                                <td>30/12/2013</td>
                                <td><strong>Update</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END LIST DATA LEAD -->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN GENERAL DATA-->
                <div id="_gen_data_lead" class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i> GENERAL DATA</h4>
                        <span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                            <div class="accordion" id="accordion1a">
                                <!-- Begin Lead Administration Data -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Lead Administration Data</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info Lead Name!</strong> 
                                                    <p>For one Lead structure, please use same name with alphabetical or number postfix, use punctuation mark `-` (hyphen) without space before and after to separate Lead name, i.e : Garuda-1, Garuda-2 or Merah Putih-A, Merah Putih-B. Lead name must be unique within Working Area and meaningful, name that only have word like, i.e: Lead-A, Prospect-1; is not acceptable, if Lead already given name like that, we advise to change it and contact SKK Migas.</p>
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <label class="control-label">Lead Name :</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" required/>
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Play :</label>
                                                <div id="wajib"class="controls">
                                                    <input type="hidden" id="play_in" class="span6" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <blockquote>
                                                        <small>For one Lead structure, Play must be different with other Lead.</small>
                                                    </blockquote>
                                                </div>
                                            </div>                            
                                            <div class="control-group">
                                                <label class="control-label">Clarified by :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="_clarifiedby" class="span3" type="text" style="text-align: center;"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Year of Study :</label>
                                                <div class="controls">
                                                    <input id="_yearofstudy" class="span3" type="text" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lead Name Initiation Date :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="l5" type="text" class="m-wrap medium popovers" style="max-width:172px;" data-trigger="hover"  data-container="body" data-content="If Year that only available, please choose 1-January for Day and Month, if not leave it blank." data-original-title="Lead Name Initiation Date"/>
                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Lead Administration Data -->
                                <!-- Begin Geographical Lead Area -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen2_lead">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Geographical Lead Area</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen2_lead" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info!</strong> For one Lead structure, please fill with exact same coordinate or with difference of 25'' with other Lead of one structure
                                                </div>
                                            </div>
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <label class="control-label">Center Latitude :</label>
                                                <div class="controls">
                                                    <table>
                                                    <tr class="">
                                                    <td>
                                                        <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                        <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                        <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div>
                                                        <input type="text" class="input-mini" placeholder="S/ N"/><span class="add-on"></span>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-group">
                                                    <label class="control-label">Center Longitude :</label>
                                                    <div class="controls">
                                                        <table>
                                                        <tr class="">
                                                        <td>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="degree"/><span class="add-on"><sub>o</sub></span></div></div>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="minute"/><span class="add-on">'</span></div></div>
                                                            <div class=" input-append"><div class=" input-append"><input type="text" class="input-mini" placeholder="second"/><span class="add-on">"</span></div></div>
                                                            <input type="text" class="input-mini" placeholder="E/ W"/><span class="add-on"></span>
                                                        </td>
                                                        </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Geographical Lead Area -->
                                <!-- Begin Data Environment -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Environment</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Onshore or Offshore :</label>
                                                <div id="wajib"class="controls">
                                                    <input type="hidden" id="env_onoffshore" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Terrain :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="env_terrain" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Environment -->
                                <!-- Begin Data Adjacent Activity :  -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gen4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong>  Adjacent Activity</strong>
                                        </a>
                                    </div>
                                    <div id="col_gen4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Nearby Facility :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required id="n_facility" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest facility, if more than 100 Km, leave it blank." data-original-title="Nearby Facility"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>                    
                                            <div class="control-group">
                                                <label class="control-label">Nearby Development Well :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required id="n_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest development well, if more than 100 Km, leave it blank." data-original-title="Nearby Development Well"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Nearby Exploration Well :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input required id="n_exploration_developmentwell" name="custom" type="text" style="max-width:165px" class="popovers" data-trigger="hover"  data-container="body" data-content="Estimated range to the nearest exploration development well, if more than 100 Km, leave it blank." data-original-title="Nearby Exploration Development Well"/>
                                                        <span class="add-on">Km</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Adjacent Activity : -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END GENERAL DATA-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN DATA AVAILABILITY -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-paper-clip"></i> SURVEY DATA AVAILABILITY</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">                                                   	
                        	<div class="accordion">
                        		<div class="accordion-group">
                        			<div class="accordion-heading"><div class="geser" ></div>
                                        <input id="LGfs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LGfs','LGfs_link','col_dav1')" /> 
                                        <a id="LGfs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav1">Geological Field Survey</a>
                        			</div>
                        			<div id="col_dav1" class="accordion-body collapse">
                        				<div class="accordion-inner">
	                                        <div class="control-group">
	                                        	<label class="control-label">Acquisition Year :</label>
	                                        	<div class="controls">
                                                    <input id="dav_gfs_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
	                                        	</div>
	                                        </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Survey Method :</label>
	                                        	<div class="controls">
	                                        		<input id="dav_gfs_surmethod" class="span3" style="text-align: center;"/>
	                                        	</div>
	                                        </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Total Coverage Area : </label>
	                                        	<div class="controls">
	                                        		<div class=" input-append">
	                                        			<input id="dav_gfs_tca" type="text" style="max-width:170px"/>
	                                        			<span class="add-on">ac</span>
	                                        		</div>
	                                        	</div>
	                                        </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Low Estimate :</label>
	                                        	<div class="controls">
	                                        		<div class="input-append">
	                                        			<input id="dav_gfs_lowes" name="custom" type="text" style="max-width:170px" required/>
	                                        			<span class="add-on">ac</span>
                                                        <p class="help-block"></p>
	                                        		</div>
	                                        	</div>
	                                        </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Best Estimate :</label>
	                                        	<div class="controls">
	                                        		<div class=" input-append">
	                                        			<input id="dav_gfs_bestes" name="custom" type="text" style="max-width:170px" requierd/>
	                                        			<span class="add-on">ac</span>
                                                        <p class="help-block"></p>
	                                        		</div>
	                                        	</div>
	                                        </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">High Estimate :</label>
	                                        	<div class="controls">
	                                        		<div class=" input-append">
	                                        			<input id="dav_gfs_highes" name="custom" type="text" style="max-width:170px" required/>
	                                        			<span class="add-on">ac</span>
                                                        <p class="help-block"></p>
	                                        		</div>
	                                        	</div>
	                                        </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong> </strong></label>
                                            </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Remarks :</label>
	                                        	<div class="controls">
	                                        		<textarea id="dav_gfs_remarks" class="span3" rows="2"></textarea>
	                                        	</div>
	                                        </div>
	                                    </div>
                        			</div>
                        		</div>
                        		<div class="accordion-group">
                        			<div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="LDss" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LDss','LDss_link','col_dav2' )"/> 
                                            <a id="LDss_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav2"> 2D Seismic Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav2" class="accordion-body collapse" >
                        				<div class="accordion-inner">                                   
                        					<div class="control-group">
                        						<label class="control-label">Acquisition Year:</label>
                        						<div class="controls">
                                                    <input id="dav_2dss_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
                        						</div>
                        					</div>
                                            <div class="control-group">
                                                <label class="control-label">Number Of Vintage :</label>
                                                <div class="controls">
                                                    <input id="dav_2dss_numvin" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                        					<div class="control-group">
                        						<label class="control-label">Total Crossline Number :</label>
                        						<div class="controls">
                        							<input id="dav_2dss_tcn" type="text" class="span3"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Total Coverage Area :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_2dss_tca" type="text" style="max-width:168px"/>
                        								<span class="add-on">km<sup>2</sup></span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Average Spacing Parallel Interval :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_2dss_aspi" type="text" style="max-width:170px"/>
                        								<span class="add-on">km</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Latest Processing Year :</label>
                        						<div class="controls">
                                                    <input id="dav_2dss_lpy" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Only latest processing year" data-original-title="Latest Processing Year"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Latest Processing Method :</label>
                        						<div class="controls">
                        							<input id="dav_2dss_lpm" style="text-align: center;" class="span3"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Seismic Image Quality :</label>
                        						<div class="controls">
                        							<input id="dav_2dss_siq" class="span3" style="text-align: center;" />
                        						</div>
                        					</div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
	                                        <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="dav_2dss_lowes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_2dss_bestes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_2dss_highes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>                                           
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Remarks :</label>
	                                        	<div class="controls">
	                                        		<textarea id="dav_2dss_remarks" class="span3" rows="2"></textarea>
	                                        	</div>
	                                        </div>
                        				</div>
                        			</div>
                        		</div>
                        		<div class="accordion-group">
                        			<div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="LGras" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LGras','LGras_link','col_dav3' )"/> 
                                            <a id="LGras_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav3"> Gravity Survey</a>
                                        </div>
                                    </div>
                                    <div id="col_dav3" class="accordion-body collapse">
                        				<div class="accordion-inner">
                        					<div class="control-group">
                        						<label class="control-label">Acquisition Year:</label>
                        						<div class="controls">
                                                    <input id="dav_grasur_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Survey Methods :</label>
                        						<div class="controls">
                        							<input id="dav_grasur_surveymethods" class="span3" style="text-align: center;" />
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Survey Coverage Area :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_grasur_sca" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ac</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Depth Survey Penetration Range:</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_grasur_dspr" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Recorder Spacing Interval :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_grasur_rsi" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>                                            
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
	                                        <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="dav_grasur_lowes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_grasur_bestes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_grasur_highes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Remarks :</label>
	                                        	<div class="controls">
	                                        		<textarea id="dav_grasur_remarks" class="span3" rows="2"></textarea>
	                                        	</div>
	                                        </div>
                        				</div>
                        			</div>
                        		</div>                        		                        		
                        		<div class="accordion-group">
                        			<div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="LGeos" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LGeos','LGeos_link','col_dav4' )"/> 
                                            <a id="LGeos_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav4"> Geochemistry Survey</a>
                                        </div>
                        			</div>
                        			<div id="col_dav4" class="accordion-body collapse">
                        				<div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year :</label>
                                                <div class="controls">
                                                    <input id="dav_geo_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                        					<div class="control-group">
                        						<label class="control-label">Interval Samples Range :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_geo_isr" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Number of Sample Location :</label>
                        						<div class="controls">
                        							<input id="dav_geo_numsample" type="text" class="span3"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Number of Rocks Sample :</label>
                        						<div class="controls">
                        							<input id="dav_geo_numrock" type="text" class="span3"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Number of Fluid Sample :</label>
                        						<div class="controls">
                        							<input id="dav_geo_numfluid" type="text" class="span3"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Availability HC Composition :</label>
                        						<div class="controls">
                        							<input id="dav_geo_ahc" class="span3" style="text-align: center;" />
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Hydrocarbon Composition : </label>
                        						<div class="controls">
                        							<input id="dav_geo_hycomp" type="text" class="span3"/>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Laboratorium Evaluation & Report Year</label>
                        						<div class="controls">
                                                    <input id="dav_geo_lery" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year"/>
                        						</div>
                        					</div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="dav_geo_lowes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_geo_bestes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_geo_highes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                        					<div class="control-group">
                        						<label class="control-label">Remarks :</label>
                        						<div class="controls">
                        							<textarea id="dav_geo_m" class="span3" rows="2"></textarea>
                        						</div>
                        					</div>
                        				</div>
                        			</div>
                        		</div>
                        		<div class="accordion-group">
                        			<div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="LEs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LEs','LEs_link','col_dav5' )"/> 
                                            <a id="LEs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav5"> Electromagnetic Survey</a>
                                        </div>
                        			</div>
                        			<div id="col_dav5" class="accordion-body collapse">
                        				<div class="accordion-inner">
                        					<div class="control-group">
                        						<label class="control-label">Acquisition Year:</label>
                        						<div class="controls">
                                                    <input id="dav_elec_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Survey Methods :</label>
                        						<div class="controls">
                        							<input id="dav_elec_surveymethods" class="span3" style="text-align: center;" />
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Survey Coverage Area :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_elec_sca" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ac</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Depth Survey Penetration Range :</label>
                        						<div class="controls">
                        							<div class="input-append">
                        								<input id="dav_elec_dspr" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Recorder Spacing Interval :</label>
                        						<div class="controls">
                        							<div class="input-append">
                        								<input id="dav_elec_rsi" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="dav_elec_lowes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_elec_bestes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_elec_highes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                        					<div class="control-group">
                        						<label class="control-label">Remarks :</label>
                        						<div class="controls">
                        							<textarea id="dav_elec_remarks" class="span3" rows="2"></textarea>
                        						</div>
                        					</div>
                        				</div>
                        			</div>
                        		</div>
                        		<div class="accordion-group">
                        			<div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="LRs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LRs','LRs_link','col_dav6' )"/> 
                                            <a id="LRs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav6"> Resistivity Survey</a>
                                        </div>
                        			</div>
                        			<div id="col_dav6" class="accordion-body collapse">
                        				<div class="accordion-inner">
                        					<div class="control-group">
                        						<label class="control-label">Acquisition Year:</label>
                        						<div class="controls">
                                                    <input id="dav_res_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Survey Methods :</label>
                        						<div class="controls">
                        							<input id="dav_res_surveymethods" class="span3" style="text-align: center;" />
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Survey Coverage Area :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_res_sca" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ac</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Depth Survey Penetration Range :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_res_dspr" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>
                        					<div class="control-group">
                        						<label class="control-label">Recorder Spacing Interval :</label>
                        						<div class="controls">
                        							<div class=" input-append">
                        								<input id="dav_res_rsi" type="text" style="max-width:170px;"/>
                        								<span class="add-on">ft</span>
                        							</div>
                        						</div>
                        					</div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="dav_res_lowes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_res_bestes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_res_highes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
                        					<div class="control-group">
                        						<label class="control-label">Remarks :</label>
                        						<div class="controls">
                        							<textarea id="dav_res_remarks" class="span3" rows="2"></textarea>
                        						</div>
                        					</div>
                        				</div>
                        			</div>
                        		</div>
                        		<div class="accordion-group">
                        			<div class="accordion-heading">
                                        <div class="accordion-heading"><div class="geser" ></div>
                                            <input id="LOs" class="cekbok" type="checkbox" value="" onclick="toggleStatus('LOs','LOs_link','col_dav7' )"/> 
                                            <a id="LOs_link" class="accordion-toggle collapsed" data-toggle="collapse" href="#col_dav7"> Other/ Others Survey</a>
                                        </div>
                        			</div>
                        			<div id="col_dav7" class="accordion-body collapse">
                        				<div class="accordion-inner">
                                            <!-- Notification -->
                                            <div class="control-group">
                                                <div class="alert alert-success">
                                                    <button class="close" data-dismiss="alert">×</button>
                                                    <strong>Info!</strong> Please provide more information about other survey in remark.
                                                </div>
                                            </div>
                                            <!-- Notification --> 
                                            <div class="control-group">
                                                <label class="control-label">Acquisition Year:</label>
                                                <div class="controls">
                                                    <input id="dav_other_aqyear" name="custom" type="text" class="span3 popovers" data-trigger="hover"  data-container="body" data-content="Please fill with format of 4-character year, if more than one separate with commas, i.e: 1945,2012." data-original-title="Acquisition Year" required/>
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong>Predicted 2D Areal Closure :</strong></label>
                                            </div>
                        					<div class="control-group">
                                                <label class="control-label">Low Estimate :</label>
                                                <div class="controls">
                                                    <div class="input-append">
                                                        <input id="dav_other_lowes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Best Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_other_bestes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">High Estimate :</label>
                                                <div class="controls">
                                                    <div class=" input-append">
                                                        <input id="dav_other_highes" name="custom" type="text" style="max-width:170px;" required/>
                                                        <span class="add-on">ac</span>
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><strong></strong></label>
                                            </div>
	                                        <div class="control-group">
	                                        	<label class="control-label">Remarks :</label>
	                                        	<div class="controls">
	                                        		<textarea id="dav_other_remarks" class="span3" rows="2"></textarea>
	                                        	</div>
	                                        </div>
                        				</div>
                        			</div>
                        		</div>                              
                           </div>                        
                        </form>
                    </div>
                </div>
                <!-- END DATA AVAILABILITY -->                
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN LEAD GCF -->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-list"></i> DESCRIPTION LEAD GEOLOGICAL CHANCE FACTOR</h4>
						<span class="tools"><a href="javascript:;" class="icon-chevron-down"></a></span>
                    </div>
                    <div class="widget-body">
                        <form action="#" class="form-horizontal">
                            <!-- Notification -->
                            <div class="control-group">
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">×</button>
                                    <strong>Lead Geological Chance Factor</strong>
                                    <p>When play chosen this GCF automatically filled with Play GCF, change according to newest GCF or leave as it is.</p>
                                </div>
                            </div>
                            <!-- Notification -->
                            <div class="accordion"> 
                                <!-- Begin Data Source Rock -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf1">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Source Rock</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Source Rock :</label>
                                                <div id="wajib" class="controls">
                                                    <input id="gcfsrock" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Age :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock1" class="span3" style="text-align: center;" />
                                                    <input id="gcfsrock1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Source Formation Name :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Type of Kerogen :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Capacity (TOC) :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Heat Flow Unit (HFU) :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Continuity :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Maturity :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Present of Other Source Rock :</label>
                                                <div class="controls">
                                                    <input id="gcfsrock9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Source Rock:</label>
                                                <div class="controls">
                                                    <textarea id="gcfsrock10" class="span3" row="2" style="text-align: left;" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Source Rock -->
                                <!-- Begin Data Reservoir -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf2">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Reservoir</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Reservoir :</label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="gcfres" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfres1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reservoir Formation Name :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Setting :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Depositional Environment :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Contuinity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Lithology :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Porosity Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div id="prim_pro" class="control-group">
                                                <label class="control-label">Primary Porosity (%) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfres9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div id="second_pro" class="control-group">
                                                <label class="control-label">Secondary Porosity :</label>
                                                <div class="controls">
                                                <input type="hidden" id="gcfres10" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Reservoir :</label>
                                                <div class="controls">
                                                    <textarea id="gcfres11" class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Reservoir -->
                                <!-- Begin Data Trap -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf3">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Trap</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Trap :</strong></label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="gcftrap" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap1" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcftrap1a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Formation Name :</label>
                                                <div class="controls">
                                                <input type="hidden" id="gcftrap2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Distribution :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Continuity :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap4" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Sealing Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="pgcftrap5" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Age :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap6" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcftrap6a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Geometry :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trapping Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap8" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Closure Type :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcftrap9" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Trap:</label>
                                                <div class="controls">
                                                    <textarea id="gcftrap10"class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Trap -->
                                <!-- Begin Data Dynamic -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#col_gcf4">
                                            <span class="add-on"><i class="icon-list-alt"></i></span><strong> Dynamic</strong>
                                        </a>
                                    </div>
                                    <div id="col_gcf4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <div class="control-group">
                                                <label class="control-label">Dynamic :</strong></label>
                                                <div id="wajib" class="controls">
                                                    <input type="hidden" id="gcfdyn" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Authenticate Migration :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn1" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Trap Position due to Kitchen :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn2" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Order to Establish Petroleum System :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn3" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Earliest) :</label>
                                                <div class="controls">
                                                <input type="hidden" id="gcfdyn4" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfdyn4a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Tectonic Regime (Latest) :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn5" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfdyn5a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Preservation/Segregation Post Entrapment :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn6" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Migration Pathways :</label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn7" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Estimation Migration Age : </label>
                                                <div class="controls">
                                                    <input type="hidden" id="gcfdyn8" class="span3" style="text-align: center;" />
                                                    <input type="hidden" id="gcfdyn8a" class="span3" style="text-align: center;" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Remark for Dynamic:</label>
                                                <div class="controls">
                                                    <textarea id="gcfdyn9"class="span3" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Data Dynamic -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- BEGIN LEAD GCF -->
            </div>
        </div>
    </div>
</div>
