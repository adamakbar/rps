<?php /* @var $this Controller */
$InputRPS_save = array('Play','Lead','Drillable', 'Drill','Discovery','WellInventory','Maps',"Profile","AddUser");
$Login_Page = array('Login');
$Home_Page = array('Home');

$InputRPS = array('Play','Lead','Drillable', 'Post Drill', 'Discovery','Well');
$DataSummary = array('Print','Data Verification');
$FileUpload = array('File Upload');
$Manual = array('Report Form & Instruction Manual');
$Help = array('Help');
$uploadImage = array('Image1', 'Image2', 'Image3', 'Image4', 'Image5', 'Image6', 'Image7', 'Image8', 'Image9', 'Image10', 'Image11', 'Image12', 'Image13', 'Image14', 'Image15', 'Image16');
$Query = array('ExportPlay', 'ExportLead', 'ExportDrillable', 'ExportPostdrill', 'ExportDiscovery');

?>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="language" content="en" />

  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.9.0.min.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-migrate-1.2.1.js"></script>

  <link href="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /> <!-- Untuk pemilihan zone, jika tidak kelima zone akan langsung muncul -->
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_responsive.css" rel="stylesheet" /> <!-- Untuk navbar menu -->
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_default.css" rel="stylesheet" id="style_color" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2-bootstrap.css" rel="stylesheet" type="text/css"/>

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/uniform/css/uniform.default.css" />

  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/editan.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>

<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="<?php if (in_array($this->pageTitle, $Login_Page)) {
    echo "";
    }else{
        echo "fixed-top";
        } ?>">

    <div id="header" class="navbar navbar-inverse navbar-fixed-top <?php if (in_array($this->pageTitle, $Login_Page)) {
        echo "hidden";
        }?>" >

        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="<?php echo Yii::app()->createUrl("/site/index"); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" alt="SKKMIGAS" /></a>
                <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                </a>

        <div id="top_menu" >
          <ul class="top-menu nav pull-right notify-row iteminicon">
            <li class="dropdown">
                <?php if(Yii::app()->user->getState("switch") === "admin" && Yii::app()->user->getState("level") === "kkks") {;?>
                <a href="<?php echo Yii::app()->createUrl('/kkks/impersonate', array('user'=>Yii::app()->user->getState("switch")));?>" class="dropdown-toggle element" data-placement="bottom" data-toggle="tooltip" data-original-title="Switch Admin"> &nbsp;<i class="icon-refresh"></i>&nbsp;</a>
                <?php };?>
            </li>
            <li>
              <?php
                if (Yii::app()->user->getState("level") === "admin") {
                    echo "Super Administrator";
                } elseif (Yii::app()->user->getState("level") === "skk") {
                    echo "SKK Migas";
                } elseif (Yii::app()->user->getState("level") === "kkks") {
                    $cur_wk_id = Yii::app()->user->wk_id;
                    $sql = "SELECT wk_name FROM adm_working_area WHERE wk_id='{$cur_wk_id}'";
                    $sql = Yii::app()->db->createCommand($sql);
                    $cur_wk_name = $sql->queryRow();
                    echo $cur_wk_id . " - " . $cur_wk_name['wk_name'];
              ?>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle element" data-toggle="dropdown"
                 data-placement="bottom" data-toggle="tooltip" href="#"
                 data-original-title="<?php echo Yii::app()->user->wk_id." "; ?>">
                 <i class="icon-user"></i></a>
                 <div class="dropdown-menu extended">
                   <div class="row-fluid">
                     <div class="row-fluid gplus-style">
                       <div class="modal-body">
                         <div class="row-fluid">
                           <div class="span12">
                             <p><strong><?php echo Yii::app()->user->wk_id." "; ?></strong></p>
                           </div>
                         </div>
                       <div class="row-fluid">
                     <div class="span6 offset6">
                       <button class="btn btn-primary" type="button"
                               onclick="location.href='<?php echo Yii::app()->createUrl('/Kkks/createprofile');?>'">
                               Edit Profile
                       </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <?php } else {
                $this->redirect(Yii::app()->createUrl("web/login"));
            }?>
                        <!-- BEGIN OFF -->
                        <li class="dropdown">
                            <a href="<?php echo Yii::app()->createUrl('/site/logout');?>" class="dropdown-toggle element" data-placement="bottom" data-toggle="tooltip" data-original-title="Logout"> &nbsp;<i class="icon-off"></i>&nbsp;</a>
                        </li>
                        <!-- END OFF -->
                    </ul>
                </div>
                <!-- END  NOTIFICATION -->
                <div class="top-nav ">
                    <ul class="nav pull-right top-menu" ></ul>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
        </div>
        <!-- END TOP NAVIGATION BAR -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid <?php if (in_array($this->pageTitle, $DataSummary)) {
        echo "";
        }?>">
        <!-- BEGIN SIDEBAR -->
        <div id="sidebar" class="nav-collapse collapse <?php if (in_array($this->pageTitle, $Login_Page)) {
            echo "hidden";
            }?>">
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="sidebar-menu">
                <li class="has-sub <?php if($this->pageTitle == 'Home'){echo 'active';} else{echo '';} ?>">
                    <a class="" href="<?php echo Yii::app()->createUrl("/site/index"); ?>">
                        <span class="icon-box"> <i class="icon-home"></i></span>Home
                    </a>
                </li>
                <?php if(Yii::app()->user->getState("level") === "kkks") {;?>
                <li class="has-sub <?php if (in_array($this->pageTitle, $InputRPS)) {
                    echo "active open";
                    }?>">
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-tint"></i></span>Resources<span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="<?php if($this->pageTitle == 'Play'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/createplay');?>">Play</a></li>
                        <li><a class="<?php if($this->pageTitle == 'Lead'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/createlead');?>">Lead</a></li>
                        <li><a class="<?php if($this->pageTitle == 'Drillable'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/createdrillable');?>">Drillable</a></li>
                        <li><a class="<?php if($this->pageTitle == 'Well'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/createwell');?>">Well</a></li>
                        <li><a class="<?php if($this->pageTitle == 'Post Drill'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/createpostdrill');?>">Post Drill</a></li>
                        <li><a class="<?php if($this->pageTitle == 'Discovery'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/creatediscovery');?>">Discovery</a></li>
                    </ul>
                </li>
                <li class="has-sub <?php if (in_array($this->pageTitle, $DataSummary)) {
                    echo "active open";
                    }?>">
                    <a href="javascript:;" class="">
                    <span class="icon-box"><i class="icon-bar-chart"></i></span>Summary
                    <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                      <li><a class="<?php if($this->pageTitle == 'Print'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/printdata', array('view'=>'printdata'));?>">Resources Summary</a></li>
                      <li><a href="<?= route('print/index') ?>">Print All</a></li>
                    </ul>
                </li>
                <li class="has-sub <?php if (in_array($this->pageTitle, $FileUpload)) {
                    echo "active open";
                    }?>">
                    <a class="<?php if($this->pageTitle == 'File Upload'){echo "active";} else{echo '';} ?>" href="<?php echo Yii::app()->createUrl('/Kkks/uploadmaps2');?>">
                        <span class="icon-box"><i class="icon-picture"></i></span>File Upload
                    </a>
                </li>
                <?php };?>

                <?php if(Yii::app()->user->getState("level") === "skk") {;?>
                <li class="has-sub <?php if (in_array($this->pageTitle, $Query)){
                    echo "active open";
                    }?>">
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-book"></i></span> QUERY<span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="<?php if($this->pageTitle == "ExportPlay"){echo "active";} else {echo '';}?>" href="tes">EXPORT PLAY</a></li>
                        <li><a class="<?php if($this->pageTitle == "ExportLead"){echo "active";} else {echo '';}?>" href="tes">EXPORT LEAD</a></li>
                        <li><a class="<?php if($this->pageTitle == "ExportDrillable"){echo "active";} else {echo '';}?>" href="tes">EXPORT DRILLABLE</a></li>
                        <li><a class="<?php if($this->pageTitle == "ExportPostdrill"){echo "active";} else {echo '';}?>" href="tes">EXPORT POSTDRILL</a></li>
                        <li><a class="<?php if($this->pageTitle == "ExportDiscovery"){echo "active";} else {echo '';}?>" href="tes">EXPORT DISCOVERY</a></li>
                    </ul>
                </li>
                <?php };?>
                <?php if(Yii::app()->user->getState("level") === "admin") {;?>
                <li class="has-sub">
                    <a class="<?php if($this->pageTitle == 'statustime') {echo "active";} else {echo '';}?>" href="<?php echo Yii::app()->createUrl('/admin/statustime');?>">
                        <span class="icon-box"><i class="icon-time"></i></span>Set Status Time
                    </a>
                </li>
                <li class="has-sub">
                    <a class="<?php if($this->pageTitle == 'skkadmin') {echo "active";} else {echo '';}?>" href="<?php echo Yii::app()->createUrl('/admin/skkadmin');?>">
                        <span class="icon-box"><i class="icon-user"></i></span>User Management
                    </a>
                </li>
                <li class="has-sub">
                    <a class="<?php if($this->pageTitle == 'resourcesdata') {echo "active";} else {echo '';}?>" href="<?php echo Yii::app()->createUrl('/admin/resourcesdata');?>">
                        <span class="icon-box"><i class="icon-tint"></i></span>Resources Data
                    </a>
                </li>
                <?php };?>

                <?php if (Yii::app()->user->getState("level") === "kkks") { ?>
                <li class="has-sub <?php if (in_array($this->pageTitle, $Manual)) {echo "active open";}?>">
                <a class="<?php if($this->pageTitle == 'Form &amp; Manual'){echo "active";} else{echo '';} ?>"
                   href="<?php echo Yii::app()->createUrl('/Kkks/manualformat');?>">
                   <span class="icon-box"><i class="icon-book"></i></span>Form &amp; Manual
                </a>
                </li>
                <li class="has-sub <?php if (in_array($this->pageTitle, $Help)) {echo "active open";}?>">
                    <a class="<?php if($this->pageTitle == 'Contact'){echo "active";} else{echo '';} ?>"
                    href="<?php echo Yii::app()->createUrl('/Kkks/contact');?>">
                    <span class="icon-box"><i class="icon-phone"></i></span>Help
                </a>
                </li>
                <?php } ?>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div id="main-content" style="<?php if (in_array($this->pageTitle, $Login_Page)) {
            echo "margin-left: 0px;margin-top: 0px;";
            }?>" >
            <?php echo $content; ?>
        </div>
        <!-- BEGIN PAGE -->
    </div>
    <!-- END CONTAINER -->

    <div id="footer">
       <p>Copyright &copy; <?= date('Y') ?> SKK Migas</p>
    </div>

    <!-- Le Javascript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.blockui.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select2.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/table-editable.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/scripts.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ui-jqueryui.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.print.js"></script>

    <!-- ie8 fixes -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/data-tables/jquery.dataTables.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/oldassets/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/data.js"></script>

    <script>
      jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        UIJQueryUI.init();
        <?php if (in_array($this->pageTitle, $Home_Page)) {
        echo "$('#coverModal').modal('show');";}?>
        TableEditable.init();
      });
    </script>
</body>
</html>
