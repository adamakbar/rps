<!doctype html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

  <link rel="shortcut icon"
        href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico"
        type="image/x-icon" />

	<title>Revitalisasi Pelaporan Sumberdaya &middot; SKK Migas</title>
  <link href="<?= assets('css/rps.bootstrap.min.css') ?>" rel="stylesheet" />
  <link href="<?= assets('css/font-awesome.min.css') ?>" rel="stylesheet" />
  <link href="<?= assets('css/style.css') ?>" rel="stylesheet" />
  <link href="<?= assets('css/editan.css') ?>" rel="stylesheet" />
</head>

<body>
<div class="page">
	<?php echo $content; ?>
</div>
</body>
</html>