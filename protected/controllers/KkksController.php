<?php

ini_set('xdebug.var_display_max_depth', 10);
ini_set('xdebug.var_display_max_children', 1024);
ini_set('xdebug.var_display_max_data', 1025);

class KkksController extends Controller {
    public $dataPrint;
    public $today;
    public $original;
    public $update;
    public $year;

    public function init() {
        $this->dataPrint = new DataPrint();
        $this->today = date('Y-m-d');

        $this->original = Status::model()->findByAttributes(array(
            'rps_year' => date('Y'),
            'status_name' => 'original',
        ));

        $this->update = Status::model()->findByAttributes(array('rps_year' => date("Y"), 'status_name' => 'update'));
        $this->year = $this->original;
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array(
                    'createplay',
                    'createlead',
                    'createdrillable',
                    'createpostdrill',
                    'creatediscovery',
                    'getpreviousdataplay',
                    'getpreviousdatalead',
                    'getpreviousdatadrillable',
                    'getpreviousdatapostdrill',
                    'getpreviousdatadiscovery',
                    'updateplay',
                    'updatelead',
                    'updatedrillable',
                    'updatepostdrill',
                    'updatediscovery',
                    'tambahwell',
                    'getgcf',
                    'oriGcf',
                    'formationname',
                    'addformationname',
                    'basin',
                    'province',
                    'verifikasi',
                    'manualformat',
                    'printdata',
                    'printall',
                    'printplay',
                    'printlead',
                    'printdrillable',
                    'printpostdrill',
                    'printdiscovery',
                    'contact',
                    'createwellinventory',
                    'updatewellinventory',
                    'createprofile',
                    'uploadmaps2',
                    'ajaxrequestuploadmaps2',
                    'loadimage',
                    'createwell',
                    'ajaxrequestwell',
                    'updatewellpostdrill',
                    'updatewelldiscovery',
                    'getdatawelldiscovery',
                    'wellname',
                    'wellnamediscovery',
                    'playname',
                    'deleteparticipant',
                    'contractorname',
                    'operatorname',
                    'deletelead',
                    'deletedrillable',
                    'deletepostdrill',
                    'deletediscovery',
                    'getnilai',
                ),
                'expression' => 'Yii::app()->user->getState("level") === "kkks"',
            ),
            array('allow',
                'actions' => array(
                    'impersonate',
                ),
                'expression' => 'Yii::app()->user->getState("switch") === "admin"',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionImpersonate($user) {
        $ui = UserIdentity::impersonate($user);

        if ($ui) {
            Yii::app()->user->login($ui, 0);
        }

        $this->redirect('index.php?r=site/index');
    }

    public function jslog($var) {
        echo "<script>console.log('" . $var . "')</script>";
    }

    public function actionGetNilai() {
        $nilai = new Nilai;
        ($_POST['choosen'] == null) ? $value = 0.5 : $value = $nilai->getNilai($_POST['name'], $_POST['choosen'], $_POST['from'], $_POST['category']);
        ($_POST['from'] == 'play') ? $state = $nilai->getState($_POST['name']) : $state = $nilai->getState2($_POST['name']); //untuk menempatkan nilai pada id selector
        echo json_encode(array('value' => $value, 'state' => $state));
    }

    public function actionCreatePlay() {
        $mdlPlay = new Play();
        $mdlGcf = new Gcf('play');

        $model = new Play('searchRelated');
        $model->unsetAttributes();

        if (isset($_GET['Play'])) {
            $model->attributes = $_GET['Play'];
        }

        if (isset($_POST['Play'], $_POST['Gcf'])) {
            $mdlPlay->wk_id = Yii::app()->user->wk_id;

            if (isset($_POST['Play']['bound_lat_top_left_degree']) & isset($_POST['Play']['bound_lat_top_left_minute']) & isset($_POST['Play']['bound_lat_top_left_second']) & isset($_POST['Play']['bound_lat_top_left_direction']) & isset($_POST['Play']['bound_lat_bottom_right_degree']) & isset($_POST['Play']['bound_lat_bottom_right_minute']) & isset($_POST['Play']['bound_lat_bottom_right_second']) & isset($_POST['Play']['bound_lat_bottom_right_direction'])) {
                $mdlPlay->play_latitude = $_POST['Play']['bound_lat_top_left_degree'] . ',' . $_POST['Play']['bound_lat_top_left_minute'] . ',' . $_POST['Play']['bound_lat_top_left_second'] . ',' . $_POST['Play']['bound_lat_top_left_direction'] . ',' . $_POST['Play']['bound_lat_bottom_right_degree'] . ',' . $_POST['Play']['bound_lat_bottom_right_minute'] . ',' . $_POST['Play']['bound_lat_bottom_right_second'] . ',' . $_POST['Play']['bound_lat_bottom_right_direction'];
                $mdlPlay->play_longitude = $_POST['Play']['bound_long_top_left_degree'] . ',' . $_POST['Play']['bound_long_top_left_minute'] . ',' . $_POST['Play']['bound_long_top_left_second'] . ',' . $_POST['Play']['bound_long_top_left_direction'] . ',' . $_POST['Play']['bound_long_bottom_right_degree'] . ',' . $_POST['Play']['bound_long_bottom_right_minute'] . ',' . $_POST['Play']['bound_long_bottom_right_second'] . ',' . $_POST['Play']['bound_long_bottom_right_direction'];
            }

            $mdlPlay->attributes = $_POST['Play'];
            $mdlGcf->attributes = $_POST['Gcf'];

            $mdlPlay->play_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlPlay->play_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlPlay->play_submit_status = 'Update';
            }

            $mdlPlay->play_year = $this->year->rps_year;

            if ($mdlPlay->validate() & $mdlGcf->validate()) {
                if ($mdlGcf->save(false)) {
                    $mdlPlay->gcf_id = $mdlGcf->gcf_id;
                    $mdlPlay->save(false);
                    if ($mdlPlay->play_update_from == '') {
                        $mdlPlay->play_update_from = $mdlPlay->play_id;
                        $mdlPlay->play_submit_revision = 0;
                    } else {
                        $mdlPlay->play_submit_revision += 1;
                    }
                    $mdlPlay->save(false);
                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                }
            } else {
                $error2 = self::validate(array($mdlPlay, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('createplay', array(
            'mdlPlay' => $mdlPlay,
            'mdlGcf' => $mdlGcf,
            'model' => $model,
        ));
    }

    public function actionCreateLead() {
        $mdlLead = new Lead();
        $mdlLeadGeological = new LeadGeological();
        $mdlLead2d = new Lead2d();
        $mdlLeadGravity = new LeadGravity();
        $mdlLeadGeochemistry = new LeadGeochemistry();
        $mdlLeadElectromagnetic = new LeadElectromagnetic();
        $mdlLeadResistivity = new LeadResistivity();
        $mdlLeadOther = new LeadOther();
        $mdlGcf = new Gcf();

        $model = new Lead('searchRelated');
        $model->unsetAttributes();

        if (isset($_GET['Lead'])) {
            $model->attributes = $_GET['Lead'];
        }

        if (isset($_POST['Lead'], $_POST['LeadGeological'], $_POST['Lead2d'], $_POST['LeadGravity'], $_POST['LeadGeochemistry'], $_POST['LeadElectromagnetic'], $_POST['LeadResistivity'], $_POST['LeadOther'], $_POST['Gcf'])) {
            $mdlLead->attributes = $_POST['Lead'];
            $mdlLeadGeological->attributes = $_POST['LeadGeological'];
            $mdlLead2d->attributes = $_POST['Lead2d'];
            $mdlLeadGravity->attributes = $_POST['LeadGravity'];
            $mdlLeadGeochemistry->attributes = $_POST['LeadGeochemistry'];
            $mdlLeadElectromagnetic->attributes = $_POST['LeadElectromagnetic'];
            $mdlLeadResistivity->attributes = $_POST['LeadResistivity'];
            $mdlLeadOther->attributes = $_POST['LeadOther'];
            $mdlGcf->attributes = $_POST['Gcf'];

            $mdlLead->lead_latitude = $_POST['Lead']['center_lat_degree'] . ',' . $_POST['Lead']['center_lat_minute'] . ',' . $_POST['Lead']['center_lat_second'] . ',' . $_POST['Lead']['center_lat_direction'];
            $mdlLead->lead_longitude = $_POST['Lead']['center_long_degree'] . ',' . $_POST['Lead']['center_long_minute'] . ',' . $_POST['Lead']['center_long_second'] . ',' . $_POST['Lead']['center_long_direction'];

            $mdlLead->lead_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlLead->lead_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlLead->lead_submit_status = 'Update';
            }

            $mdlLead->lead_year = $this->year->rps_year;

            if ($mdlLead->validate() & $mdlLeadGeological->validate() & $mdlLead2d->validate() & $mdlLeadGravity->validate() & $mdlLeadGeochemistry->validate() & $mdlLeadElectromagnetic->validate() & $mdlLeadResistivity->validate() & $mdlLeadOther->validate() & $mdlGcf->validate()) {
                if ($mdlGcf->save(false)) {
                    $mdlLead->gcf_id = $mdlGcf->gcf_id;
                    if ($mdlLead->save(false)) {
                        if ($mdlLead->lead_update_from == '') {
                            $mdlLead->lead_update_from = $mdlLead->lead_id;
                            $mdlLead->lead_submit_revision = 0;
                        } else {
                            $mdlLead->lead_submit_revision += 1;
                        }
                        $mdlLead->save(false);
                        if ($mdlLeadGeological->is_checked == 1) {
                            $mdlLeadGeological->lead_id = $mdlLead->lead_id;
                            $mdlLeadGeological->save(false);
                        }

                        if ($mdlLead2d->is_checked == 1) {
                            $mdlLead2d->lead_id = $mdlLead->lead_id;
                            $mdlLead2d->save(false);
                        }

                        if ($mdlLeadGravity->is_checked == 1) {
                            $mdlLeadGravity->lead_id = $mdlLead->lead_id;
                            $mdlLeadGravity->save(false);
                        }

                        if ($mdlLeadGeochemistry->is_checked == 1) {
                            $mdlLeadGeochemistry->lead_id = $mdlLead->lead_id;
                            $mdlLeadGeochemistry->save(false);
                        }

                        if ($mdlLeadElectromagnetic->is_checked == 1) {
                            $mdlLeadElectromagnetic->lead_id = $mdlLead->lead_id;
                            $mdlLeadElectromagnetic->save(false);
                        }

                        if ($mdlLeadResistivity->is_checked == 1) {
                            $mdlLeadResistivity->lead_id = $mdlLead->lead_id;
                            $mdlLeadResistivity->save(false);
                        }

                        if ($mdlLeadOther->is_checked == 1) {
                            $mdlLeadOther->lead_id = $mdlLead->lead_id;
                            $mdlLeadOther->save(false);
                        }

                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                }
            } else {
                $error2 = self::validate(array($mdlLead, $mdlLeadGeological, $mdlLead2d, $mdlLeadGravity, $mdlLeadGeochemistry, $mdlLeadElectromagnetic, $mdlLeadResistivity, $mdlLeadOther, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
//          echo json_encode(array('result' => 'error', 'msg' => CHtml::errorSummary(array($mdlLead, $mdlLeadGeological, $mdlLead2d, $mdlLeadGravity, $mdlLeadGeochemistry, $mdlLeadElectromagnetic, $mdlLeadResistivity, $mdlLeadOther, $mdlGcf)), 'err'=>$error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('createlead', array(
            'mdlLead' => $mdlLead,
            'mdlLeadGeological' => $mdlLeadGeological,
            'mdlLead2d' => $mdlLead2d,
            'mdlLeadGravity' => $mdlLeadGravity,
            'mdlLeadGeochemistry' => $mdlLeadGeochemistry,
            'mdlLeadElectromagnetic' => $mdlLeadElectromagnetic,
            'mdlLeadResistivity' => $mdlLeadResistivity,
            'mdlLeadOther' => $mdlLeadOther,
            'mdlGcf' => $mdlGcf,
            'model' => $model,
        ));
    }

    public function actionCreateDrillable() {
        $mdlProspect = new Prospect();
        $mdlDrillable = new Drillable();
        $mdlGeological = new Geological();
        $mdlSeismic2d = new Seismic2d();
        $mdlGravity = new Gravity();
        $mdlGeochemistry = new Geochemistry();
        $mdlElectromagnetic = new Electromagnetic();
        $mdlResistivity = new Resistivity();
        $mdlOther = new Other();
        $mdlSeismic3d = new Seismic3d();
        $mdlGcf = new Gcf();

        $model = new Prospect('searchRelatedDrillable');
        $model->unsetAttributes();

        if (isset($_GET['Prospect'])) {
            $model->attributes = $_GET['Prospect'];
        }

        if (isset($_POST['Prospect'], $_POST['Drillable'], $_POST['Geological'], $_POST['Seismic2d'], $_POST['Gravity'], $_POST['Geochemistry'], $_POST['Electromagnetic'], $_POST['Resistivity'], $_POST['Other'], $_POST['Seismic3d'], $_POST['Gcf'])) {
            $mdlProspect->attributes = $_POST['Prospect'];
            $mdlDrillable->attributes = $_POST['Drillable'];
            $mdlGeological->attributes = $_POST['Geological'];
            $mdlSeismic2d->attributes = $_POST['Seismic2d'];
            $mdlGravity->attributes = $_POST['Gravity'];
            $mdlGeochemistry->attributes = $_POST['Geochemistry'];
            $mdlElectromagnetic->attributes = $_POST['Electromagnetic'];
            $mdlResistivity->attributes = $_POST['Resistivity'];
            $mdlOther->attributes = $_POST['Other'];
            $mdlSeismic3d->attributes = $_POST['Seismic3d'];
            $mdlGcf->attributes = $_POST['Gcf'];

            $mdlProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
            $mdlProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];
            $mdlProspect->prospect_type = 'drillable';
//      $mdlDrillable->prospect_id = 1;

            $mdlProspect->prospect_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlProspect->prospect_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlProspect->prospect_submit_status = 'Update';
            }

            $mdlProspect->prospect_year = $this->year->rps_year;

            if ($mdlProspect->validate() & $mdlDrillable->validate() & $mdlGeological->validate() & $mdlSeismic2d->validate() & $mdlGravity->validate() & $mdlGeochemistry->validate() & $mdlElectromagnetic->validate() & $mdlResistivity->validate() & $mdlOther->validate() & $mdlSeismic3d->validate() & $mdlGcf->validate()) {
                if ($mdlGcf->save(false)) {
                    $mdlProspect->gcf_id = $mdlGcf->gcf_id;
                    if ($mdlProspect->save(false)) {
                        if ($mdlProspect->prospect_update_from == '') {
                            $mdlProspect->prospect_update_from = $mdlProspect->prospect_id;
                            $mdlProspect->prospect_submit_revision = 0;
                        } else {
                            $mdlProspect->prospect_submit_revision += 1;
                        }
                        $mdlProspect->save(false);
                        $mdlDrillable->prospect_id = $mdlProspect->prospect_id;
                        $mdlDrillable->save(false);

                        if ($mdlGeological->is_checked == 1) {
                            $mdlGeological->prospect_id = $mdlProspect->prospect_id;
                            $mdlGeological->save(false);
                        }

                        if ($mdlSeismic2d->is_checked == 1) {
                            $mdlSeismic2d->prospect_id = $mdlProspect->prospect_id;
                            $mdlSeismic2d->save(false);
                        }

                        if ($mdlGravity->is_checked == 1) {
                            $mdlGravity->prospect_id = $mdlProspect->prospect_id;
                            $mdlGravity->save(false);
                        }

                        if ($mdlGeochemistry->is_checked == 1) {
                            $mdlGeochemistry->prospect_id = $mdlProspect->prospect_id;
                            $mdlGeochemistry->save(false);
                        }

                        if ($mdlElectromagnetic->is_checked == 1) {
                            $mdlElectromagnetic->prospect_id = $mdlProspect->prospect_id;
                            $mdlElectromagnetic->save(false);
                        }

                        if ($mdlResistivity->is_checked == 1) {
                            $mdlResistivity->prospect_id = $mdlProspect->prospect_id;
                            $mdlResistivity->save(false);
                        }

                        if ($mdlOther->is_checked == 1) {
                            $mdlOther->prospect_id = $mdlProspect->prospect_id;
                            $mdlOther->save(false);
                        }

                        if ($mdlSeismic3d->is_checked == 1) {
                            $mdlSeismic3d->prospect_id = $mdlProspect->prospect_id;
                            $mdlSeismic3d->save(false);
                        }

                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                }
            } else {
                $error2 = self::validate(array($mdlProspect, $mdlDrillable, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                    //echo json_encode(array('result' => 'error', 'msg' => CHtml::errorSummary(array($mdlProspect, $mdlDrillable, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf)), 'err'=>$error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('createdrillable', array(
            'mdlProspect' => $mdlProspect,
            'mdlDrillable' => $mdlDrillable,
            'mdlGeological' => $mdlGeological,
            'mdlSeismic2d' => $mdlSeismic2d,
            'mdlGravity' => $mdlGravity,
            'mdlGeochemistry' => $mdlGeochemistry,
            'mdlElectromagnetic' => $mdlElectromagnetic,
            'mdlResistivity' => $mdlResistivity,
            'mdlOther' => $mdlOther,
            'mdlSeismic3d' => $mdlSeismic3d,
            'mdlGcf' => $mdlGcf,
            'model' => $model,
        ));
    }

    public function actionCreatePostDrill() {
        $mdlProspect = new Prospect();
        $mdlPostdrill = new Postdrill();
        $mdlGeological = new Geological();
        $mdlSeismic2d = new Seismic2d();
        $mdlGravity = new Gravity();
        $mdlGeochemistry = new Geochemistry();
        $mdlElectromagnetic = new Electromagnetic();
        $mdlResistivity = new Resistivity();
        $mdlOther = new Other();
        $mdlSeismic3d = new Seismic3d();
        $mdlGcf = new Gcf();

        $model = new Postdrill('searchRelatedPostdrill');
        $model->unsetAttributes();

        if (isset($_GET['Postdrill'])) {
            $model->attributes = $_GET['Postdrill'];
        }

        $mdlProspect->scenario = 'postdrill';

        if (isset($_POST['Prospect'], $_POST['Postdrill'], $_POST['Geological'], $_POST['Seismic2d'], $_POST['Gravity'], $_POST['Geochemistry'], $_POST['Electromagnetic'], $_POST['Resistivity'], $_POST['Other'], $_POST['Seismic3d'], $_POST['Gcf'])) {

            $mdlWell = Well::model()->findByPk($_POST['Prospect']['well_name']);

            $mdlProspect->attributes = $_POST['Prospect'];
            $mdlPostdrill->attributes = $_POST['Postdrill'];
            $mdlGeological->attributes = $_POST['Geological'];
            $mdlSeismic2d->attributes = $_POST['Seismic2d'];
            $mdlGravity->attributes = $_POST['Gravity'];
            $mdlGeochemistry->attributes = $_POST['Geochemistry'];
            $mdlElectromagnetic->attributes = $_POST['Electromagnetic'];
            $mdlResistivity->attributes = $_POST['Resistivity'];
            $mdlOther->attributes = $_POST['Other'];
            $mdlSeismic3d->attributes = $_POST['Seismic3d'];
            $mdlGcf->attributes = $_POST['Gcf'];

            $mdlProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
            $mdlProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];
            $mdlProspect->prospect_type = 'postdrill';

            $mdlProspect->prospect_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlProspect->prospect_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlProspect->prospect_submit_status = 'Update';
            }

            $mdlProspect->prospect_year = $this->year->rps_year;

            if ($mdlProspect->validate() & $mdlPostdrill->validate() & $mdlGeological->validate() & $mdlSeismic2d->validate() & $mdlGravity->validate() & $mdlGeochemistry->validate() & $mdlElectromagnetic->validate() & $mdlResistivity->validate() & $mdlOther->validate() & $mdlSeismic3d->validate() & $mdlGcf->validate()) {
                if ($mdlGcf->save(false)) {
                    $mdlProspect->gcf_id = $mdlGcf->gcf_id;
                    if ($mdlProspect->save(false)) {
                        if ($mdlProspect->prospect_update_from == '') {
                            $mdlProspect->prospect_update_from = $mdlProspect->prospect_id;
                            $mdlProspect->prospect_submit_revision = 0;
                        } else {
                            $mdlProspect->prospect_submit_revision += 1;
                        }
                        $mdlProspect->save(false);
                        $mdlPostdrill->prospect_id = $mdlProspect->prospect_id;
                        $mdlPostdrill->save(false);

                        $string = $_POST['Prospect']['well_name'];
                        $array = explode(',', $string);

                        $j = 0;
                        for ($i = 1; $i <= count($array); $i++) {
                            $mdlWell = Well::model()->findByPk($array[$j++]);
                            $mdlWell->prospect_id = $mdlProspect->prospect_id;
                            $mdlWell->save(false);
                        }

                        if ($mdlGeological->is_checked == 1) {
                            $mdlGeological->prospect_id = $mdlProspect->prospect_id;
                            $mdlGeological->save(false);
                        }

                        if ($mdlSeismic2d->is_checked == 1) {
                            $mdlSeismic2d->prospect_id = $mdlProspect->prospect_id;
                            $mdlSeismic2d->save(false);
                        }

                        if ($mdlGravity->is_checked == 1) {
                            $mdlGravity->prospect_id = $mdlProspect->prospect_id;
                            $mdlGravity->save(false);
                        }

                        if ($mdlGeochemistry->is_checked == 1) {
                            $mdlGeochemistry->prospect_id = $mdlProspect->prospect_id;
                            $mdlGeochemistry->save(false);
                        }

                        if ($mdlElectromagnetic->is_checked == 1) {
                            $mdlElectromagnetic->prospect_id = $mdlProspect->prospect_id;
                            $mdlElectromagnetic->save(false);
                        }

                        if ($mdlResistivity->is_checked == 1) {
                            $mdlResistivity->prospect_id = $mdlProspect->prospect_id;
                            $mdlResistivity->save(false);
                        }

                        if ($mdlOther->is_checked == 1) {
                            $mdlOther->prospect_id = $mdlProspect->prospect_id;
                            $mdlOther->save(false);
                        }

                        if ($mdlSeismic3d->is_checked == 1) {
                            $mdlSeismic3d->prospect_id = $mdlProspect->prospect_id;
                            $mdlSeismic3d->save(false);
                        }

                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                }
            } else {
                $error2 = self::validate(array($mdlProspect, $mdlPostdrill, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('createpostdrill', array(
            'mdlProspect' => $mdlProspect,
            'mdlPostdrill' => $mdlPostdrill,
            'mdlGeological' => $mdlGeological,
            'mdlSeismic2d' => $mdlSeismic2d,
            'mdlGravity' => $mdlGravity,
            'mdlGeochemistry' => $mdlGeochemistry,
            'mdlElectromagnetic' => $mdlElectromagnetic,
            'mdlResistivity' => $mdlResistivity,
            'mdlOther' => $mdlOther,
            'mdlSeismic3d' => $mdlSeismic3d,
            'mdlGcf' => $mdlGcf,
            'model' => $model,
        ));
    }

    public function actionCreateDiscovery() {
        $mdlProspect = new Prospect('discovery');
        $mdlDiscovery = new Discovery();
        $mdlGeological = new Geological();
        $mdlSeismic2d = new Seismic2d();
        $mdlGravity = new Gravity();
        $mdlGeochemistry = new Geochemistry();
        $mdlElectromagnetic = new Electromagnetic();
        $mdlResistivity = new Resistivity();
        $mdlOther = new Other();
        $mdlSeismic3d = new Seismic3d();
        $mdlGcf = new Gcf();

        $model = new Discovery('searchRelatedDiscovery');
        $model->unsetAttributes();

        if (isset($_GET['Discovery'])) {
            $model->attributes = $_GET['Discovery'];
        }

        if (isset($_POST['Prospect'], $_POST['Geological'], $_POST['Seismic2d'], $_POST['Gravity'], $_POST['Geochemistry'], $_POST['Electromagnetic'], $_POST['Resistivity'], $_POST['Other'], $_POST['Seismic3d'], $_POST['Gcf'])) {

            $model = array();

            $mdlProspect->attributes = $_POST['Prospect'];
            $mdlDiscovery->attributes = $_POST['Discovery'];
            $mdlGeological->attributes = $_POST['Geological'];
            $mdlSeismic2d->attributes = $_POST['Seismic2d'];
            $mdlGravity->attributes = $_POST['Gravity'];
            $mdlGeochemistry->attributes = $_POST['Geochemistry'];
            $mdlElectromagnetic->attributes = $_POST['Electromagnetic'];
            $mdlResistivity->attributes = $_POST['Resistivity'];
            $mdlOther->attributes = $_POST['Other'];
            $mdlSeismic3d->attributes = $_POST['Seismic3d'];
            $mdlGcf->attributes = $_POST['Gcf'];

            $mdlProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
            $mdlProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];
            $mdlProspect->prospect_type = 'discovery';

            $mdlProspect->prospect_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlProspect->prospect_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlProspect->prospect_submit_status = 'Update';
            }

            $mdlProspect->prospect_year = $this->year->rps_year;

            $valid = $mdlProspect->validate();
            $valid = $mdlDiscovery->validate() && $valid;
            $valid = $mdlGeological->validate() && $valid;
            $valid = $mdlSeismic2d->validate() && $valid;
            $valid = $mdlGravity->validate() && $valid;
            $valid = $mdlGeochemistry->validate() && $valid;
            $valid = $mdlElectromagnetic->validate() && $valid;
            $valid = $mdlResistivity->validate() && $valid;
            $valid = $mdlOther->validate() && $valid;
            $valid = $mdlSeismic3d->validate() && $valid;
            $valid = $mdlGcf->validate() && $valid;

            array_push($model, $mdlProspect);
            array_push($model, $mdlDiscovery);

            array_push($model, $mdlGeological);
            array_push($model, $mdlSeismic2d);
            array_push($model, $mdlGravity);
            array_push($model, $mdlGeochemistry);
            array_push($model, $mdlElectromagnetic);
            array_push($model, $mdlResistivity);
            array_push($model, $mdlOther);
            array_push($model, $mdlSeismic3d);
            array_push($model, $mdlGcf);

            $error = array();
            foreach ($model as $mdl) {
                array_push($error, $mdl);
            }

            if ($valid) {

                if ($mdlGcf->save(false)) {
                    $mdlProspect->gcf_id = $mdlGcf->gcf_id;
                    if ($mdlProspect->save(false)) {
                        if ($mdlProspect->prospect_update_from == '') {
                            $mdlProspect->prospect_update_from = $mdlProspect->prospect_id;
                            $mdlProspect->prospect_submit_revision = 0;
                        } else {
                            $mdlProspect->prospect_submit_revision += 1;
                        }
                        $mdlProspect->save(false);
                        $mdlDiscovery->prospect_id = $mdlProspect->prospect_id;
                        $mdlDiscovery->save(false);

                        $string = $_POST['Prospect']['well_name'];
                        $array = explode(',', $string);

                        $j = 0;
                        for ($i = 1; $i <= count($array); $i++) {
                            $mdlWell = Well::model()->findByPk($array[$j++]);
                            $mdlWell->prospect_id = $mdlProspect->prospect_id;
                            $mdlWell->save(false);
                        }

                        if ($mdlGeological->is_checked == 1) {
                            $mdlGeological->prospect_id = $mdlProspect->prospect_id;
                            $mdlGeological->save(false);
                        }

                        if ($mdlSeismic2d->is_checked == 1) {
                            $mdlSeismic2d->prospect_id = $mdlProspect->prospect_id;
                            $mdlSeismic2d->save(false);
                        }

                        if ($mdlGravity->is_checked == 1) {
                            $mdlGravity->prospect_id = $mdlProspect->prospect_id;
                            $mdlGravity->save(false);
                        }

                        if ($mdlGeochemistry->is_checked == 1) {
                            $mdlGeochemistry->prospect_id = $mdlProspect->prospect_id;
                            $mdlGeochemistry->save(false);
                        }

                        if ($mdlElectromagnetic->is_checked == 1) {
                            $mdlElectromagnetic->prospect_id = $mdlProspect->prospect_id;
                            $mdlElectromagnetic->save(false);
                        }

                        if ($mdlResistivity->is_checked == 1) {
                            $mdlResistivity->prospect_id = $mdlProspect->prospect_id;
                            $mdlResistivity->save(false);
                        }

                        if ($mdlOther->is_checked == 1) {
                            $mdlOther->prospect_id = $mdlProspect->prospect_id;
                            $mdlOther->save(false);
                        }

                        if ($mdlSeismic3d->is_checked == 1) {
                            $mdlSeismic3d->prospect_id = $mdlProspect->prospect_id;
                            $mdlSeismic3d->save(false);
                        }

                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                }
            } else {
                $error2 = self::validate(array($mdlProspect, $mdlDiscovery, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }

        }

        $this->render('creatediscovery', array(
            'mdlProspect' => $mdlProspect,
            'mdlDiscovery' => $mdlDiscovery,
            'mdlGeological' => $mdlGeological,
            'mdlSeismic2d' => $mdlSeismic2d,
            'mdlGravity' => $mdlGravity,
            'mdlGeochemistry' => $mdlGeochemistry,
            'mdlElectromagnetic' => $mdlElectromagnetic,
            'mdlResistivity' => $mdlResistivity,
            'mdlOther' => $mdlOther,
            'mdlSeismic3d' => $mdlSeismic3d,
            'mdlGcf' => $mdlGcf,
            'model' => $model,
        ));

    }

    public function actionUpdatePlay($id) {
        if (!($mdlPlay = Play::model()->findByAttributes(array('play_id' => $id, 'wk_id' => Yii::app()->user->wk_id)))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $mdlPlaySearch = Play::model();
        $model = new Play('search');
        $model->unsetAttributes();

        $criteria = new CDbCriteria;
        $criteria->select = "max(b.play_submit_date)";
        $criteria->alias = 'b';
        $criteria->condition = "b.play_update_from = t.play_update_from";
        $subQuery = $model->getCommandBuilder()->createFindCommand($model->getTableSchema(), $criteria)->getText();

        $mainCriteria = new CDbCriteria;
        $mainCriteria->with = array('gcfs');
        $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
        $mainCriteria->addCondition('play_submit_date = (' . $subQuery . ')', 'AND');
        $mainCriteria->group = 'play_update_from';

        $playDataProvider = Play::model()->findAll($mainCriteria);

        $dataArray = array();
        foreach ($playDataProvider as $data) {
            $dataArray[] = $data->play_id;
        }
        if (!$this->GetLastData($id, $dataArray)) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $play_update_from = $mdlPlay->play_update_from;
        $play_submit_revision = $mdlPlay->play_submit_revision + 1;
        $play_id = $mdlPlay->play_id;

        $mdlGcf = Gcf::model()->findByAttributes(array('gcf_id' => $mdlPlay->gcf_id));

        $mdlGcf->scenario = 'play';
        $mdlFormation = new Formation();

        if (isset($mdlPlay->play_latitude) & isset($mdlPlay->play_longitude)) {
            $bonlat = preg_split("/[,]+/", $mdlPlay->play_latitude);
            $bonlong = preg_split("/[,]+/", $mdlPlay->play_longitude);

            $mdlPlay->bound_lat_top_left_degree = $bonlat[0];
            $mdlPlay->bound_lat_top_left_minute = $bonlat[1];
            $mdlPlay->bound_lat_top_left_second = $bonlat[2];
            $mdlPlay->bound_lat_top_left_direction = $bonlat[3];
            $mdlPlay->bound_lat_bottom_right_degree = $bonlat[4];
            $mdlPlay->bound_lat_bottom_right_minute = $bonlat[5];
            $mdlPlay->bound_lat_bottom_right_second = $bonlat[6];
            $mdlPlay->bound_lat_bottom_right_direction = $bonlat[7];

            $mdlPlay->bound_long_top_left_degree = $bonlong[0];
            $mdlPlay->bound_long_top_left_minute = $bonlong[1];
            $mdlPlay->bound_long_top_left_second = $bonlong[2];
            $mdlPlay->bound_long_top_left_direction = $bonlong[3];
            $mdlPlay->bound_long_bottom_right_degree = $bonlong[4];
            $mdlPlay->bound_long_bottom_right_minute = $bonlong[5];
            $mdlPlay->bound_long_bottom_right_second = $bonlong[6];
            $mdlPlay->bound_long_bottom_right_direction = $bonlong[7];
        }

        if (isset($_POST['Play'], $_POST['Gcf'])) {

            $mdlPlay->attributes = $_POST['Play'];
            $mdlGcf->attributes = $_POST['Gcf'];

            if (isset($_POST['Play']['bound_lat_top_left_degree']) & isset($_POST['Play']['bound_lat_top_left_minute']) & isset($_POST['Play']['bound_lat_top_left_second']) & isset($_POST['Play']['bound_lat_top_left_direction']) & isset($_POST['Play']['bound_lat_bottom_right_degree']) & isset($_POST['Play']['bound_lat_bottom_right_minute']) & isset($_POST['Play']['bound_lat_bottom_right_second']) & isset($_POST['Play']['bound_lat_bottom_right_direction'])) {
                $mdlPlay->play_latitude = $_POST['Play']['bound_lat_top_left_degree'] . ',' . $_POST['Play']['bound_lat_top_left_minute'] . ',' . $_POST['Play']['bound_lat_top_left_second'] . ',' . $_POST['Play']['bound_lat_top_left_direction'] . ',' . $_POST['Play']['bound_lat_bottom_right_degree'] . ',' . $_POST['Play']['bound_lat_bottom_right_minute'] . ',' . $_POST['Play']['bound_lat_bottom_right_second'] . ',' . $_POST['Play']['bound_lat_bottom_right_direction'];
                $mdlPlay->play_longitude = $_POST['Play']['bound_long_top_left_degree'] . ',' . $_POST['Play']['bound_long_top_left_minute'] . ',' . $_POST['Play']['bound_long_top_left_second'] . ',' . $_POST['Play']['bound_long_top_left_direction'] . ',' . $_POST['Play']['bound_long_bottom_right_degree'] . ',' . $_POST['Play']['bound_long_bottom_right_minute'] . ',' . $_POST['Play']['bound_long_bottom_right_second'] . ',' . $_POST['Play']['bound_long_bottom_right_direction'];
            }

            if ($_POST['Gcf']['gcf_res_por_type'] == 'Average Primary Porosity Reservoir') {
                $mdlGcf->gcf_res_por_secondary = '';
            } else if ($_POST['Gcf']['gcf_res_por_type'] == 'Secondary Porosity') {
                $mdlGcf->gcf_res_por_primary = '';
            }

            $mdlPlay->play_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlPlay->play_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlPlay->play_submit_status = 'Update';
            }

            $mdlPlay->play_year = $this->year->rps_year;

            if ($mdlPlay->validate() & $mdlGcf->validate()) {
                $mdlPlay->save(false);
                $mdlGcf->save(false);
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlPlay, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                    //          echo json_encode(array('result' => 'error', 'msg' => CHtml::errorSummary(array($mdlPlay, $mdlGcf)), 'err'=>$error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updateplay', array(
            'mdlPlay' => $mdlPlay,
            'mdlGcf' => $mdlGcf,
            'mdlFormation' => $mdlFormation,
        ));
    }

    public function actionUpdateLead($id) {
        $criteria = new CDbCriteria;
        $criteria->with = array('plays');
        $criteria->compare('lead_id', $id);
        $criteria->compare('plays.wk_id', Yii::app()->user->wk_id);
        $criteria->compare('lead_is_deleted', 0);
        if (!($mdlLead = Lead::model()->find($criteria))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $mdlLeadSearch = Lead::model();
        $model = new Lead('search');
        $model->unsetAttributes();

        $criteria = new CDbCriteria;
        $criteria->select = "max(b.lead_submit_date)";
        $criteria->alias = 'b';
        $criteria->condition = "b.lead_update_from = t.lead_update_from";
        $subQuery = $model->getCommandBuilder()->createFindCommand($model->getTableSchema(), $criteria)->getText();

        $mainCriteria = new CDbCriteria;
        $mainCriteria->with = array('plays');
        $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
        $mainCriteria->addCondition('lead_submit_date = (' . $subQuery . ')', 'AND');
        $mainCriteria->group = 'lead_update_from';

        $leadDataProvider = Lead::model()->findAll($mainCriteria);

        $dataArray = array();
        foreach ($leadDataProvider as $data) {
            $dataArray[] = $data->lead_id;
        }

        if (!$this->GetLastData($id, $dataArray)) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $lead_update_from = $mdlLead->lead_update_from;
        $lead_submit_revision = $mdlLead->lead_submit_revision + 1;

        if (!($mdlLeadGeological = LeadGeological::model()->findByattributes(array('lead_id' => $id)))) {
            $mdlLeadGeological = new LeadGeological();
            $mdlLeadGeological->lead_id = $id;
        } else {
            $mdlLeadGeological->is_checked = 1;
        }

        if (!($mdlLead2d = Lead2d::model()->findByAttributes(array('lead_id' => $id)))) {
            $mdlLead2d = new Lead2d();
            $mdlLead2d->lead_id = $id;
        } else {
            $mdlLead2d->is_checked = 1;
        }

        if (!($mdlLeadGravity = LeadGravity::model()->findByAttributes(array('lead_id' => $id)))) {
            $mdlLeadGravity = new LeadGravity();
            $mdlLeadGravity->lead_id = $id;
        } else {
            $mdlLeadGravity->is_checked = 1;
        }

        if (!($mdlLeadGeochemistry = LeadGeochemistry::model()->findByAttributes(array('lead_id' => $id)))) {
            $mdlLeadGeochemistry = new LeadGeochemistry();
            $mdlLeadGeochemistry->lead_id = $id;
        } else {
            $mdlLeadGeochemistry->is_checked = 1;
        }

        if (!($mdlLeadElectromagnetic = LeadElectromagnetic::model()->findByAttributes(array('lead_id' => $id)))) {
            $mdlLeadElectromagnetic = new LeadElectromagnetic();
            $mdlLeadElectromagnetic->lead_id = $id;
        } else {
            $mdlLeadElectromagnetic->is_checked = 1;
        }

        if (!($mdlLeadResistivity = LeadResistivity::model()->findByAttributes(array('lead_id' => $id)))) {
            $mdlLeadResistivity = new LeadResistivity();
            $mdlLeadResistivity->lead_id = $id;
        } else {
            $mdlLeadResistivity->is_checked = 1;
        }

        if (!($mdlLeadOther = LeadOther::model()->findByAttributes(array('lead_id' => $id)))) {
            $mdlLeadOther = new LeadOther();
            $mdlLeadOther->lead_id = $id;
        } else {
            $mdlLeadOther->is_checked = 1;
        }

        $mdlGcf = Gcf::model()->findByPk($mdlLead->gcf_id);

        if (isset($mdlLead->lead_latitude) & isset($mdlLead->lead_longitude)) {
            $bonlat = preg_split("/[,]+/", $mdlLead->lead_latitude);
            $bonlong = preg_split("/[,]+/", $mdlLead->lead_longitude);

            $mdlLead->center_lat_degree = $bonlat[0];
            $mdlLead->center_lat_minute = $bonlat[1];
            $mdlLead->center_lat_second = $bonlat[2];
            $mdlLead->center_lat_direction = $bonlat[3];

            $mdlLead->center_long_degree = $bonlong[0];
            $mdlLead->center_long_minute = $bonlong[1];
            $mdlLead->center_long_second = $bonlong[2];
            $mdlLead->center_long_direction = $bonlong[3];
        }

        if (isset($_POST['Lead'], $_POST['LeadGeological'], $_POST['Lead2d'], $_POST['LeadGravity'], $_POST['LeadGeochemistry'], $_POST['LeadElectromagnetic'], $_POST['LeadResistivity'], $_POST['LeadOther'], $_POST['Gcf'])) {
            $mdlLead->attributes = $_POST['Lead'];
            $mdlLeadGeological->attributes = $_POST['LeadGeological'];
            $mdlLead2d->attributes = $_POST['Lead2d'];
            $mdlLeadGravity->attributes = $_POST['LeadGravity'];
            $mdlLeadGeochemistry->attributes = $_POST['LeadGeochemistry'];
            $mdlLeadElectromagnetic->attributes = $_POST['LeadElectromagnetic'];
            $mdlLeadResistivity->attributes = $_POST['LeadResistivity'];
            $mdlLeadOther->attributes = $_POST['LeadOther'];
            $mdlGcf->attributes = $_POST['Gcf'];
            $mdlGcf->gcf_id_reference = $_POST['gcf_id_reference_update'];

            $mdlLead->lead_latitude = $_POST['Lead']['center_lat_degree'] . ',' . $_POST['Lead']['center_lat_minute'] . ',' . $_POST['Lead']['center_lat_second'] . ',' . $_POST['Lead']['center_lat_direction'];
            $mdlLead->lead_longitude = $_POST['Lead']['center_long_degree'] . ',' . $_POST['Lead']['center_long_minute'] . ',' . $_POST['Lead']['center_long_second'] . ',' . $_POST['Lead']['center_long_direction'];

            if ($_POST['Gcf']['gcf_res_por_type'] == 'Average Primary Porosity Reservoir') {
                $mdlGcf->gcf_res_por_secondary = '';
            } else if ($_POST['Gcf']['gcf_res_por_type'] == 'Secondary Porosity') {
                $mdlGcf->gcf_res_por_primary = '';
            }

            $mdlLead->lead_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin &
                $this->today <= $this->original->end) {

                $mdlLead->lead_submit_status = 'Original';

            } elseif ($this->today >= $this->update->begin &&
                $this->today <= $this->update->end) {

                $mdlLead->lead_submit_status = 'Update';
            }

            $mdlLead->lead_year = $this->year->rps_year;

            if ($mdlLead->validate() &
                $mdlLeadGeological->validate() &
                $mdlLead2d->validate() &
                $mdlLeadGravity->validate() &
                $mdlLeadGeochemistry->validate() &
                $mdlLeadElectromagnetic->validate() &
                $mdlLeadResistivity->validate() &
                $mdlLeadOther->validate() &
                $mdlGcf->validate()) {

                $mdlLead->save(false);
                ($mdlLeadGeological->is_checked == 1) ? $mdlLeadGeological->save(false) : LeadGeological::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                ($mdlLead2d->is_checked == 1) ? $mdlLead2d->save(false) : Lead2d::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                ($mdlLeadGravity->is_checked == 1) ? $mdlLeadGravity->save(false) : LeadGravity::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                ($mdlLeadGeochemistry->is_checked == 1) ? $mdlLeadGeochemistry->save(false) : LeadGeochemistry::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                ($mdlLeadElectromagnetic->is_checked == 1) ? $mdlLeadElectromagnetic->save(false) : LeadElectromagnetic::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                ($mdlLeadResistivity->is_checked == 1) ? $mdlLeadResistivity->save(false) : LeadResistivity::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                ($mdlLeadOther->is_checked == 1) ? $mdlLeadOther->save(false) : LeadOther::model()->deleteAll('`lead_id` = :lead_id', array(':lead_id' => $id));
                $mdlGcf->save(false);
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlLead, $mdlLeadGeological, $mdlLead2d, $mdlLeadGravity, $mdlLeadGeochemistry, $mdlLeadElectromagnetic, $mdlLeadResistivity, $mdlLeadOther, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                    //          echo json_encode(array('result' => 'error', 'msg' => CHtml::errorSummary(array($mdlLead, $mdlLeadGeological, $mdlLead2d, $mdlLeadGravity, $mdlLeadGeochemistry, $mdlLeadElectromagnetic, $mdlLeadResistivity, $mdlLeadOther, $mdlGcf)), 'err'=>$error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatelead', array(
            'mdlLead' => $mdlLead,
            'mdlLeadGeological' => $mdlLeadGeological,
            'mdlLead2d' => $mdlLead2d,
            'mdlLeadGravity' => $mdlLeadGravity,
            'mdlLeadGeochemistry' => $mdlLeadGeochemistry,
            'mdlLeadElectromagnetic' => $mdlLeadElectromagnetic,
            'mdlLeadResistivity' => $mdlLeadResistivity,
            'mdlLeadOther' => $mdlLeadOther,
            'mdlGcf' => $mdlGcf,
        ));
    }

    public function GetLastData($id, $array) {
        foreach ($array as $data) {
            if ($id == $data) {
                return true;
            }

        }
        return false;
    }

    public function actionUpdateDrillable($id) {
        $criteria1 = new CDbCriteria;
        $criteria1->with = array('plays');
        $criteria1->compare('prospect_id', $id);
        $criteria1->compare('plays.wk_id', Yii::app()->user->wk_id);
        $criteria1->compare('prospect_is_deleted', 0);

        $criteria2 = new CDbCriteria;
        $criteria2->compare('prospect_id', $id);

        if (!($mdlProspect = Prospect::model()->find($criteria1)) | !($mdlDrillable = Drillable::model()->find($criteria2))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $mdldrillableSearch = Prospect::model();
        $model = new Prospect('search');
        $model->unsetAttributes();

        $criteria = new CDbCriteria;
        $criteria->select = "max(b.prospect_submit_date)";
        $criteria->alias = 'b';
        $criteria->condition = "b.prospect_update_from = t.prospect_update_from";
        $subQuery = $model->getCommandBuilder()->createFindCommand($model->getTableSchema(), $criteria)->getText();

        $mainCriteria = new CDbCriteria;
        $mainCriteria->with = array('plays');
        $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
        $mainCriteria->addCondition('prospect_type = "drillable"', 'AND');
        $mainCriteria->addCondition('prospect_submit_date = (' . $subQuery . ')', 'AND');
        $mainCriteria->group = 'prospect_update_from';

        $drillableDataProvider = Prospect::model()->findAll($mainCriteria);

        $dataArray = array();
        foreach ($drillableDataProvider as $data) {
            $dataArray[] = $data->prospect_id;
        }

        if (!$this->GetLastData($id, $dataArray)) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $prospect_update_from = $mdlProspect->prospect_update_from;
        $prospect_submit_revision = $mdlProspect->prospect_submit_revision + 1;

        if (!($mdlGeological = Geological::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlGeological = new Geological();
            $mdlGeological->prospect_id = $id;
        } else {
            $mdlGeological->is_checked = 1;
        }

        if (!($mdlSeismic2d = Seismic2d::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlSeismic2d = new Seismic2d();
            $mdlSeismic2d->prospect_id = $id;
        } else {
            $mdlSeismic2d->is_checked = 1;
        }

        if (!($mdlGravity = Gravity::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlGravity = new Gravity();
            $mdlGravity->prospect_id = $id;
        } else {
            $mdlGravity->is_checked = 1;
        }

        if (!($mdlGeochemistry = Geochemistry::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlGeochemistry = new Geochemistry();
            $mdlGeochemistry->prospect_id = $id;
        } else {
            $mdlGeochemistry->is_checked = 1;
        }

        if (!($mdlElectromagnetic = Electromagnetic::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlElectromagnetic = new Electromagnetic();
            $mdlElectromagnetic->prospect_id = $id;
        } else {
            $mdlElectromagnetic->is_checked = 1;
        }

        if (!($mdlResistivity = Resistivity::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlResistivity = new Resistivity();
            $mdlResistivity->prospect_id = $id;
        } else {
            $mdlResistivity->is_checked = 1;
        }

        if (!($mdlOther = Other::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlOther = new Other();
            $mdlOther->prospect_id = $id;
        } else {
            $mdlOther->is_checked = 1;
        }

        if (!($mdlSeismic3d = Seismic3d::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlSeismic3d = new Seismic3d();
            $mdlSeismic3d->prospect_id = $id;
        } else {
            $mdlSeismic3d->is_checked = 1;
        }

        $mdlGcf = Gcf::model()->findByPk($mdlProspect->gcf_id);

        if (isset($mdlProspect->prospect_latitude) & isset($mdlProspect->prospect_longitude)) {
            $bonlat = preg_split("/[,]+/", $mdlProspect->prospect_latitude);
            $bonlong = preg_split("/[,]+/", $mdlProspect->prospect_longitude);

            $mdlProspect->center_lat_degree = $bonlat[0];
            $mdlProspect->center_lat_minute = $bonlat[1];
            $mdlProspect->center_lat_second = $bonlat[2];
            $mdlProspect->center_lat_direction = $bonlat[3];

            $mdlProspect->center_long_degree = $bonlong[0];
            $mdlProspect->center_long_minute = $bonlong[1];
            $mdlProspect->center_long_second = $bonlong[2];
            $mdlProspect->center_long_direction = $bonlong[3];
        }

        if (isset($_POST['Prospect'], $_POST['Drillable'], $_POST['Geological'], $_POST['Seismic2d'], $_POST['Gravity'], $_POST['Geochemistry'], $_POST['Electromagnetic'], $_POST['Resistivity'], $_POST['Other'], $_POST['Seismic3d'], $_POST['Gcf'])) {
            $mdlProspect->attributes = $_POST['Prospect'];
            $mdlDrillable->attributes = $_POST['Drillable'];
            $mdlGeological->attributes = $_POST['Geological'];
            $mdlSeismic2d->attributes = $_POST['Seismic2d'];
            $mdlGravity->attributes = $_POST['Gravity'];
            $mdlGeochemistry->attributes = $_POST['Geochemistry'];
            $mdlElectromagnetic->attributes = $_POST['Electromagnetic'];
            $mdlResistivity->attributes = $_POST['Resistivity'];
            $mdlOther->attributes = $_POST['Other'];
            $mdlSeismic3d->attributes = $_POST['Seismic3d'];
            $mdlGcf->attributes = $_POST['Gcf'];
            $mdlGcf->gcf_id_reference = $_POST['gcf_id_reference_update'];

            $mdlProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
            $mdlProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];

            if ($_POST['Gcf']['gcf_res_por_type'] == 'Average Primary Porosity Reservoir') {
                $mdlGcf->gcf_res_por_secondary = '';
            } else if ($_POST['Gcf']['gcf_res_por_type'] == 'Secondary Porosity') {
                $mdlGcf->gcf_res_por_primary = '';
            }

            $mdlProspect->prospect_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlProspect->prospect_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlProspect->prospect_submit_status = 'Update';
            }

            $mdlProspect->prospect_year = $this->year->rps_year;

            if ($mdlProspect->validate() & $mdlDrillable->validate() & $mdlGeological->validate() & $mdlSeismic2d->validate() & $mdlGravity->validate() & $mdlGeochemistry->validate() & $mdlElectromagnetic->validate() & $mdlResistivity->validate() & $mdlOther->validate() & $mdlSeismic3d->validate() & $mdlGcf->validate()) {
                $mdlProspect->save(false);
                $mdlDrillable->save(false);
                ($mdlGeological->is_checked == 1) ? $mdlGeological->save(false) : Geological::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlSeismic2d->is_checked == 1) ? $mdlSeismic2d->save(false) : Seismic2d::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlGravity->is_checked == 1) ? $mdlGravity->save(false) : Gravity::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlGeochemistry->is_checked == 1) ? $mdlGeochemistry->save(false) : Geochemistry::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlElectromagnetic->is_checked == 1) ? $mdlElectromagnetic->save(false) : Electromagnetic::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlResistivity->is_checked == 1) ? $mdlResistivity->save(false) : Resistivity::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlOther->is_checked == 1) ? $mdlOther->save(false) : Other::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlSeismic3d->is_checked == 1) ? $mdlSeismic3d->save(false) : Seismic3d::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                $mdlGcf->save(false);
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlProspect, $mdlDrillable, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                    //          echo json_encode(array('result' => 'error', 'msg' => CHtml::errorSummary(array($mdlProspect, $mdlDrillable, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf)), 'err'=>$error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatedrillable', array(
            'mdlProspect' => $mdlProspect,
            'mdlDrillable' => $mdlDrillable,
            'mdlGeological' => $mdlGeological,
            'mdlSeismic2d' => $mdlSeismic2d,
            'mdlGravity' => $mdlGravity,
            'mdlGeochemistry' => $mdlGeochemistry,
            'mdlElectromagnetic' => $mdlElectromagnetic,
            'mdlResistivity' => $mdlResistivity,
            'mdlOther' => $mdlOther,
            'mdlSeismic3d' => $mdlSeismic3d,
            'mdlGcf' => $mdlGcf,
        ));
    }

    public function actionUpdatePostdrill($id) {
        $criteria1 = new CDbCriteria;
        $criteria1->with = array('plays');
        $criteria1->compare('prospect_id', $id);
        $criteria1->compare('plays.wk_id', Yii::app()->user->wk_id);
        $criteria1->compare('prospect_is_deleted', 0);

        $criteria2 = new CDbCriteria;
        $criteria2->compare('prospect_id', $id);

        if (!($mdlProspect = Prospect::model()->find($criteria1)) | !($mdlPostdrill = Postdrill::model()->find($criteria2))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $mdlProspect->scenario = 'postdrill';

        $prospect_update_from = $mdlProspect->prospect_update_from;
        $prospect_submit_revision = $mdlProspect->prospect_submit_revision + 1;

        $mdlWell = Well::model()->findAllByAttributes(array('prospect_id' => $id));

        $Well = '';
        $j = 0;
        for ($i = 1; $i <= count($mdlWell); $i++) {
            $Well .= $mdlWell[$j++]->wl_id . ',';
        }

        $Well = substr($Well, 0, -1);

        if (!($mdlGeological = Geological::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlGeological = new Geological();
            $mdlGeological->prospect_id = $id;
        } else {
            $mdlGeological->is_checked = 1;
        }

        if (!($mdlSeismic2d = Seismic2d::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlSeismic2d = new Seismic2d();
            $mdlSeismic2d->prospect_id = $id;
        } else {
            $mdlSeismic2d->is_checked = 1;
        }

        if (!($mdlGravity = Gravity::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlGravity = new Gravity();
            $mdlGravity->prospect_id = $id;
        } else {
            $mdlGravity->is_checked = 1;
        }

        if (!($mdlGeochemistry = Geochemistry::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlGeochemistry = new Geochemistry();
            $mdlGeochemistry->prospect_id = $id;
        } else {
            $mdlGeochemistry->is_checked = 1;
        }

        if (!($mdlElectromagnetic = Electromagnetic::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlElectromagnetic = new Electromagnetic();
            $mdlElectromagnetic->prospect_id = $id;
        } else {
            $mdlElectromagnetic->is_checked = 1;
        }

        if (!($mdlResistivity = Resistivity::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlResistivity = new Resistivity();
            $mdlResistivity->prospect_id = $id;
        } else {
            $mdlResistivity->is_checked = 1;
        }

        if (!($mdlOther = Other::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlOther = new Other();
            $mdlOther->prospect_id = $id;
        } else {
            $mdlOther->is_checked = 1;
        }

        if (!($mdlSeismic3d = Seismic3d::model()->findByattributes(array('prospect_id' => $id)))) {
            $mdlSeismic3d = new Seismic3d();
            $mdlSeismic3d->prospect_id = $id;
        } else {
            $mdlSeismic3d->is_checked = 1;
        }

        $mdlGcf = Gcf::model()->findByPk($mdlProspect->gcf_id);

        if (isset($mdlProspect->prospect_latitude) & isset($mdlProspect->prospect_longitude)) {
            $bonlat = preg_split("/[,]+/", $mdlProspect->prospect_latitude);
            $bonlong = preg_split("/[,]+/", $mdlProspect->prospect_longitude);

            $mdlProspect->center_lat_degree = $bonlat[0];
            $mdlProspect->center_lat_minute = $bonlat[1];
            $mdlProspect->center_lat_second = $bonlat[2];
            $mdlProspect->center_lat_direction = $bonlat[3];

            $mdlProspect->center_long_degree = $bonlong[0];
            $mdlProspect->center_long_minute = $bonlong[1];
            $mdlProspect->center_long_second = $bonlong[2];
            $mdlProspect->center_long_direction = $bonlong[3];
        }

        $mdlProspect->well_name = $Well;

        if (isset($_POST['Prospect'], $_POST['Postdrill'], $_POST['Geological'], $_POST['Seismic2d'], $_POST['Gravity'], $_POST['Geochemistry'], $_POST['Electromagnetic'], $_POST['Resistivity'], $_POST['Other'], $_POST['Seismic3d'], $_POST['Gcf'])) {

            $mdlProspect->attributes = $_POST['Prospect'];
            $mdlPostdrill->attributes = $_POST['Postdrill'];
            $mdlGeological->attributes = $_POST['Geological'];
            $mdlSeismic2d->attributes = $_POST['Seismic2d'];
            $mdlGravity->attributes = $_POST['Gravity'];
            $mdlGeochemistry->attributes = $_POST['Geochemistry'];
            $mdlElectromagnetic->attributes = $_POST['Electromagnetic'];
            $mdlResistivity->attributes = $_POST['Resistivity'];
            $mdlOther->attributes = $_POST['Other'];
            $mdlSeismic3d->attributes = $_POST['Seismic3d'];
            $mdlGcf->attributes = $_POST['Gcf'];
            $mdlGcf->gcf_id_reference = $_POST['gcf_id_reference_update'];

            $mdlProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
            $mdlProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];

            if ($_POST['Gcf']['gcf_res_por_type'] == 'Average Primary Porosity Reservoir') {
                $mdlGcf->gcf_res_por_secondary = '';
            } else if ($_POST['Gcf']['gcf_res_por_type'] == 'Secondary Porosity') {
                $mdlGcf->gcf_res_por_primary = '';
            }

            $mdlProspect->prospect_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlProspect->prospect_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlProspect->prospect_submit_status = 'Update';
            }

            $mdlProspect->prospect_year = $this->year->rps_year;

            if ($mdlProspect->validate() & $mdlPostdrill->validate() & $mdlGeological->validate() & $mdlSeismic2d->validate() & $mdlGravity->validate() & $mdlGeochemistry->validate() & $mdlElectromagnetic->validate() & $mdlResistivity->validate() & $mdlOther->validate() & $mdlSeismic3d->validate() & $mdlGcf->validate()) {
                //mengganti well yang dipilih
                $mdlWell = Well::model()->updateAll(array('prospect_id' => null), 'prospect_id="' . $mdlProspect->prospect_id . '"');

                $string = $_POST['Prospect']['well_name'];
                $array = explode(',', $string);

                $j = 0;
                for ($i = 1; $i <= count($array); $i++) {
                    $mdlWell2 = Well::model()->findByPk($array[$j++]);
                    $mdlWell2->prospect_id = $_POST['Prospect']['prospect_id'];
                    $mdlWell2->save(false);
                }
                //end mengganti well yang dipilih

                $mdlProspect->save(false);
                $mdlPostdrill->save(false);
                ($mdlGeological->is_checked == 1) ? $mdlGeological->save(false) : Geological::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlSeismic2d->is_checked == 1) ? $mdlSeismic2d->save(false) : Seismic2d::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlGravity->is_checked == 1) ? $mdlGravity->save(false) : Gravity::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlGeochemistry->is_checked == 1) ? $mdlGeochemistry->save(false) : Geochemistry::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlElectromagnetic->is_checked == 1) ? $mdlElectromagnetic->save(false) : Electromagnetic::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlResistivity->is_checked == 1) ? $mdlResistivity->save(false) : Resistivity::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlOther->is_checked == 1) ? $mdlOther->save(false) : Other::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                ($mdlSeismic3d->is_checked == 1) ? $mdlSeismic3d->save(false) : Seismic3d::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $id));
                $mdlGcf->save(false);

                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlProspect, $mdlPostdrill, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatepostdrill', array(
            'mdlProspect' => $mdlProspect,
            'mdlPostdrill' => $mdlPostdrill,
            'mdlGeological' => $mdlGeological,
            'mdlSeismic2d' => $mdlSeismic2d,
            'mdlGravity' => $mdlGravity,
            'mdlGeochemistry' => $mdlGeochemistry,
            'mdlElectromagnetic' => $mdlElectromagnetic,
            'mdlResistivity' => $mdlResistivity,
            'mdlOther' => $mdlOther,
            'mdlSeismic3d' => $mdlSeismic3d,
            'mdlGcf' => $mdlGcf,
        ));
    }

    public function actionUpdateDiscovery($id) {
        $criteria1 = new CDbCriteria;
        $criteria1->with = array('plays');
        $criteria1->compare('prospect_id', $id);
        $criteria1->compare('plays.wk_id', Yii::app()->user->wk_id);
        $criteria1->compare('prospect_is_deleted', 0);

        $criteria2 = new CDbCriteria;
        $criteria2->compare('prospect_id', $id);

        if (!($mdlProspect = Prospect::model()->find($criteria1)) | !($discovery = Discovery::model()->find($criteria2))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $prospect_verifikasi = $mdlProspect->prospect_verifikasi;
        $prospect_update_from = $mdlProspect->prospect_update_from;
        $prospect_submit_revision = $mdlProspect->prospect_submit_revision + 1;

        $mdlProspect = new Prospect('discovery');
        $mdlDiscovery = new Discovery();
        $mdlGeological = new Geological();
        $mdlSeismic2d = new Seismic2d();
        $mdlGravity = new Gravity();
        $mdlGeochemistry = new Geochemistry();
        $mdlElectromagnetic = new Electromagnetic();
        $mdlResistivity = new Resistivity();
        $mdlOther = new Other();
        $mdlSeismic3d = new Seismic3d();
        $mdlGcf = new Gcf();

        if (isset($_POST['Prospect'])) {
            $model = array();

            $mdlProspect->attributes = $_POST['Prospect'];
            $mdlDiscovery->attributes = $_POST['Discovery'];
            $mdlGeological->attributes = $_POST['Geological'];
            $mdlSeismic2d->attributes = $_POST['Seismic2d'];
            $mdlGravity->attributes = $_POST['Gravity'];
            $mdlGeochemistry->attributes = $_POST['Geochemistry'];
            $mdlElectromagnetic->attributes = $_POST['Electromagnetic'];
            $mdlResistivity->attributes = $_POST['Resistivity'];
            $mdlOther->attributes = $_POST['Other'];
            $mdlSeismic3d->attributes = $_POST['Seismic3d'];
            $mdlGcf->attributes = $_POST['Gcf'];

            $mdlProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
            $mdlProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];
            $mdlProspect->prospect_type = 'discovery';

            if ($_POST['Gcf']['gcf_res_por_type'] == 'Average Primary Porosity Reservoir') {
                $mdlGcf->gcf_res_por_secondary = '';
            } else if ($_POST['Gcf']['gcf_res_por_type'] == 'Secondary Porosity') {
                $mdlGcf->gcf_res_por_primary = '';
            }

            $mdlProspect->prospect_submit_date = new CDbExpression('NOW()');

            if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                $mdlProspect->prospect_submit_status = 'Original';
            } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                $mdlProspect->prospect_submit_status = 'Update';
            }

            $valid = $mdlProspect->validate();
            $valid = $mdlDiscovery->validate() && $valid;
            $valid = $mdlGeological->validate() && $valid;
            $valid = $mdlSeismic2d->validate() && $valid;
            $valid = $mdlGravity->validate() && $valid;
            $valid = $mdlGeochemistry->validate() && $valid;
            $valid = $mdlElectromagnetic->validate() && $valid;
            $valid = $mdlResistivity->validate() && $valid;
            $valid = $mdlOther->validate() && $valid;
            $valid = $mdlSeismic3d->validate() && $valid;
            $valid = $mdlGcf->validate() && $valid;

            array_push($model, $mdlProspect);
            array_push($model, $mdlDiscovery);

            array_push($model, $mdlGeological);
            array_push($model, $mdlSeismic2d);
            array_push($model, $mdlGravity);
            array_push($model, $mdlGeochemistry);
            array_push($model, $mdlElectromagnetic);
            array_push($model, $mdlResistivity);
            array_push($model, $mdlOther);
            array_push($model, $mdlSeismic3d);
            array_push($model, $mdlGcf);

            $error = array();
            foreach ($model as $mdl) {
                array_push($error, $mdl);
            }

            if ($valid) {

                //        if(!empty($_POST['Prospect']['prospect_id']))
                //        {
                $mdlUpdateProspect = Prospect::model()->findByPk($_POST['Prospect']['prospect_id']);
                $mdlUpdateProspect->attributes = $_POST['Prospect'];

                $mdlUpdateProspect->prospect_latitude = $_POST['Prospect']['center_lat_degree'] . ',' . $_POST['Prospect']['center_lat_minute'] . ',' . $_POST['Prospect']['center_lat_second'] . ',' . $_POST['Prospect']['center_lat_direction'];
                $mdlUpdateProspect->prospect_longitude = $_POST['Prospect']['center_long_degree'] . ',' . $_POST['Prospect']['center_long_minute'] . ',' . $_POST['Prospect']['center_long_second'] . ',' . $_POST['Prospect']['center_long_direction'];

                $mdlUpdateProspect->prospect_submit_date = new CDbExpression('NOW()');

                if ($this->today >= $this->original->begin && $this->today <= $this->original->end) {
                    $mdlUpdateProspect->prospect_submit_status = 'Original';
                } else if ($this->today >= $this->update->begin && $this->today <= $this->update->end) {
                    $mdlUpdateProspect->prospect_submit_status = 'Update';
                }

                $mdlUpdateProspect->prospect_year = $this->year->rps_year;
                $mdlUpdateProspect->save(false);
                //        }

                //mengganti well yang dipilih
                $mdlWell = Well::model()->updateAll(array('prospect_id' => null), 'prospect_id="' . $mdlUpdateProspect->prospect_id . '"');

                $string = $_POST['Prospect']['well_name'];
                $array = explode(',', $string);

                $j = 0;
                for ($i = 1; $i <= count($array); $i++) {
                    $mdlWell2 = Well::model()->findByPk($array[$j++]);
                    $mdlWell2->prospect_id = $_POST['Prospect']['prospect_id'];
                    $mdlWell2->save(false);
                }

                //end mengganti well yang dipilih

                if (isset($_POST['Discovery']['dc_id'])) {
                    $mdlUpdateDiscovery = Discovery::model()->findByPk($_POST['Discovery']['dc_id']);
                    $mdlUpdateDiscovery->attributes = $_POST['Discovery'];
                    $mdlUpdateDiscovery->save(false);
                }

                if ($_POST['Geological']['is_checked'] == 1) {
                    if (!empty($_POST['Geological']['sgf_id'])) {
                        $mdlUpdateGeological = Geological::model()->findByPk($_POST['Geological']['sgf_id']);
                        $mdlUpdateGeological->attributes = $_POST['Geological'];
                        $mdlUpdateGeological->save(false);

                    } else {
                        $mdlUpdateGeological = new Geological();
                        $mdlUpdateGeological->attributes = $_POST['Geological'];
                        $mdlUpdateGeological->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateGeological->save(false);
                    }
                } else {
                    Geological::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Seismic2d']['is_checked'] == 1) {
                    if (!empty($_POST['Seismic2d']['s2d_id'])) {
                        $mdlUpdateSeismic2d = Seismic2d::model()->findByPk($_POST['Seismic2d']['s2d_id']);
                        $mdlUpdateSeismic2d->attributes = $_POST['Seismic2d'];
                        $mdlUpdateSeismic2d->save(false);
                    } else {
                        $mdlUpdateSeismic2d = new Seismic2d();
                        $mdlUpdateSeismic2d->attributes = $_POST['Seismic2d'];
                        $mdlUpdateSeismic2d->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateSeismic2d->save(false);
                    }
                } else {
                    Seismic2d::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Gravity']['is_checked'] == 1) {
                    if (!empty($_POST['Gravity']['sgv_id'])) {
                        $mdlUpdateGravity = Gravity::model()->findByPk($_POST['Gravity']['sgv_id']);
                        $mdlUpdateGravity->attributes = $_POST['Gravity'];
                        $mdlUpdateGravity->save(false);
                    } else {
                        $mdlUpdateGravity = new Gravity();
                        $mdlUpdateGravity->attributes = $_POST['Gravity'];
                        $mdlUpdateGravity->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateGravity->save(false);
                    }
                } else {
                    Gravity::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Geochemistry']['is_checked'] == 1) {
                    if (!empty($_POST['Geochemistry']['sgc_id'])) {
                        $mdlUpdateGeochemistry = Geochemistry::model()->findByPk($_POST['Geochemistry']['sgc_id']);
                        $mdlUpdateGeochemistry->attributes = $_POST['Geochemistry'];
                        $mdlUpdateGeochemistry->save(false);
                    } else {
                        $mdlUpdateGeochemistry = new Geochemistry();
                        $mdlUpdateGeochemistry->attributes = $_POST['Geochemistry'];
                        $mdlUpdateGeochemistry->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateGeochemistry->save(false);
                    }
                } else {
                    Geochemistry::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Electromagnetic']['is_checked'] == 1) {
                    if (!empty($_POST['Electromagnetic']['sel_id'])) {
                        $mdlUpdateElectromagnetic = Electromagnetic::model()->findByPk($_POST['Electromagnetic']['sel_id']);
                        $mdlUpdateElectromagnetic->attributes = $_POST['Electromagnetic'];
                        $mdlUpdateElectromagnetic->save(false);
                    } else {
                        $mdlUpdateElectromagnetic = new Electromagnetic();
                        $mdlUpdateElectromagnetic->attributes = $_POST['Electromagnetic'];
                        $mdlUpdateElectromagnetic->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateElectromagnetic->save(false);
                    }
                } else {
                    Electromagnetic::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Resistivity']['is_checked'] == 1) {
                    if (!empty($_POST['Resistivity']['rst_id'])) {
                        $mdlUpdateResistivity = Resistivity::model()->findByPk($_POST['Resistivity']['rst_id']);
                        $mdlUpdateResistivity->attributes = $_POST['Resistivity'];
                        $mdlUpdateResistivity->save(false);
                    } else {
                        $mdlUpdateResistivity = new Resistivity();
                        $mdlUpdateResistivity->attributes = $_POST['Resistivity'];
                        $mdlUpdateResistivity->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateResistivity->save(false);
                    }
                } else {
                    Resistivity::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Other']['is_checked'] == 1) {
                    if (!empty($_POST['Other']['sor_id'])) {
                        $mdlUpdateOther = Other::model()->findByPk($_POST['Other']['sor_id']);
                        $mdlUpdateOther->attributes = $_POST['Other'];
                        $mdlUpdateOther->save(false);
                    } else {
                        $mdlUpdateOther = new Other();
                        $mdlUpdateOther->attributes = $_POST['Other'];
                        $mdlUpdateOther->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateOther->save(false);
                    }
                } else {
                    Other::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if ($_POST['Seismic3d']['is_checked'] == 1) {
                    if (!empty($_POST['Seismic3d']['s3d_id'])) {
                        $mdlUpdateSeismic3d = Seismic3d::model()->findByPk($_POST['Seismic3d']['s3d_id']);
                        $mdlUpdateSeismic3d->attributes = $_POST['Seismic3d'];
                        $mdlUpdateSeismic3d->save(false);
                    } else {
                        $mdlUpdateSeismic3d = new Seismic3d();
                        $mdlUpdateSeismic3d->attributes = $_POST['Seismic3d'];
                        $mdlUpdateSeismic3d->prospect_id = $mdlUpdateProspect->prospect_id;
                        $mdlUpdateSeismic3d->save(false);
                    }
                } else {
                    Seismic3d::model()->deleteAll('`prospect_id` = :prospect_id', array(':prospect_id' => $mdlUpdateProspect->prospect_id));
                }

                if (isset($_POST['Gcf']['gcf_id'])) {
                    $mdlUpdateGcf = Gcf::model()->findByPk($_POST['Gcf']['gcf_id']);
                    $mdlUpdateGcf->attributes = $_POST['Gcf'];
                    $mdlUpdateGcf->gcf_id_reference = $_POST['gcf_id_reference_update'];

                    $mdlUpdateGcf->save(false);
                }
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlProspect, $mdlDiscovery, $mdlGeological, $mdlSeismic2d, $mdlGravity, $mdlGeochemistry, $mdlElectromagnetic, $mdlResistivity, $mdlOther, $mdlSeismic3d, $mdlGcf));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }

        }

        $this->render('updatediscovery', array(
            'mdlProspect' => $mdlProspect,
            'mdlDiscovery' => $mdlDiscovery,
            'mdlGeological' => $mdlGeological,
            'mdlSeismic2d' => $mdlSeismic2d,
            'mdlGravity' => $mdlGravity,
            'mdlGeochemistry' => $mdlGeochemistry,
            'mdlElectromagnetic' => $mdlElectromagnetic,
            'mdlResistivity' => $mdlResistivity,
            'mdlOther' => $mdlOther,
            'mdlSeismic3d' => $mdlSeismic3d,
            'mdlGcf' => $mdlGcf,
            'id' => $id,
        ));
    }

    public function actionGetGcf() {
        if (Yii::app()->request->isAjaxRequest) {
            $playId = $_POST['play_id'];
            $mdlGcf = Gcf::model()->with(array('plays' => array('condition' => 'play_id="' . $playId . '"')))->find();

            echo CJSON::encode(array(
                'gcf_id' => $mdlGcf->gcf_id,
                'gcf_is_sr' => $mdlGcf->gcf_is_sr,
                'gcf_sr_age_system' => $mdlGcf->gcf_sr_age_system,
                'gcf_sr_age_serie' => $mdlGcf->gcf_sr_age_serie,
                'gcf_sr_formation' => $mdlGcf->gcf_sr_formation,
                'gcf_sr_formation_serie' => $mdlGcf->gcf_sr_formation_serie,
                'gcf_sr_kerogen' => $mdlGcf->gcf_sr_kerogen,
                'gcf_sr_toc' => $mdlGcf->gcf_sr_toc,
                'gcf_sr_hfu' => $mdlGcf->gcf_sr_hfu,
                'gcf_sr_distribution' => $mdlGcf->gcf_sr_distribution,
                'gcf_sr_continuity' => $mdlGcf->gcf_sr_continuity,
                'gcf_sr_maturity' => $mdlGcf->gcf_sr_maturity,
                'gcf_sr_otr' => $mdlGcf->gcf_sr_otr,
                'gcf_sr_remark' => $mdlGcf->gcf_sr_remark,
                'gcf_is_res' => $mdlGcf->gcf_is_res,
                'gcf_res_age_system' => $mdlGcf->gcf_res_age_system,
                'gcf_res_age_serie' => $mdlGcf->gcf_res_age_serie,
                'gcf_res_formation' => $mdlGcf->gcf_res_formation,
                'gcf_res_formation_serie' => $mdlGcf->gcf_res_formation_serie,
                'gcf_res_depos_set' => $mdlGcf->gcf_res_depos_set,
                'gcf_res_depos_env' => $mdlGcf->gcf_res_depos_env,
                'gcf_res_distribution' => $mdlGcf->gcf_res_distribution,
                'gcf_res_continuity' => $mdlGcf->gcf_res_continuity,
                'gcf_res_lithology' => $mdlGcf->gcf_res_lithology,
                'gcf_res_por_type' => $mdlGcf->gcf_res_por_type,
                'gcf_res_por_primary' => $mdlGcf->gcf_res_por_primary,
                'gcf_res_por_secondary' => $mdlGcf->gcf_res_por_secondary,
                'gcf_res_remark' => $mdlGcf->gcf_res_remark,
                'gcf_is_trap' => $mdlGcf->gcf_is_trap,
                'gcf_trap_seal_age_system' => $mdlGcf->gcf_trap_seal_age_system,
                'gcf_trap_seal_age_serie' => $mdlGcf->gcf_trap_seal_age_serie,
                'gcf_trap_seal_formation' => $mdlGcf->gcf_trap_seal_formation,
                'gcf_trap_seal_formation_serie' => $mdlGcf->gcf_trap_seal_formation_serie,
                'gcf_trap_seal_distribution' => $mdlGcf->gcf_trap_seal_distribution,
                'gcf_trap_seal_continuity' => $mdlGcf->gcf_trap_seal_continuity,
                'gcf_trap_seal_type' => $mdlGcf->gcf_trap_seal_type,
                'gcf_trap_age_system' => $mdlGcf->gcf_trap_age_system,
                'gcf_trap_age_serie' => $mdlGcf->gcf_trap_age_serie,
                'gcf_trap_geometry' => $mdlGcf->gcf_trap_geometry,
                'gcf_trap_type' => $mdlGcf->gcf_trap_type,
                'gcf_trap_closure' => $mdlGcf->gcf_trap_closure,
                'gcf_trap_remark' => $mdlGcf->gcf_trap_remark,
                'gcf_is_dyn' => $mdlGcf->gcf_is_dyn,
                'gcf_dyn_migration' => $mdlGcf->gcf_dyn_migration,
                'gcf_dyn_kitchen' => $mdlGcf->gcf_dyn_kitchen,
                'gcf_dyn_petroleum' => $mdlGcf->gcf_dyn_petroleum,
                'gcf_dyn_early_age_system' => $mdlGcf->gcf_dyn_early_age_system,
                'gcf_dyn_early_age_serie' => $mdlGcf->gcf_dyn_early_age_serie,
                'gcf_dyn_late_age_system' => $mdlGcf->gcf_dyn_late_age_system,
                'gcf_dyn_late_age_serie' => $mdlGcf->gcf_dyn_late_age_serie,
                'gcf_dyn_preservation' => $mdlGcf->gcf_dyn_preservation,
                'gcf_dyn_pathways' => $mdlGcf->gcf_dyn_pathways,
                'gcf_dyn_migration_age_system' => $mdlGcf->gcf_dyn_migration_age_system,
                'gcf_dyn_migration_age_serie' => $mdlGcf->gcf_dyn_migration_age_serie,
                'gcf_dyn_remark' => $mdlGcf->gcf_dyn_remark,
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400, 'Invalid request. You are trying to access this comment for which you are not permitted.');
        }
    }

    public function actionOriGcf() {
        if (Yii::app()->request->isAjaxRequest) {
            $gcf_id_ori = $_POST['gcf_id_ori'];
            $mdlGcf = Gcf::model()->findByPk($gcf_id_ori);

            echo CJSON::encode(array(
                'gcf_id' => $mdlGcf->gcf_id,
                'gcf_is_sr' => $mdlGcf->gcf_is_sr,
                'gcf_sr_age_system' => $mdlGcf->gcf_sr_age_system,
                'gcf_sr_age_serie' => $mdlGcf->gcf_sr_age_serie,
                'gcf_sr_formation' => $mdlGcf->gcf_sr_formation,
                'gcf_sr_formation_serie' => $mdlGcf->gcf_sr_formation_serie,
                'gcf_sr_kerogen' => $mdlGcf->gcf_sr_kerogen,
                'gcf_sr_toc' => $mdlGcf->gcf_sr_toc,
                'gcf_sr_hfu' => $mdlGcf->gcf_sr_hfu,
                'gcf_sr_distribution' => $mdlGcf->gcf_sr_distribution,
                'gcf_sr_continuity' => $mdlGcf->gcf_sr_continuity,
                'gcf_sr_maturity' => $mdlGcf->gcf_sr_maturity,
                'gcf_sr_otr' => $mdlGcf->gcf_sr_otr,
                'gcf_sr_remark' => $mdlGcf->gcf_sr_remark,
                'gcf_is_res' => $mdlGcf->gcf_is_res,
                'gcf_res_age_system' => $mdlGcf->gcf_res_age_system,
                'gcf_res_age_serie' => $mdlGcf->gcf_res_age_serie,
                'gcf_res_formation' => $mdlGcf->gcf_res_formation,
                'gcf_res_formation_serie' => $mdlGcf->gcf_res_formation_serie,
                'gcf_res_depos_set' => $mdlGcf->gcf_res_depos_set,
                'gcf_res_depos_env' => $mdlGcf->gcf_res_depos_env,
                'gcf_res_distribution' => $mdlGcf->gcf_res_distribution,
                'gcf_res_continuity' => $mdlGcf->gcf_res_continuity,
                'gcf_res_lithology' => $mdlGcf->gcf_res_lithology,
                'gcf_res_por_type' => $mdlGcf->gcf_res_por_type,
                'gcf_res_por_primary' => $mdlGcf->gcf_res_por_primary,
                'gcf_res_por_secondary' => $mdlGcf->gcf_res_por_secondary,
                'gcf_res_remark' => $mdlGcf->gcf_res_remark,
                'gcf_is_trap' => $mdlGcf->gcf_is_trap,
                'gcf_trap_seal_age_system' => $mdlGcf->gcf_trap_seal_age_system,
                'gcf_trap_seal_age_serie' => $mdlGcf->gcf_trap_seal_age_serie,
                'gcf_trap_seal_formation' => $mdlGcf->gcf_trap_seal_formation,
                'gcf_trap_seal_formation_serie' => $mdlGcf->gcf_trap_seal_formation_serie,
                'gcf_trap_seal_distribution' => $mdlGcf->gcf_trap_seal_distribution,
                'gcf_trap_seal_continuity' => $mdlGcf->gcf_trap_seal_continuity,
                'gcf_trap_seal_type' => $mdlGcf->gcf_trap_seal_type,
                'gcf_trap_age_system' => $mdlGcf->gcf_trap_age_system,
                'gcf_trap_age_serie' => $mdlGcf->gcf_trap_age_serie,
                'gcf_trap_geometry' => $mdlGcf->gcf_trap_geometry,
                'gcf_trap_type' => $mdlGcf->gcf_trap_type,
                'gcf_trap_closure' => $mdlGcf->gcf_trap_closure,
                'gcf_trap_remark' => $mdlGcf->gcf_trap_remark,
                'gcf_is_dyn' => $mdlGcf->gcf_is_dyn,
                'gcf_dyn_migration' => $mdlGcf->gcf_dyn_migration,
                'gcf_dyn_kitchen' => $mdlGcf->gcf_dyn_kitchen,
                'gcf_dyn_petroleum' => $mdlGcf->gcf_dyn_petroleum,
                'gcf_dyn_early_age_system' => $mdlGcf->gcf_dyn_early_age_system,
                'gcf_dyn_early_age_serie' => $mdlGcf->gcf_dyn_early_age_serie,
                'gcf_dyn_late_age_system' => $mdlGcf->gcf_dyn_late_age_system,
                'gcf_dyn_late_age_serie' => $mdlGcf->gcf_dyn_late_age_serie,
                'gcf_dyn_preservation' => $mdlGcf->gcf_dyn_preservation,
                'gcf_dyn_pathways' => $mdlGcf->gcf_dyn_pathways,
                'gcf_dyn_migration_age_system' => $mdlGcf->gcf_dyn_migration_age_system,
                'gcf_dyn_migration_age_serie' => $mdlGcf->gcf_dyn_migration_age_serie,
                'gcf_dyn_remark' => $mdlGcf->gcf_dyn_remark,
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400, 'Invalid request. You are trying to access this comment for which you are not permitted.');
        }
    }

    public function actionTambahWell() {
        if (Yii::app()->request->isAjaxRequest) {
            $form = $_POST['form'];
            $mdlWellPostdrillj = new WellPostdrill();
            $this->renderPartial('_tes', array('form' => $form, 'mdlWellPostdrillj' => $mdlWellPostdrillj), false, true);
            Yii::app()->end();
        } else {
            throw new CHttpException(400, 'Invalid request. You are trying to access this comment for which you are not permitted.');
        }
    }

    public function actionGetPreviousDataPlay($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlPlay = Play::model()->findByPk($id);
            $mdlGcf = Gcf::model()->findByPk($mdlPlay->gcf_id);

            $bonlat = preg_split("/[,]+/", $mdlPlay->play_latitude);
            $bonlong = preg_split("/[,]+/", $mdlPlay->play_longitude);

            $mdlPlay->bound_lat_top_left_degree = $bonlat[0];
            $mdlPlay->bound_lat_top_left_minute = $bonlat[1];
            $mdlPlay->bound_lat_top_left_second = $bonlat[2];
            $mdlPlay->bound_lat_top_left_direction = $bonlat[3];
            $mdlPlay->bound_lat_bottom_right_degree = $bonlat[4];
            $mdlPlay->bound_lat_bottom_right_minute = $bonlat[5];
            $mdlPlay->bound_lat_bottom_right_second = $bonlat[6];
            $mdlPlay->bound_lat_bottom_right_direction = $bonlat[7];

            $mdlPlay->bound_long_top_left_degree = $bonlong[0];
            $mdlPlay->bound_long_top_left_minute = $bonlong[1];
            $mdlPlay->bound_long_top_left_second = $bonlong[2];
            $mdlPlay->bound_long_top_left_direction = $bonlong[3];
            $mdlPlay->bound_long_bottom_right_degree = $bonlong[4];
            $mdlPlay->bound_long_bottom_right_minute = $bonlong[5];
            $mdlPlay->bound_long_bottom_right_second = $bonlong[6];
            $mdlPlay->bound_long_bottom_right_direction = $bonlong[7];

            if (strpos($mdlGcf->gcf_sr_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_sr_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_sr_formation);
                $mdlGcf->gcf_sr_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_sr_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_sr_formation_name_pre = $mdlGcf->gcf_sr_formation;
            }

            if (strpos($mdlGcf->gcf_res_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_res_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_res_formation);
                $mdlGcf->gcf_res_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_res_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_res_formation_name_pre = $mdlGcf->gcf_res_formation;
            }

            if (strpos($mdlGcf->gcf_trap_seal_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_trap_seal_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_trap_seal_formation);
                $mdlGcf->gcf_trap_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_trap_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_trap_formation_name_pre = $mdlGcf->gcf_trap_seal_formation;
            }

            echo CJSON::encode(array(
                'status' => 'berhasil',
                'error' => false,
                'play_update_from' => $mdlPlay->play_update_from,
                'play_submit_revision' => $mdlPlay->play_submit_revision,
                'bound_lat_top_left_degree' => $mdlPlay->bound_lat_top_left_degree,
                'bound_lat_top_left_minute' => $mdlPlay->bound_lat_top_left_minute,
                'bound_lat_top_left_second' => $mdlPlay->bound_lat_top_left_second,
                'bound_lat_top_left_direction' => $mdlPlay->bound_lat_top_left_direction,
                'bound_lat_bottom_right_degree' => $mdlPlay->bound_lat_bottom_right_degree,
                'bound_lat_bottom_right_minute' => $mdlPlay->bound_lat_bottom_right_minute,
                'bound_lat_bottom_right_second' => $mdlPlay->bound_lat_bottom_right_second,
                'bound_lat_bottom_right_direction' => $mdlPlay->bound_lat_bottom_right_direction,
                'bound_long_top_left_degree' => $mdlPlay->bound_long_top_left_degree,
                'bound_long_top_left_minute' => $mdlPlay->bound_long_top_left_minute,
                'bound_long_top_left_second' => $mdlPlay->bound_long_top_left_second,
                'bound_long_top_left_direction' => $mdlPlay->bound_long_top_left_direction,
                'bound_long_bottom_right_degree' => $mdlPlay->bound_long_bottom_right_degree,
                'bound_long_bottom_right_minute' => $mdlPlay->bound_long_bottom_right_minute,
                'bound_long_bottom_right_second' => $mdlPlay->bound_long_bottom_right_second,
                'bound_long_bottom_right_direction' => $mdlPlay->bound_long_bottom_right_direction,
                'basin_id' => $mdlPlay->basin_id,
                'province_id' => $mdlPlay->province_id,
                'play_analog_to' => $mdlPlay->play_analog_to,
                'play_analog_distance' => $mdlPlay->play_analog_distance,
                'play_exr_method' => $mdlPlay->play_exr_method,
                'play_shore' => $mdlPlay->play_shore,
                'play_terrain' => $mdlPlay->play_terrain,
                'play_near_field' => $mdlPlay->play_near_field,
                'play_near_infra_structure' => $mdlPlay->play_near_infra_structure,
                'play_support_data' => $mdlPlay->play_support_data,
                'play_outcrop_distance' => $mdlPlay->play_outcrop_distance,
                'play_s2d_year' => $mdlPlay->play_s2d_year,
                'play_s2d_crossline' => $mdlPlay->play_s2d_crossline,
                'play_s2d_line_intervall' => $mdlPlay->play_s2d_line_intervall,
                'play_s2d_img_quality' => $mdlPlay->play_s2d_img_quality,
                'play_sgc' => $mdlPlay->play_sgc,
                'play_sgc_sample' => $mdlPlay->play_sgc_sample,
                'play_sgc_depth' => $mdlPlay->play_sgc_depth,
                'play_sgv' => $mdlPlay->play_sgv,
                'play_sgv_acre' => $mdlPlay->play_sgv_acre,
                'play_sgv_depth' => $mdlPlay->play_sgv_depth,
                'play_sel' => $mdlPlay->play_sel,
                'play_sel_acre' => $mdlPlay->play_sel_acre,
                'play_sel_depth' => $mdlPlay->play_sel_depth,
                'play_srt' => $mdlPlay->play_srt,
                'play_srt_acre' => $mdlPlay->play_srt_acre,
                'play_map_scale' => $mdlPlay->play_map_scale,
                'play_map_author' => $mdlPlay->play_map_author,
                'play_map_year' => $mdlPlay->play_map_year,
                'play_remark' => $mdlPlay->play_remark,
                'gcf_is_sr' => $mdlGcf->gcf_is_sr,
                'gcf_sr_age_system' => $mdlGcf->gcf_sr_age_system,
                'gcf_sr_age_serie' => $mdlGcf->gcf_sr_age_serie,
                'gcf_sr_formation_name_pre' => $mdlGcf->gcf_sr_formation_name_pre,
                'pgcf_sr_formation_name_post' => $mdlGcf->gcf_sr_formation_name_post,
                'gcf_sr_kerogen' => $mdlGcf->gcf_sr_kerogen,
                'gcf_sr_toc' => $mdlGcf->gcf_sr_toc,
                'gcf_sr_hfu' => $mdlGcf->gcf_sr_hfu,
                'gcf_sr_distribution' => $mdlGcf->gcf_sr_distribution,
                'gcf_sr_continuity' => $mdlGcf->gcf_sr_continuity,
                'gcf_sr_maturity' => $mdlGcf->gcf_sr_maturity,
                'gcf_sr_otr' => $mdlGcf->gcf_sr_otr,
                'gcf_sr_remark' => $mdlGcf->gcf_sr_remark,
                'gcf_is_res' => $mdlGcf->gcf_is_res,
                'gcf_res_age_system' => $mdlGcf->gcf_res_age_system,
                'gcf_res_age_serie' => $mdlGcf->gcf_res_age_serie,
                'gcf_res_formation_name_pre' => $mdlGcf->gcf_res_formation_name_pre,
                'pgcf_res_formation_name_post' => $mdlGcf->gcf_res_formation_name_post,
                'gcf_res_depos_set' => $mdlGcf->gcf_res_depos_set,
                'gcf_res_depos_env' => $mdlGcf->gcf_res_depos_env,
                'gcf_res_distribution' => $mdlGcf->gcf_res_distribution,
                'gcf_res_continuity' => $mdlGcf->gcf_res_continuity,
                'gcf_res_lithology' => $mdlGcf->gcf_res_lithology,
                'gcf_res_por_type' => $mdlGcf->gcf_res_por_type,
                'gcf_res_por_primary' => $mdlGcf->gcf_res_por_primary,
                'gcf_res_por_secondary' => $mdlGcf->gcf_res_por_secondary,
                'gcf_res_remark' => $mdlGcf->gcf_res_remark,
                'gcf_is_trap' => $mdlGcf->gcf_is_trap,
                'gcf_trap_seal_age_system' => $mdlGcf->gcf_trap_seal_age_system,
                'gcf_trap_seal_age_serie' => $mdlGcf->gcf_trap_seal_age_serie,
                'gcf_trap_formation_name_pre' => $mdlGcf->gcf_trap_formation_name_pre,
                'gcf_trap_formation_name_post' => $mdlGcf->gcf_trap_formation_name_post,
                'gcf_trap_seal_distribution' => $mdlGcf->gcf_trap_seal_distribution,
                'gcf_trap_seal_continuity' => $mdlGcf->gcf_trap_seal_continuity,
                'gcf_trap_seal_type' => $mdlGcf->gcf_trap_seal_type,
                'gcf_trap_age_system' => $mdlGcf->gcf_trap_age_system,
                'gcf_trap_age_serie' => $mdlGcf->gcf_trap_age_serie,
                'gcf_trap_geometry' => $mdlGcf->gcf_trap_geometry,
                'gcf_trap_type' => $mdlGcf->gcf_trap_type,
                'gcf_trap_closure' => $mdlGcf->gcf_trap_closure,
                'gcf_trap_remark' => $mdlGcf->gcf_trap_remark,
                'gcf_is_dyn' => $mdlGcf->gcf_is_dyn,
                'gcf_dyn_migration' => $mdlGcf->gcf_dyn_migration,
                'gcf_dyn_kitchen' => $mdlGcf->gcf_dyn_kitchen,
                'gcf_dyn_petroleum' => $mdlGcf->gcf_dyn_petroleum,
                'gcf_dyn_early_age_system' => $mdlGcf->gcf_dyn_early_age_system,
                'gcf_dyn_early_age_serie' => $mdlGcf->gcf_dyn_early_age_serie,
                'gcf_dyn_late_age_system' => $mdlGcf->gcf_dyn_late_age_system,
                'gcf_dyn_late_age_serie' => $mdlGcf->gcf_dyn_late_age_serie,
                'gcf_dyn_preservation' => $mdlGcf->gcf_dyn_preservation,
                'gcf_dyn_pathways' => $mdlGcf->gcf_dyn_pathways,
                'gcf_dyn_migration_age_system' => $mdlGcf->gcf_dyn_migration_age_system,
                'gcf_dyn_migration_age_serie' => $mdlGcf->gcf_dyn_migration_age_serie,
                'gcf_dyn_remark' => $mdlGcf->gcf_dyn_remark,
            ));
            Yii::app()->end();
        }
    }

    public function actionGetPreviousDataLead($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlLead = Lead::model()->with(array('plays' => array('condition' => 'wk_id="' . Yii::app()->user->wk_id . '"')))->findByPk($id);
            if (!($mdlLeadGeological = LeadGeological::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLeadGeological = new LeadGeological();
                $mdlLeadGeological->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLeadGeological->is_checked = 1;
            }

            if (!($mdlLead2d = Lead2d::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLead2d = new Lead2d();
                $mdlLead2d->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLead2d->is_checked = 1;
            }

            if (!($mdlLeadGravity = LeadGravity::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLeadGravity = new LeadGravity();
                $mdlLeadGravity->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLeadGravity->is_checked = 1;
            }

            if (!($mdlLeadGeochemistry = LeadGeochemistry::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLeadGeochemistry = new LeadGeochemistry();
                $mdlLeadGeochemistry->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLeadGeochemistry->is_checked = 1;
            }

            if (!($mdlLeadElectromagnetic = LeadElectromagnetic::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLeadElectromagnetic = new LeadElectromagnetic();
                $mdlLeadElectromagnetic->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLeadElectromagnetic->is_checked = 1;
            }

            if (!($mdlLeadResistivity = LeadResistivity::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLeadResistivity = new LeadResistivity();
                $mdlLeadResistivity->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLeadResistivity->is_checked = 1;
            }

            if (!($mdlLeadOther = LeadOther::model()->findByattributes(array('lead_id' => $mdlLead->lead_id)))) {
                $mdlLeadOther = new LeadOther();
                $mdlLeadOther->lead_id = $mdlLead->lead_id;
            } else {
                $mdlLeadOther->is_checked = 1;
            }
            $mdlGcf = Gcf::model()->findByPk($mdlLead->gcf_id);

            $bonlat = preg_split("/[,]+/", $mdlLead->lead_latitude);
            $bonlong = preg_split("/[,]+/", $mdlLead->lead_longitude);

            $mdlLead->center_lat_degree = $bonlat[0];
            $mdlLead->center_lat_minute = $bonlat[1];
            $mdlLead->center_lat_second = $bonlat[2];
            $mdlLead->center_lat_direction = $bonlat[3];

            $mdlLead->center_long_degree = $bonlong[0];
            $mdlLead->center_long_minute = $bonlong[1];
            $mdlLead->center_long_second = $bonlong[2];
            $mdlLead->center_long_direction = $bonlong[3];

            if (strpos($mdlGcf->gcf_sr_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_sr_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_sr_formation);
                $mdlGcf->gcf_sr_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_sr_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_sr_formation_name_pre = $mdlGcf->gcf_sr_formation;
            }

            if (strpos($mdlGcf->gcf_res_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_res_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_res_formation);
                $mdlGcf->gcf_res_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_res_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_res_formation_name_pre = $mdlGcf->gcf_res_formation;
            }

            if (strpos($mdlGcf->gcf_trap_seal_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_trap_seal_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_trap_seal_formation);
                $mdlGcf->gcf_trap_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_trap_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_trap_formation_name_pre = $mdlGcf->gcf_trap_seal_formation;
            }

            echo CJSON::encode(array(
                'error' => false,
                'lead_update_from' => $mdlLead->lead_update_from,
                'lead_submit_revision' => $mdlLead->lead_submit_revision,
                'structure_name' => $mdlLead->structure_name,
                'play_id' => $mdlLead->play_id,
                'lead_clarified' => $mdlLead->lead_clarified,
                'lead_year_study' => $mdlLead->lead_year_study,
                'lead_date_initiate' => $mdlLead->lead_date_initiate,
                'center_lat_degree' => $mdlLead->center_lat_degree,
                'center_lat_minute' => $mdlLead->center_lat_minute,
                'center_lat_second' => $mdlLead->center_lat_second,
                'center_lat_direction' => $mdlLead->center_lat_direction,
                'center_long_degree' => $mdlLead->center_long_degree,
                'center_long_minute' => $mdlLead->center_long_minute,
                'center_long_second' => $mdlLead->center_long_second,
                'center_long_direction' => $mdlLead->center_long_direction,

                'lead_shore' => $mdlLead->lead_shore,
                'lead_terrain' => $mdlLead->lead_terrain,
                'lead_near_field' => $mdlLead->lead_near_field,
                'lead_near_infra_structure' => $mdlLead->lead_near_infra_structure,
                'lsgf_is_check' => $mdlLeadGeological->is_checked,
                'lsgf_year_survey' => $mdlLeadGeological->lsgf_year_survey,
                'lsgf_survey_method' => $mdlLeadGeological->lsgf_survey_method,
                'lsgf_coverage_area' => $mdlLeadGeological->lsgf_coverage_area,
                'lsgf_low_estimate' => $mdlLeadGeological->lsgf_low_estimate,
                'lsgf_best_estimate' => $mdlLeadGeological->lsgf_best_estimate,
                'lsgf_high_estimate' => $mdlLeadGeological->lsgf_high_estimate,
                'lsgf_remark' => $mdlLeadGeological->lsgf_remark,
                'ls2d_is_check' => $mdlLead2d->is_checked,
                'ls2d_year_survey' => $mdlLead2d->ls2d_year_survey,
                'ls2d_vintage_number' => $mdlLead2d->ls2d_vintage_number,
                'ls2d_total_crossline' => $mdlLead2d->ls2d_total_crossline,
                'ls2d_total_coverage' => $mdlLead2d->ls2d_total_coverage,
                'ls2d_average_interval' => $mdlLead2d->ls2d_average_interval,
                'ls2d_year_late_process' => $mdlLead2d->ls2d_year_late_process,
                'ls2d_late_method' => $mdlLead2d->ls2d_late_method,
                'ls2d_img_quality' => $mdlLead2d->ls2d_img_quality,
                'ls2d_low_estimate' => $mdlLead2d->ls2d_low_estimate,
                'ls2d_best_estimate' => $mdlLead2d->ls2d_best_estimate,
                'ls2d_hight_estimate' => $mdlLead2d->ls2d_hight_estimate,
                'ls2d_remark' => $mdlLead2d->ls2d_remark,
                'lsgv_is_check' => $mdlLeadGravity->is_checked,
                'lsgv_year_survey' => $mdlLeadGravity->lsgv_year_survey,
                'lsgv_survey_method' => $mdlLeadGravity->lsgv_survey_method,
                'lsgv_coverage_area' => $mdlLeadGravity->lsgv_coverage_area,
                'lsgv_range_penetration' => $mdlLeadGravity->lsgv_range_penetration,
                'lsgv_spacing_interval' => $mdlLeadGravity->lsgv_spacing_interval,
                'lsgv_low_estimate' => $mdlLeadGravity->lsgv_low_estimate,
                'lsgv_best_estimate' => $mdlLeadGravity->lsgv_best_estimate,
                'lsgv_high_estimate' => $mdlLeadGravity->lsgv_high_estimate,
                'lsgv_remark' => $mdlLeadGravity->lsgv_remark,
                'lsgc_is_check' => $mdlLeadGeochemistry->is_checked,
                'lsgc_year_survey' => $mdlLeadGeochemistry->lsgc_year_survey,
                'lsgc_range_interval' => $mdlLeadGeochemistry->lsgc_range_interval,
                'lsgc_number_sample' => $mdlLeadGeochemistry->lsgc_number_sample,
                'lsgc_number_rock' => $mdlLeadGeochemistry->lsgc_number_rock,
                'lsgc_number_fluid' => $mdlLeadGeochemistry->lsgc_number_fluid,
                'lsgc_hc_availability' => $mdlLeadGeochemistry->lsgc_hc_availability,
                'lsgc_hc_composition' => $mdlLeadGeochemistry->lsgc_hc_composition,
                'lsgc_year_report' => $mdlLeadGeochemistry->lsgc_year_report,
                'lsgc_low_estimate' => $mdlLeadGeochemistry->lsgc_low_estimate,
                'lsgc_best_estimate' => $mdlLeadGeochemistry->lsgc_best_estimate,
                'lsgc_high_estimate' => $mdlLeadGeochemistry->lsgc_high_estimate,
                'lsgc_remark' => $mdlLeadGeochemistry->lsgc_remark,
                'lsel_is_check' => $mdlLeadElectromagnetic->is_checked,
                'lsel_year_survey' => $mdlLeadElectromagnetic->lsel_year_survey,
                'lsel_survey_method' => $mdlLeadElectromagnetic->lsel_survey_method,
                'lsel_coverage_area' => $mdlLeadElectromagnetic->lsel_coverage_area,
                'lsel_range_penetration' => $mdlLeadElectromagnetic->lsel_range_penetration,
                'lsel_spacing_interval' => $mdlLeadElectromagnetic->lsel_spacing_interval,
                'lsel_low_estimate' => $mdlLeadElectromagnetic->lsel_low_estimate,
                'lsel_best_estimate' => $mdlLeadElectromagnetic->lsel_best_estimate,
                'lsel_high_estimate' => $mdlLeadElectromagnetic->lsel_high_estimate,
                'lsel_remark' => $mdlLeadElectromagnetic->lsel_remark,
                'lsrt_is_check' => $mdlLeadResistivity->is_checked,
                'lsrt_year_survey' => $mdlLeadResistivity->lsrt_year_survey,
                'lsrt_survey_method' => $mdlLeadResistivity->lsrt_survey_method,
                'lsrt_coverage_area' => $mdlLeadResistivity->lsrt_coverage_area,
                'lsrt_range_penetration' => $mdlLeadResistivity->lsrt_range_penetration,
                'lsrt_spacing_interval' => $mdlLeadResistivity->lsrt_spacing_interval,
                'lsrt_low_estimate' => $mdlLeadResistivity->lsrt_low_estimate,
                'lsrt_best_estimate' => $mdlLeadResistivity->lsrt_best_estimate,
                'lsrt_high_estimate' => $mdlLeadResistivity->lsrt_high_estimate,
                'lsrt_remark' => $mdlLeadResistivity->lsrt_remark,
                'lsor_is_check' => $mdlLeadOther->is_checked,
                'lsor_year_survey' => $mdlLeadOther->lsor_year_survey,
                'lsor_low_estimate' => $mdlLeadOther->lsor_low_estimate,
                'lsor_best_estimate' => $mdlLeadOther->lsor_best_estimate,
                'lsor_high_estimate' => $mdlLeadOther->lsor_high_estimate,
                'lsor_remark' => $mdlLeadOther->lsor_remark,
                'gcf_is_sr' => $mdlGcf->gcf_is_sr,
                'gcf_sr_age_system' => $mdlGcf->gcf_sr_age_system,
                'gcf_sr_age_serie' => $mdlGcf->gcf_sr_age_serie,
                'gcf_sr_formation_name_pre' => $mdlGcf->gcf_sr_formation_name_pre,
                'gcf_sr_formation_name_post' => $mdlGcf->gcf_sr_formation_name_post,
                'gcf_sr_kerogen' => $mdlGcf->gcf_sr_kerogen,
                'gcf_sr_toc' => $mdlGcf->gcf_sr_toc,
                'gcf_sr_hfu' => $mdlGcf->gcf_sr_hfu,
                'gcf_sr_distribution' => $mdlGcf->gcf_sr_distribution,
                'gcf_sr_continuity' => $mdlGcf->gcf_sr_continuity,
                'gcf_sr_maturity' => $mdlGcf->gcf_sr_maturity,
                'gcf_sr_otr' => $mdlGcf->gcf_sr_otr,
                'gcf_sr_remark' => $mdlGcf->gcf_sr_remark,
                'gcf_is_res' => $mdlGcf->gcf_is_res,
                'gcf_res_age_system' => $mdlGcf->gcf_res_age_system,
                'gcf_res_age_serie' => $mdlGcf->gcf_res_age_serie,
                'gcf_res_formation_name_pre' => $mdlGcf->gcf_res_formation_name_pre,
                'gcf_res_formation_name_post' => $mdlGcf->gcf_res_formation_name_post,
                'gcf_res_depos_set' => $mdlGcf->gcf_res_depos_set,
                'gcf_res_depos_env' => $mdlGcf->gcf_res_depos_env,
                'gcf_res_distribution' => $mdlGcf->gcf_res_distribution,
                'gcf_res_continuity' => $mdlGcf->gcf_res_continuity,
                'gcf_res_lithology' => $mdlGcf->gcf_res_lithology,
                'gcf_res_por_type' => $mdlGcf->gcf_res_por_type,
                'gcf_res_por_primary' => $mdlGcf->gcf_res_por_primary,
                'gcf_res_por_secondary' => $mdlGcf->gcf_res_por_secondary,
                'gcf_res_remark' => $mdlGcf->gcf_res_remark,
                'gcf_is_trap' => $mdlGcf->gcf_is_trap,
                'gcf_trap_seal_age_system' => $mdlGcf->gcf_trap_seal_age_system,
                'gcf_trap_seal_age_serie' => $mdlGcf->gcf_trap_seal_age_serie,
                'gcf_trap_formation_name_pre' => $mdlGcf->gcf_trap_formation_name_pre,
                'gcf_trap_formation_name_post' => $mdlGcf->gcf_trap_formation_name_post,
                'gcf_trap_seal_distribution' => $mdlGcf->gcf_trap_seal_distribution,
                'gcf_trap_seal_continuity' => $mdlGcf->gcf_trap_seal_continuity,
                'gcf_trap_seal_type' => $mdlGcf->gcf_trap_seal_type,
                'gcf_trap_age_system' => $mdlGcf->gcf_trap_age_system,
                'gcf_trap_age_serie' => $mdlGcf->gcf_trap_age_serie,
                'gcf_trap_geometry' => $mdlGcf->gcf_trap_geometry,
                'gcf_trap_type' => $mdlGcf->gcf_trap_type,
                'gcf_trap_closure' => $mdlGcf->gcf_trap_closure,
                'gcf_trap_remark' => $mdlGcf->gcf_trap_remark,
                'gcf_is_dyn' => $mdlGcf->gcf_is_dyn,
                'gcf_dyn_migration' => $mdlGcf->gcf_dyn_migration,
                'gcf_dyn_kitchen' => $mdlGcf->gcf_dyn_kitchen,
                'gcf_dyn_petroleum' => $mdlGcf->gcf_dyn_petroleum,
                'gcf_dyn_early_age_system' => $mdlGcf->gcf_dyn_early_age_system,
                'gcf_dyn_early_age_serie' => $mdlGcf->gcf_dyn_early_age_serie,
                'gcf_dyn_late_age_system' => $mdlGcf->gcf_dyn_late_age_system,
                'gcf_dyn_late_age_serie' => $mdlGcf->gcf_dyn_late_age_serie,
                'gcf_dyn_preservation' => $mdlGcf->gcf_dyn_preservation,
                'gcf_dyn_pathways' => $mdlGcf->gcf_dyn_pathways,
                'gcf_dyn_migration_age_system' => $mdlGcf->gcf_dyn_migration_age_system,
                'gcf_dyn_migration_age_serie' => $mdlGcf->gcf_dyn_migration_age_serie,
                'gcf_dyn_remark' => $mdlGcf->gcf_dyn_remark,
            ));
            Yii::app()->end();
        }
    }

    public function actionGetPreviousDataDrillable($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlDrillable = Drillable::model()->with(array('plays' => array('condition' => 'wk_id="' . Yii::app()->user->wk_id . '"')))->findByAttributes(array('prospect_id' => $id));
            $mdlProspect = Prospect::model()->findByPk($mdlDrillable->prospect_id);
            if (!($mdlGeological = Geological::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGeological = new Geological();
                $mdlGeological->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGeological->is_checked = 1;
            }

            if (!($mdlSeismic2d = Seismic2d::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlSeismic2d = new Seismic2d();
                $mdlSeismic2d->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlSeismic2d->is_checked = 1;
            }

            if (!($mdlGravity = Gravity::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGravity = new Gravity();
                $mdlGravity->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGravity->is_checked = 1;
            }

            if (!($mdlGeochemistry = Geochemistry::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGeochemistry = new Geochemistry();
                $mdlGeochemistry->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGeochemistry->is_checked = 1;
            }

            if (!($mdlElectromagnetic = Electromagnetic::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlElectromagnetic = new Electromagnetic();
                $mdlElectromagnetic->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlElectromagnetic->is_checked = 1;
            }

            if (!($mdlResistivity = Resistivity::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlResistivity = new Resistivity();
                $mdlResistivity->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlResistivity->is_checked = 1;
            }

            if (!($mdlOther = Other::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlOther = new Other();
                $mdlOther->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlOther->is_checked = 1;
            }

            if (!($mdlSeismic3d = Seismic3d::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlSeismic3d = new Seismic3d();
                $mdlSeismic3d->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlSeismic3d->is_checked = 1;
            }

            $mdlGcf = Gcf::model()->findByPk($mdlProspect->gcf_id);

            $bonlat = preg_split("/[,]+/", $mdlProspect->prospect_latitude);
            $bonlong = preg_split("/[,]+/", $mdlProspect->prospect_longitude);

            $mdlProspect->center_lat_degree = $bonlat[0];
            $mdlProspect->center_lat_minute = $bonlat[1];
            $mdlProspect->center_lat_second = $bonlat[2];
            $mdlProspect->center_lat_direction = $bonlat[3];

            $mdlProspect->center_long_degree = $bonlong[0];
            $mdlProspect->center_long_minute = $bonlong[1];
            $mdlProspect->center_long_second = $bonlong[2];
            $mdlProspect->center_long_direction = $bonlong[3];

            if (strpos($mdlGcf->gcf_sr_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_sr_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_sr_formation);
                $mdlGcf->gcf_sr_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_sr_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_sr_formation_name_pre = $mdlGcf->gcf_sr_formation;
            }

            if (strpos($mdlGcf->gcf_res_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_res_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_res_formation);
                $mdlGcf->gcf_res_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_res_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_res_formation_name_pre = $mdlGcf->gcf_res_formation;
            }

            if (strpos($mdlGcf->gcf_trap_seal_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_trap_seal_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_trap_seal_formation);
                $mdlGcf->gcf_trap_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_trap_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_trap_formation_name_pre = $mdlGcf->gcf_trap_seal_formation;
            }

            echo CJSON::encode(array(
                'error' => false,
                'prospect_update_from' => $mdlProspect->prospect_update_from,
                'prospect_submit_revision' => $mdlProspect->prospect_submit_revision,
                'structure_name' => $mdlProspect->structure_name,
                'play_id' => $mdlProspect->play_id,
                'prospect_clarified' => $mdlProspect->prospect_clarified,
                'prospect_processing_type' => $mdlProspect->prospect_processing_type,
                'prospect_year_seismic' => $mdlProspect->prospect_year_seismic,
                'prospect_year_late' => $mdlProspect->prospect_year_late,
                'prospect_date_initiate' => $mdlProspect->prospect_date_initiate,
                'center_lat_degree' => $mdlProspect->center_lat_degree,
                'center_lat_minute' => $mdlProspect->center_lat_minute,
                'center_lat_second' => $mdlProspect->center_lat_second,
                'center_lat_direction' => $mdlProspect->center_lat_direction,
                'center_long_degree' => $mdlProspect->center_long_degree,
                'center_long_minute' => $mdlProspect->center_long_minute,
                'center_long_second' => $mdlProspect->center_long_second,
                'center_long_direction' => $mdlProspect->center_long_direction,
                'prospect_shore' => $mdlProspect->prospect_shore,
                'prospect_terrain' => $mdlProspect->prospect_terrain,
                'prospect_near_field' => $mdlProspect->prospect_near_field,
                'prospect_near_infra_structure' => $mdlProspect->prospect_near_infra_structure,
                'dr_por_quantity' => $mdlDrillable->dr_por_quantity,
                'dr_por_p90' => $mdlDrillable->dr_por_p90,
                'dr_por_p50' => $mdlDrillable->dr_por_p50,
                'dr_por_p10' => $mdlDrillable->dr_por_p10,
                'dr_por_p90_type' => $mdlDrillable->dr_por_p90_type,
                'dr_por_p50_type' => $mdlDrillable->dr_por_p50_type,
                'dr_por_p10_type' => $mdlDrillable->dr_por_p10_type,
                'dr_por_remark' => $mdlDrillable->dr_por_remark,
                'dr_satur_quantity' => $mdlDrillable->dr_satur_quantity,
                'dr_satur_p90' => $mdlDrillable->dr_satur_p90,
                'dr_satur_p50' => $mdlDrillable->dr_satur_p50,
                'dr_satur_p10' => $mdlDrillable->dr_satur_p10,
                'dr_satur_p90_type' => $mdlDrillable->dr_satur_p90_type,
                'dr_satur_p50_type' => $mdlDrillable->dr_satur_p50_type,
                'dr_satur_p10_type' => $mdlDrillable->dr_satur_p10_type,
                'dr_satur_remark' => $mdlDrillable->dr_satur_remark,
                'sgf_is_check' => $mdlGeological->is_checked,
                'sgf_year_survey' => $mdlGeological->sgf_year_survey,
                'sgf_survey_method' => $mdlGeological->sgf_survey_method,
                'sgf_coverage_area' => $mdlGeological->sgf_coverage_area,
                'sgf_p90_area' => $mdlGeological->sgf_p90_area,
                'sgf_p90_thickness' => $mdlGeological->sgf_p90_thickness,
                'sgf_p90_net' => $mdlGeological->sgf_p90_net,
                'sgf_p50_area' => $mdlGeological->sgf_p50_area,
                'sgf_p50_thickness' => $mdlGeological->sgf_p50_thickness,
                'sgf_p50_net' => $mdlGeological->sgf_p50_net,
                'sgf_p10_area' => $mdlGeological->sgf_p10_area,
                'sgf_p10_thickness' => $mdlGeological->sgf_p10_thickness,
                'sgf_p10_net' => $mdlGeological->sgf_p10_net,
                'sgf_remark' => $mdlGeological->sgf_remark,
                's2d_is_check' => $mdlSeismic2d->is_checked,
                's2d_year_survey' => $mdlSeismic2d->s2d_year_survey,
                's2d_vintage_number' => $mdlSeismic2d->s2d_vintage_number,
                's2d_total_crossline' => $mdlSeismic2d->s2d_total_crossline,
                's2d_seismic_line' => $mdlSeismic2d->s2d_seismic_line,
                's2d_average_interval' => $mdlSeismic2d->s2d_average_interval,
                's2d_record_length_ms' => $mdlSeismic2d->s2d_record_length_ms,
                's2d_record_length_ft' => $mdlSeismic2d->s2d_record_length_ft,
                's2d_year_late_process' => $mdlSeismic2d->s2d_year_late_process,
                's2d_late_method' => $mdlSeismic2d->s2d_late_method,
                's2d_img_quality' => $mdlSeismic2d->s2d_img_quality,
                's2d_top_depth_ft' => $mdlSeismic2d->s2d_top_depth_ft,
                's2d_top_depth_ms' => $mdlSeismic2d->s2d_top_depth_ms,
                's2d_bot_depth_ft' => $mdlSeismic2d->s2d_bot_depth_ft,
                's2d_bot_depth_ms' => $mdlSeismic2d->s2d_bot_depth_ms,
                's2d_depth_spill' => $mdlSeismic2d->s2d_depth_spill,
                's2d_depth_estimate' => $mdlSeismic2d->s2d_depth_estimate,
                's2d_depth_estimate_analog' => $mdlSeismic2d->s2d_depth_estimate_analog,
                's2d_depth_estimate_spill' => $mdlSeismic2d->s2d_depth_estimate_spill,
                's2d_depth_low' => $mdlSeismic2d->s2d_depth_low,
                's2d_depth_low_analog' => $mdlSeismic2d->s2d_depth_low_analog,
                's2d_depth_low_spill' => $mdlSeismic2d->s2d_depth_low_spill,
                's2d_formation_thickness' => $mdlSeismic2d->s2d_formation_thickness,
                's2d_gross_thickness' => $mdlSeismic2d->s2d_gross_thickness,
                's2d_avail_pay' => $mdlSeismic2d->s2d_avail_pay,
                's2d_net_p90_thickness' => $mdlSeismic2d->s2d_net_p90_thickness,
                's2d_net_p90_vsh' => $mdlSeismic2d->s2d_net_p90_vsh,
                's2d_net_p90_por' => $mdlSeismic2d->s2d_net_p90_por,
                's2d_net_p90_satur' => $mdlSeismic2d->s2d_net_p90_satur,
                's2d_net_p50_thickness' => $mdlSeismic2d->s2d_net_p50_thickness,
                's2d_net_p50_vsh' => $mdlSeismic2d->s2d_net_p50_vsh,
                's2d_net_p50_por' => $mdlSeismic2d->s2d_net_p50_por,
                's2d_net_p50_satur' => $mdlSeismic2d->s2d_net_p50_satur,
                's2d_net_p10_thickness' => $mdlSeismic2d->s2d_net_p10_thickness,
                's2d_net_p10_vsh' => $mdlSeismic2d->s2d_net_p10_vsh,
                's2d_net_p10_por' => $mdlSeismic2d->s2d_net_p10_por,
                's2d_net_p10_satur' => $mdlSeismic2d->s2d_net_p10_satur,
                's2d_vol_p90_area' => $mdlSeismic2d->s2d_vol_p90_area,
                's2d_vol_p50_area' => $mdlSeismic2d->s2d_vol_p50_area,
                's2d_vol_p10_area' => $mdlSeismic2d->s2d_vol_p10_area,
                's2d_remark' => $mdlSeismic2d->s2d_remark,
                'sgv_is_check' => $mdlGravity->is_checked,
                'sgv_year_survey' => $mdlGravity->sgv_year_survey,
                'sgv_survey_method' => $mdlGravity->sgv_survey_method,
                'sgv_coverage_area' => $mdlGravity->sgv_coverage_area,
                'sgv_depth_range' => $mdlGravity->sgv_depth_range,
                'sgv_spacing_interval' => $mdlGravity->sgv_spacing_interval,
                'sgv_depth_spill' => $mdlGravity->sgv_depth_spill,
                'sgv_depth_estimate' => $mdlGravity->sgv_depth_estimate,
                'sgv_depth_estimate_analog' => $mdlGravity->sgv_depth_estimate_analog,
                'sgv_depth_estimate_spill' => $mdlGravity->sgv_depth_estimate_spill,
                'sgv_depth_low' => $mdlGravity->sgv_depth_low,
                'sgv_depth_low_analog' => $mdlGravity->sgv_depth_low_analog,
                'sgv_depth_low_spill' => $mdlGravity->sgv_depth_low_spill,
                'sgv_res_thickness' => $mdlGravity->sgv_res_thickness,
                'sgv_res_according' => $mdlGravity->sgv_res_according,
                'sgv_res_top_depth' => $mdlGravity->sgv_res_top_depth,
                'sgv_res_bot_depth' => $mdlGravity->sgv_res_bot_depth,
                'sgv_gross_thickness' => $mdlGravity->sgv_gross_thickness,
                'sgv_gross_according' => $mdlGravity->sgv_gross_according,
                'sgv_gross_well' => $mdlGravity->sgv_gross_well,
                'sgv_net_gross' => $mdlGravity->sgv_net_gross,
                'sgv_vol_p90_area' => $mdlGravity->sgv_vol_p90_area,
                'sgv_vol_p50_area' => $mdlGravity->sgv_vol_p50_area,
                'sgv_vol_p10_area' => $mdlGravity->sgv_vol_p10_area,
                'sgv_remark' => $mdlGravity->sgv_remark,
                'sgc_is_check' => $mdlGeochemistry->is_checked,
                'sgc_year_survey' => $mdlGeochemistry->sgc_year_survey,
                'sgc_sample_interval' => $mdlGeochemistry->sgc_sample_interval,
                'sgc_number_sample' => $mdlGeochemistry->sgc_number_sample,
                'sgc_number_rock' => $mdlGeochemistry->sgc_number_rock,
                'sgc_number_fluid' => $mdlGeochemistry->sgc_number_fluid,
                'sgc_avail_hc' => $mdlGeochemistry->sgc_avail_hc,
                'sgc_hc_composition' => $mdlGeochemistry->sgc_hc_composition,
                'sgc_lab_report' => $mdlGeochemistry->sgc_lab_report,
                'sgc_vol_p90_area' => $mdlGeochemistry->sgc_vol_p90_area,
                'sgc_vol_p50_area' => $mdlGeochemistry->sgc_vol_p50_area,
                'sgc_vol_p10_area' => $mdlGeochemistry->sgc_vol_p10_area,
                'sgc_remark' => $mdlGeochemistry->sgc_remark,
                'sel_is_check' => $mdlElectromagnetic->is_checked,
                'sel_year_survey' => $mdlElectromagnetic->sel_year_survey,
                'sel_survey_method' => $mdlElectromagnetic->sel_survey_method,
                'sel_coverage_area' => $mdlElectromagnetic->sel_coverage_area,
                'sel_depth_range' => $mdlElectromagnetic->sel_depth_range,
                'sel_spacing_interval' => $mdlElectromagnetic->sel_spacing_interval,
                'sel_vol_p90_area' => $mdlElectromagnetic->sel_vol_p90_area,
                'sel_vol_p50_area' => $mdlElectromagnetic->sel_vol_p50_area,
                'sel_vol_p10_area' => $mdlElectromagnetic->sel_vol_p10_area,
                'sel_remark' => $mdlElectromagnetic->sel_remark,
                'rst_is_check' => $mdlElectromagnetic->is_checked,
                'rst_year_survey' => $mdlResistivity->rst_year_survey,
                'rst_survey_method' => $mdlResistivity->rst_survey_method,
                'rst_coverage_area' => $mdlResistivity->rst_coverage_area,
                'rst_depth_range' => $mdlResistivity->rst_depth_range,
                'rst_spacing_interval' => $mdlResistivity->rst_spacing_interval,
                'rst_vol_p90_area' => $mdlResistivity->rst_vol_p90_area,
                'rst_vol_p50_area' => $mdlResistivity->rst_vol_p50_area,
                'rst_vol_p10_area' => $mdlResistivity->rst_vol_p10_area,
                'rst_remark' => $mdlResistivity->rst_remark,
                'sor_is_check' => $mdlOther->is_checked,
                'sor_year_survey' => $mdlOther->sor_year_survey,
                'sor_vol_p90_area' => $mdlOther->sor_vol_p90_area,
                'sor_vol_p50_area' => $mdlOther->sor_vol_p50_area,
                'sor_vol_p10_area' => $mdlOther->sor_vol_p10_area,
                'sor_remark' => $mdlOther->sor_remark,
                's3d_is_check' => $mdlSeismic3d->is_checked,
                's3d_year_survey' => $mdlSeismic3d->s3d_year_survey,
                's3d_vintage_number' => $mdlSeismic3d->s3d_vintage_number,
                's3d_bin_size' => $mdlSeismic3d->s3d_bin_size,
                's3d_coverage_area' => $mdlSeismic3d->s3d_coverage_area,
                's3d_frequency' => $mdlSeismic3d->s3d_frequency,
                's3d_frequency_lateral' => $mdlSeismic3d->s3d_frequency_lateral,
                's3d_frequency_vertical' => $mdlSeismic3d->s3d_frequency_vertical,
                's3d_year_late_process' => $mdlSeismic3d->s3d_year_late_process,
                's3d_late_method' => $mdlSeismic3d->s3d_late_method,
                's3d_img_quality' => $mdlSeismic3d->s3d_img_quality,
                's3d_top_depth_ft' => $mdlSeismic3d->s3d_top_depth_ft,
                's3d_top_depth_ms' => $mdlSeismic3d->s3d_top_depth_ms,
                's3d_bot_depth_ft' => $mdlSeismic3d->s3d_bot_depth_ft,
                's3d_top_depth_ms' => $mdlSeismic3d->s3d_top_depth_ms,
                's3d_depth_spill' => $mdlSeismic3d->s3d_depth_spill,
                's3d_ability_offset' => $mdlSeismic3d->s3d_ability_offset,
                's3d_ability_rock' => $mdlSeismic3d->s3d_ability_rock,
                's3d_ability_fluid' => $mdlSeismic3d->s3d_ability_fluid,
                's3d_depth_estimate' => $mdlSeismic3d->s3d_depth_estimate,
                's3d_depth_estimate_analog' => $mdlSeismic3d->s3d_depth_estimate_analog,
                's3d_depth_estimate_spill' => $mdlSeismic3d->s3d_depth_estimate_spill,
                's3d_depth_low' => $mdlSeismic3d->s3d_depth_low,
                's3d_depth_low_analog' => $mdlSeismic3d->s3d_depth_low_analog,
                's3d_depth_low_spill' => $mdlSeismic3d->s3d_depth_low_spill,
                's3d_formation_thickness' => $mdlSeismic3d->s3d_formation_thickness,
                's3d_gross_thickness' => $mdlSeismic3d->s3d_gross_thickness,
                's3d_avail_pay' => $mdlSeismic3d->s3d_avail_pay,
                's3d_net_p90_thickness' => $mdlSeismic3d->s3d_net_p90_thickness,
                's3d_net_p90_vsh' => $mdlSeismic3d->s3d_net_p90_vsh,
                's3d_net_p90_por' => $mdlSeismic3d->s3d_net_p90_por,
                's3d_net_p90_satur' => $mdlSeismic3d->s3d_net_p90_satur,
                's3d_net_p50_thickness' => $mdlSeismic3d->s3d_net_p50_thickness,
                's3d_net_p50_vsh' => $mdlSeismic3d->s3d_net_p50_vsh,
                's3d_net_p50_por' => $mdlSeismic3d->s3d_net_p50_por,
                's3d_net_p50_satur' => $mdlSeismic3d->s3d_net_p50_satur,
                's3d_net_p10_thickness' => $mdlSeismic3d->s3d_net_p10_thickness,
                's3d_net_p10_vsh' => $mdlSeismic3d->s3d_net_p10_vsh,
                's3d_net_p10_por' => $mdlSeismic3d->s3d_net_p10_por,
                's3d_net_p10_satur' => $mdlSeismic3d->s3d_net_p10_satur,
                's3d_vol_p90_area' => $mdlSeismic3d->s3d_vol_p90_area,
                's3d_vol_p50_area' => $mdlSeismic3d->s3d_vol_p50_area,
                's3d_vol_p10_area' => $mdlSeismic3d->s3d_vol_p10_area,
                's3d_remark' => $mdlSeismic3d->s3d_remark,
                'gcf_is_sr' => $mdlGcf->gcf_is_sr,
                'gcf_sr_age_system' => $mdlGcf->gcf_sr_age_system,
                'gcf_sr_age_serie' => $mdlGcf->gcf_sr_age_serie,
                'gcf_sr_formation_name_pre' => $mdlGcf->gcf_sr_formation_name_pre,
                'gcf_sr_formation_name_post' => $mdlGcf->gcf_sr_formation_name_post,
                'gcf_sr_kerogen' => $mdlGcf->gcf_sr_kerogen,
                'gcf_sr_toc' => $mdlGcf->gcf_sr_toc,
                'gcf_sr_hfu' => $mdlGcf->gcf_sr_hfu,
                'gcf_sr_distribution' => $mdlGcf->gcf_sr_distribution,
                'gcf_sr_continuity' => $mdlGcf->gcf_sr_continuity,
                'gcf_sr_maturity' => $mdlGcf->gcf_sr_maturity,
                'gcf_sr_otr' => $mdlGcf->gcf_sr_otr,
                'gcf_sr_remark' => $mdlGcf->gcf_sr_remark,
                'gcf_is_res' => $mdlGcf->gcf_is_res,
                'gcf_res_age_system' => $mdlGcf->gcf_res_age_system,
                'gcf_res_age_serie' => $mdlGcf->gcf_res_age_serie,
                'gcf_res_formation_name_pre' => $mdlGcf->gcf_res_formation_name_pre,
                'gcf_res_formation_name_post' => $mdlGcf->gcf_res_formation_name_post,
                'gcf_res_depos_set' => $mdlGcf->gcf_res_depos_set,
                'gcf_res_depos_env' => $mdlGcf->gcf_res_depos_env,
                'gcf_res_distribution' => $mdlGcf->gcf_res_distribution,
                'gcf_res_continuity' => $mdlGcf->gcf_res_continuity,
                'gcf_res_lithology' => $mdlGcf->gcf_res_lithology,
                'gcf_res_por_type' => $mdlGcf->gcf_res_por_type,
                'gcf_res_por_primary' => $mdlGcf->gcf_res_por_primary,
                'gcf_res_por_secondary' => $mdlGcf->gcf_res_por_secondary,
                'gcf_res_remark' => $mdlGcf->gcf_res_remark,
                'gcf_is_trap' => $mdlGcf->gcf_is_trap,
                'gcf_trap_seal_age_system' => $mdlGcf->gcf_trap_seal_age_system,
                'gcf_trap_seal_age_serie' => $mdlGcf->gcf_trap_seal_age_serie,
                'gcf_trap_formation_name_pre' => $mdlGcf->gcf_trap_formation_name_pre,
                'gcf_trap_formation_name_post' => $mdlGcf->gcf_trap_formation_name_post,
                'gcf_trap_seal_distribution' => $mdlGcf->gcf_trap_seal_distribution,
                'gcf_trap_seal_continuity' => $mdlGcf->gcf_trap_seal_continuity,
                'gcf_trap_seal_type' => $mdlGcf->gcf_trap_seal_type,
                'gcf_trap_age_system' => $mdlGcf->gcf_trap_age_system,
                'gcf_trap_age_serie' => $mdlGcf->gcf_trap_age_serie,
                'gcf_trap_geometry' => $mdlGcf->gcf_trap_geometry,
                'gcf_trap_type' => $mdlGcf->gcf_trap_type,
                'gcf_trap_closure' => $mdlGcf->gcf_trap_closure,
                'gcf_trap_remark' => $mdlGcf->gcf_trap_remark,
                'gcf_is_dyn' => $mdlGcf->gcf_is_dyn,
                'gcf_dyn_migration' => $mdlGcf->gcf_dyn_migration,
                'gcf_dyn_kitchen' => $mdlGcf->gcf_dyn_kitchen,
                'gcf_dyn_petroleum' => $mdlGcf->gcf_dyn_petroleum,
                'gcf_dyn_early_age_system' => $mdlGcf->gcf_dyn_early_age_system,
                'gcf_dyn_early_age_serie' => $mdlGcf->gcf_dyn_early_age_serie,
                'gcf_dyn_late_age_system' => $mdlGcf->gcf_dyn_late_age_system,
                'gcf_dyn_late_age_serie' => $mdlGcf->gcf_dyn_late_age_serie,
                'gcf_dyn_preservation' => $mdlGcf->gcf_dyn_preservation,
                'gcf_dyn_pathways' => $mdlGcf->gcf_dyn_pathways,
                'gcf_dyn_migration_age_system' => $mdlGcf->gcf_dyn_migration_age_system,
                'gcf_dyn_migration_age_serie' => $mdlGcf->gcf_dyn_migration_age_serie,
                'gcf_dyn_remark' => $mdlGcf->gcf_dyn_remark,
            ));
        }
    }

    public function actionGetPreviousDataPostdrill($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlPostdrill = Postdrill::model()->with(array('plays' => array('condition' => 'wk_id="' . Yii::app()->user->wk_id . '"')))->findByAttributes(array('prospect_id' => $id));
            $mdlProspect = Prospect::model()->findByPk($mdlPostdrill->prospect_id);
            $mdlWell = Well::model()->findByAttributes(array('prospect_id' => $id));
            if (!($mdlGeological = Geological::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGeological = new Geological();
                $mdlGeological->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGeological->is_checked = 1;
            }

            if (!($mdlSeismic2d = Seismic2d::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlSeismic2d = new Seismic2d();
                $mdlSeismic2d->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlSeismic2d->is_checked = 1;
            }

            if (!($mdlGravity = Gravity::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGravity = new Gravity();
                $mdlGravity->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGravity->is_checked = 1;
            }

            if (!($mdlGeochemistry = Geochemistry::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGeochemistry = new Geochemistry();
                $mdlGeochemistry->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGeochemistry->is_checked = 1;
            }

            if (!($mdlElectromagnetic = Electromagnetic::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlElectromagnetic = new Electromagnetic();
                $mdlElectromagnetic->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlElectromagnetic->is_checked = 1;
            }

            if (!($mdlResistivity = Resistivity::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlResistivity = new Resistivity();
                $mdlResistivity->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlResistivity->is_checked = 1;
            }

            if (!($mdlOther = Other::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlOther = new Other();
                $mdlOther->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlOther->is_checked = 1;
            }

            if (!($mdlSeismic3d = Seismic3d::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlSeismic3d = new Seismic3d();
                $mdlSeismic3d->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlSeismic3d->is_checked = 1;
            }
            $mdlGcf = Gcf::model()->findByPk($mdlProspect->gcf_id);

            $bonlat = preg_split("/[,]+/", $mdlProspect->prospect_latitude);
            $bonlong = preg_split("/[,]+/", $mdlProspect->prospect_longitude);

            $mdlProspect->center_lat_degree = $bonlat[0];
            $mdlProspect->center_lat_minute = $bonlat[1];
            $mdlProspect->center_lat_second = $bonlat[2];
            $mdlProspect->center_lat_direction = $bonlat[3];

            $mdlProspect->center_long_degree = $bonlong[0];
            $mdlProspect->center_long_minute = $bonlong[1];
            $mdlProspect->center_long_second = $bonlong[2];
            $mdlProspect->center_long_direction = $bonlong[3];

            if (strpos($mdlGcf->gcf_sr_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_sr_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_sr_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_sr_formation);
                $mdlGcf->gcf_sr_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_sr_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_sr_formation_name_pre = $mdlGcf->gcf_sr_formation;
            }

            if (strpos($mdlGcf->gcf_res_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_res_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_res_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_res_formation);
                $mdlGcf->gcf_res_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_res_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_res_formation_name_pre = $mdlGcf->gcf_res_formation;
            }

            if (strpos($mdlGcf->gcf_trap_seal_formation, 'Lower') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Middle') !== false ||
                strpos($mdlGcf->gcf_trap_seal_formation, 'Upper') !== false) {
                $words = str_word_count($mdlGcf->gcf_trap_seal_formation, 1);
                $lastWord = array_pop($words);
                $textWithoutLastWord = preg_replace('~\s+\S+$~', '', $mdlGcf->gcf_trap_seal_formation);
                $mdlGcf->gcf_trap_formation_name_pre = $textWithoutLastWord;
                $mdlGcf->gcf_trap_formation_name_post = $lastWord;
            } else {
                $mdlGcf->gcf_trap_formation_name_pre = $mdlGcf->gcf_trap_seal_formation;
            }

            echo CJSON::encode(array(
                'error' => false,
                'prospect_update_from' => $mdlProspect->prospect_update_from,
                'prospect_submit_revision' => $mdlProspect->prospect_submit_revision,
                'structure_name' => $mdlProspect->structure_name,
                'play_id' => $mdlProspect->play_id,
                'prospect_clarified' => $mdlProspect->prospect_clarified,
                'prospect_year_seismic' => $mdlProspect->prospect_year_seismic,
                'prospect_processing_type' => $mdlProspect->prospect_processing_type,
                'prospect_year_late' => $mdlProspect->prospect_year_late,
                'pd_name_revise' => $mdlPostdrill->pd_name_revise,
                'pd_name_revise_date' => $mdlPostdrill->pd_name_revise_date,
                'prospect_date_initiate' => $mdlProspect->prospect_date_initiate,
                'center_lat_degree' => $mdlProspect->center_lat_degree,
                'center_lat_minute' => $mdlProspect->center_lat_minute,
                'center_lat_second' => $mdlProspect->center_lat_second,
                'center_lat_direction' => $mdlProspect->center_lat_direction,
                'center_long_degree' => $mdlProspect->center_long_degree,
                'center_long_minute' => $mdlProspect->center_long_minute,
                'center_long_second' => $mdlProspect->center_long_second,
                'center_long_direction' => $mdlProspect->center_long_direction,
                'prospect_shore' => $mdlProspect->prospect_shore,
                'prospect_terrain' => $mdlProspect->prospect_terrain,
                'prospect_near_field' => $mdlProspect->prospect_near_field,
                'prospect_near_infra_structure' => $mdlProspect->prospect_near_infra_structure,
                'wl_id' => $mdlWell->wl_id,
                'sgf_is_check' => $mdlGeological->is_checked,
                'sgf_year_survey' => $mdlGeological->sgf_year_survey,
                'sgf_survey_method' => $mdlGeological->sgf_survey_method,
                'sgf_coverage_area' => $mdlGeological->sgf_coverage_area,
                'sgf_p90_area' => $mdlGeological->sgf_p90_area,
                'sgf_p90_thickness' => $mdlGeological->sgf_p90_thickness,
                'sgf_p90_net' => $mdlGeological->sgf_p90_net,
                'sgf_p50_area' => $mdlGeological->sgf_p50_area,
                'sgf_p50_thickness' => $mdlGeological->sgf_p50_thickness,
                'sgf_p50_net' => $mdlGeological->sgf_p50_net,
                'sgf_p10_area' => $mdlGeological->sgf_p10_area,
                'sgf_p10_thickness' => $mdlGeological->sgf_p10_thickness,
                'sgf_p10_net' => $mdlGeological->sgf_p10_net,
                'sgf_remark' => $mdlGeological->sgf_remark,
                's2d_is_check' => $mdlSeismic2d->is_checked,
                's2d_year_survey' => $mdlSeismic2d->s2d_year_survey,
                's2d_vintage_number' => $mdlSeismic2d->s2d_vintage_number,
                's2d_total_crossline' => $mdlSeismic2d->s2d_total_crossline,
                's2d_seismic_line' => $mdlSeismic2d->s2d_seismic_line,
                's2d_average_interval' => $mdlSeismic2d->s2d_average_interval,
                's2d_record_length_ms' => $mdlSeismic2d->s2d_record_length_ms,
                's2d_record_length_ft' => $mdlSeismic2d->s2d_record_length_ft,
                's2d_year_late_process' => $mdlSeismic2d->s2d_year_late_process,
                's2d_late_method' => $mdlSeismic2d->s2d_late_method,
                's2d_img_quality' => $mdlSeismic2d->s2d_img_quality,
                's2d_top_depth_ft' => $mdlSeismic2d->s2d_top_depth_ft,
                's2d_top_depth_ms' => $mdlSeismic2d->s2d_top_depth_ms,
                's2d_bot_depth_ft' => $mdlSeismic2d->s2d_bot_depth_ft,
                's2d_bot_depth_ms' => $mdlSeismic2d->s2d_bot_depth_ms,
                's2d_depth_spill' => $mdlSeismic2d->s2d_depth_spill,
                's2d_depth_estimate' => $mdlSeismic2d->s2d_depth_estimate,
                's2d_depth_estimate_analog' => $mdlSeismic2d->s2d_depth_estimate_analog,
                's2d_depth_estimate_spill' => $mdlSeismic2d->s2d_depth_estimate_spill,
                's2d_depth_low' => $mdlSeismic2d->s2d_depth_low,
                's2d_depth_low_analog' => $mdlSeismic2d->s2d_depth_low_analog,
                's2d_depth_low_spill' => $mdlSeismic2d->s2d_depth_low_spill,
                's2d_formation_thickness' => $mdlSeismic2d->s2d_formation_thickness,
                's2d_gross_thickness' => $mdlSeismic2d->s2d_gross_thickness,
                's2d_avail_pay' => $mdlSeismic2d->s2d_avail_pay,
                's2d_net_p90_thickness' => $mdlSeismic2d->s2d_net_p90_thickness,
                's2d_net_p90_vsh' => $mdlSeismic2d->s2d_net_p90_vsh,
                's2d_net_p90_por' => $mdlSeismic2d->s2d_net_p90_por,
                's2d_net_p90_satur' => $mdlSeismic2d->s2d_net_p90_satur,
                's2d_net_p50_thickness' => $mdlSeismic2d->s2d_net_p50_thickness,
                's2d_net_p50_vsh' => $mdlSeismic2d->s2d_net_p50_vsh,
                's2d_net_p50_por' => $mdlSeismic2d->s2d_net_p50_por,
                's2d_net_p50_satur' => $mdlSeismic2d->s2d_net_p50_satur,
                's2d_net_p10_thickness' => $mdlSeismic2d->s2d_net_p10_thickness,
                's2d_net_p10_vsh' => $mdlSeismic2d->s2d_net_p10_vsh,
                's2d_net_p10_por' => $mdlSeismic2d->s2d_net_p10_por,
                's2d_net_p10_satur' => $mdlSeismic2d->s2d_net_p10_satur,
                's2d_vol_p90_area' => $mdlSeismic2d->s2d_vol_p90_area,
                's2d_vol_p50_area' => $mdlSeismic2d->s2d_vol_p50_area,
                's2d_vol_p10_area' => $mdlSeismic2d->s2d_vol_p10_area,
                's2d_remark' => $mdlSeismic2d->s2d_remark,
                'sgv_is_check' => $mdlGravity->is_checked,
                'sgv_year_survey' => $mdlGravity->sgv_year_survey,
                'sgv_survey_method' => $mdlGravity->sgv_survey_method,
                'sgv_coverage_area' => $mdlGravity->sgv_coverage_area,
                'sgv_depth_range' => $mdlGravity->sgv_depth_range,
                'sgv_spacing_interval' => $mdlGravity->sgv_spacing_interval,
                'sgv_depth_spill' => $mdlGravity->sgv_depth_spill,
                'sgv_depth_estimate' => $mdlGravity->sgv_depth_estimate,
                'sgv_depth_estimate_analog' => $mdlGravity->sgv_depth_estimate_analog,
                'sgv_depth_estimate_spill' => $mdlGravity->sgv_depth_estimate_spill,
                'sgv_depth_low' => $mdlGravity->sgv_depth_low,
                'sgv_depth_low_analog' => $mdlGravity->sgv_depth_low_analog,
                'sgv_depth_low_spill' => $mdlGravity->sgv_depth_low_spill,
                'sgv_res_thickness' => $mdlGravity->sgv_res_thickness,
                'sgv_res_according' => $mdlGravity->sgv_res_according,
                'sgv_res_top_depth' => $mdlGravity->sgv_res_top_depth,
                'sgv_res_bot_depth' => $mdlGravity->sgv_res_bot_depth,
                'sgv_gross_thickness' => $mdlGravity->sgv_gross_thickness,
                'sgv_gross_according' => $mdlGravity->sgv_gross_according,
                'sgv_gross_well' => $mdlGravity->sgv_gross_well,
                'sgv_net_gross' => $mdlGravity->sgv_net_gross,
                'sgv_vol_p90_area' => $mdlGravity->sgv_vol_p90_area,
                'sgv_vol_p50_area' => $mdlGravity->sgv_vol_p50_area,
                'sgv_vol_p10_area' => $mdlGravity->sgv_vol_p10_area,
                'sgv_remark' => $mdlGravity->sgv_remark,
                'sgc_is_check' => $mdlGeochemistry->is_checked,
                'sgc_year_survey' => $mdlGeochemistry->sgc_year_survey,
                'sgc_sample_interval' => $mdlGeochemistry->sgc_sample_interval,
                'sgc_number_sample' => $mdlGeochemistry->sgc_number_sample,
                'sgc_number_rock' => $mdlGeochemistry->sgc_number_rock,
                'sgc_number_fluid' => $mdlGeochemistry->sgc_number_fluid,
                'sgc_avail_hc' => $mdlGeochemistry->sgc_avail_hc,
                'sgc_hc_composition' => $mdlGeochemistry->sgc_hc_composition,
                'sgc_lab_report' => $mdlGeochemistry->sgc_lab_report,
                'sgc_vol_p90_area' => $mdlGeochemistry->sgc_vol_p90_area,
                'sgc_vol_p50_area' => $mdlGeochemistry->sgc_vol_p50_area,
                'sgc_vol_p10_area' => $mdlGeochemistry->sgc_vol_p10_area,
                'sgc_remark' => $mdlGeochemistry->sgc_remark,
                'sel_is_check' => $mdlElectromagnetic->is_checked,
                'sel_year_survey' => $mdlElectromagnetic->sel_year_survey,
                'sel_survey_method' => $mdlElectromagnetic->sel_survey_method,
                'sel_coverage_area' => $mdlElectromagnetic->sel_coverage_area,
                'sel_depth_range' => $mdlElectromagnetic->sel_depth_range,
                'sel_spacing_interval' => $mdlElectromagnetic->sel_spacing_interval,
                'sel_vol_p90_area' => $mdlElectromagnetic->sel_vol_p90_area,
                'sel_vol_p50_area' => $mdlElectromagnetic->sel_vol_p50_area,
                'sel_vol_p10_area' => $mdlElectromagnetic->sel_vol_p10_area,
                'sel_remark' => $mdlElectromagnetic->sel_remark,
                'rst_is_check' => $mdlResistivity->is_checked,
                'rst_year_survey' => $mdlResistivity->rst_year_survey,
                'rst_survey_method' => $mdlResistivity->rst_survey_method,
                'rst_coverage_area' => $mdlResistivity->rst_coverage_area,
                'rst_depth_range' => $mdlResistivity->rst_depth_range,
                'rst_spacing_interval' => $mdlResistivity->rst_spacing_interval,
                'rst_vol_p90_area' => $mdlResistivity->rst_vol_p90_area,
                'rst_vol_p50_area' => $mdlResistivity->rst_vol_p50_area,
                'rst_vol_p10_area' => $mdlResistivity->rst_vol_p10_area,
                'rst_remark' => $mdlResistivity->rst_remark,
                'sor_is_check' => $mdlOther->is_checked,
                'sor_year_survey' => $mdlOther->sor_year_survey,
                'sor_vol_p90_area' => $mdlOther->sor_vol_p90_area,
                'sor_vol_p50_area' => $mdlOther->sor_vol_p50_area,
                'sor_vol_p10_area' => $mdlOther->sor_vol_p10_area,
                'sor_remark' => $mdlOther->sor_remark,
                's3d_is_check' => $mdlSeismic3d->is_checked,
                's3d_year_survey' => $mdlSeismic3d->s3d_year_survey,
                's3d_vintage_number' => $mdlSeismic3d->s3d_vintage_number,
                's3d_bin_size' => $mdlSeismic3d->s3d_bin_size,
                's3d_coverage_area' => $mdlSeismic3d->s3d_coverage_area,
                's3d_frequency' => $mdlSeismic3d->s3d_frequency,
                's3d_frequency_lateral' => $mdlSeismic3d->s3d_frequency_lateral,
                's3d_frequency_vertical' => $mdlSeismic3d->s3d_frequency_vertical,
                's3d_year_late_process' => $mdlSeismic3d->s3d_year_late_process,
                's3d_late_method' => $mdlSeismic3d->s3d_late_method,
                's3d_img_quality' => $mdlSeismic3d->s3d_img_quality,
                's3d_top_depth_ft' => $mdlSeismic3d->s3d_top_depth_ft,
                's3d_top_depth_ms' => $mdlSeismic3d->s3d_top_depth_ms,
                's3d_bot_depth_ft' => $mdlSeismic3d->s3d_bot_depth_ft,
                's3d_top_depth_ms' => $mdlSeismic3d->s3d_top_depth_ms,
                's3d_depth_spill' => $mdlSeismic3d->s3d_depth_spill,
                's3d_ability_offset' => $mdlSeismic3d->s3d_ability_offset,
                's3d_ability_rock' => $mdlSeismic3d->s3d_ability_rock,
                's3d_ability_fluid' => $mdlSeismic3d->s3d_ability_fluid,
                's3d_depth_estimate' => $mdlSeismic3d->s3d_depth_estimate,
                's3d_depth_estimate_analog' => $mdlSeismic3d->s3d_depth_estimate_analog,
                's3d_depth_estimate_spill' => $mdlSeismic3d->s3d_depth_estimate_spill,
                's3d_depth_low' => $mdlSeismic3d->s3d_depth_low,
                's3d_depth_low_analog' => $mdlSeismic3d->s3d_depth_low_analog,
                's3d_depth_low_spill' => $mdlSeismic3d->s3d_depth_low_spill,
                's3d_formation_thickness' => $mdlSeismic3d->s3d_formation_thickness,
                's3d_gross_thickness' => $mdlSeismic3d->s3d_gross_thickness,
                's3d_avail_pay' => $mdlSeismic3d->s3d_avail_pay,
                's3d_net_p90_thickness' => $mdlSeismic3d->s3d_net_p90_thickness,
                's3d_net_p90_vsh' => $mdlSeismic3d->s3d_net_p90_vsh,
                's3d_net_p90_por' => $mdlSeismic3d->s3d_net_p90_por,
                's3d_net_p90_satur' => $mdlSeismic3d->s3d_net_p90_satur,
                's3d_net_p50_thickness' => $mdlSeismic3d->s3d_net_p50_thickness,
                's3d_net_p50_vsh' => $mdlSeismic3d->s3d_net_p50_vsh,
                's3d_net_p50_por' => $mdlSeismic3d->s3d_net_p50_por,
                's3d_net_p50_satur' => $mdlSeismic3d->s3d_net_p50_satur,
                's3d_net_p10_thickness' => $mdlSeismic3d->s3d_net_p10_thickness,
                's3d_net_p10_vsh' => $mdlSeismic3d->s3d_net_p10_vsh,
                's3d_net_p10_por' => $mdlSeismic3d->s3d_net_p10_por,
                's3d_net_p10_satur' => $mdlSeismic3d->s3d_net_p10_satur,
                's3d_vol_p90_area' => $mdlSeismic3d->s3d_vol_p90_area,
                's3d_vol_p50_area' => $mdlSeismic3d->s3d_vol_p50_area,
                's3d_vol_p10_area' => $mdlSeismic3d->s3d_vol_p10_area,
                's3d_remark' => $mdlSeismic3d->s3d_remark,
                'gcf_is_sr' => $mdlGcf->gcf_is_sr,
                'gcf_sr_age_system' => $mdlGcf->gcf_sr_age_system,
                'gcf_sr_age_serie' => $mdlGcf->gcf_sr_age_serie,
                'gcf_sr_formation_name_pre' => $mdlGcf->gcf_sr_formation_name_pre,
                'gcf_sr_formation_name_post' => $mdlGcf->gcf_sr_formation_name_post,
                'gcf_sr_kerogen' => $mdlGcf->gcf_sr_kerogen,
                'gcf_sr_toc' => $mdlGcf->gcf_sr_toc,
                'gcf_sr_hfu' => $mdlGcf->gcf_sr_hfu,
                'gcf_sr_distribution' => $mdlGcf->gcf_sr_distribution,
                'gcf_sr_continuity' => $mdlGcf->gcf_sr_continuity,
                'gcf_sr_maturity' => $mdlGcf->gcf_sr_maturity,
                'gcf_sr_otr' => $mdlGcf->gcf_sr_otr,
                'gcf_sr_remark' => $mdlGcf->gcf_sr_remark,
                'gcf_is_res' => $mdlGcf->gcf_is_res,
                'gcf_res_age_system' => $mdlGcf->gcf_res_age_system,
                'gcf_res_age_serie' => $mdlGcf->gcf_res_age_serie,
                'gcf_res_formation_name_pre' => $mdlGcf->gcf_res_formation_name_pre,
                'gcf_res_formation_name_post' => $mdlGcf->gcf_res_formation_name_post,
                'gcf_res_depos_set' => $mdlGcf->gcf_res_depos_set,
                'gcf_res_depos_env' => $mdlGcf->gcf_res_depos_env,
                'gcf_res_distribution' => $mdlGcf->gcf_res_distribution,
                'gcf_res_continuity' => $mdlGcf->gcf_res_continuity,
                'gcf_res_lithology' => $mdlGcf->gcf_res_lithology,
                'gcf_res_por_type' => $mdlGcf->gcf_res_por_type,
                'gcf_res_por_primary' => $mdlGcf->gcf_res_por_primary,
                'gcf_res_por_secondary' => $mdlGcf->gcf_res_por_secondary,
                'gcf_res_remark' => $mdlGcf->gcf_res_remark,
                'gcf_is_trap' => $mdlGcf->gcf_is_trap,
                'gcf_trap_seal_age_system' => $mdlGcf->gcf_trap_seal_age_system,
                'gcf_trap_seal_age_serie' => $mdlGcf->gcf_trap_seal_age_serie,
                'gcf_trap_formation_name_pre' => $mdlGcf->gcf_trap_formation_name_pre,
                'gcf_trap_formation_name_post' => $mdlGcf->gcf_trap_formation_name_post,
                'gcf_trap_seal_distribution' => $mdlGcf->gcf_trap_seal_distribution,
                'gcf_trap_seal_continuity' => $mdlGcf->gcf_trap_seal_continuity,
                'gcf_trap_seal_type' => $mdlGcf->gcf_trap_seal_type,
                'gcf_trap_age_system' => $mdlGcf->gcf_trap_age_system,
                'gcf_trap_age_serie' => $mdlGcf->gcf_trap_age_serie,
                'gcf_trap_geometry' => $mdlGcf->gcf_trap_geometry,
                'gcf_trap_type' => $mdlGcf->gcf_trap_type,
                'gcf_trap_closure' => $mdlGcf->gcf_trap_closure,
                'gcf_trap_remark' => $mdlGcf->gcf_trap_remark,
                'gcf_is_dyn' => $mdlGcf->gcf_is_dyn,
                'gcf_dyn_migration' => $mdlGcf->gcf_dyn_migration,
                'gcf_dyn_kitchen' => $mdlGcf->gcf_dyn_kitchen,
                'gcf_dyn_petroleum' => $mdlGcf->gcf_dyn_petroleum,
                'gcf_dyn_early_age_system' => $mdlGcf->gcf_dyn_early_age_system,
                'gcf_dyn_early_age_serie' => $mdlGcf->gcf_dyn_early_age_serie,
                'gcf_dyn_late_age_system' => $mdlGcf->gcf_dyn_late_age_system,
                'gcf_dyn_late_age_serie' => $mdlGcf->gcf_dyn_late_age_serie,
                'gcf_dyn_preservation' => $mdlGcf->gcf_dyn_preservation,
                'gcf_dyn_pathways' => $mdlGcf->gcf_dyn_pathways,
                'gcf_dyn_migration_age_system' => $mdlGcf->gcf_dyn_migration_age_system,
                'gcf_dyn_migration_age_serie' => $mdlGcf->gcf_dyn_migration_age_serie,
                'gcf_dyn_remark' => $mdlGcf->gcf_dyn_remark,
            ));
        }
    }

    public function actionGetPreviousDataDiscovery($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlDiscovery = Discovery::model()->with(array('plays' => array('condition' => 'wk_id="' . Yii::app()->user->wk_id . '"')))->findByAttributes(array('prospect_id' => $id));

            $mdlProspect = Prospect::model()->findByPk($mdlDiscovery->prospect_id);

            $mdlWell = Well::model()->findAllByAttributes(array('prospect_id' => $id));

            $arrayWell = array();
            $j = 0;
            for ($i = 1; $i <= count($mdlWell); $i++) {
                $arrayWell[] = $mdlWell[$j++]->wl_id;
            }

            if (!($mdlGeological = Geological::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGeological = new Geological();
                $mdlGeological->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGeological->is_checked = 1;
            }

            if (!($mdlSeismic2d = Seismic2d::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlSeismic2d = new Seismic2d();
                $mdlSeismic2d->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlSeismic2d->is_checked = 1;
            }

            if (!($mdlGravity = Gravity::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGravity = new Gravity();
                $mdlGravity->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGravity->is_checked = 1;
            }

            if (!($mdlGeochemistry = Geochemistry::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlGeochemistry = new Geochemistry();
                $mdlGeochemistry->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlGeochemistry->is_checked = 1;
            }

            if (!($mdlElectromagnetic = Electromagnetic::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlElectromagnetic = new Electromagnetic();
                $mdlElectromagnetic->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlElectromagnetic->is_checked = 1;
            }

            if (!($mdlResistivity = Resistivity::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlResistivity = new Resistivity();
                $mdlResistivity->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlResistivity->is_checked = 1;
            }

            if (!($mdlOther = Other::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlOther = new Other();
                $mdlOther->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlOther->is_checked = 1;
            }

            if (!($mdlSeismic3d = Seismic3d::model()->findByattributes(array('prospect_id' => $mdlProspect->prospect_id)))) {
                $mdlSeismic3d = new Seismic3d();
                $mdlSeismic3d->prospect_id = $mdlProspect->prospect_id;
            } else {
                $mdlSeismic3d->is_checked = 1;
            }

            $mdlGcf = Gcf::model()->findByPk($mdlProspect->gcf_id);

            //array discovery
            $arrayDiscovery = array();
            $arrayDiscovery[] = $mdlDiscovery->attributes;
            //end array discovery

            //array prospect
            $arrayProspect = array();
            $arrayProspect[] = $mdlProspect->attributes;

            if (isset($mdlProspect->prospect_latitude) & isset($mdlProspect->prospect_longitude)) {
                $bonlat = preg_split("/[,]+/", $mdlProspect->prospect_latitude);
                $bonlong = preg_split("/[,]+/", $mdlProspect->prospect_longitude);
                $arrayProspect[]['center_lat_degree'] = $bonlat[0];
                $arrayProspect[]['center_lat_minute'] = $bonlat[1];
                $arrayProspect[]['center_lat_second'] = $bonlat[2];
                $arrayProspect[]['center_lat_direction'] = $bonlat[3];
                $arrayProspect[]['center_long_degree'] = $bonlong[0];
                $arrayProspect[]['center_long_minute'] = $bonlong[1];
                $arrayProspect[]['center_long_second'] = $bonlong[2];
                $arrayProspect[]['center_long_direction'] = $bonlong[3];
            }
            $arrayProspect[]['well_name'] = $arrayWell;
            //end array prospect

            //array geological
            $arrayGeological = array();
            $arrayGeological[] = $mdlGeological->attributes;
            $arrayGeological[0]['sgf_is_check'] = $mdlGeological->is_checked;
            //end array geological

            //array seismic2d
            $arraySeismic2d = array();
            $arraySeismic2d[] = $mdlSeismic2d->attributes;
            $arraySeismic2d[0]['s2d_is_check'] = $mdlSeismic2d->is_checked;
            //end array seismic2d

            //array gravity
            $arrayGravity = array();
            $arrayGravity[] = $mdlGravity->attributes;
            $arrayGravity[0]['sgv_is_check'] = $mdlGravity->is_checked;
            //end array gravity

            //array geochemistry
            $arrayGeochemistry = array();
            $arrayGeochemistry[] = $mdlGeochemistry->attributes;
            $arrayGeochemistry[0]['sgc_is_check'] = $mdlGeochemistry->is_checked;
            //end array geochemistry

            //array electromagnetic
            $arrayElectromagnetic = array();
            $arrayElectromagnetic[] = $mdlElectromagnetic->attributes;
            $arrayElectromagnetic[0]['sel_is_check'] = $mdlElectromagnetic->is_checked;
            //end array electromagnetic

            //array resistivity
            $arrayResistivity = array();
            $arrayResistivity[] = $mdlResistivity->attributes;
            $arrayResistivity[0]['rst_is_check'] = $mdlResistivity->is_checked;
            //end array resistivity

            //array other
            $arrayOther = array();
            $arrayOther[] = $mdlOther->attributes;
            $arrayOther[0]['sor_is_check'] = $mdlOther->is_checked;
            //end array other

            //array seismic3d
            $arraySeismic3d = array();
            $arraySeismic3d[] = $mdlSeismic3d->attributes;
            $arraySeismic3d[0]['s3d_is_check'] = $mdlSeismic3d->is_checked;
            //end array seismic3d

            //array gcf
            $arrayGcf = array();
            $arrayGcf[] = $mdlGcf->attributes;

            //end array gcf

            $singleArrayDiscovery = $this->array_flatten($arrayDiscovery, 'one');
            $singleArrayProspect = $this->array_flatten($arrayProspect, 'one');
            $singleArrayGeological = $this->array_flatten($arrayGeological, 'one');
            $singleArraySeismic2d = $this->array_flatten($arraySeismic2d, 'one');
            $singleArrayGravity = $this->array_flatten($arrayGravity, 'one');
            $singleArrayGeochemistry = $this->array_flatten($arrayGeochemistry, 'one');
            $singleArrayElectromagnetic = $this->array_flatten($arrayElectromagnetic, 'one');
            $singleArrayResistivity = $this->array_flatten($arrayResistivity, 'one');
            $singleArrayOther = $this->array_flatten($arrayOther, 'one');
            $singleArraySeismic3d = $this->array_flatten($arraySeismic3d, 'one');
            $singleArrayGcf = $this->array_flatten($arrayGcf, 'one');

            $array_merge = array_merge($singleArrayDiscovery, $singleArrayProspect, $singleArrayGeological, $singleArraySeismic2d, $singleArrayGravity, $singleArrayGeochemistry, $singleArrayElectromagnetic, $singleArrayResistivity, $singleArrayOther, $singleArraySeismic3d, $singleArrayGcf);
            echo CJSON::encode($array_merge);

        }
    }

    public function array_flatten($array, $name) {

        $final_array = array();
        $i = 1;
        $j = 1;

        switch ($name) {
        case 'one':
            foreach ($array as $key => $val) {
                foreach ($val as $key2 => $val2) {
                    $final_array[$key2] = $val2;
                }
            }
            return $final_array;
            break;

        case 'well':
            foreach ($array as $key => $val) {
                foreach ($val as $key2 => $val2) {
                    $final_array[$key2 . $j] = $val2;
                }
                $j++;
            }
            return $final_array;
            break;

        case 'wellzone':
            foreach ($array as $key => $val) {

                if ($j <= 5) {

                } else {
                    $i++;
                    $j = 1;
                }
                foreach ($val as $key2 => $val2) {
                    $final_array[$key2 . $i . $j] = $val2;
                }
                $j++;
            }
            return $final_array;
            break;
        }
    }

    public function actionFormationName() {
        if (Yii::app()->request->isAjaxRequest) {
            $dataFormations = Formation::model()->findAll();
            $hasil = array();
            foreach ($dataFormations as $dataFormation) {
                $hasil[] = array(
                    'id' => $dataFormation->formation_name,
                    'text' => $dataFormation->formation_name,
                );
            }

            echo CJSON::encode(array('formation' => $hasil));
            Yii::app()->end();
        }
    }

    public function actionAddFormationName() {
        $mdlFormation = new Formation();
        $mdlFormation->formation_name = $_POST['name'];
        $mdlFormation->save(false);
    }

    public function actionBasin() {
        if (Yii::app()->request->isAjaxRequest) {
            $dataBasins = Basin::model()->findAll(array('order' => 'basin_name'));
            $hasil = array();
            foreach ($dataBasins as $dataBasin) {
                $hasil[] = array(
                    'id' => $dataBasin->basin_id,
                    'text' => $dataBasin->basin_name,
                );
            }

            echo CJSON::encode(array('basin' => $hasil));
            Yii::app()->end();
        }
    }

    public function actionProvince() {
        if (Yii::app()->request->isAjaxRequest) {
            $dataProvinces = Province::model()->findAll(array('order' => 'province_name'));
            $hasil = array();
            foreach ($dataProvinces as $dataProvince) {
                $hasil[] = array(
                    'id' => $dataProvince->province_id,
                    'text' => $dataProvince->province_name,
                );
            }

            echo CJSON::encode(array('province' => $hasil));
            Yii::app()->end();
        }
    }

    public function actionContact() {
        $this->render('contact');
    }

    public function actionPrintdata() {
        $p = $this->dataPrint;
        $de = $p->printCheckErr(Yii::app()->user->wk_id); // TEST THIS WITH NEW WORKING AREA

        if ($de == 'complete') {
            $adm = $p->printGetAdm();

            if (isset($_POST['print-pdf'])) {
                to_pdf($_SESSION['pdfdata'], $adm['wk_name']);
                // $ks = str_replace(' ', '_', $adm['wk_name']);
                // $jp = dirname(__FILE__) . '/../../assets/printpdf/';
                // $printp = $jp . 'pjs.exe ' . $jp . 'r.js ' . $jp . $ks . '.html '
                //     . $jp . $ks . '.pdf A4';
                // file_put_contents('assets/printpdf/' . $ks . '.html', $_SESSION['pdfdata']);
                // exec($printp . ' 2>&1', $output);
                // unlink('assets/printpdf/' . $ks . '.html');

                // $fpdf = 'assets/printpdf/' . $ks . '.pdf';
                // if (file_exists($fpdf)) {
                //     header('Content-Description: File Transfer');
                //     header('Content-Type: application/octet-stream');
                //     header('Content-Disposition: attachment; filename=' . basename($fpdf));
                //     header('Expires: 0');
                //     header('Cache-Control: must-revalidate');
                //     header('Pragma: public');
                //     header('Content-Length: ' . filesize($fpdf));
                //     readfile($fpdf);
                //     exit;
                // }
            }

            $s = array();
            $s['wk_id'] = $adm['wk_id'];
            $s['wk_name'] = $adm['wk_name'];
            $s['kkks_name'] = $adm['kkks_name'];
            $s['data_avail'] = $p->printCheckData($adm['wk_id']);
            $s['a1'] = $p->printA1($adm['wk_id']);
            $s['a2'] = $p->printA2($adm['wk_id']);
            $s['a3']['play'] = $p->printA3Play($adm['wk_id']);
            if ($s['data_avail']['lead'] == 1) {
                $s['a3']['lead'] = $p->printA3Lead($adm['wk_id']);
            }
            if ($s['data_avail']['drillable'] == 1) {
                $s['a3']['drillable'] = $p->printA3Drillable($adm['wk_id']);
            }
            if ($s['data_avail']['postdrill'] == 1) {
                $s['a3']['postdrill'] = $p->printA3postdrill($adm['wk_id']);
            }
            if ($s['data_avail']['discovery'] == 1) {
                $s['a3']['discovery'] = $p->printA3discovery($adm['wk_id']);
            }

            $this->render('printdata', array('s' => $s));
        } else {
            $this->render('printdata', array('s' => $de));
        }
    }

    public function actionManualformat() {
        $this->render('manualformat');
    }

    public function actionVerifikasi() {
        if (Yii::app()->request->isAjaxRequest) {
            $playCriteria = new CDbCriteria;
            $playCriteria->select = 'play_id, play_submit_date';
            $playCriteria->condition = 'play_verifikasi = 0';
            $playCriteria->compare('DATE_FORMAT(play_submit_date, "%Y")', date("Y"), true);
            $dataPlay = Play::model()->updateAll(array('play_verifikasi' => 1), $playCriteria);

            $leadCriteria = new CDbCriteria;
            $leadCriteria->select = 'lead_id, lead_submit_date';
            $leadCriteria->condition = 'lead_verifikasi = 0';
            $leadCriteria->compare('DATE_FORMAT(lead_submit_date, "%Y")', date("Y"), true);
            $dataLead = Lead::model()->updateAll(array('lead_verifikasi' => 1), $leadCriteria);

            $prospectCriteria = new CDbCriteria;
            $prospectCriteria->select = 'prospect_id, prospect_submit_date';
            $prospectCriteria->condition = 'prospect_verifikasi = 0';
            $prospectCriteria->compare('DATE_FORMAT(prospect_submit_date, "%Y")', date("Y"), true);
            $dataProspect = Prospect::model()->updateAll(array('prospect_verifikasi' => 1), $prospectCriteria);

            Yii::app()->end();
        }

        $this->render('verifikasi');
    }

    public function actionCreateWellInventory() {
        $mdlWellInventory = new WellInventory();

        $mdlWellInventorySearch = WellInventory::model();
        $wellInventoryCriteria = new CDbCriteria;
        $wellInventoryCriteria->with = array('wks');
        $wellInventoryCriteria->compare('t.wk_id', Yii::app()->user->wk_id);
        $wellInventoryDataProvider = new CActiveDataProvider(get_class($mdlWellInventorySearch), array('criteria' => $wellInventoryCriteria));

        if (isset($_POST['WellInventory'])) {
            $mdlWellInventory->wk_id = Yii::app()->user->wk_id;

            $mdlWellInventory->a2_latitude = $_POST['WellInventory']['center_lat_degree'] . ',' . $_POST['WellInventory']['center_lat_minute'] . ',' . $_POST['WellInventory']['center_lat_second'] . ',' . $_POST['WellInventory']['center_lat_direction'];
            $mdlWellInventory->a2_longitude = $_POST['WellInventory']['center_long_degree'] . ',' . $_POST['WellInventory']['center_long_minute'] . ',' . $_POST['WellInventory']['center_long_second'] . ',' . $_POST['WellInventory']['center_long_direction'];

            $mdlWellInventory->attributes = $_POST['WellInventory'];

            if ($mdlWellInventory->validate()) {
                if ($mdlWellInventory->save(false)) {
                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                }
            } else {
                $error2 = self::validate(array($mdlWellInventory));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('createwellinventory', array(
            'mdlWellInventory' => $mdlWellInventory,
            'wellInventoryDataProvider' => $wellInventoryDataProvider,
        ));
    }

    public function actionUpdateWellInventory($id) {
        if (!($mdlWellInventory = WellInventory::model()->findByAttributes(array('a2_id' => $id, 'wk_id' => Yii::app()->user->wk_id)))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        if (isset($mdlWellInventory->a2_latitude) & isset($mdlWellInventory->a2_longitude)) {
            $bonlat = preg_split("/[,]+/", $mdlWellInventory->a2_latitude);
            $bonlong = preg_split("/[,]+/", $mdlWellInventory->a2_longitude);

            $mdlWellInventory->center_lat_degree = $bonlat[0];
            $mdlWellInventory->center_lat_minute = $bonlat[1];
            $mdlWellInventory->center_lat_second = $bonlat[2];
            $mdlWellInventory->center_lat_direction = $bonlat[3];

            $mdlWellInventory->center_long_degree = $bonlong[0];
            $mdlWellInventory->center_long_minute = $bonlong[1];
            $mdlWellInventory->center_long_second = $bonlong[2];
            $mdlWellInventory->center_long_direction = $bonlong[3];
        }

        if (isset($_POST['WellInventory'])) {
            $mdlWellInventory->attributes = $_POST['WellInventory'];

            if (isset($_POST['WellInventory']['center_lat_degree']) & isset($_POST['WellInventory']['center_lat_minute']) & isset($_POST['WellInventory']['center_lat_second']) & isset($_POST['WellInventory']['center_lat_direction'])
                 & isset($_POST['WellInventory']['center_long_degree']) & isset($_POST['WellInventory']['center_long_minute']) & isset($_POST['WellInventory']['center_long_second']) & isset($_POST['WellInventory']['center_long_direction'])) {
                $mdlWellInventory->a2_latitude = $_POST['WellInventory']['center_lat_degree'] . ',' . $_POST['WellInventory']['center_lat_minute'] . ',' . $_POST['WellInventory']['center_lat_second'] . ',' . $_POST['WellInventory']['center_lat_direction'];
                $mdlWellInventory->a2_longitude = $_POST['WellInventory']['center_long_degree'] . ',' . $_POST['WellInventory']['center_long_minute'] . ',' . $_POST['WellInventory']['center_long_second'] . ',' . $_POST['WellInventory']['center_long_direction'];
            }

            if ($mdlWellInventory->validate()) {
                $mdlWellInventory->save(false);
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlWellInventory));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatewellinventory', array(
            'mdlWellInventory' => $mdlWellInventory,
        ));
    }

    public function actionCreateProfile() {
        $mdlKkks = new Kkks();

        $mdlKkksSearch = Kkks::model();
        $criteria = new CDbCriteria;
        $criteria->with = array('KkksPsc');
        $criteria->compare('wk_id', Yii::app()->user->wk_id);
        $data = new CActiveDataProvider(get_class($mdlKkksSearch), array('criteria' => $criteria));

        $mdlPsc = Psc::model()->findByAttributes(array('wk_id' => Yii::app()->user->wk_id));

        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['type'])) {
                if ($_POST['type'] == 'add') {

                    $sql = 'select psc_id
              from adm_psc WHERE wk_id="' . Yii::app()->user->wk_id . '"';
                    $command = Yii::app()->db->createCommand($sql);
                    $dataPsc = $command->queryRow();

                    $mdlKkksFind = Kkks::model()->findByPk(array('kkks_id' => $_POST['Kkks']['kkks_id']));
                    $mdlKkksFind->kkks_interest = $_POST['Kkks']['kkks_interest'];
                    if ($mdlKkksFind->validate()) {
                        $mdlKkksFind->psc_id = $dataPsc['psc_id'];
                        $mdlKkksFind->kkks_interest = $_POST['Kkks']['kkks_interest'];
                        $mdlKkksFind->save(false);
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    } else {
                        $error2 = self::validate(array($mdlKkksFind));
                        if ($error2 != '[]') {
                            echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                        }
                        Yii::app()->end();
                    }
                } else if ($_POST['type'] == 'profile') {
                    $sql = 'select psc_id
              from adm_psc WHERE wk_id="' . Yii::app()->user->wk_id . '"';
                    $command = Yii::app()->db->createCommand($sql);
                    $dataPsc = $command->queryRow();

                    $criteria3 = new CDbCriteria;
                    $criteria3->condition = 'psc_id="' . $mdlPsc->psc_id . '"';

                    $mdlKkksUpdateIsOperator = Kkks::model()->updateAll(array('kkks_is_operator' => 0, 'psc_id' => null), $criteria3);

                    $testing = Kkks::model()->findByPk($_POST['Kkks']['kkks_is_operator']);
                    $testing->psc_id = $dataPsc['psc_id'];
                    $testing->kkks_is_operator = 1;
                    $testing->save(false);

                    $mdlPsc->psc_date_sign = $_POST['Psc']['psc_date_sign'];
                    $mdlPsc->psc_date_end = $_POST['Psc']['psc_date_end'];
                    $mdlPsc->psc_authorized_by = $_POST['Psc']['psc_authorized_by'];
                    $mdlPsc->psc_authorized_email = $_POST['Psc']['psc_authorized_email'];
                    if ($mdlPsc->validate()) {
                        $mdlPsc->save(false);
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    } else {
                        $error2 = self::validate(array($mdlPsc));
                        if ($error2 != '[]') {
                            echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                        }
                        Yii::app()->end();
                    }

                }
            }
        }

        $this->render('createprofile', array(
            'mdlKkks' => $mdlKkks,
            'mdlPsc' => $mdlPsc,
            'data' => $data,
        ));
    }

    public static function validate($models, $dynamicModel = null, $totalZone = 0, $attributes = null, $loadInput = true) {
        //copy from CActiveForm:validate
        $result = array();
        if (!is_array($models)) {
            $models = array($models);
        }

        foreach ($models as $model) {
            if ($loadInput && isset($_POST[get_class($model)])) {
                $model->attributes = $_POST[get_class($model)];
            }

            $model->validate($attributes);
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[CHtml::activeId($model, $attribute)] = $errors;
            }

        }
        //end copying

        //your own customization
        for ($count = 1; $count <= $totalZone; $count++) {
            $dynamicModel->clearErrors();

            if ($loadInput && isset($_POST[get_class($dynamicModel)][$count])) {
                $dynamicModel->attributes = $_POST[get_class($dynamicModel)][$count];
            }

            $dynamicModel->validate($attributes);

            foreach ($dynamicModel->getErrors() as $attribute => $errors) {
                $result[get_class($dynamicModel) . '_' . $count . '_' . $attribute] = $errors;
            }

        }

        return function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
    }

    public function actionUploadMaps2() {
        $mdlUploadImage = new UploadImage();

        $this->render('uploadmaps2', array(
            'mdlUploadImage' => $mdlUploadImage,
        ));
    }

    public function actionAjaxRequestUploadMaps2() {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $galleryDir = 'gallery';
            $message = array();

            if (count($_FILES) == 1) {
                foreach ($_FILES['file']['error'] as $key => $error) {
                    $model = new UploadImage();

                    if ($error == UPLOAD_ERR_OK) {
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = finfo_file($finfo, $_FILES['file']['tmp_name'][$key]);
                        $time = time();
                        $random_num = rand(00, 99);
                        $name = $time . $random_num . $_FILES['file']['name'][$key];
                        $imageTypeUnderscore = str_replace(' ', '_', $_POST['UploadImage']);

                        if (!file_exists($galleryDir . '/' . Yii::app()->user->wk_id)) {
                            mkdir($galleryDir . '/' . Yii::app()->user->wk_id, 0777);
                        }

                        if (!file_exists($galleryDir . '/' . Yii::app()->user->wk_id . '/' . $imageTypeUnderscore)) {
                            mkdir($galleryDir . '/' . Yii::app()->user->wk_id . '/' . $imageTypeUnderscore, 0777);
                        }

                        $path = $galleryDir . '/' . Yii::app()->user->wk_id . '/' . $imageTypeUnderscore;

                        try {
                            Yii::app()->image->load($_FILES['file']['tmp_name'][$key])->save($path . '/' . $name);

                            $model->wk_id = Yii::app()->user->wk_id;
                            $model->upload_type = $_FILES['file']['type'][$key];
                            $model->upload_path = $path . '/' . $name;
                            $model->upload_category = $_POST['UploadImage'];
                            $model->save();

                            //$return = CJSON::encode(array("err"=>"success", "result"=>"Successfully Uploaded"));
                            $message[$name] = "Successfully Uploaded";
                        } catch (Exception $e) {
                            move_uploaded_file($_FILES['file']['tmp_name'][$key], $path . '/' . $name);

                            $model->wk_id = Yii::app()->user->wk_id;
                            $model->upload_type = $mime;
                            $model->upload_path = $path . '/' . $name;
                            $model->upload_category = $_POST['UploadImage'];
                            $model->save();
                            $message[$name] = "Successfully Uploaded";
                        }

                    }
                }
            } else {
                $message['info'] = "Max size upload 40 MB";
            }
            $messagejson = function_exists('json_encode') ? json_encode($message) : CJSON::encode($message);
            echo json_encode(array('err' => $messagejson));
        }
    }

    public function actionLoadImage() {
        if (Yii::app()->request->isAjaxRequest) {
            if (Yii::app()->user->getState("level") === "kkks") {
                $path = "./gallery/" . Yii::app()->user->wk_id . "/";
                if (file_exists($path)) {
                    $imageView = self::createDir($path);
                    echo CJSON::encode(array('imageView' => $imageView));
                } else {
                    $imageView = '';
                    echo CJSON::encode(array('imageView' => $imageView));
                }
            }
        }
    }

    public function createDir($path) {
        $content = '';

        if ($handle = opendir($path)) {

            $content .= "<ul>";

            $queue = array();
            while (false !== ($file = readdir($handle))) {
                if (is_dir($path . $file) && $file != '.' && $file != '..') {
                    $content .= self::printSubDir($file, $path, $queue);
                } else if ($file != '.' && $file != '..') {
                    $queue[] = $file;
                }

            }

            $content .= self::printQueue($queue, $path);
            $content .= "</ul>";

            return $content;
        }
    }

    public function printQueue($queue, $path) {
        $content = '';
        foreach ($queue as $file) {
            $content .= self::printFile($file, $path);
        }

        return $content;
    }

    public function printFile($file, $path) {
        $content = "<li><a class='gambar' href=\"" . $path . $file . "\">$file</a></li>";
        return $content;
    }

    public function printSubDir($dir, $path) {
        $content = '';
        $content .= "<li><span class=\"toggle\">$dir</span>";
        $content .= self::createDir($path . $dir . "/");
        $content .= "</li>";
        return $content;
    }

    public static function imageView($total, $images) {
        $count = 0;
        $content = '';
        $image_path = 'upload_path';

        if ($total != 0) {
            while (1 != 2) {
                $content .= "<div class='row' style='margin-left: 0;'>";
                for ($i = 1; $i <= 6; $i++) {

                    $tes = $images[$count++][$image_path];
                    $content .= "<li class='span2'>";
                    $content .= "<div class='thumbnail'>";
                    $content .= "<div class='thumbimg'>";
                    $content .= "<a class='gambar' href='$tes' rel='tooltip'>";
                    $content .= "<img src='$tes'>";
                    $content .= "</a>";
                    $content .= "</div>";
                    $content .= "</div>";
                    $content .= "</li>";
                    if ($count == $total) {
                        break;
                    }
                }
                $content .= "</div>";
                if ($count == $total) {
                    return $content;
                    break;
                }

            }
        } else {
            return '';
        }
    }

    public function actionCreateWell() {

        $mdlWell = new Well();
        $mdlWellZone = new Wellzone();

        $mdlWellSearch = Well::model();
        $wellCriteria = new CDbCriteria;
        $wellCriteria->with = array('wellzones');
        $wellCriteria->compare('wk_id', Yii::app()->user->wk_id);
        $wellDataProvider = new CActiveDataProvider(get_class($mdlWellSearch), array('criteria' => $wellCriteria));

        if (isset($_POST['Well'], $_POST['Wellzone'])) {
            if ($_POST['Well']['prospect_type'] == 'Discovery') {
                $mdlWell->scenario = 'wellzone';
                $mdlWellZone->scenario = 'zone';
            }

            $mdlWell->wk_id = Yii::app()->user->wk_id;
            $mdlWell->attributes = $_POST['Well'];

            $mdlWell->wl_latitude = $_POST['Well']['lat_degree'] . ',' . $_POST['Well']['lat_minute'] . ',' . $_POST['Well']['lat_second'] . ',' . $_POST['Well']['lat_direction'];
            $mdlWell->wl_longitude = $_POST['Well']['long_degree'] . ',' . $_POST['Well']['long_minute'] . ',' . $_POST['Well']['long_second'] . ',' . $_POST['Well']['long_direction'];

            $wl_electro_list = '';
            for ($j = 1; $j <= 16; $j++) {
                $wl_electro_list .= $_POST['Well']["wl_electro_list$j"] . ',';
            }

            $wl_electro_list = substr($wl_electro_list, 0, -1);
            $mdlWell->wl_electro_list = $wl_electro_list;

            $mdlWell->wl_submit_date = new CDbExpression('NOW()');

            $mdlWell->wl_year = $this->year->rps_year;

            if ($_POST['Well']['prospect_type'] == 'Postdrill') {
                $mdlWellZone->attributes = $_POST['Wellzone'];

                if ($mdlWell->validate() & $mdlWellZone->validate()) {
                    if ($mdlWell->save(false)) {
                        $mdlWellZone->wl_id = $mdlWell->wl_id;
                        $mdlWellZone->save(false);
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                } else {
                    $error2 = self::validate(array($mdlWell, $mdlWellZone));
                    if ($error2 != '[]') {
                        echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                    }
                    Yii::app()->end();
                }
            } else {

                $model = array();

                $valid = $mdlWell->validate();

                array_push($model, $mdlWell);

                for ($countWell = 1; $countWell <= $_POST['Well']['wl_total_zone']; $countWell++) {
                    $mdlNewWellZone = new Wellzone();
                    $mdlNewWellZone->scenario = 'zone';
                    $mdlNewWellZone->attributes = $_POST['Wellzone'][$countWell];
                    $valid = $mdlNewWellZone->validate() && $valid;
                    array_push($model, $mdlNewWellZone);
                }

                $error = array();
                foreach ($model as $mdl) {
                    array_push($error, $mdl);
                }

                if ($valid) {
                    if ($mdlWell->save(false)) {
                        for ($j = 1; $j <= $_POST['Well']['wl_total_zone']; $j++) {
                            $mdlWellZone->zone_id = null;
                            $mdlWellZone->wl_id = $mdlWell->wl_id;
                            $mdlWellZone->attributes = $_POST['Wellzone'][$j];

                            $mdlWellZone->isNewRecord = true;
                            $mdlWellZone->save(false);
                        }

                    }
                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                } else {
                    $error2 = self::validate(array($mdlWell), $mdlWellZone, $_POST['Well']['wl_total_zone']);

                    if ($error2 != '[]') {
                        echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                    }
                    Yii::app()->end();
                }
            }

        }
        $this->render('createwell', array(
            'mdlWell' => $mdlWell,
            'wellDataProvider' => $wellDataProvider,
        ));
    }

    public function actionAjaxRequestWell() {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlWell = new Well();
            $mdlWellZone = new Wellzone();
            if ($_POST['prospect_type'] == 'Postdrill') {
                echo CJSON::encode(array(
                    'div' => $this->renderPartial('_wellpostdrill', array(
                        'mdlWell' => $mdlWell,
                        'mdlWellZone' => $mdlWellZone,
                    ), true, true),
                ));
                exit;
            } else {
                echo CJSON::encode(array(
                    'div' => $this->renderPartial('_welldiscovery', array(
                        'mdlWell' => $mdlWell,
                        'mdlWellZone' => $mdlWellZone,
                    ), true, true),
                ));
                exit;
            }

        }
    }

    public function actionUpdateWellPostdrill($id) {
        if (!($mdlPlay = Well::model()->findByAttributes(array('wl_id' => $id, 'wk_id' => Yii::app()->user->wk_id, 'prospect_type' => 'Postdrill')))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $mdlWell = Well::model()->findByPk($id);
        $mdlWellZone = Wellzone::model()->findByAttributes(array('wl_id' => $id));

        if (isset($mdlWell->wl_latitude) & isset($mdlWell->wl_longitude)) {
            $bonlat = preg_split("/[,]+/", $mdlWell->wl_latitude);
            $bonlong = preg_split("/[,]+/", $mdlWell->wl_longitude);

            $mdlWell->lat_degree = $bonlat[0];
            $mdlWell->lat_minute = $bonlat[1];
            $mdlWell->lat_second = $bonlat[2];
            $mdlWell->lat_direction = $bonlat[3];

            $mdlWell->long_degree = $bonlong[0];
            $mdlWell->long_minute = $bonlong[1];
            $mdlWell->long_second = $bonlong[2];
            $mdlWell->long_direction = $bonlong[3];
        }

        if ($mdlWell->wl_electro_list != null) {
            $string = $mdlWell->wl_electro_list;
            $array = explode(',', $string);

            $j = 0;
            for ($i = 1; $i <= 16; $i++) {
                $list = 'wl_electro_list' . $i;
                $mdlWell->$list = $array[$j++];
            }
        }

        if (isset($_POST['Well'])) {
            $mdlWell->attributes = $_POST['Well'];
            $mdlWellZone->attributes = $_POST['Wellzone'];

            $mdlWell->wl_latitude = $_POST['Well']['lat_degree'] . ',' . $_POST['Well']['lat_minute'] . ',' . $_POST['Well']['lat_second'] . ',' . $_POST['Well']['lat_direction'];
            $mdlWell->wl_longitude = $_POST['Well']['long_degree'] . ',' . $_POST['Well']['long_minute'] . ',' . $_POST['Well']['long_second'] . ',' . $_POST['Well']['long_direction'];

            $wl_electro_list = '';
            for ($j = 1; $j <= 16; $j++) {
                $wl_electro_list .= $_POST['Well']["wl_electro_list$j"] . ',';
            }

            $mdlWell->wl_submit_date = new CDbExpression('NOW()');

            $mdlWell->wl_year = $this->year->rps_year;

            $wl_electro_list = substr($wl_electro_list, 0, -1);
            $mdlWell->wl_electro_list = $wl_electro_list;

            if ($mdlWell->validate() & $mdlWellZone->validate()) {
                if ($mdlWell->save(false)) {
                    $mdlWellZone->save(false);
                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                }
            } else {
                $error2 = self::validate(array($mdlWell, $mdlWellZone));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatewellpostdrill', array(
            'mdlWell' => $mdlWell,
            'mdlWellZone' => $mdlWellZone,
        ));
    }

    public function actionUpdateWellDiscovery($id) {
        if (!($mdlPlay = Well::model()->findByAttributes(array('wl_id' => $id, 'wk_id' => Yii::app()->user->wk_id, 'prospect_type' => 'Discovery')))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $mdlWell = new Well();
        $mdlWellZone = new Wellzone('zone');

        if (isset($_POST['Well'])) {
            $model = array();
            $mdlWell->attributes = $_POST['Well'];
            $mdlWell->prospect_type = 'Discovery';

            $valid = $mdlWell->validate();

            array_push($model, $mdlWell);

            for ($countWell = 1; $countWell <= $_POST['Well']['wl_total_zone']; $countWell++) {
                $mdlNewWellZone = new Wellzone('zone');
                $mdlNewWellZone->attributes = $_POST['Wellzone'][$countWell];
                $valid = $mdlNewWellZone->validate() && $valid;
                array_push($model, $mdlNewWellZone);
            }

            $error = array();
            foreach ($model as $mdl) {
                array_push($error, $mdl);
            }

            if ($valid) {
                $mdlUpdateWell = Well::model()->findByPk($_POST['Well']['wl_id']);
                $mdlUpdateWell->attributes = $_POST['Well'];
                $mdlUpdateWell->wl_latitude = $_POST['Well']['lat_degree'] . ',' . $_POST['Well']['lat_minute'] . ',' . $_POST['Well']['lat_second'] . ',' . $_POST['Well']['lat_direction'];
                $mdlUpdateWell->wl_longitude = $_POST['Well']['long_degree'] . ',' . $_POST['Well']['long_minute'] . ',' . $_POST['Well']['long_second'] . ',' . $_POST['Well']['long_direction'];
                $wl_electro_list = '';
                for ($j = 1; $j <= 16; $j++) {
                    $wl_electro_list .= $_POST['Well']["wl_electro_list$j"] . ',';
                }

                $wl_electro_list = substr($wl_electro_list, 0, -1);
                $mdlUpdateWell->wl_electro_list = $wl_electro_list;

                $mdlUpdateWell->wl_submit_date = new CDbExpression('NOW()');

                $mdlUpdateWell->wl_year = $this->year->rps_year;

                $mdlUpdateWell->save(false);

                for ($i = 1; $i <= $_POST['Well']['wl_total_zone']; $i++) {
                    if (!empty($_POST['Wellzone'][$i]['zone_id'])) {
                        $mdlUpdateWellZone = Wellzone::model()->findByPk($_POST['Wellzone'][$i]['zone_id']);
                        $mdlUpdateWellZone->attributes = $_POST['Wellzone'][$i];
                        $mdlUpdateWellZone->save(false);
                    } else {
                        $mdlWellZone->zone_id = null;
                        $mdlWellZone->wl_id = $_POST['Well']['wl_id'];
                        $mdlWellZone->attributes = $_POST['Wellzone'][$i];
                        $mdlWellZone->isNewRecord = true;
                        $mdlWellZone->save(false);
                    }
                }
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlWell), $mdlWellZone, $_POST['Well']['wl_total_zone']);

                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatewelldiscovery', array(
            'mdlWell' => $mdlWell,
            'mdlWellZone' => $mdlWellZone,
            'id' => $id,
        ));
    }

    public function actionGetDataWellDiscovery($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlWell = Well::model()->findByPk($id);
            $mdlWellZone = Wellzone::model()->findAllByAttributes(array('wl_id' => $id));
            $count = count($mdlWellZone);

            //array Well
            $arrayWell = array();
            $arrayWell[] = $mdlWell->attributes;
            if (isset($mdlWell->wl_latitude) & isset($mdlWell->wl_longitude)) {
                $bonlat = preg_split("/[,]+/", $mdlWell->wl_latitude);
                $bonlong = preg_split("/[,]+/", $mdlWell->wl_longitude);

                $arrayWell[0]['lat_degree'] = $bonlat[0];
                $arrayWell[0]['lat_minute'] = $bonlat[1];
                $arrayWell[0]['lat_second'] = $bonlat[2];
                $arrayWell[0]['lat_direction'] = $bonlat[3];

                $arrayWell[0]['long_degree'] = $bonlong[0];
                $arrayWell[0]['long_minute'] = $bonlong[1];
                $arrayWell[0]['long_second'] = $bonlong[2];
                $arrayWell[0]['long_direction'] = $bonlong[3];
            }

            if ($mdlWell->wl_electro_list != null) {
                $string = $mdlWell->wl_electro_list;
                $array = explode(',', $string);

                $j = 0;
                for ($i = 1; $i <= 16; $i++) {
                    $list = 'wl_electro_list' . $i;
                    $arrayWell[0]['wl_electro_list' . $i] = $array[$j++];
                }
            }

            $arrayWell[0]['total_wellzone'] = $count;
            //end array Well

            //array Wellzone
            $arrayWellZone = array();
            for ($i = 0; $i <= 4; $i++) {
                if ($i <= (sizeof($mdlWellZone) - 1)) {
                    $arrayWellZone[$i] = $mdlWellZone[$i]->attributes;
                } else {
                    $mdlNewWellZone = new Wellzone();
                    $arrayWellZone[$i] = $mdlNewWellZone->attributes;
                }
            }
            //end array well and zone discovery

            $singleArrayWell = $this->array_flatten($arrayWell, 'one');
            $singleArrayWellZone = $this->array_flatten($arrayWellZone, 'well');

            $array_merge = array_merge($singleArrayWell, $singleArrayWellZone);
            echo CJSON::encode($array_merge);
        }
    }

    public function actionWellName() {
        if (Yii::app()->request->isAjaxRequest) {
            $criteria = new CDbCriteria;
            $criteria->condition = 'prospect_id="' . $_POST['prospect_id'] . '"';
            $criteria->addCondition('prospect_id IS NULL', 'OR');
            $criteria->addCondition('wk_id="' . Yii::app()->user->wk_id . '"', 'AND');
            $criteria->addCondition('prospect_type="' . $_POST['prospect_type'] . '"', 'AND');
            $dataWellNames = Well::model()->findAll($criteria);

            $hasil = array();
            foreach ($dataWellNames as $dataWellName) {
                $hasil[] = array(
                    'id' => $dataWellName->wl_id,
                    'text' => $dataWellName->wl_name . ' - ' . $dataWellName->wl_formation,
                );
            }

            echo CJSON::encode(array('wellname' => $hasil));
            Yii::app()->end();
        }
    }

    public function actionWellNameDiscovery() {
        if (Yii::app()->request->isAjaxRequest) {
            $prospect_id = $_POST['prospect_id'];
            $sql = 'select rsc_well.wl_id, wl_name, rsc_well.prospect_type, GROUP_CONCAT(zone_name) zone_name
          from rsc_well left join rsc_wellzone on rsc_wellzone.wl_id = rsc_well.wl_id
          WHERE (rsc_well.prospect_id="' . $prospect_id . '" OR rsc_well.prospect_id IS NULL) AND wk_id="' . Yii::app()->user->wk_id . '" AND rsc_well.prospect_type="Discovery"
          GROUP BY rsc_wellzone.wl_id';
            $command = Yii::app()->db->createCommand($sql);
            $dataWellNames = $command->queryAll();

            $hasil = array();
            foreach ($dataWellNames as $dataWellName) {
                $hasil[] = array(
                    'id' => $dataWellName['wl_id'],
                    'text' => $dataWellName['wl_name'] . ' - ' . $dataWellName['zone_name'],
                );
            }

            echo CJSON::encode(array('wellname' => $hasil));
            Yii::app()->end();
        }
    }

    public function actionPlayName() {
        if (Yii::app()->request->isAjaxRequest) {
            $mdlPlaySearch = Play::model();
            $model = new Play('search');
            $model->unsetAttributes();

            $criteria = new CDbCriteria;
            $criteria->select = "max(b.play_submit_date)";
            $criteria->alias = 'b';
            $criteria->condition = "b.play_update_from = t.play_update_from";
            $subQuery = $model->getCommandBuilder()->createFindCommand($model->getTableSchema(), $criteria)->getText();

            $mainCriteria = new CDbCriteria;
            $mainCriteria->with = array('gcfs');
            $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
            $mainCriteria->addCondition('play_submit_date = (' . $subQuery . ')', 'AND');
            $mainCriteria->group = 'play_update_from';

            $dataPlays = Play::model()->findAll($mainCriteria);
            $hasil = array();
            foreach ($dataPlays as $dataPlay) {
                $hasil[] = array(
                    'id' => $dataPlay->play_id,
                    'text' => $dataPlay->gcfs->playName,
                );
            }

            echo CJSON::encode(array('play' => $hasil));
            Yii::app()->end();
        }
    }

    public function actionDeleteParticipant($id) {
        if (Yii::app()->request->isAjaxRequest) {
            Kkks::model()->UpdateByPk($id, array('psc_id' => null, 'kkks_interest' => 0, 'kkks_is_operator' => 0));
            Yii::app()->end();
        }
    }

    public function actionContractorName() {
        if (Yii::app()->request->isAjaxRequest) {

            $criteria2 = new CDbCriteria;
            $criteria2->with = array('KkksPsc');
            $criteria2->compare('KkksPsc.wk_id', Yii::app()->user->wk_id);
            $criteria2->compare('kkks_is_operator', 1);

            $dataSelected = Kkks::model()->find($criteria2);
            if ($dataSelected != null) {
                $kkks_id = $dataSelected->kkks_id;
            } else {
                $kkks_id = null;
            }

            $count = count($dataSelected);

            $criteria = new CDbCriteria;
            $criteria->select = "kkks_id, kkks_name";
            $criteria->with = array('KkksPsc');
            $criteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
            $criteria->addCondition('t.psc_id IS NULL', 'OR');

            $dataContractors = Kkks::model()->findAll($criteria);
            $hasil = array();
            foreach ($dataContractors as $dataContractor) {
                $hasil[] = array(
                    'id' => $dataContractor->kkks_id,
                    'text' => $dataContractor->kkks_name,
                );
            }

            echo CJSON::encode(array('contractor' => $hasil, 'count' => $count, 'selected' => $kkks_id));
            Yii::app()->end();
        }
    }

    public function actionOperatorName() {
        if (Yii::app()->request->isAjaxRequest) {
            $criteria = new CDbCriteria;
            $criteria->with = array('KkksPsc');
            $criteria->compare('KkksPsc.wk_id', Yii::app()->user->wk_id);

            $dataOperators = Kkks::model()->findAll($criteria);

            $criteria2 = new CDbCriteria;
            $criteria2->with = array('KkksPsc');
            $criteria2->compare('KkksPsc.wk_id', Yii::app()->user->wk_id);
            $criteria2->compare('kkks_is_operator', 1);

            $dataSelected = Kkks::model()->find($criteria2);
            if ($dataSelected != null) {
                $kkks_id = $dataSelected->kkks_id;
            } else {
                $kkks_id = null;
            }

            $count = count($dataSelected);

            $hasil = array();
            foreach ($dataOperators as $dataOperator) {
                $hasil[] = array(
                    'id' => $dataOperator->kkks_id,
                    'text' => $dataOperator->kkks_name,
                );
            }

            echo CJSON::encode(array('operator' => $hasil, 'count' => $count, 'selected' => $kkks_id));
            Yii::app()->end();
        }
    }

    public function actionDeleteLead($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $criteria = new CDbCriteria;
            $criteria->with = array('plays');
            $criteria->compare('lead_id', $id);
            $criteria->compare('plays.wk_id', Yii::app()->user->wk_id);
            if (!($mdlLead = Lead::model()->find($criteria))) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }

            $mdlLead->lead_is_deleted = 1;
            $mdlLead->save(false);

        }
    }

    public function actionDeleteDrillable($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $criteria1 = new CDbCriteria;
            $criteria1->with = array('plays');
            $criteria1->compare('prospect_id', $id);
            $criteria1->compare('plays.wk_id', Yii::app()->user->wk_id);

            $criteria2 = new CDbCriteria;
            $criteria2->compare('prospect_id', $id);

            if (!($mdlProspect = Prospect::model()->find($criteria1)) | !($mdlDrillable = Drillable::model()->find($criteria2))) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }

            $mdlProspect->prospect_is_deleted = 1;
            $mdlProspect->save(false);
        }
    }

    public function actionDeletePostdrill($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $criteria1 = new CDbCriteria;
            $criteria1->with = array('plays');
            $criteria1->compare('prospect_id', $id);
            $criteria1->compare('plays.wk_id', Yii::app()->user->wk_id);

            $criteria2 = new CDbCriteria;
            $criteria2->compare('prospect_id', $id);

            if (!($mdlProspect = Prospect::model()->find($criteria1)) | !($mdlPostdrill = Postdrill::model()->find($criteria2))) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }

            $mdlProspect->prospect_is_deleted = 1;
            $mdlProspect->save(false);
        }
    }

    public function actionDeleteDiscovery($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $criteria1 = new CDbCriteria;
            $criteria1->with = array('plays');
            $criteria1->compare('prospect_id', $id);
            $criteria1->compare('plays.wk_id', Yii::app()->user->wk_id);

            $criteria2 = new CDbCriteria;
            $criteria2->compare('prospect_id', $id);

            if (!($mdlProspect = Prospect::model()->find($criteria1)) | !($discovery = Discovery::model()->find($criteria2))) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }

            $mdlProspect->prospect_is_deleted = 1;
            $mdlProspect->save(false);
        }
    }
}
