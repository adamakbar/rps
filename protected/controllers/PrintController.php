<?php

class PrintController extends CController
{

    public $repository;

    public function init()
    {
        $this->repository = new ResourcesRepository;
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array(
                    'Index',
                    'PlayDetail',
                ),
                'expression' => 'Yii::app()->user->getState("level") === "kkks"',
            ),
            array('allow',
                'actions' => array(
                    'Impersonate',
                ),
                'expression' => 'Yii::app()->user->getState("switch") === "admin"',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionImpersonate($user) {
        $ui = UserIdentity::impersonate($user);

        if ($ui) {
            Yii::app()->user->login($ui, 0);
        }

        $this->redirect('index.php?r=site/index');
    }

	public function actionIndex()
	{
        $this->render('index');
	}

    public function actionPlayDetail($id) {
        $play = $this->repository->getPlayDetail($id);
        echo json_encode($play);die();
        $html = $this->renderPartial('/print/play_detail', null, true);
        to_pdf($html, 'tes');
    }
}