<?php
class WebController extends Controller
{
  public $layout = '//layouts/login';
  
  public function actionNoie()
  {
    $this->render("noie");
  }

  public function actionMaingate()
  {
    $this->render("maingate");
  }
	
	public function actionLogin()
	{
		$model = new LoginForm();
		
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
		{
			echo TbActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if(isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			if($model->validate() && $model->login())
			{
				$this->redirect('index.php?r=site/index');
			}
		}
		
		$this->render('login', array(
			'model'=>$model,
		));
	}
	
	public function actionError()
	{
		if($error = Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
?>
