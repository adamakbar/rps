<?php

class AdminController extends Controller
{
    public $repository;
    public $dataPrint;

    public function init()
    {
        $this->repository = new ResourcesRepository;
        $this->dataPrint = new DataPrint;
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array(
                    'statustime',
                    'updatestatustime',
                    'skkadmin',
                    'resourcesdata',
                    'manualformat',
                    'resourcesdetail',
                    'impersonate',
                    'getwkdetail',
                    'changeuserpass',
                    'changeadminpass',
                    'changesurat',
                    'resetallpass',
                    'PrintAllData',
                    'KenMaret',
                    'PrintDetailAll',
                    'PrintDetailAllPdf',
                ),
                'expression' => 'Yii::app()->user->getState("level") === "admin"',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionImpersonate($user)
    {
        $ui = UserIdentity::impersonate($user);

        if ($ui) {
            Yii::app()->user->login($ui, 0);
        }

        $this->redirect('index.php?r=site/index');
    }

    public function actionPrintDetailAll()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $headers = new FormatHeader();
        $excel = new PHPExcel();

        $plays = $this->dataPrint->playDetail();
        $leads = $this->dataPrint->leadDetail();
        $drillables = $this->dataPrint->drillableDetail();
        $postdrills = $this->dataPrint->postdrillDetail();
        $discoverys = $this->dataPrint->discoveryDetail();

        // Play
        array_unshift($plays, $headers->play());
        $playSheet = new PHPExcel_Worksheet($excel, 'Play');
        $excel->addSheet($playSheet);
        $playSheet->fromArray($plays, null, 'A1');

        // Lead
        array_unshift($leads, $headers->lead());
        $leadSheet = new PHPExcel_Worksheet($excel, 'Lead');
        $excel->addSheet($leadSheet);
        $leadSheet->fromArray($leads, null, 'A1');

        // Drillable
        array_unshift($drillables, $headers->drillable());
        $drillableSheet = new PHPExcel_Worksheet($excel, 'Drillable');
        $excel->addSheet($drillableSheet);
        $drillableSheet->fromArray($drillables, null, 'A1');

        // Postdrill
        array_unshift($postdrills, $headers->postdrill());
        $postdrillSheet = new PHPExcel_Worksheet($excel, 'Postdrill');
        $excel->addSheet($postdrillSheet);
        $postdrillSheet->fromArray($postdrills, null, 'A1');

        // Discovery
        array_unshift($discoverys, $headers->discovery());
        $discoverySheet = new PHPExcel_Worksheet($excel, 'Discovery');
        $excel->addSheet($discoverySheet);
        $discoverySheet->fromArray($discoverys, null, 'A1');

        $path = "C:/Users/Don/Desktop/ALLDATA.xlsx";
        if (file_exists($path)) {
            unlink($path);
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($path);

        header('Location: ' . route('admin/resourcesdata'));
    }

    public function actionPrintDetailAllPdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $basins = $this->dataPrint->getBasinList();

        foreach ($basins as $basin) {
            $wkList = $this->dataPrint->getWkListBasin($basin);
            $html = '';

            foreach ($wkList as $wk) {
                $html .= $this->wkAssetsHtml($wk);
            }

            $html = $this->scopeBasinPdf($html, $basin);
            to_pdf($html, $basin, null, false);
        }
        // $workingAreas = Yii::app()->db->createCommand()
        //     ->select([
        //         'basin',
        //         'wk',
        //     ])
        //     ->from('adm_basin_wk')
        //     ->where('wk <> "WK1047"')
        //     ->order('basin', 'wk')
        //     ->queryAll();

        // // Test-purpose: WK1062
        // foreach ($workingAreas as $workingArea) {
        //     $basins[$workingArea['basin']] .= $this->wkAssetsHtml($workingArea['wk']);
        //     // $wkhtml = $this->wkAssetsHtml($workingArea['wk']);
        // }

        // foreach ($basins as $basin => $html) {
        //     to_pdf($this->scopeBasinPdf($html, $basin), $basin, null, false);
        //     die();
        // }

        return 'wa';
    }

    public function scopeBasinPdf($html, $basinName)
    {
        $listWk = $this->dataPrint->getWkListBasin($basinName);
        return $this->renderPartial('//print/pdf_basin',
            ['contents' => $html, 'name' => $basinName, 'listWk' => $listWk], true);
    }

    public function wkAssetsHtml($wkid)
    {
        $assets = $this->dataPrint->getWorkingAreaAssets($wkid);
        $plays = '';
        $leads = '';
        $drillables = '';
        $postdrills = '';
        $discoveries = '';
        $postdrillWells = '';
        $discoveryWells = '';

        $playdata = [];
        $leaddata = [];
        $drillabledata = [];
        $postdrilldata = [];
        $discoverydata = [];

        if ($assets['play']) {
            foreach ($assets['play'] as $play) {
                $tmp = $this->playAssetsHtml($play['s']);
                $plays .= $tmp['html'];
                $playdata[$play['s']] = $tmp['data'];
            }
        }

        if ($assets['lead']) {
            foreach ($assets['lead'] as $lead) {
                $tmp = $this->leadAssetsHtml($lead['s']);
                $leads .= $tmp['html'];
                $leaddata[$lead['s']] = $tmp['data'];
            }
        }

        if ($assets['drillable']) {
            foreach ($assets['drillable'] as $drillable) {
                $tmp = $this->drillableAssetsHtml($drillable['s']);
                $drillables .= $tmp['html'];
                $drillabledata[$drillable['s']] = $tmp['data'];
            }
        }

        if ($assets['postdrill']) {
            foreach ($assets['postdrill'] as $postdrill) {
                $tmp = $this->postdrillAssetsHtml($postdrill['s']);
                $postdrills .= $tmp['html'];
                $postdrilldata[$postdrill['s']] = $tmp['data'];
            }
        }

        if ($assets['discovery']) {
            foreach ($assets['discovery'] as $discovery) {
                $tmp = $this->discoveryAssetsHtml($discovery['s']);
                $discoveries .= $tmp['html'];
                $discoverydata[$discovery['s']] = $tmp['data'];
            }
        }

        if ($assets['postdrill_well']) {
            foreach ($assets['postdrill_well'] as $pdWell) {
                $postdrillWells .= $this->wellAssetsHtml($pdWell['well_id']);
            }
        }

        if ($assets['discovery_well']) {
            foreach ($assets['discovery_well'] as $dcWell) {
                $discoveryWells .= $this->wellAssetsHtml($dcWell['well_id']);
            }
        }

        $wkCover = $this->renderPartial('//print/coverwk', ['wkid' => $wkid, 'data' => [$playdata, $leaddata, $drillabledata, $postdrilldata, $discoverydata]], true);

        return $wkCover . $plays . $leads . $drillables . $postdrills . $discoveries . $postdrillWells . $discoveryWells;
    }

    public function playAssetsHtml($playId)
    {
        $data = $this->dataPrint->playDetail($playId);
        $data['play_name'] = play_name_creator(
            $data['gcf_res_lithology'],
            $data['gcf_res_formation_serie'],
            $data['gcf_res_formation'],
            $data['gcf_res_age_serie'],
            $data['gcf_res_age_system'],
            $data['gcf_res_depos_env'],
            $data['gcf_trap_type']);

        $html = $this->renderPartial('/print/form_play', ['data' => $data], true) .
            $this->renderPartial('/print/form_gcf', ['data' => $data], true);

        return ['html' => $html, 'data' => $data];
    }

    public function leadAssetsHtml($leadId)
    {
        $data = $this->dataPrint->leadDetail($leadId);
        $data['play_name'] = play_name_creator(
            $data['plithology'],
            $data['pformation_serie'],
            $data['pformation'],
            $data['page_serie'],
            $data['page'],
            $data['penv'],
            $data['ptrap']);

        $html = $this->renderPartial('/print/form_lead', ['data' => $data], true) .
            $this->renderPartial('/print/form_gcf', ['data' => $data], true);

        return ['html' => $html, 'data' => $data];
    }

    public function drillableAssetsHtml($drillableId)
    {
        $data = $this->dataPrint->drillableDetail($drillableId);
        $data['play_name'] = play_name_creator(
            $data['plithology'],
            $data['pformation_serie'],
            $data['pformation'],
            $data['page_serie'],
            $data['page'],
            $data['penv'],
            $data['ptrap']);

        $html = $this->renderPartial('/print/form_drillable', ['data' => $data], true) .
            $this->renderPartial('/print/form_survey', ['data' => $data], true) .
            $this->renderPartial('/print/form_gcf', ['data' => $data], true);

        return ['html' => $html, 'data' => $data];
    }

    public function postdrillAssetsHtml($postdrillId)
    {
        $data = $this->dataPrint->postdrillDetail($postdrillId);
        $data['play_name'] = play_name_creator(
            $data['plithology'],
            $data['pformation_serie'],
            $data['pformation'],
            $data['page_serie'],
            $data['page'],
            $data['penv'],
            $data['ptrap']);

        $html = $this->renderPartial('/print/form_postdrill', ['data' => $data], true) .
            $this->renderPartial('/print/form_survey', ['data' => $data], true) .
            $this->renderPartial('/print/form_gcf', ['data' => $data], true);

        return ['html' => $html, 'data' => $data];
    }

    public function discoveryAssetsHtml($discoveryId)
    {
        $data = $this->dataPrint->discoveryDetail($discoveryId);
        $data['play_name'] = play_name_creator(
            $data['plithology'],
            $data['pformation_serie'],
            $data['pformation'],
            $data['page_serie'],
            $data['page'],
            $data['penv'],
            $data['ptrap']);

        $html = $this->renderPartial('/print/form_discovery', ['data' => $data], true) .
            $this->renderPartial('/print/form_survey', ['data' => $data], true) .
            $this->renderPartial('/print/form_gcf', ['data' => $data], true);

        return ['html' => $html, 'data' => $data];
    }

    public function wellAssetsHtml($wellId)
    {
        $well = $this->dataPrint->wellDetail($wellId);
        $zones = $this->dataPrint->zoneDetail($wellId);
        $html = '';

        $html .= $this->renderPartial('/print/form_well', ['data' => $well], true);

        foreach ($zones as $zone) {
            $html .= $this->renderPartial('/print/form_zone', ['data' => $zone], true);
        }

        return $html;
    }

    public function arrayToCsv($filename, $arr, $header = '', $datename = true, $xhr = false)
    {
        $filename = "oldassets/" . $filename;

        if ($header != '') {
            array_unshift($arr, $header);
        }

        if ($datename) {
            $today = getdate();
            $today = "_({$today['mday']}-{$today['month']}-{$today['year']})";
            $csp = $filename . $today . '.csv';
        } else {
            $csp = $filename . '.csv';
        }

        $fp = fopen($csp, 'w');
        foreach ($arr as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);

        if ($xhr == true) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($csp));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($csp));
            return $csp;
        } else {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($csp));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($csp));
            readfile($csp);
            unlink($csp);
        }
    }

    public function actionResourcesdata()
    {
        if (isset($_POST['rekap-list-wk'])) {
            $data = $this->dataPrint->rekap_list_wk();

        } elseif (isset($_POST['rekap-play'])) {
            $data = $this->dataPrint->rekap_play();

        } elseif (isset($_POST['rekap-lead'])) {
            $data = $this->dataPrint->rekap_lead();

        } elseif (isset($_POST['rekap-drillable'])) {
            $data = $this->dataPrint->rekap_drillable();

        } elseif (isset($_POST['rekap-postdrill'])) {
            $data = $this->dataPrint->rekap_postdrill();

        } elseif (isset($_POST['rekap-discovery'])) {
            $data = $this->dataPrint->rekap_discovery();

        } elseif (isset($_POST['rekap-well-postdrill'])) {
            $data = $this->dataPrint->rekap_well_postdrill();

        } elseif (isset($_POST['rekap-well-discovery'])) {
            $data = $this->dataPrint->rekap_well_discovery();

        } elseif (isset($_POST['rekap-all-data'])) {
            $data = $this->dataPrint->rekap_all_data();
        }

        $total_all = array(
            'wk_provider' => $this->dataPrint->rekap_resources(),
            'total_wk' => $this->dataPrint->total_wk(),
            'total_play' => $this->dataPrint->total_play(),
            'total_lead' => $this->dataPrint->total_lead(),
            'total_drillable' => $this->dataPrint->total_drillable(),
            'total_postdrill' => $this->dataPrint->total_postdrill(),
            'total_discovery' => $this->dataPrint->total_discovery(),
            'total_well_postdrill' => $this->dataPrint->total_well_postdrill(),
            'total_well_discovery' => $this->dataPrint->total_well_discovery(),
            'total_kirim_surat' => $this->dataPrint->total_kirim_surat(),
            'total_tidak_kirim_surat' => $this->dataPrint->total_tidak_kirim_surat(),
            'total_ada_data' => $this->dataPrint->total_ada_data(),
            'total_tidak_ada_data' => $this->dataPrint->total_tidak_ada_data(),
            'total_wk_eksplorasi' => $this->dataPrint->total_wk_eksplorasi(),
            'total_wk_eksploitasi' => $this->dataPrint->total_wk_eksploitasi(),
            'total_wk_terminasi' => $this->dataPrint->total_wk_terminasi(),
        );

        $this->render('resourcesdata', $total_all);
    }

    public function generate_user_pass($admin = false, $admin_new = '')
    {
        $char = 'abcdefghkmnpqrtuvwxyzABCDEFGHKMNPQRTUVWXYZ346789';
        $len = 8;

        if ($admin == false) {
            $user = $this->get_random_string($char, $len);
            $pass = $this->get_random_string($char, $len);
        } else {
            $user = 'admin';
            $pass = $admin_new;
        }

        $hash = CPasswordHelper::hashPassword($pass);

        return array('user' => $user, 'password' => $pass, 'hash' => $hash);
    }

    public function actionChangesurat($wk_id)
    {
        $surat = Yii::app()->db->createCommand("
            SELECT wk_surat
            FROM adm_working_area
            WHERE wk_id = '{$wk_id}'
        ")->queryAll()[0]['wk_surat'];

        if ($surat == 1) {
            Yii::app()->db->createCommand("
                UPDATE adm_working_area
                SET wk_surat = 0
                WHERE wk_id = '{$wk_id}'
            ")->execute();
        } else {
            Yii::app()->db->createCommand("
                UPDATE adm_working_area
                SET wk_surat = 1
                WHERE wk_id = '{$wk_id}'
            ")->execute();
        }
    }

    public function actionGetwkdetail($wk_id)
    {
        $data = Yii::app()->db->createCommand("
            SELECT
             kkks.kkks_name
            FROM
             adm_working_area wk,
             adm_kkks kkks,
             adm_psc psc
            WHERE
             wk.wk_id = '{$wk_id}'
             AND psc.wk_id = wk.wk_id
             AND kkks.psc_id = psc.psc_id
        ")->queryAll()[0]['kkks_name'];

        echo json_encode(array('kkks_name' => $data));
    }

    public function actionChangeuserpass($wk_id)
    {
        $user_detail = $this->generate_user_pass();

        Yii::app()->db->createCommand("
            UPDATE adm_user
            SET user_name = '{$user_detail['user']}',
                user_pass = '{$user_detail['hash']}'
            WHERE wk_id = '{$wk_id}'
        ")->execute();

        echo json_encode($user_detail);
    }

    public function actionChangeadminpass($new_pass)
    {
        $user_detail = $this->generate_user_pass(true, $new_pass);

        Yii::app()->db->createCommand("
            UPDATE adm_user
            SET user_pass = '{$user_detail['hash']}'
            WHERE wk_id = 'admin'
        ")->execute();
    }

    public function actionResetallpass()
    {
        $char = 'abcdefghkmnpqrtuvwxyzABCDEFGHKMNPQRTUVWXYZ346789';
        $len = 8;

        $q = Yii::app()->db->createCommand("
            SELECT u.wk_id, w.wk_name
            FROM adm_user u, adm_working_area w
            WHERE u.wk_id LIKE 'WK%' AND w.wk_id=u.wk_id
        ")->queryAll();

        $n = array();

        foreach ($q as $key => $val) {
            $user = $this->get_random_string($char, $len);
            $pass = $this->get_random_string($char, $len);
            $hash = CPasswordHelper::hashPassword($pass);
            $n[] = array(
                "wk_id" => $val['wk_id'],
                "wk_name" => $val['wk_name'],
                "user" => $user,
                "pass" => $pass,
                "hash" => $hash);
        }

        foreach ($n as $key => $val) {
            $s = Yii::app()->db->createCommand("
                UPDATE adm_user
                SET user_name='{$val['user']}', user_pass='{$val['hash']}'
                WHERE wk_id='{$val['wk_id']}'
            ")->execute();
        }

        foreach ($n as $key => $val) {
            unset($n[$key]['hash']);
        }

        $hdr = array("WKID", "Nama WK", "User", "Password");
        $filename = $this->arrayToCsv('new_password', $n, $hdr, true, true);
        echo json_encode(array('filename' => $filename));
    }

    public function actionSkkadmin()
    {
        if (isset($_POST['new-wk'])) {
            $char = 'abcdefghkmnpqrtuvwxyzABCDEFGHKMNPQRTUVWXYZ346789';
            $len = 8;
            $wkid = $_POST['wk-id'];
            $wknm = $_POST['wk-name'];
            $ksnm = $_POST['kkks-name'];
            $user = $this->get_random_string($char, $len);
            $pass = $this->get_random_string($char, $len);
            $hash = CPasswordHelper::hashPassword($pass);
            $n = array(
                "wkid" => $wkid,
                "wknm" => $wknm,
                "ksnm" => $ksnm,
                "user" => $user,
                "pass" => $pass,
                "hash" => $hash);

            Yii::app()->db->createCommand("
                INSERT INTO adm_working_area (wk_id, wk_name) VALUES ('{$wkid}', '{$wknm}');
                INSERT INTO adm_kkks (kkks_name) VALUES ('{$ksnm}');
                INSERT INTO adm_psc (wk_id, psc_status) VALUES ('{$wkid}', 1);
                INSERT INTO adm_user (wk_id, user_name, user_pass, user_level) VALUES
                    ('{$wkid}', '{$user}', '{$hash}', 'kkks');
                ")->execute();

            $this->render('skkadmin', array('n' => $n));

        } elseif (isset($_POST['recreate'])) {
            $char = 'abcdefghkmnpqrtuvwxyzABCDEFGHKMNPQRTUVWXYZ346789';
            $len = 8;

            $q = Yii::app()->db->createCommand(
                "SELECT u.wk_id, w.wk_name
                FROM adm_user u, adm_working_area w
                WHERE u.wk_id LIKE 'WK%' AND w.wk_id=u.wk_id");
            $q = $q->queryAll();
            $n = array();

            foreach ($q as $key => $val) {
                $user = $this->get_random_string($char, $len);
                $pass = $this->get_random_string($char, $len);
                $hash = CPasswordHelper::hashPassword($pass);
                $n[] = array(
                    "wk_id" => $val['wk_id'],
                    "wk_name" => $val['wk_name'],
                    "user" => $user,
                    "pass" => $pass,
                    "hash" => $hash);
            }

            foreach ($n as $key => $val) {
                $s = Yii::app()->db->createCommand(
                    "UPDATE adm_user
                    SET user_name='{$val['user']}', user_pass='{$val['hash']}'
                    WHERE wk_id='{$val['wk_id']}'
                ")->execute();
            }

            foreach ($n as $key => $val) {
                unset($n[$key]['hash']);
            }
            $hdr = array("WKID", "Nama WK", "User", "Password");
            $this->arrayToCsv('new_password', $n, $hdr);
            $this->render('skkadmin');

        } else {
            $q = Yii::app()->db->createCommand("
                SELECT wk_id, wk_name FROM adm_working_area WHERE wk_id <> 'WK0000' AND wk_id <> 'WK9999'
            ")->queryAll();

            $n = array(
                "wkid" => $q,
                "wknm" => '',
                "ksnm" => '',
                "user" => '',
                "pass" => '');

            $this->render('skkadmin', array('n' => $n));
        }

    }

    public function get_random_string($valid_chars, $length)
    {
        $random_string = '';
        $num_valid_chars = strlen($valid_chars);

        for ($i = 0; $i < $length; $i++) {
            $random_pick = mt_rand(1, $num_valid_chars);
            $random_char = $valid_chars[$random_pick - 1];
            $random_string .= $random_char;
        }
        return $random_string;
    }

    public function actionStatusTime()
    {
        $mdlStatus = new Status();
        $mdlStatusSearch = Status::model();
        $statusDataProvider = new CActiveDataProvider(get_class($mdlStatusSearch));

        if (isset($_POST['Status'])) {
            $mdlStatus->attributes = $_POST['Status'];
            if ($mdlStatus->validate()) {
                $mdlStatus->save(false);
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlStatus));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('statustime', array(
            'statusDataProvider' => $statusDataProvider,
            'mdlStatus' => $mdlStatus,
        ));
    }

    public function actionUpdateStatusTime($id)
    {
        if (!($mdlStatus = Status::model()->findByPk($id))) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        if (isset($_POST['Status'])) {
            $mdlStatus->attributes = $_POST['Status'];
            if ($mdlStatus->validate()) {
                $mdlStatus->save(false);
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                $error2 = self::validate(array($mdlStatus));
                if ($error2 != '[]') {
                    echo json_encode(array('result' => 'error', 'msg' => 'Failed! Has input error', 'err' => $error2));
                }
                Yii::app()->end();
            }
        }

        $this->render('updatestatustime', array('mdlStatus' => $mdlStatus));
    }

    public static function validate($models, $dynamicModel = null, $totalZone = 0, $attributes = null, $loadInput = true)
    {
        //copy from CActiveForm:validate
        $result = array();
        if (!is_array($models)) {
            $models = array($models);
        }

        foreach ($models as $model) {
            if ($loadInput && isset($_POST[get_class($model)])) {
                $model->attributes = $_POST[get_class($model)];
            }

            $model->validate($attributes);
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[CHtml::activeId($model, $attribute)] = $errors;
            }

        }
        //end copying

        //your own customization
        for ($count = 1; $count <= $totalZone; $count++) {
            $dynamicModel->clearErrors();

            if ($loadInput && isset($_POST[get_class($dynamicModel)][$count])) {
                $dynamicModel->attributes = $_POST[get_class($dynamicModel)][$count];
            }

            $dynamicModel->validate($attributes);

            foreach ($dynamicModel->getErrors() as $attribute => $errors) {
                $result[get_class($dynamicModel) . '_' . $count . '_' . $attribute] = $errors;
            }

        }

        return function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
    }

    public function actionKenMaret()
    {
        $fvf = new FormationVolumeFactor();

        $headers = [
            'Basin',
            'Province',
            'KKKS',
            'WKID',
            'Nama WK',
            'Class',
            'Structure',
            'Well Name',
            'Well Zone',
            'Boi P90',
            'Boi P50',
            'Boi P10',
            'Bgi P90',
            'Bgi P50',
            'Bgi P10',
            'Perforation',
            'Initial Reservoir Pressure',
            'Fluid Compressibility',
            'Permeability',
            'Viscocity',
        ];

        $postdrills = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             CASE pr.prospect_type WHEN 'postdrill' THEN 'Postdrill' ELSE 'Discovery' END class,
             pr.structure_name structure,
             wl.wl_name,
             wz.zone_name,

             wz.zone_fvf_oil_p90 fvfboi_p90,
             wz.zone_fvf_oil_p50 fvfboi_p50,
             wz.zone_fvf_oil_p10 fvfboi_p10,
             wz.zone_fvf_gas_p90 fvfbgi_p90,
             wz.zone_fvf_gas_p50 fvfbgi_p50,
             wz.zone_fvf_gas_p10 fvfbgi_p10,
             wz.zone_perforation perforation,
             wz.zone_initial_pressure initial_pressure,
             wz.zone_prod_compres_fluid fluid_compressibility,
             wz.zone_prod_permeability permeability,
             wz.zone_viscocity viscosity

            FROM
             rsc_prospect pr
             LEFT JOIN rsc_seismic_2d s2 ON s2.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geological gl ON gl.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geochemistry gh ON gh.prospect_id = pr.prospect_id
             LEFT JOIN rsc_gravity gr ON gr.prospect_id = pr.prospect_id
             LEFT JOIN rsc_electromagnetic el ON el.prospect_id = pr.prospect_id
             LEFT JOIN rsc_resistivity rs ON rs.prospect_id = pr.prospect_id
             LEFT JOIN rsc_other ot ON ot.prospect_id = pr.prospect_id
             LEFT JOIN rsc_seismic_3d s3 ON s3.prospect_id = pr.prospect_id
             LEFT JOIN rsc_play pl ON pl.play_id = pr.play_id
             LEFT JOIN rsc_gcf gcf ON gcf.gcf_id = pr.gcf_id
             LEFT JOIN rsc_gcf pgcf ON pgcf.gcf_id = pl.gcf_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id
             LEFT JOIN rsc_well wl ON (wl.prospect_id = pr.prospect_id
               AND wl.wk_id = pl.wk_id AND (wl.prospect_type = 'postdrill' OR wl.prospect_type = 'discovery'))
             LEFT JOIN rsc_wellzone wz ON wz.wl_id = wl.wl_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND pl.wk_id <> 'WK1452'
             AND pr.prospect_is_deleted = 0
             AND (pr.prospect_type = 'postdrill' OR pr.prospect_type = 'discovery')
        ";

        $data = Yii::app()->db->createCommand($postdrills)->queryAll();

        $perforationAvg = [];
        $initial_pressureAvg = [];
        $fluid_compressibilityAvg = [];
        $permeabilityAvg = [];
        $viscocityAvg = [];

        $new = [];

        // Pencarian average
        foreach ($data as &$structure) {
            // Perforation berisi data yang tidak sesuai dengan float cth: "1340-1345"
            // hapus data tersebut
            if (strpos($structure['perforation'], '-') !== false || $structure['structure'] == 'Bayan A') {
                $structure['perforation'] = null;
            } elseif (strpos($structure['perforation'], ',') !== false) {
                $structure['perforation'] = null;
            }

            if (!empty($structure['perforation'])) {
                $perforationAvg[] = (float) $structure['perforation'];
            }

            if (!empty($structure['initial_pressure'])) {
                $initial_pressureAvg[] = (float) $structure['initial_pressure'];
            }

            if (!empty($structure['fluid_compressibility'])) {
                $fluid_compressibilityAvg[] = (float) $structure['fluid_compressibility'];
            }

            if (!empty($structure['permeability'])) {
                $permeabilityAvg[] = (float) $structure['permeability'];
            }

            if (!empty($structure['viscosity'])) {
                $viscosityAvg[] = (float) $structure['viscosity'];
            }
        }

        $perforationAvg = array_sum($perforationAvg) / count($perforationAvg);
        $initial_pressureAvg = array_sum($initial_pressureAvg) / count($initial_pressureAvg);
        $fluid_compressibilityAvg = array_sum($fluid_compressibilityAvg) / count($fluid_compressibilityAvg);
        $permeabilityAvg = array_sum($permeabilityAvg) / count($permeabilityAvg);
        $viscocityAvg = array_sum($viscosityAvg) / count($viscosityAvg);

        // Gunakan average pada data yang kosong
        foreach ($data as &$structure) {
            if (empty($structure['perforation'])) {
                $structure['perforation'] = $perforationAvg;
            }

            if (empty($structure['fvfboi_p50'])) {
                $structure['fvfboi_p90'] = $fvf->bo($structure['perforation']);
                $structure['fvfboi_p50'] = $fvf->bo($structure['perforation']);
                $structure['fvfboi_p10'] = $fvf->bo($structure['perforation']);
            }

            if (empty($structure['fvfbgi_p50'])) {
                $structure['fvfbgi_p90'] = $fvf->bg($structure['perforation']);
                $structure['fvfbgi_p50'] = $fvf->bg($structure['perforation']);
                $structure['fvfbgi_p10'] = $fvf->bg($structure['perforation']);
            }

            if (empty($structure['initial_pressure'])) {
                $structure['initial_pressure'] = $initial_pressureAvg;
            }

            if (empty($structure['fluid_compressibility'])) {
                $structure['fluid_compressibility'] = $fluid_compressibilityAvg;
            }

            if (empty($structure['permeability'])) {
                $structure['permeability'] = $permeabilityAvg;
            }

            if (empty($structure['viscosity'])) {
                $structure['viscosity'] = array_sum($viscosityAvg) / count($viscosityAvg);
            }
        }
        array_unshift($data, $headers);

        $this->arrayToCsv('KEN Maret', $data);
    }

    public function actionPrintAllData()
    {
        $workingAreas = Yii::app()->db->createCommand()
            ->select([
                'wk.wk_id',
                'wk.wk_name wkName',
            ])
            ->from('adm_working_area wk')
            ->where('wk.wk_id <> "WK0000"')
            ->andWhere('wk.wk_id <> "WK9999"')
            ->queryAll();

        $excel = new ExcelFormat('WK1001');
        $excel->addPlay('1');
        $excel->save();

        // foreach ($workingAreas as $workingArea) {
        //     $excel = new ExcelFormat($workingArea['wk_id']);
        //     $excel->addPlay('1')->save();
        // }
    }

    private function _printWorkingArea($wkid)
    {
        $plays = $this->repository->getPlayIndex('WK1001');
        $nPlays = [];

        foreach ($plays as $play) {
            $nPlays[] = $this->repository->getPlayDetail($play['play_id']);
        }

    }
}

