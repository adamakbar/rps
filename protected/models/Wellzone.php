<?php

/**
 * This is the model class for table "rsc_wellzone".
 *
 * The followings are the available columns in table 'rsc_wellzone':
 * @property integer $zone_id
 * @property integer $wl_id
 * @property integer $prospect_id
 * @property string $zone_hc_oil_show
 * @property string $zone_hc_oil_show_tools
 * @property string $zone_hc_gas_show
 * @property string $zone_hc_gas_show_tools
 * @property string $zone_hc_water_cut
 * @property string $zone_hc_water_gwc
 * @property string $zone_hc_water_gwd_tools
 * @property string $zone_hc_water_owc
 * @property string $zone_hc_water_owc_tools
 * @property string $zone_rock_method
 * @property string $zone_rock_petro
 * @property string $zone_rock_petro_sample
 * @property string $zone_rock_top_depth
 * @property string $zone_rock_bot_depth
 * @property string $zone_rock_barrel
 * @property string $zone_rock_barrel_equal
 * @property string $zone_rock_total_core
 * @property string $zone_rock_preservative
 * @property string $zone_rock_routine
 * @property string $zone_rock_routine_sample
 * @property string $zone_rock_scal
 * @property string $zone_rock_scal_sample
 * @property double $zone_clastic_p10_thickness
 * @property double $zone_clastic_p50_thickness
 * @property double $zone_clastic_p90_thickness
 * @property double $zone_clastic_p10_gr
 * @property double $zone_clastic_p50_gr
 * @property double $zone_clastic_p90_gr
 * @property double $zone_clastic_p10_sp
 * @property double $zone_clastic_p50_sp
 * @property double $zone_clastic_p90_sp
 * @property double $zone_clastic_p10_net
 * @property double $zone_clastic_p50_net
 * @property double $zone_clastic_p90_net
 * @property double $zone_clastic_p10_por
 * @property double $zone_clastic_p50_por
 * @property double $zone_clastic_p90_por
 * @property double $zone_clastic_p10_satur
 * @property double $zone_clastic_p50_satur
 * @property double $zone_clastic_p90_satur
 * @property double $zone_carbo_p10_thickness
 * @property double $zone_carbo_p50_thickness
 * @property double $zone_carbo_p90_thickness
 * @property double $zone_carbo_p10_dtc
 * @property double $zone_carbo_p50_dtc
 * @property double $zone_carbo_p90_dtc
 * @property double $zone_carbo_p10_total
 * @property double $zone_carbo_p50_total
 * @property double $zone_carbo_p90_total
 * @property double $zone_carbo_p10_net
 * @property double $zone_carbo_p50_net
 * @property double $zone_carbo_p90_net
 * @property double $zone_carbo_p10_por
 * @property double $zone_carbo_p50_por
 * @property double $zone_carbo_p90_por
 * @property double $zone_carbo_p10_satur
 * @property double $zone_carbo_p50_satur
 * @property double $zone_carbo_p90_satur
 * @property string $zone_fluid_date
 * @property string $zone_fluid_sample
 * @property string $zone_fluid_ratio
 * @property string $zone_fluid_pressure
 * @property string $zone_fluid_temp
 * @property string $zone_fluid_tubing
 * @property string $zone_fluid_casing
 * @property string $zone_fluid_by
 * @property string $zone_fluid_report
 * @property string $zone_fluid_finger
 * @property string $zone_fluid_grv_oil
 * @property string $zone_fluid_grv_gas
 * @property string $zone_fluid_grv_conden
 * @property string $zone_fluid_pvt
 * @property string $zone_fluid_quantity
 * @property string $zone_fluid_z
 * @property double $zone_fvf_oil_p10
 * @property double $zone_fvf_oil_p50
 * @property double $zone_fvf_oil_p90
 * @property double $zone_fvf_gas_p10
 * @property double $zone_fvf_gas_p50
 * @property double $zone_fvf_gas_p90
 * @property string $zone_viscocity
 * @property string $zone_name
 * @property string $zone_test
 * @property string $zone_test_date
 * @property string $zone_result
 * @property string $zone_area
 * @property string $zone_thickness
 * @property string $zone_depth
 * @property string $zone_perforation
 * @property string $zone_well_test
 * @property string $zone_well_duration
 * @property string $zone_initial_flow
 * @property string $zone_initial_shutin
 * @property string $zone_tubing_size
 * @property string $zone_initial_temp
 * @property string $zone_initial_pressure
 * @property string $zone_pseudostate
 * @property string $zone_well_formation
 * @property string $zone_head
 * @property string $zone_pressure_wellbore
 * @property string $zone_average_por
 * @property string $zone_water_cut
 * @property string $zone_initial_water
 * @property string $zone_low_gas
 * @property string $zone_low_oil
 * @property string $zone_free_water
 * @property string $zone_grv_gas
 * @property string $zone_grv_grv_oil
 * @property string $zone_wellbore_coefficient
 * @property string $zone_wellbore_time
 * @property string $zone_res_shape
 * @property string $zone_prod_oil_choke
 * @property string $zone_prod_oil_flow
 * @property string $zone_prod_gas_choke
 * @property string $zone_prod_gas_flow
 * @property string $zone_prod_gasoil_ratio
 * @property string $zone_prod_conden_ratio
 * @property string $zone_prod_cumm_gas
 * @property string $zone_prod_cumm_oil
 * @property string $zone_prod_aof_bbl
 * @property string $zone_prod_aof_scf
 * @property string $zone_prod_critical_bbl
 * @property string $zone_prod_critical_scf
 * @property string $zone_prod_index_bbl
 * @property string $zone_prod_index_scf
 * @property string $zone_prod_diffusity
 * @property string $zone_prod_permeability
 * @property string $zone_prod_infinite
 * @property string $zone_prod_well_radius
 * @property string $zone_prod_pseudostate
 * @property string $zone_prod_res_radius
 * @property string $zone_prod_delta
 * @property string $zone_prod_wellbore
 * @property string $zone_prod_compres_rock
 * @property string $zone_prod_compres_fluid
 * @property string $zone_prod_compres_total
 * @property string $zone_prod_second
 * @property string $zone_prod_lambda
 * @property string $zone_prod_omega
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 * @property RscWell $wl
 */
class Wellzone extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_wellzone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
// 			array(
// 				'zone_hc_oil_show, zone_hc_gas_show',	
// 				'required'
// 			),
			array(
				'zone_name, zone_test, zone_prod_well_radius',
				'required', 
				'on'=>'zone'
			),
			array(
				'zone_fluid_z',
				'decimalFluid',
			),
			array(
				'zone_viscocity',
				'numerical',
				'min'=>0.1,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('zone_fluid_temp',
				'numerical',
// 				'numberPattern'=>'/^\s*[-+]?\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'zone_hc_water_gwc, zone_hc_water_owc, zone_rock_top_depth, zone_rock_bot_depth,
					zone_rock_barrel, zone_rock_barrel_equal, zone_rock_total_core, zone_rock_preservative,
					zone_clastic_p10_thickness, zone_clastic_p50_thickness, zone_clastic_p90_thickness,
					zone_clastic_p10_gr, zone_clastic_p50_gr, zone_clastic_p90_gr,
					zone_clastic_p10_sp, zone_clastic_p50_sp, zone_clastic_p90_sp,
					zone_carbo_p10_thickness, zone_carbo_p50_thickness, zone_carbo_p90_thickness,
					zone_carbo_p10_dtc, zone_carbo_p50_dtc, zone_carbo_p90_dtc,
					zone_carbo_p10_total, zone_carbo_p50_total, zone_carbo_p90_total,
					zone_fluid_sample, zone_fluid_ratio, zone_fluid_pressure, zone_fluid_tubing,
					zone_fluid_casing, zone_fluid_grv_oil, zone_fluid_grv_gas, zone_fluid_grv_conden,
					zone_fvf_oil_p10, zone_fvf_oil_p50, zone_fvf_oil_p90,
					zone_fvf_gas_p10, zone_fvf_gas_p50, zone_fvf_gas_p90',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'zone_area, zone_thickness, zone_well_duration, zone_initial_flow, zone_initial_shutin,
					zone_tubing_size, zone_initial_temp, zone_initial_pressure, zone_pseudostate, zone_well_formation, zone_head,
					zone_pressure_wellbore, zone_low_gas, zone_low_oil, zone_free_water, zone_prod_oil_choke, zone_prod_oil_flow,
					zone_prod_gas_choke, zone_prod_gas_flow, zone_prod_gasoil_ratio, zone_prod_cumm_gas, zone_prod_cumm_oil,
					zone_prod_aof_bbl, zone_prod_aof_scf, zone_prod_critical_bbl, zone_prod_critical_scf, zone_prod_index_bbl,
					zone_prod_index_scf, zone_prod_permeability, zone_prod_infinite, zone_prod_well_radius, zone_prod_pseudostate,
					zone_prod_res_radius, zone_prod_compres_rock, zone_prod_compres_fluid, zone_prod_compres_total',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/',
				'on'=>'zone'
			),
			array('wl_id, prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'zone_hc_water_cut, zone_clastic_p10_net, zone_clastic_p50_net, zone_clastic_p90_net,
					zone_clastic_p10_por, zone_clastic_p50_por, zone_clastic_p90_por,
					zone_clastic_p10_satur, zone_clastic_p50_satur, zone_clastic_p90_satur,
					zone_carbo_p10_net, zone_carbo_p50_net, zone_carbo_p90_net,
					zone_carbo_p10_por, zone_carbo_p50_por, zone_carbo_p90_por,
					zone_carbo_p10_satur, zone_carbo_p50_satur, zone_carbo_p90_satur',
				'numerical',
				'min'=>0,
				'max'=>100,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/',
			),
			array(
				'zone_average_por, zone_water_cut, zone_initial_water, zone_prod_second, zone_prod_omega',
				'numerical',
				'min'=>0,
				'max'=>100,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/',
				'on'=>'zone'
			),
			array('zone_hc_oil_show, zone_hc_oil_show_tools, zone_hc_gas_show, zone_hc_gas_show_tools, zone_hc_water_cut, zone_hc_water_gwc, zone_hc_water_gwd_tools, zone_hc_water_owc, zone_hc_water_owc_tools, zone_rock_method, zone_rock_petro, zone_rock_petro_sample, zone_rock_top_depth, zone_rock_bot_depth, zone_rock_barrel, zone_rock_barrel_equal, zone_rock_total_core, zone_rock_preservative, zone_rock_routine, zone_rock_routine_sample, zone_rock_scal, zone_rock_scal_sample, zone_fluid_sample, zone_fluid_ratio, zone_fluid_pressure, zone_fluid_temp, zone_fluid_tubing, zone_fluid_casing, zone_fluid_by, zone_fluid_report, zone_fluid_finger, zone_fluid_grv_oil, zone_fluid_grv_gas, zone_fluid_grv_conden, zone_fluid_pvt, zone_fluid_quantity, zone_fluid_z, zone_viscocity, zone_name, zone_test, zone_result, zone_area, zone_thickness, zone_depth, zone_perforation, zone_well_test, zone_well_duration, zone_initial_flow, zone_initial_shutin, zone_tubing_size, zone_initial_temp, zone_initial_pressure, zone_pseudostate, zone_well_formation, zone_head, zone_pressure_wellbore, zone_average_por, zone_water_cut, zone_initial_water, zone_low_gas, zone_low_oil, zone_free_water, zone_grv_gas, zone_grv_grv_oil, zone_wellbore_coefficient, zone_wellbore_time, zone_res_shape, zone_prod_oil_choke, zone_prod_oil_flow, zone_prod_gas_choke, zone_prod_gas_flow, zone_prod_gasoil_ratio, zone_prod_conden_ratio, zone_prod_cumm_gas, zone_prod_cumm_oil, zone_prod_aof_bbl, zone_prod_aof_scf, zone_prod_critical_bbl, zone_prod_critical_scf, zone_prod_index_bbl, zone_prod_index_scf, zone_prod_diffusity, zone_prod_permeability, zone_prod_infinite, zone_prod_well_radius, zone_prod_pseudostate, zone_prod_res_radius, zone_prod_delta, zone_prod_wellbore, zone_prod_compres_rock, zone_prod_compres_fluid, zone_prod_compres_total, zone_prod_second, zone_prod_lambda, zone_prod_omega', 'length', 'max'=>50),
			array(
				'zone_clastic_p10_thickness, zone_clastic_p50_thickness, zone_clastic_p90_thickness,
					zone_clastic_p10_gr, zone_clastic_p50_gr, zone_clastic_p90_gr,
					zone_clastic_p10_sp, zone_clastic_p50_sp, zone_clastic_p90_sp,
					zone_clastic_p10_net, zone_clastic_p50_net, zone_clastic_p90_net,
					zone_clastic_p10_por, zone_clastic_p50_por, zone_clastic_p90_por,
					zone_clastic_p10_satur, zone_clastic_p50_satur, zone_clastic_p90_satur,
					zone_carbo_p10_thickness, zone_carbo_p50_thickness, zone_carbo_p90_thickness,
					zone_carbo_p10_dtc, zone_carbo_p50_dtc, zone_carbo_p90_dtc,
					zone_carbo_p10_total, zone_carbo_p50_total zone_carbo_p90_total,
					zone_carbo_p10_net, zone_carbo_p50_net, zone_carbo_p90_net,
					zone_carbo_p10_por, zone_carbo_p50_por, zone_carbo_p90_por,
					zone_carbo_p10_satur, zone_carbo_p50_satur, zone_carbo_p90_satur,
					zone_fvf_oil_p10, zone_fvf_oil_p50, zone_fvf_oil_p90,
					zone_fvf_gas_p10, zone_fvf_gas_p50, zone_fvf_gas_p90',
				'chkDependentWell'
			),
			array('zone_fluid_date, zone_test_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('zone_id, wl_id, prospect_id, zone_hc_oil_show, zone_hc_oil_show_tools, zone_hc_gas_show, zone_hc_gas_show_tools, zone_hc_water_cut, zone_hc_water_gwc, zone_hc_water_gwd_tools, zone_hc_water_owc, zone_hc_water_owc_tools, zone_rock_method, zone_rock_petro, zone_rock_petro_sample, zone_rock_top_depth, zone_rock_bot_depth, zone_rock_barrel, zone_rock_barrel_equal, zone_rock_total_core, zone_rock_preservative, zone_rock_routine, zone_rock_routine_sample, zone_rock_scal, zone_rock_scal_sample, zone_clastic_p10_thickness, zone_clastic_p50_thickness, zone_clastic_p90_thickness, zone_clastic_p10_gr, zone_clastic_p50_gr, zone_clastic_p90_gr, zone_clastic_p10_sp, zone_clastic_p50_sp, zone_clastic_p90_sp, zone_clastic_p10_net, zone_clastic_p50_net, zone_clastic_p90_net, zone_clastic_p10_por, zone_clastic_p50_por, zone_clastic_p90_por, zone_clastic_p10_satur, zone_clastic_p50_satur, zone_clastic_p90_satur, zone_carbo_p10_thickness, zone_carbo_p50_thickness, zone_carbo_p90_thickness, zone_carbo_p10_dtc, zone_carbo_p50_dtc, zone_carbo_p90_dtc, zone_carbo_p10_total, zone_carbo_p50_total, zone_carbo_p90_total, zone_carbo_p10_net, zone_carbo_p50_net, zone_carbo_p90_net, zone_carbo_p10_por, zone_carbo_p50_por, zone_carbo_p90_por, zone_carbo_p10_satur, zone_carbo_p50_satur, zone_carbo_p90_satur, zone_fluid_date, zone_fluid_sample, zone_fluid_ratio, zone_fluid_pressure, zone_fluid_temp, zone_fluid_tubing, zone_fluid_casing, zone_fluid_by, zone_fluid_report, zone_fluid_finger, zone_fluid_grv_oil, zone_fluid_grv_gas, zone_fluid_grv_conden, zone_fluid_pvt, zone_fluid_quantity, zone_fluid_z, zone_fvf_oil_p10, zone_fvf_oil_p50, zone_fvf_oil_p90, zone_fvf_gas_p10, zone_fvf_gas_p50, zone_fvf_gas_p90, zone_viscocity, zone_name, zone_test, zone_test_date, zone_result, zone_area, zone_thickness, zone_depth, zone_perforation, zone_well_test, zone_well_duration, zone_initial_flow, zone_initial_shutin, zone_tubing_size, zone_initial_temp, zone_initial_pressure, zone_pseudostate, zone_well_formation, zone_head, zone_pressure_wellbore, zone_average_por, zone_water_cut, zone_initial_water, zone_low_gas, zone_low_oil, zone_free_water, zone_grv_gas, zone_grv_grv_oil, zone_wellbore_coefficient, zone_wellbore_time, zone_res_shape, zone_prod_oil_choke, zone_prod_oil_flow, zone_prod_gas_choke, zone_prod_gas_flow, zone_prod_gasoil_ratio, zone_prod_conden_ratio, zone_prod_cumm_gas, zone_prod_cumm_oil, zone_prod_aof_bbl, zone_prod_aof_scf, zone_prod_critical_bbl, zone_prod_critical_scf, zone_prod_index_bbl, zone_prod_index_scf, zone_prod_diffusity, zone_prod_permeability, zone_prod_infinite, zone_prod_well_radius, zone_prod_pseudostate, zone_prod_res_radius, zone_prod_delta, zone_prod_wellbore, zone_prod_compres_rock, zone_prod_compres_fluid, zone_prod_compres_total, zone_prod_second, zone_prod_lambda, zone_prod_omega', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
			'wl' => array(self::BELONGS_TO, 'RscWell', 'wl_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'zone_id' => 'Zone',
			'wl_id' => 'Wl',
			'prospect_id' => 'Prospect',
			'zone_hc_oil_show' => 'Zone Hc Oil Show',
			'zone_hc_oil_show_tools' => 'Zone Hc Oil Show Tools',
			'zone_hc_gas_show' => 'Zone Hc Gas Show',
			'zone_hc_gas_show_tools' => 'Zone Hc Gas Show Tools',
			'zone_hc_water_cut' => 'Zone Hc Water Cut',
			'zone_hc_water_gwc' => 'Zone Hc Water Gwc',
			'zone_hc_water_gwd_tools' => 'Zone Hc Water Gwd Tools',
			'zone_hc_water_owc' => 'Zone Hc Water Owc',
			'zone_hc_water_owc_tools' => 'Zone Hc Water Owc Tools',
			'zone_rock_method' => 'Zone Rock Method',
			'zone_rock_petro' => 'Zone Rock Petro',
			'zone_rock_petro_sample' => 'Zone Rock Petro Sample',
			'zone_rock_top_depth' => 'Zone Rock Top Depth',
			'zone_rock_bot_depth' => 'Zone Rock Bot Depth',
			'zone_rock_barrel' => 'Zone Rock Barrel',
			'zone_rock_barrel_equal' => 'Zone Rock Barrel Equal',
			'zone_rock_total_core' => 'Zone Rock Total Core',
			'zone_rock_preservative' => 'Zone Rock Preservative',
			'zone_rock_routine' => 'Zone Rock Routine',
			'zone_rock_routine_sample' => 'Zone Rock Routine Sample',
			'zone_rock_scal' => 'Zone Rock Scal',
			'zone_rock_scal_sample' => 'Zone Rock Scal Sample',
			'zone_clastic_p10_thickness' => 'Zone Clastic P10 Thickness',
			'zone_clastic_p50_thickness' => 'Zone Clastic P50 Thickness',
			'zone_clastic_p90_thickness' => 'Zone Clastic P90 Thickness',
			'zone_clastic_p10_gr' => 'Zone Clastic P10 Gr',
			'zone_clastic_p50_gr' => 'Zone Clastic P50 Gr',
			'zone_clastic_p90_gr' => 'Zone Clastic P90 Gr',
			'zone_clastic_p10_sp' => 'Zone Clastic P10 Sp',
			'zone_clastic_p50_sp' => 'Zone Clastic P50 Sp',
			'zone_clastic_p90_sp' => 'Zone Clastic P90 Sp',
			'zone_clastic_p10_net' => 'Zone Clastic P10 Net',
			'zone_clastic_p50_net' => 'Zone Clastic P50 Net',
			'zone_clastic_p90_net' => 'Zone Clastic P90 Net',
			'zone_clastic_p10_por' => 'Zone Clastic P10 Por',
			'zone_clastic_p50_por' => 'Zone Clastic P50 Por',
			'zone_clastic_p90_por' => 'Zone Clastic P90 Por',
			'zone_clastic_p10_satur' => 'Zone Clastic P10 Satur',
			'zone_clastic_p50_satur' => 'Zone Clastic P50 Satur',
			'zone_clastic_p90_satur' => 'Zone Clastic P90 Satur',
			'zone_carbo_p10_thickness' => 'Zone Carbo P10 Thickness',
			'zone_carbo_p50_thickness' => 'Zone Carbo P50 Thickness',
			'zone_carbo_p90_thickness' => 'Zone Carbo P90 Thickness',
			'zone_carbo_p10_dtc' => 'Zone Carbo P10 Dtc',
			'zone_carbo_p50_dtc' => 'Zone Carbo P50 Dtc',
			'zone_carbo_p90_dtc' => 'Zone Carbo P90 Dtc',
			'zone_carbo_p10_total' => 'Zone Carbo P10 Total',
			'zone_carbo_p50_total' => 'Zone Carbo P50 Total',
			'zone_carbo_p90_total' => 'Zone Carbo P90 Total',
			'zone_carbo_p10_net' => 'Zone Carbo P10 Net',
			'zone_carbo_p50_net' => 'Zone Carbo P50 Net',
			'zone_carbo_p90_net' => 'Zone Carbo P90 Net',
			'zone_carbo_p10_por' => 'Zone Carbo P10 Por',
			'zone_carbo_p50_por' => 'Zone Carbo P50 Por',
			'zone_carbo_p90_por' => 'Zone Carbo P90 Por',
			'zone_carbo_p10_satur' => 'Zone Carbo P10 Satur',
			'zone_carbo_p50_satur' => 'Zone Carbo P50 Satur',
			'zone_carbo_p90_satur' => 'Zone Carbo P90 Satur',
			'zone_fluid_date' => 'Zone Fluid Date',
			'zone_fluid_sample' => 'Zone Fluid Sample',
			'zone_fluid_ratio' => 'Zone Fluid Ratio',
			'zone_fluid_pressure' => 'Zone Fluid Pressure',
			'zone_fluid_temp' => 'Zone Fluid Temp',
			'zone_fluid_tubing' => 'Zone Fluid Tubing',
			'zone_fluid_casing' => 'Zone Fluid Casing',
			'zone_fluid_by' => 'Zone Fluid By',
			'zone_fluid_report' => 'Zone Fluid Report',
			'zone_fluid_finger' => 'Zone Fluid Finger',
			'zone_fluid_grv_oil' => 'Zone Fluid Grv Oil',
			'zone_fluid_grv_gas' => 'Zone Fluid Grv Gas',
			'zone_fluid_grv_conden' => 'Zone Fluid Grv Conden',
			'zone_fluid_pvt' => 'Zone Fluid Pvt',
			'zone_fluid_quantity' => 'Zone Fluid Quantity',
			'zone_fluid_z' => 'Zone Fluid Z',
			'zone_fvf_oil_p10' => 'Zone Fvf Oil P10',
			'zone_fvf_oil_p50' => 'Zone Fvf Oil P50',
			'zone_fvf_oil_p90' => 'Zone Fvf Oil P90',
			'zone_fvf_gas_p10' => 'Zone Fvf Gas P10',
			'zone_fvf_gas_p50' => 'Zone Fvf Gas P50',
			'zone_fvf_gas_p90' => 'Zone Fvf Gas P90',
			'zone_viscocity' => 'Zone Viscocity',
			'zone_name' => 'Zone Name',
			'zone_test' => 'Zone Test',
			'zone_test_date' => 'Zone Test Date',
			'zone_result' => 'Zone Result',
			'zone_area' => 'Zone Area',
			'zone_thickness' => 'Zone Thickness',
			'zone_depth' => 'Zone Depth',
			'zone_perforation' => 'Zone Perforation',
			'zone_well_test' => 'Zone Well Test',
			'zone_well_duration' => 'Zone Well Duration',
			'zone_initial_flow' => 'Zone Initial Flow',
			'zone_initial_shutin' => 'Zone Initial Shutin',
			'zone_tubing_size' => 'Zone Tubing Size',
			'zone_initial_temp' => 'Zone Initial Temp',
			'zone_initial_pressure' => 'Zone Initial Pressure',
			'zone_pseudostate' => 'Zone Pseudostate',
			'zone_well_formation' => 'Zone Well Formation',
			'zone_head' => 'Zone Head',
			'zone_pressure_wellbore' => 'Zone Pressure Wellbore',
			'zone_average_por' => 'Zone Average Por',
			'zone_water_cut' => 'Zone Water Cut',
			'zone_initial_water' => 'Zone Initial Water',
			'zone_low_gas' => 'Zone Low Gas',
			'zone_low_oil' => 'Zone Low Oil',
			'zone_free_water' => 'Zone Free Water',
			'zone_grv_gas' => 'Zone Grv Gas',
			'zone_grv_grv_oil' => 'Zone Grv Grv Oil',
			'zone_wellbore_coefficient' => 'Zone Wellbore Coefficient',
			'zone_wellbore_time' => 'Zone Wellbore Time',
			'zone_res_shape' => 'Zone Res Shape',
			'zone_prod_oil_choke' => 'Zone Prod Oil Choke',
			'zone_prod_oil_flow' => 'Zone Prod Oil Flow',
			'zone_prod_gas_choke' => 'Zone Prod Gas Choke',
			'zone_prod_gas_flow' => 'Zone Prod Gas Flow',
			'zone_prod_gasoil_ratio' => 'Zone Prod Gasoil Ratio',
			'zone_prod_conden_ratio' => 'Zone Prod Conden Ratio',
			'zone_prod_cumm_gas' => 'Zone Prod Cumm Gas',
			'zone_prod_cumm_oil' => 'Zone Prod Cumm Oil',
			'zone_prod_aof_bbl' => 'Zone Prod Aof Bbl',
			'zone_prod_aof_scf' => 'Zone Prod Aof Scf',
			'zone_prod_critical_bbl' => 'Zone Prod Critical Bbl',
			'zone_prod_critical_scf' => 'Zone Prod Critical Scf',
			'zone_prod_index_bbl' => 'Zone Prod Index Bbl',
			'zone_prod_index_scf' => 'Zone Prod Index Scf',
			'zone_prod_diffusity' => 'Zone Prod Diffusity',
			'zone_prod_permeability' => 'Zone Prod Permeability',
			'zone_prod_infinite' => 'Zone Prod Infinite',
			'zone_prod_well_radius' => 'Zone Prod Well Radius',
			'zone_prod_pseudostate' => 'Zone Prod Pseudostate',
			'zone_prod_res_radius' => 'Zone Prod Res Radius',
			'zone_prod_delta' => 'Zone Prod Delta',
			'zone_prod_wellbore' => 'Zone Prod Wellbore',
			'zone_prod_compres_rock' => 'Zone Prod Compres Rock',
			'zone_prod_compres_fluid' => 'Zone Prod Compres Fluid',
			'zone_prod_compres_total' => 'Zone Prod Compres Total',
			'zone_prod_second' => 'Zone Prod Second',
			'zone_prod_lambda' => 'Zone Prod Lambda',
			'zone_prod_omega' => 'Zone Prod Omega',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('wl_id',$this->wl_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('zone_hc_oil_show',$this->zone_hc_oil_show,true);
		$criteria->compare('zone_hc_oil_show_tools',$this->zone_hc_oil_show_tools,true);
		$criteria->compare('zone_hc_gas_show',$this->zone_hc_gas_show,true);
		$criteria->compare('zone_hc_gas_show_tools',$this->zone_hc_gas_show_tools,true);
		$criteria->compare('zone_hc_water_cut',$this->zone_hc_water_cut,true);
		$criteria->compare('zone_hc_water_gwc',$this->zone_hc_water_gwc,true);
		$criteria->compare('zone_hc_water_gwd_tools',$this->zone_hc_water_gwd_tools,true);
		$criteria->compare('zone_hc_water_owc',$this->zone_hc_water_owc,true);
		$criteria->compare('zone_hc_water_owc_tools',$this->zone_hc_water_owc_tools,true);
		$criteria->compare('zone_rock_method',$this->zone_rock_method,true);
		$criteria->compare('zone_rock_petro',$this->zone_rock_petro,true);
		$criteria->compare('zone_rock_petro_sample',$this->zone_rock_petro_sample,true);
		$criteria->compare('zone_rock_top_depth',$this->zone_rock_top_depth,true);
		$criteria->compare('zone_rock_bot_depth',$this->zone_rock_bot_depth,true);
		$criteria->compare('zone_rock_barrel',$this->zone_rock_barrel,true);
		$criteria->compare('zone_rock_barrel_equal',$this->zone_rock_barrel_equal,true);
		$criteria->compare('zone_rock_total_core',$this->zone_rock_total_core,true);
		$criteria->compare('zone_rock_preservative',$this->zone_rock_preservative,true);
		$criteria->compare('zone_rock_routine',$this->zone_rock_routine,true);
		$criteria->compare('zone_rock_routine_sample',$this->zone_rock_routine_sample,true);
		$criteria->compare('zone_rock_scal',$this->zone_rock_scal,true);
		$criteria->compare('zone_rock_scal_sample',$this->zone_rock_scal_sample,true);
		$criteria->compare('zone_clastic_p10_thickness',$this->zone_clastic_p10_thickness);
		$criteria->compare('zone_clastic_p50_thickness',$this->zone_clastic_p50_thickness);
		$criteria->compare('zone_clastic_p90_thickness',$this->zone_clastic_p90_thickness);
		$criteria->compare('zone_clastic_p10_gr',$this->zone_clastic_p10_gr);
		$criteria->compare('zone_clastic_p50_gr',$this->zone_clastic_p50_gr);
		$criteria->compare('zone_clastic_p90_gr',$this->zone_clastic_p90_gr);
		$criteria->compare('zone_clastic_p10_sp',$this->zone_clastic_p10_sp);
		$criteria->compare('zone_clastic_p50_sp',$this->zone_clastic_p50_sp);
		$criteria->compare('zone_clastic_p90_sp',$this->zone_clastic_p90_sp);
		$criteria->compare('zone_clastic_p10_net',$this->zone_clastic_p10_net);
		$criteria->compare('zone_clastic_p50_net',$this->zone_clastic_p50_net);
		$criteria->compare('zone_clastic_p90_net',$this->zone_clastic_p90_net);
		$criteria->compare('zone_clastic_p10_por',$this->zone_clastic_p10_por);
		$criteria->compare('zone_clastic_p50_por',$this->zone_clastic_p50_por);
		$criteria->compare('zone_clastic_p90_por',$this->zone_clastic_p90_por);
		$criteria->compare('zone_clastic_p10_satur',$this->zone_clastic_p10_satur);
		$criteria->compare('zone_clastic_p50_satur',$this->zone_clastic_p50_satur);
		$criteria->compare('zone_clastic_p90_satur',$this->zone_clastic_p90_satur);
		$criteria->compare('zone_carbo_p10_thickness',$this->zone_carbo_p10_thickness);
		$criteria->compare('zone_carbo_p50_thickness',$this->zone_carbo_p50_thickness);
		$criteria->compare('zone_carbo_p90_thickness',$this->zone_carbo_p90_thickness);
		$criteria->compare('zone_carbo_p10_dtc',$this->zone_carbo_p10_dtc);
		$criteria->compare('zone_carbo_p50_dtc',$this->zone_carbo_p50_dtc);
		$criteria->compare('zone_carbo_p90_dtc',$this->zone_carbo_p90_dtc);
		$criteria->compare('zone_carbo_p10_total',$this->zone_carbo_p10_total);
		$criteria->compare('zone_carbo_p50_total',$this->zone_carbo_p50_total);
		$criteria->compare('zone_carbo_p90_total',$this->zone_carbo_p90_total);
		$criteria->compare('zone_carbo_p10_net',$this->zone_carbo_p10_net);
		$criteria->compare('zone_carbo_p50_net',$this->zone_carbo_p50_net);
		$criteria->compare('zone_carbo_p90_net',$this->zone_carbo_p90_net);
		$criteria->compare('zone_carbo_p10_por',$this->zone_carbo_p10_por);
		$criteria->compare('zone_carbo_p50_por',$this->zone_carbo_p50_por);
		$criteria->compare('zone_carbo_p90_por',$this->zone_carbo_p90_por);
		$criteria->compare('zone_carbo_p10_satur',$this->zone_carbo_p10_satur);
		$criteria->compare('zone_carbo_p50_satur',$this->zone_carbo_p50_satur);
		$criteria->compare('zone_carbo_p90_satur',$this->zone_carbo_p90_satur);
		$criteria->compare('zone_fluid_date',$this->zone_fluid_date,true);
		$criteria->compare('zone_fluid_sample',$this->zone_fluid_sample,true);
		$criteria->compare('zone_fluid_ratio',$this->zone_fluid_ratio,true);
		$criteria->compare('zone_fluid_pressure',$this->zone_fluid_pressure,true);
		$criteria->compare('zone_fluid_temp',$this->zone_fluid_temp,true);
		$criteria->compare('zone_fluid_tubing',$this->zone_fluid_tubing,true);
		$criteria->compare('zone_fluid_casing',$this->zone_fluid_casing,true);
		$criteria->compare('zone_fluid_by',$this->zone_fluid_by,true);
		$criteria->compare('zone_fluid_report',$this->zone_fluid_report,true);
		$criteria->compare('zone_fluid_finger',$this->zone_fluid_finger,true);
		$criteria->compare('zone_fluid_grv_oil',$this->zone_fluid_grv_oil,true);
		$criteria->compare('zone_fluid_grv_gas',$this->zone_fluid_grv_gas,true);
		$criteria->compare('zone_fluid_grv_conden',$this->zone_fluid_grv_conden,true);
		$criteria->compare('zone_fluid_pvt',$this->zone_fluid_pvt,true);
		$criteria->compare('zone_fluid_quantity',$this->zone_fluid_quantity,true);
		$criteria->compare('zone_fluid_z',$this->zone_fluid_z,true);
		$criteria->compare('zone_fvf_oil_p10',$this->zone_fvf_oil_p10);
		$criteria->compare('zone_fvf_oil_p50',$this->zone_fvf_oil_p50);
		$criteria->compare('zone_fvf_oil_p90',$this->zone_fvf_oil_p90);
		$criteria->compare('zone_fvf_gas_p10',$this->zone_fvf_gas_p10);
		$criteria->compare('zone_fvf_gas_p50',$this->zone_fvf_gas_p50);
		$criteria->compare('zone_fvf_gas_p90',$this->zone_fvf_gas_p90);
		$criteria->compare('zone_viscocity',$this->zone_viscocity,true);
		$criteria->compare('zone_name',$this->zone_name,true);
		$criteria->compare('zone_test',$this->zone_test,true);
		$criteria->compare('zone_test_date',$this->zone_test_date,true);
		$criteria->compare('zone_result',$this->zone_result,true);
		$criteria->compare('zone_area',$this->zone_area,true);
		$criteria->compare('zone_thickness',$this->zone_thickness,true);
		$criteria->compare('zone_depth',$this->zone_depth,true);
		$criteria->compare('zone_perforation',$this->zone_perforation,true);
		$criteria->compare('zone_well_test',$this->zone_well_test,true);
		$criteria->compare('zone_well_duration',$this->zone_well_duration,true);
		$criteria->compare('zone_initial_flow',$this->zone_initial_flow,true);
		$criteria->compare('zone_initial_shutin',$this->zone_initial_shutin,true);
		$criteria->compare('zone_tubing_size',$this->zone_tubing_size,true);
		$criteria->compare('zone_initial_temp',$this->zone_initial_temp,true);
		$criteria->compare('zone_initial_pressure',$this->zone_initial_pressure,true);
		$criteria->compare('zone_pseudostate',$this->zone_pseudostate,true);
		$criteria->compare('zone_well_formation',$this->zone_well_formation,true);
		$criteria->compare('zone_head',$this->zone_head,true);
		$criteria->compare('zone_pressure_wellbore',$this->zone_pressure_wellbore,true);
		$criteria->compare('zone_average_por',$this->zone_average_por,true);
		$criteria->compare('zone_water_cut',$this->zone_water_cut,true);
		$criteria->compare('zone_initial_water',$this->zone_initial_water,true);
		$criteria->compare('zone_low_gas',$this->zone_low_gas,true);
		$criteria->compare('zone_low_oil',$this->zone_low_oil,true);
		$criteria->compare('zone_free_water',$this->zone_free_water,true);
		$criteria->compare('zone_grv_gas',$this->zone_grv_gas,true);
		$criteria->compare('zone_grv_grv_oil',$this->zone_grv_grv_oil,true);
		$criteria->compare('zone_wellbore_coefficient',$this->zone_wellbore_coefficient,true);
		$criteria->compare('zone_wellbore_time',$this->zone_wellbore_time,true);
		$criteria->compare('zone_res_shape',$this->zone_res_shape,true);
		$criteria->compare('zone_prod_oil_choke',$this->zone_prod_oil_choke,true);
		$criteria->compare('zone_prod_oil_flow',$this->zone_prod_oil_flow,true);
		$criteria->compare('zone_prod_gas_choke',$this->zone_prod_gas_choke,true);
		$criteria->compare('zone_prod_gas_flow',$this->zone_prod_gas_flow,true);
		$criteria->compare('zone_prod_gasoil_ratio',$this->zone_prod_gasoil_ratio,true);
		$criteria->compare('zone_prod_conden_ratio',$this->zone_prod_conden_ratio,true);
		$criteria->compare('zone_prod_cumm_gas',$this->zone_prod_cumm_gas,true);
		$criteria->compare('zone_prod_cumm_oil',$this->zone_prod_cumm_oil,true);
		$criteria->compare('zone_prod_aof_bbl',$this->zone_prod_aof_bbl,true);
		$criteria->compare('zone_prod_aof_scf',$this->zone_prod_aof_scf,true);
		$criteria->compare('zone_prod_critical_bbl',$this->zone_prod_critical_bbl,true);
		$criteria->compare('zone_prod_critical_scf',$this->zone_prod_critical_scf,true);
		$criteria->compare('zone_prod_index_bbl',$this->zone_prod_index_bbl,true);
		$criteria->compare('zone_prod_index_scf',$this->zone_prod_index_scf,true);
		$criteria->compare('zone_prod_diffusity',$this->zone_prod_diffusity,true);
		$criteria->compare('zone_prod_permeability',$this->zone_prod_permeability,true);
		$criteria->compare('zone_prod_infinite',$this->zone_prod_infinite,true);
		$criteria->compare('zone_prod_well_radius',$this->zone_prod_well_radius,true);
		$criteria->compare('zone_prod_pseudostate',$this->zone_prod_pseudostate,true);
		$criteria->compare('zone_prod_res_radius',$this->zone_prod_res_radius,true);
		$criteria->compare('zone_prod_delta',$this->zone_prod_delta,true);
		$criteria->compare('zone_prod_wellbore',$this->zone_prod_wellbore,true);
		$criteria->compare('zone_prod_compres_rock',$this->zone_prod_compres_rock,true);
		$criteria->compare('zone_prod_compres_fluid',$this->zone_prod_compres_fluid,true);
		$criteria->compare('zone_prod_compres_total',$this->zone_prod_compres_total,true);
		$criteria->compare('zone_prod_second',$this->zone_prod_second,true);
		$criteria->compare('zone_prod_lambda',$this->zone_prod_lambda,true);
		$criteria->compare('zone_prod_omega',$this->zone_prod_omega,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Wellzone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function totalZone($id_well)
	{
		$wellzone = Wellzone::model()->findAllByAttributes(array('wl_id'=>$id_well));
		return count($wellzone);
	}
	
	public function chkDependentWell($attribute)
	{
		if($attribute == 'zone_clastic_p10_thickness')
		{
			if($this->zone_clastic_p10_thickness != '')
			{
				if($this->zone_clastic_p10_thickness == 0 || $this->zone_clastic_p10_thickness <= $this->zone_clastic_p50_thickness || $this->zone_clastic_p10_thickness <= $this->zone_clastic_p90_thickness)
					$this->addError('zone_clastic_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater than Clastic P50 Gross Reservoir Thickness and Clastic P90 Gross Reservoir Thickness');
			}
		}
		
		if($attribute == 'zone_clastic_p50_thickness')
		{
			if($this->zone_clastic_p50_thickness != '')
			{
				if($this->zone_clastic_p50_thickness == 0 || $this->zone_clastic_p50_thickness <= $this->zone_clastic_p90_thickness)
					$this->addError('zone_clastic_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater than Clastic P90 Gross Reservoir Thickness');
			}
		}
		
		if($attribute == 'zone_clastic_p10_gr')
		{
			if($this->zone_clastic_p10_gr != '')
			{
				if($this->zone_clastic_p10_gr == 0 || $this->zone_clastic_p10_gr <= $this->zone_clastic_p50_gr || $this->zone_clastic_p10_gr <= $this->zone_clastic_p90_gr)
					$this->addError('zone_clastic_p10_gr', $this->getAttributeLabel($attribute) . ' must greater than Clastic P50 Reservoir Vshale Content (GR Log) and Clastic P90 Reservoir Vshale Content (GR Log)');
			}
		}
		
		if($attribute == 'zone_clastic_p50_gr')
		{
			if($this->zone_clastic_p50_gr != '')
			{
				if($this->zone_clastic_p50_gr == 0 || $this->zone_clastic_p50_gr <= $this->zone_clastic_p90_gr)
					$this->addError('zone_clastic_p50_gr', $this->getAttributeLabel($attribute) . ' must greater than Clastic P90 Reservoir Vshale Content (GR Log)');
			}
		}
		
		if($attribute == 'zone_clastic_p10_sp')
		{
			if($this->zone_clastic_p10_sp != '') {
				if($this->zone_clastic_p10_sp == 0 || $this->zone_clastic_p10_sp <= $this->zone_clastic_p50_sp || $this->zone_clastic_p10_sp <= $this->zone_clastic_p90_sp)
					$this->addError('zone_clastic_p10_sp', $this->getAttributeLabel($attribute) . ' must greater than Clastic P50 Reservoir Vshale Content (SP Log) and Clastic P90 Reservoir Vshale Content (SP Log)');
			}
		}
			
		if($attribute == 'zone_clastic_p50_sp')
		{
			if($this->zone_clastic_p50_sp != '') {
				if($this->zone_clastic_p50_sp == 0 || $this->zone_clastic_p50_sp <= $this->zone_clastic_p90_sp)
					$this->addError('zone_clastic_p50_sp', $this->getAttributeLabel($attribute) . ' must greater than Clastic P90 Reservoir Vshale Content (SP Log)');
			}
		}
		
		if($attribute == 'zone_clastic_p10_net')
		{
			if($this->zone_clastic_p10_net != '') {
				if($this->zone_clastic_p10_net == 0 || $this->zone_clastic_p10_net <= $this->zone_clastic_p50_net || $this->zone_clastic_p10_net <= $this->zone_clastic_p90_net)
					$this->addError('zone_clastic_p10_net', $this->getAttributeLabel($attribute) . ' must greater than Clastic P50 Net to Gross and Clastic P90 Net to Gross');
			}
		}
		
		if($attribute == 'zone_clastic_p50_net')
		{
			if($this->zone_clastic_p50_net != '') {
				if($this->zone_clastic_p50_net == 0 || $this->zone_clastic_p50_net <= $this->zone_clastic_p90_net)
					$this->addError('zone_clastic_p50_net', $this->getAttributeLabel($attribute) . ' must greater than Clastic P90 Net to Gross');
			}
		}
		
		if($attribute == 'zone_clastic_p10_por')
		{
			if($this->zone_clastic_p10_por != '') {
				if($this->zone_clastic_p10_por == 0 || $this->zone_clastic_p10_por <= $this->zone_clastic_p50_por || $this->zone_clastic_p10_por <= $this->zone_clastic_p90_por)
					$this->addError('zone_clastic_p10_por', $this->getAttributeLabel($attribute) . ' must greater than Clastic P50 Reservoir Porosity and Clastic P90 Reservoir Porosity');
			}
		}
		
		if($attribute == 'zone_clastic_p50_por')
		{
			if($this->zone_clastic_p50_por != '') {
				if($this->zone_clastic_p50_por == 0 || $this->zone_clastic_p50_por <= $this->zone_clastic_p90_por)
					$this->addError('zone_clastic_p50_por', $this->getAttributeLabel($attribute) . ' must greater than Clastic P90 Reservoir Porosity');
			}
		}
		
		if($attribute == 'zone_clastic_p10_satur')
		{
			if($this->zone_clastic_p10_satur != '') {
				if($this->zone_clastic_p10_satur == 0 || $this->zone_clastic_p10_satur <= $this->zone_clastic_p50_satur || $this->zone_clastic_p10_satur <= $this->zone_clastic_p90_satur)
					$this->addError('zone_clastic_p10_satur', $this->getAttributeLabel($attribute) . ' must greater than Clastic P50 HC Saturation (1-Sw) and Clastic P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'zone_clastic_p50_satur')
		{
			if($this->zone_clastic_p50_satur != '') {
				if($this->zone_clastic_p50_satur == 0 || $this->zone_clastic_p50_satur <= $this->zone_clastic_p90_satur)
					$this->addError('zone_clastic_p50_satur', $this->getAttributeLabel($attribute) . ' must greater than Clastic P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'zone_carbo_p10_thickness')
		{
			if($this->zone_carbo_p10_thickness != '') {
				if($this->zone_carbo_p10_thickness == 0 || $this->zone_carbo_p10_thickness <= $this->zone_carbo_p50_thickness || $this->zone_carbo_p10_thickness <= $this->zone_carbo_p90_thickness)
					$this->addError('zone_carbo_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P50 Gross Reservoir Thickness and Carbonate P90 Gross Reservoir Thickness');
			}
		}
			
		if($attribute == 'zone_carbo_p50_thickness')
		{
			if($this->zone_carbo_p50_thickness != '') {
				if($this->zone_carbo_p50_thickness == 0 || $this->zone_carbo_p50_thickness <= $this->zone_carbo_p90_thickness)
					$this->addError('zone_carbo_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P90 Gross Reservoir Thickness');
			}
		}
		
		if($attribute == 'zone_carbo_p10_dtc')
		{
			if($this->zone_carbo_p10_dtc != '') {
				if($this->zone_carbo_p10_dtc == 0 || $this->zone_carbo_p10_dtc <= $this->zone_carbo_p50_dtc || $this->zone_carbo_p10_dtc <= $this->zone_carbo_p90_dtc)
					$this->addError('zone_carbo_p10_dtc', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P50 Thickness Reservoir Pore Throat Connectivity (DTC) and Carbonate P90 Thickness Reservoir Pore Throat Connectivity (DTC)');
			}
		}
		
		if($attribute == 'zone_carbo_p50_dtc')
		{
			if($this->zone_carbo_p50_dtc != '') {
				if($this->zone_carbo_p50_dtc == 0 || $this->zone_carbo_p50_dtc <= $this->zone_carbo_p90_dtc)
					$this->addError('zone_carbo_p50_dtc', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P90 Thickness Reservoir Pore Throat Connectivity (DTC)');
			}
		}
		
		if($attribute == 'zone_carbo_p10_total')
		{
			if($this->zone_carbo_p10_total != '') {
				if($this->zone_carbo_p10_total == 0 || $this->zone_carbo_p10_total <= $this->zone_carbo_p50_total || $this->zone_carbo_p10_total <= $this->zone_carbo_p90_total)
					$this->addError('zone_carbo_p10_total', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P50 Thickness Reservoir Total Pore and Carbonate P90 Thickness Reservoir Total Pore');
			}
		}
			
		if($attribute == 'zone_carbo_p50_total')
		{
			if($this->zone_carbo_p50_total != '') {
				if($this->zone_carbo_p50_total == 0 || $this->zone_carbo_p50_total <= $this->zone_carbo_p90_total)
					$this->addError('zone_carbo_p50_total', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P90 Thickness Reservoir Total Pore');
			}
		}
		
		if($attribute == 'zone_carbo_p10_net')
		{
			if($this->zone_carbo_p10_net != '') {
				if($this->zone_carbo_p10_net == 0 || $this->zone_carbo_p10_net <= $this->zone_carbo_p50_net || $this->zone_carbo_p10_net <= $this->zone_carbo_p90_net)
					$this->addError('zone_carbo_p10_net', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P50 Net to Gross and Carbonate P90 Net to Gross');
			}
		}
		
		if($attribute == 'zone_carbo_p50_net')
		{
			if($this->zone_carbo_p50_net != '') {
				if($this->zone_carbo_p50_net == 0 || $this->zone_carbo_p50_net <= $this->zone_carbo_p90_net)
					$this->addError('zone_carbo_p50_net', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P90 Net to Gross');
			}
		}
		
		if($attribute == 'zone_carbo_p10_por')
		{
			if($this->zone_carbo_p10_por != '') {
				if($this->zone_carbo_p10_por == 0 || $this->zone_carbo_p10_por <= $this->zone_carbo_p50_por || $this->zone_carbo_p10_por <= $this->zone_carbo_p90_por)
					$this->addError('zone_carbo_p10_por', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P50 Reservoir Porosity and Carbonate P90 Reservoir Porosity');
			}
		}
			
		if($attribute == 'zone_carbo_p50_por')
		{
			if($this->zone_carbo_p50_por != '') {
				if($this->zone_carbo_p50_por == 0 || $this->zone_carbo_p50_por <= $this->zone_carbo_p90_por)
					$this->addError('zone_carbo_p50_por', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P90 Reservoir Porosity');
			}
		}
		
		if($attribute == 'zone_carbo_p10_satur')
		{
			if($this->zone_carbo_p10_satur != '') {
				if($this->zone_carbo_p10_satur == 0 || $this->zone_carbo_p10_satur <= $this->zone_carbo_p50_satur || $this->zone_carbo_p10_satur <= $this->zone_carbo_p90_satur)
					$this->addError('zone_carbo_p10_satur', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P50 HC Saturation (1-Sw) and Carbonate P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'zone_carbo_p50_satur')
		{
			if($this->zone_carbo_p50_satur != '') {
				if($this->zone_carbo_p50_satur == 0 || $this->zone_carbo_p50_satur <= $this->zone_carbo_p90_satur)
					$this->addError('zone_carbo_p50_satur', $this->getAttributeLabel($attribute) . ' must greater than Carbonate P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'zone_fvf_oil_p10')
		{
			if($this->zone_fvf_oil_p10 != '') {
				if($this->zone_fvf_oil_p10 == 0 || $this->zone_fvf_oil_p10 <= $this->zone_fvf_oil_p50 || $this->zone_fvf_oil_p10 <= $this->zone_fvf_oil_p90)
					$this->addError('zone_fvf_oil_p10', $this->getAttributeLabel($attribute) . ' must greater than Initial Formation Volume Factor P50 Oil (Boi) and Initial Formation Volume Factor P90 Oil (Boi)');
			}
		}
		
		if($attribute == 'zone_fvf_oil_p50')
		{
			if($this->zone_fvf_oil_p50 != '') {
				if($this->zone_fvf_oil_p50 == 0 || $this->zone_fvf_oil_p50 <= $this->zone_fvf_oil_p90)
					$this->addError('zone_fvf_oil_p50', $this->getAttributeLabel($attribute) . ' must greater than Initial Formation Volume Factor P90 Oil (Boi)');
			}
		}
		
		if($attribute == 'zone_fvf_gas_p10')
		{
			if($this->zone_fvf_gas_p10 != '') {
				if($this->zone_fvf_gas_p10 == 0 || $this->zone_fvf_gas_p10 <= $this->zone_fvf_gas_p50 || $this->zone_fvf_gas_p10 <= $this->zone_fvf_gas_p90)
					$this->addError('zone_fvf_gas_p10', $this->getAttributeLabel($attribute) . ' must greater than Initial Formation Volume Factor P50 Gas (Bgi) and Initial Formation Volume Factor P90 Gas (Bgi)');
			}
		}
		
		if($attribute == 'zone_fvf_gas_p50')
		{
			if($this->zone_fvf_gas_p50 != '') {
				if($this->zone_fvf_gas_p50 == 0 || $this->zone_fvf_gas_p50 <= $this->zone_fvf_gas_p90)
					$this->addError('zone_fvf_gas_p50', $this->getAttributeLabel($attribute) . ' must greater than Initial Formation Volume Factor P90 Gas (Bgi)');
			}
		}
		
	}
	
	public function decimalFluid()
	{
		if($this->zone_fluid_z != '')
		{
			$numerical = new CNumberValidator;
			$numerical->attributes = array('zone_fluid_z');
			$numerical->min = 0.1;
			$numerical->max = 2.0;
			$numerical->validate($this);
			$error  = $this->getErrors('zone_fluid_z');
			if(count($error) == 0 ) {
				Yii::import('ext.validasi.FluidZPattern');
				$checking = new FluidZPattern;
				$checking->attributes = array('zone_fluid_z');
				$checking->validate($this);
			}
		}
	}
	
}
