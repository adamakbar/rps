<?php

/**
 * This is the model class for table "rsc_seismic_3d".
 *
 * The followings are the available columns in table 'rsc_seismic_3d':
 * @property integer $s3d_id
 * @property integer $prospect_id
 * @property string $s3d_vintage_number
 * @property string $s3d_year_survey
 * @property string $s3d_bin_size
 * @property string $s3d_coverage_area
 * @property string $s3d_frequency
 * @property string $s3d_frequency_lateral
 * @property string $s3d_frequency_vertical
 * @property string $s3d_year_late_process
 * @property string $s3d_late_method
 * @property string $s3d_img_quality
 * @property string $s3d_top_depth_ft
 * @property string $s3d_top_depth_ms
 * @property string $s3d_bot_depth_ft
 * @property string $s3d_bot_depth_ms
 * @property string $s3d_depth_spill
 * @property string $s3d_ability_offset
 * @property string $s3d_ability_rock
 * @property string $s3d_ability_fluid
 * @property string $s3d_depth_estimate
 * @property string $s3d_depth_estimate_analog
 * @property string $s3d_depth_estimate_spill
 * @property string $s3d_depth_low
 * @property string $s3d_depth_low_analog
 * @property string $s3d_depth_low_spill
 * @property string $s3d_formation_thickness
 * @property string $s3d_gross_thickness
 * @property string $s3d_avail_pay
 * @property double $s3d_net_p90_thickness
 * @property double $s3d_net_p90_vsh
 * @property double $s3d_net_p90_por
 * @property double $s3d_net_p90_satur
 * @property double $s3d_net_p50_thickness
 * @property double $s3d_net_p50_vsh
 * @property double $s3d_net_p50_por
 * @property double $s3d_net_p50_satur
 * @property double $s3d_net_p10_thickness
 * @property double $s3d_net_p10_vsh
 * @property double $s3d_net_p10_por
 * @property double $s3d_net_p10_satur
 * @property double $s3d_vol_p90_area
 * @property double $s3d_vol_p50_area
 * @property double $s3d_vol_p10_area
 * @property string $s3d_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Seismic3d extends CActiveRecord
{
	public $is_checked;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_seismic_3d';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBox3dSeismic'),
// 			array('s3d_year_survey, s3d_gross_thickness, s3d_net_p90_thickness, s3d_net_p50_thickness, s3d_net_p10_thickness, s3d_vol_p90_area, s3d_vol_p50_area, s3d_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				's3d_year_survey, s3d_year_late_process',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				's3d_bin_size, s3d_coverage_area, s3d_frequency, s3d_frequency_lateral, s3d_frequency_vertical,
				s3d_top_depth_ft, s3d_top_depth_ms, s3d_bot_depth_ft, s3d_bot_depth_ms, s3d_depth_spill,
				s3d_depth_estimate, s3d_depth_low, s3d_formation_thickness, s3d_gross_thickness,
				s3d_net_p90_thickness, s3d_net_p50_thickness, s3d_net_p10_thickness,
				s3d_vol_p90_area, s3d_vol_p50_area, s3d_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				's3d_depth_estimate_spill, s3d_depth_low_spill, s3d_net_p90_por, s3d_net_p50_por, s3d_net_p10_por, s3d_net_p90_vsh, s3d_net_p50_vsh, s3d_net_p10_vsh, s3d_net_p90_satur, s3d_net_p50_satur, s3d_net_p10_satur',
				'numerical',
				'min'=>0,
				'max'=>100,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('s3d_vintage_number, s3d_year_survey, s3d_bin_size, s3d_coverage_area, s3d_frequency, s3d_frequency_lateral, s3d_frequency_vertical, s3d_year_late_process, s3d_late_method, s3d_img_quality, s3d_top_depth_ft, s3d_top_depth_ms, s3d_bot_depth_ft, s3d_bot_depth_ms, s3d_depth_spill, s3d_ability_offset, s3d_ability_rock, s3d_ability_fluid, s3d_depth_estimate, s3d_depth_estimate_analog, s3d_depth_estimate_spill, s3d_depth_low, s3d_depth_low_analog, s3d_depth_low_spill, s3d_formation_thickness, s3d_gross_thickness, s3d_avail_pay', 'length', 'max'=>50),
			array('s3d_remark', 'safe'),
// 			array('s3d_net_p90_thickness, s3d_net_p50_thickness, s3d_net_p10_thickness, s3d_net_p90_por, s3d_net_p50_por, s3d_net_p10_por, s3d_net_p90_vsh, s3d_net_p50_vsh, s3d_net_p10_vsh, s3d_net_p90_satur, s3d_net_p50_satur, s3d_net_p10_satur, s3d_vol_p90_area, s3d_vol_p50_area, s3d_vol_p10_area', 'chkDependent3dSeismic'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('s3d_id, prospect_id, s3d_vintage_number, s3d_year_survey, s3d_bin_size, s3d_coverage_area, s3d_frequency, s3d_frequency_lateral, s3d_frequency_vertical, s3d_year_late_process, s3d_late_method, s3d_img_quality, s3d_top_depth_ft, s3d_top_depth_ms, s3d_bot_depth_ft, s3d_bot_depth_ms, s3d_depth_spill, s3d_ability_offset, s3d_ability_rock, s3d_ability_fluid, s3d_depth_estimate, s3d_depth_estimate_analog, s3d_depth_estimate_spill, s3d_depth_low, s3d_depth_low_analog, s3d_depth_low_spill, s3d_formation_thickness, s3d_gross_thickness, s3d_avail_pay, s3d_net_p90_thickness, s3d_net_p90_vsh, s3d_net_p90_por, s3d_net_p90_satur, s3d_net_p50_thickness, s3d_net_p50_vsh, s3d_net_p50_por, s3d_net_p50_satur, s3d_net_p10_thickness, s3d_net_p10_vsh, s3d_net_p10_por, s3d_net_p10_satur, s3d_vol_p90_area, s3d_vol_p50_area, s3d_vol_p10_area, s3d_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			's3d_id' => 'S3d',
			'prospect_id' => 'Prospect',
			's3d_vintage_number' => 'S3d Vintage Number',
			's3d_year_survey' => '3d Seismic Acquisition Year',
			's3d_bin_size' => 'S3d Bin Size',
			's3d_coverage_area' => 'S3d Coverage Area',
			's3d_frequency' => 'S3d Frequency',
			's3d_frequency_lateral' => 'S3d Frequency Lateral',
			's3d_frequency_vertical' => 'S3d Frequency Vertical',
			's3d_year_late_process' => 'S3d Year Late Process',
			's3d_late_method' => 'S3d Late Method',
			's3d_img_quality' => 'S3d Img Quality',
			's3d_top_depth_ft' => 'S3d Top Depth Ft',
			's3d_top_depth_ms' => 'S3d Top Depth Ms',
			's3d_bot_depth_ft' => 'S3d Bot Depth Ft',
			's3d_bot_depth_ms' => 'S3d Bot Depth Ms',
			's3d_depth_spill' => 'S3d Depth Spill',
			's3d_ability_offset' => 'S3d Ability Offset',
			's3d_ability_rock' => 'S3d Ability Rock',
			's3d_ability_fluid' => 'S3d Ability Fluid',
			's3d_depth_estimate' => 'S3d Depth Estimate',
			's3d_depth_estimate_analog' => 'S3d Depth Estimate Analog',
			's3d_depth_estimate_spill' => 'S3d Depth Estimate Spill',
			's3d_depth_low' => 'S3d Depth Low',
			's3d_depth_low_analog' => 'S3d Depth Low Analog',
			's3d_depth_low_spill' => 'S3d Depth Low Spill',
			's3d_formation_thickness' => 'S3d Formation Thickness',
			's3d_gross_thickness' => '3d Seismic Grass Sand or Reservoir Thickness',
			's3d_avail_pay' => 'S3d Avail Pay',
			's3d_net_p90_thickness' => '3d Seismic P90 Thickness Net Pay',
			's3d_net_p90_vsh' => '3d Seismic P90 Vsh Cut Off',
			's3d_net_p90_por' => '3d Seismic P90 Porosity Cut Off',
			's3d_net_p90_satur' => '3d Seismic P90 Saturation Cut Off',
			's3d_net_p50_thickness' => '3d Seismic P50 Thickness Net Pay',
			's3d_net_p50_vsh' => '3d Seismic P50 Vsh Cut Off',
			's3d_net_p50_por' => '3d Seismic P50 Porosity Cut Off',
			's3d_net_p50_satur' => '3d Seismic P50 Saturation Cut Off',
			's3d_net_p10_thickness' => '3d Seismic P10 Thickness Net Pay',
			's3d_net_p10_vsh' => '3d Seismic P10 Vsh Cut Off',
			's3d_net_p10_por' => '3d Seismic P10 Porosity Cut Off',
			's3d_net_p10_satur' => '3d Seismic P10 Saturation Cut Off',
			's3d_vol_p90_area' => '3d Seismic P90 Areal Closure Estimation',
			's3d_vol_p50_area' => '3d Seismic P50 Areal Closure Estimation',
			's3d_vol_p10_area' => '3d Seismic P10 Areal Closure Estimation',
			's3d_remark' => 'S3d Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('s3d_id',$this->s3d_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('s3d_vintage_number',$this->s3d_vintage_number,true);
		$criteria->compare('s3d_year_survey',$this->s3d_year_survey,true);
		$criteria->compare('s3d_bin_size',$this->s3d_bin_size,true);
		$criteria->compare('s3d_coverage_area',$this->s3d_coverage_area,true);
		$criteria->compare('s3d_frequency',$this->s3d_frequency,true);
		$criteria->compare('s3d_frequency_lateral',$this->s3d_frequency_lateral,true);
		$criteria->compare('s3d_frequency_vertical',$this->s3d_frequency_vertical,true);
		$criteria->compare('s3d_year_late_process',$this->s3d_year_late_process,true);
		$criteria->compare('s3d_late_method',$this->s3d_late_method,true);
		$criteria->compare('s3d_img_quality',$this->s3d_img_quality,true);
		$criteria->compare('s3d_top_depth_ft',$this->s3d_top_depth_ft,true);
		$criteria->compare('s3d_top_depth_ms',$this->s3d_top_depth_ms,true);
		$criteria->compare('s3d_bot_depth_ft',$this->s3d_bot_depth_ft,true);
		$criteria->compare('s3d_bot_depth_ms',$this->s3d_bot_depth_ms,true);
		$criteria->compare('s3d_depth_spill',$this->s3d_depth_spill,true);
		$criteria->compare('s3d_ability_offset',$this->s3d_ability_offset,true);
		$criteria->compare('s3d_ability_rock',$this->s3d_ability_rock,true);
		$criteria->compare('s3d_ability_fluid',$this->s3d_ability_fluid,true);
		$criteria->compare('s3d_depth_estimate',$this->s3d_depth_estimate,true);
		$criteria->compare('s3d_depth_estimate_analog',$this->s3d_depth_estimate_analog,true);
		$criteria->compare('s3d_depth_estimate_spill',$this->s3d_depth_estimate_spill,true);
		$criteria->compare('s3d_depth_low',$this->s3d_depth_low,true);
		$criteria->compare('s3d_depth_low_analog',$this->s3d_depth_low_analog,true);
		$criteria->compare('s3d_depth_low_spill',$this->s3d_depth_low_spill,true);
		$criteria->compare('s3d_formation_thickness',$this->s3d_formation_thickness,true);
		$criteria->compare('s3d_gross_thickness',$this->s3d_gross_thickness,true);
		$criteria->compare('s3d_avail_pay',$this->s3d_avail_pay,true);
		$criteria->compare('s3d_net_p90_thickness',$this->s3d_net_p90_thickness);
		$criteria->compare('s3d_net_p90_vsh',$this->s3d_net_p90_vsh);
		$criteria->compare('s3d_net_p90_por',$this->s3d_net_p90_por);
		$criteria->compare('s3d_net_p90_satur',$this->s3d_net_p90_satur);
		$criteria->compare('s3d_net_p50_thickness',$this->s3d_net_p50_thickness);
		$criteria->compare('s3d_net_p50_vsh',$this->s3d_net_p50_vsh);
		$criteria->compare('s3d_net_p50_por',$this->s3d_net_p50_por);
		$criteria->compare('s3d_net_p50_satur',$this->s3d_net_p50_satur);
		$criteria->compare('s3d_net_p10_thickness',$this->s3d_net_p10_thickness);
		$criteria->compare('s3d_net_p10_vsh',$this->s3d_net_p10_vsh);
		$criteria->compare('s3d_net_p10_por',$this->s3d_net_p10_por);
		$criteria->compare('s3d_net_p10_satur',$this->s3d_net_p10_satur);
		$criteria->compare('s3d_vol_p90_area',$this->s3d_vol_p90_area);
		$criteria->compare('s3d_vol_p50_area',$this->s3d_vol_p50_area);
		$criteria->compare('s3d_vol_p10_area',$this->s3d_vol_p10_area);
		$criteria->compare('s3d_remark',$this->s3d_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Seismic3d the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependent3dSeismic($attribute)
// 	{
// 		if($attribute == 's3d_net_p10_thickness')
// 		{
// 			if($this->s3d_net_p10_thickness != '') {
// 				if($this->s3d_net_p10_thickness == 0 || $this->s3d_net_p10_thickness <= $this->s3d_net_p50_thickness || $this->s3d_net_p10_thickness <= $this->s3d_net_p90_thickness)
// 					$this->addError('s3d_net_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P50 Thickness Net Pay and 3d Seismic P90 Thickness Net Pay');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p50_thickness')
// 		{
// 			if($this->s3d_net_p50_thickness != '') {
// 				if($this->s3d_net_p50_thickness == 0 || $this->s3d_net_p50_thickness <= $this->s3d_net_p90_thickness)
// 					$this->addError('s3d_net_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P90 Thickness Net Pay');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p10_por')
// 		{
// 			if($this->s3d_net_p10_por != '') {
// 				if($this->s3d_net_p10_por == 0 || $this->s3d_net_p10_por <= $this->s3d_net_p50_por || $this->s3d_net_p10_por <= $this->s3d_net_p90_por)
// 					$this->addError('s3d_net_p10_por', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P50 Porosity Cut Off and 3d Seismic P90 Porosity Cut Off');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p50_por')
// 		{
// 			if($this->s3d_net_p50_por != '') {
// 				if($this->s3d_net_p50_por == 0 || $this->s3d_net_p50_por <= $this->s3d_net_p90_por)
// 					$this->addError('s3d_net_p50_por', $this->getAttributeLabel($attribute) . ' must greater 3d Seismic P90 Porosity Cut Off');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p10_vsh')
// 		{
// 			if($this->s3d_net_p10_vsh != '') {
// 				if($this->s3d_net_p10_vsh == 0 || $this->s3d_net_p10_vsh <= $this->s3d_net_p50_vsh || $this->s3d_net_p10_vsh <= $this->s3d_net_p90_vsh)
// 					$this->addError('s3d_net_p10_vsh', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P50 Vsh Cut Off and 3d Seismic P90 Vsh Cut Off');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p50_vsh')
// 		{
// 			if($this->s3d_net_p50_vsh != '') {
// 				if($this->s3d_net_p50_vsh == 0 || $this->s3d_net_p50_vsh <= $this->s3d_net_p90_vsh)
// 					$this->addError('s3d_net_p50_vsh', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P90 Vsh Cut Off');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p10_satur')
// 		{
// 			if($this->s3d_net_p10_satur != '') {
// 				if($this->s3d_net_p10_satur == 0 || $this->s3d_net_p10_satur <= $this->s3d_net_p50_satur || $this->s3d_net_p10_satur <= $this->s3d_net_p90_satur)
// 					$this->addError('s3d_net_p10_satur', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P50 Saturation Cut Off and 3d Seismic P90 Saturation Cut Off');
// 			}
// 		}
		
// 		if($attribute == 's3d_net_p50_satur')
// 		{
// 			if($this->s3d_net_p50_satur != '') {
// 				if($this->s3d_net_p50_satur == 0 || $this->s3d_net_p50_satur <= $this->s3d_net_p90_satur)
// 					$this->addError('s3d_net_p50_satur', $this->getAttributeLabel($attribute) . ' must greater 3d Seismic P90 Saturation Cut Off');
// 			}
// 		}
		
// 		if($attribute == 's3d_vol_p10_area')
// 		{
// 			if($this->s3d_vol_p10_area != '') {
// 				if($this->s3d_vol_p10_area == 0 || $this->s3d_vol_p10_area <= $this->s3d_vol_p50_area || $this->s3d_vol_p10_area <= $this->s3d_vol_p90_area)
// 					$this->addError('s3d_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P50 Areal Closure Estimation and 3d Seismic P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 's3d_vol_p50_area')
// 		{
// 			if($this->s3d_vol_p50_area != '') {
// 				if($this->s3d_vol_p50_area == 0 || $this->s3d_vol_p50_area <= $this->s3d_vol_p90_area)
// 					$this->addError('s3d_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than 3d Seismic P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBox3dSeismic()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('s3d_year_survey', 's3d_gross_thickness', 's3d_net_p90_thickness', 's3d_net_p50_thickness', 's3d_net_p10_thickness', 's3d_vol_p90_area', 's3d_vol_p50_area', 's3d_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.Dependent3dSeismic');
			$checking = new Dependent3dSeismic;
			$checking->attributes = array('s3d_net_p90_thickness', 's3d_net_p50_thickness', 's3d_net_p10_thickness', 's3d_net_p90_por', 's3d_net_p50_por', 's3d_net_p10_por', 's3d_net_p90_vsh', 's3d_net_p50_vsh', 's3d_net_p10_vsh', 's3d_net_p90_satur', 's3d_net_p50_satur', 's3d_net_p10_satur', 's3d_vol_p90_area', 's3d_vol_p50_area', 's3d_vol_p10_area');
			$checking->validate($this);
		}
	}
}
