<?php

/**
 * This is the model class for table "rsc_lead_geological".
 *
 * The followings are the available columns in table 'rsc_lead_geological':
 * @property integer $lsgf_id
 * @property integer $lead_id
 * @property string $lsgf_year_survey
 * @property string $lsgf_survey_method
 * @property string $lsgf_coverage_area
 * @property double $lsgf_low_estimate
 * @property double $lsgf_best_estimate
 * @property double $lsgf_high_estimate
 * @property string $lsgf_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class LeadGeological extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_geological';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLeadGeological'),
// 			array('lsgf_low_estimate, lsgf_best_estimate, lsgf_high_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'lsgf_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'lsgf_coverage_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('lsgf_low_estimate, lsgf_best_estimate, lsgf_high_estimate', 'numerical'),
			array('lsgf_year_survey, lsgf_survey_method, lsgf_coverage_area', 'length', 'max'=>50),
			array('lsgf_remark', 'safe'),
// 			array('lsgf_low_estimate, lsgf_best_estimate, lsgf_high_estimate', 'chkDependentAreaGeological'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lsgf_id, lead_id, lsgf_year_survey, lsgf_survey_method, lsgf_coverage_area, lsgf_low_estimate, lsgf_best_estimate, lsgf_high_estimate, lsgf_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lsgf_id' => 'Lsgf',
			'lead_id' => 'Lead',
			'lsgf_year_survey' => 'Geological Acquisition Year',
			'lsgf_survey_method' => 'Lsgf Survey Method',
			'lsgf_coverage_area' => 'Lsgf Coverage Area',
			'lsgf_low_estimate' => 'Geological Low Estimate',
			'lsgf_best_estimate' => 'Geological Best Estimate',
			'lsgf_high_estimate' => 'Geological High Estimate',
			'lsgf_remark' => 'Lsgf Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lsgf_id',$this->lsgf_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('lsgf_year_survey',$this->lsgf_year_survey,true);
		$criteria->compare('lsgf_survey_method',$this->lsgf_survey_method,true);
		$criteria->compare('lsgf_coverage_area',$this->lsgf_coverage_area,true);
		$criteria->compare('lsgf_low_estimate',$this->lsgf_low_estimate);
		$criteria->compare('lsgf_best_estimate',$this->lsgf_best_estimate);
		$criteria->compare('lsgf_high_estimate',$this->lsgf_high_estimate);
		$criteria->compare('lsgf_remark',$this->lsgf_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeadGeological the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkdDependentAreaGeological($attribute)
// 	{
// 		if($attribute == 'lsgf_high_estimate')
// 		{
// 			if($this->lsgf_high_estimate == 0 || $this->lsgf_high_estimate <= $this->lsgf_best_estimate || $this->lsgf_high_estimate <= $this->lsgf_low_estimate)
// 				$this->addError($this->lsgf_high_estimate, $this->getAttributeLabel($attribute) . ' must greater than Geological Best Estimate and Geological Low Estimate');
// 		} 
		
// 		if($attribute == 'lsgf_best_estimate')
// 		{
// 			if($this->lsgf_best_estimate == 0 || $this->lsgf_best_estimate <= $this->lsgf_low_estimate)
// 				$this->addError($this->lsgf_best_estimate, $this->getAttributeLabel($attribute) . ' must greater than Geological Low Estimate');
// 		}
// 	}
	
	public function chkBoxLeadGeological()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('lsgf_year_survey', 'lsgf_low_estimate', 'lsgf_best_estimate', 'lsgf_high_estimate');
			$required->validate($this);
			
			Yii::import('ext.validasi.DependentAreaGeological');
			$checking = new DependentAreaGeological;
			$checking->attributes = array('lsgf_low_estimate', 'lsgf_best_estimate', 'lsgf_high_estimate');
			$checking->validate($this);
		}
	}
}
