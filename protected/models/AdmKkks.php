<?php

/**
 * This is the model class for table "adm_kkks".
 *
 * The followings are the available columns in table 'adm_kkks':
 * @property integer $kkks_id
 * @property integer $holding_id
 * @property string $kkks_name
 * @property string $kkks_business_entity
 * @property integer $kkks_is_operating
 * @property string $kkks_address
 * @property string $kkks_office_phone
 *
 * The followings are the available model relations:
 * @property AdmHolding $holding
 * @property AdmParticipant[] $admParticipants
 */
class AdmKkks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_kkks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('holding_id, kkks_is_operating', 'numerical', 'integerOnly'=>true),
			array('kkks_name', 'length', 'max'=>254),
			array('kkks_business_entity, kkks_office_phone', 'length', 'max'=>50),
			array('kkks_address', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kkks_id, holding_id, kkks_name, kkks_business_entity, kkks_is_operating, kkks_address, kkks_office_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'holding' => array(self::BELONGS_TO, 'AdmHolding', 'holding_id'),
			'admParticipants' => array(self::HAS_MANY, 'AdmParticipant', 'kkks_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kkks_id' => 'Kkks',
			'holding_id' => 'Holding',
			'kkks_name' => 'Kkks Name',
			'kkks_business_entity' => 'Kkks Business Entity',
			'kkks_is_operating' => 'Kkks Is Operating',
			'kkks_address' => 'Kkks Address',
			'kkks_office_phone' => 'Kkks Office Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kkks_id',$this->kkks_id);
		$criteria->compare('holding_id',$this->holding_id);
		$criteria->compare('kkks_name',$this->kkks_name,true);
		$criteria->compare('kkks_business_entity',$this->kkks_business_entity,true);
		$criteria->compare('kkks_is_operating',$this->kkks_is_operating);
		$criteria->compare('kkks_address',$this->kkks_address,true);
		$criteria->compare('kkks_office_phone',$this->kkks_office_phone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdmKkks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
