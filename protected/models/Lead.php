<?php

/**
 * This is the model class for table "rsc_lead".
 *
 * The followings are the available columns in table 'rsc_lead':
 * @property integer $lead_id
 * @property integer $play_id
 * @property integer $gcf_id
 * @property string $structure_name
 * @property string $lead_clarified
 * @property string $lead_date_initiate
 * @property string $lead_latitude
 * @property string $lead_longitude
 * @property string $lead_shore
 * @property string $lead_terrain
 * @property string $lead_near_field
 * @property string $lead_near_infra_structure
 * @property integer $lead_update_from
 * @property string $lead_submit_status
 * @property string $lead_submit_date
 * @property integer $lead_submit_revision
 *
 * The followings are the available model relations:
 * @property RscPlay $play
 * @property RscGcf $gcf
 * @property RscLead2d[] $rscLead2ds
 * @property RscLeadElectromagnetic[] $rscLeadElectromagnetics
 * @property RscLeadGeochemistry[] $rscLeadGeochemistries
 * @property RscLeadGeological[] $rscLeadGeologicals
 * @property RscLeadGravity[] $rscLeadGravities
 * @property RscLeadOther[] $rscLeadOthers
 * @property RscLeadResistivity[] $rscLeadResistivities
 */
class Lead extends CActiveRecord {
	public $center_lat_degree;
	public $center_lat_minute;
	public $center_lat_second;
	public $center_lat_direction;
	public $center_long_degree;
	public $center_long_minute;
	public $center_long_second;
	public $center_long_direction;
	public $basin_name;
	public $lead_name;
	
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'rsc_lead';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
				array (
					'structure_name, play_id, lead_clarified, lead_date_initiate, center_lat_degree, center_lat_minute, center_lat_second, center_lat_direction,
					center_long_degree, center_long_minute, center_long_second, center_long_direction, lead_shore, lead_terrain, lead_near_field, lead_near_infra_structure',
					'required' 
				),
// 				array(
// 					'structure_name',
// 					'check_structure_name_lead'
// 				),
				array(
					'center_lat_degree',
					'numerical',
					'min'=>0,
					'max'=>20,
					'numberPattern'=>'/^([1-9]{0,1})([0-9]{1})?$/'
				),
				array(
					'center_long_degree',
					'numerical',
					'min'=>90,
					'max'=>145,
					'numberPattern'=>'/^([1-9]{0,2})([0-9]{2})?$/'
				),
				array(
					'center_lat_minute, center_long_minute',
					'numerical',
					'min'=>0,
					'max'=>59
				),
				array(
					'center_lat_second, center_long_second',
					'numerical',
					'min'=>0,
					'max'=>59.999,
					'numberPattern'=>'/^\d{1,6}(\.\d{1,3})?$/'
				),
				array (
					'play_id, gcf_id, lead_update_from, lead_submit_revision',
					'numerical',
					'integerOnly' => true 
				),
				array(
					'center_lat_direction, center_long_direction',
					'length',
					'max'=>1
				),
				array (
						'structure_name, lead_clarified, lead_latitude, lead_longitude, lead_terrain, lead_near_field, lead_near_infra_structure, lead_submit_status',
						'length',
						'max' => 50 
				),
				array (
						'center_lat_degree, center_lat_minute, center_lat_second,
						center_long_degree, center_long_minute, center_long_second,
						lead_shore',
						'length',
						'max' => 8 
				),
				array (
						'lead_date_initiate, lead_submit_date',
						'safe' 
				),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array (
						'lead_id, play_id, gcf_id, structure_name, lead_clarified, lead_date_initiate, lead_latitude, lead_longitude, lead_shore, lead_terrain, lead_near_field, lead_near_infra_structure, lead_update_from, lead_submit_status, lead_submit_date, lead_submit_revision',
						'safe',
						'on' => 'search' 
				),
				array (
						'lead_id, play_id, gcf_id, basin_name, lead_name, structure_name, lead_clarified, lead_date_initiate, lead_latitude, lead_longitude, lead_shore, lead_terrain, lead_near_field, lead_near_infra_structure, lead_update_from, lead_submit_status, lead_submit_date, lead_submit_revision',
						'safe',
						'on' => 'searchRelated' 
				)
		);
	}
	
	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array (
				'plays' => array (
						self::BELONGS_TO,
						'Play',
						'play_id' 
				),
				'gcf' => array (
						self::BELONGS_TO,
						'Gcf',
						'gcf_id' 
				),
				'rscLead2ds' => array (
						self::HAS_MANY,
						'RscLead2d',
						'lead_id' 
				),
				'rscLeadElectromagnetics' => array (
						self::HAS_MANY,
						'RscLeadElectromagnetic',
						'lead_id' 
				),
				'rscLeadGeochemistries' => array (
						self::HAS_MANY,
						'RscLeadGeochemistry',
						'lead_id' 
				),
				'rscLeadGeologicals' => array (
						self::HAS_MANY,
						'RscLeadGeological',
						'lead_id' 
				),
				'rscLeadGravities' => array (
						self::HAS_MANY,
						'RscLeadGravity',
						'lead_id' 
				),
				'rscLeadOthers' => array (
						self::HAS_MANY,
						'RscLeadOther',
						'lead_id' 
				),
				'rscLeadResistivities' => array (
						self::HAS_MANY,
						'RscLeadResistivity',
						'lead_id' 
				) 
		);
	}
	
	/**
	 *
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array (
				'center_lat_degree'=>'Center Latitude Degree',
				'center_lat_minute'=>'Center Latitude Minute',
				'center_lat_second'=>'Center Latitude Second',
				'center_lat_direction'=>'Center Latitude Direction',
				'center_long_degree'=>'Center Longitude Degree',
				'center_long_minute'=>'Center Longitude Minute',
				'center_long_second'=>'Center Longitude Second',
				'center_long_direction'=>'Center Longitude Direction',
				'lead_id' => 'Lead',
				'play_id' => 'Play',
				'gcf_id' => 'Gcf',
				'structure_name' => 'Structure Name',
				'lead_clarified' => 'Clarified by',
				'lead_date_initiate' => 'Lead Name Initiation Date',
				'lead_latitude' => 'Lead Latitude',
				'lead_longitude' => 'Lead Longitude',
				'lead_shore' => 'Onshore or Offshore',
				'lead_terrain' => 'Terrain',
				'lead_near_field' => 'Nearby Field',
				'lead_near_infra_structure' => 'Nearby Infra Structure',
				'lead_update_from' => 'Lead Update From',
				'lead_submit_status' => 'Lead Submit Status',
				'lead_submit_date' => 'Lead Submit Date',
				'lead_submit_revision' => 'Lead Submit Revision' 
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 *         based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria ();
		
		$criteria->compare ( 'lead_id', $this->lead_id );
		$criteria->compare ( 'play_id', $this->play_id );
		$criteria->compare ( 'gcf_id', $this->gcf_id );
		$criteria->compare ( 'structure_name', $this->structure_name, true );
		$criteria->compare ( 'lead_clarified', $this->lead_clarified, true );
		$criteria->compare ( 'lead_date_initiate', $this->lead_date_initiate, true );
		$criteria->compare ( 'lead_latitude', $this->lead_latitude, true );
		$criteria->compare ( 'lead_longitude', $this->lead_longitude, true );
		$criteria->compare ( 'lead_shore', $this->lead_shore, true );
		$criteria->compare ( 'lead_terrain', $this->lead_terrain, true );
		$criteria->compare ( 'lead_near_field', $this->lead_near_field, true );
		$criteria->compare ( 'lead_near_infra_structure', $this->lead_near_infra_structure, true );
		$criteria->compare ( 'lead_update_from', $this->lead_update_from );
		$criteria->compare ( 'lead_submit_status', $this->lead_submit_status, true );
		$criteria->compare ( 'lead_submit_date', $this->lead_submit_date, true );
		$criteria->compare ( 'lead_submit_revision', $this->lead_submit_revision );
		
		return new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
	}

	public function searchRelated() {

		$criteria = new CDbCriteria;
	    $criteria->select="max(b.lead_submit_date)";
	    $criteria->alias = 'b';
	    $criteria->condition="b.lead_update_from = t.lead_update_from";
	    $subQuery=$this->getCommandBuilder()->createFindCommand($this->getTableSchema(),$criteria)->getText();
	    
	    $mainCriteria = new CDbCriteria;
	    $mainCriteria->together = true;
	    $mainCriteria->with = array('plays', 'plays.basin', 'gcf');
	    $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
	    $mainCriteria->addCondition('lead_is_deleted = 0', 'AND');
	    $mainCriteria->addCondition('lead_submit_date = (' . $subQuery . ')', 'AND');
	    $mainCriteria->addCondition('basin.basin_name LIKE "%' . $this->basin_name . '%"', 'AND');
	    $mainCriteria->addCondition('CONCAT(
	    	structure_name, " @ ", gcf.gcf_res_lithology, " - ", gcf.gcf_res_formation, " - ", 
				gcf.gcf_res_age_serie, " ", gcf.gcf_res_age_system, " - ", gcf.gcf_res_depos_env, " - ", gcf.gcf_trap_type
			) LIKE "%' . $this->lead_name . '%"', 'AND');
	    $mainCriteria->group = 'lead_update_from';
	    
	    return new CActiveDataProvider(get_class($this), array(
	        'criteria'=>$mainCriteria,
	        'sort'=>array(
	            'defaultOrder'=>'basin_urut'
	          )
	      ));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * 
	 * @param string $className
	 *        	active record class name.
	 * @return Lead the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
	
	public function getLeadName($gcf_id = null)
	{
		$sql = "select structure_name, 
					IF(gcf_res_lithology = 'Unknown', '', gcf_res_lithology) gcf_res_lithology, 
					IF(gcf_res_depos_env = 'Others', '', gcf_res_depos_env) gcf_res_depos_env, 
					IF(gcf_res_formation_serie = 'Not Available', '', gcf_res_formation_serie) gcf_res_formation_serie, 
					gcf_res_formation, 
					gcf_res_age_system, 
					IF(gcf_res_age_serie = 'Not Available', '', gcf_res_age_serie) gcf_res_age_serie, 
					IF(gcf_trap_type = 'Unknown', '', gcf_trap_type) gcf_trap_type
				from rsc_lead inner join rsc_gcf on rsc_lead.gcf_id = rsc_gcf.gcf_id where rsc_lead.gcf_id =" . $gcf_id;
		$command = Yii::app()->db->createCommand($sql);
		$leadData = $command->queryRow();
		return  '<b>' . $leadData['structure_name'] . '</b> @ ' . $leadData['gcf_res_lithology'] . ' - <b>' . $leadData['gcf_res_formation_serie'] . ' ' . $leadData['gcf_res_formation'] . '</b> - ' . $leadData['gcf_res_age_serie'] . ' ' . $leadData['gcf_res_age_system'] . ' - ' . $leadData['gcf_res_depos_env'] . ' - ' . $leadData['gcf_trap_type'];
	}
	
// 	public function check_structure_name_lead()
// 	{
// 		$regex = "/(?:lead|drillable|discovery)/";
	
// 		$sql = "select structure_name from rsc_lead where rsc_lead.structure_name = '" . $this->structure_name . "'";
// 		$command = Yii::app()->db->createCommand($sql);
// 		$data = $command->query();
	
// 		$sql3 = "select wk_id, prospect_id, structure_name from rsc_prospect, rsc_play where structure_name = '" . $this->structure_name . "' and rsc_prospect.play_id = rsc_play.play_id";
// 		$command3 = Yii::app()->db->createCommand($sql3);
// 		$data3 = $command3->queryAll();
	
// 		$check = 0;
// 		foreach ($data3 as $datas3)
// 		{
// 			if($datas3['wk_id'] != Yii::app()->user->wk_id)
// 			{
// 				$check = 1;
// 				break;
// 			}
// 		}
	
// 		if(preg_match($regex, $this->structure_name) || $data->rowCount > 0 || $check == 1)
// 		{
// 			$this->addError('structure_name', $this->getAttributeLabel('structure_name') . ' not allowed');
// 		}
// 	}
}
