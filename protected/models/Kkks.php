<?php

/**
 * This is the model class for table "adm_kkks".
 *
 * The followings are the available columns in table 'adm_kkks':
 * @property integer $kkks_id
 * @property integer $psc_id
 * @property string $kkks_name
 * @property string $kkks_interest
 * @property integer $kkks_is_operator
 * @property string $kkks_holding
 *
 * The followings are the available model relations:
 * @property AdmPsc $psc
 */
class Kkks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_kkks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kkks_interest', 'required'),
			array('psc_id, kkks_is_operator', 'numerical', 'integerOnly'=>true),
			array('kkks_name', 'length', 'max'=>254),
			array('kkks_holding', 'length', 'max'=>100),
			array(
				'kkks_interest',
				'numerical',
				'min'=>0.1,
				'max'=>100,
				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'kkks_interest',
				'chkValue'
			),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kkks_id, psc_id, kkks_name, kkks_interest, kkks_is_operator, kkks_holding', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'KkksPsc' => array(self::BELONGS_TO, 'Psc', 'psc_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kkks_id' => 'Kkks',
			'psc_id' => 'Psc',
			'kkks_name' => 'Kkks Name',
			'kkks_interest' => 'Kkks Interest',
			'kkks_is_operator' => 'Kkks Is Operator',
			'kkks_holding' => 'Kkks Holding',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kkks_id',$this->kkks_id);
		$criteria->compare('psc_id',$this->psc_id);
		$criteria->compare('kkks_name',$this->kkks_name,true);
		$criteria->compare('kkks_interest',$this->kkks_interest,true);
		$criteria->compare('kkks_is_operator',$this->kkks_is_operator);
		$criteria->compare('kkks_holding',$this->kkks_holding,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kkks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function chkValue()
	{
		$sql = 'select adm_kkks.psc_id, sum(kkks_interest) total 
				from adm_kkks 
				left join adm_psc on adm_psc.psc_id =  adm_kkks.psc_id 
				where adm_psc.wk_id = "' . Yii::app()->user->wk_id . '"';
		
		$command = Yii::app()->db->createCommand($sql);
		$dataKkks = $command->queryRow();
		
		$sisa = 100 - $dataKkks['total'];
		
		if($this->kkks_interest > $sisa)
			$this->addError('kkks_interest', 'limited value');
	}
}
