<?php

/**
 * This is the model class for table "rsc_gravity".
 *
 * The followings are the available columns in table 'rsc_gravity':
 * @property integer $sgv_id
 * @property integer $prospect_id
 * @property string $sgv_year_survey
 * @property string $sgv_survey_method
 * @property string $sgv_coverage_area
 * @property string $sgv_depth_range
 * @property string $sgv_spacing_interval
 * @property string $sgv_depth_spill
 * @property string $sgv_depth_estimate
 * @property string $sgv_depth_estimate_analog
 * @property string $sgv_depth_estimate_spill
 * @property string $sgv_depth_low
 * @property string $sgv_depth_low_analog
 * @property string $sgv_depth_low_spill
 * @property string $sgv_res_thickness
 * @property string $sgv_res_according
 * @property string $sgv_res_top_depth
 * @property string $sgv_res_bot_depth
 * @property string $sgv_gross_thickness
 * @property string $sgv_gross_according
 * @property string $sgv_gross_well
 * @property string $sgv_net_gross
 * @property double $sgv_vol_p90_area
 * @property double $sgv_vol_p50_area
 * @property double $sgv_vol_p10_area
 * @property string $sgv_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Gravity extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_gravity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxGravity'),
// 			array('sgv_year_survey, sgv_vol_p90_area, sgv_vol_p50_area, sgv_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'sgv_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'sgv_coverage_area, sgv_depth_range, sgv_spacing_interval, sgv_depth_spill, sgv_depth_estimate,
				sgv_depth_low, sgv_res_thickness,
				sgv_vol_p90_area, sgv_vol_p50_area, sgv_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'sgv_depth_estimate_spill, sgv_depth_low_spill, sgv_gross_thickness, sgv_net_gross',
				'numerical',
				'min'=>0,
				'max'=>100,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('sgv_year_survey, sgv_survey_method, sgv_coverage_area, sgv_depth_range, sgv_spacing_interval, sgv_depth_spill, sgv_depth_estimate, sgv_depth_estimate_analog, sgv_depth_estimate_spill, sgv_depth_low, sgv_depth_low_analog, sgv_depth_low_spill, sgv_res_thickness, sgv_res_according, sgv_res_top_depth, sgv_res_bot_depth, sgv_gross_thickness, sgv_gross_according, sgv_gross_well, sgv_net_gross', 'length', 'max'=>50),
			array('sgv_remark', 'safe'),
// 			array('sgv_vol_p90_area, sgv_vol_p50_area, sgv_vol_p10_area', 'chkDependentGravity'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sgv_id, prospect_id, sgv_year_survey, sgv_survey_method, sgv_coverage_area, sgv_depth_range, sgv_spacing_interval, sgv_depth_spill, sgv_depth_estimate, sgv_depth_estimate_analog, sgv_depth_estimate_spill, sgv_depth_low, sgv_depth_low_analog, sgv_depth_low_spill, sgv_res_thickness, sgv_res_according, sgv_res_top_depth, sgv_res_bot_depth, sgv_gross_thickness, sgv_gross_according, sgv_gross_well, sgv_net_gross, sgv_vol_p90_area, sgv_vol_p50_area, sgv_vol_p10_area, sgv_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sgv_id' => 'Sgv',
			'prospect_id' => 'Prospect',
			'sgv_year_survey' => 'Gravity Acquisition Year',
			'sgv_survey_method' => 'Sgv Survey Method',
			'sgv_coverage_area' => 'Sgv Coverage Area',
			'sgv_depth_range' => 'Sgv Depth Range',
			'sgv_spacing_interval' => 'Sgv Spacing Interval',
			'sgv_depth_spill' => 'Sgv Depth Spill',
			'sgv_depth_estimate' => 'Sgv Depth Estimate',
			'sgv_depth_estimate_analog' => 'Sgv Depth Estimate Analog',
			'sgv_depth_estimate_spill' => 'Sgv Depth Estimate Spill',
			'sgv_depth_low' => 'Sgv Depth Low',
			'sgv_depth_low_analog' => 'Sgv Depth Low Analog',
			'sgv_depth_low_spill' => 'Sgv Depth Low Spill',
			'sgv_res_thickness' => 'Sgv Res Thickness',
			'sgv_res_according' => 'Sgv Res According',
			'sgv_res_top_depth' => 'Sgv Res Top Depth',
			'sgv_res_bot_depth' => 'Sgv Res Bot Depth',
			'sgv_gross_thickness' => 'Sgv Gross Thickness',
			'sgv_gross_according' => 'Sgv Gross According',
			'sgv_gross_well' => 'Sgv Gross Well',
			'sgv_net_gross' => 'Sgv Net Gross',
			'sgv_vol_p90_area' => 'Gravity P90 Areal Closure Estimation',
			'sgv_vol_p50_area' => 'Gravity P50 Areal Closure Estimation',
			'sgv_vol_p10_area' => 'Gravity P10 Areal Closure Estimation',
			'sgv_remark' => 'Sgv Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sgv_id',$this->sgv_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('sgv_year_survey',$this->sgv_year_survey,true);
		$criteria->compare('sgv_survey_method',$this->sgv_survey_method,true);
		$criteria->compare('sgv_coverage_area',$this->sgv_coverage_area,true);
		$criteria->compare('sgv_depth_range',$this->sgv_depth_range,true);
		$criteria->compare('sgv_spacing_interval',$this->sgv_spacing_interval,true);
		$criteria->compare('sgv_depth_spill',$this->sgv_depth_spill,true);
		$criteria->compare('sgv_depth_estimate',$this->sgv_depth_estimate,true);
		$criteria->compare('sgv_depth_estimate_analog',$this->sgv_depth_estimate_analog,true);
		$criteria->compare('sgv_depth_estimate_spill',$this->sgv_depth_estimate_spill,true);
		$criteria->compare('sgv_depth_low',$this->sgv_depth_low,true);
		$criteria->compare('sgv_depth_low_analog',$this->sgv_depth_low_analog,true);
		$criteria->compare('sgv_depth_low_spill',$this->sgv_depth_low_spill,true);
		$criteria->compare('sgv_res_thickness',$this->sgv_res_thickness,true);
		$criteria->compare('sgv_res_according',$this->sgv_res_according,true);
		$criteria->compare('sgv_res_top_depth',$this->sgv_res_top_depth,true);
		$criteria->compare('sgv_res_bot_depth',$this->sgv_res_bot_depth,true);
		$criteria->compare('sgv_gross_thickness',$this->sgv_gross_thickness,true);
		$criteria->compare('sgv_gross_according',$this->sgv_gross_according,true);
		$criteria->compare('sgv_gross_well',$this->sgv_gross_well,true);
		$criteria->compare('sgv_net_gross',$this->sgv_net_gross,true);
		$criteria->compare('sgv_vol_p90_area',$this->sgv_vol_p90_area);
		$criteria->compare('sgv_vol_p50_area',$this->sgv_vol_p50_area);
		$criteria->compare('sgv_vol_p10_area',$this->sgv_vol_p10_area);
		$criteria->compare('sgv_remark',$this->sgv_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gravity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentGravity($attribute)
// 	{
// 		if($attribute == 'sgv_vol_p10_area')
// 		{
// 			if($this->sgv_vol_p10_area != '') {
// 				if($this->sgv_vol_p10_area == 0 || $this->sgv_vol_p10_area <= $this->sgv_vol_p50_area || $this->sgv_vol_p10_area <= $this->sgv_vol_p90_area)
// 					$this->addError('sgv_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than Gravity P50 Areal Closure Estimation and Gravity P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'sgv_vol_p50_area')
// 		{
// 			if($this->sgv_vol_p50_area != '') {
// 				if($this->sgv_vol_p50_area == 0 || $this->sgv_vol_p50_area <= $this->sgv_vol_p90_area)
// 					$this->addError('sgv_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than Gravity P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBoxGravity()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('sgv_year_survey', 'sgv_vol_p90_area', 'sgv_vol_p50_area', 'sgv_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.DependentGravity');
			$checking = new DependentGravity;
			$checking->attributes = array('sgv_vol_p90_area', 'sgv_vol_p50_area', 'sgv_vol_p10_area');
			$checking->validate($this);
		}
	}
}
