<?php

/**
 * This is the model class for table "adm_psc".
 *
 * The followings are the available columns in table 'adm_psc':
 * @property integer $psc_id
 * @property string $wk_id
 * @property integer $psc_status
 * @property string $psc_date_sign
 * @property string $psc_date_effective
 * @property string $psc_date_end
 * @property string $psc_remark
 * @property string $psc_authorized_by
 * @property string $psc_authorized_email
 *
 * The followings are the available model relations:
 * @property AdmCommitment[] $admCommitments
 * @property AdmKkks[] $admKkks
 * @property AdmWorkingArea $wk
 */
class Psc extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_psc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'psc_date_sign, psc_date_end, psc_authorized_by, psc_authorized_email',
				'required'
			),
			array(
				'psc_authorized_email',
				'email'
			),
			array('psc_status', 'numerical', 'integerOnly'=>true),
			array('wk_id', 'length', 'max'=>6),
			array('psc_authorized_by, psc_authorized_email', 'length', 'max'=>50),
			array('psc_date_sign, psc_date_effective, psc_date_end, psc_remark', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('psc_id, wk_id, psc_status, psc_date_sign, psc_date_effective, psc_date_end, psc_remark, psc_authorized_by, psc_authorized_email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'admCommitments' => array(self::HAS_MANY, 'AdmCommitment', 'psc_id'),
			'PcsKkks' => array(self::HAS_MANY, 'Kkks', 'psc_id'),
			'wk' => array(self::BELONGS_TO, 'AdmWorkingArea', 'wk_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'psc_id' => 'Psc',
			'wk_id' => 'Wk',
			'psc_status' => 'Psc Status',
			'psc_date_sign' => 'Psc Date Sign',
			'psc_date_effective' => 'Psc Date Effective',
			'psc_date_end' => 'Psc Date End',
			'psc_remark' => 'Psc Remark',
			'psc_authorized_by' => 'Psc Authorized By',
			'psc_authorized_email' => 'Psc Authorized Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('psc_id',$this->psc_id);
		$criteria->compare('wk_id',$this->wk_id,true);
		$criteria->compare('psc_status',$this->psc_status);
		$criteria->compare('psc_date_sign',$this->psc_date_sign,true);
		$criteria->compare('psc_date_effective',$this->psc_date_effective,true);
		$criteria->compare('psc_date_end',$this->psc_date_end,true);
		$criteria->compare('psc_remark',$this->psc_remark,true);
		$criteria->compare('psc_authorized_by',$this->psc_authorized_by,true);
		$criteria->compare('psc_authorized_email',$this->psc_authorized_email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Psc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
