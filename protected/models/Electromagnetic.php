<?php

/**
 * This is the model class for table "rsc_electromagnetic".
 *
 * The followings are the available columns in table 'rsc_electromagnetic':
 * @property integer $sel_id
 * @property integer $prospect_id
 * @property string $sel_year_survey
 * @property string $sel_survey_method
 * @property string $sel_coverage_area
 * @property string $sel_depth_range
 * @property string $sel_spacing_interval
 * @property double $sel_vol_p90_area
 * @property double $sel_vol_p50_area
 * @property double $sel_vol_p10_area
 * @property string $sel_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Electromagnetic extends CActiveRecord
{
	public $is_checked;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_electromagnetic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxElectromagnetic'),
// 			array('sel_year_survey, sel_vol_p90_area, sel_vol_p50_area, sel_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'sel_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'sel_coverage_area, sel_depth_range, sel_spacing_interval,
				sel_vol_p90_area, sel_vol_p50_area, sel_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('sel_year_survey, sel_survey_method, sel_coverage_area, sel_depth_range, sel_spacing_interval', 'length', 'max'=>50),
			array('sel_remark', 'safe'),
// 			array('sel_vol_p90_area, sel_vol_p50_area, sel_vol_p10_area', 'chkDependentElectromagnetic'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sel_id, prospect_id, sel_year_survey, sel_survey_method, sel_coverage_area, sel_depth_range, sel_spacing_interval, sel_vol_p90_area, sel_vol_p50_area, sel_vol_p10_area, sel_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sel_id' => 'Sel',
			'prospect_id' => 'Prospect',
			'sel_year_survey' => 'Electromagnetic Acquisition Year',
			'sel_survey_method' => 'Sel Survey Method',
			'sel_coverage_area' => 'Sel Coverage Area',
			'sel_depth_range' => 'Sel Depth Range',
			'sel_spacing_interval' => 'Sel Spacing Interval',
			'sel_vol_p90_area' => 'Electromagnetic P90 Areal Closure Estimation',
			'sel_vol_p50_area' => 'Electromagnetic P50 Areal Closure Estimation',
			'sel_vol_p10_area' => 'Electromagnetic P10 Areal Closure Estimation',
			'sel_remark' => 'Sel Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sel_id',$this->sel_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('sel_year_survey',$this->sel_year_survey,true);
		$criteria->compare('sel_survey_method',$this->sel_survey_method,true);
		$criteria->compare('sel_coverage_area',$this->sel_coverage_area,true);
		$criteria->compare('sel_depth_range',$this->sel_depth_range,true);
		$criteria->compare('sel_spacing_interval',$this->sel_spacing_interval,true);
		$criteria->compare('sel_vol_p90_area',$this->sel_vol_p90_area);
		$criteria->compare('sel_vol_p50_area',$this->sel_vol_p50_area);
		$criteria->compare('sel_vol_p10_area',$this->sel_vol_p10_area);
		$criteria->compare('sel_remark',$this->sel_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Electromagnetic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentElectromagnetic($attribute)
// 	{
// 		if($attribute == 'sel_vol_p10_area')
// 		{
// 			if($this->sel_vol_p10_area != '') {
// 				if($this->sel_vol_p10_area == 0 || $this->sel_vol_p10_area <= $this->sel_vol_p50_area || $this->sel_vol_p10_area <= $this->sel_vol_p90_area)
// 					$this->addError('sel_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than Electromagnetic P50 Areal Closure Estimation and Electromagnetic P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'sel_vol_p50_area')
// 		{
// 			if($this->sel_vol_p50_area != '') {
// 				if($this->sel_vol_p50_area == 0 || $this->sel_vol_p50_area <= $this->sel_vol_p90_area)
// 					$this->addError('sel_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than Electromagnetic P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBoxElectromagnetic()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('sel_year_survey', 'sel_vol_p90_area', 'sel_vol_p50_area', 'sel_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.DependentElectromagnetic');
			$checking = new DependentElectromagnetic;
			$checking->attributes = array('sel_vol_p90_area', 'sel_vol_p50_area', 'sel_vol_p10_area');
			$checking->validate($this);
		}
	}
}
