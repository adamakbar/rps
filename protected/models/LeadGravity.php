<?php

/**
 * This is the model class for table "rsc_lead_gravity".
 *
 * The followings are the available columns in table 'rsc_lead_gravity':
 * @property integer $lsgv_id
 * @property integer $lead_id
 * @property string $lsgv_year_survey
 * @property string $lsgv_survey_method
 * @property string $lsgv_coverage_area
 * @property string $lsgv_range_penetration
 * @property string $lsgv_spacing_interval
 * @property double $lsgv_low_estimate
 * @property double $lsgv_best_estimate
 * @property double $lsgv_high_estimate
 * @property string $lsgv_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class LeadGravity extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_gravity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLeadGravity'),
// 			array('lsgv_low_estimate, lsgv_best_estimate, lsgv_high_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'lsgv_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'lsgv_coverage_area, lsgv_range_penetration, lsgv_spacing_interval',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('lsgv_low_estimate, lsgv_best_estimate, lsgv_high_estimate', 'numerical'),
			array('lsgv_year_survey, lsgv_survey_method, lsgv_coverage_area, lsgv_range_penetration, lsgv_spacing_interval', 'length', 'max'=>50),
			array('lsgv_remark', 'safe'),
// 			array('lsgv_low_estimate, lsgv_best_estimate, lsgv_high_estimate', 'chkDependentAreaGravity'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lsgv_id, lead_id, lsgv_year_survey, lsgv_survey_method, lsgv_coverage_area, lsgv_range_penetration, lsgv_spacing_interval, lsgv_low_estimate, lsgv_best_estimate, lsgv_high_estimate, lsgv_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lsgv_id' => 'Lsgv',
			'lead_id' => 'Lead',
			'lsgv_year_survey' => 'Gravity Acquisition Year',
			'lsgv_survey_method' => 'Lsgv Survey Method',
			'lsgv_coverage_area' => 'Gravity Survey Coverage Area',
			'lsgv_range_penetration' => 'Gravity Depth Survey Penetration Range',
			'lsgv_spacing_interval' => 'Gravity Recorder Spacing Interval',
			'lsgv_low_estimate' => 'Gravity Low Estimate',
			'lsgv_best_estimate' => 'Gravity Best Estimate',
			'lsgv_high_estimate' => 'Gravity High Estimate',
			'lsgv_remark' => 'Lsgv Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lsgv_id',$this->lsgv_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('lsgv_year_survey',$this->lsgv_year_survey,true);
		$criteria->compare('lsgv_survey_method',$this->lsgv_survey_method,true);
		$criteria->compare('lsgv_coverage_area',$this->lsgv_coverage_area,true);
		$criteria->compare('lsgv_range_penetration',$this->lsgv_range_penetration,true);
		$criteria->compare('lsgv_spacing_interval',$this->lsgv_spacing_interval,true);
		$criteria->compare('lsgv_low_estimate',$this->lsgv_low_estimate);
		$criteria->compare('lsgv_best_estimate',$this->lsgv_best_estimate);
		$criteria->compare('lsgv_high_estimate',$this->lsgv_high_estimate);
		$criteria->compare('lsgv_remark',$this->lsgv_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeadGravity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentAreaGravity($attribute)
// 	{
// 		if($attribute == 'lsgv_high_estimate')
// 		{
// 			if($this->lsgv_high_estimate == 0 || $this->lsgv_high_estimate <= $this->lsgv_best_estimate || $this->lsgv_high_estimate <= $this->lsgv_low_estimate)
// 				$this->addError($this->lsgv_high_estimate, $this->getAttributeLabel($attribute) . ' must greater than Gravity Best Estimate and Gravity Low Estimate');
// 		}
		
// 		if($attribute == 'lsgv_best_estimate')
// 		{
// 			if($this->lsgv_best_estimate == 0 || $this->lsgv_best_estimate <= $this->lsgv_low_estimate)
// 				$this->addError($this->lsgv_best_estimate, $this->getAttributeLabel($attribute) . ' must greater than Gravity Low Estimate');
// 		}
// 	}
	
	public function chkBoxLeadGravity()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('lsgv_year_survey', 'lsgv_low_estimate', 'lsgv_best_estimate', 'lsgv_high_estimate');
			$required->validate($this);
				
			Yii::import('ext.validasi.DependentAreaGravity');
			$checking = new DependentAreaGravity;
			$checking->attributes = array('lsgv_low_estimate', 'lsgv_best_estimate', 'lsgv_high_estimate');
			$checking->validate($this);
		}
	}
}
