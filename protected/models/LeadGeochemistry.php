<?php

/**
 * This is the model class for table "rsc_lead_geochemistry".
 *
 * The followings are the available columns in table 'rsc_lead_geochemistry':
 * @property integer $lsgc_id
 * @property integer $lead_id
 * @property string $lsgc_year_survey
 * @property string $lsgc_range_interval
 * @property string $lsgc_number_sample
 * @property string $lsgc_number_rock
 * @property string $lsgc_number_fluid
 * @property string $lsgc_hc_availability
 * @property string $lsgc_hc_composition
 * @property string $lsgc_year_report
 * @property double $lsgc_low_estimate
 * @property double $lsgc_high_estimate
 * @property double $lsgc_best_estimate
 * @property string $lsgc_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class LeadGeochemistry extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_geochemistry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLeadGeochemistry'),
// 			array('lsgc_low_estimate, lsgc_high_estimate, lsgc_best_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'lsgc_year_survey, lsgc_year_report',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'lsgc_range_interval, lsgc_number_sample, lsgc_number_rock, lsgc_number_fluid',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('lsgc_low_estimate, lsgc_high_estimate, lsgc_best_estimate', 'numerical'),
			array('lsgc_year_survey, lsgc_range_interval, lsgc_number_sample, lsgc_number_rock, lsgc_number_fluid, lsgc_hc_availability, lsgc_hc_composition, lsgc_year_report', 'length', 'max'=>50),
			array('lsgc_remark', 'safe'),
// 				array('lsgc_low_estimate, lsgc_high_estimate, lsgc_best_estimate', 'chkDependentAreaGeochemistry'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lsgc_id, lead_id, lsgc_year_survey, lsgc_range_interval, lsgc_number_sample, lsgc_number_rock, lsgc_number_fluid, lsgc_hc_availability, lsgc_hc_composition, lsgc_year_report, lsgc_low_estimate, lsgc_high_estimate, lsgc_best_estimate, lsgc_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lsgc_id' => 'Lsgc',
			'lead_id' => 'Lead',
			'lsgc_year_survey' => 'Geochemistry Acquisition Year',
			'lsgc_range_interval' => 'Geochemistry Interval Samples Range',
			'lsgc_number_sample' => 'Geochemistry Number of Sample Location',
			'lsgc_number_rock' => 'Geochemistry Number of Rocks Sample',
			'lsgc_number_fluid' => 'Geochemistry Number Fluid',
			'lsgc_hc_availability' => 'Geochemistry Availability HC Composition',
			'lsgc_hc_composition' => 'Geochemistry Hydrocarbon Composition',
			'lsgc_year_report' => 'Geochemistry Year Report',
			'lsgc_low_estimate' => 'Geochemistry Low Estimate',
			'lsgc_high_estimate' => 'Geochemistry High Estimate',
			'lsgc_best_estimate' => 'Geochemistry Best Estimate',
			'lsgc_remark' => 'Lsgc Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lsgc_id',$this->lsgc_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('lsgc_year_survey',$this->lsgc_year_survey,true);
		$criteria->compare('lsgc_range_interval',$this->lsgc_range_interval,true);
		$criteria->compare('lsgc_number_sample',$this->lsgc_number_sample,true);
		$criteria->compare('lsgc_number_rock',$this->lsgc_number_rock,true);
		$criteria->compare('lsgc_number_fluid',$this->lsgc_number_fluid,true);
		$criteria->compare('lsgc_hc_availability',$this->lsgc_hc_availability,true);
		$criteria->compare('lsgc_hc_composition',$this->lsgc_hc_composition,true);
		$criteria->compare('lsgc_year_report',$this->lsgc_year_report,true);
		$criteria->compare('lsgc_low_estimate',$this->lsgc_low_estimate);
		$criteria->compare('lsgc_high_estimate',$this->lsgc_high_estimate);
		$criteria->compare('lsgc_best_estimate',$this->lsgc_best_estimate);
		$criteria->compare('lsgc_remark',$this->lsgc_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeadGeochemistry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentAreaGeochemistry($attribute)
// 	{
// 		if($attribute == 'lsgc_high_estimate')
// 		{
// 			if($this->lsgc_high_estimate == 0 || $this->lsgc_high_estimate <= $this->lsgc_best_estimate || $this->lsgc_high_estimate <= $this->lsgc_low_estimate)
// 				$this->addError($this->lsgc_high_estimate, $this->getAttributeLabel($attribute) . ' must greater than Geochemistry Best Estimate and Geochemistry Low Estimate');
// 		}
		
// 		if($attribute == 'lsgc_best_estimate')
// 		{
// 			if($this->lsgc_best_estimate == 0 || $this->lsgc_best_estimate <= $this->lsgc_low_estimate)
// 				$this->addError($this->lsgc_best_estimate, $this->getAttributeLabel($attribute) . ' must greater than Geochemistry Low Estimate');
// 		}
// 	}
	
	public function chkBoxLeadGeochemistry()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('lsgc_year_survey', 'lsgc_low_estimate', 'lsgc_best_estimate', 'lsgc_high_estimate');
			$required->validate($this);
				
			Yii::import('ext.validasi.DependentAreaGeochemistry');
			$checking = new DependentAreaGeochemistry;
			$checking->attributes = array('lsgc_low_estimate', 'lsgc_best_estimate', 'lsgc_high_estimate');
			$checking->validate($this);
		}
	}
}
