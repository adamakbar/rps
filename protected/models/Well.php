<?php

/**
 * This is the model class for table "rsc_well".
 *
 * The followings are the available columns in table 'rsc_well':
 * @property integer $wl_id
 * @property integer $prospect_id
 * @property string $wk_id
 * @property string $prospect_type
 * @property string $wl_name
 * @property string $wl_latitude
 * @property string $wl_longitude
 * @property string $wl_type
 * @property string $wl_result
 * @property string $wl_shore
 * @property string $wl_terrain
 * @property string $wl_status
 * @property string $wl_formation
 * @property string $wl_date_complete
 * @property string $wl_target_depth_tvd
 * @property string $wl_target_depth_md
 * @property string $wl_actual_depth
 * @property string $wl_target_play
 * @property string $wl_actual_play
 * @property string $wl_number_mdt
 * @property string $wl_number_rft
 * @property string $wl_res_pressure
 * @property string $wl_last_pressure
 * @property string $wl_pressure_gradient
 * @property string $wl_last_temp
 * @property string $wl_integrity
 * @property string $wl_electro_avail
 * @property string $wl_electro_list
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 * @property RscWellzone[] $rscWellzones
 */
class Well extends CActiveRecord
{
	public $lat_degree;
	public $lat_minute;
	public $lat_second;
	public $lat_direction;
	public $long_degree;
	public $long_minute;
	public $long_second;
	public $long_direction;
	
	public $wl_electro_list1;
	public $wl_electro_list2;
	public $wl_electro_list3;
	public $wl_electro_list4;
	public $wl_electro_list5;
	public $wl_electro_list6;
	public $wl_electro_list7;
	public $wl_electro_list8;
	public $wl_electro_list9;
	public $wl_electro_list10;
	public $wl_electro_list11;
	public $wl_electro_list12;
	public $wl_electro_list13;
	public $wl_electro_list14;
	public $wl_electro_list15;
	public $wl_electro_list16;
	
	public $wl_total_zone;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_well';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'prospect_type, wl_name, lat_degree, lat_minute, lat_second, lat_direction, 
					long_degree, long_minute, long_second, long_direction, wl_result, wl_type,
					wl_shore, wl_terrain, wl_formation', 
				'required'
			),
			array(
				'wl_total_zone',
				'required', 
				'on'=>'wellzone'
			),
			array(
				'lat_degree',
				'numerical',
				'min'=>0,
				'max'=>20,
				'numberPattern'=>'/^([1-9]{0,1})([0-9]{1})?$/'
			),
			array(
				'long_degree',
				'numerical',
				'min'=>90,
				'max'=>145,
				'numberPattern'=>'/^([1-9]{0,2})([0-9]{2})?$/'
			),
			array(
				'lat_minute, long_minute',
				'numerical',
				'min'=>0,
				'max'=>59
			),
			array(
				'lat_second, long_second',
				'numerical',
				'min'=>0,
				'max'=>59.999,
				'numberPattern'=>'/^\d{1,6}(\.\d{1,3})?$/'
			),
			array(
				'wl_target_depth_tvd, wl_target_depth_md, wl_actual_depth, wl_target_play,
					wl_actual_play, wl_res_pressure, wl_last_pressure, wl_pressure_gradient,
					wl_last_temp, ',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'lat_direction, long_direction',
				'length',
				'max'=>1
			),
			array(
				'lat_degree, lat_minute, lat_second, long_degree, long_minute, long_second',
				'length',
				'max'=>8
			),
			array('prospect_type', 'length', 'max'=>9),
			array('wl_name, wl_latitude, wl_longitude, wl_type, wl_result, wl_shore, wl_terrain, wl_status, wl_formation, wl_date_complete, wl_target_depth_tvd, wl_target_depth_md, wl_actual_depth, wl_target_play, wl_actual_play, wl_number_mdt, wl_number_rft, wl_res_pressure, wl_last_pressure, wl_pressure_gradient, wl_last_temp, wl_integrity, wl_electro_avail', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wl_id, prospect_id, wk_id, prospect_type, wl_name, wl_latitude, wl_longitude, wl_type, wl_result, wl_shore, wl_terrain, wl_status, wl_formation, wl_date_complete, wl_target_depth_tvd, wl_target_depth_md, wl_actual_depth, wl_target_play, wl_actual_play, wl_number_mdt, wl_number_rft, wl_res_pressure, wl_last_pressure, wl_pressure_gradient, wl_last_temp, wl_integrity, wl_electro_avail, wl_electro_list', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospects' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'wellzones' => array(self::HAS_MANY, 'Wellzone', 'wl_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wl_id' => 'Wl',
			'prospect_id' => 'Prospect',
			'wk_id' => 'Wk',
			'prospect_type' => 'Prospect Type',
			'wl_name' => 'Wl Name',
			'wl_latitude' => 'Wl Latitude',
			'wl_longitude' => 'Wl Longitude',
			'wl_type' => 'Wl Type',
			'wl_result' => 'Wl Result',
			'wl_shore' => 'Wl Shore',
			'wl_terrain' => 'Wl Terrain',
			'wl_status' => 'Wl Status',
			'wl_formation' => 'Wl Formation',
			'wl_date_complete' => 'Wl Date Complete',
			'wl_target_depth_tvd' => 'Wl Target Depth Tvd',
			'wl_target_depth_md' => 'Wl Target Depth Md',
			'wl_actual_depth' => 'Wl Actual Depth',
			'wl_target_play' => 'Wl Target Play',
			'wl_actual_play' => 'Wl Actual Play',
			'wl_number_mdt' => 'Wl Number Mdt',
			'wl_number_rft' => 'Wl Number Rft',
			'wl_res_pressure' => 'Wl Res Pressure',
			'wl_last_pressure' => 'Wl Last Pressure',
			'wl_pressure_gradient' => 'Wl Pressure Gradient',
			'wl_last_temp' => 'Wl Last Temp',
			'wl_integrity' => 'Wl Integrity',
			'wl_electro_avail' => 'Wl Electro Avail',
			'wl_electro_list' => 'Wl Electro List',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wl_id',$this->wl_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('wk_id',$this->wk_id,true);
		$criteria->compare('prospect_type',$this->prospect_type,true);
		$criteria->compare('wl_name',$this->wl_name,true);
		$criteria->compare('wl_latitude',$this->wl_latitude,true);
		$criteria->compare('wl_longitude',$this->wl_longitude,true);
		$criteria->compare('wl_type',$this->wl_type,true);
		$criteria->compare('wl_result',$this->wl_result,true);
		$criteria->compare('wl_shore',$this->wl_shore,true);
		$criteria->compare('wl_terrain',$this->wl_terrain,true);
		$criteria->compare('wl_status',$this->wl_status,true);
		$criteria->compare('wl_formation',$this->wl_formation,true);
		$criteria->compare('wl_date_complete',$this->wl_date_complete,true);
		$criteria->compare('wl_target_depth_tvd',$this->wl_target_depth_tvd,true);
		$criteria->compare('wl_target_depth_md',$this->wl_target_depth_md,true);
		$criteria->compare('wl_actual_depth',$this->wl_actual_depth,true);
		$criteria->compare('wl_target_play',$this->wl_target_play,true);
		$criteria->compare('wl_actual_play',$this->wl_actual_play,true);
		$criteria->compare('wl_number_mdt',$this->wl_number_mdt,true);
		$criteria->compare('wl_number_rft',$this->wl_number_rft,true);
		$criteria->compare('wl_res_pressure',$this->wl_res_pressure,true);
		$criteria->compare('wl_last_pressure',$this->wl_last_pressure,true);
		$criteria->compare('wl_pressure_gradient',$this->wl_pressure_gradient,true);
		$criteria->compare('wl_last_temp',$this->wl_last_temp,true);
		$criteria->compare('wl_integrity',$this->wl_integrity,true);
		$criteria->compare('wl_electro_avail',$this->wl_electro_avail,true);
		$criteria->compare('wl_electro_list',$this->wl_electro_list,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Well the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
