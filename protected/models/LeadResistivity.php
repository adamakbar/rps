<?php

/**
 * This is the model class for table "rsc_lead_resistivity".
 *
 * The followings are the available columns in table 'rsc_lead_resistivity':
 * @property integer $lsrt_id
 * @property integer $lead_id
 * @property string $lsrt_year_survey
 * @property string $lsrt_survey_method
 * @property string $lsrt_coverage_area
 * @property string $lsrt_range_penetration
 * @property string $lsrt_spacing_interval
 * @property double $lsrt_low_estimate
 * @property double $lsrt_best_estimate
 * @property double $lsrt_high_estimate
 * @property string $lsrt_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class LeadResistivity extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_resistivity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLeadResistivity'),
// 			array('lsrt_low_estimate, lsrt_best_estimate, lsrt_high_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'lsrt_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'lsrt_coverage_area, lsrt_range_penetration, lsrt_spacing_interval',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('lsrt_low_estimate, lsrt_best_estimate, lsrt_high_estimate', 'numerical'),
			array('lsrt_year_survey, lsrt_survey_method, lsrt_coverage_area, lsrt_range_penetration, lsrt_spacing_interval', 'length', 'max'=>50),
			array('lsrt_remark', 'safe'),
// 			array('lsrt_low_estimate, lsrt_best_estimate, lsrt_high_estimate', 'chkDependentAreaResistivity'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lsrt_id, lead_id, lsrt_year_survey, lsrt_survey_method, lsrt_coverage_area, lsrt_range_penetration, lsrt_spacing_interval, lsrt_low_estimate, lsrt_best_estimate, lsrt_high_estimate, lsrt_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lsrt_id' => 'Lsrt',
			'lead_id' => 'Lead',
			'lsrt_year_survey' => 'Resistivity Acquisition Year',
			'lsrt_survey_method' => 'Resistivity Survey Method',
			'lsrt_coverage_area' => 'Resistivity Survey Coverage Area',
			'lsrt_range_penetration' => 'Resistivity Depth Survey Penetration Range',
			'lsrt_spacing_interval' => 'Resistivity Recorder Spacing Interval',
			'lsrt_low_estimate' => 'Resistivity Low Estimate',
			'lsrt_best_estimate' => 'Resistivity Best Estimate',
			'lsrt_high_estimate' => 'Resistivity High Estimate',
			'lsrt_remark' => 'Lsrt Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lsrt_id',$this->lsrt_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('lsrt_year_survey',$this->lsrt_year_survey,true);
		$criteria->compare('lsrt_survey_method',$this->lsrt_survey_method,true);
		$criteria->compare('lsrt_coverage_area',$this->lsrt_coverage_area,true);
		$criteria->compare('lsrt_range_penetration',$this->lsrt_range_penetration,true);
		$criteria->compare('lsrt_spacing_interval',$this->lsrt_spacing_interval,true);
		$criteria->compare('lsrt_low_estimate',$this->lsrt_low_estimate);
		$criteria->compare('lsrt_best_estimate',$this->lsrt_best_estimate);
		$criteria->compare('lsrt_high_estimate',$this->lsrt_high_estimate);
		$criteria->compare('lsrt_remark',$this->lsrt_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeadResistivity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentAreaResistivity($attribute)
// 	{
// 		if($attribute == 'lsrt_high_estimate')
// 		{
// 			if($this->lsrt_high_estimate == 0 || $this->lsrt_high_estimate <= $this->lsrt_best_estimate || $this->lsrt_high_estimate <= $this->lsrt_low_estimate)
// 				$this->addError($this->lsrt_high_estimate, $this->getAttributeLabel($attribute) . ' must greater than Resistivity Best Estimate and Resistivity Low Estimate');
// 		}
		
// 		if($attribute == 'lsrt_best_estimate')
// 		{
// 			if($this->lsrt_best_estimate == 0 || $this->lsrt_best_estimate <= $this->lsrt_low_estimate)
// 				$this->addError($this->lsrt_best_estimate, $this->getAttributeLabel($attribute) . ' must greater Resistivity Low Estimate');
// 		}
// 	}
	
	public function chkBoxLeadResistivity()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('lsrt_year_survey', 'lsrt_low_estimate', 'lsrt_best_estimate', 'lsrt_high_estimate');
			$required->validate($this);
				
			Yii::import('ext.validasi.DependentAreaResistivity');
			$checking = new DependentAreaResistivity;
			$checking->attributes = array('lsrt_low_estimate', 'lsrt_best_estimate', 'lsrt_high_estimate');
			$checking->validate($this);
		}
	}
}
