<?php

/**
 * This is the model class for table "rsc_geochemistry".
 *
 * The followings are the available columns in table 'rsc_geochemistry':
 * @property integer $sgc_id
 * @property integer $prospect_id
 * @property string $sgc_year_survey
 * @property string $sgc_sample_interval
 * @property string $sgc_number_sample
 * @property string $sgc_number_rock
 * @property string $sgc_number_fluid
 * @property string $sgc_avail_hc
 * @property string $sgc_hc_composition
 * @property string $sgc_lab_report
 * @property double $sgc_vol_p90_area
 * @property double $sgc_vol_p50_area
 * @property double $sgc_vol_p10_area
 * @property string $sgc_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Geochemistry extends CActiveRecord
{
	public $is_checked;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_geochemistry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxGeochemistry'),
// 			array('sgc_year_survey, sgc_vol_p90_area, sgc_vol_p50_area, sgc_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'sgc_year_survey, sgc_lab_report',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'sgc_sample_interval, sgc_number_sample, sgc_number_rock, sgc_number_fluid,
				sgc_vol_p90_area, sgc_vol_p50_area, sgc_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('sgc_year_survey, sgc_sample_interval, sgc_number_sample, sgc_number_rock, sgc_number_fluid, sgc_avail_hc, sgc_hc_composition, sgc_lab_report', 'length', 'max'=>50),
			array('sgc_remark', 'safe'),
// 			array('sgc_vol_p90_area, sgc_vol_p50_area, sgc_vol_p10_area', 'chkDependentGeochemistry'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sgc_id, prospect_id, sgc_year_survey, sgc_sample_interval, sgc_number_sample, sgc_number_rock, sgc_number_fluid, sgc_avail_hc, sgc_hc_composition, sgc_lab_report, sgc_vol_p90_area, sgc_vol_p50_area, sgc_vol_p10_area, sgc_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sgc_id' => 'Sgc',
			'prospect_id' => 'Prospect',
			'sgc_year_survey' => 'Geochemistry Acquisition Year',
			'sgc_sample_interval' => 'Sgc Sample Interval',
			'sgc_number_sample' => 'Sgc Number Sample',
			'sgc_number_rock' => 'Sgc Number Rock',
			'sgc_number_fluid' => 'Sgc Number Fluid',
			'sgc_avail_hc' => 'Sgc Avail Hc',
			'sgc_hc_composition' => 'Sgc Hc Composition',
			'sgc_lab_report' => 'Sgc Lab Report',
			'sgc_vol_p90_area' => 'Geochemistry P90 Areal Closure Estimation',
			'sgc_vol_p50_area' => 'Geochemistry P50 Areal Closure Estimation',
			'sgc_vol_p10_area' => 'Geochemistry P10 Areal Closure Estimation',
			'sgc_remark' => 'Sgc Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sgc_id',$this->sgc_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('sgc_year_survey',$this->sgc_year_survey,true);
		$criteria->compare('sgc_sample_interval',$this->sgc_sample_interval,true);
		$criteria->compare('sgc_number_sample',$this->sgc_number_sample,true);
		$criteria->compare('sgc_number_rock',$this->sgc_number_rock,true);
		$criteria->compare('sgc_number_fluid',$this->sgc_number_fluid,true);
		$criteria->compare('sgc_avail_hc',$this->sgc_avail_hc,true);
		$criteria->compare('sgc_hc_composition',$this->sgc_hc_composition,true);
		$criteria->compare('sgc_lab_report',$this->sgc_lab_report,true);
		$criteria->compare('sgc_vol_p90_area',$this->sgc_vol_p90_area);
		$criteria->compare('sgc_vol_p50_area',$this->sgc_vol_p50_area);
		$criteria->compare('sgc_vol_p10_area',$this->sgc_vol_p10_area);
		$criteria->compare('sgc_remark',$this->sgc_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Geochemistry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentGeochemistry($attribute)
// 	{
// 		if($attribute == 'sgc_vol_p10_area')
// 		{
// 			if($this->sgc_vol_p10_area != '') {
// 				if($this->sgc_vol_p10_area == 0 || $this->sgc_vol_p10_area <= $this->sgc_vol_p50_area || $this->sgc_vol_p10_area <= $this->sgc_vol_p90_area)
// 					$this->addError('sgc_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than Geochemistry P50 Areal Closure Estimation and Geochemistry P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'sgc_vol_p50_area')
// 		{
// 			if($this->sgc_vol_p50_area != '') {
// 				if($this->sgc_vol_p50_area == 0 || $this->sgc_vol_p50_area <= $this->sgc_vol_p90_area)
// 					$this->addError('sgc_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than Geochemistry P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBoxGeochemistry()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('sgc_year_survey', 'sgc_vol_p90_area', 'sgc_vol_p50_area', 'sgc_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.DependentGeochemistry');
			$checking = new DependentGeochemistry;
			$checking->attributes = array('sgc_vol_p90_area', 'sgc_vol_p50_area', 'sgc_vol_p10_area');
			$checking->validate($this);
		}
	}
}
