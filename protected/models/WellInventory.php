<?php

/**
 * This is the model class for table "rsc_well_inventory".
 *
 * The followings are the available columns in table 'rsc_well_inventory':
 * @property integer $a2_id
 * @property string $wk_id
 * @property string $a2_name
 * @property string $a2_category
 * @property string $a2_latitude
 * @property string $a2_longitude
 * @property string $a2_year_drill
 *
 * The followings are the available model relations:
 * @property AdmWorkingArea $wk
 */
class WellInventory extends CActiveRecord
{
	public $center_lat_degree;
	public $center_lat_minute;
	public $center_lat_second;
	public $center_lat_direction;
	public $center_long_degree;
	public $center_long_minute;
	public $center_long_second;
	public $center_long_direction;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_well_inventory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'a2_name, a2_category, a2_year_drill, center_lat_degree, center_lat_minute, center_lat_second, center_lat_direction,
				center_long_degree, center_long_minute, center_long_second, center_long_direction',
				'required'
			),
			array(
				'center_lat_degree, center_lat_minute, center_lat_second,
				center_long_degree, center_long_minute, center_long_second',
				'numerical',
				'numberPattern'=>'/^\s*[-+]?\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'center_lat_direction, center_long_direction',
				'length',
				'max' => 1,
			),
			array('wk_id', 'length', 'max'=>6),
			array(
				'center_lat_degree, center_lat_minute, center_lat_second,
				center_long_degree, center_long_minute, center_long_second',
				'length',
				'max' => 8,
			),
			array('a2_name, a2_latitude, a2_longitude, a2_year_drill', 'length', 'max'=>50),
			array('a2_category', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('a2_id, wk_id, a2_name, a2_category, a2_latitude, a2_longitude, a2_year_drill', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'wks' => array(self::BELONGS_TO, 'WorkingArea', 'wk_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'a2_id' => 'A2',
			'wk_id' => 'Wk',
			'a2_name' => 'A2 Name',
			'a2_category' => 'A2 Category',
			'a2_latitude' => 'A2 Latitude',
			'a2_longitude' => 'A2 Longitude',
			'a2_year_drill' => 'A2 Year Drill',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('a2_id',$this->a2_id);
		$criteria->compare('wk_id',$this->wk_id,true);
		$criteria->compare('a2_name',$this->a2_name,true);
		$criteria->compare('a2_category',$this->a2_category,true);
		$criteria->compare('a2_latitude',$this->a2_latitude,true);
		$criteria->compare('a2_longitude',$this->a2_longitude,true);
		$criteria->compare('a2_year_drill',$this->a2_year_drill,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WellInventory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getLatitude() 
	{
		$wellInventoryBonlat = preg_split("/[,-]+/", $this->a2_latitude);
		
		return $wellInventoryBonlat[0] . html_entity_decode('&deg;') . ' ' . $wellInventoryBonlat[1] . '\'' . ' ' . $wellInventoryBonlat[2] . '"' . ' ' . $wellInventoryBonlat[3];
	}
	
	public function getLongitude()
	{
		$wellInventoryBonlong = preg_split("/[,-]+/", $this->a2_longitude);
	
		return $wellInventoryBonlong[0] . html_entity_decode('&deg;') . ' ' . $wellInventoryBonlong[1] . '\'' . ' ' . $wellInventoryBonlong[2] . '"' . ' ' . $wellInventoryBonlong[3];
	}
}
