<?php

/**
 * This is the model class for table "rsc_resistivity".
 *
 * The followings are the available columns in table 'rsc_resistivity':
 * @property integer $rst_id
 * @property integer $prospect_id
 * @property string $rst_year_survey
 * @property string $rst_survey_method
 * @property string $rst_coverage_area
 * @property string $rst_depth_range
 * @property string $rst_spacing_interval
 * @property double $rst_vol_p90_area
 * @property double $rst_vol_p50_area
 * @property double $rst_vol_p10_area
 * @property string $rst_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Resistivity extends CActiveRecord
{
	public $is_checked;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_resistivity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxResistivity'),
// 			array('rst_year_survey, rst_vol_p90_area, rst_vol_p50_area, rst_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'rst_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'rst_coverage_area, rst_depth_range, rst_spacing_interval,
				rst_vol_p90_area, rst_vol_p50_area, rst_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('rst_year_survey, rst_survey_method, rst_coverage_area, rst_depth_range, rst_spacing_interval', 'length', 'max'=>50),
			array('rst_remark', 'safe'),
// 			array('rst_vol_p90_area, rst_vol_p50_area, rst_vol_p10_area', 'chkDependentResistivity'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rst_id, prospect_id, rst_year_survey, rst_survey_method, rst_coverage_area, rst_depth_range, rst_spacing_interval, rst_vol_p90_area, rst_vol_p50_area, rst_vol_p10_area, rst_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rst_id' => 'Rst',
			'prospect_id' => 'Prospect',
			'rst_year_survey' => 'Resistivity Acquisition Year',
			'rst_survey_method' => 'Rst Survey Method',
			'rst_coverage_area' => 'Rst Coverage Area',
			'rst_depth_range' => 'Rst Depth Range',
			'rst_spacing_interval' => 'Rst Spacing Interval',
			'rst_vol_p90_area' => 'Resistivity P90 Areal Closure Estimation',
			'rst_vol_p50_area' => 'Resistivity P50 Areal Closure Estimation',
			'rst_vol_p10_area' => 'Resistivity P10 Areal Closure Estimation',
			'rst_remark' => 'Rst Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rst_id',$this->rst_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('rst_year_survey',$this->rst_year_survey,true);
		$criteria->compare('rst_survey_method',$this->rst_survey_method,true);
		$criteria->compare('rst_coverage_area',$this->rst_coverage_area,true);
		$criteria->compare('rst_depth_range',$this->rst_depth_range,true);
		$criteria->compare('rst_spacing_interval',$this->rst_spacing_interval,true);
		$criteria->compare('rst_vol_p90_area',$this->rst_vol_p90_area);
		$criteria->compare('rst_vol_p50_area',$this->rst_vol_p50_area);
		$criteria->compare('rst_vol_p10_area',$this->rst_vol_p10_area);
		$criteria->compare('rst_remark',$this->rst_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Resistivity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentResistivity($attribute)
// 	{
// 		if($attribute == 'rst_vol_p10_area')
// 		{
// 			if($this->rst_vol_p10_area != '') {
// 				if($this->rst_vol_p10_area == 0 || $this->rst_vol_p10_area <= $this->rst_vol_p50_area || $this->rst_vol_p10_area <= $this->rst_vol_p90_area)
// 					$this->addError('rst_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than Resistivity P50 Areal Closure Estimation and Resistivity P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'rst_vol_p50_area')
// 		{
// 			if($this->rst_vol_p50_area != '') {
// 				if($this->rst_vol_p50_area == 0 || $this->rst_vol_p50_area <= $this->rst_vol_p90_area)
// 					$this->addError('rst_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than Resistivity P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBoxResistivity()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('rst_year_survey', 'rst_vol_p90_area', 'rst_vol_p50_area', 'rst_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.DependentResistivity');
			$checking = new DependentResistivity;
			$checking->attributes = array('rst_vol_p90_area', 'rst_vol_p50_area', 'rst_vol_p10_area');
			$checking->validate($this);
		}
	}
}
