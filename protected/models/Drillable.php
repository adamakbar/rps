<?php

/**
 * This is the model class for table "rsc_drillable".
 *
 * The followings are the available columns in table 'rsc_drillable':
 * @property integer $dr_id
 * @property integer $prospect_id
 * @property string $dr_por_quantity
 * @property double $dr_por_p90
 * @property string $dr_por_p90_type
 * @property double $dr_por_p50
 * @property string $dr_por_p50_type
 * @property double $dr_por_p10
 * @property string $dr_por_p10_type
 * @property string $dr_por_remark
 * @property string $dr_satur_quantity
 * @property double $dr_satur_p90
 * @property string $dr_satur_p90_type
 * @property double $dr_satur_p50
 * @property string $dr_satur_p50_type
 * @property double $dr_satur_p10
 * @property string $dr_satur_p10_type
 * @property string $dr_satur_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Drillable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_drillable';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dr_por_p90_type, dr_por_p50_type, dr_por_p10_type, dr_satur_p90_type, dr_satur_p50_type, dr_satur_p10_type, dr_por_p90, dr_por_p50, dr_por_p10, dr_satur_p90, dr_satur_p50, dr_satur_p10', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array('dr_por_p90, dr_por_p50, dr_por_p10, dr_satur_p90, dr_satur_p50, dr_satur_p10', 'numerical', 'min'=>0, 'max'=>100),
			array('dr_por_quantity, dr_por_p90_type, dr_por_p50_type, dr_por_p10_type, dr_satur_quantity, dr_satur_p90_type, dr_satur_p50_type, dr_satur_p10_type', 'length', 'max'=>50),
			array('dr_por_remark, dr_satur_remark', 'safe'),
			array('dr_por_p90, dr_por_p50, dr_por_p10, dr_satur_p90, dr_satur_p50, dr_satur_p10', 'chkDependentAdjacentDrillable'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dr_id, prospect_id, dr_por_quantity, dr_por_p90, dr_por_p90_type, dr_por_p50, dr_por_p50_type, dr_por_p10, dr_por_p10_type, dr_por_remark, dr_satur_quantity, dr_satur_p90, dr_satur_p90_type, dr_satur_p50, dr_satur_p50_type, dr_satur_p10, dr_satur_p10_type, dr_satur_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospects' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'plays' => array(self::BELONGS_TO, 'Play', array('play_id'=>'play_id'), 'through'=>'prospects'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dr_id' => 'Dr',
			'prospect_id' => 'Prospect',
			'dr_por_quantity' => 'Dr Por Quantity',
			'dr_por_p90' => 'Porosity P90',
			'dr_por_p90_type' => 'Porosity P90 Type',
			'dr_por_p50' => 'Porosity P50',
			'dr_por_p50_type' => 'Porosity P50 Type',
			'dr_por_p10' => 'Porosity P10',
			'dr_por_p10_type' => 'Porosity P10 Type',
			'dr_por_remark' => 'Dr Por Remark',
			'dr_satur_quantity' => 'Dr Satur Quantity',
			'dr_satur_p90' => 'Saturation P90',
			'dr_satur_p90_type' => 'Saturation P90 Type',
			'dr_satur_p50' => 'Saturation P50',
			'dr_satur_p50_type' => 'Saturation P50 Type',
			'dr_satur_p10' => 'Saturation P10',
			'dr_satur_p10_type' => 'Saturation P10 Type',
			'dr_satur_remark' => 'Dr Satur Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dr_id',$this->dr_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('dr_por_quantity',$this->dr_por_quantity,true);
		$criteria->compare('dr_por_p90',$this->dr_por_p90);
		$criteria->compare('dr_por_p90_type',$this->dr_por_p90_type,true);
		$criteria->compare('dr_por_p50',$this->dr_por_p50);
		$criteria->compare('dr_por_p50_type',$this->dr_por_p50_type,true);
		$criteria->compare('dr_por_p10',$this->dr_por_p10);
		$criteria->compare('dr_por_p10_type',$this->dr_por_p10_type,true);
		$criteria->compare('dr_por_remark',$this->dr_por_remark,true);
		$criteria->compare('dr_satur_quantity',$this->dr_satur_quantity,true);
		$criteria->compare('dr_satur_p90',$this->dr_satur_p90);
		$criteria->compare('dr_satur_p90_type',$this->dr_satur_p90_type,true);
		$criteria->compare('dr_satur_p50',$this->dr_satur_p50);
		$criteria->compare('dr_satur_p50_type',$this->dr_satur_p50_type,true);
		$criteria->compare('dr_satur_p10',$this->dr_satur_p10);
		$criteria->compare('dr_satur_p10_type',$this->dr_satur_p10_type,true);
		$criteria->compare('dr_satur_remark',$this->dr_satur_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Drillable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function chkDependentAdjacentDrillable($attribute)
	{
		if($attribute == 'dr_por_p10')
		{
			if($this->dr_por_p10 == 0 || $this->dr_por_p10 <= $this->dr_por_p50 || $this->dr_por_p10 <= $this->dr_por_p90)
				$this->addError('dr_por_p10', $this->getAttributeLabel($attribute) . ' must greater than Porosity P50 and Porosity P90');
		}
		
		if($attribute == 'dr_por_p50')
		{
			if($this->dr_por_p50 == 0 || $this->dr_por_p50 <= $this->dr_por_p90)
				$this->addError('dr_por_p50', $this->getAttributeLabel($attribute) . ' must greater than Porosity P90');
		}
		
		if($attribute == 'dr_satur_p90')
		{
			if($this->dr_satur_p90 == 0 || $this->dr_satur_p90 <= $this->dr_satur_p50 || $this->dr_satur_p90 <= $this->dr_satur_p10)
				$this->addError('dr_satur_p90', $this->getAttributeLabel($attribute) . ' must greater than Saturation P50 and Saturation P10');
		}
		
		if($attribute == 'dr_satur_p50')
		{
			if($this->dr_satur_p50 == 0 || $this->dr_satur_p50 <= $this->dr_satur_p10)
				$this->addError('dr_satur_p50', $this->getAttributeLabel($attribute) . ' must greater than Saturation P10');
		}
	}
}
