<?php

/**
 * This is the model class for table "adm_working_area".
 *
 * The followings are the available columns in table 'adm_working_area':
 * @property string $wk_id
 * @property string $wk_name
 * @property string $wk_shore
 * @property double $wk_area_current
 * @property string $wk_stage
 * @property string $wk_type
 *
 * The followings are the available model relations:
 * @property AdmAfe[] $admAves
 * @property AdmCommendation[] $admCommendations
 * @property AdmPsc[] $admPscs
 * @property AdmUser[] $admUsers
 * @property FieldPetroleum[] $fieldPetroleums
 * @property RscPlay[] $rscPlays
 * @property RscWellInventory[] $rscWellInventories
 */
class WorkingArea extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_working_area';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wk_id', 'required'),
			array('wk_area_current', 'numerical'),
			array('wk_id', 'length', 'max'=>6),
			array('wk_name, wk_type', 'length', 'max'=>50),
			array('wk_shore', 'length', 'max'=>8),
			array('wk_stage', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wk_id, wk_name, wk_shore, wk_area_current, wk_stage, wk_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'admAves' => array(self::HAS_MANY, 'AdmAfe', 'wk_id'),
			'admCommendations' => array(self::HAS_MANY, 'AdmCommendation', 'wk_id'),
			'admPscs' => array(self::HAS_MANY, 'AdmPsc', 'wk_id'),
			'admUsers' => array(self::HAS_MANY, 'AdmUser', 'wk_id'),
			'fieldPetroleums' => array(self::HAS_MANY, 'FieldPetroleum', 'wk_id'),
			'rscPlays' => array(self::HAS_MANY, 'RscPlay', 'wk_id'),
			'rscWellInventories' => array(self::HAS_MANY, 'RscWellInventory', 'wk_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wk_id' => 'Wk',
			'wk_name' => 'Wk Name',
			'wk_shore' => 'Wk Shore',
			'wk_area_current' => 'Wk Area Current',
			'wk_stage' => 'Wk Stage',
			'wk_type' => 'Wk Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wk_id',$this->wk_id,true);
		$criteria->compare('wk_name',$this->wk_name,true);
		$criteria->compare('wk_shore',$this->wk_shore,true);
		$criteria->compare('wk_area_current',$this->wk_area_current);
		$criteria->compare('wk_stage',$this->wk_stage,true);
		$criteria->compare('wk_type',$this->wk_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkingArea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
