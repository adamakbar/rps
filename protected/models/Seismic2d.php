<?php

/**
 * This is the model class for table "rsc_seismic_2d".
 *
 * The followings are the available columns in table 'rsc_seismic_2d':
 * @property integer $s2d_id
 * @property integer $prospect_id
 * @property string $s2d_vintage_number
 * @property string $s2d_year_survey
 * @property string $s2d_total_crossline
 * @property string $s2d_seismic_line
 * @property string $s2d_average_interval
 * @property string $s2d_record_length_ms
 * @property string $s2d_record_length_ft
 * @property string $s2d_year_late_process
 * @property string $s2d_late_method
 * @property string $s2d_img_quality
 * @property string $s2d_top_depth_ft
 * @property string $s2d_top_depth_ms
 * @property string $s2d_bot_depth_ft
 * @property string $s2d_bot_depth_ms
 * @property string $s2d_depth_spill
 * @property string $s2d_depth_estimate
 * @property string $s2d_depth_estimate_analog
 * @property string $s2d_depth_estimate_spill
 * @property string $s2d_depth_low
 * @property string $s2d_depth_low_analog
 * @property string $s2d_depth_low_spill
 * @property string $s2d_formation_thickness
 * @property string $s2d_gross_thickness
 * @property string $s2d_avail_pay
 * @property double $s2d_net_p90_thickness
 * @property double $s2d_net_p90_vsh
 * @property double $s2d_net_p90_por
 * @property double $s2d_net_p90_satur
 * @property double $s2d_net_p50_thickness
 * @property double $s2d_net_p50_vsh
 * @property double $s2d_net_p50_por
 * @property double $s2d_net_p50_satur
 * @property double $s2d_net_p10_thickness
 * @property double $s2d_net_p10_vsh
 * @property double $s2d_net_p10_por
 * @property double $s2d_net_p10_satur
 * @property double $s2d_vol_p90_area
 * @property double $s2d_vol_p50_area
 * @property double $s2d_vol_p10_area
 * @property string $s2d_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Seismic2d extends CActiveRecord
{
	public $is_checked;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_seismic_2d';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBox2dSeismic'),
// 			array('s2d_year_survey, s2d_gross_thickness, s2d_net_p90_thickness, s2d_net_p50_thickness, s2d_net_p10_thickness, s2d_vol_p90_area, s2d_vol_p50_area, s2d_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				's2d_year_survey, s2d_year_late_process',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				's2d_total_crossline, s2d_seismic_line, s2d_average_interval, s2d_record_length_ms, s2d_record_length_ft,
				s2d_top_depth_ft, s2d_top_depth_ms, s2d_bot_depth_ft, s2d_bot_depth_ms, s2d_depth_spill, s2d_depth_estimate,
				s2d_depth_low, s2d_formation_thickness, s2d_gross_thickness,
				s2d_net_p90_thickness, s2d_net_p50_thickness, s2d_net_p10_thickness,
				s2d_vol_p90_area, s2d_vol_p50_area, s2d_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				's2d_depth_estimate_spill, s2d_depth_low_spill, s2d_net_p90_vsh, s2d_net_p50_vsh, s2d_net_p10_vsh, s2d_net_p90_por, s2d_net_p50_por, s2d_net_p10_por, s2d_net_p90_satur, s2d_net_p50_satur, s2d_net_p10_satur',
				'numerical',
				'min'=>0,
				'max'=>100,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('s2d_vintage_number, s2d_year_survey, s2d_total_crossline, s2d_seismic_line, s2d_average_interval, s2d_record_length_ms, s2d_record_length_ft, s2d_year_late_process, s2d_late_method, s2d_img_quality, s2d_top_depth_ft, s2d_top_depth_ms, s2d_bot_depth_ft, s2d_bot_depth_ms, s2d_depth_spill, s2d_depth_estimate, s2d_depth_estimate_analog, s2d_depth_estimate_spill, s2d_depth_low, s2d_depth_low_analog, s2d_depth_low_spill, s2d_formation_thickness, s2d_gross_thickness, s2d_avail_pay', 'length', 'max'=>50),
			array('s2d_remark', 'safe'),
// 			array('s2d_net_p90_thickness, s2d_net_p50_thickness, s2d_net_p10_thickness, s2d_net_p90_vsh, s2d_net_p50_vsh, s2d_net_p10_vsh, s2d_net_p90_por, s2d_net_p50_por, s2d_net_p10_por, s2d_net_p90_satur, s2d_net_p50_satur, s2d_net_p10_satur, s2d_vol_p90_area, s2d_vol_p50_area, s2d_vol_p10_area', 'chkDependent2dSeismic'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('s2d_id, prospect_id, s2d_vintage_number, s2d_year_survey, s2d_total_crossline, s2d_seismic_line, s2d_average_interval, s2d_record_length_ms, s2d_record_length_ft, s2d_year_late_process, s2d_late_method, s2d_img_quality, s2d_top_depth_ft, s2d_top_depth_ms, s2d_bot_depth_ft, s2d_bot_depth_ms, s2d_depth_spill, s2d_depth_estimate, s2d_depth_estimate_analog, s2d_depth_estimate_spill, s2d_depth_low, s2d_depth_low_analog, s2d_depth_low_spill, s2d_formation_thickness, s2d_gross_thickness, s2d_avail_pay, s2d_net_p90_thickness, s2d_net_p90_vsh, s2d_net_p90_por, s2d_net_p90_satur, s2d_net_p50_thickness, s2d_net_p50_vsh, s2d_net_p50_por, s2d_net_p50_satur, s2d_net_p10_thickness, s2d_net_p10_vsh, s2d_net_p10_por, s2d_net_p10_satur, s2d_vol_p90_area, s2d_vol_p50_area, s2d_vol_p10_area, s2d_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			's2d_id' => 'S2d',
			'prospect_id' => 'Prospect',
			's2d_vintage_number' => 'S2d Vintage Number',
			's2d_year_survey' => '2d Survey Acquisition Year',
			's2d_total_crossline' => 'S2d Total Crossline',
			's2d_seismic_line' => 'S2d Seismic Line',
			's2d_average_interval' => 'S2d Average Interval',
			's2d_record_length_ms' => 'S2d Record Length Ms',
			's2d_record_length_ft' => 'S2d Record Length Ft',
			's2d_year_late_process' => 'S2d Year Late Process',
			's2d_late_method' => 'S2d Late Method',
			's2d_img_quality' => 'S2d Img Quality',
			's2d_top_depth_ft' => 'S2d Top Depth Ft',
			's2d_top_depth_ms' => 'S2d Top Depth Ms',
			's2d_bot_depth_ft' => 'S2d Bot Depth Ft',
			's2d_bot_depth_ms' => 'S2d Bot Depth Ms',
			's2d_depth_spill' => 'S2d Depth Spill',
			's2d_depth_estimate' => 'S2d Depth Estimate',
			's2d_depth_estimate_analog' => 'S2d Depth Estimate Analog',
			's2d_depth_estimate_spill' => 'S2d Depth Estimate Spill',
			's2d_depth_low' => 'S2d Depth Low',
			's2d_depth_low_analog' => 'S2d Depth Low Analog',
			's2d_depth_low_spill' => 'S2d Depth Low Spill',
			's2d_formation_thickness' => 'S2d Formation Thickness',
			's2d_gross_thickness' => '2d Survey Gross Sand or Reservoir Thickness',
			's2d_avail_pay' => 'S2d Avail Pay',
			's2d_net_p90_thickness' => '2d Survey P90 Net Pay Thickness',
			's2d_net_p90_vsh' => '2d Survey P90 Vsh Cut-off',
			's2d_net_p90_por' => '2d Survey P90 Porosity Cut-off',
			's2d_net_p90_satur' => '2d Survey P90 Saturation Cut-Off',
			's2d_net_p50_thickness' => '2d Survey P50 Net Pay Thickness',
			's2d_net_p50_vsh' => '2d Survey P50 Vsh Cut-off',
			's2d_net_p50_por' => '2d Survey P50 Porosity Cut-off',
			's2d_net_p50_satur' => '2d Survey P50 Saturation Cut-Off',
			's2d_net_p10_thickness' => '2d Survey P10 Net Pay Thickness',
			's2d_net_p10_vsh' => '2d Survey P10 Vsh Cut-off',
			's2d_net_p10_por' => '2d Survey P10 Porosity Cut-off',
			's2d_net_p10_satur' => '2d Survey P10 Saturation Cut-Off',
			's2d_vol_p90_area' => '2d Survey P90 Areal Closure Estimation',
			's2d_vol_p50_area' => '2d Survey P50 Areal Closure Estimation',
			's2d_vol_p10_area' => '2d Survey P10 Areal Closure Estimation',
			's2d_remark' => 'S2d Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('s2d_id',$this->s2d_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('s2d_vintage_number',$this->s2d_vintage_number,true);
		$criteria->compare('s2d_year_survey',$this->s2d_year_survey,true);
		$criteria->compare('s2d_total_crossline',$this->s2d_total_crossline,true);
		$criteria->compare('s2d_seismic_line',$this->s2d_seismic_line,true);
		$criteria->compare('s2d_average_interval',$this->s2d_average_interval,true);
		$criteria->compare('s2d_record_length_ms',$this->s2d_record_length_ms,true);
		$criteria->compare('s2d_record_length_ft',$this->s2d_record_length_ft,true);
		$criteria->compare('s2d_year_late_process',$this->s2d_year_late_process,true);
		$criteria->compare('s2d_late_method',$this->s2d_late_method,true);
		$criteria->compare('s2d_img_quality',$this->s2d_img_quality,true);
		$criteria->compare('s2d_top_depth_ft',$this->s2d_top_depth_ft,true);
		$criteria->compare('s2d_top_depth_ms',$this->s2d_top_depth_ms,true);
		$criteria->compare('s2d_bot_depth_ft',$this->s2d_bot_depth_ft,true);
		$criteria->compare('s2d_bot_depth_ms',$this->s2d_bot_depth_ms,true);
		$criteria->compare('s2d_depth_spill',$this->s2d_depth_spill,true);
		$criteria->compare('s2d_depth_estimate',$this->s2d_depth_estimate,true);
		$criteria->compare('s2d_depth_estimate_analog',$this->s2d_depth_estimate_analog,true);
		$criteria->compare('s2d_depth_estimate_spill',$this->s2d_depth_estimate_spill,true);
		$criteria->compare('s2d_depth_low',$this->s2d_depth_low,true);
		$criteria->compare('s2d_depth_low_analog',$this->s2d_depth_low_analog,true);
		$criteria->compare('s2d_depth_low_spill',$this->s2d_depth_low_spill,true);
		$criteria->compare('s2d_formation_thickness',$this->s2d_formation_thickness,true);
		$criteria->compare('s2d_gross_thickness',$this->s2d_gross_thickness,true);
		$criteria->compare('s2d_avail_pay',$this->s2d_avail_pay,true);
		$criteria->compare('s2d_net_p90_thickness',$this->s2d_net_p90_thickness);
		$criteria->compare('s2d_net_p90_vsh',$this->s2d_net_p90_vsh);
		$criteria->compare('s2d_net_p90_por',$this->s2d_net_p90_por);
		$criteria->compare('s2d_net_p90_satur',$this->s2d_net_p90_satur);
		$criteria->compare('s2d_net_p50_thickness',$this->s2d_net_p50_thickness);
		$criteria->compare('s2d_net_p50_vsh',$this->s2d_net_p50_vsh);
		$criteria->compare('s2d_net_p50_por',$this->s2d_net_p50_por);
		$criteria->compare('s2d_net_p50_satur',$this->s2d_net_p50_satur);
		$criteria->compare('s2d_net_p10_thickness',$this->s2d_net_p10_thickness);
		$criteria->compare('s2d_net_p10_vsh',$this->s2d_net_p10_vsh);
		$criteria->compare('s2d_net_p10_por',$this->s2d_net_p10_por);
		$criteria->compare('s2d_net_p10_satur',$this->s2d_net_p10_satur);
		$criteria->compare('s2d_vol_p90_area',$this->s2d_vol_p90_area);
		$criteria->compare('s2d_vol_p50_area',$this->s2d_vol_p50_area);
		$criteria->compare('s2d_vol_p10_area',$this->s2d_vol_p10_area);
		$criteria->compare('s2d_remark',$this->s2d_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Seismic2d the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependent2dSeismic($attribute)
// 	{
// 		if($attribute == 's2d_net_p10_thickness')
// 		{
// 			if($this->s2d_net_p10_thickness == 0 || $this->s2d_net_p10_thickness <= $this->s2d_net_p50_thickness || $this->s2d_net_p10_thickness <= $this->s2d_net_p90_thickness)
// 				$this->addError('s2d_net_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater 2d Survey P50 Net Pay Thickness and 2d Survey P90 Net Pay Thickness');
// 		}
		
// 		if($attribute == 's2d_net_p50_thickness')
// 		{
// 			if($this->s2d_net_p50_thickness == 0 || $this->s2d_net_p50_thickness <= $this->s2d_net_p90_thickness)
// 				$this->addError('s2d_net_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater 2d Survey P90 Net Pay Thickness');
// 		}
		
// 		if($attribute == 's2d_net_p10_vsh')
// 		{
// 			if($this->s2d_net_p10_vsh != '') {
// 				if($this->s2d_net_p10_vsh == 0 || $this->s2d_net_p10_vsh <= $this->s2d_net_p50_vsh || $this->s2d_net_p10_vsh <= $this->s2d_net_p90_vsh)
// 					$this->addError('s2d_net_p10_vsh', $this->getAttributeLabel($attribute) . ' must greater than 2d Survey P50 Vsh Cut-off and 2d Survey P90 Vsh Cut-off');
// 			}
// 		}
		
// 		if($attribute == 's2d_net_p50_vsh')
// 		{
// 			if($this->s2d_net_p50_vsh != '') {
// 				if($this->s2d_net_p50_vsh == 0 || $this->s2d_net_p50_vsh <= $this->s2d_net_p90_vsh)
// 					$this->addError('s2d_net_p50_vsh', $this->getAttributeLabel($attribute) . ' must greater than 2d Survey P90 Vsh Cut-off');
// 			}
// 		}
		
// 		if($attribute == 's2d_net_p10_por')
// 		{
// 			if($this->s2d_net_p10_por != '') {
// 				if($this->s2d_net_p10_por == 0 || $this->s2d_net_p10_por <= $this->s2d_net_p50_por || $this->s2d_net_p10_por <= $this->s2d_net_p90_por)
// 					$this->addError('s2d_net_p10_por', $this->getAttributeLabel($attribute) . ' must greater than 2d Survey P50 Porosity Cut-off and 2d Survey P90 Porosity Cut-off');
// 			}
// 		}
		
// 		if($attribute == 's2d_net_p50_por')
// 		{
// 			if($this->s2d_net_p50_por != '') {
// 				if($this->s2d_net_p50_por == 0 || $this->s2d_net_p50_por <= $this->s2d_net_p90_por)
// 					$this->addError('s2d_net_p50_por', $this->getAttributeLabel($attribute) . ' must greater 2d Survey P90 Porosity Cut-off');
// 			}
// 		}
		
// 		if($attribute == 's2d_net_p10_satur')
// 		{
// 			if($this->s2d_net_p10_satur != '') {
// 				if($this->s2d_net_p10_satur == 0 || $this->s2d_net_p10_satur <= $this->s2d_net_p50_satur || $this->s2d_net_p10_satur <= $this->s2d_net_p90_satur)
// 					$this->addError('s2d_net_p10_satur', $this->getAttributeLabel($attribute) . ' must greater 2d Survey P50 Saturation Cut-Off and 2d Survey P90 Saturation Cut-Off');
// 			}
// 		}
		
// 		if($attribute == 's2d_net_p50_satur')
// 		{
// 			if($this->s2d_net_p50_satur != '') {
// 				if($this->s2d_net_p50_satur == 0 || $this->s2d_net_p50_satur <= $this->s2d_net_p90_satur)
// 					$this->addError('s2d_net_p50_satur', $this->getAttributeLabel($attribute) . ' must greater 2d Survey P90 Saturation Cut-Off');
// 			}
// 		}
		
// 		if($attribute == 's2d_vol_p10_area')
// 		{
// 			if($this->s2d_vol_p10_area != '') {
// 				if($this->s2d_vol_p10_area == 0 || $this->s2d_vol_p10_area <= $this->s2d_vol_p50_area || $this->s2d_vol_p10_area <= $this->s2d_vol_p90_area)
// 					$this->addError('s2d_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than 2d Survey P50 Areal Closure Estimation and 2d Survey P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 's2d_vol_p50_area')
// 		{
// 			if($this->s2d_vol_p50_area != '') {
// 				if($this->s2d_vol_p50_area == 0 || $this->s2d_vol_p50_area <= $this->s2d_vol_p90_area)
// 					$this->addError('s2d_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than 2d Survey P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBox2dSeismic()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('s2d_year_survey', 's2d_total_crossline', 's2d_average_interval', 's2d_gross_thickness', 's2d_net_p90_thickness', 's2d_net_p50_thickness', 's2d_net_p10_thickness', 's2d_vol_p90_area', 's2d_vol_p50_area', 's2d_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.Dependent2dSeismic');
			$checking = new Dependent2dSeismic;
			$checking->attributes = array('s2d_net_p90_thickness', 's2d_net_p50_thickness', 's2d_net_p10_thickness', 's2d_net_p90_vsh', 's2d_net_p50_vsh', 's2d_net_p10_vsh', 's2d_net_p90_por', 's2d_net_p50_por', 's2d_net_p10_por', 's2d_net_p90_satur', 's2d_net_p50_satur', 's2d_net_p10_satur', 's2d_vol_p90_area', 's2d_vol_p50_area', 's2d_vol_p10_area');
			$checking->validate($this);
		}
	}
}
