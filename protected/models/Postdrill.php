<?php

/**
 * This is the model class for table "rsc_postdrill".
 *
 * The followings are the available columns in table 'rsc_postdrill':
 * @property integer $pd_id
 * @property integer $prospect_id
 * @property string $pd_name_revise
 * @property string $pd_name_revise_date
 * @property integer $pd_total_well
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 * @property RscWellPostdrill[] $rscWellPostdrills
 */
class Postdrill extends CActiveRecord
{

	public $basin_name;
	public $postdrill_name;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_postdrill';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prospect_id, pd_total_well', 'numerical', 'integerOnly'=>true),
			array('pd_name_revise', 'length', 'max'=>50),
			array('pd_name_revise_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pd_id, prospect_id, pd_name_revise, pd_name_revise_date, pd_total_well', 'safe', 'on'=>'search'),
			array('pd_id, basin_name, postdrill_name, prospect_id, pd_name_revise, pd_name_revise_date, pd_total_well', 'safe', 'on'=>'searchRelatedPostdrill'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospects' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'prospectg' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'rscWellPostdrills' => array(self::HAS_MANY, 'RscWellPostdrill', 'pd_id'),
			'plays' => array(self::BELONGS_TO, 'Play', array('play_id'=>'play_id'), 'through'=>'prospects'),
			'gcf' => array(self::BELONGS_TO, 'Gcf', array('gcf_id'=>'gcf_id'), 'through'=>'prospectg'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pd_id' => 'Pd',
			'prospect_id' => 'Prospect',
			'pd_name_revise' => 'Pd Name Revise',
			'pd_name_revise_date' => 'Pd Name Revise Date',
			'pd_total_well' => 'Pd Total Well',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pd_id',$this->pd_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('pd_name_revise',$this->pd_name_revise,true);
		$criteria->compare('pd_name_revise_date',$this->pd_name_revise_date,true);
		$criteria->compare('pd_total_well',$this->pd_total_well);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchRelatedPostdrill() {

	    $postdrillCriteria = new CDbCriteria;
	    $postdrillCriteria->with = array('plays', 'prospects', 'plays.basin', 'gcf');
	    $postdrillCriteria->condition = 'plays.wk_id ="' . Yii::app()->user->wk_id . '"';
    	$postdrillCriteria->addCondition('prospects.prospect_is_deleted = 0', 'AND');
    	$postdrillCriteria->addCondition('basin.basin_name LIKE "%' . $this->basin_name . '%"', 'AND');
    	$postdrillCriteria->addCondition('CONCAT(
	        prospects.structure_name, " @ ", gcf.gcf_res_lithology, " - ", gcf.gcf_res_formation, " - ", 
	        gcf.gcf_res_age_serie, " ", gcf.gcf_res_age_system, " - ", gcf.gcf_res_depos_env, " - ", gcf.gcf_trap_type
	      ) LIKE "%' . $this->postdrill_name . '%"', 'AND');

	    return new CActiveDataProvider(get_class($this), array(
	        'criteria'=>$postdrillCriteria,
	        'sort'=>array(
	            'defaultOrder'=>'basin_urut'
	          )
	      ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Postdrill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
