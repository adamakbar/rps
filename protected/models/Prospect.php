<?php

/**
 * This is the model class for table "rsc_prospect".
 *
 * The followings are the available columns in table 'rsc_prospect':
 * @property integer $prospect_id
 * @property integer $play_id
 * @property integer $gcf_id
 * @property string $prospect_type
 * @property string $structure_name
 * @property string $prospect_clarified
 * @property string $prospect_year_seismic
 * @property string $prospect_processing_type
 * @property string $prospect_year_late
 * @property string $prospect_date_initiate
 * @property string $prospect_latitude
 * @property string $prospect_longitude
 * @property string $prospect_shore
 * @property string $prospect_terrain
 * @property string $prospect_near_field
 * @property string $prospect_near_infra_structure
 * @property integer $prospect_update_from
 * @property string $prospect_submit_status
 * @property string $prospect_submit_date
 * @property integer $prospect_submit_revision
 *
 * The followings are the available model relations:
 * @property RscDiscovery[] $rscDiscoveries
 * @property RscDrillable[] $rscDrillables
 * @property RscElectromagnetic[] $rscElectromagnetics
 * @property RscGeochemistry[] $rscGeochemistries
 * @property RscGeological[] $rscGeologicals
 * @property RscGravity[] $rscGravities
 * @property RscOther[] $rscOthers
 * @property RscPostdrill[] $rscPostdrills
 * @property RscPlay $play
 * @property RscGcf $gcf
 * @property RscResistivity[] $rscResistivities
 * @property RscSeismic2d[] $rscSeismic2ds
 * @property RscSeismic3d[] $rscSeismic3ds
 */
class Prospect extends CActiveRecord {
  public $center_lat_degree;
  public $center_lat_minute;
  public $center_lat_second;
  public $center_lat_direction;
  public $center_long_degree;
  public $center_long_minute;
  public $center_long_second;
  public $center_long_direction;
  public $basin_name;
  public $drillable_name;
  
  public $well_name;
  public $zone_name;
  
  /**
   *
   * @return string the associated database table name
   */
  public function tableName() {
    return 'rsc_prospect';
  }
  
  /**
   *
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array (
        array (
            'structure_name, play_id, prospect_clarified, prospect_date_initiate, center_lat_degree, center_lat_minute, center_lat_second, center_lat_direction,
            center_long_degree, center_long_minute, center_long_second, center_long_direction, prospect_shore, prospect_terrain, prospect_near_field,
            prospect_near_infra_structure',
            'required' 
        ),
//         array(
//           'structure_name',
//           'check_structure_name_prospect'
//         ),
        array(
          'well_name',
          'required',
          'on'=>'postdrill',
        ),
        array(
          'well_name',
          'required',
          'on'=>'discovery',
        ),
        array(
        	'center_lat_degree',
        	'numerical',
        	'min'=>0,
        	'max'=>20,
        	'numberPattern'=>'/^([1-9]{0,1})([0-9]{1})?$/'
        ),
        array(
        	'center_long_degree',
        	'numerical',
        	'min'=>90,
        	'max'=>145,
        	'numberPattern'=>'/^([1-9]{0,2})([0-9]{2})?$/'
        ),
        array(
        	'center_lat_minute, center_long_minute',
        	'numerical',
        	'min'=>0,
        	'max'=>59
        ),
        array(
        	'center_lat_second, center_long_second',
        	'numerical',
        	'min'=>0,
        	'max'=>59.999,
        	'numberPattern'=>'/^\d{1,6}(\.\d{1,3})?$/'
        ),
        array (
            'play_id, gcf_id, prospect_update_from, prospect_submit_revision',
            'numerical',
            'integerOnly' => true 
        ),
        array(
          'center_lat_direction, center_long_direction',
          'length',
          'max'=>1
        ),
        array (
            'prospect_type, structure_name, prospect_clarified, prospect_year_seismic, prospect_processing_type, prospect_year_late, prospect_latitude, prospect_longitude, prospect_terrain, prospect_near_field, prospect_near_infra_structure, prospect_submit_status',
            'length',
            'max' => 50 
        ),
        array (
            'center_lat_degree, center_lat_minute, center_lat_second,
            center_long_degree, center_long_minute, center_long_second,
            prospect_shore',
            'length',
            'max' => 8 
        ),
        array (
            'prospect_date_initiate, prospect_submit_date',
            'safe' 
        ),
        // The following rule is used by search().
        // @todo Please remove those attributes that should not be searched.
        array (
            'prospect_id, play_id, gcf_id, prospect_type, structure_name, prospect_clarified, prospect_year_seismic, prospect_processing_type, prospect_year_late, prospect_date_initiate, prospect_latitude, prospect_longitude, prospect_shore, prospect_terrain, prospect_near_field, prospect_near_infra_structure, prospect_update_from, prospect_submit_status, prospect_submit_date, prospect_submit_revision',
            'safe',
            'on' => 'search' 
        ),
        array (
            'prospect_id, play_id, gcf_id, basin_name, drillable_name, prospect_type, structure_name, prospect_clarified, prospect_year_seismic, prospect_processing_type, prospect_year_late, prospect_date_initiate, prospect_latitude, prospect_longitude, prospect_shore, prospect_terrain, prospect_near_field, prospect_near_infra_structure, prospect_update_from, prospect_submit_status, prospect_submit_date, prospect_submit_revision',
            'safe',
            'on' => 'searchRelatedDrillable' 
        )
    );
  }
  
  /**
   *
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array (
        'rscDiscoveries' => array (
            self::HAS_MANY,
            'RscDiscovery',
            'prospect_id' 
        ),
        'rscDrillables' => array (
            self::HAS_MANY,
            'RscDrillable',
            'prospect_id' 
        ),
        'rscElectromagnetics' => array (
            self::HAS_MANY,
            'RscElectromagnetic',
            'prospect_id' 
        ),
        'rscGeochemistries' => array (
            self::HAS_MANY,
            'RscGeochemistry',
            'prospect_id' 
        ),
        'rscGeologicals' => array (
            self::HAS_MANY,
            'RscGeological',
            'prospect_id' 
        ),
        'rscGravities' => array (
            self::HAS_MANY,
            'RscGravity',
            'prospect_id' 
        ),
        'rscOthers' => array (
            self::HAS_MANY,
            'RscOther',
            'prospect_id' 
        ),
        'rscPostdrills' => array (
            self::HAS_MANY,
            'RscPostdrill',
            'prospect_id' 
        ),
        'plays' => array (
            self::BELONGS_TO,
            'Play',
            'play_id' 
        ),
        'gcf' => array (
            self::BELONGS_TO,
            'Gcf',
            'gcf_id' 
        ),
        'rscResistivities' => array (
            self::HAS_MANY,
            'RscResistivity',
            'prospect_id' 
        ),
        'rscSeismic2ds' => array (
            self::HAS_MANY,
            'RscSeismic2d',
            'prospect_id' 
        ),
        'rscSeismic3ds' => array (
            self::HAS_MANY,
            'RscSeismic3d',
            'prospect_id' 
        ) 
    );
  }
  
  /**
   *
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array (
        'center_lat_degree'=>'Center Latitude Degree',
        'center_lat_minute'=>'Center Latitude Minute',
        'center_lat_second'=>'Center Latitude Second',
        'center_lat_direction'=>'Center Latitude Direction',
        'center_long_degree'=>'Center Longitude Degree',
        'center_long_minute'=>'Center Longitude Minute',
        'center_long_second'=>'Center Longitude Second',
        'prospect_id' => 'Prospect',
        'play_id' => 'Play Name',
        'gcf_id' => 'Gcf',
        'prospect_type' => 'Prospect Type',
        'structure_name' => 'Structure Name',
        'prospect_clarified' => 'Clarified by',
        'prospect_year_seismic' => 'Seismic Year',
        'prospect_processing_type' => 'Processing Type',
        'prospect_year_late' => 'Latest Year of Processing',
        'prospect_date_initiate' => 'Prospect Name Initiation Date',
        'prospect_latitude' => 'Prospect Latitude',
        'prospect_longitude' => 'Prospect Longitude',
        'prospect_shore' => 'Onshore or Offshore',
        'prospect_terrain' => 'Terrain',
        'prospect_near_field' => 'Nearby Field',
        'prospect_near_infra_structure' => 'Nearby Infra Structure',
        'prospect_update_from' => 'Prospect Update From',
        'prospect_submit_status' => 'Prospect Submit Status',
        'prospect_submit_date' => 'Prospect Submit Date',
        'prospect_submit_revision' => 'Prospect Submit Revision' 
    );
  }
  
  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   *         based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.
    $criteria = new CDbCriteria ();
    
    $criteria->compare ( 'prospect_id', $this->prospect_id );
    $criteria->compare ( 'play_id', $this->play_id );
    $criteria->compare ( 'gcf_id', $this->gcf_id );
    $criteria->compare ( 'prospect_type', $this->prospect_type, true );
    $criteria->compare ( 'structure_name', $this->structure_name, true );
    $criteria->compare ( 'prospect_clarified', $this->prospect_clarified, true );
    $criteria->compare ( 'prospect_year_seismic', $this->prospect_year_seismic, true );
    $criteria->compare ( 'prospect_processing_type', $this->prospect_processing_type, true );
    $criteria->compare ( 'prospect_year_late', $this->prospect_year_late, true );
    $criteria->compare ( 'prospect_date_initiate', $this->prospect_date_initiate, true );
    $criteria->compare ( 'prospect_latitude', $this->prospect_latitude, true );
    $criteria->compare ( 'prospect_longitude', $this->prospect_longitude, true );
    $criteria->compare ( 'prospect_shore', $this->prospect_shore, true );
    $criteria->compare ( 'prospect_terrain', $this->prospect_terrain, true );
    $criteria->compare ( 'prospect_near_field', $this->prospect_near_field, true );
    $criteria->compare ( 'prospect_near_infra_structure', $this->prospect_near_infra_structure, true );
    $criteria->compare ( 'prospect_update_from', $this->prospect_update_from );
    $criteria->compare ( 'prospect_submit_status', $this->prospect_submit_status, true );
    $criteria->compare ( 'prospect_submit_date', $this->prospect_submit_date, true );
    $criteria->compare ( 'prospect_submit_revision', $this->prospect_submit_revision );
    
    return new CActiveDataProvider ( $this, array (
        'criteria' => $criteria 
    ) );
  }

  public function searchRelatedDrillable() {
    $criteria = new CDbCriteria;
    $criteria->select="max(b.prospect_submit_date)";
    $criteria->alias = 'b';
    $criteria->condition="b.prospect_update_from = t.prospect_update_from";
    $subQuery=$this->getCommandBuilder()->createFindCommand($this->getTableSchema(),$criteria)->getText();
    
    $mainCriteria = new CDbCriteria;
    $mainCriteria->together = true;
    $mainCriteria->with = array('plays', 'plays.basin', 'gcf');
    $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
    $mainCriteria->addCondition('prospect_is_deleted = 0', 'AND');
    $mainCriteria->addCondition('prospect_type = "drillable"', 'AND');
    $mainCriteria->addCondition('prospect_submit_date = (' . $subQuery . ')', 'AND');
    $mainCriteria->addCondition('basin.basin_name LIKE "%' . $this->basin_name . '%"', 'AND');
    $mainCriteria->addCondition('CONCAT(
        structure_name, " @ ", gcf.gcf_res_lithology, " - ", gcf.gcf_res_formation, " - ", 
        gcf.gcf_res_age_serie, " ", gcf.gcf_res_age_system, " - ", gcf.gcf_res_depos_env, " - ", gcf.gcf_trap_type
      ) LIKE "%' . $this->drillable_name . '%"', 'AND');
    $mainCriteria->group = 'prospect_update_from';
    
    return new CActiveDataProvider(get_class($this), array(
        'criteria'=>$mainCriteria,
        'sort'=>array(
            'defaultOrder'=>'basin_urut'
          )
        ));

  }
  
  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * 
   * @param string $className
   *          active record class name.
   * @return Prospect the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model ( $className );
  }
  
  /*penamaan prospect name / closure name pada list drillable  */
  public function getProspectName1($gcf_id = null)
  {
    $sql = "select structure_name, 
          IF(gcf_res_lithology = 'Unknown', '', gcf_res_lithology) gcf_res_lithology, 
          IF(gcf_res_depos_env ='Others', '', gcf_res_depos_env) gcf_res_depos_env,
          IF(gcf_res_formation_serie ='Not Available', '', gcf_res_formation_serie) gcf_res_formation_serie,
          gcf_res_formation, 
          gcf_res_age_system, 
          IF(gcf_res_age_serie = 'Not Available', '', gcf_res_age_serie) gcf_res_age_serie,
          IF(gcf_trap_type = 'Unknown', '', gcf_trap_type) gcf_trap_type
        from rsc_prospect inner join rsc_gcf on rsc_prospect.gcf_id = rsc_gcf.gcf_id where rsc_prospect.gcf_id=" . $gcf_id;
    $command = Yii::app()->db->createCommand($sql);
    $prospectData = $command->queryRow();
    return '<b>' . $prospectData['structure_name'] . '</b> @ ' . $prospectData['gcf_res_lithology'] . ' - <b>' . $prospectData['gcf_res_formation_serie'] . ' ' . $prospectData['gcf_res_formation'] . '</b> - ' . $prospectData['gcf_res_age_serie'] . ' ' . $prospectData['gcf_res_age_system'] . ' - ' .  $prospectData['gcf_res_depos_env'] . ' - ' . $prospectData['gcf_trap_type'];
  }
  
  /*penamaan prospect name / closure name pada list postdrill dan discovery*/
  public function getProspectName2($prospect_id = null)
  {
    $sql = "select structure_name, 
          IF(gcf_res_lithology = 'Unknown', '', gcf_res_lithology) gcf_res_lithology, 
          IF(gcf_res_depos_env ='Others', '', gcf_res_depos_env) gcf_res_depos_env, 
          IF(gcf_res_formation_serie ='Not Available', '', gcf_res_formation_serie) gcf_res_formation_serie, 
          gcf_res_formation, 
          gcf_res_age_system, 
          IF(gcf_res_age_serie = 'Not Available', '', gcf_res_age_serie) gcf_res_age_serie,
          IF(gcf_trap_type = 'Unknown', '', gcf_trap_type) gcf_trap_type
        from rsc_prospect inner join rsc_gcf on rsc_prospect.gcf_id = rsc_gcf.gcf_id where rsc_prospect.prospect_id=" . $prospect_id;
    $command = Yii::app()->db->createCommand($sql);
    $prospectData = $command->queryRow();
    return '<b>' . $prospectData['structure_name'] . '</b> @ ' . $prospectData['gcf_res_lithology'] . ' - <b>' . $prospectData['gcf_res_formation_serie'] . ' ' . $prospectData['gcf_res_formation'] . '</b> - ' . $prospectData['gcf_res_age_serie'] . ' ' . $prospectData['gcf_res_age_system'] . ' - ' . $prospectData['gcf_res_depos_env'] . ' - ' . $prospectData['gcf_trap_type'];
  }
  
//   public function check_structure_name_prospect()
//   {
//   	$regex = "/(?:lead|drillable|discovery)/";
  
//   	$sql = "select rsc_play.wk_id, structure_name 
//   			from rsc_lead 
//   			inner join rsc_play on rsc_play.play_id = rsc_lead.play_id 
//   			where rsc_lead.structure_name = '" . $this->structure_name . "'";
//   	$command = Yii::app()->db->createCommand($sql);
//   	$data = $command->queryRow();
  
//   	$check2 = 0;
//   	if(is_array($data))
//   	{
//   		if(Yii::app()->user->wk_id != $data['wk_id'])
//   			$check2 = 1;
//   	}
  
  
//   	if($this->prospect_type == 'drillable')
//   	{
//   		$sql2 = "select prospect_id, structure_name 
//   				from rsc_prospect 
//   				where structure_name = '" . $this->structure_name . "' and prospect_type = 'drillable'";
//   		$command2 = Yii::app()->db->createCommand($sql2);
//   		$data2 = $command2->queryRow();
  			
//   		$sql3 = "select wk_id, prospect_id, structure_name 
//   				from rsc_prospect, rsc_play 
//   				where structure_name = '" . $this->structure_name . "' and rsc_prospect.play_id = rsc_play.play_id";
//   		$command3 = Yii::app()->db->createCommand($sql3);
//   		$data3 = $command3->queryAll();
  			
//   		$check = 0;
//   		if($this->prospect_id != null)
//   		{
//   			if($data2['prospect_id'] != $this->prospect_id)
//   			{
//   				$check = 1;
//   			}
//   		} else if(is_array($data2))
//   		{
//   			$check = 1;
//   		} else
//   		{
//   			foreach ($data3 as $datas3)
//   			{
//   				if($datas3['wk_id'] != Yii::app()->user->wk_id)
//   				{
//   					$check = 1;
//   					break;
//   				}
//   			}
//   		}
  			
//   	} else if($this->prospect_type == 'postdrill')
//   	{
//   		$sql2 = "select prospect_id, structure_name from rsc_prospect where structure_name = '" . $this->structure_name . "' and prospect_type = 'postdrill'";
//   		$command2 = Yii::app()->db->createCommand($sql2);
//   		$data2 = $command2->queryRow();
  			
//   		$sql3 = "select wk_id, prospect_id, structure_name from rsc_prospect, rsc_play where structure_name = '" . $this->structure_name . "' and rsc_prospect.play_id = rsc_play.play_id";
//   		$command3 = Yii::app()->db->createCommand($sql3);
//   		$data3 = $command3->queryAll();
  			
//   		$check = 0;
//   		if($this->prospect_id != null)
//   		{
//   			if($data2['prospect_id'] != $this->prospect_id)
//   			{
//   				$check = 1;
//   			}
//   		} else if(is_array($data2))
//   		{
//   			$check = 1;
//   		} else
//   		{
//   			foreach ($data3 as $datas3)
//   			{
//   				if($datas3['wk_id'] != Yii::app()->user->wk_id)
//   				{
//   					$check = 1;
//   					break;
//   				}
//   			}
//   		}
//   	} else if($this->prospect_type == 'discovery')
//   	{
//   		$sql2 = "select prospect_id, structure_name from rsc_prospect where structure_name = '" . $this->structure_name . "' and prospect_type = 'discovery'";
//   		$command2 = Yii::app()->db->createCommand($sql2);
//   		$data2 = $command2->queryRow();
  			
//   		$sql3 = "select wk_id, prospect_id, structure_name from rsc_prospect, rsc_play where structure_name = '" . $this->structure_name . "' and rsc_prospect.play_id = rsc_play.play_id";
//   		$command3 = Yii::app()->db->createCommand($sql3);
//   		$data3 = $command3->queryAll();
  			
//   		$check = 0;
//   		if($this->prospect_id != null)
//   		{
//   			if($data2['prospect_id'] != $this->prospect_id)
//   			{
//   				$check = 1;
//   			}
//   		} else if(is_array($data2))
//   		{
//   			$check = 1;
//   		} else
//   		{
//   			foreach ($data3 as $datas3)
//   			{
//   				if($datas3['wk_id'] != Yii::app()->user->wk_id)
//   				{
//   					$check = 1;
//   					break;
//   				}
//   			}
//   		}
//   	}
  
//   	if(preg_match($regex, $this->structure_name) || $check == 1 || $check2 == 1)
//   	{
//   		$this->addError('structure_name', $this->getAttributeLabel('structure_name') . ' not allowed');
//   	}
//   }
}
