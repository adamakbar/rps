<?php

/**
 * This is the model class for table "rsc_well_postdrill".
 *
 * The followings are the available columns in table 'rsc_well_postdrill':
 * @property integer $wpd_id
 * @property integer $pd_id
 * @property string $wpd_name
 * @property string $wpd_latitude
 * @property string $wpd_longitude
 * @property string $wpd_category
 * @property string $wpd_type
 * @property string $wpd_shore
 * @property string $wpd_terrain
 * @property string $wpd_status
 * @property string $wpd_formation
 * @property string $wpd_date_complete
 * @property string $wpd_target_depth_tvd
 * @property string $wpd_target_depth_md
 * @property string $wpd_actual_depth
 * @property string $wpd_target_play
 * @property string $wpd_actual_play
 * @property string $wpd_number_mdt
 * @property string $wpd_number_rft
 * @property string $wpd_res_pressure
 * @property string $wpd_last_pressure
 * @property string $wpd_pressure_gradient
 * @property string $wpd_last_temp
 * @property string $wpd_integrity
 * @property string $wpd_electro_avail
 * @property string $wpd_electro_list
 * @property string $wpd_hc_oil_show
 * @property string $wpd_hc_oil_show_tools
 * @property string $wpd_hc_gas_show
 * @property string $wpd_hc_gas_show_tools
 * @property string $wpd_hc_water_cut
 * @property string $wpd_hc_water_gwc
 * @property string $wpd_hc_water_gwd_tools
 * @property string $wpd_hc_water_owc
 * @property string $wpd_hc_water_owc_tools
 * @property string $wpd_rock_method
 * @property string $wpd_rock_petro
 * @property string $wpd_rock_petro_sample
 * @property string $wpd_rock_top_depth
 * @property string $wpd_rock_bot_depth
 * @property string $wpd_rock_barrel
 * @property string $wpd_rock_barrel_equal
 * @property string $wpd_rock_total_core
 * @property string $wpd_rock_preservative
 * @property string $wpd_rock_routine
 * @property string $wpd_rock_routine_sample
 * @property string $wpd_rock_scal
 * @property string $wpd_rock_scal_sample
 * @property double $wpd_clastic_p10_thickness
 * @property double $wpd_clastic_p50_thickness
 * @property double $wpd_clastic_p90_thickness
 * @property double $wpd_clastic_p10_gr
 * @property double $wpd_clastic_p50_gr
 * @property double $wpd_clastic_p90_gr
 * @property double $wpd_clastic_p10_sp
 * @property double $wpd_clastic_p50_sp
 * @property double $wpd_clastic_p90_sp
 * @property double $wpd_clastic_p10_net
 * @property double $wpd_clastic_p50_net
 * @property double $wpd_clastic_p90_net
 * @property double $wpd_clastic_p10_por
 * @property double $wpd_clastic_p50_por
 * @property double $wpd_clastic_p90_por
 * @property double $wpd_clastic_p10_satur
 * @property double $wpd_clastic_p50_satur
 * @property double $wpd_clastic_p90_satur
 * @property double $wpd_carbo_p10_thickness
 * @property double $wpd_carbo_p50_thickness
 * @property double $wpd_carbo_p90_thickness
 * @property double $wpd_carbo_p10_dtc
 * @property double $wpd_carbo_p50_dtc
 * @property double $wpd_carbo_p90_dtc
 * @property double $wpd_carbo_p10_total
 * @property double $wpd_carbo_p50_total
 * @property double $wpd_carbo_p90_total
 * @property double $wpd_carbo_p10_net
 * @property double $wpd_carbo_p50_net
 * @property double $wpd_carbo_p90_net
 * @property double $wpd_carbo_p10_por
 * @property double $wpd_carbo_p50_por
 * @property double $wpd_carbo_p90_por
 * @property double $wpd_carbo_p10_satur
 * @property double $wpd_carbo_p50_satur
 * @property double $wpd_carbo_p90_satur
 * @property string $wpd_fluid_date
 * @property string $wpd_fluid_sample
 * @property string $wpd_fluid_ratio
 * @property string $wpd_fluid_pressure
 * @property string $wpd_fluid_temp
 * @property string $wpd_fluid_tubing
 * @property string $wpd_fluid_casing
 * @property string $wpd_fluid_by
 * @property string $wpd_fluid_report
 * @property string $wpd_fluid_finger
 * @property string $wpd_fluid_grv_oil
 * @property string $wpd_fluid_grv_gas
 * @property string $wpd_fluid_grv_conden
 * @property string $wpd_fluid_pvt
 * @property string $wpd_fluid_quantity
 * @property string $wpd_fluid_z
 * @property double $wpd_fvf_oil_p10
 * @property double $wpd_fvf_oil_p50
 * @property double $wpd_fvf_oil_p90
 * @property double $wpd_fvf_gas_p10
 * @property double $wpd_fvf_gas_p50
 * @property double $wpd_fvf_gas_p90
 * @property string $wpd_viscocity
 *
 * The followings are the available model relations:
 * @property RscPostdrill $pd
 */
class WellPostdrill extends CActiveRecord
{
	public $lat_degree;
	public $lat_minute;
	public $lat_second;
	public $lat_direction;
	public $long_degree;
	public $long_minute;
	public $long_second;
	public $long_direction;
	
	public $wpd_electro_list1;
	public $wpd_electro_list2;
	public $wpd_electro_list3;
	public $wpd_electro_list4;
	public $wpd_electro_list5;
	public $wpd_electro_list6;
	public $wpd_electro_list7;
	public $wpd_electro_list8;
	public $wpd_electro_list9;
	public $wpd_electro_list10;
	public $wpd_electro_list11;
	public $wpd_electro_list12;
	public $wpd_electro_list13;
	public $wpd_electro_list14;
	public $wpd_electro_list15;
	public $wpd_electro_list16;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_well_postdrill';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wpd_name, lat_degree, lat_minute, lat_second, lat_direction, long_degree, long_minute, long_second, long_direction, wpd_category, wpd_type, wpd_shore, wpd_terrain, wpd_formation, wpd_hc_oil_show, wpd_hc_gas_show', 'required'),
			array(
				'lat_degree, lat_minute, lat_second, long_degree, long_minute, long_second,
				wpd_fluid_temp',
				'numerical',
				'numberPattern'=>'/^\s*[-+]?\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'wpd_target_depth_tvd, wpd_target_depth_md, wpd_actual_depth, wpd_target_play, wpd_actual_play, wpd_res_pressure,
				wpd_last_pressure, wpd_pressure_gradient, wpd_last_temp, wpd_hc_water_gwc, wpd_hc_water_owc, 
				wpd_rock_top_depth, wpd_rock_bot_depth, wpd_rock_barrel, wpd_rock_barrel_equal, wpd_rock_total_core, wpd_rock_preservative,
				wpd_clastic_p10_thickness, wpd_clastic_p50_thickness, wpd_clastic_p90_thickness,
				wpd_clastic_p10_gr, wpd_clastic_p50_gr, wpd_clastic_p90_gr,
				wpd_clastic_p10_sp, wpd_clastic_p50_sp, wpd_clastic_p90_sp,
				wpd_carbo_p10_thickness, wpd_carbo_p50_thickness, wpd_carbo_p90_thickness,
				wpd_carbo_p10_dtc, wpd_carbo_p50_dtc, wpd_carbo_p90_dtc,
				wpd_carbo_p10_total, wpd_carbo_p50_total, wpd_carbo_p90_total,
				wpd_fluid_sample, wpd_fluid_ratio, wpd_fluid_pressure, wpd_fluid_tubing, wpd_fluid_casing, wpd_fluid_grv_oil,
				wpd_fluid_grv_gas, wpd_fluid_grv_conden,
				wpd_fvf_oil_p10, wpd_fvf_oil_p50, wpd_fvf_oil_p90,
				wpd_fvf_gas_p10, wpd_fvf_gas_p50, wpd_fvf_gas_p90,
				wpd_viscocity',
				'numerical',
				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('pd_id', 'numerical', 'integerOnly'=>true),
			array('wpd_hc_water_cut, wpd_clastic_p90_net, wpd_clastic_p50_net, wpd_clastic_p10_net, wpd_clastic_p90_por, wpd_clastic_p50_por, wpd_clastic_p10_por, wpd_clastic_p90_satur, wpd_clastic_p50_satur, wpd_clastic_p10_satur, wpd_carbo_p90_net, wpd_carbo_p50_net, wpd_carbo_p10_net, wpd_carbo_p90_por, wpd_carbo_p50_por, wpd_carbo_p10_por, wpd_carbo_p90_satur, wpd_carbo_p50_satur, wpd_carbo_p10_satur', 'numerical', 'min'=>0, 'max'=>100),
			array(
				'lat_direction, long_direction',
				'length',
				'max'=>1
			),
			array(
				'lat_degree, lat_minute, lat_second, long_degree, long_minute, long_second',
				'length',
				'max'=>8
			),
			array('wpd_name, wpd_latitude, wpd_longitude, wpd_category, wpd_type, wpd_shore, wpd_terrain, wpd_status, wpd_formation, wpd_date_complete, wpd_target_depth_tvd, wpd_target_depth_md, wpd_actual_depth, wpd_target_play, wpd_actual_play, wpd_number_mdt, wpd_number_rft, wpd_res_pressure, wpd_last_pressure, wpd_pressure_gradient, wpd_last_temp, wpd_integrity, wpd_electro_avail, wpd_electro_list, wpd_hc_oil_show, wpd_hc_oil_show_tools, wpd_hc_gas_show, wpd_hc_gas_show_tools, wpd_hc_water_cut, wpd_hc_water_gwc, wpd_hc_water_gwd_tools, wpd_hc_water_owc, wpd_hc_water_owc_tools, wpd_rock_method, wpd_rock_petro, wpd_rock_petro_sample, wpd_rock_top_depth, wpd_rock_bot_depth, wpd_rock_barrel, wpd_rock_barrel_equal, wpd_rock_total_core, wpd_rock_preservative, wpd_rock_routine, wpd_rock_routine_sample, wpd_rock_scal, wpd_rock_scal_sample, wpd_fluid_sample, wpd_fluid_ratio, wpd_fluid_pressure, wpd_fluid_temp, wpd_fluid_tubing, wpd_fluid_casing, wpd_fluid_by, wpd_fluid_report, wpd_fluid_finger, wpd_fluid_grv_oil, wpd_fluid_grv_gas, wpd_fluid_grv_conden, wpd_fluid_pvt, wpd_fluid_quantity, wpd_fluid_z, wpd_viscocity', 'length', 'max'=>50),
			array('wpd_fluid_date', 'safe'),
			array(
				'wpd_clastic_p90_thickness, wpd_clastic_p50_thickness, wpd_clastic_p10_thickness, wpd_clastic_p90_gr, wpd_clastic_p50_gr, wpd_clastic_p10_gr,
				wpd_clastic_p90_sp, wpd_clastic_p50_sp, wpd_clastic_p10_sp, wpd_clastic_p90_net, wpd_clastic_p50_net, wpd_clastic_p10_net,
				wpd_clastic_p90_por, wpd_clastic_p50_por, wpd_clastic_p10_por, wpd_clastic_p90_satur, wpd_clastic_p50_satur, wpd_clastic_p10_satur,
				wpd_carbo_p90_thickness, wpd_carbo_p50_thickness, wpd_carbo_p10_thickness, wpd_carbo_p90_dtc, wpd_carbo_p50_dtc, wpd_carbo_p10_dtc,
				wpd_carbo_p90_total, wpd_carbo_p50_total, wpd_carbo_p10_total, wpd_carbo_p90_net, wpd_carbo_p50_net, wpd_carbo_p10_net,
				wpd_carbo_p90_por, wpd_carbo_p50_por, wpd_carbo_p10_por, wpd_carbo_p90_satur, wpd_carbo_p50_satur, wpd_carbo_p10_satur,
				wpd_fvf_oil_p90, wpd_fvf_oil_p50, wpd_fvf_oil_p10, wpd_fvf_gas_p90, wpd_fvf_gas_p50, wpd_fvf_gas_p10',
				'chkDependentWellPostdrill'
			),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wpd_id, pd_id, wpd_name, wpd_latitude, wpd_longitude, wpd_category, wpd_type, wpd_shore, wpd_terrain, wpd_status, wpd_formation, wpd_date_complete, wpd_target_depth_tvd, wpd_target_depth_md, wpd_actual_depth, wpd_target_play, wpd_actual_play, wpd_number_mdt, wpd_number_rft, wpd_res_pressure, wpd_last_pressure, wpd_pressure_gradient, wpd_last_temp, wpd_integrity, wpd_electro_avail, wpd_electro_list, wpd_hc_oil_show, wpd_hc_oil_show_tools, wpd_hc_gas_show, wpd_hc_gas_show_tools, wpd_hc_water_cut, wpd_hc_water_gwc, wpd_hc_water_gwd_tools, wpd_hc_water_owc, wpd_hc_water_owc_tools, wpd_rock_method, wpd_rock_petro, wpd_rock_petro_sample, wpd_rock_top_depth, wpd_rock_bot_depth, wpd_rock_barrel, wpd_rock_barrel_equal, wpd_rock_total_core, wpd_rock_preservative, wpd_rock_routine, wpd_rock_routine_sample, wpd_rock_scal, wpd_rock_scal_sample, wpd_clastic_p10_thickness, wpd_clastic_p50_thickness, wpd_clastic_p90_thickness, wpd_clastic_p10_gr, wpd_clastic_p50_gr, wpd_clastic_p90_gr, wpd_clastic_p10_sp, wpd_clastic_p50_sp, wpd_clastic_p90_sp, wpd_clastic_p10_net, wpd_clastic_p50_net, wpd_clastic_p90_net, wpd_clastic_p10_por, wpd_clastic_p50_por, wpd_clastic_p90_por, wpd_clastic_p10_satur, wpd_clastic_p50_satur, wpd_clastic_p90_satur, wpd_carbo_p10_thickness, wpd_carbo_p50_thickness, wpd_carbo_p90_thickness, wpd_carbo_p10_dtc, wpd_carbo_p50_dtc, wpd_carbo_p90_dtc, wpd_carbo_p10_total, wpd_carbo_p50_total, wpd_carbo_p90_total, wpd_carbo_p10_net, wpd_carbo_p50_net, wpd_carbo_p90_net, wpd_carbo_p10_por, wpd_carbo_p50_por, wpd_carbo_p90_por, wpd_carbo_p10_satur, wpd_carbo_p50_satur, wpd_carbo_p90_satur, wpd_fluid_date, wpd_fluid_sample, wpd_fluid_ratio, wpd_fluid_pressure, wpd_fluid_temp, wpd_fluid_tubing, wpd_fluid_casing, wpd_fluid_by, wpd_fluid_report, wpd_fluid_finger, wpd_fluid_grv_oil, wpd_fluid_grv_gas, wpd_fluid_grv_conden, wpd_fluid_pvt, wpd_fluid_quantity, wpd_fluid_z, wpd_fvf_oil_p10, wpd_fvf_oil_p50, wpd_fvf_oil_p90, wpd_fvf_gas_p10, wpd_fvf_gas_p50, wpd_fvf_gas_p90, wpd_viscocity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pd' => array(self::BELONGS_TO, 'RscPostdrill', 'pd_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wpd_id' => 'Wpd',
			'pd_id' => 'Pd',
			'wpd_name' => 'Well Name',
			'lat_degree' => 'Latitude Degree',
			'lat_minute' => 'Latitude Minute',
			'lat_second' => 'Latitude Second',
			'lat_direction' => 'Latitude Direction',
			'long_degree' => 'Longitude Degree',
			'long_minute' => 'Longitude Minute',
			'long_second' => 'Longitude Second',
			'long_direction' => 'Longitude Direction',
			'wpd_latitude' => 'Wpd Latitude',
			'wpd_longitude' => 'Wpd Longitude',
			'wpd_category' => 'Well Category',
			'wpd_type' => 'Well Type',
			'wpd_shore' => 'Well Shore',
			'wpd_terrain' => 'Well Terrain',
			'wpd_status' => 'Wpd Status',
			'wpd_formation' => 'Well Formation',
			'wpd_date_complete' => 'Wpd Date Complete',
			'wpd_target_depth_tvd' => 'Wpd Target Depth Tvd',
			'wpd_target_depth_md' => 'Wpd Target Depth Md',
			'wpd_actual_depth' => 'Wpd Actual Depth',
			'wpd_target_play' => 'Wpd Target Play',
			'wpd_actual_play' => 'Wpd Actual Play',
			'wpd_number_mdt' => 'Wpd Number Mdt',
			'wpd_number_rft' => 'Wpd Number Rft',
			'wpd_res_pressure' => 'Wpd Res Pressure',
			'wpd_last_pressure' => 'Wpd Last Pressure',
			'wpd_pressure_gradient' => 'Wpd Pressure Gradient',
			'wpd_last_temp' => 'Wpd Last Temp',
			'wpd_integrity' => 'Wpd Integrity',
			'wpd_electro_avail' => 'Wpd Electro Avail',
			'wpd_electro_list' => 'Wpd Electro List',
			'wpd_hc_oil_show' => 'Well Oil Show or Reading',
			'wpd_hc_oil_show_tools' => 'Wpd Hc Oil Show Tools',
			'wpd_hc_gas_show' => 'Well Gas Show or Reading',
			'wpd_hc_gas_show_tools' => 'Wpd Hc Gas Show Tools',
			'wpd_hc_water_cut' => 'Wpd Hc Water Cut',
			'wpd_hc_water_gwc' => 'Wpd Hc Water Gwc',
			'wpd_hc_water_gwd_tools' => 'Wpd Hc Water Gwd Tools',
			'wpd_hc_water_owc' => 'Wpd Hc Water Owc',
			'wpd_hc_water_owc_tools' => 'Wpd Hc Water Owc Tools',
			'wpd_rock_method' => 'Wpd Rock Method',
			'wpd_rock_petro' => 'Wpd Rock Petro',
			'wpd_rock_petro_sample' => 'Wpd Rock Petro Sample',
			'wpd_rock_top_depth' => 'Wpd Rock Top Depth',
			'wpd_rock_bot_depth' => 'Wpd Rock Bot Depth',
			'wpd_rock_barrel' => 'Wpd Rock Barrel',
			'wpd_rock_barrel_equal' => 'Wpd Rock Barrel Equal',
			'wpd_rock_total_core' => 'Wpd Rock Total Core',
			'wpd_rock_preservative' => 'Wpd Rock Preservative',
			'wpd_rock_routine' => 'Wpd Rock Routine',
			'wpd_rock_routine_sample' => 'Wpd Rock Routine Sample',
			'wpd_rock_scal' => 'Wpd Rock Scal',
			'wpd_rock_scal_sample' => 'Wpd Rock Scal Sample',
			'wpd_clastic_p10_thickness' => 'Wpd Clastic P10 Thickness',
			'wpd_clastic_p50_thickness' => 'Wpd Clastic P50 Thickness',
			'wpd_clastic_p90_thickness' => 'Wpd Clastic P90 Thickness',
			'wpd_clastic_p10_gr' => 'Wpd Clastic P10 Gr',
			'wpd_clastic_p50_gr' => 'Wpd Clastic P50 Gr',
			'wpd_clastic_p90_gr' => 'Wpd Clastic P90 Gr',
			'wpd_clastic_p10_sp' => 'Wpd Clastic P10 Sp',
			'wpd_clastic_p50_sp' => 'Wpd Clastic P50 Sp',
			'wpd_clastic_p90_sp' => 'Wpd Clastic P90 Sp',
			'wpd_clastic_p10_net' => 'Wpd Clastic P10 Net',
			'wpd_clastic_p50_net' => 'Wpd Clastic P50 Net',
			'wpd_clastic_p90_net' => 'Wpd Clastic P90 Net',
			'wpd_clastic_p10_por' => 'Wpd Clastic P10 Por',
			'wpd_clastic_p50_por' => 'Wpd Clastic P50 Por',
			'wpd_clastic_p90_por' => 'Wpd Clastic P90 Por',
			'wpd_clastic_p10_satur' => 'Wpd Clastic P10 Satur',
			'wpd_clastic_p50_satur' => 'Wpd Clastic P50 Satur',
			'wpd_clastic_p90_satur' => 'Wpd Clastic P90 Satur',
			'wpd_carbo_p10_thickness' => 'Wpd Carbo P10 Thickness',
			'wpd_carbo_p50_thickness' => 'Wpd Carbo P50 Thickness',
			'wpd_carbo_p90_thickness' => 'Wpd Carbo P90 Thickness',
			'wpd_carbo_p10_dtc' => 'Wpd Carbo P10 Dtc',
			'wpd_carbo_p50_dtc' => 'Wpd Carbo P50 Dtc',
			'wpd_carbo_p90_dtc' => 'Wpd Carbo P90 Dtc',
			'wpd_carbo_p10_total' => 'Wpd Carbo P10 Total',
			'wpd_carbo_p50_total' => 'Wpd Carbo P50 Total',
			'wpd_carbo_p90_total' => 'Wpd Carbo P90 Total',
			'wpd_carbo_p10_net' => 'Wpd Carbo P10 Net',
			'wpd_carbo_p50_net' => 'Wpd Carbo P50 Net',
			'wpd_carbo_p90_net' => 'Wpd Carbo P90 Net',
			'wpd_carbo_p10_por' => 'Wpd Carbo P10 Por',
			'wpd_carbo_p50_por' => 'Wpd Carbo P50 Por',
			'wpd_carbo_p90_por' => 'Wpd Carbo P90 Por',
			'wpd_carbo_p10_satur' => 'Wpd Carbo P10 Satur',
			'wpd_carbo_p50_satur' => 'Wpd Carbo P50 Satur',
			'wpd_carbo_p90_satur' => 'Wpd Carbo P90 Satur',
			'wpd_fluid_date' => 'Wpd Fluid Date',
			'wpd_fluid_sample' => 'Wpd Fluid Sample',
			'wpd_fluid_ratio' => 'Wpd Fluid Ratio',
			'wpd_fluid_pressure' => 'Wpd Fluid Pressure',
			'wpd_fluid_temp' => 'Wpd Fluid Temp',
			'wpd_fluid_tubing' => 'Wpd Fluid Tubing',
			'wpd_fluid_casing' => 'Wpd Fluid Casing',
			'wpd_fluid_by' => 'Wpd Fluid By',
			'wpd_fluid_report' => 'Wpd Fluid Report',
			'wpd_fluid_finger' => 'Wpd Fluid Finger',
			'wpd_fluid_grv_oil' => 'Wpd Fluid Grv Oil',
			'wpd_fluid_grv_gas' => 'Wpd Fluid Grv Gas',
			'wpd_fluid_grv_conden' => 'Wpd Fluid Grv Conden',
			'wpd_fluid_pvt' => 'Wpd Fluid Pvt',
			'wpd_fluid_quantity' => 'Wpd Fluid Quantity',
			'wpd_fluid_z' => 'Wpd Fluid Z',
			'wpd_fvf_oil_p10' => 'Wpd Fvf Oil P10',
			'wpd_fvf_oil_p50' => 'Wpd Fvf Oil P50',
			'wpd_fvf_oil_p90' => 'Wpd Fvf Oil P90',
			'wpd_fvf_gas_p10' => 'Wpd Fvf Gas P10',
			'wpd_fvf_gas_p50' => 'Wpd Fvf Gas P50',
			'wpd_fvf_gas_p90' => 'Wpd Fvf Gas P90',
			'wpd_viscocity' => 'Wpd Viscocity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wpd_id',$this->wpd_id);
		$criteria->compare('pd_id',$this->pd_id);
		$criteria->compare('wpd_name',$this->wpd_name,true);
		$criteria->compare('wpd_latitude',$this->wpd_latitude,true);
		$criteria->compare('wpd_longitude',$this->wpd_longitude,true);
		$criteria->compare('wpd_category',$this->wpd_category,true);
		$criteria->compare('wpd_type',$this->wpd_type,true);
		$criteria->compare('wpd_shore',$this->wpd_shore,true);
		$criteria->compare('wpd_terrain',$this->wpd_terrain,true);
		$criteria->compare('wpd_status',$this->wpd_status,true);
		$criteria->compare('wpd_formation',$this->wpd_formation,true);
		$criteria->compare('wpd_date_complete',$this->wpd_date_complete,true);
		$criteria->compare('wpd_target_depth_tvd',$this->wpd_target_depth_tvd,true);
		$criteria->compare('wpd_target_depth_md',$this->wpd_target_depth_md,true);
		$criteria->compare('wpd_actual_depth',$this->wpd_actual_depth,true);
		$criteria->compare('wpd_target_play',$this->wpd_target_play,true);
		$criteria->compare('wpd_actual_play',$this->wpd_actual_play,true);
		$criteria->compare('wpd_number_mdt',$this->wpd_number_mdt,true);
		$criteria->compare('wpd_number_rft',$this->wpd_number_rft,true);
		$criteria->compare('wpd_res_pressure',$this->wpd_res_pressure,true);
		$criteria->compare('wpd_last_pressure',$this->wpd_last_pressure,true);
		$criteria->compare('wpd_pressure_gradient',$this->wpd_pressure_gradient,true);
		$criteria->compare('wpd_last_temp',$this->wpd_last_temp,true);
		$criteria->compare('wpd_integrity',$this->wpd_integrity,true);
		$criteria->compare('wpd_electro_avail',$this->wpd_electro_avail,true);
		$criteria->compare('wpd_electro_list',$this->wpd_electro_list,true);
		$criteria->compare('wpd_hc_oil_show',$this->wpd_hc_oil_show,true);
		$criteria->compare('wpd_hc_oil_show_tools',$this->wpd_hc_oil_show_tools,true);
		$criteria->compare('wpd_hc_gas_show',$this->wpd_hc_gas_show,true);
		$criteria->compare('wpd_hc_gas_show_tools',$this->wpd_hc_gas_show_tools,true);
		$criteria->compare('wpd_hc_water_cut',$this->wpd_hc_water_cut,true);
		$criteria->compare('wpd_hc_water_gwc',$this->wpd_hc_water_gwc,true);
		$criteria->compare('wpd_hc_water_gwd_tools',$this->wpd_hc_water_gwd_tools,true);
		$criteria->compare('wpd_hc_water_owc',$this->wpd_hc_water_owc,true);
		$criteria->compare('wpd_hc_water_owc_tools',$this->wpd_hc_water_owc_tools,true);
		$criteria->compare('wpd_rock_method',$this->wpd_rock_method,true);
		$criteria->compare('wpd_rock_petro',$this->wpd_rock_petro,true);
		$criteria->compare('wpd_rock_petro_sample',$this->wpd_rock_petro_sample,true);
		$criteria->compare('wpd_rock_top_depth',$this->wpd_rock_top_depth,true);
		$criteria->compare('wpd_rock_bot_depth',$this->wpd_rock_bot_depth,true);
		$criteria->compare('wpd_rock_barrel',$this->wpd_rock_barrel,true);
		$criteria->compare('wpd_rock_barrel_equal',$this->wpd_rock_barrel_equal,true);
		$criteria->compare('wpd_rock_total_core',$this->wpd_rock_total_core,true);
		$criteria->compare('wpd_rock_preservative',$this->wpd_rock_preservative,true);
		$criteria->compare('wpd_rock_routine',$this->wpd_rock_routine,true);
		$criteria->compare('wpd_rock_routine_sample',$this->wpd_rock_routine_sample,true);
		$criteria->compare('wpd_rock_scal',$this->wpd_rock_scal,true);
		$criteria->compare('wpd_rock_scal_sample',$this->wpd_rock_scal_sample,true);
		$criteria->compare('wpd_clastic_p10_thickness',$this->wpd_clastic_p10_thickness);
		$criteria->compare('wpd_clastic_p50_thickness',$this->wpd_clastic_p50_thickness);
		$criteria->compare('wpd_clastic_p90_thickness',$this->wpd_clastic_p90_thickness);
		$criteria->compare('wpd_clastic_p10_gr',$this->wpd_clastic_p10_gr);
		$criteria->compare('wpd_clastic_p50_gr',$this->wpd_clastic_p50_gr);
		$criteria->compare('wpd_clastic_p90_gr',$this->wpd_clastic_p90_gr);
		$criteria->compare('wpd_clastic_p10_sp',$this->wpd_clastic_p10_sp);
		$criteria->compare('wpd_clastic_p50_sp',$this->wpd_clastic_p50_sp);
		$criteria->compare('wpd_clastic_p90_sp',$this->wpd_clastic_p90_sp);
		$criteria->compare('wpd_clastic_p10_net',$this->wpd_clastic_p10_net);
		$criteria->compare('wpd_clastic_p50_net',$this->wpd_clastic_p50_net);
		$criteria->compare('wpd_clastic_p90_net',$this->wpd_clastic_p90_net);
		$criteria->compare('wpd_clastic_p10_por',$this->wpd_clastic_p10_por);
		$criteria->compare('wpd_clastic_p50_por',$this->wpd_clastic_p50_por);
		$criteria->compare('wpd_clastic_p90_por',$this->wpd_clastic_p90_por);
		$criteria->compare('wpd_clastic_p10_satur',$this->wpd_clastic_p10_satur);
		$criteria->compare('wpd_clastic_p50_satur',$this->wpd_clastic_p50_satur);
		$criteria->compare('wpd_clastic_p90_satur',$this->wpd_clastic_p90_satur);
		$criteria->compare('wpd_carbo_p10_thickness',$this->wpd_carbo_p10_thickness);
		$criteria->compare('wpd_carbo_p50_thickness',$this->wpd_carbo_p50_thickness);
		$criteria->compare('wpd_carbo_p90_thickness',$this->wpd_carbo_p90_thickness);
		$criteria->compare('wpd_carbo_p10_dtc',$this->wpd_carbo_p10_dtc);
		$criteria->compare('wpd_carbo_p50_dtc',$this->wpd_carbo_p50_dtc);
		$criteria->compare('wpd_carbo_p90_dtc',$this->wpd_carbo_p90_dtc);
		$criteria->compare('wpd_carbo_p10_total',$this->wpd_carbo_p10_total);
		$criteria->compare('wpd_carbo_p50_total',$this->wpd_carbo_p50_total);
		$criteria->compare('wpd_carbo_p90_total',$this->wpd_carbo_p90_total);
		$criteria->compare('wpd_carbo_p10_net',$this->wpd_carbo_p10_net);
		$criteria->compare('wpd_carbo_p50_net',$this->wpd_carbo_p50_net);
		$criteria->compare('wpd_carbo_p90_net',$this->wpd_carbo_p90_net);
		$criteria->compare('wpd_carbo_p10_por',$this->wpd_carbo_p10_por);
		$criteria->compare('wpd_carbo_p50_por',$this->wpd_carbo_p50_por);
		$criteria->compare('wpd_carbo_p90_por',$this->wpd_carbo_p90_por);
		$criteria->compare('wpd_carbo_p10_satur',$this->wpd_carbo_p10_satur);
		$criteria->compare('wpd_carbo_p50_satur',$this->wpd_carbo_p50_satur);
		$criteria->compare('wpd_carbo_p90_satur',$this->wpd_carbo_p90_satur);
		$criteria->compare('wpd_fluid_date',$this->wpd_fluid_date,true);
		$criteria->compare('wpd_fluid_sample',$this->wpd_fluid_sample,true);
		$criteria->compare('wpd_fluid_ratio',$this->wpd_fluid_ratio,true);
		$criteria->compare('wpd_fluid_pressure',$this->wpd_fluid_pressure,true);
		$criteria->compare('wpd_fluid_temp',$this->wpd_fluid_temp,true);
		$criteria->compare('wpd_fluid_tubing',$this->wpd_fluid_tubing,true);
		$criteria->compare('wpd_fluid_casing',$this->wpd_fluid_casing,true);
		$criteria->compare('wpd_fluid_by',$this->wpd_fluid_by,true);
		$criteria->compare('wpd_fluid_report',$this->wpd_fluid_report,true);
		$criteria->compare('wpd_fluid_finger',$this->wpd_fluid_finger,true);
		$criteria->compare('wpd_fluid_grv_oil',$this->wpd_fluid_grv_oil,true);
		$criteria->compare('wpd_fluid_grv_gas',$this->wpd_fluid_grv_gas,true);
		$criteria->compare('wpd_fluid_grv_conden',$this->wpd_fluid_grv_conden,true);
		$criteria->compare('wpd_fluid_pvt',$this->wpd_fluid_pvt,true);
		$criteria->compare('wpd_fluid_quantity',$this->wpd_fluid_quantity,true);
		$criteria->compare('wpd_fluid_z',$this->wpd_fluid_z,true);
		$criteria->compare('wpd_fvf_oil_p10',$this->wpd_fvf_oil_p10);
		$criteria->compare('wpd_fvf_oil_p50',$this->wpd_fvf_oil_p50);
		$criteria->compare('wpd_fvf_oil_p90',$this->wpd_fvf_oil_p90);
		$criteria->compare('wpd_fvf_gas_p10',$this->wpd_fvf_gas_p10);
		$criteria->compare('wpd_fvf_gas_p50',$this->wpd_fvf_gas_p50);
		$criteria->compare('wpd_fvf_gas_p90',$this->wpd_fvf_gas_p90);
		$criteria->compare('wpd_viscocity',$this->wpd_viscocity,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WellPostdrill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function chkDependentWellPostdrill($attribute)
	{
		if($attribute == 'wpd_clastic_p10_thickness')
		{
			if($this->wpd_clastic_p10_thickness != '')
			{
				if($this->wpd_clastic_p10_thickness == 0 || $this->wpd_clastic_p10_thickness <= $this->wpd_clastic_p50_thickness || $this->wpd_clastic_p10_thickness <= $this->wpd_clastic_p90_thickness)
					$this->addError('wpd_clastic_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P50 Gross Reservoir Thickness and Well Clastic P90 Gross Reservoir Thickness');
			}
		}
		
		if($attribute == 'wpd_clastic_p50_thickness')
		{
			if($this->wpd_clastic_p50_thickness != '')
			{
				if($this->wpd_clastic_p50_thickness == 0 || $this->wpd_clastic_p50_thickness <= $this->wpd_clastic_p90_thickness)
					$this->addError('wpd_clastic_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P90 Gross Reservoir Thickness');
			}
		}
		
		if($attribute == 'wpd_clastic_p10_gr')
		{
			if($this->wpd_clastic_p10_gr != '')
			{
				if($this->wpd_clastic_p10_gr == 0 || $this->wpd_clastic_p10_gr <= $this->wpd_clastic_p50_gr || $this->wpd_clastic_p10_gr <= $this->wpd_clastic_p90_gr)
					$this->addError('wpd_clastic_p10_gr', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P50 Reservoir Vshale Content (GR Log) and Well Clastic P90 Reservoir Vshale Content (GR Log)');
			}
		}
		
		if($attribute == 'wpd_clastic_p50_gr')
		{
			if($this->wpd_clastic_p50_gr != '')
			{
				if($this->wpd_clastic_p50_gr == 0 || $this->wpd_clastic_p50_gr <= $this->wpd_clastic_p90_gr)
					$this->addError('wpd_clastic_p50_gr', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P90 Reservoir Vshale Content (GR Log)');
			}
		}
		
		if($attribute == 'wpd_clastic_p10_sp')
		{
			if($this->wpd_clastic_p10_sp != '') {
				if($this->wpd_clastic_p10_sp == 0 || $this->wpd_clastic_p10_sp <= $this->wpd_clastic_p50_sp || $this->wpd_clastic_p10_sp <= $this->wpd_clastic_p90_sp)
					$this->addError('wpd_clastic_p10_sp', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P50 Reservoir Vshale Content (SP Log) and Well Clastic P90 Reservoir Vshale Content (SP Log)');
			}
		}
			
		if($attribute == 'wpd_clastic_p50_sp')
		{
			if($this->wpd_clastic_p50_sp != '') {
				if($this->wpd_clastic_p50_sp == 0 || $this->wpd_clastic_p50_sp <= $this->wpd_clastic_p90_sp)
					$this->addError('wpd_clastic_p50_sp', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P90 Reservoir Vshale Content (SP Log)');
			}
		}
		
		if($attribute == 'wpd_clastic_p10_net')
		{
			if($this->wpd_clastic_p10_net != '') {
				if($this->wpd_clastic_p10_net == 0 || $this->wpd_clastic_p10_net <= $this->wpd_clastic_p50_net || $this->wpd_clastic_p10_net <= $this->wpd_clastic_p90_net)
					$this->addError('wpd_clastic_p10_net', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P50 Net to Gross and Well Clastic P90 Net to Gross');
			}
		}
		
		if($attribute == 'wpd_clastic_p50_net')
		{
			if($this->wpd_clastic_p50_net != '') {
				if($this->wpd_clastic_p50_net == 0 || $this->wpd_clastic_p50_net <= $this->wpd_clastic_p90_net)
					$this->addError('wpd_clastic_p50_net', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P90 Net to Gross');
			}
		}
		
		if($attribute == 'wpd_clastic_p10_por')
		{
			if($this->wpd_clastic_p10_por != '') {
				if($this->wpd_clastic_p10_por == 0 || $this->wpd_clastic_p10_por <= $this->wpd_clastic_p50_por || $this->wpd_clastic_p10_por <= $this->wpd_clastic_p90_por)
					$this->addError('wpd_clastic_p10_por', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P50 Reservoir Porosity and Well Clastic P90 Reservoir Porosity');
			}
		}
		
		if($attribute == 'wpd_clastic_p50_por')
		{
			if($this->wpd_clastic_p50_por != '') {
				if($this->wpd_clastic_p50_por == 0 || $this->wpd_clastic_p50_por <= $this->wpd_clastic_p90_por)
					$this->addError('wpd_clastic_p50_por', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P90 Reservoir Porosity');
			}
		}
		
		if($attribute == 'wpd_clastic_p10_satur')
		{
			if($this->wpd_clastic_p10_satur != '') {
				if($this->wpd_clastic_p10_satur == 0 || $this->wpd_clastic_p10_satur <= $this->wpd_clastic_p50_satur || $this->wpd_clastic_p10_satur <= $this->wpd_clastic_p90_satur)
					$this->addError('wpd_clastic_p10_satur', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P50 HC Saturation (1-Sw) and Well Clastic P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'wpd_clastic_p50_satur')
		{
			if($this->wpd_clastic_p50_satur != '') {
				if($this->wpd_clastic_p50_satur == 0 || $this->wpd_clastic_p50_satur <= $this->wpd_clastic_p90_satur)
					$this->addError('wpd_clastic_p50_satur', $this->getAttributeLabel($attribute) . ' must greater than Well Clastic P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'wpd_carbo_p10_thickness')
		{
			if($this->wpd_carbo_p10_thickness != '') {
				if($this->wpd_carbo_p10_thickness == 0 || $this->wpd_carbo_p10_thickness <= $this->wpd_carbo_p50_thickness || $this->wpd_carbo_p10_thickness <= $this->wpd_carbo_p90_thickness)
					$this->addError('wpd_carbo_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P50 Gross Reservoir Thickness and Well Carbonate P90 Gross Reservoir Thickness');
			}
		}
			
		if($attribute == 'wpd_carbo_p50_thickness')
		{
			if($this->wpd_carbo_p50_thickness != '') {
				if($this->wpd_carbo_p50_thickness == 0 || $this->wpd_carbo_p50_thickness <= $this->wpd_carbo_p90_thickness)
					$this->addError('wpd_carbo_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P90 Gross Reservoir Thickness');
			}
		}
		
		if($attribute == 'wpd_carbo_p10_dtc')
		{
			if($this->wpd_carbo_p10_dtc != '') {
				if($this->wpd_carbo_p10_dtc == 0 || $this->wpd_carbo_p10_dtc <= $this->wpd_carbo_p50_dtc || $this->wpd_carbo_p10_dtc <= $this->wpd_carbo_p90_dtc)
					$this->addError('wpd_carbo_p10_dtc', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P50 Thickness Reservoir Pore Throat Connectivity (DTC) and Well Carbonate P90 Thickness Reservoir Pore Throat Connectivity (DTC)');
			}
		}
		
		if($attribute == 'wpd_carbo_p50_dtc')
		{
			if($this->wpd_carbo_p50_dtc != '') {
				if($this->wpd_carbo_p50_dtc == 0 || $this->wpd_carbo_p50_dtc <= $this->wpd_carbo_p90_dtc)
					$this->addError('wpd_carbo_p50_dtc', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P90 Thickness Reservoir Pore Throat Connectivity (DTC)');
			}
		}
		
		if($attribute == 'wpd_carbo_p10_total')
		{
			if($this->wpd_carbo_p10_total != '') {
				if($this->wpd_carbo_p10_total == 0 || $this->wpd_carbo_p10_total <= $this->wpd_carbo_p50_total || $this->wpd_carbo_p10_total <= $this->wpd_carbo_p90_total)
					$this->addError('wpd_carbo_p10_total', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P50 Thickness Reservoir Total Pore and Well Carbonate P90 Thickness Reservoir Total Pore');
			}
		}
			
		if($attribute == 'wpd_carbo_p50_total')
		{
			if($this->wpd_carbo_p50_total != '') {
				if($this->wpd_carbo_p50_total == 0 || $this->wpd_carbo_p50_total <= $this->wpd_carbo_p90_total)
					$this->addError('wpd_carbo_p50_total', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P90 Thickness Reservoir Total Pore');
			}
		}
		
		if($attribute == 'wpd_carbo_p10_net')
		{
			if($this->wpd_carbo_p10_net != '') {
				if($this->wpd_carbo_p10_net == 0 || $this->wpd_carbo_p10_net <= $this->wpd_carbo_p50_net || $this->wpd_carbo_p10_net <= $this->wpd_carbo_p90_net)
					$this->addError('wpd_carbo_p10_net', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P50 Net to Gross and Well Carbonate P90 Net to Gross');
			}
		}
		
		if($attribute == 'wpd_carbo_p50_net')
		{
			if($this->wpd_carbo_p50_net != '') {
				if($this->wpd_carbo_p50_net == 0 || $this->wpd_carbo_p50_net <= $this->wpd_carbo_p90_net)
					$this->addError('wpd_carbo_p50_net', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P90 Net to Gross');
			}
		}
		
		if($attribute == 'wpd_carbo_p10_por')
		{
			if($this->wpd_carbo_p10_por != '') {
				if($this->wpd_carbo_p10_por == 0 || $this->wpd_carbo_p10_por <= $this->wpd_carbo_p50_por || $this->wpd_carbo_p10_por <= $this->wpd_carbo_p90_por)
					$this->addError('wpd_carbo_p10_por', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P50 Reservoir Porosity and Well Carbonate P90 Reservoir Porosity');
			}
		}
			
		if($attribute == 'wpd_carbo_p50_por')
		{
			if($this->wpd_carbo_p50_por != '') {
				if($this->wpd_carbo_p50_por == 0 || $this->wpd_carbo_p50_por <= $this->wpd_carbo_p90_por)
					$this->addError('wpd_carbo_p50_por', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P90 Reservoir Porosity');
			}
		}
		
		if($attribute == 'wpd_carbo_p10_satur')
		{
			if($this->wpd_carbo_p10_satur != '') {
				if($this->wpd_carbo_p10_satur == 0 || $this->wpd_carbo_p10_satur <= $this->wpd_carbo_p50_satur || $this->wpd_carbo_p10_satur <= $this->wpd_carbo_p90_satur)
					$this->addError('wpd_carbo_p10_satur', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P50 HC Saturation (1-Sw) and Well Carbonate P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'wpd_carbo_p50_satur')
		{
			if($this->wpd_carbo_p50_satur != '') {
				if($this->wpd_carbo_p50_satur == 0 || $this->wpd_carbo_p50_satur <= $this->wpd_carbo_p90_satur)
					$this->addError('wpd_carbo_p50_satur', $this->getAttributeLabel($attribute) . ' must greater than Well Carbonate P90 HC Saturation (1-Sw)');
			}
		}
		
		if($attribute == 'wpd_fvf_oil_p10')
		{
			if($this->wpd_fvf_oil_p10 != '') {
				if($this->wpd_fvf_oil_p10 == 0 || $this->wpd_fvf_oil_p10 <= $this->wpd_fvf_oil_p50 || $this->wpd_fvf_oil_p10 <= $this->wpd_fvf_oil_p90)
					$this->addError('wpd_fvf_oil_p10', $this->getAttributeLabel($attribute) . ' must greater than Well Initial Formation Volume Factor P50 Oil (Boi) and Well Initial Formation Volume Factor P90 Oil (Boi)');
			}
		}
		
		if($attribute == 'wpd_fvf_oil_p50')
		{
			if($this->wpd_fvf_oil_p50 != '') {
				if($this->wpd_fvf_oil_p50 == 0 || $this->wpd_fvf_oil_p50 <= $this->wpd_fvf_oil_p90)
					$this->addError('wpd_fvf_oil_p50', $this->getAttributeLabel($attribute) . ' must greater than Well Initial Formation Volume Factor P90 Oil (Boi)');
			}
		}
		
		if($attribute == 'wpd_fvf_gas_p10')
		{
			if($this->wpd_fvf_gas_p10 != '') {
				if($this->wpd_fvf_gas_p10 == 0 || $this->wpd_fvf_gas_p10 <= $this->wpd_fvf_gas_p50 || $this->wpd_fvf_gas_p10 <= $this->wpd_fvf_gas_p90)
					$this->addError('wpd_fvf_gas_p10', $this->getAttributeLabel($attribute) . ' must greater than Well Initial Formation Volume Factor P50 Gas (Bgi) and Well Initial Formation Volume Factor P90 Gas (Bgi)');
			}
		}
		
		if($attribute == 'wpd_fvf_gas_p50')
		{
			if($this->wpd_fvf_gas_p50 != '') {
				if($this->wpd_fvf_gas_p50 == 0 || $this->wpd_fvf_gas_p50 <= $this->wpd_fvf_gas_p90)
					$this->addError('wpd_fvf_gas_p50', $this->getAttributeLabel($attribute) . ' must greater than Well Initial Formation Volume Factor P90 Gas (Bgi)');
			}
		}
	}
}
