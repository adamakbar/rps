<?php

/**
 * This is the model class for table "rsc_discovery".
 *
 * The followings are the available columns in table 'rsc_discovery':
 * @property integer $dc_id
 * @property integer $prospect_id
 * @property string $dc_name_revise
 * @property string $dc_name_revise_date
 * @property integer $dc_total_well
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 * @property RscWellDiscovery[] $rscWellDiscoveries
 */
class Discovery extends CActiveRecord
{

	public $basin_name;
	public $discovery_name;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_discovery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
// 			array('prospect_id', 'required'),
			array('prospect_id, dc_total_well', 'numerical', 'integerOnly'=>true),
			array('dc_name_revise', 'length', 'max'=>50),
			array('dc_name_revise_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dc_id, prospect_id, dc_name_revise, dc_name_revise_date, dc_total_well', 'safe', 'on'=>'search'),
			array('dc_id, basin_name, discovery_name, prospect_id, dc_name_revise, dc_name_revise_date, dc_total_well', 'safe', 'on'=>'searchRelatedDiscovery'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospects' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'prospectd' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'rscWellDiscoveries' => array(self::HAS_MANY, 'RscWellDiscovery', 'dc_id'),
			'plays' => array(self::BELONGS_TO, 'Play', array('play_id'=>'play_id'), 'through'=>'prospects'),
			'gcf' => array(self::BELONGS_TO, 'Gcf', array('gcf_id'=>'gcf_id'), 'through'=>'prospectd'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dc_id' => 'Dc',
			'prospect_id' => 'Prospect',
			'dc_name_revise' => 'Revised Prospect Name',
			'dc_name_revise_date' => 'Prospect Name Revised Date',
			'dc_total_well' => 'Number of Well',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dc_id',$this->dc_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('dc_name_revise',$this->dc_name_revise,true);
		$criteria->compare('dc_name_revise_date',$this->dc_name_revise_date,true);
		$criteria->compare('dc_total_well',$this->dc_total_well);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchRelatedDiscovery() {

	    $discoveryCriteria = new CDbCriteria;
	    $discoveryCriteria->with = array('plays', 'prospects', 'plays.basin', 'gcf');
	    $discoveryCriteria->condition = 'plays.wk_id ="' . Yii::app()->user->wk_id . '"';
    	$discoveryCriteria->addCondition('prospects.prospect_is_deleted = 0', 'AND');
    	$discoveryCriteria->addCondition('basin.basin_name LIKE "%' . $this->basin_name . '%"', 'AND');
    	$discoveryCriteria->addCondition('CONCAT(
	        prospects.structure_name, " @ ", gcf.gcf_res_lithology, " - ", gcf.gcf_res_formation, " - ", 
	        gcf.gcf_res_age_serie, " ", gcf.gcf_res_age_system, " - ", gcf.gcf_res_depos_env, " - ", gcf.gcf_trap_type
	      ) LIKE "%' . $this->discovery_name . '%"', 'AND');

	    return new CActiveDataProvider(get_class($this), array(
	        'criteria'=>$discoveryCriteria,
	        'sort'=>array(
	            'defaultOrder'=>'basin_urut'
	          )
	      ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Discovery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
