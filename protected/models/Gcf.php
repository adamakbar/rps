<?php

/**
 * This is the model class for table "rsc_gcf".
 *
 * The followings are the available columns in table 'rsc_gcf':
 * @property integer $gcf_id
 * @property string $gcf_is_sr
 * @property string $gcf_sr_age_system
 * @property string $gcf_sr_age_serie
 * @property string $gcf_sr_formation
 * @property string $gcf_sr_formation_serie
 * @property string $gcf_sr_kerogen
 * @property string $gcf_sr_toc
 * @property string $gcf_sr_hfu
 * @property string $gcf_sr_distribution
 * @property string $gcf_sr_continuity
 * @property string $gcf_sr_maturity
 * @property string $gcf_sr_otr
 * @property string $gcf_sr_remark
 * @property string $gcf_is_res
 * @property string $gcf_res_age_system
 * @property string $gcf_res_age_serie
 * @property string $gcf_res_formation
 * @property string $gcf_res_formation_serie
 * @property string $gcf_res_depos_set
 * @property string $gcf_res_depos_env
 * @property string $gcf_res_distribution
 * @property string $gcf_res_continuity
 * @property string $gcf_res_lithology
 * @property string $gcf_res_por_type
 * @property string $gcf_res_por_primary
 * @property string $gcf_res_por_secondary
 * @property string $gcf_res_remark
 * @property string $gcf_is_trap
 * @property string $gcf_trap_seal_age_system
 * @property string $gcf_trap_seal_age_serie
 * @property string $gcf_trap_seal_formation
 * @property string $gcf_trap_seal_formation_serie
 * @property string $gcf_trap_seal_distribution
 * @property string $gcf_trap_seal_continuity
 * @property string $gcf_trap_seal_type
 * @property string $gcf_trap_age_system
 * @property string $gcf_trap_age_serie
 * @property string $gcf_trap_geometry
 * @property string $gcf_trap_type
 * @property string $gcf_trap_closure
 * @property string $gcf_trap_remark
 * @property string $gcf_is_dyn
 * @property string $gcf_dyn_migration
 * @property string $gcf_dyn_kitchen
 * @property string $gcf_dyn_petroleum
 * @property string $gcf_dyn_early_age_system
 * @property string $gcf_dyn_early_age_serie
 * @property string $gcf_dyn_late_age_system
 * @property string $gcf_dyn_late_age_serie
 * @property string $gcf_dyn_preservation
 * @property string $gcf_dyn_pathways
 * @property string $gcf_dyn_migration_age_system
 * @property string $gcf_dyn_migration_age_serie
 * @property string $gcf_dyn_remark
 * @property integer $gcf_id_reference
 *
 * The followings are the available model relations:
 * @property RscLead[] $rscLeads
 * @property RscPlay[] $rscPlays
 * @property RscProspect[] $rscProspects
 */
class Gcf extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_gcf';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gcf_is_sr, gcf_is_res, gcf_is_trap, gcf_is_dyn', 'required'),
			array(
				'gcf_sr_formation_serie, gcf_trap_seal_formation_serie',
				'check_pre'
			),
			array('gcf_res_age_system, gcf_res_age_serie, gcf_res_formation, gcf_res_depos_env, gcf_res_lithology, gcf_trap_type', 'required', 'on'=>'play'),
			array(
				'gcf_res_por_type', 
				'not_empty'
			),
			array (
				'gcf_id_reference',
				'numerical',
				'integerOnly' => true
			),
			array('gcf_is_sr, gcf_sr_age_system, gcf_sr_age_serie, gcf_sr_formation, gcf_sr_kerogen, gcf_sr_toc, gcf_sr_hfu, gcf_sr_distribution, gcf_sr_continuity, gcf_sr_maturity, gcf_sr_otr, gcf_is_res, gcf_res_age_system, gcf_res_age_serie, gcf_res_formation, gcf_res_formation_serie, gcf_res_depos_set, gcf_res_depos_env, gcf_res_distribution, gcf_res_continuity, gcf_res_lithology, gcf_res_por_type, gcf_res_por_primary, gcf_res_por_secondary, gcf_is_trap, gcf_trap_seal_age_system, gcf_trap_seal_age_serie, gcf_trap_seal_formation, gcf_trap_seal_distribution, gcf_trap_seal_continuity, gcf_trap_seal_type, gcf_trap_age_system, gcf_trap_age_serie, gcf_trap_geometry, gcf_trap_type, gcf_trap_closure, gcf_is_dyn, gcf_dyn_migration, gcf_dyn_kitchen, gcf_dyn_petroleum, gcf_dyn_early_age_system, gcf_dyn_early_age_serie, gcf_dyn_late_age_system, gcf_dyn_late_age_serie, gcf_dyn_preservation, gcf_dyn_pathways, gcf_dyn_migration_age_system, gcf_dyn_migration_age_serie', 'length', 'max'=>50),
			array('gcf_sr_remark, gcf_res_remark, gcf_trap_remark, gcf_dyn_remark', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('gcf_id, gcf_is_sr, gcf_sr_age_system, gcf_sr_age_serie, gcf_sr_formation, gcf_sr_kerogen, gcf_sr_toc, gcf_sr_hfu, gcf_sr_distribution, gcf_sr_continuity, gcf_sr_maturity, gcf_sr_otr, gcf_sr_remark, gcf_is_res, gcf_res_age_system, gcf_res_age_serie, gcf_res_formation, gcf_res_formation_serie, gcf_res_depos_set, gcf_res_depos_env, gcf_res_distribution, gcf_res_continuity, gcf_res_lithology, gcf_res_por_type, gcf_res_por_primary, gcf_res_por_secondary, gcf_res_remark, gcf_is_trap, gcf_trap_seal_age_system, gcf_trap_seal_age_serie, gcf_trap_seal_formation, gcf_trap_seal_distribution, gcf_trap_seal_continuity, gcf_trap_seal_type, gcf_trap_age_system, gcf_trap_age_serie, gcf_trap_geometry, gcf_trap_type, gcf_trap_closure, gcf_trap_remark, gcf_is_dyn, gcf_dyn_migration, gcf_dyn_kitchen, gcf_dyn_petroleum, gcf_dyn_early_age_system, gcf_dyn_early_age_serie, gcf_dyn_late_age_system, gcf_dyn_late_age_serie, gcf_dyn_preservation, gcf_dyn_pathways, gcf_dyn_migration_age_system, gcf_dyn_migration_age_serie, gcf_dyn_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rscLeads' => array(self::HAS_MANY, 'RscLead', 'gcf_id'),
			'plays' => array(self::HAS_MANY, 'Play', 'gcf_id'),
			'rscProspects' => array(self::HAS_MANY, 'RscProspect', 'gcf_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'gcf_id' => 'Gcf',
			'gcf_is_sr' => 'Source Rock',
			'gcf_sr_age_system' => 'Gcf Sr Age System',
			'gcf_sr_age_serie' => 'Gcf Sr Age Serie',
			'gcf_sr_formation' => 'Gcf Sr Formation',
			'gcf_sr_formation_serie' => 'Gcf Sr Formation Serie',
			'gcf_sr_kerogen' => 'Gcf Sr Kerogen',
			'gcf_sr_toc' => 'Gcf Sr Toc',
			'gcf_sr_hfu' => 'Gcf Sr Hfu',
			'gcf_sr_distribution' => 'Gcf Sr Distribution',
			'gcf_sr_continuity' => 'Gcf Sr Continuity',
			'gcf_sr_maturity' => 'Gcf Sr Maturity',
			'gcf_sr_otr' => 'Gcf Sr Otr',
			'gcf_sr_remark' => 'Gcf Sr Remark',
			'gcf_is_res' => 'Reservoir',
			'gcf_res_age_system' => 'Reservoir Age System',
			'gcf_res_age_serie' => 'Reservoir Age Serie',
			'gcf_res_formation' => 'Reservoir Formation Name or Equivalent',
			'gcf_res_formation_serie' => 'Reservoir Formation Name or Equivalent',
			'gcf_res_depos_set' => 'Reservoir Depositional Setting',
			'gcf_res_depos_env' => 'Gcf Res Depos Env',
			'gcf_res_distribution' => 'Gcf Res Distribution',
			'gcf_res_continuity' => 'Gcf Res Continuity',
			'gcf_res_lithology' => 'Reservoir Lithology',
			'gcf_res_por_type' => 'Gcf Res Por Type',
			'gcf_res_por_primary' => 'Gcf Res Por Primary',
			'gcf_res_por_secondary' => 'Gcf Res Por Secondary',
			'gcf_res_remark' => 'Gcf Res Remark',
			'gcf_is_trap' => 'Trap',
			'gcf_trap_seal_age_system' => 'Gcf Trap Seal Age System',
			'gcf_trap_seal_age_serie' => 'Gcf Trap Seal Age Serie',
			'gcf_trap_seal_formation' => 'Gcf Trap Seal Formation',
			'gcf_trap_seal_formation_serie' => 'Gcf Trap Seal Formation',
			'gcf_trap_seal_distribution' => 'Gcf Trap Seal Distribution',
			'gcf_trap_seal_continuity' => 'Gcf Trap Seal Continuity',
			'gcf_trap_seal_type' => 'Gcf Trap Seal Type',
			'gcf_trap_age_system' => 'Gcf Trap Age System',
			'gcf_trap_age_serie' => 'Gcf Trap Age Serie',
			'gcf_trap_geometry' => 'Gcf Trap Geometry',
			'gcf_trap_type' => 'Trap Type',
			'gcf_trap_closure' => 'Gcf Trap Closure',
			'gcf_trap_remark' => 'Gcf Trap Remark',
			'gcf_is_dyn' => 'Dynamic',
			'gcf_dyn_migration' => 'Gcf Dyn Migration',
			'gcf_dyn_kitchen' => 'Gcf Dyn Kitchen',
			'gcf_dyn_petroleum' => 'Gcf Dyn Petroleum',
			'gcf_dyn_early_age_system' => 'Gcf Dyn Early Age System',
			'gcf_dyn_early_age_serie' => 'Gcf Dyn Early Age Serie',
			'gcf_dyn_late_age_system' => 'Gcf Dyn Late Age System',
			'gcf_dyn_late_age_serie' => 'Gcf Dyn Late Age Serie',
			'gcf_dyn_preservation' => 'Gcf Dyn Preservation',
			'gcf_dyn_pathways' => 'Gcf Dyn Pathways',
			'gcf_dyn_migration_age_system' => 'Gcf Dyn Migration Age System',
			'gcf_dyn_migration_age_serie' => 'Gcf Dyn Migration Age Serie',
			'gcf_dyn_remark' => 'Gcf Dyn Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('gcf_id',$this->gcf_id);
		$criteria->compare('gcf_is_sr',$this->gcf_is_sr,true);
		$criteria->compare('gcf_sr_age_system',$this->gcf_sr_age_system,true);
		$criteria->compare('gcf_sr_age_serie',$this->gcf_sr_age_serie,true);
		$criteria->compare('gcf_sr_formation',$this->gcf_sr_formation,true);
		$criteria->compare('gcf_sr_formation_serie',$this->gcf_sr_formation_serie,true);
		$criteria->compare('gcf_sr_kerogen',$this->gcf_sr_kerogen,true);
		$criteria->compare('gcf_sr_toc',$this->gcf_sr_toc,true);
		$criteria->compare('gcf_sr_hfu',$this->gcf_sr_hfu,true);
		$criteria->compare('gcf_sr_distribution',$this->gcf_sr_distribution,true);
		$criteria->compare('gcf_sr_continuity',$this->gcf_sr_continuity,true);
		$criteria->compare('gcf_sr_maturity',$this->gcf_sr_maturity,true);
		$criteria->compare('gcf_sr_otr',$this->gcf_sr_otr,true);
		$criteria->compare('gcf_sr_remark',$this->gcf_sr_remark,true);
		$criteria->compare('gcf_is_res',$this->gcf_is_res,true);
		$criteria->compare('gcf_res_age_system',$this->gcf_res_age_system,true);
		$criteria->compare('gcf_res_age_serie',$this->gcf_res_age_serie,true);
		$criteria->compare('gcf_res_formation',$this->gcf_res_formation,true);
		$criteria->compare('gcf_res_formation_serie',$this->gcf_res_formation_serie,true);
		$criteria->compare('gcf_res_depos_set',$this->gcf_res_depos_set,true);
		$criteria->compare('gcf_res_depos_env',$this->gcf_res_depos_env,true);
		$criteria->compare('gcf_res_distribution',$this->gcf_res_distribution,true);
		$criteria->compare('gcf_res_continuity',$this->gcf_res_continuity,true);
		$criteria->compare('gcf_res_lithology',$this->gcf_res_lithology,true);
		$criteria->compare('gcf_res_por_type',$this->gcf_res_por_type,true);
		$criteria->compare('gcf_res_por_primary',$this->gcf_res_por_primary,true);
		$criteria->compare('gcf_res_por_secondary',$this->gcf_res_por_secondary,true);
		$criteria->compare('gcf_res_remark',$this->gcf_res_remark,true);
		$criteria->compare('gcf_is_trap',$this->gcf_is_trap,true);
		$criteria->compare('gcf_trap_seal_age_system',$this->gcf_trap_seal_age_system,true);
		$criteria->compare('gcf_trap_seal_age_serie',$this->gcf_trap_seal_age_serie,true);
		$criteria->compare('gcf_trap_seal_formation',$this->gcf_trap_seal_formation,true);
		$criteria->compare('gcf_trap_seal_formation_serie',$this->gcf_trap_seal_formation_serie,true);
		$criteria->compare('gcf_trap_seal_distribution',$this->gcf_trap_seal_distribution,true);
		$criteria->compare('gcf_trap_seal_continuity',$this->gcf_trap_seal_continuity,true);
		$criteria->compare('gcf_trap_seal_type',$this->gcf_trap_seal_type,true);
		$criteria->compare('gcf_trap_age_system',$this->gcf_trap_age_system,true);
		$criteria->compare('gcf_trap_age_serie',$this->gcf_trap_age_serie,true);
		$criteria->compare('gcf_trap_geometry',$this->gcf_trap_geometry,true);
		$criteria->compare('gcf_trap_type',$this->gcf_trap_type,true);
		$criteria->compare('gcf_trap_closure',$this->gcf_trap_closure,true);
		$criteria->compare('gcf_trap_remark',$this->gcf_trap_remark,true);
		$criteria->compare('gcf_is_dyn',$this->gcf_is_dyn,true);
		$criteria->compare('gcf_dyn_migration',$this->gcf_dyn_migration,true);
		$criteria->compare('gcf_dyn_kitchen',$this->gcf_dyn_kitchen,true);
		$criteria->compare('gcf_dyn_petroleum',$this->gcf_dyn_petroleum,true);
		$criteria->compare('gcf_dyn_early_age_system',$this->gcf_dyn_early_age_system,true);
		$criteria->compare('gcf_dyn_early_age_serie',$this->gcf_dyn_early_age_serie,true);
		$criteria->compare('gcf_dyn_late_age_system',$this->gcf_dyn_late_age_system,true);
		$criteria->compare('gcf_dyn_late_age_serie',$this->gcf_dyn_late_age_serie,true);
		$criteria->compare('gcf_dyn_preservation',$this->gcf_dyn_preservation,true);
		$criteria->compare('gcf_dyn_pathways',$this->gcf_dyn_pathways,true);
		$criteria->compare('gcf_dyn_migration_age_system',$this->gcf_dyn_migration_age_system,true);
		$criteria->compare('gcf_dyn_migration_age_serie',$this->gcf_dyn_migration_age_serie,true);
		$criteria->compare('gcf_dyn_remark',$this->gcf_dyn_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gcf the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getPlayName()
	{
		if(($this->gcf_res_lithology === null || $this->gcf_res_lithology === '') || $this->gcf_res_lithology === 'Unknown')
			$this->gcf_res_lithology = '';
		if(($this->gcf_res_depos_env === null || $this->gcf_res_depos_env === '') || $this->gcf_res_depos_env === 'Others')
			$this->gcf_res_depos_env = '';
		if(($this->gcf_res_formation_serie === null || $this->gcf_res_formation_serie === '') || $this->gcf_res_formation_serie === 'Not Available')
			$this->gcf_res_formation_serie = '';
		if(($this->gcf_res_age_serie === null || $this->gcf_res_age_serie === '') || $this->gcf_res_age_serie === 'Not Available')
			$this->gcf_res_age_serie = '';
		if(($this->gcf_trap_type === null || $this->gcf_trap_type === '') || $this->gcf_trap_type === 'Unknown')
			$this->gcf_trap_type = '';
	
		return $this->gcf_res_lithology . ' - <b>' . $this->gcf_res_formation_serie . ' ' . $this->gcf_res_formation . '</b> - ' . $this->gcf_res_age_serie . ' ' . $this->gcf_res_age_system . ' - ' . $this->gcf_res_depos_env . ' - ' . $this->gcf_trap_type;
	}
	
	public function not_empty()
	{
		if($this->gcf_res_por_type == 'Average Primary Porosity Reservoir' && $this->gcf_res_por_primary == '')
			$this->addError('gcf_res_por_primary', $this->getAttributeLabel('gcf_res_por_primary') . ' Cannot be blank.');
		
		if($this->gcf_res_por_type == 'Secondary Porosity' && $this->gcf_res_por_secondary == '')
			$this->addError('gcf_res_por_secondary', $this->getAttributeLabel('gcf_res_por_secondary') . ' Cannot be blank.');
		
		if($this->gcf_res_por_type == 'Both') {
			if($this->gcf_res_por_primary == '')
			{
				$this->addError('gcf_res_por_primary', $this->getAttributeLabel('gcf_res_por_primary') . ' Cannot be blank.');
			}
			if($this->gcf_res_por_secondary == '')
			{
				$this->addError('gcf_res_por_secondary', $this->getAttributeLabel('gcf_res_por_secondary') . ' Cannot be blank.');
			}
		}
	}
	
	public function check_pre($attribute)
	{
		if($attribute == 'gcf_sr_formation_serie')
		{
			if($this->gcf_sr_formation_serie != '' && $this->gcf_sr_formation == '')
			{
				$this->addError('gcf_sr_formation', $this->getAttributeLabel('gcf_sr_formation') . ' Cannot be blank.');
			}
		}
		
		if($attribute == 'gcf_trap_seal_formation_serie')
		{
			if($this->gcf_trap_seal_formation_serie != '' && $this->gcf_trap_seal_formation == '')
			{
				$this->addError('gcf_trap_seal_formation', $this->getAttributeLabel('gcf_trap_seal_formation') . ' Cannot be blank.');
			}
		}
	}
}
