<?php

/**
 * This is the model class for table "rsc_lead_2d".
 *
 * The followings are the available columns in table 'rsc_lead_2d':
 * @property integer $ls2d_id
 * @property integer $lead_id
 * @property string $ls2d_vintage_number
 * @property string $ls2d_year_survey
 * @property string $ls2d_total_crossline
 * @property string $ls2d_total_coverage
 * @property string $ls2d_average_interval
 * @property string $ls2d_year_late_process
 * @property string $ls2d_late_method
 * @property string $ls2d_img_quality
 * @property double $ls2d_low_estimate
 * @property double $ls2d_best_estimate
 * @property double $ls2d_hight_estimate
 * @property string $ls2d_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class Lead2d extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_2d';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLead2d'),
// 			array('ls2d_low_estimate, ls2d_best_estimate, ls2d_hight_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'ls2d_year_survey, ls2d_year_late_process',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'ls2d_total_crossline, ls2d_total_coverage, ls2d_average_interval',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('ls2d_low_estimate, ls2d_best_estimate, ls2d_hight_estimate', 'numerical'),
			array('ls2d_vintage_number, ls2d_year_survey, ls2d_total_crossline, ls2d_total_coverage, ls2d_average_interval, ls2d_year_late_process, ls2d_late_method, ls2d_img_quality', 'length', 'max'=>50),
			array('ls2d_remark', 'safe'),
// 			array('ls2d_low_estimate, ls2d_best_estimate, ls2d_hight_estimate', 'chkDependentArea2d'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ls2d_id, lead_id, ls2d_vintage_number, ls2d_year_survey, ls2d_total_crossline, ls2d_total_coverage, ls2d_average_interval, ls2d_year_late_process, ls2d_late_method, ls2d_img_quality, ls2d_low_estimate, ls2d_best_estimate, ls2d_hight_estimate, ls2d_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ls2d_id' => 'Ls2d',
			'lead_id' => 'Lead',
			'ls2d_vintage_number' => 'Ls2d Vintage Number',
			'ls2d_year_survey' => '2D Seismic Acquisition Year',
			'ls2d_total_crossline' => '2D Seismic Total Crossline Number',
			'ls2d_total_coverage' => 'Ls2d Total Coverage',
			'ls2d_average_interval' => '2D Seismic Average Spacing Parallel Interval',
			'ls2d_year_late_process' => 'Ls2d Year Late Process',
			'ls2d_late_method' => 'Ls2d Late Method',
			'ls2d_img_quality' => 'Ls2d Img Quality',
			'ls2d_low_estimate' => '2D Seismic Low Estimate',
			'ls2d_best_estimate' => '2D Seismic Best Estimate',
			'ls2d_hight_estimate' => '2D Seismic High Estimate',
			'ls2d_remark' => 'Ls2d Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ls2d_id',$this->ls2d_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('ls2d_vintage_number',$this->ls2d_vintage_number,true);
		$criteria->compare('ls2d_year_survey',$this->ls2d_year_survey,true);
		$criteria->compare('ls2d_total_crossline',$this->ls2d_total_crossline,true);
		$criteria->compare('ls2d_total_coverage',$this->ls2d_total_coverage,true);
		$criteria->compare('ls2d_average_interval',$this->ls2d_average_interval,true);
		$criteria->compare('ls2d_year_late_process',$this->ls2d_year_late_process,true);
		$criteria->compare('ls2d_late_method',$this->ls2d_late_method,true);
		$criteria->compare('ls2d_img_quality',$this->ls2d_img_quality,true);
		$criteria->compare('ls2d_low_estimate',$this->ls2d_low_estimate);
		$criteria->compare('ls2d_best_estimate',$this->ls2d_best_estimate);
		$criteria->compare('ls2d_hight_estimate',$this->ls2d_hight_estimate);
		$criteria->compare('ls2d_remark',$this->ls2d_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lead2d the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentArea2d($attribute)
// 	{
// 		if($attribute == 'ls2d_hight_estimate')
// 		{
// 			if($this->ls2d_hight_estimate == 0 || $this->ls2d_hight_estimate <= $this->ls2d_best_estimate || $this->ls2d_hight_estimate <= $this->ls2d_low_estimate)
// 				$this->addError($this->ls2d_hight_estimate, $this->getAttributeLabel($attribute) . ' must greater than 2D Seismic Best Estimate and 2D Seismic Low Estimate');
// 		}
		
// 		if($attribute == 'ls2d_best_estimate')
// 		{
// 			if($this->ls2d_best_estimate == 0 || $this->ls2d_best_estimate <= $this->ls2d_low_estimate)
// 				$this->addError($this->ls2d_best_estimate, $this->getAttributeLabel($attribute) . ' must greater than 2D Seismic Low Estimate');
// 		}
// 	}
	
	public function chkBoxLead2d()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('ls2d_year_survey', 'ls2d_total_crossline', 'ls2d_average_interval', 'ls2d_low_estimate', 'ls2d_best_estimate', 'ls2d_hight_estimate');
			$required->validate($this);
				
			Yii::import('ext.validasi.DependentArea2d');
			$checking = new DependentArea2d;
			$checking->attributes = array('ls2d_low_estimate', 'ls2d_best_estimate', 'ls2d_hight_estimate');
			$checking->validate($this);
		}
	}
}
