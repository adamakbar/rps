<?php

/**
 * This is the model class for table "rsc_other".
 *
 * The followings are the available columns in table 'rsc_other':
 * @property integer $sor_id
 * @property integer $prospect_id
 * @property string $sor_year_survey
 * @property double $sor_vol_p90_area
 * @property double $sor_vol_p50_area
 * @property double $sor_vol_p10_area
 * @property string $sor_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Other extends CActiveRecord
{
	public $is_checked;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_other';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxOther'),
// 			array('sor_year_survey, sor_vol_p90_area, sor_vol_p50_area, sor_vol_p10_area', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'sor_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'sor_vol_p90_area, sor_vol_p50_area, sor_vol_p10_area',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('sor_year_survey', 'length', 'max'=>50),
			array('sor_remark', 'safe'),
// 			array('sor_vol_p90_area, sor_vol_p50_area, sor_vol_p10_area', 'chkDependentOther'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sor_id, prospect_id, sor_year_survey, sor_vol_p90_area, sor_vol_p50_area, sor_vol_p10_area, sor_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sor_id' => 'Sor',
			'prospect_id' => 'Prospect',
			'sor_year_survey' => 'Other Acquisition Year',
			'sor_vol_p90_area' => 'Other P90 Areal Closure Estimation',
			'sor_vol_p50_area' => 'Other P50 Areal Closure Estimation',
			'sor_vol_p10_area' => 'Other P10 Areal Closure Estimation',
			'sor_remark' => 'Sor Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sor_id',$this->sor_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('sor_year_survey',$this->sor_year_survey,true);
		$criteria->compare('sor_vol_p90_area',$this->sor_vol_p90_area);
		$criteria->compare('sor_vol_p50_area',$this->sor_vol_p50_area);
		$criteria->compare('sor_vol_p10_area',$this->sor_vol_p10_area);
		$criteria->compare('sor_remark',$this->sor_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Other the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentOther($attribute)
// 	{
// 		if($attribute == 'sor_vol_p10_area')
// 		{
// 			if($this->sor_vol_p10_area != '') {
// 				if($this->sor_vol_p10_area == 0 || $this->sor_vol_p10_area <= $this->sor_vol_p50_area || $this->sor_vol_p10_area <= $this->sor_vol_p90_area)
// 					$this->addError('sor_vol_p10_area', $this->getAttributeLabel($attribute) . ' must greater than Other P50 Areal Closure Estimation and Other P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'sor_vol_p50_area')
// 		{
// 			if($this->sor_vol_p50_area != '') {
// 				if($this->sor_vol_p50_area == 0 || $this->sor_vol_p50_area <= $this->sor_vol_p90_area)
// 					$this->addError('sor_vol_p50_area', $this->getAttributeLabel($attribute) . ' must greater than Other P90 Areal Closure Estimation');
// 			}
// 		}
// 	}
	
	public function chkBoxOther()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('sor_year_survey', 'sor_vol_p90_area', 'sor_vol_p50_area', 'sor_vol_p10_area');
			$required->validate($this);
		
			Yii::import('ext.validasi.DependentOther');
			$checking = new DependentOther;
			$checking->attributes = array('sor_vol_p90_area', 'sor_vol_p50_area', 'sor_vol_p10_area');
			$checking->validate($this);
		}
	}
}
