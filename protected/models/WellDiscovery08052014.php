<?php

/**
 * This is the model class for table "rsc_well_discovery".
 *
 * The followings are the available columns in table 'rsc_well_discovery':
 * @property integer $wdc_id
 * @property integer $dc_id
 * @property string $wdc_name
 * @property integer $wdc_total_zone
 * @property string $wdc_latitude
 * @property string $wdc_longitude
 * @property string $wdc_type
 * @property string $wdc_shore
 * @property string $wdc_terrain
 * @property string $wdc_status
 * @property string $wdc_formation
 * @property string $wdc_date_complete
 * @property string $wdc_target_depth_tvd
 * @property string $wdc_target_depth_md
 * @property string $wdc_actual_depth
 * @property string $wdc_target_play
 * @property string $wdc_actual_play
 * @property string $wdc_number_mdt
 * @property string $wdc_number_rft
 * @property string $wdc_res_pressure
 * @property string $wdc_last_pressure
 * @property string $wdc_pressure_gradient
 * @property string $wdc_last_temp
 * @property string $wdc_integrity
 * @property string $wdc_electro_avail
 * @property string $wdc_electro_list
 *
 * The followings are the available model relations:
 * @property RscDiscovery $dc
 * @property RscWellzoneDiscovery[] $rscWellzoneDiscoveries
 */
class WellDiscovery extends CActiveRecord
{
	public $lat_degree;
	public $lat_minute;
	public $lat_second;
	public $lat_direction;
	public $long_degree;
	public $long_minute;
	public $long_second;
	public $long_direction;
	
	public $wdc_electro_list1;
	public $wdc_electro_list2;
	public $wdc_electro_list3;
	public $wdc_electro_list4;
	public $wdc_electro_list5;
	public $wdc_electro_list6;
	public $wdc_electro_list7;
	public $wdc_electro_list8;
	public $wdc_electro_list9;
	public $wdc_electro_list10;
	public $wdc_electro_list11;
	public $wdc_electro_list12;
	public $wdc_electro_list13;
	public $wdc_electro_list14;
	public $wdc_electro_list15;
	public $wdc_electro_list16;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_well_discovery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wdc_name, lat_degree, lat_minute, lat_second, lat_direction, long_degree, long_minute, long_second, long_direction, wdc_type, wdc_shore, wdc_terrain, wdc_formation', 'required'),
			array(
				'lat_degree, lat_minute, lat_second, long_degree, long_minute, long_second',
				'numerical',
				'numberPattern'=>'/^\s*[-+]?\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'wdc_target_depth_tvd, wdc_target_depth_md, wdc_actual_depth, wdc_target_play, wdc_actual_play, wdc_res_pressure,
				wdc_last_pressure, wdc_pressure_gradient, wdc_last_temp',
				'numerical',
				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('dc_id, wdc_total_zone', 'numerical', 'integerOnly'=>true),
			array(
				'lat_direction, long_direction',
				'length',
				'max'=>1
			),
			array(
				'lat_degree, lat_minute, lat_second, long_degree, long_minute, long_second',
				'length',
				'max'=>8
			),
			array('wdc_name, wdc_latitude, wdc_longitude, wdc_type, wdc_shore, wdc_terrain, wdc_status, wdc_formation, wdc_date_complete, wdc_target_depth_tvd, wdc_target_depth_md, wdc_actual_depth, wdc_target_play, wdc_actual_play, wdc_number_mdt, wdc_number_rft, wdc_res_pressure, wdc_last_pressure, wdc_pressure_gradient, wdc_last_temp, wdc_integrity, wdc_electro_avail, wdc_electro_list', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wdc_id, dc_id, wdc_name, wdc_total_zone, wdc_latitude, wdc_longitude, wdc_type, wdc_shore, wdc_terrain, wdc_status, wdc_formation, wdc_date_complete, wdc_target_depth_tvd, wdc_target_depth_md, wdc_actual_depth, wdc_target_play, wdc_actual_play, wdc_number_mdt, wdc_number_rft, wdc_res_pressure, wdc_last_pressure, wdc_pressure_gradient, wdc_last_temp, wdc_integrity, wdc_electro_avail, wdc_electro_list', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dc' => array(self::BELONGS_TO, 'RscDiscovery', 'dc_id'),
			'rscWellzoneDiscoveries' => array(self::HAS_MANY, 'RscWellzoneDiscovery', 'wdc_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wdc_id' => 'Wdc',
			'dc_id' => 'Dc',
			'wdc_name' => 'Well Name',
			'lat_degree' => 'Well Latitude Degree',
			'lat_minute' => 'Well Latitude Minute',
			'lat_second' => 'Well Latitude Second',
			'lat_direction' => 'Well Latitude Direction',
			'long_degree' => 'Well Longitude Degree',
			'long_minute' => 'Well Longitude Minute',
			'long_second' => 'Well Longitude Second',
			'long_direction' => 'Well Longitude Direction',
			'wdc_total_zone' => 'Wdc Total Zone',
			'wdc_latitude' => 'Wdc Latitude',
			'wdc_longitude' => 'Wdc Longitude',
			'wdc_type' => 'Well Type',
			'wdc_shore' => 'Well Onshore or Offshore',
			'wdc_terrain' => 'Well Terrain',
			'wdc_status' => 'Wdc Status',
			'wdc_formation' => 'Well Targeted Formation Name',
			'wdc_date_complete' => 'Wdc Date Complete',
			'wdc_target_depth_tvd' => 'Wdc Target Depth Tvd',
			'wdc_target_depth_md' => 'Wdc Target Depth Md',
			'wdc_actual_depth' => 'Wdc Actual Depth',
			'wdc_target_play' => 'Wdc Target Play',
			'wdc_actual_play' => 'Wdc Actual Play',
			'wdc_number_mdt' => 'Wdc Number Mdt',
			'wdc_number_rft' => 'Wdc Number Rft',
			'wdc_res_pressure' => 'Wdc Res Pressure',
			'wdc_last_pressure' => 'Wdc Last Pressure',
			'wdc_pressure_gradient' => 'Wdc Pressure Gradient',
			'wdc_last_temp' => 'Wdc Last Temp',
			'wdc_integrity' => 'Wdc Integrity',
			'wdc_electro_avail' => 'Wdc Electro Avail',
			'wdc_electro_list' => 'Wdc Electro List',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wdc_id',$this->wdc_id);
		$criteria->compare('dc_id',$this->dc_id);
		$criteria->compare('wdc_name',$this->wdc_name,true);
		$criteria->compare('wdc_total_zone',$this->wdc_total_zone);
		$criteria->compare('wdc_latitude',$this->wdc_latitude,true);
		$criteria->compare('wdc_longitude',$this->wdc_longitude,true);
		$criteria->compare('wdc_type',$this->wdc_type,true);
		$criteria->compare('wdc_shore',$this->wdc_shore,true);
		$criteria->compare('wdc_terrain',$this->wdc_terrain,true);
		$criteria->compare('wdc_status',$this->wdc_status,true);
		$criteria->compare('wdc_formation',$this->wdc_formation,true);
		$criteria->compare('wdc_date_complete',$this->wdc_date_complete,true);
		$criteria->compare('wdc_target_depth_tvd',$this->wdc_target_depth_tvd,true);
		$criteria->compare('wdc_target_depth_md',$this->wdc_target_depth_md,true);
		$criteria->compare('wdc_actual_depth',$this->wdc_actual_depth,true);
		$criteria->compare('wdc_target_play',$this->wdc_target_play,true);
		$criteria->compare('wdc_actual_play',$this->wdc_actual_play,true);
		$criteria->compare('wdc_number_mdt',$this->wdc_number_mdt,true);
		$criteria->compare('wdc_number_rft',$this->wdc_number_rft,true);
		$criteria->compare('wdc_res_pressure',$this->wdc_res_pressure,true);
		$criteria->compare('wdc_last_pressure',$this->wdc_last_pressure,true);
		$criteria->compare('wdc_pressure_gradient',$this->wdc_pressure_gradient,true);
		$criteria->compare('wdc_last_temp',$this->wdc_last_temp,true);
		$criteria->compare('wdc_integrity',$this->wdc_integrity,true);
		$criteria->compare('wdc_electro_avail',$this->wdc_electro_avail,true);
		$criteria->compare('wdc_electro_list',$this->wdc_electro_list,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WellDiscovery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
