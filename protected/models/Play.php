<?php

/**
 * This is the model class for table "rsc_play".
 *
 * The followings are the available columns in table 'rsc_play':
 * @property integer $play_id
 * @property string $wk_id
 * @property integer $basin_id
 * @property integer $gcf_id
 * @property string $province_id
 * @property string $play_latitude
 * @property string $play_longitude
 * @property string $play_analog_to
 * @property string $play_analog_distance
 * @property string $play_exr_method
 * @property string $play_shore
 * @property string $play_terrain
 * @property string $play_near_field
 * @property string $play_near_infra_structure
 * @property string $play_support_data
 * @property string $play_outcrop_distance
 * @property string $play_s2d_year
 * @property string $play_s2d_crossline
 * @property string $play_s2d_line_intervall
 * @property string $play_s2d_img_quality
 * @property string $play_sgc
 * @property string $play_sgc_sample
 * @property string $play_sgc_depth
 * @property string $play_sgv
 * @property string $play_sgv_acre
 * @property string $play_sgv_depth
 * @property string $play_sel
 * @property string $play_sel_acre
 * @property string $play_sel_depth
 * @property string $play_srt
 * @property string $play_srt_acre
 * @property string $play_map_scale
 * @property string $play_map_author
 * @property string $play_map_year
 * @property string $play_remark
 * @property integer $play_update_from
 * @property string $play_submit_status
 * @property string $play_submit_date
 * @property integer $play_submit_revision
 *
 * The followings are the available model relations:
 * @property RscLead[] $rscLeads
 * @property AdmWorkingArea $wk
 * @property AdmBasin $basin
 * @property RscGcf $gcf
 * @property AdmProvince $province
 * @property RscProspect[] $rscProspects
 */
class Play extends CActiveRecord {
	public $bound_lat_top_left_degree;
	public $bound_lat_top_left_minute;
	public $bound_lat_top_left_second;
	public $bound_lat_top_left_direction;
	public $bound_lat_bottom_right_degree;
	public $bound_lat_bottom_right_minute;
	public $bound_lat_bottom_right_second;
	public $bound_lat_bottom_right_direction;
	public $bound_long_top_left_degree;
	public $bound_long_top_left_minute;
	public $bound_long_top_left_second;
	public $bound_long_top_left_direction;
	public $bound_long_bottom_right_degree;
	public $bound_long_bottom_right_minute;
	public $bound_long_bottom_right_second;
	public $bound_long_bottom_right_direction;
	public $basin_name;
	public $play_name;
	
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'rsc_play';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
				array (
					'bound_lat_top_left_degree, bound_lat_top_left_minute, bound_lat_top_left_second, bound_lat_top_left_direction, bound_lat_bottom_right_degree, bound_lat_bottom_right_minute, bound_lat_bottom_right_second, bound_lat_bottom_right_direction,
					bound_long_top_left_degree, bound_long_top_left_minute, bound_long_top_left_second, bound_long_top_left_direction, bound_long_bottom_right_degree, bound_long_bottom_right_minute, bound_long_bottom_right_second, bound_long_bottom_right_direction,
					basin_id, province_id, play_analog_to, play_analog_distance, play_exr_method, play_shore, play_terrain, play_near_field, play_near_infra_structure',
					'required' 
				),
				array(
					'bound_lat_top_left_degree, bound_lat_bottom_right_degree',
					'numerical',
					'min'=>0,
					'max'=>20,
					'numberPattern'=>'/^([1-9]{0,1})([0-9]{1})?$/'
				),
				array(
					'bound_long_top_left_degree, bound_long_bottom_right_degree',
					'numerical',
					'min'=>90,
					'max'=>145,
					'numberPattern'=>'/^([1-9]{0,2})([0-9]{2})?$/'
				),
				array(
					'bound_lat_top_left_minute, bound_lat_bottom_right_minute, bound_long_top_left_minute, bound_long_bottom_right_minute',
					'numerical',
					'min'=>0,
					'max'=>59
				),
				array(
					'bound_lat_top_left_second, bound_lat_bottom_right_second,
					bound_long_top_left_second, bound_long_bottom_right_second',
					'numerical',
					'min'=>0,
					'max'=>59.999,
					'numberPattern'=>'/^\d{1,6}(\.\d{1,3})?$/'
				),
				array(
					'play_outcrop_distance',
					'numerical',
					'min'=>0,
					'max'=>8514,
// 					'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
				),
				array(
					'play_s2d_year',
					'match',
					'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
				),
				array(
					'play_map_scale',
					'match',
					'pattern'=>'/^[1-9]+\d*(\:[1-9]\d{0,9})+$/' //skala
				),
				array (
						'basin_id, gcf_id, play_update_from, play_submit_revision',
						'numerical',
						'integerOnly' => true 
				),
				array(
					'bound_lat_top_left_direction, bound_lat_bottom_right_direction, bound_long_top_left_direction, bound_long_bottom_right_direction',
					'length',
					'max'=>1
				),
				array (
						'wk_id',
						'length',
						'max' => 6 
				),
				array (
						'province_id',
						'length',
						'max' => 4 
				),
				array (
						'play_latitude, play_longitude, play_analog_to, play_analog_distance, 
						play_exr_method, play_terrain, play_near_field, play_near_infra_structure, 
						play_support_data, play_outcrop_distance, play_s2d_year, play_s2d_crossline, 
						play_s2d_line_intervall, play_s2d_img_quality, play_sgc, play_sgc_sample, 
						play_sgc_depth, play_sgv, play_sgv_acre, play_sgv_depth, play_sel, 
						play_sel_acre, play_sel_depth, play_srt, play_srt_acre, play_map_scale, 
						play_map_author, play_map_year, play_submit_status',
						'length',
						'max' => 50 
				),
				array (
						'bound_lat_top_left_degree, bound_lat_top_left_minute, bound_lat_top_left_second, bound_lat_bottom_right_degree, bound_lat_bottom_right_minute, bound_lat_bottom_right_second, 
						bound_long_top_left_degree, bound_long_top_left_minute, bound_long_top_left_second, bound_long_bottom_right_degree, bound_long_bottom_right_minute, bound_long_bottom_right_second,
						play_shore',
						'length',
						'max' => 8 
				),
				array (
						'play_submit_date',
						'safe' 
				),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array (
						'play_id, wk_id, basin_id, gcf_id, province_id, play_latitude, play_longitude, play_analog_to, play_analog_distance, play_exr_method, play_shore, play_terrain, play_near_field, play_near_infra_structure, play_support_data, play_outcrop_distance, play_s2d_year, play_s2d_crossline, play_s2d_line_intervall, play_s2d_img_quality, play_sgc, play_sgc_sample, play_sgc_depth, play_sgv, play_sgv_acre, play_sgv_depth, play_sel, play_sel_acre, play_sel_depth, play_srt, play_srt_acre, play_map_scale, play_map_author, play_map_year, play_remark, play_update_from, play_submit_status, play_submit_date, play_submit_revision',
						'safe',
						'on' => 'search' 
				),
				array (
						'play_id, wk_id, basin_id, basin_name, play_name, gcf_id, province_id, play_latitude, play_longitude, play_analog_to, play_analog_distance, play_exr_method, play_shore, play_terrain, play_near_field, play_near_infra_structure, play_support_data, play_outcrop_distance, play_s2d_year, play_s2d_crossline, play_s2d_line_intervall, play_s2d_img_quality, play_sgc, play_sgc_sample, play_sgc_depth, play_sgv, play_sgv_acre, play_sgv_depth, play_sel, play_sel_acre, play_sel_depth, play_srt, play_srt_acre, play_map_scale, play_map_author, play_map_year, play_remark, play_update_from, play_submit_status, play_submit_date, play_submit_revision',
						'safe',
						'on' => 'searchRelated' 
				)
		);
	}
	
	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array (
				'rscLeads' => array (
						self::HAS_MANY,
						'RscLead',
						'play_id' 
				),
				'wk' => array (
						self::BELONGS_TO,
						'AdmWorkingArea',
						'wk_id' 
				),
				'basin' => array (
						self::BELONGS_TO,
						'Basin',
						'basin_id' 
				),
				'gcfs' => array (
						self::BELONGS_TO,
						'Gcf',
						'gcf_id' 
				),
				'province' => array (
						self::BELONGS_TO,
						'AdmProvince',
						'province_id' 
				),
				'rscProspects' => array (
						self::HAS_MANY,
						'RscProspect',
						'play_id' 
				) 
		);
	}
	
	/**
	 *
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array (
				'bound_lat_top_left_degree'=>'Boundary Latitude Top Left Degree',
				'bound_lat_top_left_minute'=>'Boundary Latitude Top Left Minute',
				'bound_lat_top_left_second'=>'Boundary Latitude Top Left Second',
				'bound_lat_top_left_direction'=>'Boundary Latitude Top Left Direction',
				'bound_lat_bottom_right_degree'=>'Boundary Latitude Bottom Right Degree',
				'bound_lat_bottom_right_minute'=>'Boundary Latitude Bottom Right Minute',
				'bound_lat_bottom_right_second'=>'Boundary Latitude Bottom Right Second',
				'bound_lat_bottom_right_direction'=>'Boundary Latitude Bottom Right Direction',
				'bound_long_top_left_degree'=>'Boundary Longitude Top Left Degree',
				'bound_long_top_left_minute'=>'Boundary Longitude Top Left Minute',
				'bound_long_top_left_second'=>'Boundary Longitude Top Left Second',
				'bound_long_top_left_direction'=>'Boundary Longitude Top Left Direction',
				'bound_long_bottom_right_degree'=>'Boundary Longitude Bottom Right Degree',
				'bound_long_bottom_right_minute'=>'Boundary Longitude Bottom Right Minute',				
				'bound_long_bottom_right_second'=>'Boundary Longitude Bottom Right Second',
				'bound_long_bottom_right_direction'=>'Boundary Longitude Bottom Right Direction',
				'play_id' => 'Play',
				'wk_id' => 'Wk',
				'basin_id' => 'Basin',
				'gcf_id' => 'Gcf',
				'province_id' => 'Province',
				'play_latitude' => 'Play Latitude',
				'play_longitude' => 'Play Longitude',
				'play_analog_to' => 'By Analog To',
				'play_analog_distance' => 'Distance to Analog',
				'play_exr_method' => 'Exploration Methods',
				'play_shore' => 'Onshore or Offshore',
				'play_terrain' => 'Terrain',
				'play_near_field' => 'Nearby Field',
				'play_near_infra_structure' => 'Nearby Infra Structure',
				'play_support_data' => 'Play Support Data',
				'play_outcrop_distance' => 'Play Outcrop Distance',
				'play_s2d_year' => 'Play S2d Year',
				'play_s2d_crossline' => 'Play S2d Crossline',
				'play_s2d_line_intervall' => 'Play S2d Line Intervall',
				'play_s2d_img_quality' => 'Play S2d Img Quality',
				'play_sgc' => 'Play Sgc',
				'play_sgc_sample' => 'Play Sgc Sample',
				'play_sgc_depth' => 'Play Sgc Depth',
				'play_sgv' => 'Play Sgv',
				'play_sgv_acre' => 'Play Sgv Acre',
				'play_sgv_depth' => 'Play Sgv Depth',
				'play_sel' => 'Play Sel',
				'play_sel_acre' => 'Play Sel Acre',
				'play_sel_depth' => 'Play Sel Depth',
				'play_srt' => 'Play Srt',
				'play_srt_acre' => 'Play Srt Acre',
				'play_map_scale' => 'Play Map Scale',
				'play_map_author' => 'Play Map Author',
				'play_map_year' => 'Play Map Year',
				'play_remark' => 'Play Remark',
				'play_update_from' => 'Play Update From',
				'play_submit_status' => 'Play Submit Status',
				'play_submit_date' => 'Play Submit Date',
				'play_submit_revision' => 'Play Submit Revision' 
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 *         based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria ();
		
		$criteria->compare ( 'play_id', $this->play_id );
		$criteria->compare ( 'wk_id', $this->wk_id, true );
		$criteria->compare ( 'basin_id', $this->basin_id );
		$criteria->compare ( 'gcf_id', $this->gcf_id );
		$criteria->compare ( 'province_id', $this->province_id, true );
		$criteria->compare ( 'play_latitude', $this->play_latitude, true );
		$criteria->compare ( 'play_longitude', $this->play_longitude, true );
		$criteria->compare ( 'play_analog_to', $this->play_analog_to, true );
		$criteria->compare ( 'play_analog_distance', $this->play_analog_distance, true );
		$criteria->compare ( 'play_exr_method', $this->play_exr_method, true );
		$criteria->compare ( 'play_shore', $this->play_shore, true );
		$criteria->compare ( 'play_terrain', $this->play_terrain, true );
		$criteria->compare ( 'play_near_field', $this->play_near_field, true );
		$criteria->compare ( 'play_near_infra_structure', $this->play_near_infra_structure, true );
		$criteria->compare ( 'play_support_data', $this->play_support_data, true );
		$criteria->compare ( 'play_outcrop_distance', $this->play_outcrop_distance, true );
		$criteria->compare ( 'play_s2d_year', $this->play_s2d_year, true );
		$criteria->compare ( 'play_s2d_crossline', $this->play_s2d_crossline, true );
		$criteria->compare ( 'play_s2d_line_intervall', $this->play_s2d_line_intervall, true );
		$criteria->compare ( 'play_s2d_img_quality', $this->play_s2d_img_quality, true );
		$criteria->compare ( 'play_sgc', $this->play_sgc, true );
		$criteria->compare ( 'play_sgc_sample', $this->play_sgc_sample, true );
		$criteria->compare ( 'play_sgc_depth', $this->play_sgc_depth, true );
		$criteria->compare ( 'play_sgv', $this->play_sgv, true );
		$criteria->compare ( 'play_sgv_acre', $this->play_sgv_acre, true );
		$criteria->compare ( 'play_sgv_depth', $this->play_sgv_depth, true );
		$criteria->compare ( 'play_sel', $this->play_sel, true );
		$criteria->compare ( 'play_sel_acre', $this->play_sel_acre, true );
		$criteria->compare ( 'play_sel_depth', $this->play_sel_depth, true );
		$criteria->compare ( 'play_srt', $this->play_srt, true );
		$criteria->compare ( 'play_srt_acre', $this->play_srt_acre, true );
		$criteria->compare ( 'play_map_scale', $this->play_map_scale, true );
		$criteria->compare ( 'play_map_author', $this->play_map_author, true );
		$criteria->compare ( 'play_map_year', $this->play_map_year, true );
		$criteria->compare ( 'play_remark', $this->play_remark, true );
		$criteria->compare ( 'play_update_from', $this->play_update_from );
		$criteria->compare ( 'play_submit_status', $this->play_submit_status, true );
		$criteria->compare ( 'play_submit_date', $this->play_submit_date, true );
		$criteria->compare ( 'play_submit_revision', $this->play_submit_revision );
		
		return new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
	}

	public function searchRelated() {

		// SELECT adm_basin.basin_name, CONCAT(
		// 	rsc_gcf.gcf_res_lithology, ' - ', rsc_gcf.gcf_res_formation, ' - ', 
		// 	rsc_gcf.gcf_res_age_serie, ' ', rsc_gcf.gcf_res_age_system, ' - ', rsc_gcf.gcf_res_depos_env, ' - ', rsc_gcf.gcf_trap_type
		// ) as play_name
		// FROM rsc_play a
		// INNER JOIN adm_basin ON a.basin_id = adm_basin.basin_id
		// INNER JOIN rsc_gcf ON a.gcf_id = rsc_gcf.gcf_id
		// WHERE wk_id = 'WK1230' AND a.play_submit_date =(
		// 	SELECT max(b.play_submit_date)
		// 	FROM rsc_play b
		// 	WHERE b.play_update_from = a.play_update_from
		// )
		// AND adm_basin.basin_name LIKE '%%'
		// AND CONCAT(
		// 	rsc_gcf.gcf_res_lithology, ' - ', rsc_gcf.gcf_res_formation, ' - ', 
		// 	rsc_gcf.gcf_res_age_serie, ' ', rsc_gcf.gcf_res_age_system, ' - ', rsc_gcf.gcf_res_depos_env, ' - ', rsc_gcf.gcf_trap_type
		// ) LIKE ('%%')
		// GROUP BY a.play_update_from

		$criteria = new CDbCriteria;
	    $criteria->select="max(b.play_submit_date)";
	    $criteria->alias = 'b';
	    $criteria->condition="b.play_update_from = t.play_update_from";
	    $subQuery=$this->getCommandBuilder()->createFindCommand($this->getTableSchema(),$criteria)->getText();
	    
	    $mainCriteria = new CDbCriteria;
	    $mainCriteria->together = true;
	    $mainCriteria->with = array('gcfs', 'basin');
	    $mainCriteria->condition = 'wk_id ="' . Yii::app()->user->wk_id . '"';
	    $mainCriteria->addCondition('play_submit_date = (' . $subQuery . ')', 'AND');
	    $mainCriteria->addCondition('basin.basin_name LIKE "%' . $this->basin_name . '%"', 'AND');
	    $mainCriteria->addCondition('CONCAT(
				gcfs.gcf_res_lithology, " - ", gcfs.gcf_res_formation, " - ", 
				gcfs.gcf_res_age_serie, " ", gcfs.gcf_res_age_system, " - ", gcfs.gcf_res_depos_env, " - ", gcfs.gcf_trap_type
			) LIKE "%' . $this->play_name . '%"', 'AND');
	    $mainCriteria->group = 'play_update_from';

	    return new CActiveDataProvider(get_class($this), array(
	        'criteria'=>$mainCriteria,
	        'sort'=>array(
	            'defaultOrder'=>'basin_urut'
	          )
	      ));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * 
	 * @param string $className
	 *        	active record class name.
	 * @return Play the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
