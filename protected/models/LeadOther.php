<?php

/**
 * This is the model class for table "rsc_lead_other".
 *
 * The followings are the available columns in table 'rsc_lead_other':
 * @property integer $lsor_id
 * @property integer $lead_id
 * @property string $lsor_year_survey
 * @property double $lsor_low_estimate
 * @property double $lsor_best_estimate
 * @property double $lsor_high_estimate
 * @property string $lsor_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class LeadOther extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_other';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLeadOther'),
// 			array('lsor_low_estimate, lsor_best_estimate, lsor_high_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'lsor_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array('lsor_low_estimate, lsor_best_estimate, lsor_high_estimate', 'numerical'),
			array('lsor_year_survey', 'length', 'max'=>50),
			array('lsor_remark', 'safe'),
// 			array('lsor_low_estimate, lsor_best_estimate, lsor_high_estimate', 'chkDependentArea2d'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lsor_id, lead_id, lsor_year_survey, lsor_low_estimate, lsor_best_estimate, lsor_high_estimate, lsor_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lsor_id' => 'Lsor',
			'lead_id' => 'Lead',
			'lsor_year_survey' => 'Other Acquisition Year',
			'lsor_low_estimate' => 'Other Low Estimate',
			'lsor_best_estimate' => 'Other Best Estimate',
			'lsor_high_estimate' => 'Other High Estimate',
			'lsor_remark' => 'Lsor Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lsor_id',$this->lsor_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('lsor_year_survey',$this->lsor_year_survey,true);
		$criteria->compare('lsor_low_estimate',$this->lsor_low_estimate);
		$criteria->compare('lsor_best_estimate',$this->lsor_best_estimate);
		$criteria->compare('lsor_high_estimate',$this->lsor_high_estimate);
		$criteria->compare('lsor_remark',$this->lsor_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeadOther the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentArea2d($attribute)
// 	{
// 		if($attribute == 'lsor_high_estimate')
// 		{
// 			if($this->lsor_high_estimate == 0 || $this->lsor_high_estimate <= $this->lsor_best_estimate || $this->lsor_high_estimate <= $this->lsor_low_estimate)
// 				$this->addError($this->lsor_high_estimate, $this->getAttributeLabel($attribute) . ' must greater than Other Best Estimate and Other Low Estimate');
// 		}
		
// 		if($attribute == 'lsor_best_estimate')
// 		{
// 			if($this->lsor_best_estimate == 0 || $this->lsor_best_estimate <= $this->lsor_low_estimate)
// 				$this->addError($this->lsor_best_estimate, $this->getAttributeLabel($attribute) . ' must greater than Other Low Estimate');
// 		}
// 	}
	
	public function chkBoxLeadOther()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('lsor_year_survey', 'lsor_low_estimate', 'lsor_best_estimate', 'lsor_high_estimate');
			$required->validate($this);
				
			Yii::import('ext.validasi.DependentAreaOther');
			$checking = new DependentAreaOther;
			$checking->attributes = array('lsor_low_estimate', 'lsor_best_estimate', 'lsor_high_estimate');
			$checking->validate($this);
		}
	}
}
