<?php

/**
 * This is the model class for table "adm_basin".
 *
 * The followings are the available columns in table 'adm_basin':
 * @property integer $basin_id
 * @property string $basin_name
 * @property string $basin_type
 * @property string $basin_status
 * @property double $basin_area
 *
 * The followings are the available model relations:
 * @property RscPlay[] $rscPlays
 */
class Basin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_basin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('basin_area', 'numerical'),
			array('basin_name, basin_status', 'length', 'max'=>50),
			array('basin_type', 'length', 'max'=>254),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('basin_id, basin_name, basin_type, basin_status, basin_area', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rscPlays' => array(self::HAS_MANY, 'RscPlay', 'basin_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'basin_id' => 'Basin',
			'basin_name' => 'Basin Name',
			'basin_type' => 'Basin Type',
			'basin_status' => 'Basin Status',
			'basin_area' => 'Basin Area',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('basin_id',$this->basin_id);
		$criteria->compare('basin_name',$this->basin_name,true);
		$criteria->compare('basin_type',$this->basin_type,true);
		$criteria->compare('basin_status',$this->basin_status,true);
		$criteria->compare('basin_area',$this->basin_area);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Basin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
