<?php

/**
 * This is the model class for table "adm_participant".
 *
 * The followings are the available columns in table 'adm_participant':
 * @property integer $participant_id
 * @property integer $kkks_id
 * @property integer $psc_id
 * @property double $interest_value
 * @property integer $is_kkks_operator
 *
 * The followings are the available model relations:
 * @property AdmKkks $kkks
 * @property AdmPsc $psc
 */
class Participant extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_participant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kkks_id, psc_id, is_kkks_operator', 'numerical', 'integerOnly'=>true),
			array('interest_value', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('participant_id, kkks_id, psc_id, interest_value, is_kkks_operator', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kkkses' => array(self::BELONGS_TO, 'AdmKkks', 'kkks_id'),
			'psces' => array(self::BELONGS_TO, 'Psc', 'psc_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'participant_id' => 'Participant',
			'kkks_id' => 'Kkks',
			'psc_id' => 'Psc',
			'interest_value' => 'Interest Value',
			'is_kkks_operator' => 'Is Kkks Operator',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('participant_id',$this->participant_id);
		$criteria->compare('kkks_id',$this->kkks_id);
		$criteria->compare('psc_id',$this->psc_id);
		$criteria->compare('interest_value',$this->interest_value);
		$criteria->compare('is_kkks_operator',$this->is_kkks_operator);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Participant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
