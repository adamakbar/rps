<?php

/**
 * This is the model class for table "rsc_geological".
 *
 * The followings are the available columns in table 'rsc_geological':
 * @property integer $sgf_id
 * @property integer $prospect_id
 * @property string $sgf_year_survey
 * @property string $sgf_survey_method
 * @property string $sgf_coverage_area
 * @property double $sgf_p90_area
 * @property double $sgf_p90_thickness
 * @property double $sgf_p90_net
 * @property double $sgf_p50_area
 * @property double $sgf_p50_thickness
 * @property double $sgf_p50_net
 * @property double $sgf_p10_area
 * @property double $sgf_p10_thickness
 * @property double $sgf_p10_net
 * @property string $sgf_remark
 *
 * The followings are the available model relations:
 * @property RscProspect $prospect
 */
class Geological extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_geological';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxGeological'),
// 			array('sgf_year_survey, sgf_p90_area, sgf_p50_area, sgf_p10_area, sgf_p90_thickness, sgf_p50_thickness, sgf_p10_thickness, sgf_p90_net, sgf_p50_net, sgf_p10_net', 'required'),
			array('prospect_id', 'numerical', 'integerOnly'=>true),
			array(
				'sgf_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'sgf_coverage_area, sgf_p90_area, sgf_p90_thickness, sgf_p50_area, sgf_p50_thickness, sgf_p10_area, sgf_p10_thickness',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array(
				'sgf_p90_net, sgf_p50_net, sgf_p10_net',
				'numerical',
				'min'=>0,
				'max'=>100,
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('sgf_year_survey, sgf_survey_method, sgf_coverage_area', 'length', 'max'=>50),
			array('sgf_remark', 'safe'),
// 			array('sgf_p90_area, sgf_p50_area, sgf_p10_area, sgf_p90_thickness, sgf_p50_thickness, sgf_p10_thickness, sgf_p90_net, sgf_p50_net, sgf_p10_net', 'chkDependentGeological'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sgf_id, prospect_id, sgf_year_survey, sgf_survey_method, sgf_coverage_area, sgf_p90_area, sgf_p90_thickness, sgf_p90_net, sgf_p50_area, sgf_p50_thickness, sgf_p50_net, sgf_p10_area, sgf_p10_thickness, sgf_p10_net, sgf_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prospect' => array(self::BELONGS_TO, 'RscProspect', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sgf_id' => 'Sgf',
			'prospect_id' => 'Prospect',
			'sgf_year_survey' => 'Geological Acquisition Year',
			'sgf_survey_method' => 'Sgf Survey Method',
			'sgf_coverage_area' => 'Sgf Coverage Area',
			'sgf_p90_area' => 'Geological P90 Areal Closure Estimation',
			'sgf_p90_thickness' => 'Geological P90 Gross Sand Thickness',
			'sgf_p90_net' => 'Geological P90 Net to Gross',
			'sgf_p50_area' => 'Geological P50 Areal Closure Estimation',
			'sgf_p50_thickness' => 'Geological P50 Gross Sand Thickness',
			'sgf_p50_net' => 'Geological P50 Net to Gross',
			'sgf_p10_area' => 'Geological P10 Areal Closure Estimation',
			'sgf_p10_thickness' => 'Geological P10 Gross Sand Thickness',
			'sgf_p10_net' => 'Geological P10 Net to Gross',
			'sgf_remark' => 'Sgf Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sgf_id',$this->sgf_id);
		$criteria->compare('prospect_id',$this->prospect_id);
		$criteria->compare('sgf_year_survey',$this->sgf_year_survey,true);
		$criteria->compare('sgf_survey_method',$this->sgf_survey_method,true);
		$criteria->compare('sgf_coverage_area',$this->sgf_coverage_area,true);
		$criteria->compare('sgf_p90_area',$this->sgf_p90_area);
		$criteria->compare('sgf_p90_thickness',$this->sgf_p90_thickness);
		$criteria->compare('sgf_p90_net',$this->sgf_p90_net);
		$criteria->compare('sgf_p50_area',$this->sgf_p50_area);
		$criteria->compare('sgf_p50_thickness',$this->sgf_p50_thickness);
		$criteria->compare('sgf_p50_net',$this->sgf_p50_net);
		$criteria->compare('sgf_p10_area',$this->sgf_p10_area);
		$criteria->compare('sgf_p10_thickness',$this->sgf_p10_thickness);
		$criteria->compare('sgf_p10_net',$this->sgf_p10_net);
		$criteria->compare('sgf_remark',$this->sgf_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Geological the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentGeological($attribute)
// 	{
// 		if($attribute == 'sgf_p10_area')
// 		{
// 			if($this->sgf_p10_area != '') {
// 				if($this->sgf_p10_area == 0 || $this->sgf_p10_area <= $this->sgf_p50_area || $this->sgf_p10_area <= $this->sgf_p90_area)
// 					$this->addError('sgf_p10_area', $this->getAttributeLabel($attribute) . ' must greater than Geological P50 Areal Closure Estimation and Geological P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'sgf_p50_area')
// 		{
// 			if($this->sgf_p50_area != '') {
// 				if($this->sgf_p50_area == 0 || $this->sgf_p50_area <= $this->sgf_p90_area)
// 					$this->addError('sgf_p50_area', $this->getAttributeLabel($attribute) . ' must greater than Geological P90 Areal Closure Estimation');
// 			}
// 		}
		
// 		if($attribute == 'sgf_p10_thickness')
// 		{
// 			if($this->sgf_p10_thickness != '') {
// 				if($this->sgf_p10_thickness == 0 || $this->sgf_p10_thickness <= $this->sgf_p50_thickness || $this->sgf_p10_thickness <= $this->sgf_p90_thickness)
// 					$this->addError('sgf_p10_thickness', $this->getAttributeLabel($attribute) . ' must greater than Geological P50 Gross Sand Thickness and Geological P90 Gross Sand Thickness');
// 			}
// 		}
		
// 		if($attribute == 'sgf_p50_thickness')
// 		{
// 			if($this->sgf_p50_thickness != '') {
// 				if($this->sgf_p50_thickness == 0 || $this->sgf_p50_thickness <= $this->sgf_p90_thickness)
// 					$this->addError('sgf_p50_thickness', $this->getAttributeLabel($attribute) . ' must greater than Geological P90 Gross Sand Thickness');
// 			}
// 		}
		
// 		if($attribute == 'sgf_p10_net')
// 		{
// 			if($this->sgf_p10_net != '') {
// 				if($this->sgf_p10_net == 0 || $this->sgf_p10_net <= $this->sgf_p50_net || $this->sgf_p10_net <= $this->sgf_p90_net)
// 					$this->addError('sgf_p10_net', $this->getAttributeLabel($attribute) . ' must greater than Geological P50 Net to Gross and Geological P90 Net to Gross');
// 			}
// 		}
		
// 		if($attribute == 'sgf_p50_net')
// 		{
// 			if($this->sgf_p50_net != '') {
// 				if($this->sgf_p50_net == 0 || $this->sgf_p50_net <= $this->sgf_p90_net)
// 					$this->addError('sgf_p50_net', $this->getAttributeLabel($attribute) . ' must greater than Geological P90 Net to Gross');
// 			}
// 		}
		
// 	}
	
	public function chkBoxGeological()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('sgf_year_survey', 'sgf_p90_area', 'sgf_p50_area', 'sgf_p10_area', 'sgf_p90_thickness', 'sgf_p50_thickness', 'sgf_p10_thickness', 'sgf_p90_net', 'sgf_p50_net', 'sgf_p10_net');
			$required->validate($this);
		
			Yii::import('ext.validasi.DependentGeological');
			$checking = new DependentGeological;
			$checking->attributes = array('sgf_p90_area', 'sgf_p50_area', 'sgf_p10_area', 'sgf_p90_thickness', 'sgf_p50_thickness', 'sgf_p10_thickness', 'sgf_p90_net', 'sgf_p50_net', 'sgf_p10_net');
			$checking->validate($this);
		}
	}
}
