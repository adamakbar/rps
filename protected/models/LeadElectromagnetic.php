<?php

/**
 * This is the model class for table "rsc_lead_electromagnetic".
 *
 * The followings are the available columns in table 'rsc_lead_electromagnetic':
 * @property integer $lsel_id
 * @property integer $lead_id
 * @property string $lsel_year_survey
 * @property string $lsel_survey_method
 * @property string $lsel_coverage_area
 * @property string $lsel_range_penetration
 * @property string $lsel_spacing_interval
 * @property double $lsel_low_estimate
 * @property double $lsel_best_estimate
 * @property double $lsel_high_estimate
 * @property string $lsel_remark
 *
 * The followings are the available model relations:
 * @property RscLead $lead
 */
class LeadElectromagnetic extends CActiveRecord
{
	public $is_checked;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rsc_lead_electromagnetic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_checked', 'chkBoxLeadElectromagnetic'),
// 			array('lsel_low_estimate, lsel_best_estimate, lsel_high_estimate', 'required'),
			array('lead_id', 'numerical', 'integerOnly'=>true),
			array(
				'lsel_year_survey',
				'match',
				'pattern'=>'/^\d{4,4}(\,\d{4,4})*$/u' //'/^[0-9]+(,[0-9]+)*$/u' //'/^([0-9]+,?)+$/u',
			),
			array(
				'lsel_coverage_area, lsel_range_penetration, lsel_spacing_interval',
				'numerical',
// 				'numberPattern'=>'/^\d{1,6}(\.\d{1,2})?$/'
			),
			array('lsel_low_estimate, lsel_best_estimate, lsel_high_estimate', 'numerical'),
			array('lsel_year_survey, lsel_survey_method, lsel_coverage_area, lsel_range_penetration, lsel_spacing_interval', 'length', 'max'=>50),
			array('lsel_remark', 'safe'),
// 			array('lsel_low_estimate, lsel_best_estimate, lsel_high_estimate', 'chkDependentAreaElectromagnetic'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lsel_id, lead_id, lsel_year_survey, lsel_survey_method, lsel_coverage_area, lsel_range_penetration, lsel_spacing_interval, lsel_low_estimate, lsel_best_estimate, lsel_high_estimate, lsel_remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'RscLead', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lsel_id' => 'Lsel',
			'lead_id' => 'Lead',
			'lsel_year_survey' => 'Electromagnetic Acquisition Year',
			'lsel_survey_method' => 'Electromagnetic Survey Method',
			'lsel_coverage_area' => 'Electromagnetic Survey Coverage Area',
			'lsel_range_penetration' => 'Electromagnetic Depth Survey Penetration Range',
			'lsel_spacing_interval' => 'Electromagnetic Recorder Spacing Interval',
			'lsel_low_estimate' => 'Electromagnetic Low Estimate',
			'lsel_best_estimate' => 'Electromagnetic Best Estimate',
			'lsel_high_estimate' => 'Electromagnetic High Estimate',
			'lsel_remark' => 'Lsel Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lsel_id',$this->lsel_id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('lsel_year_survey',$this->lsel_year_survey,true);
		$criteria->compare('lsel_survey_method',$this->lsel_survey_method,true);
		$criteria->compare('lsel_coverage_area',$this->lsel_coverage_area,true);
		$criteria->compare('lsel_range_penetration',$this->lsel_range_penetration,true);
		$criteria->compare('lsel_spacing_interval',$this->lsel_spacing_interval,true);
		$criteria->compare('lsel_low_estimate',$this->lsel_low_estimate);
		$criteria->compare('lsel_best_estimate',$this->lsel_best_estimate);
		$criteria->compare('lsel_high_estimate',$this->lsel_high_estimate);
		$criteria->compare('lsel_remark',$this->lsel_remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeadElectromagnetic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
// 	public function chkDependentAreaElectromagnetic($attribute)
// 	{
// 		if($attribute == 'lsel_high_estimate')
// 		{
// 			if($this->lsel_high_estimate == 0 || $this->lsel_high_estimate <= $this->lsel_best_estimate || $this->lsel_high_estimate <= $this->lsel_low_estimate)
// 				$this->addError($this->lsel_high_estimate, $this->getAttributeLabel($attribute) . ' must greater than Electromagnetic Best Estimate and Electromagnetic Low Estimate');
// 		}
		
// 		if($attribute == 'lsel_best_estimate')
// 		{
// 			if($this->lsel_best_estimate == 0 || $this->lsel_best_estimate <= $this->lsel_low_estimate)
// 				$this->addError($this->lsel_best_estimate, $this->getAttributeLabel($attribute) . ' must greater than Electromagnetic Low Estimate');
// 		}
// 	}
	
	public function chkBoxLeadElectromagnetic()
	{
		if($this->is_checked == 1)
		{
			$required = new CRequiredValidator;
			$required->attributes = array('lsel_year_survey', 'lsel_low_estimate', 'lsel_best_estimate', 'lsel_high_estimate');
			$required->validate($this);
				
			Yii::import('ext.validasi.DependentAreaElectromagnetic');
			$checking = new DependentAreaElectromagnetic;
			$checking->attributes = array('lsel_low_estimate', 'lsel_best_estimate', 'lsel_high_estimate');
			$checking->validate($this);
		}
	}
}
