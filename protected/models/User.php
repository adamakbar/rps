<?php

/**
 * This is the model class for table "adm_user".
 *
 * The followings are the available columns in table 'adm_user':
 * @property integer $user_id
 * @property string $wk_id
 * @property string $user_name
 * @property string $user_pass
 * @property string $user_level
 *
 * The followings are the available model relations:
 * @property AdmWorkingArea $wk
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adm_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wk_id', 'length', 'max'=>6),
			array('user_name, user_level', 'length', 'max'=>45),
			array('user_pass', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, wk_id, user_name, user_pass, user_level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'wk' => array(self::BELONGS_TO, 'AdmWorkingArea', 'wk_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'wk_id' => 'Wk',
			'user_name' => 'User Name',
			'user_pass' => 'User Pass',
			'user_level' => 'User Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('wk_id',$this->wk_id,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_pass',$this->user_pass,true);
		$criteria->compare('user_level',$this->user_level,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
