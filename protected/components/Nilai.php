<?php 

class Nilai {

  protected $sr_ker = 0.15;
  protected $sr_toc = 0.3;
  protected $sr_hfu = 0.07;
  protected $sr_mat = 0.45;
  protected $sr_otr = 0.03;

  protected $re_dis = 0.2;
  protected $re_lit = 0.2;
  protected $re_pri = 0.4;
  protected $re_sec = 0.2;

  protected $tr_sdi = 0.35;
  protected $tr_scn = 0.25;
  protected $tr_stp = 0.2;
  protected $tr_geo = 0.1;
  protected $tr_trp = 0.1;

  protected $dn_kit = 0.4;
  protected $dn_tec = 0.1;
  protected $dn_prv = 0.25;
  protected $dn_mig = 0.25;

  public function play($x, $y) {
    $gcfPlay = array (
      'pgcfsrock3' => array(
        'I' => 1.0,
        'I/II' => 0.9,
        'II' => 0.8,
        'II/III' => 0.7,
        'III' => 0.6,
        'IV' => 0.1,
        'Unknown' => 0.5
      ),
      'pgcfsrock4' => array(
        '0 - 0.5' => 0.2,
        '0.6 - 1.0' => 0.6, 
        '1.1 - 2.0' => 0.7,
        '2.1 - 4.0' => 0.8,
        '> 4' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcfsrock5' => array(
        '<= 1.0' => 0.5, 
        '1.1 - 2.0' => 0.7,
        '2.1 - 3.0' => 0.9,
        '> 3.0' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcfsrock8' => array(
        'Immature' => 0.1,
        'Early Mature' => 0.4,
        'Mature' => 1.0,
        'Over Mature' => 0.8,  
        'Unknown' => 0.5
      ),
      'pgcfsrock9' => array(
        'Yes' => 1.0,
        'No' => 0.0,
        'Unknown' => 0.5
      ),
      'pgcfres5' => array(
        'Single Distribution' => 1.0, 
        'Double Distribution' => 0.8,
        'Multiple Distribution' => 0.7,
        'Unknown' => 0.5
      ),
      'pgcfres7' => array(
        'Siltstone' => 0.5,
        'Sandstone' => 1.0,
        'Reef Carbonate' => 1.0,
        'Platform Carbonate' => 0.5,
        'Coal' => 0.6,
        'Alternate Rocks' => 0.5,
        'Naturally Fractured' => 0.7,
        'Others' => 0.3,
        'Unknown' => 0.5
      ),
      'pgcfres10' => array(
        'Vugs Porosity' => 0.4,
        'Fracture Porosity' => 0.9,
        'Integrated Porosity' => 1.0,
        'None' => 0.0,
        'Unknown' => 0.5
      ),
      'pgcfres9' => array(
        '0 - 10' => 0.4,
        '11 - 15' => 0.6,
        '16 - 20' => 0.7,
        '21 - 30' => 0.9,
        '> 30' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcftrap3' => array(
        'Single Distribution Impermeable Rocks' => 0.6,
        'Double Distribution Impermeable Rocks' => 0.8,
        'Multiple Distribution Impermeable Rocks' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcftrap4' => array(
        'Tank' => 0.4,
        'Truncated' => 0.5,
        'Limited Spread' => 0.7,
        'Spread' => 0.9,
        'Extensive Spread' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcftrap5' => array(
        'Primary' => 0.8,
        'Diagenetic' => 0.9,
        'Alternate Tectonic Mech' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcftrap7' => array(
        'Anticline Hangingwall' => 0.9,
        'Anticline Footwall' => 1.0,
        'Anticline Pericline' => 0.9,
        'Anticline Dome' => 0.9,
        'Anticline Nose' => 0.9,
        'Anticline Unqualified' => 0.9,
        'Faulted Anticline Hangingwall' => 0.8,
        'Faulted Anticline Footwall' => 0.8,
        'Faulted Anticline Pericline' => 0.8,
        'Faulted Anticline Dome' => 0.8,
        'Faulted Anticline Nose' => 0.8,
        'Faulted Anticline Unqualified' => 0.8,
        'Tilted Hangingwall' => 0.7,
        'Tilted Fault Block Footwall' => 0.7,
        'Tilted Fault Block Terrace' => 0.7,
        'Tilted Fault Block Unqualified' => 0.7,
        'Horst Simple' => 0.6,
        'Horst Faulted' => 0.6,
        'Horst Complex' => 0.6,
        'Horst Unqualified' => 0.6,
        'Wedge Tilted' => 0.5,
        'Wedge Flexured' => 0.5,
        'Wedge Radial' => 0.5,
        'Wedge Marginal' => 0.5,
        'Wedge Faulted' => 0.5,
        'Wedge Onlap' => 0.5,
        'Wedge Subcrop' => 0.5,
        'Wedge Unqualified' => 0.5,
        'Abutment Hangingwall' => 0.4,
        'Abutment Footwall' => 0.4,
        'Abutment Pericline' => 0.4,
        'Abutment Terrace' => 0.4,
        'Abutment Complex' => 0.4,
        'Abutment Unqualified' => 0.4,
        'Irregular Hangingwall' => 0.4,
        'Irregular Footwall' => 0.4,
        'Irregular Pericline' => 0.4,
        'Irregular Dome' => 0.4,
        'Irregular Terrace' => 0.4,
        'Irregular Unqualified' => 0.4,
        'Unknown' => 0.5
      ),
      'pgcftrap8' => array(
        'Structural Tectonic Extensional' => 0.6,
        'Structural Tectonic Compressional Thin Skinned' => 0.9,
        'Structural Tectonic Compressional Inverted' => 1.0,
        'Structural Tectonic Strike Slip' => 1.0,
        'Structural Tectonic Unqualified' => 0.8,
        'Structural Drape' => 0.8,
        'Structural Compactional' => 0.8,
        'Structural Diapiric Salt' => 0.7,
        'Structural Diapiric Mud' => 0.7,
        'Structural Diapiric Unqualified' => 0.7,
        'Structural Gravitational' => 0.6,
        'Structural Unqualified' => 0.6,
        'Stratigraphic Depositional Pinch-out' => 0.6,
        'Stratigraphic Depositional Shale-out' => 0.6,
        'Stratigraphic Depositional Facies-limited' => 0.6,
        'Stratigraphic Depositional Reef' => 1.0,
        'Stratigraphic Depositional Unqualified' => 0.6,
        'Stratigraphic Unconformity Subcrop' => 0.6,
        'Stratigraphic Unconformity Onlap' => 0.6,
        'Stratigraphic Unconformity Unqualified' => 0.6,
        'Stratigraphic Other Diagenetic' => 0.6,
        'Stratigraphic Other Tar Mat' => 0.6,
        'Stratigraphic Other Gas Hydrate' => 0.6,
        'Stratigraphic Other Gas Permafrosh' => 0.6,
        'Stratigraphic Unqualified' => 0.6,
        'Unknown' => 0.5
      ),
      'pgcfdyn2' => array(
        'Very Near (0 - 2 Km)' => 1.0,
        'Near (2 - 5 Km)' => 0.9,
        'Middle (5 - 10 Km)' => 0.8,
        'Long (10 - 20 Km)' => 0.7,
        'Very Long (> 20 Km)' => 0.5,
        'Unknown' => 0.5
      ),
      'pgcfdyn3' => array(
        'Single Order' => 0.7,
        'Multiple Order' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcfdyn6' => array(
        'Not Occur' => 0.2,
        'Occur' => 1.0,
        'Unknown' => 0.5
      ),
      'pgcfdyn7' => array(
        'Vertical' => 0.7,
        'Horizontal' => 0.8,
        'Multiple Directions' => 1.0,
        'Unknown' => 0.5
      ),
    );

    return $gcfPlay[$x][$y];
  }

  public function besidePlay($x, $y) {
    $gcfPlay = array (
      'gcfsrock3' => array(
        'I' => 1.0,
        'I/II' => 0.9,
        'II' => 0.8,
        'II/III' => 0.7,
        'III' => 0.6,
        'IV' => 0.1,
        'Unknown' => 0.5
      ),
      'gcfsrock4' => array(
        '0 - 0.5' => 0.2,
        '0.6 - 1.0' => 0.6, 
        '1.1 - 2.0' => 0.7,
        '2.1 - 4.0' => 0.8,
        '> 4' => 1.0,
        'Unknown' => 0.5
      ),
      'gcfsrock5' => array(
        '<= 1.0' => 0.5, 
        '1.1 - 2.0' => 0.7,
        '2.1 - 3.0' => 0.9,
        '> 3.0' => 1.0,
        'Unknown' => 0.5
      ),
      'gcfsrock8' => array(
        'Immature' => 0.1,
        'Early Mature' => 0.4,
        'Mature' => 1.0,
        'Over Mature' => 0.8,  
        'Unknown' => 0.5
      ),
      'gcfsrock9' => array(
        'Yes' => 1.0,
        'No' => 0.0,
        'Unknown' => 0.5
      ),
      'gcfres5' => array(
        'Single Distribution' => 1.0, 
        'Double Distribution' => 0.8,
        'Multiple Distribution' => 0.7,
        'Unknown' => 0.5
      ),
      'gcfres7' => array(
        'Siltstone' => 0.5,
        'Sandstone' => 1.0,
        'Reef Carbonate' => 1.0,
        'Platform Carbonate' => 0.5,
        'Coal' => 0.6,
        'Alternate Rocks' => 0.5,
        'Naturally Fractured' => 0.7,
        'Others' => 0.3,
        'Unknown' => 0.5
      ),
      'gcfres10' => array(
        'Vugs Porosity' => 0.4,
        'Fracture Porosity' => 0.9,
        'Integrated Porosity' => 1.0,
        'None' => 0.0,
        'Unknown' => 0.5
      ),
      'gcfres9' => array(
        '0 - 10' => 0.4,
        '11 - 15' => 0.6,
        '16 - 20' => 0.7,
        '21 - 30' => 0.9,
        '> 30' => 1.0,
        'Unknown' => 0.5
      ),
      'gcftrap3' => array(
        'Single Distribution Impermeable Rocks' => 0.6,
        'Double Distribution Impermeable Rocks' => 0.8,
        'Multiple Distribution Impermeable Rocks' => 1.0,
        'Unknown' => 0.5
      ),
      'gcftrap4' => array(
        'Tank' => 0.4,
        'Truncated' => 0.5,
        'Limited Spread' => 0.7,
        'Spread' => 0.9,
        'Extensive Spread' => 1.0,
        'Unknown' => 0.5
      ),
      'gcftrap5' => array(
        'Primary' => 0.8,
        'Diagenetic' => 0.9,
        'Alternate Tectonic Mech' => 1.0,
        'Unknown' => 0.5
      ),
      'gcftrap7' => array(
        'Anticline Hangingwall' => 0.9,
        'Anticline Footwall' => 1.0,
        'Anticline Pericline' => 0.9,
        'Anticline Dome' => 0.9,
        'Anticline Nose' => 0.9,
        'Anticline Unqualified' => 0.9,
        'Faulted Anticline Hangingwall' => 0.8,
        'Faulted Anticline Footwall' => 0.8,
        'Faulted Anticline Pericline' => 0.8,
        'Faulted Anticline Dome' => 0.8,
        'Faulted Anticline Nose' => 0.8,
        'Faulted Anticline Unqualified' => 0.8,
        'Tilted Hangingwall' => 0.7,
        'Tilted Fault Block Footwall' => 0.7,
        'Tilted Fault Block Terrace' => 0.7,
        'Tilted Fault Block Unqualified' => 0.7,
        'Horst Simple' => 0.6,
        'Horst Faulted' => 0.6,
        'Horst Complex' => 0.6,
        'Horst Unqualified' => 0.6,
        'Wedge Tilted' => 0.5,
        'Wedge Flexured' => 0.5,
        'Wedge Radial' => 0.5,
        'Wedge Marginal' => 0.5,
        'Wedge Faulted' => 0.5,
        'Wedge Onlap' => 0.5,
        'Wedge Subcrop' => 0.5,
        'Wedge Unqualified' => 0.5,
        'Abutment Hangingwall' => 0.4,
        'Abutment Footwall' => 0.4,
        'Abutment Pericline' => 0.4,
        'Abutment Terrace' => 0.4,
        'Abutment Complex' => 0.4,
        'Abutment Unqualified' => 0.4,
        'Irregular Hangingwall' => 0.4,
        'Irregular Footwall' => 0.4,
        'Irregular Pericline' => 0.4,
        'Irregular Dome' => 0.4,
        'Irregular Terrace' => 0.4,
        'Irregular Unqualified' => 0.4,
        'Unknown' => 0.5
      ),
      'gcftrap8' => array(
        'Structural Tectonic Extensional' => 0.6,
        'Structural Tectonic Compressional Thin Skinned' => 0.9,
        'Structural Tectonic Compressional Inverted' => 1.0,
        'Structural Tectonic Strike Slip' => 1.0,
        'Structural Tectonic Unqualified' => 0.8,
        'Structural Drape' => 0.8,
        'Structural Compactional' => 0.8,
        'Structural Diapiric Salt' => 0.7,
        'Structural Diapiric Mud' => 0.7,
        'Structural Diapiric Unqualified' => 0.7,
        'Structural Gravitational' => 0.6,
        'Structural Unqualified' => 0.6,
        'Stratigraphic Depositional Pinch-out' => 0.6,
        'Stratigraphic Depositional Shale-out' => 0.6,
        'Stratigraphic Depositional Facies-limited' => 0.6,
        'Stratigraphic Depositional Reef' => 1.0,
        'Stratigraphic Depositional Unqualified' => 0.6,
        'Stratigraphic Unconformity Subcrop' => 0.6,
        'Stratigraphic Unconformity Onlap' => 0.6,
        'Stratigraphic Unconformity Unqualified' => 0.6,
        'Stratigraphic Other Diagenetic' => 0.6,
        'Stratigraphic Other Tar Mat' => 0.6,
        'Stratigraphic Other Gas Hydrate' => 0.6,
        'Stratigraphic Other Gas Permafrosh' => 0.6,
        'Stratigraphic Unqualified' => 0.6,
        'Unknown' => 0.5
      ),
      'gcfdyn2' => array(
        'Very Near (0 - 2 Km)' => 1.0,
        'Near (2 - 5 Km)' => 0.9,
        'Middle (5 - 10 Km)' => 0.8,
        'Long (10 - 20 Km)' => 0.7,
        'Very Long (> 20 Km)' => 0.5,
        'Unknown' => 0.5
      ),
      'gcfdyn3' => array(
        'Single Order' => 0.7,
        'Multiple Order' => 1.0,
        'Unknown' => 0.5
      ),
      'gcfdyn6' => array(
        'Not Occur' => 0.2,
        'Occur' => 1.0,
        'Unknown' => 0.5
      ),
      'gcfdyn7' => array(
        'Vertical' => 0.7,
        'Horizontal' => 0.8,
        'Multiple Directions' => 1.0,
        'Unknown' => 0.5
      ),
    );

    return $gcfPlay[$x][$y];
  }

  public function getNilai($name, $choosen, $from, $category) {
    
    if($from == 'play') {
      $konstanta = substr($this->getState($name), 1);

      if($category == 'Proven') {
        return $this->play($name, $choosen) * $this->$konstanta;
      } else {
        return ((0.4 * $this->play($name, $choosen) + 0.3) * $this->$konstanta);
      }
    } else {
      $konstanta2 = substr($this->getState2($name), 1);

      if($category == 'Proven') {
        return $this->besidePlay($name, $choosen) * $this->$konstanta2;
      } else {
        return ((0.4 * $this->besidePlay($name, $choosen) + 0.3) * $this->$konstanta2);
      }
    }
    // return number_format($this->array[$name][$choosen], 1);
  }

  public function getState($name) {
    $state = array(
      'pgcfsrock3'=>'#sr_ker',
      'pgcfsrock4'=>'#sr_toc',
      'pgcfsrock5'=>'#sr_hfu',
      'pgcfsrock8'=>'#sr_mat',
      'pgcfsrock9'=>'#sr_otr',
      'pgcfres5'=>'#re_dis',
      'pgcfres7'=>'#re_lit',
      'pgcfres9'=>'#re_pri',
      'pgcfres10'=>'#re_sec',
      'pgcftrap3'=>'#tr_sdi',
      'pgcftrap4'=>'#tr_scn',
      'pgcftrap5'=>'#tr_stp',
      'pgcftrap7'=>'#tr_geo',
      'pgcftrap8'=>'#tr_trp',
      'pgcfdyn2'=>'#dn_kit',
      'pgcfdyn3'=>'#dn_tec',
      'pgcfdyn6'=>'#dn_prv',
      'pgcfdyn7'=>'#dn_mig',
      
    );
    return $state[$name];
  }

  public function getState2($name) {
    $state = array(
      'gcfsrock3'=>'#sr_ker',
      'gcfsrock4'=>'#sr_toc',
      'gcfsrock5'=>'#sr_hfu',
      'gcfsrock8'=>'#sr_mat',
      'gcfsrock9'=>'#sr_otr',
      'gcfres5'=>'#re_dis',
      'gcfres7'=>'#re_lit',
      'gcfres9'=>'#re_pri',
      'gcfres10'=>'#re_sec',
      'gcftrap3'=>'#tr_sdi',
      'gcftrap4'=>'#tr_scn',
      'gcftrap5'=>'#tr_stp',
      'gcftrap7'=>'#tr_geo',
      'gcftrap8'=>'#tr_trp',
      'gcfdyn2'=>'#dn_kit',
      'gcfdyn3'=>'#dn_tec',
      'gcfdyn6'=>'#dn_prv',
      'gcfdyn7'=>'#dn_mig',
      
    );
    return $state[$name];
  }
}

?>