<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	
	private $_level;
	
	public function authenticate()
	{
// 		$users=array(
// 			// username => password
// 			'eni'=>'eni',
// 			'administrator'=>'admin123',
// 		);
// 		if(!isset($users[$this->username]))
// 			$this->errorCode=self::ERROR_USERNAME_INVALID;
// 		elseif($users[$this->username]!==$this->password)
// 			$this->errorCode=self::ERROR_PASSWORD_INVALID;
// 		else
// 			$this->errorCode=self::ERROR_NONE;
// 		return !$this->errorCode;

		$username = strtolower($this->username);
		$user = User::model()->find('LOWER(user_name)=?', array($username));
		if($user === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if(!CPasswordHelper::verifyPassword($this->password, $user->user_pass))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->logInUser($user);
		}
		
		return $this->errorCode == self::ERROR_NONE;
	}

	protected function logInUser($user, $switch = null) {
		if($user) {
			$this->_level = $user->user_level;
			$this->setState('level', $user->user_level);
			$this->setState('wk_id', $user->wk_id);
			$this->setState('switch', $switch);
			$this->username = $user->user_name;
			$this->errorCode = self::ERROR_NONE;
		}
	}

	public static function impersonate($user) {
		$ui = null;
		$userImpersonate = User::model()->findByAttributes(array('user_name'=>$user));


		if($userImpersonate) {
			$ui = new UserIdentity($userImpersonate->user_name, '');
			$ui->logInUser($userImpersonate, "admin");
		}
		return $ui;
	}
}