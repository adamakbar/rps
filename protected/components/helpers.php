<?php

/**
 * Merubah HTML menjadi PDF menggunakan WKHTMLTOPDF.
 *
 * @param  string $html Format HTML yang valid untuk dijadikan PDF
 * @param  string $fileName Nama file PDF
 * @param  string $outDir Direktori output untuk PDF
 * @return string
 */
function to_pdf($html, $fileName, $outDir=null, $autoDl=true)
{
    if ($outDir === null) {
        $outDir = Yii::app()->basePath . '/../blob/generated/';
    }

    $fileName = str_replace(' ', '_', $fileName);
    $bin = Yii::app()->basePath . '/vendor/wkhtmltopdf/wkhtmltopdf.exe';
    $fileHtml = $outDir . $fileName . '.html';
    if (file_exists($fileHtml)) {
        unlink($fileHtml);
    }

    $filePdf = $outDir . $fileName . '.pdf';
    if (file_exists($filePdf)) {
        unlink($filePdf);
    }

    file_put_contents($fileHtml, $html);
    exec($bin . ' ' . $fileHtml . ' ' . $filePdf . ' 2>&1', $output);

    unlink($fileHtml);

    if ($autoDl) {
        if (file_exists($filePdf)) {
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment; filename=' . basename($filePdf));
            header('Content-Length: ' . filesize($filePdf));
            ob_clean();
            flush();
            readfile($filePdf);
        }

        unlink($filePdf);
        exit();
    }

    return null;
}

/**
 * Membuat link ke aset yang dituju.
 *
 * @param  string $pathname Nama serta path dari file assets publik
 * @param  boolean $withHost Menambahkan nama hostname di awal link
 * @return string
 */
function assets($pathname, $withHost=false)
{
    if (substr($pathname, 0) === '/') {
        $pathname = substr($pathname, 1);
    }

    return Yii::app()->getBaseUrl($withHost) . '/' . $pathname;
}

/**
 * Buat link URL route dengan nama controller dan action.
 *
 * @param string @routeName Nama Controller dengan action yang dituju
 * @return string
 */
function route($routeName, $attribute=[])
{
    return Yii::app()->createUrl($routeName, $attribute);
}

function play_name_creator($lithology, $formationSerie, $formation, $ageSerie, $age, $environment, $trapType)
{
    $playName = "{$lithology} - {$formationSerie} {$formation} - {$ageSerie} {$age} - {$environment} - {$trapType}";

    $playName = str_replace(['Unknown', 'Not Available', 'Others'],
        '', $playName);
    $playName = str_replace('- ', '-', $playName);
    $playName = str_replace(' -', '-', $playName);
    $playName = rtrim($playName, '--');
    $playName = trim($playName, '-');
    $playName = str_replace('--', '-', $playName);

    return $playName;
}

function oneField($label, $value, $unit=null)
{
    if ($value == null) {
        $unit = null;
        return '';
    }

    if ($unit != null) {
        $unit = ' ' . $unit;
    }

    return <<<HTML
<div class="pure-g p-form">
  <div class="pure-u-7-24">{$label}</div>
  <div class="pure-u-3-5">{$value}{$unit}</div>
</div>
HTML;
}

function getWkName($wkid)
{
    return Yii::app()->db->createCommand()
        ->select(['wk_name'])
        ->from('adm_working_area')
        ->where('wk_id = :wkid', [':wkid' => $wkid])
        ->queryScalar();
}