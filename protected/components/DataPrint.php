<?php

class DataPrint
{

    /**
     * Code Log: General SQL for Prospect Survey
     */
    public function __construct()
    {
        $this->_survey_sql = <<<EOD
            IFNULL((SELECT s2d_vol_p90_area
                    FROM rsc_seismic_2d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s2d_area_p90,
            IFNULL((SELECT s3d_vol_p90_area
                    FROM rsc_seismic_3d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s3d_area_p90,
            IFNULL((SELECT sgf_p90_area
                    FROM rsc_geological
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgf_area_p90,
            IFNULL((SELECT sgc_vol_p90_area
                    FROM rsc_geochemistry
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgc_area_p90,
            IFNULL((SELECT sgv_vol_p90_area
                    FROM rsc_gravity
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgv_area_p90,
            IFNULL((SELECT sel_vol_p90_area
                    FROM rsc_electromagnetic
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sel_area_p90,
            IFNULL((SELECT rst_vol_p90_area
                    FROM rsc_resistivity
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) rst_area_p90,
            IFNULL((SELECT sor_vol_p90_area
                    FROM rsc_other
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sor_area_p90,

            IFNULL((SELECT s2d_vol_p50_area
                    FROM rsc_seismic_2d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s2d_area_p50,
            IFNULL((SELECT s3d_vol_p50_area
                    FROM rsc_seismic_3d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s3d_area_p50,
            IFNULL((SELECT sgf_p50_area
                    FROM rsc_geological
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgf_area_p50,
            IFNULL((SELECT sgc_vol_p50_area
                    FROM rsc_geochemistry
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgc_area_p50,
            IFNULL((SELECT sgv_vol_p50_area
                    FROM rsc_gravity
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgv_area_p50,
            IFNULL((SELECT sel_vol_p50_area
                    FROM rsc_electromagnetic
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sel_area_p50,
            IFNULL((SELECT rst_vol_p50_area
                    FROM rsc_resistivity
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) rst_area_p50,
            IFNULL((SELECT sor_vol_p50_area
                    FROM rsc_other
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sor_area_p50,

            IFNULL((SELECT s2d_vol_p10_area
                    FROM rsc_seismic_2d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s2d_area_p10,
            IFNULL((SELECT s3d_vol_p10_area
                    FROM rsc_seismic_3d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s3d_area_p10,
            IFNULL((SELECT sgf_p10_area
                    FROM rsc_geological
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgf_area_p10,
            IFNULL((SELECT sgc_vol_p10_area
                    FROM rsc_geochemistry
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgc_area_p10,
            IFNULL((SELECT sgv_vol_p10_area
                    FROM rsc_gravity
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sgv_area_p10,
            IFNULL((SELECT sel_vol_p10_area
                    FROM rsc_electromagnetic
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sel_area_p10,
            IFNULL((SELECT rst_vol_p10_area
                    FROM rsc_resistivity
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) rst_area_p10,
            IFNULL((SELECT sor_vol_p10_area
                    FROM rsc_other
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) sor_area_p10,

            IFNULL((SELECT s2d_net_p90_thickness
                    FROM rsc_seismic_2d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s2d_net_p90,
            IFNULL((SELECT s3d_net_p90_thickness
                    FROM rsc_seismic_3d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s3d_net_p90,
            IFNULL((SELECT s2d_net_p50_thickness
                    FROM rsc_seismic_2d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s2d_net_p50,
            IFNULL((SELECT s3d_net_p50_thickness
                    FROM rsc_seismic_3d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s3d_net_p50,
            IFNULL((SELECT s2d_net_p10_thickness
                    FROM rsc_seismic_2d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s2d_net_p10,
            IFNULL((SELECT s3d_net_p10_thickness
                    FROM rsc_seismic_3d
                    WHERE prospect_id=pr.prospect_id LIMIT 1), 0) s3d_net_p10
EOD;
    }

    public function init()
    {
        Yii::import('DataQuery', true);
    }

    public function average($arr)
    {
        if (empty($arr)) {
            return 0;
        } else {
            return array_sum($arr) / count($arr);
        }
    }

    public function smin($arr)
    {
        $adif = array_diff($arr, array('0'));
        if (empty($arr) || empty($adif)) {
            return 0;
        } else {
            return min($adif);
        }
    }

    public function smax($arr)
    {
        $adif = array_diff($arr, array('0'));
        if (empty($arr) || empty($adif)) {
            return 0;
        } else {
            return max($adif);
        }
    }

    public function rationalize($arr)
    {
        $arr = array_filter($arr, function ($x) {
            return $x != 0;
        });

        if (empty($arr)) {
            $arr = array();
        }

        return $arr;
    }

    public function echo_delim($arr, $delim = ':')
    {
        $o = '';
        foreach ($arr as $key => $val) {
            $o = $o . $delim . $val;
        }
        $o = $o . '<br/>';
        return $o;
    }

    public function is_array_all_null($arr)
    {
        $arr = array_filter($arr, function ($x) {
            return $x != null;
        });

        if (empty($arr)) {
            return true;
        } else {
            return false;
        }
    }

    public function qGet($sql)
    {
        $q = Yii::app()->db->createCommand($sql);
        $q = $q->queryAll();
        return $q;
    }

    public function to_float($f, $p = 2)
    {
        if ($f < 1) {
            $f = sprintf('%.4F', $f);
        } elseif ($f == '0') {
            $f = 0;
        } else {
            $f = sprintf('%.2F', $f);
        }

        return floatval($f);
    }

    public function notAvailableDestroyer($str)
    {
        $str = str_replace('Not Available', ' ', $str);
        $str = str_replace('Unknown', ' ', $str);
        $str = str_replace('Others', ' ', $str);
        $str = preg_replace('!\s+!', ' ', $str);
        $str = str_replace('--', '-', $str);
        $str = str_replace('- -', '-', $str);
        $str = str_replace('- ', '-', $str);
        $str = str_replace(' -', '', $str);
        return $str;
    }

    public function oil_or_gas($result)
    {
        $result = explode('+', $result);

        foreach ($result as $key => $val) {
            if ($val == 'Condensate') {
                unset($result[$key]);
            }
        }

        return $result;
    }

    public function wtohc_sat($sat)
    {
        /*
        // Convert Water Saturation to HC Saturation
         */
        if ($sat == 0) {
            $sat = 100;
        }

        return 100 - $sat;
    }

    public function sw_to_hc($sw, $is_percent = true)
    {
        /*
        // Convert Water Saturation to Hydrocarbon Saturation (1-Sw)
        // Sw can be decimal or percent.
         */
        if ($is_percent == true) {
            $sw = $sw / 100;
        }

        if ($sw == 0 || $sw == '') {
            $hc_sat = 0;
        } else {
            $hc_sat = 1 - $sw;
        }

        return $hc_sat;
    }

    public function arrayToCsvDataPrint($filename, $arr, $header = '', $datename = true)
    {
        if ($header != '') {
            array_unshift($arr, $header);
        }

        if ($datename) {
            $today = getdate();
            $today = "_({$today['mday']}-{$today['month']}-{$today['year']})";
            $csp = $filename . $today . '.csv';
        }

        $fp = fopen($csp, 'w');
        foreach ($arr as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($csp));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($csp));
        readfile($csp);
        unlink($csp);
    }

    public function ifAllZero($arr)
    {
        $s = false;

        foreach ($arr as $v) {
            if ($v != 0) {
                $s = true;
            }
        }

        return $s;
    }

    public function ifAllToZero($arr)
    {
        if (!$this->ifAllZero($arr)) {
            $arr = array(0);
        }

        return $arr;
    }

    public function check_survey($arr)
    {
        $survey = array();
        foreach ($arr as $key => $val) {
            if ($val != '') {
                $survey[$key] = 1;
            }
        }

        return $survey;
    }

    public function is_play_diff($play_one, $play_two)
    {
        /*
        // Check if two Play actually different or not
        //
        // Args :
        //   $play_n = array('play_litho', 'play_form', 'play_age', 'play_trap')
         */

        if ($play_one['play_litho'] == $play_two['play_litho'] &&
            $play_one['play_form'] == $play_two['play_form'] &&
            $play_one['play_age'] == $play_two['play_age'] &&
            $play_one['play_trap'] == $play_two['play_trap']) {

            return true;
        } else {
            return false;
        }
    }

    public function coord_break($coord)
    {
        /*
        // Break string $coord with format "D,M,S,W"
        // $coord can be latitude or longitude
         */
        $new = array();
        $coord = explode(',', $coord);
        $new['degree'] = $coord[0];
        $new['minute'] = $coord[1];
        $new['second'] = $coord[2];
        $new['wind'] = strtoupper($coord[3]);
        $new['unite'] = $new['degree'] . ' ' . $new['minute'] . ' ' . $new['second'] . ' ' . $new['wind'];

        return $new;
    }

    public function survey_correcting_lead($low, $best, $high)
    {
        $ret = array(
            'low' => array(),
            'best' => array(),
            'high' => array(),
            'info' => array(
                'is' => '',
                'available' => array(),
                'not_available' => array(),
            ),
        );

        foreach ($low as $key => $val) {
            if (empty($val) && empty($best[$key]) && empty($high[$key])) {
                $ret['info']['not_available'][] = $key;
            } else {
                $ret['info']['available'][] = $key;
                $ret['low'][$key] = $val;
                $ret['best'][$key] = $best[$key];
                $ret['high'][$key] = $high[$key];
            }
        }

        if (empty($info['available'])) {
            $ret['info']['is'] = false;
        } else {
            $ret['info']['is'] = true;
        }

        return $ret;
    }

    public function survey_correcting_prospect($area, $netpay, $stage)
    {
        /*
        // Check if any survey available for Lead and
        // Prospect, also correcting if P90 or other 0 to 0.0001
        //
        // Args :
        //   $data = 7 or 8 survey depending on closure type,
        //           either P90, P50, or P10
         */
        if ($stage == 'drillable' || $stage == 'postdrill' || $stage == 'discovery') {
            $ret = array(
                'area' => array(
                    'p90' => array(),
                    'p50' => array(),
                    'p10' => array(),
                ),
                'netpay' => array(
                    'p90' => array(),
                    'p50' => array(),
                    'p10' => array(),
                ),
                'info' => array(
                    'is' => '',
                    'available' => array(),
                    'not_available' => array(),
                ),
            );

            // If one of the survey KKKS input have P90, P50, P10 -> 0, 1, 2
            // this mean that survey not count.

            foreach ($area['p10'] as $key => $val) {
                if ($key == 's2' || $key == 's3') {
                    if (empty($val) && empty($netpay['p10'][$key])) {
                        $ret['info']['not_available'] = $key;
                    } elseif (!empty($val) && !empty($netpay['p10'][$key])) {
                        $ret['info']['available'] = $key;
                        if ($val + $area['p50'][$key] + $area['p90'][$key] <= 2) {
                            $ret['area']['p90'][$key] = 0;
                            $ret['area']['p50'][$key] = 0;
                            $ret['area']['p10'][$key] = 0;
                            $ret['netpay']['p90'][$key] = $netpay['p90'][$key];
                            $ret['netpay']['p50'][$key] = $netpay['p50'][$key];
                            $ret['netpay']['p10'][$key] = $netpay['p10'][$key];
                        } elseif ($netpay['p10'][$key] + $netpay['p50'][$key] + $netpay['p90'][$key] <= 2) {
                            $ret['area']['p90'][$key] = $area['p90'][$key];
                            $ret['area']['p50'][$key] = $area['p50'][$key];
                            $ret['area']['p10'][$key] = $area['p10'][$key];
                            $ret['netpay']['p90'][$key] = 0;
                            $ret['netpay']['p50'][$key] = 0;
                            $ret['netpay']['p10'][$key] = 0;
                        } elseif ($area['p90'][$key] == 0) {
                            $ret['area']['p90'][$key] = $area['p50'][$key] - 1;
                            $ret['area']['p50'][$key] = $area['p50'][$key];
                            $ret['area']['p10'][$key] = $area['p10'][$key];
                            $ret['netpay']['p90'][$key] = $netpay['p90'][$key];
                            $ret['netpay']['p50'][$key] = $netpay['p50'][$key];
                            $ret['netpay']['p10'][$key] = $netpay['p10'][$key];
                        } elseif ($netpay['p90'][$key] == 0) {
                            $ret['area']['p90'][$key] = $area['p90'][$key];
                            $ret['area']['p50'][$key] = $area['p50'][$key];
                            $ret['area']['p10'][$key] = $area['p10'][$key];
                            $ret['netpay']['p90'][$key] = $netpay['p50'][$key] - 1;
                            $ret['netpay']['p50'][$key] = $netpay['p50'][$key];
                            $ret['netpay']['p10'][$key] = $netpay['p10'][$key];
                        } else {
                            $ret['area']['p90'][$key] = $area['p90'][$key];
                            $ret['area']['p50'][$key] = $area['p50'][$key];
                            $ret['area']['p10'][$key] = $area['p10'][$key];
                            $ret['netpay']['p90'][$key] = $netpay['p90'][$key];
                            $ret['netpay']['p50'][$key] = $netpay['p50'][$key];
                            $ret['netpay']['p10'][$key] = $netpay['p10'][$key];
                        }
                    }
                } else {
                    if (empty($val)) {
                        $ret['info']['not_available'] = $key;
                    } elseif (!empty($val)) {
                        $ret['info']['available'] = $key;
                        if ($val + $area['p50'][$key] + $area['p90'][$key] <= 2) {
                            $ret['area']['p90'][$key] = 0;
                            $ret['area']['p50'][$key] = 0;
                            $ret['area']['p10'][$key] = 0;
                        } elseif ($area['p90'][$key] == 0) {
                            $ret['area']['p90'][$key] = $area['p50'][$key] - 1;
                            $ret['area']['p50'][$key] = $area['p50'][$key];
                            $ret['area']['p10'][$key] = $area['p10'][$key];
                        } else {
                            $ret['area']['p90'][$key] = $area['p90'][$key];
                            $ret['area']['p50'][$key] = $area['p50'][$key];
                            $ret['area']['p10'][$key] = $area['p10'][$key];
                        }
                    }
                }
            }

            if (empty($info['available'])) {
                $ret['info']['is'] = false;
            } else {
                $ret['info']['is'] = true;
            }
        }

        return $ret;
    }

    public function multiexplode($delimiters, $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);

        return $launch;
    }

    public function clear_name($string)
    {
        $string = str_replace(array(';', '-', '_', '"', '(', ')', '/'), ' ', strtoupper($string));
        $string = preg_replace('/\s+/', ' ', $string);
        $string = preg_replace('/ BT +/', ' BT', $string);
        $string = trim($string);

        return $string;
    }

    public function play_name_maker($data)
    {
        $info = array('is' => '', 'no_data' => array());

        foreach ($data as $key => $val) {
            if ($val == '') {
                $info['no_data'][] = $key;
            }
        }

        if (empty($info['no_data'])) {
            $info['is'] = true;
        } else {
            $info['is'] = false;
        }

        // Making of Play Name
        $play_name = $data['play_litho'] . '-' .
            $data['play_form_serie'] . ' ' . $data['play_form'] . '-' .
            $data['play_age_serie'] . ' ' . $data['play_age'] . '-' .
            $data['play_env'] . '-' .
            $data['play_trap'];

        $play_name = str_replace(array('Unknown', 'Not Available', 'Others'), '', $play_name);
        $play_name = str_replace('- ', '-', $play_name);
        $play_name = str_replace(' -', '-', $play_name);
        $play_name = rtrim($play_name, '--');
        $play_name = trim($play_name, '-');
        $play_name = str_replace('--', '-', $play_name);

        $play = array('play' => $play_name, 'info' => $info);

        return $play;
    }

    public function all_get_essential($data, $stage)
    {
        $es = array();

        // does we need 'info' child ?

        foreach ($data as $key => $val) {
            $es[$key] = array();

            $play_name = $this->play_name_maker(array(
                'play_litho' => $val['play_litho'],
                'play_form_serie' => $val['play_form_serie'],
                'play_form' => $val['play_form'],
                'play_age_serie' => $val['play_age_serie'],
                'play_age' => $val['play_age'],
                'play_env' => $val['play_env'],
                'play_trap' => $val['play_trap'],
            ))['play'];

            $gcf = $this->gcfCalc(array(
                'sr' => $val['sr'],
                're' => $val['re'],
                'tr' => $val['tr'],
                'dn' => $val['dn'],
                'sr_ker' => $val['sr_ker'],
                'sr_toc' => $val['sr_toc'],
                'sr_hfu' => $val['sr_hfu'],
                'sr_mat' => $val['sr_mat'],
                'sr_otr' => $val['sr_otr'],
                're_dis' => $val['re_dis'],
                're_lit' => $val['re_lit'],
                're_sec' => $val['re_sec'],
                're_pri' => $val['re_pri'],
                'tr_sdi' => $val['tr_sdi'],
                'tr_scn' => $val['tr_scn'],
                'tr_stp' => $val['tr_stp'],
                'tr_geo' => $val['tr_geo'],
                'tr_trp' => $val['tr_trp'],
                'dn_kit' => $val['dn_kit'],
                'dn_tec' => $val['dn_tec'],
                'dn_prv' => $val['dn_prv'],
                'dn_mig' => $val['dn_mig'],
            ), $stage);

            $ld_low = array(
                's2' => $val['s2_low'],
                'gl' => $val['gl_low'],
                'gh' => $val['gh_low'],
                'gr' => $val['gr_low'],
                'el' => $val['el_low'],
                'rs' => $val['rs_low'],
                'ot' => $val['ot_low'],
            );

            $ld_best = array(
                's2' => $val['s2_best'],
                'gl' => $val['gl_best'],
                'gh' => $val['gh_best'],
                'gr' => $val['gr_best'],
                'el' => $val['el_best'],
                'rs' => $val['rs_best'],
                'ot' => $val['ot_best'],
            );

            $ld_high = array(
                's2' => $val['s2_high'],
                'gl' => $val['gl_high'],
                'gh' => $val['gh_high'],
                'gr' => $val['gr_high'],
                'el' => $val['el_high'],
                'rs' => $val['rs_high'],
                'ot' => $val['ot_high'],
            );

            $es[$key]['basin'] = $val['basin'];
            $es[$key]['province'] = $val['province'];
            $es[$key]['kkks'] = $val['kkks'];
            $es[$key]['wk_id'] = $val['wk_id'];
            $es[$key]['wk'] = $val['wk'];
            $es[$key]['closure'] = $this->clear_name($val['closure']);
            $es[$key]['resource_stage'] = $val['resource_stage'];
            $es[$key]['play_name'] = $play_name;
            $es[$key]['clarified'] = $val['clarified'];
            $es[$key]['latitude'] = $this->coord_break($val['latitude'])['unite'];
            $es[$key]['longitude'] = $this->coord_break($val['longitude'])['unite'];
            $es[$key]['shore'] = $val['shore'];
            $es[$key]['terrain'] = $val['terrain'];
            $es[$key]['near_field'] = $val['near_field'];
            $es[$key]['near_infra_structure'] = $val['near_infra_structure'];

            $es[$key]['gcf'] = $gcf['rda'];
            $es[$key]['gcf_normal'] = $gcf['rda_final'];

            // LANJUT
            // DETAIL GCF

            if ($stage == 'lead') {
                $es[$key]['ld_s2_year'] = $val['ld_s2_year'];
                $es[$key]['ld_s2_crossline'] = $val['ld_s2_crossline'];
                $es[$key]['ld_s2_coverage'] = $val['ld_s2_coverage'];
                $es[$key]['ld_s2_interval'] = $val['ld_s2_interval'];
                $es[$key]['ld_s2_method'] = $val['ld_s2_method'];
                $es[$key]['ld_s2_img'] = $val['ld_s2_img'];
                $es[$key]['ld_s2_remark'] = $val['ld_s2_remark'];

                $es[$key]['ld_gl_year'] = $val['ld_gl_year'];
                $es[$key]['ld_gl_method'] = $val['ld_gl_method'];
                $es[$key]['ld_gl_area'] = $val['ld_gl_area'];
                $es[$key]['ld_gl_remark'] = $val['ld_gl_remark'];

                $es[$key]['ld_gh_year'] = $val['ld_gh_year'];
                $es[$key]['ld_gh_method'] = $val['ld_gh_method'];
                $es[$key]['ld_gh_remark'] = $val['ld_gh_remark'];

                $es[$key]['ld_gr_year'] = $val['ld_gr_year'];
                $es[$key]['ld_gr_method'] = $val['ld_gr_method'];
                $es[$key]['ld_gr_area'] = $val['ld_gr_area'];
                $es[$key]['ld_gr_remark'] = $val['ld_gr_remark'];

                $es[$key]['ld_el_year'] = $val['ld_el_year'];
                $es[$key]['ld_el_method'] = $val['ld_el_method'];
                $es[$key]['ld_el_area'] = $val['ld_el_area'];
                $es[$key]['ld_el_remark'] = $val['ld_el_remark'];

                $es[$key]['ld_rs_year'] = $val['ld_rs_year'];
                $es[$key]['ld_rs_method'] = $val['ld_rs_method'];
                $es[$key]['ld_rs_area'] = $val['ld_rs_area'];
                $es[$key]['ld_rs_remark'] = $val['ld_rs_remark'];

                $es[$key]['ld_ot_year'] = $val['ld_ot_year'];
            } elseif ($stage == 'prospect') {
            }

            if ($stage == 'well_discovery') {
                var_dump($val);die();
                $es[$key]['well'] = $val['well'];
                $es[$key]['well_formation'] = $val['well_formation'];
                $es[$key]['well_latitude'] = $val['well_latitude'];
                $es[$key]['well_longitude'] = $val['well_longitude'];
                $es[$key]['well_result'] = $val['well_result'];
                $es[$key]['well_ratio'] = $val['ratio'];
                $es[$key]['well_radius'] = $val['radius'];

                $es[$key]['porosity']['clastic'] = array(
                    'p90' => $val['wz_por_p90_clastic'],
                    'p50' => $val['wz_por_p50_clastic'],
                    'p10' => $val['wz_por_p10_clastic'],
                );

                $es[$key]['porosity']['carbo'] = array(
                    'p90' => $val['wz_por_p90_carbo'],
                    'p50' => $val['wz_por_p50_carbo'],
                    'p10' => $val['wz_por_p10_carbo'],
                );

                $es[$key]['saturation']['clastic'] = array(
                    'p90' => $val['wz_sat_p90_clastic'],
                    'p50' => $val['wz_sat_p50_clastic'],
                    'p10' => $val['wz_sat_p10_clastic'],
                );

                $es[$key]['saturation']['carbo'] = array(
                    'p90' => $val['wz_sat_p90_carbo'],
                    'p50' => $val['wz_sat_p50_carbo'],
                    'p10' => $val['wz_sat_p10_carbo'],
                );

                $es[$key]['boi'] = array(
                    'p90' => $val['boi_p90'],
                    'p50' => $val['boi_p50'],
                    'p10' => $val['boi_p10'],
                );

                $es[$key]['bgi'] = array(
                    'p90' => $val['bgi_p90'],
                    'p50' => $val['bgi_p50'],
                    'p10' => $val['bgi_p10'],
                );
            }

            if ($stage == 'discovery') {
                $es[$key]['gcf'] = 100;
            } elseif ($stage == 'play' ||
                $stage == 'lead' ||
                $stage == 'drillable' ||
                $stage == 'postdrill') {
                $es[$key]['gcf'] = array(
                    'sr' => $val['sr'],
                    're' => $val['re'],
                    'tr' => $val['tr'],
                    'dn' => $val['dn'],
                    'sr_ker' => $val['sr_ker'],
                    'sr_toc' => $val['sr_toc'],
                    'sr_hfu' => $val['sr_hfu'],
                    'sr_mat' => $val['sr_mat'],
                    'sr_otr' => $val['sr_otr'],
                    're_dis' => $val['re_dis'],
                    're_lit' => $val['re_lit'],
                    're_sec' => $val['re_sec'],
                    're_pri' => $val['re_pri'],
                    'tr_sdi' => $val['tr_sdi'],
                    'tr_scn' => $val['tr_scn'],
                    'tr_stp' => $val['tr_stp'],
                    'tr_geo' => $val['tr_geo'],
                    'tr_trp' => $val['tr_trp'],
                    'dn_kit' => $val['dn_kit'],
                    'dn_tec' => $val['dn_tec'],
                    'dn_prv' => $val['dn_prv'],
                    'dn_mig' => $val['dn_mig'],
                );
            }

            if ($stage == 'play') {
                $es[$key]['gcf'] = $this->gcfCalc($es[$key]['gcf'], $stage);
            }

            if ($stage == 'lead') {
                $es[$key]['low'] = array(
                    's2' => $val['s2_low'],
                    'gl' => $val['gl_low'],
                    'gh' => $val['gh_low'],
                    'gr' => $val['gr_low'],
                    'el' => $val['el_low'],
                    'rs' => $val['rs_low'],
                    'ot' => $val['ot_low'],
                );

                $es[$key]['best'] = array(
                    's2' => $val['s2_best'],
                    'gl' => $val['gl_best'],
                    'gh' => $val['gh_best'],
                    'gr' => $val['gr_best'],
                    'el' => $val['el_best'],
                    'rs' => $val['rs_best'],
                    'ot' => $val['ot_best'],
                );

                $es[$key]['high'] = array(
                    's2' => $val['s2_high'],
                    'gl' => $val['gl_high'],
                    'gh' => $val['gh_high'],
                    'gr' => $val['gr_high'],
                    'el' => $val['el_high'],
                    'rs' => $val['rs_high'],
                    'ot' => $val['ot_high'],
                );
            }

            if ($stage == 'drillable' || $stage == 'postdrill' ||
                $stage == 'discovery') {
                $es[$key]['area']['p90'] = array(
                    's2' => $val['s2_area_p90'],
                    's3' => $val['s3_area_p90'],
                    'gl' => $val['gl_area_p90'],
                    'gh' => $val['gh_area_p90'],
                    'gr' => $val['gr_area_p90'],
                    'el' => $val['el_area_p90'],
                    'rs' => $val['rs_area_p90'],
                    'ot' => $val['ot_area_p90'],
                );

                $es[$key]['area']['p50'] = array(
                    's2' => $val['s2_area_p50'],
                    's3' => $val['s3_area_p50'],
                    'gl' => $val['gl_area_p50'],
                    'gh' => $val['gh_area_p50'],
                    'gr' => $val['gr_area_p50'],
                    'el' => $val['el_area_p50'],
                    'rs' => $val['rs_area_p50'],
                    'ot' => $val['ot_area_p50'],
                );

                $es[$key]['area']['p10'] = array(
                    's2' => $val['s2_area_p10'],
                    's3' => $val['s3_area_p10'],
                    'gl' => $val['gl_area_p10'],
                    'gh' => $val['gh_area_p10'],
                    'gr' => $val['gr_area_p10'],
                    'el' => $val['el_area_p10'],
                    'rs' => $val['rs_area_p10'],
                    'ot' => $val['ot_area_p10'],
                );

                $es[$key]['netpay']['p90'] = array(
                    's2' => $val['s2_net_p90'],
                    's3' => $val['s3_net_p90'],
                );

                $es[$key]['netpay']['p50'] = array(
                    's2' => $val['s2_net_p50'],
                    's3' => $val['s3_net_p50'],
                );

                $es[$key]['netpay']['p10'] = array(
                    's2' => $val['s2_net_p10'],
                    's3' => $val['s3_net_p10'],
                );
            }

            if ($stage == 'drillable') {
                $es[$key]['porosity'] = array(
                    'p90' => $val['por_p90'],
                    'p50' => $val['por_p50'],
                    'p10' => $val['por_p10'],
                );

                $es[$key]['saturation'] = array(
                    'p90' => $val['sat_p90'],
                    'p50' => $val['sat_p50'],
                    'p10' => $val['sat_p10'],
                );
            }

            if ($stage == 'postdrill') {
                $es[$key]['well'] = $val['well'];
                $es[$key]['well_formation'] = $val['well_formation'];
                $es[$key]['well_latitude'] = $val['well_latitude'];
                $es[$key]['well_longitude'] = $val['well_longitude'];
                $es[$key]['well_result'] = $val['well_result'];
                $es[$key]['well_oil_show'] = $val['zone_oil_show'];
                $es[$key]['well_gas_show'] = $val['zone_gas_show'];

                if ($stage == 'discovery') {
                    $es[$key]['zone'] = $val['zone'];
                    $es[$key]['zone_thickness'] = $val['wz_thickness'];
                    $es[$key]['zone_radius'] = $val['wz_radius'];
                }

                $es[$key]['porosity']['clastic'] = array(
                    'p90' => $val['wz_por_p90_clastic'],
                    'p50' => $val['wz_por_p50_clastic'],
                    'p10' => $val['wz_por_p10_clastic'],
                );

                $es[$key]['porosity']['carbo'] = array(
                    'p90' => $val['wz_por_p90_carbo'],
                    'p50' => $val['wz_por_p50_carbo'],
                    'p10' => $val['wz_por_p10_carbo'],
                );

                $es[$key]['saturation']['clastic'] = array(
                    'p90' => $val['wz_sat_p90_clastic'],
                    'p50' => $val['wz_sat_p50_clastic'],
                    'p10' => $val['wz_sat_p10_clastic'],
                );

                $es[$key]['saturation']['carbo'] = array(
                    'p90' => $val['wz_sat_p90_carbo'],
                    'p50' => $val['wz_sat_p50_carbo'],
                    'p10' => $val['wz_sat_p10_carbo'],
                );

                $es[$key]['boi'] = array(
                    'p90' => $val['boi_p90'],
                    'p50' => $val['boi_p50'],
                    'p10' => $val['boi_p10'],
                );

                $es[$key]['bgi'] = array(
                    'p90' => $val['bgi_p90'],
                    'p50' => $val['bgi_p50'],
                    'p10' => $val['bgi_p10'],
                );
            }

        }

        return $es;
    }

    public function get_essential($data, $stage)
    {
        /*
        // New get_essential
         */
        $es = array();

        // "info" for any usefull debugging information
        // like two same closure name have one same Play

        $last_key = count($data) - 1;

        foreach ($data as $key => $val) {
            $es[$key] = array();
            $es[$key]['info'] = array();

            $es[$key]['basin'] = $val['basin'];
            $es[$key]['province'] = $val['province'];
            $es[$key]['kkks'] = $val['kkks'];
            $es[$key]['wk_id'] = $val['wk_id'];
            $es[$key]['wk'] = $val['wk'];
            $es[$key]['stage'] = $val['stage'];

            if ($stage == 'lead') {
                $es[$key]['structure'] = $val['structure'];
                $es[$key]['lead_latitude'] = $this->coord_break($val['lead_latitude'])['unite'];
                $es[$key]['lead_longitude'] = $this->coord_break($val['lead_longitude'])['unite'];
            } elseif ($stage == 'drillable' || $stage == 'postdrill' ||
                $stage == 'discovery') {
                $es[$key]['structure'] = $val['structure'];
                $es[$key]['prospect_latitude'] = $this->coord_break($val['prospect_latitude'])['unite'];
                $es[$key]['prospect_longitude'] = $this->coord_break($val['prospect_longitude'])['unite'];
            } elseif ($stage == 'well_discovery') {
                $es[$key]['structure'] = $val['structure'];
                $es[$key]['wz_name'] = $val['wz_name'];
                $es[$key]['wz_area'] = $val['wz_area'];
                $es[$key]['wz_thickness'] = $val['wz_thickness'];
                $es[$key]['wz_avg_por'] = $val['wz_avg_por'];
                $es[$key]['wz_second_por'] = $val['wz_second_por'];
                $es[$key]['wz_initial_sw'] = $val['wz_initial_sw'];
                $es[$key]['wz_oil_show'] = $val['wz_oil_show'];
                $es[$key]['wz_gas_show'] = $val['wz_gas_show'];
            }

            if ($stage == 'play' ||
                $stage == 'lead' ||
                $stage == 'drillable' ||
                $stage == 'postdrill' ||
                $stage == 'discovery') {
                $play_name = $this->play_name_maker(array(
                    'play_litho' => $val['play_litho'],
                    'play_form_serie' => $val['play_form_serie'],
                    'play_form' => $val['play_form'],
                    'play_age_serie' => $val['play_age_serie'],
                    'play_age' => $val['play_age'],
                    'play_env' => $val['play_env'],
                    'play_trap' => $val['play_trap'],
                ));
                $es[$key]['play'] = $play_name['play'];
                $es[$key]['info']['play_complete'] = $play_name['info'];

                $es[$key]['sr_data'] = $val['sr'];
                $es[$key]['re_data'] = $val['re'];
                $es[$key]['tr_data'] = $val['tr'];
                $es[$key]['dn_data'] = $val['dn'];

                $es[$key]['sr_age_system'] = $val['sr_age_system'];
                $es[$key]['sr_age_serie'] = $val['sr_age_serie'];
                $es[$key]['sr_formation'] = $val['sr_formation'];
                $es[$key]['sr_formation_serie'] = $val['sr_formation_serie'];
                $es[$key]['sr_kerogen'] = $val['sr_ker'];
                $es[$key]['sr_toc'] = $val['sr_toc'];
                $es[$key]['sr_hfu'] = $val['sr_hfu'];
                $es[$key]['sr_distribution'] = $val['sr_distribution'];
                $es[$key]['sr_continuity'] = $val['sr_continuity'];
                $es[$key]['sr_maturity'] = $val['sr_mat'];
                $es[$key]['sr_otr'] = $val['sr_otr'];

                $es[$key]['re_age_system'] = $val['re_age_system'];
                $es[$key]['re_age_serie'] = $val['re_age_serie'];
                $es[$key]['re_formation'] = $val['re_formation'];
                $es[$key]['re_formation_serie'] = $val['re_formation_serie'];
                $es[$key]['re_depos_set'] = $val['re_depos_set'];
                $es[$key]['re_depos_env'] = $val['re_depos_env'];
                $es[$key]['re_distribution'] = $val['re_dis'];
                $es[$key]['re_continuity'] = $val['re_continuity'];
                $es[$key]['re_lithology'] = $val['re_lit'];
                $es[$key]['re_por_primary'] = $val['re_pri'];
                $es[$key]['re_por_secondary'] = $val['re_sec'];

                $es[$key]['tr_seal_age_system'] = $val['tr_seal_age_system'];
                $es[$key]['tr_seal_age_serie'] = $val['tr_seal_age_serie'];
                $es[$key]['tr_seal_formation'] = $val['tr_seal_formation'];
                $es[$key]['tr_seal_formation_serie'] = $val['tr_seal_formation_serie'];
                $es[$key]['tr_seal_distribution'] = $val['tr_sdi'];
                $es[$key]['tr_seal_continuity'] = $val['tr_scn'];
                $es[$key]['tr_seal_type'] = $val['tr_stp'];
                $es[$key]['tr_age_system'] = $val['tr_age_system'];
                $es[$key]['tr_age_serie'] = $val['tr_age_serie'];
                $es[$key]['tr_geometry'] = $val['tr_geo'];
                $es[$key]['tr_type'] = $val['tr_trp'];
                $es[$key]['tr_closure'] = $val['tr_closure'];

                $es[$key]['dn_migration'] = $val['dn_migration'];
                $es[$key]['dn_kitchen'] = $val['dn_kit'];
                $es[$key]['dn_petroleum'] = $val['dn_tec'];
                $es[$key]['dn_age_early_system'] = $val['dn_age_early_system'];
                $es[$key]['dn_age_early_serie'] = $val['dn_age_early_serie'];
                $es[$key]['dn_age_late_system'] = $val['dn_age_late_system'];
                $es[$key]['dn_age_late_serie'] = $val['dn_age_late_serie'];
                $es[$key]['dn_preservation'] = $val['dn_prv'];
                $es[$key]['dn_pathway'] = $val['dn_mig'];
                $es[$key]['dn_migration_age_system'] = $val['dn_migration_age_system'];
                $es[$key]['dn_migration_age_serie'] = $val['dn_migration_age_serie'];
            }

            if ($stage == 'well_discovery') {
                $es[$key]['well'] = $val['well'];
                $es[$key]['well_formation'] = $val['well_formation'];
                $es[$key]['well_latitude'] = $val['well_latitude'];
                $es[$key]['well_longitude'] = $val['well_longitude'];
                $es[$key]['well_result'] = $val['well_result'];
                $es[$key]['well_ratio'] = $val['ratio'];
                $es[$key]['well_radius'] = $val['radius'];

                $es[$key]['porosity']['clastic'] = array(
                    'p90' => $val['wz_por_p90_clastic'],
                    'p50' => $val['wz_por_p50_clastic'],
                    'p10' => $val['wz_por_p10_clastic'],
                );

                $es[$key]['porosity']['carbo'] = array(
                    'p90' => $val['wz_por_p90_carbo'],
                    'p50' => $val['wz_por_p50_carbo'],
                    'p10' => $val['wz_por_p10_carbo'],
                );

                $es[$key]['saturation']['clastic'] = array(
                    'p90' => $val['wz_sat_p90_clastic'],
                    'p50' => $val['wz_sat_p50_clastic'],
                    'p10' => $val['wz_sat_p10_clastic'],
                );

                $es[$key]['saturation']['carbo'] = array(
                    'p90' => $val['wz_sat_p90_carbo'],
                    'p50' => $val['wz_sat_p50_carbo'],
                    'p10' => $val['wz_sat_p10_carbo'],
                );

                $es[$key]['boi'] = array(
                    'p90' => $val['boi_p90'],
                    'p50' => $val['boi_p50'],
                    'p10' => $val['boi_p10'],
                );

                $es[$key]['bgi'] = array(
                    'p90' => $val['bgi_p90'],
                    'p50' => $val['bgi_p50'],
                    'p10' => $val['bgi_p10'],
                );
            }

            if ($stage == 'discovery') {
                $es[$key]['gcf'] = 100;
            } elseif ($stage == 'play' ||
                $stage == 'lead' ||
                $stage == 'drillable' ||
                $stage == 'postdrill') {
                $es[$key]['gcf'] = array(
                    'sr' => $val['sr'],
                    're' => $val['re'],
                    'tr' => $val['tr'],
                    'dn' => $val['dn'],
                    'sr_ker' => $val['sr_ker'],
                    'sr_toc' => $val['sr_toc'],
                    'sr_hfu' => $val['sr_hfu'],
                    'sr_mat' => $val['sr_mat'],
                    'sr_otr' => $val['sr_otr'],
                    're_dis' => $val['re_dis'],
                    're_lit' => $val['re_lit'],
                    're_sec' => $val['re_sec'],
                    're_pri' => $val['re_pri'],
                    'tr_sdi' => $val['tr_sdi'],
                    'tr_scn' => $val['tr_scn'],
                    'tr_stp' => $val['tr_stp'],
                    'tr_geo' => $val['tr_geo'],
                    'tr_trp' => $val['tr_trp'],
                    'dn_kit' => $val['dn_kit'],
                    'dn_tec' => $val['dn_tec'],
                    'dn_prv' => $val['dn_prv'],
                    'dn_mig' => $val['dn_mig'],
                );
            }

            if ($stage == 'play') {
                $es[$key]['gcf'] = $this->gcfCalc($es[$key]['gcf'], $stage);
            }

            if ($stage == 'lead') {
                $es[$key]['low'] = array(
                    's2' => $val['s2_low'],
                    'gl' => $val['gl_low'],
                    'gh' => $val['gh_low'],
                    'gr' => $val['gr_low'],
                    'el' => $val['el_low'],
                    'rs' => $val['rs_low'],
                    'ot' => $val['ot_low'],
                );

                $es[$key]['best'] = array(
                    's2' => $val['s2_best'],
                    'gl' => $val['gl_best'],
                    'gh' => $val['gh_best'],
                    'gr' => $val['gr_best'],
                    'el' => $val['el_best'],
                    'rs' => $val['rs_best'],
                    'ot' => $val['ot_best'],
                );

                $es[$key]['high'] = array(
                    's2' => $val['s2_high'],
                    'gl' => $val['gl_high'],
                    'gh' => $val['gh_high'],
                    'gr' => $val['gr_high'],
                    'el' => $val['el_high'],
                    'rs' => $val['rs_high'],
                    'ot' => $val['ot_high'],
                );
            }

            if ($stage == 'drillable' || $stage == 'postdrill' ||
                $stage == 'discovery') {
                $es[$key]['area']['p90'] = array(
                    's2' => $val['s2_area_p90'],
                    's3' => $val['s3_area_p90'],
                    'gl' => $val['gl_area_p90'],
                    'gh' => $val['gh_area_p90'],
                    'gr' => $val['gr_area_p90'],
                    'el' => $val['el_area_p90'],
                    'rs' => $val['rs_area_p90'],
                    'ot' => $val['ot_area_p90'],
                );

                $es[$key]['area']['p50'] = array(
                    's2' => $val['s2_area_p50'],
                    's3' => $val['s3_area_p50'],
                    'gl' => $val['gl_area_p50'],
                    'gh' => $val['gh_area_p50'],
                    'gr' => $val['gr_area_p50'],
                    'el' => $val['el_area_p50'],
                    'rs' => $val['rs_area_p50'],
                    'ot' => $val['ot_area_p50'],
                );

                $es[$key]['area']['p10'] = array(
                    's2' => $val['s2_area_p10'],
                    's3' => $val['s3_area_p10'],
                    'gl' => $val['gl_area_p10'],
                    'gh' => $val['gh_area_p10'],
                    'gr' => $val['gr_area_p10'],
                    'el' => $val['el_area_p10'],
                    'rs' => $val['rs_area_p10'],
                    'ot' => $val['ot_area_p10'],
                );

                $es[$key]['netpay']['p90'] = array(
                    's2' => $val['s2_net_p90'],
                    's3' => $val['s3_net_p90'],
                );

                $es[$key]['netpay']['p50'] = array(
                    's2' => $val['s2_net_p50'],
                    's3' => $val['s3_net_p50'],
                );

                $es[$key]['netpay']['p10'] = array(
                    's2' => $val['s2_net_p10'],
                    's3' => $val['s3_net_p10'],
                );
            }

            if ($stage == 'drillable') {
                $es[$key]['porosity'] = array(
                    'p90' => $val['por_p90'],
                    'p50' => $val['por_p50'],
                    'p10' => $val['por_p10'],
                );

                $es[$key]['saturation'] = array(
                    'p90' => $val['sat_p90'],
                    'p50' => $val['sat_p50'],
                    'p10' => $val['sat_p10'],
                );
            }

            if ($stage == 'postdrill') {
                $es[$key]['well'] = $val['well'];
                $es[$key]['well_formation'] = $val['well_formation'];
                $es[$key]['well_latitude'] = $val['well_latitude'];
                $es[$key]['well_longitude'] = $val['well_longitude'];
                $es[$key]['well_result'] = $val['well_result'];
                $es[$key]['well_oil_show'] = $val['zone_oil_show'];
                $es[$key]['well_gas_show'] = $val['zone_gas_show'];

                if ($stage == 'discovery') {
                    $es[$key]['zone'] = $val['zone'];
                    $es[$key]['zone_thickness'] = $val['wz_thickness'];
                    $es[$key]['zone_radius'] = $val['wz_radius'];
                }

                $es[$key]['porosity']['clastic'] = array(
                    'p90' => $val['wz_por_p90_clastic'],
                    'p50' => $val['wz_por_p50_clastic'],
                    'p10' => $val['wz_por_p10_clastic'],
                );

                $es[$key]['porosity']['carbo'] = array(
                    'p90' => $val['wz_por_p90_carbo'],
                    'p50' => $val['wz_por_p50_carbo'],
                    'p10' => $val['wz_por_p10_carbo'],
                );

                $es[$key]['saturation']['clastic'] = array(
                    'p90' => $val['wz_sat_p90_clastic'],
                    'p50' => $val['wz_sat_p50_clastic'],
                    'p10' => $val['wz_sat_p10_clastic'],
                );

                $es[$key]['saturation']['carbo'] = array(
                    'p90' => $val['wz_sat_p90_carbo'],
                    'p50' => $val['wz_sat_p50_carbo'],
                    'p10' => $val['wz_sat_p10_carbo'],
                );

                $es[$key]['boi'] = array(
                    'p90' => $val['boi_p90'],
                    'p50' => $val['boi_p50'],
                    'p10' => $val['boi_p10'],
                );

                $es[$key]['bgi'] = array(
                    'p90' => $val['bgi_p90'],
                    'p50' => $val['bgi_p50'],
                    'p10' => $val['bgi_p10'],
                );
            }
        }

        return $es;
    }

    public function data_normalize($data, $stage)
    {
        if ($stage == 'lead') {
            foreach ($data as $key => $val) {
                $asv = $this->survey_correcting_lead($val['low'], $val['best'], $val['high']);
                $data[$key]['structure'] = $this->clear_name($val['structure']);
                $data[$key]['info']['survey_availability'] = $asv['info'];
                $data[$key]['low'] = floatval($this->smin($asv['low']));
                $data[$key]['best'] = floatval($this->average($asv['best']));
                $data[$key]['high'] = floatval($this->smax($asv['high']));
                $data[$key]['gcf'] = $this->gcfCalc($val['gcf'], $stage);
            }
        } elseif ($stage == 'drillable') {
            foreach ($data as $key => $val) {
                $asv = $this->survey_correcting_prospect($val['area'], $val['netpay'], $stage);
                $data[$key]['info']['survey_availability'] = $asv;
                $data[$key]['structure'] = $this->clear_name($val['structure']);
                $data[$key]['porosity']['p90'] = floatval($val['porosity']['p90'] / 100);
                $data[$key]['porosity']['p50'] = floatval($val['porosity']['p50'] / 100);
                $data[$key]['porosity']['p10'] = floatval($val['porosity']['p10'] / 100);

                $data[$key]['saturation']['p90'] = floatval($this->sw_to_hc($val['saturation']['p90']));
                $data[$key]['saturation']['p50'] = floatval($this->sw_to_hc($val['saturation']['p50']));
                $data[$key]['saturation']['p10'] = floatval($this->sw_to_hc($val['saturation']['p10']));

                $data[$key]['area']['p90'] = floatval($this->smin($asv['area']['p90']));
                $data[$key]['area']['p50'] = floatval($this->average($asv['area']['p50']));
                $data[$key]['area']['p10'] = floatval($this->smax($asv['area']['p10']));

                $data[$key]['netpay']['p90'] = floatval($this->smin($asv['netpay']['p90']));
                $data[$key]['netpay']['p50'] = floatval($this->average($asv['netpay']['p50']));
                $data[$key]['netpay']['p10'] = floatval($this->smax($asv['netpay']['p10']));
            }
        } elseif ($stage == 'postdrill') {
            foreach ($data as $key => $val) {
                $ps = $this->clastic_or_carbo($data[$key]['porosity'], $data[$key]['saturation']);
                $asv = $this->survey_correcting_prospect($val['area'], $val['netpay'], $stage);
                $data[$key]['info']['survey_availability'] = $asv['info'];
                $data[$key]['structure'] = $this->clear_name($val['structure']);
                $data[$key]['well_result'] = $this->oil_or_gas($val['well_result']);

                $data[$key]['porosity']['p90'] = floatval($ps['porosity']['p90'] / 100);
                $data[$key]['porosity']['p50'] = floatval($ps['porosity']['p50'] / 100);
                $data[$key]['porosity']['p10'] = floatval($ps['porosity']['p10'] / 100);
                unset($data[$key]['porosity']['clastic']);
                unset($data[$key]['porosity']['carbo']);

                $data[$key]['saturation']['p90'] = floatval($this->sw_to_hc($ps['saturation']['p90']));
                $data[$key]['saturation']['p50'] = floatval($this->sw_to_hc($ps['saturation']['p50']));
                $data[$key]['saturation']['p10'] = floatval($this->sw_to_hc($ps['saturation']['p10']));
                unset($data[$key]['saturation']['clastic']);
                unset($data[$key]['saturation']['carbo']);

                $data[$key]['area']['p90'] = floatval($this->smin($asv['area']['p90']));
                $data[$key]['area']['p50'] = floatval($this->average($asv['area']['p50']));
                $data[$key]['area']['p10'] = floatval($this->smax($asv['area']['p10']));

                $data[$key]['netpay']['p90'] = floatval($this->smin($asv['netpay']['p90']));
                $data[$key]['netpay']['p50'] = floatval($this->average($asv['netpay']['p50']));
                $data[$key]['netpay']['p10'] = floatval($this->smax($asv['netpay']['p10']));
            }
        } elseif ($stage == 'discovery') {
            foreach ($data as $key => $val) {
                $asv = $this->survey_correcting_prospect($val['area'], $val['netpay'], $stage);
                $data[$key]['info']['survey_availability'] = $asv['info'];
                $data[$key]['structure'] = $this->clear_name($val['structure']);

                $data[$key]['area']['p90'] = floatval($this->smin($asv['area']['p90']));
                $data[$key]['area']['p50'] = floatval($this->average($asv['area']['p50']));
                $data[$key]['area']['p10'] = floatval($this->smax($asv['area']['p10']));

                $data[$key]['netpay']['p90'] = floatval($this->smin($asv['netpay']['p90']));
                $data[$key]['netpay']['p50'] = floatval($this->average($asv['netpay']['p50']));
                $data[$key]['netpay']['p10'] = floatval($this->smax($asv['netpay']['p10']));
            }
        } elseif ($stage == 'well_discovery') {
            foreach ($data as $key => $val) {
                $ps = $this->clastic_or_carbo($data[$key]['porosity'], $data[$key]['saturation']);
                $data[$key]['structure'] = $this->clear_name($val['structure']);
                $data[$key]['well_result'] = $this->oil_or_gas($val['well_result']);
                $data[$key]['wz_name'] = $val['wz_name'];
                $data[$key]['wz_area'] = $val['wz_area'];
                $data[$key]['wz_thickness'] = $val['wz_thickness'];
                $data[$key]['wz_average_porosity'] = $val['wz_avg_por'];
                $data[$key]['wz_initial_sw'] = $val['wz_initial_sw'];
                $data[$key]['wz_oil_show'] = $val['wz_oil_show'];
                $data[$key]['wz_gas_show'] = $val['wz_gas_show'];

                $data[$key]['porosity']['p90'] = floatval($ps['porosity']['p90'] / 100);
                $data[$key]['porosity']['p50'] = floatval($ps['porosity']['p50'] / 100);
                $data[$key]['porosity']['p10'] = floatval($ps['porosity']['p10'] / 100);
                unset($data[$key]['porosity']['clastic']);
                unset($data[$key]['porosity']['carbo']);

                $data[$key]['saturation']['p90'] = floatval($this->sw_to_hc($ps['saturation']['p90']));
                $data[$key]['saturation']['p50'] = floatval($this->sw_to_hc($ps['saturation']['p50']));
                $data[$key]['saturation']['p10'] = floatval($this->sw_to_hc($ps['saturation']['p10']));
                unset($data[$key]['saturation']['clastic']);
                unset($data[$key]['saturation']['carbo']);
            }
        }
        return $data;
    }

    public function res_calculate($data, $stage)
    {
        if ($stage == 'drillable') {
            foreach ($data as $key => $val) {
                $data[$key]['ooip']['p90'] = $this->ooip_calc($val['area']['p90'],
                    $val['netpay']['p90'],
                    $val['porosity']['p90'],
                    $val['saturation']['p90']);
                $data[$key]['ooip']['p50'] = $this->ooip_calc($val['area']['p50'],
                    $val['netpay']['p50'],
                    $val['porosity']['p50'],
                    $val['saturation']['p50']);
                $data[$key]['ooip']['p10'] = $this->ooip_calc($val['area']['p10'],
                    $val['netpay']['p10'],
                    $val['porosity']['p10'],
                    $val['saturation']['p10']);

                $data[$key]['ogip']['p90'] = $this->ogip_calc($val['area']['p90'],
                    $val['netpay']['p90'],
                    $val['porosity']['p90'],
                    $val['saturation']['p90']);
                $data[$key]['ogip']['p50'] = $this->ogip_calc($val['area']['p50'],
                    $val['netpay']['p50'],
                    $val['porosity']['p50'],
                    $val['saturation']['p50']);
                $data[$key]['ogip']['p10'] = $this->ogip_calc($val['area']['p10'],
                    $val['netpay']['p10'],
                    $val['porosity']['p10'],
                    $val['saturation']['p10']);

                $data[$key]['gcf'] = $this->gcfCalc($val['gcf'], $stage);
            }
        } elseif ($stage == 'postdrill') {
            foreach ($data as $key => $val) {
                $ogip_p90 = 0;
                $ogip_p50 = 0;
                $ogip_p10 = 0;

                $igip_p90 = 0;
                $igip_p50 = 0;
                $igip_p10 = 0;

                $ooip_p90 = 0;
                $ooip_p50 = 0;
                $ooip_p10 = 0;

                $stoip_p90 = 0;
                $stoip_p50 = 0;
                $stoip_p10 = 0;

                if (empty($val['well'])) {
                    $data[$key]['well_result'] = '';
                } else {
                    $data[$key]['well_result'] = implode(' & ', $val['well_result']);
                }

                if ($val['boi']['p50'] == 0 || empty($val['boi']['p50'])) {
                    $boi_p50 = 1.22;
                } else {
                    $boi_p50 = $val['boi']['p50'];
                }

                if ($val['bgi']['p50'] == 0 || empty($val['bgi']['p50'])) {
                    $bgi_p50 = 0.0063;
                } else {
                    $bgi_p50 = $val['bgi']['p50'];
                }

                $ooip_p90 = $this->ooip_calc($val['area']['p90'],
                    $val['netpay']['p90'],
                    $val['porosity']['p90'],
                    $val['saturation']['p90']);
                $ooip_p50 = $this->ooip_calc($val['area']['p50'],
                    $val['netpay']['p50'],
                    $val['porosity']['p50'],
                    $val['saturation']['p50']);
                $ooip_p10 = $this->ooip_calc($val['area']['p10'],
                    $val['netpay']['p10'],
                    $val['porosity']['p10'],
                    $val['saturation']['p10']);

                $ogip_p90 = $this->ogip_calc($val['area']['p90'],
                    $val['netpay']['p90'],
                    $val['porosity']['p90'],
                    $val['saturation']['p90']);
                $ogip_p50 = $this->ogip_calc($val['area']['p50'],
                    $val['netpay']['p50'],
                    $val['porosity']['p50'],
                    $val['saturation']['p50']);
                $ogip_p10 = $this->ogip_calc($val['area']['p10'],
                    $val['netpay']['p10'],
                    $val['porosity']['p10'],
                    $val['saturation']['p10']);

                if ((in_array('Oil', $val['well_result']) &&
                    in_array('Gas', $val['well_result'])) ||
                    in_array('Dry', $val['well_result']) ||
                    in_array('Not Conclusive', $val['well_result'])) {
                    $stoip_p90 = $this->to_float($ooip_p90 / $boi_p50) * 0.5;
                    $stoip_p50 = $this->to_float($ooip_p50 / $boi_p50) * 0.5;
                    $stoip_p10 = $this->to_float($ooip_p10 / $boi_p50) * 0.5;

                    $igip_p90 = $this->to_float($ogip_p90 / $bgi_p50) * 0.5;
                    $igip_p50 = $this->to_float($ogip_p50 / $bgi_p50) * 0.5;
                    $igip_p10 = $this->to_float($ogip_p10 / $bgi_p50) * 0.5;

                } elseif (in_array('Oil', $val['well_result']) &&
                    !in_array('Gas', $val['well_result'])) {
                    $stoip_p90 = $this->to_float($ooip_p90 / $boi_p50);
                    $stoip_p50 = $this->to_float($ooip_p50 / $boi_p50);
                    $stoip_p10 = $this->to_float($ooip_p10 / $boi_p50);

                    $ogip_p90 = 0;
                    $ogip_p50 = 0;
                    $ogip_p10 = 0;

                    $igip_p90 = 0;
                    $igip_p50 = 0;
                    $igip_p10 = 0;

                    $bgi_p50 = 0;

                } elseif (in_array('Gas', $val['well_result']) &&
                    !in_array('Oil', $val['well_result'])) {
                    $igip_p90 = $this->to_float($ogip_p90 / $bgi_p50);
                    $igip_p50 = $this->to_float($ogip_p50 / $bgi_p50);
                    $igip_p10 = $this->to_float($ogip_p10 / $bgi_p50);

                    $ooip_p90 = 0;
                    $ooip_p50 = 0;
                    $ooip_p10 = 0;

                    $stoip_p90 = 0;
                    $stoip_p50 = 0;
                    $stoip_p10 = 0;

                    $boi_p50 = 0;
                }

                // Cek jika OOIP atau OGIP semuanya kosong
                // Tidak perlu ditampilkan ke dalam rekap

                if ($ooip_p90 == 0 && $ooip_p50 == 0 && $ooip_p10 == 0) {
                    $boi_p50 = 0;
                }

                if ($ogip_p90 == 0 && $ogip_p50 == 0 && $ogip_p10 == 0) {
                    $bgi_p50 = 0;
                }

                $data[$key]['ooip']['p90'] = $ooip_p90;
                $data[$key]['ooip']['p50'] = $ooip_p50;
                $data[$key]['ooip']['p10'] = $ooip_p10;

                $data[$key]['ogip']['p90'] = $ogip_p90;
                $data[$key]['ogip']['p50'] = $ogip_p50;
                $data[$key]['ogip']['p10'] = $ogip_p10;

                $data[$key]['stoip']['p90'] = $stoip_p90;
                $data[$key]['stoip']['p50'] = $stoip_p50;
                $data[$key]['stoip']['p10'] = $stoip_p10;

                $data[$key]['igip']['p90'] = $igip_p90;
                $data[$key]['igip']['p50'] = $igip_p50;
                $data[$key]['igip']['p10'] = $igip_p10;

                $data[$key]['boi']['p50'] = $boi_p50;
                $data[$key]['bgi']['p50'] = $bgi_p50;

                $data[$key]['gcf'] = $this->gcfCalc($val['gcf'], $stage);
            }
        }
        return $data;
    }

    public function ooip_calc($area, $net, $por, $sat, $floatret = true)
    {
        $ooip = 7758 * $area * $net * $por * $sat / pow(10, 6);

        if ($floatret == true) {
            $ooip = $this->to_float($ooip);
        }

        return $ooip;
    }

    public function stoip_calc($area, $net, $por, $sat, $boi, $floatret = true)
    {
        $stoip = 7758 * $area * $net * $por * $sat / pow(10, 6) / $boi;

        if ($floatret == true) {
            $stoip = $this->to_float($stoip);
        }

        return $stoip;
    }

    public function pone_oil_calc($rad, $net, $por, $sat, $boi, $floatret = true)
    {
        $pone = 7758 * 3.14 * pow($rad, 2) / 43560 * $net * $por * $sat / $boi / pow(10, 6);

        if ($floatret == true) {
            $pone = $this->to_float($pone);
        }

        return $pone;
    }

    public function twop_oil_calc($rad, $net, $por, $sat, $boi, $floatret = true)
    {
        $twop = 7758 * 3.14 * 1.5 * pow($rad, 2) / 43560 * $net * $por * $sat / $boi / pow(10, 6);

        if ($floatret == true) {
            $twop = $this->to_float($twop);
        }

        return $twop;
    }

    public function ogip_calc($area, $net, $por, $sat, $floatret = true)
    {
        $ogip = 43560 * $area * $net * $por * $sat / pow(10, 9);

        if ($floatret == true) {
            $ogip = $this->to_float($ogip);
        }

        return $ogip;
    }

    public function igip_calc($area, $net, $por, $sat, $bgi, $floatret = true)
    {
        $igip = 43560 * $area * $net * $por * $sat / pow(10, 9) / $bgi;

        if ($floatret == true) {
            $igip = $this->to_float($igip);
        }

        return $igip;
    }

    public function pone_gas_calc($rad, $net, $por, $sat, $bgi, $floatret = true)
    {
        $pone = 3.14 * pow($rad, 2) * $net * $por * $sat / $bgi / pow(10, 9);

        if ($floatret == true) {
            $pone = $this->to_float($pone);
        }

        return $pone;
    }

    public function twop_gas_calc($rad, $net, $por, $sat, $bgi, $floatret = true)
    {
        $twop = 3.14 * 1.5 * pow($rad, 2) * $net * $por * $sat / $bgi / pow(10, 9);

        if ($floatret == true) {
            $twop = $this->to_float($twop);
        }

        return $twop;
    }

    public function printCheckErr($w)
    {
        $is_profile = $this->qGet("SELECT kkks.psc_id is_profile
            FROM adm_kkks kkks, adm_psc psc WHERE psc.wk_id='{$w}' AND kkks.psc_id=psc.psc_id");
        $is_data = $this->qGet("SELECT play_id FROM
            rsc_play WHERE wk_id='{$w}'");

        if (!$is_profile) {
            return array(
                'err' => 1,
                'err_type' => 'profile',
                'err_con' => "Please update Working Area Profile before print.");
        } elseif (!$is_data) {
            return array(
                'err' => 'err_print',
                'err_type' => 'no_data',
                'err_con' => "You don't have any resource data.");
        } else {
            return 'complete';
        }
    }

    public function printCheckData($w)
    {
        $d = array('lead' => 1, 'drillable' => 1, 'postdrill' => 1, 'discovery' => 1, 'well' => 1);

        if (!$this->qGet("SELECT ld.lead_id FROM rsc_lead ld, rsc_play pl
            WHERE pl.wk_id='{$w}' AND ld.play_id=pl.play_id LIMIT 1")) {
            $d['lead'] = 0;
        }

        if (!$this->qGet("SELECT pr.prospect_id FROM rsc_play pl, rsc_prospect pr
            WHERE pl.wk_id='{$w}' AND pr.play_id=pl.play_id AND pr.prospect_type='drillable' LIMIT 1")) {
            $d['drillable'] = 0;
        }

        if (!$this->qGet("SELECT pr.prospect_id FROM rsc_play pl, rsc_prospect pr
            WHERE pl.wk_id='{$w}' AND pr.play_id=pl.play_id AND pr.prospect_type='postdrill' LIMIT 1")) {
            $d['postdrill'] = 0;
        }

        if (!$this->qGet("SELECT pr.prospect_id FROM rsc_play pl, rsc_prospect pr
            WHERE pl.wk_id='{$w}' AND pr.play_id=pl.play_id AND pr.prospect_type='discovery' LIMIT 1")) {
            $d['discovery'] = 0;
        }

        if (!$this->qGet("SELECT 1 FROM rsc_well WHERE wk_id='{$w}' LIMIT 1")) {
            $d['well'] = 0;
        }

        return $d;
    }

    public function printGetAdm()
    {
        $wk_id = Yii::app()->user->wk_id;
        $wk_name = $this->qGet("SELECT wk_name FROM adm_working_area WHERE wk_id='{$wk_id}'");
        $kkks_name = $this->qGet(
            "SELECT kkks.kkks_name kkks_name
            FROM adm_working_area wk, adm_kkks kkks, adm_psc psc
            WHERE wk.wk_id='{$wk_id}'
            AND psc.wk_id=wk.wk_id
            AND kkks.psc_id=psc.psc_id");

        return array('wk_id' => $wk_id, 'wk_name' => $wk_name[0]['wk_name'],
            'kkks_name' => $kkks_name[0]['kkks_name']);
    }

    public function leadSurveyCheck($lead_id)
    {
        $chk = $this->qGet("
            SELECT EXISTS(
            SELECT ls2d_id
            FROM rsc_lead_2d
            WHERE lead_id='{$lead_id}') 2d_ls2d, EXISTS(
            SELECT lsel_id
            FROM rsc_lead_electromagnetic
            WHERE lead_id='{$lead_id}') electromagnetic_lsel, EXISTS(
            SELECT lsgc_id
            FROM rsc_lead_geochemistry
            WHERE lead_id='{$lead_id}') geochemistry_lsgc, EXISTS(
            SELECT lsgf_id
            FROM rsc_lead_geological
            WHERE lead_id='{$lead_id}') geological_lsgf, EXISTS(
            SELECT lsgv_id
            FROM rsc_lead_gravity
            WHERE lead_id='{$lead_id}') gravity_lsgv, EXISTS(
            SELECT lsor_id
            FROM rsc_lead_other
            WHERE lead_id='{$lead_id}') other_lsor, EXISTS(
            SELECT lsrt_id
            FROM rsc_lead_resistivity
            WHERE lead_id='{$lead_id}') resistivity_lsrt");

        $retVal = array();
        foreach ($chk[0] as $key => $val) {
            if ($val == '1') {
                $retVal[$key] = $val;
            }

        }

        if (empty($retVal)) {
            $retVal = 0;
        }

        return $retVal;
    }

    public function area_check($arr)
    {
        $p90 = array_slice($arr, 0, 8);
        $p50 = array_slice($arr, 8, 8);
        $p10 = array_slice($arr, 16, 8);
        $fp90 = array_filter(array_slice($arr, 0, 8));
        $fp50 = array_filter(array_slice($arr, 8, 8));
        $fp10 = array_filter(array_slice($arr, 16, 8));

        if (!empty($fp90)) {
            $p90 = floatval(min($fp90));
        } else {
            $p90 = 0;
        }

        if (!empty($fp50)) {
            $p50 = floatval($this->average($fp50));
        } else {
            $p50 = 0;
        }

        if (!empty($fp10)) {
            $p10 = floatval(max($fp10));
        } else {
            $p10 = 0;
        }

        return array("p90" => $p90, "p50" => $p50, "p10" => $p10);
    }

    public function net_check($arr)
    {
        $p90 = array_slice($arr, 0, 2);
        $p50 = array_slice($arr, 2, 2);
        $p10 = array_slice($arr, 4, 2);
        $fp90 = array_filter(array_slice($arr, 0, 2));
        $fp50 = array_filter(array_slice($arr, 2, 2));
        $fp10 = array_filter(array_slice($arr, 4, 2));

        if (!empty($fp90)) {
            $p90 = floatval(min($fp90));
        } else {
            $p90 = 0;
        }

        if (!empty($fp50)) {
            $p50 = floatval($this->average($fp50));
        } else {
            $p50 = 0;
        }

        if (!empty($fp10)) {
            $p10 = floatval(max($fp10));
        } else {
            $p10 = 0;
        }

        return array("p90" => $p90, "p50" => $p50, "p10" => $p10);
    }

    public function clastic_or_carbo($porosity, $saturation)
    {
        /*
        // Check if Porosity and Saturation from Wellzone
        // use Clastic or Carbonate or both.
        //
        //   Args :
        //     $porosity = array(
        //        'clastic' => array('p90', 'p50', 'p10'),
        //        'carbo' => array('p90', 'p50', 'p10'),
        //     )
        //
        //     $saturation = array(
        //        'clastic' => array('p90', 'p50', 'p10'),
        //        'carbo' => array('p90', 'p50', 'p10'),
        //     )
         */
        $ret = array('porosity' => array(), 'saturation' => array());

        $por_clastic = $this->rationalize($porosity['clastic']);
        $sat_clastic = $this->rationalize($saturation['clastic']);
        $por_carbo = $this->rationalize($porosity['carbo']);
        $sat_carbo = $this->rationalize($saturation['carbo']);

        if (empty($por_carbo) && empty($sat_carbo)) {
            $ret['porosity']['p90'] = $porosity['clastic']['p90'];
            $ret['porosity']['p50'] = $porosity['clastic']['p50'];
            $ret['porosity']['p10'] = $porosity['clastic']['p10'];
            $ret['saturation']['p90'] = $saturation['clastic']['p90'];
            $ret['saturation']['p50'] = $saturation['clastic']['p50'];
            $ret['saturation']['p10'] = $saturation['clastic']['p10'];
        } elseif (empty($por_clastic) && empty($sat_clastic)) {
            $ret['porosity']['p90'] = $porosity['carbo']['p90'];
            $ret['porosity']['p50'] = $porosity['carbo']['p50'];
            $ret['porosity']['p10'] = $porosity['carbo']['p10'];
            $ret['saturation']['p90'] = $saturation['carbo']['p90'];
            $ret['saturation']['p50'] = $saturation['carbo']['p50'];
            $ret['saturation']['p10'] = $saturation['carbo']['p10'];
        } elseif ((empty($por_carbo) && empty($sat_carbo)) && (empty($por_clastic) && empty($sat_clastic))) {
            $ret['porosity']['p90'] = 0;
            $ret['porosity']['p50'] = 0;
            $ret['porosity']['p10'] = 0;
            $ret['saturation']['p90'] = 0;
            $ret['saturation']['p50'] = 0;
            $ret['saturation']['p10'] = 0;
        } else {
            $sum_clastic = array_sum($porosity['clastic']) + array_sum($saturation['clastic']);
            $sum_carbo = array_sum($porosity['carbo']) + array_sum($saturation['carbo']);

            if ($sum_carbo >= $sum_clastic) {
                $ret['porosity']['p90'] = $porosity['carbo']['p90'];
                $ret['porosity']['p50'] = $porosity['carbo']['p50'];
                $ret['porosity']['p10'] = $porosity['carbo']['p10'];
                $ret['saturation']['p90'] = $saturation['carbo']['p90'];
                $ret['saturation']['p50'] = $saturation['carbo']['p50'];
                $ret['saturation']['p10'] = $saturation['carbo']['p10'];
            } else {
                $ret['porosity']['p90'] = $porosity['clastic']['p90'];
                $ret['porosity']['p50'] = $porosity['clastic']['p50'];
                $ret['porosity']['p10'] = $porosity['clastic']['p10'];
                $ret['saturation']['p90'] = $saturation['clastic']['p90'];
                $ret['saturation']['p50'] = $saturation['clastic']['p50'];
                $ret['saturation']['p10'] = $saturation['clastic']['p10'];
            }
        }

        return $ret;
    }

    public function clastic_check($por, $sat)
    {
        $clastic = array_filter(array_merge(array_slice($por, 0, 3), array_slice($sat, 0, 3)));
        $carbo = array_filter(array_merge(array_slice($por, 3, 3), array_slice($sat, 3, 3)));

        if (!empty($clastic) && !empty($carbo)) {
            $clastic_score = ($por['cl_por_p10']) * ($sat['cl_sat_p10']);
            $carbo_score = ($por['cr_por_p10']) * ($sat['cr_sat_p10']);

            if ($clastic_score >= $carbo_score) {
                $use_what = 'clastic';
            } elseif ($carbo_score >= $clastic_score) {
                $use_what = 'carbo';
            } else {
                $use_what = 'nothing';
            }
        } elseif (!empty($clastic)) {
            $use_what = 'clastic';
        } elseif (!empty($carbo)) {
            $use_what = 'carbo';
        } else {
            $use_what = 'nothing';
        }

        if ($use_what == 'clastic') {
            $no_avail = 0;
            $por_p90 = $por['cl_por_p90'] / 100;
            $por_p50 = $por['cl_por_p50'] / 100;
            $por_p10 = $por['cl_por_p10'] / 100;
            $sat_p90 = $sat['cl_sat_p90'] / 100;
            $sat_p50 = $sat['cl_sat_p50'] / 100;
            $sat_p10 = $sat['cl_sat_p10'] / 100;
        } elseif ($use_what == 'carbo') {
            $no_avail = 0;
            $por_p90 = $por['cr_por_p90'] / 100;
            $por_p50 = $por['cr_por_p50'] / 100;
            $por_p10 = $por['cr_por_p10'] / 100;
            $sat_p90 = $sat['cr_sat_p90'] / 100;
            $sat_p50 = $sat['cr_sat_p50'] / 100;
            $sat_p10 = $sat['cr_sat_p10'] / 100;
        } elseif ($use_what == 'nothing') {
            $no_avail = 1;
            $por_p90 = 0;
            $por_p50 = 0;
            $por_p10 = 0;
            $sat_p90 = 0;
            $sat_p50 = 0;
            $sat_p10 = 0;
        }

        return array(
            'por_p90' => $por_p90,
            'por_p50' => $por_p50,
            'por_p10' => $por_p10,
            'sat_p90' => $sat_p90,
            'sat_p50' => $sat_p50,
            'sat_p10' => $sat_p10,
            'no_avail' => $no_avail,
        );
    }

    public function total_wk()
    {
        $r = new DataQuery;
        $re = $r->total_wk_sql();
        $data = $this->qGet($re);

        return $data[0]['total_wk'];
    }

    public function total_play()
    {
        $r = new DataQuery;
        $re = $r->total_play_sql();
        $data = $this->qGet($re);

        return $data[0]['total_play'];
    }

    public function total_lead()
    {
        $r = new DataQuery;
        $re = $r->total_lead_sql();
        $data = $this->qGet($re);

        return $data[0]['total_lead'];
    }

    public function total_drillable()
    {
        $r = new DataQuery;
        $re = $r->total_drillable_sql();
        $data = $this->qGet($re);

        return $data[0]['total_drillable'];
    }

    public function total_postdrill()
    {
        $r = new DataQuery;
        $re = $r->total_postdrill_sql();
        $data = $this->qGet($re);

        return $data[0]['total_postdrill'];
    }

    public function total_discovery()
    {
        $r = new DataQuery;
        $re = $r->total_discovery_sql();
        $data = $this->qGet($re);

        return $data[0]['total_discovery'];
    }

    public function total_well_postdrill()
    {
        $r = new DataQuery;
        $re = $r->total_well_postdrill_sql();
        $data = $this->qGet($re);

        return $data[0]['total_well_postdrill'];
    }

    public function total_well_discovery()
    {
        $r = new DataQuery;
        $re = $r->total_well_discovery_sql();
        $data = $this->qGet($re);

        return $data[0]['total_well_discovery'];
    }

    public function total_kirim_surat()
    {
        $r = new DataQuery;
        $re = $r->total_kirim_surat_sql();
        $data = $this->qGet($re);

        return $data[0]['total_kirim_surat'];
    }

    public function total_tidak_kirim_surat()
    {
        $r = new DataQuery;
        $re = $r->total_tidak_kirim_surat_sql();
        $data = $this->qGet($re);

        return $data[0]['total_tidak_kirim_surat'];
    }

    public function total_ada_data()
    {
        $r = new DataQuery;
        $re = $r->total_ada_data_sql();
        $data = $this->qGet($re);

        return $data[0]['total_ada_data'];
    }

    public function total_tidak_ada_data()
    {
        $r = new DataQuery;
        $re = $r->total_tidak_ada_data_sql();
        $data = $this->qGet($re);

        return $data[0]['total_tidak_ada_data'];
    }

    public function total_wk_eksplorasi()
    {
        $r = new DataQuery;
        $re = $r->total_wk_eksplorasi_sql();
        $data = $this->qGet($re);

        return $data[0]['total_wk_eksplorasi'];
    }

    public function total_wk_eksploitasi()
    {
        $r = new DataQuery;
        $re = $r->total_wk_eksploitasi_sql();
        $data = $this->qGet($re);

        return $data[0]['total_wk_eksploitasi'];
    }

    public function total_wk_terminasi()
    {
        $r = new DataQuery;
        $re = $r->total_wk_terminasi_sql();
        $data = $this->qGet($re);

        return $data[0]['total_wk_terminasi'];
    }

    public function rekap_resources()
    {
        $r = new DataQuery;
        $re = $r->resources_sql();

        $data = $this->qGet($re['sql']);

        foreach ($data as $key => $val) {
            $data[$key]['basin'] = str_replace(',', '<br/>', $val['basin']);
        }

        $sort = new CSort();
        $sort->attributes = array(
            'Basin' => array(
                'asc' => 'basin',
                'desc' => 'basin DESC',
            ),
            'Nama WK' => array(
                'asc' => 'wk',
                'desc' => 'wk DESC',
            ),
            'Play' => array(
                'asc' => 'play',
                'desc' => 'play DESC',
            ),
            'Lead' => array(
                'asc' => 'lead',
                'desc' => 'lead DESC',
            ),
            'Drillable' => array(
                'asc' => 'drillable',
                'desc' => 'drillable DESC',
            ),
            'Postdrill' => array(
                'asc' => 'postdrill',
                'desc' => 'postdrill DESC',
            ),
            'Discovery' => array(
                'asc' => 'discovery',
                'desc' => 'discovery DESC',
            ),
            'Status Pengiriman Surat' => array(
                'asc' => 'surat',
                'desc' => 'surat DESC',
            ),
            'Montage 2012' => array(
                'asc' => 'montage_link',
                'desc' => 'montage_link DESC',
            ),
            'Montage 2013' => array(
                'asc' => 'montage_link',
                'desc' => 'montage_link DESC',
            ),
            'Montage 2014' => array(
                'asc' => 'montage_link',
                'desc' => 'montage_link DESC',
            ),
            'Status WK' => array(
                'asc' => 'stage',
                'desc' => 'stage DESC',
            ),
        );

        $wk_provider = new CArrayDataProvider($data, array(
            'keyField' => 'wk_id',
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 300,
            ),
        ));

        return $wk_provider;
    }

    public function rekap_list_wk()
    {
        $r = new DataQuery();
        $re = $r->resources_sql();

        $data = $this->qGet($re['sql']);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'play' => $val['play'],
                'lead' => $val['lead'],
                'drillable' => $val['drillable'],
                'postdrill' => $val['postdrill'],
                'discovery' => $val['discovery'],
                'surat' => $val['surat'],
                'stage' => $val['stage'],
            );
        }

        $this->arrayToCsvDataPrint('List_All_WK', $result, $re['header']);
    }

    public function get_play_detail($play_id)
    {
        $play = Yii::app()->db->createCommand("
            SELECT * FROM rsc_play WHERE play_id = '{$play_id}'
        ")->queryAll();
    }

    public function list_play($wkid)
    {
        $plays = Yii::app()->db->createCommand("
            SELECT
                'Play' as classification,
                play.play_id,
                gcf.gcf_res_lithology as play_litho,
                gcf.gcf_res_formation_serie as play_form_serie,
                gcf.gcf_res_formation as play_form,
                gcf.gcf_res_age_serie as play_age_serie,
                gcf.gcf_res_age_system as play_age,
                gcf.gcf_res_depos_env as play_env,
                gcf.gcf_trap_type as play_trap
            FROM
                rsc_play as play,
                rsc_gcf as gcf
            WHERE
                gcf.gcf_id = play.gcf_id
                AND play.wk_id = '{$wkid}'
        ")->queryAll();

        foreach ($plays as &$play) {
            $play['play_name'] = $this->play_name_maker($play)['play'];
        }

        return $plays;
    }

    public function rekap_play()
    {
        $stage = 'play';
        $r = new DataQuery;
        $re = $r->rekap_play_sql();

        $data = $this->qGet($re['sql']);
        $data = $this->get_essential($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'play' => $val['play'],
                'gcf' => $val['gcf']['rda'],
            );
        }
        $this->arrayToCsvDataPrint('Rekapitulasi_Play', $result, $re['header']);
    }

    public function rekap_lead()
    {
        $stage = 'lead';
        $r = new DataQuery;
        $re = $r->rekap_lead_sql();

        $data = $this->qGet($re['sql']);
        $data = $this->get_essential($data, $stage);
        $data = $this->data_normalize($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'latitude' => $val['lead_latitude'],
                'longitude' => $val['lead_longitude'],
                'structure' => $val['structure'],
                'play' => $val['play'],
                'low' => $val['low'],
                'best' => $val['best'],
                'high' => $val['high'],

                'sr_data' => $val['sr_data'],
                'sr_age_system' => $val['sr_age_system'],
                'sr_age_serie' => $val['sr_age_serie'],
                'sr_formation' => $val['sr_formation'],
                'sr_formation_serie' => $val['sr_formation_serie'],
                'sr_kerogen' => $val['sr_kerogen'],
                'sr_toc' => $val['sr_toc'],
                'sr_hfu' => $val['sr_hfu'],
                'sr_distribution' => $val['sr_distribution'],
                'sr_continuity' => $val['sr_continuity'],
                'sr_maturity' => $val['sr_maturity'],
                'sr_otr' => $val['sr_otr'],

                're_data' => $val['re_data'],
                're_age_system' => $val['re_age_system'],
                're_age_serie' => $val['re_age_serie'],
                're_formation' => $val['re_formation'],
                're_formation_serie' => $val['re_formation_serie'],
                're_depos_set' => $val['re_depos_set'],
                're_depos_env' => $val['re_depos_env'],
                're_distribution' => $val['re_distribution'],
                're_continuity' => $val['re_continuity'],
                're_lithology' => $val['re_lithology'],
                're_por_primary' => $val['re_por_primary'],
                're_por_secondary' => $val['re_por_secondary'],

                'tr_data' => $val['tr_data'],
                'tr_seal_age_system' => $val['tr_seal_age_system'],
                'tr_seal_age_serie' => $val['tr_seal_age_serie'],
                'tr_seal_formation' => $val['tr_seal_formation'],
                'tr_seal_formation_serie' => $val['tr_seal_formation_serie'],
                'tr_seal_distribution' => $val['tr_seal_distribution'],
                'tr_seal_continuity' => $val['tr_seal_continuity'],
                'tr_seal_type' => $val['tr_seal_type'],
                'tr_age_system' => $val['tr_age_system'],
                'tr_age_serie' => $val['tr_age_serie'],
                'tr_geometry' => $val['tr_geometry'],
                'tr_type' => $val['tr_type'],
                'tr_closure' => $val['tr_closure'],

                'dn_data' => $val['dn_data'],
                'dn_migration' => $val['dn_migration'],
                'dn_kitchen' => $val['dn_kitchen'],
                'dn_petroleum' => $val['dn_petroleum'],
                'dn_age_early_system' => $val['dn_age_early_system'],
                'dn_age_early_serie' => $val['dn_age_early_serie'],
                'dn_age_late_system' => $val['dn_age_late_system'],
                'dn_age_late_serie' => $val['dn_age_late_serie'],
                'dn_preservation' => $val['dn_preservation'],
                'dn_pathway' => $val['dn_pathway'],
                'dn_migration_age_system' => $val['dn_migration_age_system'],
                'dn_migration_age_serie' => $val['dn_migration_age_serie'],

                'sr_val' => $val['gcf']['sr'],
                'res_val' => $val['gcf']['re'],
                'tr_val' => $val['gcf']['tr'],
                'dn_val' => $val['gcf']['dn'],
                'gcf' => $val['gcf']['rda'],
                'gcf_norm' => $val['gcf']['rda_final'],
            );
        }
        $this->arrayToCsvDataPrint('Rekapitulasi_Lead', $result, $re['header']);
    }

    public function rekap_drillable()
    {
        $stage = 'drillable';
        $r = new DataQuery;
        $re = $r->rekap_drillable_sql();

        $data = $this->qGet($re['sql']);
        $data = $this->get_essential($data, $stage);
        $data = $this->data_normalize($data, $stage);
        $data = $this->res_calculate($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'latitude' => $val['prospect_latitude'],
                'longitude' => $val['prospect_longitude'],
                'structure' => $val['structure'],
                'play' => $val['play'],
                'area_p90' => $val['area']['p90'],
                'area_p50' => $val['area']['p50'],
                'area_p10' => $val['area']['p10'],
                'netpay_p90' => $val['netpay']['p90'],
                'netpay_p50' => $val['netpay']['p50'],
                'netpay_p10' => $val['netpay']['p10'],
                'porosity_p90' => $val['porosity']['p90'],
                'porosity_p50' => $val['porosity']['p50'],
                'porosity_p10' => $val['porosity']['p10'],
                'saturation_p90' => $val['saturation']['p90'],
                'saturation_p50' => $val['saturation']['p50'],
                'saturation_p10' => $val['saturation']['p10'],
                'ooip_p90' => $val['ooip']['p90'],
                'ooip_p50' => $val['ooip']['p50'],
                'ooip_p10' => $val['ooip']['p10'],
                'ogip_p90' => $val['ogip']['p90'],
                'ogip_p50' => $val['ogip']['p50'],
                'ogip_p10' => $val['ogip']['p10'],
                'sr_data' => $val['sr_data'],
                'sr_age_system' => $val['sr_age_system'],
                'sr_age_serie' => $val['sr_age_serie'],
                'sr_formation' => $val['sr_formation'],
                'sr_formation_serie' => $val['sr_formation_serie'],
                'sr_kerogen' => $val['sr_kerogen'],
                'sr_toc' => $val['sr_toc'],
                'sr_hfu' => $val['sr_hfu'],
                'sr_distribution' => $val['sr_distribution'],
                'sr_continuity' => $val['sr_continuity'],
                'sr_maturity' => $val['sr_maturity'],
                'sr_otr' => $val['sr_otr'],

                're_data' => $val['re_data'],
                're_age_system' => $val['re_age_system'],
                're_age_serie' => $val['re_age_serie'],
                're_formation' => $val['re_formation'],
                're_formation_serie' => $val['re_formation_serie'],
                're_depos_set' => $val['re_depos_set'],
                're_depos_env' => $val['re_depos_env'],
                're_distribution' => $val['re_distribution'],
                're_continuity' => $val['re_continuity'],
                're_lithology' => $val['re_lithology'],
                're_por_primary' => $val['re_por_primary'],
                're_por_secondary' => $val['re_por_secondary'],

                'tr_data' => $val['tr_data'],
                'tr_seal_age_system' => $val['tr_seal_age_system'],
                'tr_seal_age_serie' => $val['tr_seal_age_serie'],
                'tr_seal_formation' => $val['tr_seal_formation'],
                'tr_seal_formation_serie' => $val['tr_seal_formation_serie'],
                'tr_seal_distribution' => $val['tr_seal_distribution'],
                'tr_seal_continuity' => $val['tr_seal_continuity'],
                'tr_seal_type' => $val['tr_seal_type'],
                'tr_age_system' => $val['tr_age_system'],
                'tr_age_serie' => $val['tr_age_serie'],
                'tr_geometry' => $val['tr_geometry'],
                'tr_type' => $val['tr_type'],
                'tr_closure' => $val['tr_closure'],

                'dn_data' => $val['dn_data'],
                'dn_migration' => $val['dn_migration'],
                'dn_kitchen' => $val['dn_kitchen'],
                'dn_petroleum' => $val['dn_petroleum'],
                'dn_age_early_system' => $val['dn_age_early_system'],
                'dn_age_early_serie' => $val['dn_age_early_serie'],
                'dn_age_late_system' => $val['dn_age_late_system'],
                'dn_age_late_serie' => $val['dn_age_late_serie'],
                'dn_preservation' => $val['dn_preservation'],
                'dn_pathway' => $val['dn_pathway'],
                'dn_migration_age_system' => $val['dn_migration_age_system'],
                'dn_migration_age_serie' => $val['dn_migration_age_serie'],

                'sr_val' => $val['gcf']['sr'],
                'res_val' => $val['gcf']['re'],
                'tr_val' => $val['gcf']['tr'],
                'dn_val' => $val['gcf']['dn'],
                'gcf' => $val['gcf']['rda'],
                'gcf_normal' => $val['gcf']['rda_final'],
            );
        }
        $this->arrayToCsvDataPrint('Rekapitulasi_Drillable', $result, $re['header']);
    }

    public function rekap_postdrill()
    {
        $stage = 'postdrill';
        $r = new DataQuery;
        $re = $r->rekap_postdrill_sql();

        $data = $this->qGet($re['sql']);
        $data = $this->get_essential($data, $stage);
        $data = $this->data_normalize($data, $stage);
        $data = $this->res_calculate($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'latitude' => $val['prospect_latitude'],
                'longitude' => $val['prospect_longitude'],
                'structure' => $val['structure'],
                'play' => $val['play'],
                'well' => $val['well'],
                'well_formation' => $val['well_formation'],
                'well_result' => $val['well_result'],
                'area_p90' => $val['area']['p90'],
                'area_p50' => $val['area']['p50'],
                'area_p10' => $val['area']['p10'],
                'netpay_p90' => $val['netpay']['p90'],
                'netpay_p50' => $val['netpay']['p50'],
                'netpay_p10' => $val['netpay']['p10'],
                'porosity_p90' => $val['porosity']['p90'],
                'porosity_p50' => $val['porosity']['p50'],
                'porosity_p10' => $val['porosity']['p10'],
                'saturation_p90' => $val['saturation']['p90'],
                'saturation_p50' => $val['saturation']['p50'],
                'saturation_p10' => $val['saturation']['p10'],
                'ooip_p90' => $val['ooip']['p90'],
                'ooip_p50' => $val['ooip']['p50'],
                'ooip_p10' => $val['ooip']['p10'],
                'boi_p50' => $val['boi']['p50'],
                'stoip_p90' => $val['stoip']['p90'],
                'stoip_p50' => $val['stoip']['p50'],
                'stoip_p10' => $val['stoip']['p10'],
                'ogip_p90' => $val['ogip']['p90'],
                'ogip_p50' => $val['ogip']['p50'],
                'ogip_p10' => $val['ogip']['p10'],
                'bgi_p50' => $val['bgi']['p50'],
                'igip_p90' => $val['igip']['p90'],
                'igip_p50' => $val['igip']['p50'],
                'igip_p10' => $val['igip']['p10'],
                'sr_data' => $val['sr_data'],
                'sr_age_system' => $val['sr_age_system'],
                'sr_age_serie' => $val['sr_age_serie'],
                'sr_formation' => $val['sr_formation'],
                'sr_formation_serie' => $val['sr_formation_serie'],
                'sr_kerogen' => $val['sr_kerogen'],
                'sr_toc' => $val['sr_toc'],
                'sr_hfu' => $val['sr_hfu'],
                'sr_distribution' => $val['sr_distribution'],
                'sr_continuity' => $val['sr_continuity'],
                'sr_maturity' => $val['sr_maturity'],
                'sr_otr' => $val['sr_otr'],

                're_data' => $val['re_data'],
                're_age_system' => $val['re_age_system'],
                're_age_serie' => $val['re_age_serie'],
                're_formation' => $val['re_formation'],
                're_formation_serie' => $val['re_formation_serie'],
                're_depos_set' => $val['re_depos_set'],
                're_depos_env' => $val['re_depos_env'],
                're_distribution' => $val['re_distribution'],
                're_continuity' => $val['re_continuity'],
                're_lithology' => $val['re_lithology'],
                're_por_primary' => $val['re_por_primary'],
                're_por_secondary' => $val['re_por_secondary'],

                'tr_data' => $val['tr_data'],
                'tr_seal_age_system' => $val['tr_seal_age_system'],
                'tr_seal_age_serie' => $val['tr_seal_age_serie'],
                'tr_seal_formation' => $val['tr_seal_formation'],
                'tr_seal_formation_serie' => $val['tr_seal_formation_serie'],
                'tr_seal_distribution' => $val['tr_seal_distribution'],
                'tr_seal_continuity' => $val['tr_seal_continuity'],
                'tr_seal_type' => $val['tr_seal_type'],
                'tr_age_system' => $val['tr_age_system'],
                'tr_age_serie' => $val['tr_age_serie'],
                'tr_geometry' => $val['tr_geometry'],
                'tr_type' => $val['tr_type'],
                'tr_closure' => $val['tr_closure'],

                'dn_data' => $val['dn_data'],
                'dn_migration' => $val['dn_migration'],
                'dn_kitchen' => $val['dn_kitchen'],
                'dn_petroleum' => $val['dn_petroleum'],
                'dn_age_early_system' => $val['dn_age_early_system'],
                'dn_age_early_serie' => $val['dn_age_early_serie'],
                'dn_age_late_system' => $val['dn_age_late_system'],
                'dn_age_late_serie' => $val['dn_age_late_serie'],
                'dn_preservation' => $val['dn_preservation'],
                'dn_pathway' => $val['dn_pathway'],
                'dn_migration_age_system' => $val['dn_migration_age_system'],
                'dn_migration_age_serie' => $val['dn_migration_age_serie'],

                'sr_val' => $val['gcf']['sr'],
                'res_val' => $val['gcf']['re'],
                'tr_val' => $val['gcf']['tr'],
                'dn_val' => $val['gcf']['dn'],
                'gcf' => $val['gcf']['rda'],
                'gcf_norm' => $val['gcf']['rda_final'],
            );
        }

        $this->arrayToCsvDataPrint('Rekapitulasi_Postdrill', $result, $re['header']);
    }

    public function rekap_discovery()
    {
        $stage = 'discovery';
        $r = new DataQuery;
        $re = $r->rekap_discovery_sql();

        $data = $this->qGet($re['sql']);
        $data = $this->get_essential($data, $stage);
        $data = $this->data_normalize($data, $stage);
        //$data = $this->res_calculate($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'latitude' => $val['prospect_latitude'],
                'longitude' => $val['prospect_longitude'],
                'structure' => $val['structure'],
                'play' => $val['play'],
                'prospect_latitude' => $val['prospect_latitude'],
                'prospect_longitude' => $val['prospect_longitude'],
                'area_p90' => $val['area']['p90'],
                'area_p50' => $val['area']['p50'],
                'area_p10' => $val['area']['p10'],
                'netpay_p90' => $val['netpay']['p90'],
                'netpay_p50' => $val['netpay']['p50'],
                'netpay_p10' => $val['netpay']['p10'],
            );
        }

        $this->arrayToCsvDataPrint('Rekapitulasi_Discovery', $result, $re['header']);
    }

    public function rekap_well_discovery()
    {
        $stage = 'well_discovery';
        $r = new DataQuery;
        $re = $r->rekap_well_discovery_sql();

        $data = $this->qGet($re['sql']);
        $data = $this->get_essential($data, $stage);
        $data = $this->data_normalize($data, $stage);
        //$data = $this->res_calculate($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'structure' => $val['structure'],
                'well' => $val['well'],
                'well_formation' => $val['well_formation'],
                'well_latitude' => $val['well_latitude'],
                'well_longitude' => $val['well_longitude'],
                'well_result' => implode(' & ', $val['well_result']),
                'well_ratio' => $val['well_ratio'],
                'well_radius' => $val['well_radius'],
                'wz_name' => $val['wz_name'],
                'wz_area' => $val['wz_area'],
                'wz_thickness' => $val['wz_thickness'],
                'wz_avg_por' => $val['wz_avg_por'],
                'wz_second_por' => $val['wz_second_por'],
                'wz_initial' => $val['wz_initial_sw'],
                'wz_oil_show' => $val['wz_oil_show'],
                'wz_gas_show' => $val['wz_gas_show'],
                'porosity_p90' => $val['porosity']['p90'],
                'porosity_p50' => $val['porosity']['p50'],
                'porosity_p10' => $val['porosity']['p10'],
                'saturation_p90' => $val['saturation']['p90'],
                'saturation_p50' => $val['saturation']['p50'],
                'saturation_p10' => $val['saturation']['p10'],
                'boi_p90' => $val['boi']['p90'],
                'boi_p50' => $val['boi']['p50'],
                'boi_p10' => $val['boi']['p10'],
                'bgi_p90' => $val['bgi']['p90'],
                'bgi_p50' => $val['bgi']['p50'],
                'bgi_p10' => $val['bgi']['p10'],
            );
        }

        $this->arrayToCsvDataPrint('Rekapitulasi_Well_Discovery', $result, $re['header']);
    }

    public function rekap_all_data()
    {
        $r = new DataQuery;
        $re = $r->rekap_all_data_sql();

        $ld = $this->qGet($re['sql']['lead']);
        $pr = $this->qGet($re['sql']['prospect']);

        //echo 'LEAD';
        //var_dump($ld[0]);die();

        //echo 'PROSPECT';
        //var_dump($pr[0]);die();

        $ld = $this->all_get_essential($ld, 'lead');
        $pr = $this->all_get_essential($pr, 'prospect');
        $data = $this->data_normalize($data, $stage);
        //$data = $this->res_calculate($data, $stage);

        $result = array();
        foreach ($data as $key => $val) {
            $result[] = array(
                'basin' => $val['basin'],
                'province' => $val['province'],
                'kkks' => $val['kkks'],
                'wk_id' => $val['wk_id'],
                'wk' => $val['wk'],
                'stage' => $val['stage'],
                'structure' => $val['structure'],
                'well' => $val['well'],
                'well_formation' => $val['well_formation'],
                'well_latitude' => $val['well_latitude'],
                'well_longitude' => $val['well_longitude'],
                'well_result' => implode(' & ', $val['well_result']),
                'well_ratio' => $val['well_ratio'],
                'well_radius' => $val['well_radius'],
                'porosity_p90' => $val['porosity']['p90'],
                'porosity_p50' => $val['porosity']['p50'],
                'porosity_p10' => $val['porosity']['p10'],
                'saturation_p90' => $val['saturation']['p90'],
                'saturation_p50' => $val['saturation']['p50'],
                'saturation_p10' => $val['saturation']['p10'],
                'boi_p90' => $val['boi']['p90'],
                'boi_p50' => $val['boi']['p50'],
                'boi_p10' => $val['boi']['p10'],
                'bgi_p90' => $val['bgi']['p90'],
                'bgi_p50' => $val['bgi']['p50'],
                'bgi_p10' => $val['bgi']['p10'],
            );
        }

        $this->arrayToCsvDataPrint('Rekapitulasi_Well_Discovery', $result, $re['header']);
    }

    public function getWorkingAreaAssets($wkid)
    {
        $play = $this->qGet("SELECT play_id s FROM rsc_play
                    WHERE wk_id='{$wkid}'");

        $lead = $this->qGet("SELECT ld.lead_id s
                    FROM rsc_play pl, rsc_lead ld
                    WHERE pl.wk_id='{$wkid}'
                    AND ld.play_id=pl.play_id
                    AND ld.lead_is_deleted=0");

        $drillable = $this->qGet("SELECT pr.prospect_id s
                    FROM rsc_play pl, rsc_prospect pr
                    WHERE pl.wk_id='{$wkid}'
                    AND pr.play_id=pl.play_id
                    AND pr.prospect_type='Drillable'
                    AND pr.prospect_is_deleted=0");

        $postdrill = $this->qGet("SELECT pr.prospect_id s
                    FROM rsc_play pl, rsc_prospect pr
                    WHERE pl.wk_id='{$wkid}'
                    AND pr.play_id=pl.play_id
                    AND pr.prospect_type='Postdrill'
                    AND pr.prospect_is_deleted=0");

        $discovery = $this->qGet("SELECT pr.prospect_id s
                    FROM rsc_play pl, rsc_prospect pr
                    WHERE pl.wk_id='{$wkid}'
                    AND pl.wk_id<>'WK1452'
                    AND pr.play_id=pl.play_id
                    AND pr.prospect_type='Discovery'
                    AND pr.prospect_is_deleted=0");

        $postdrillWell = Yii::app()->db->createCommand()
            ->select(['wl_id well_id'])
            ->from('rsc_well')
            ->where('wk_id = :wkid', [':wkid' => $wkid])
            ->andWhere('prospect_type = "Postdrill"')
            ->queryAll();

        $discoveryWell = Yii::app()->db->createCommand()
            ->select(['wl_id well_id'])
            ->from('rsc_well')
            ->where('wk_id = :wkid', [':wkid' => $wkid])
            ->andWhere('prospect_type = "Discovery"')
            ->queryAll();

        return [
            'play' => $play,
            'lead' => $lead,
            'drillable' => $drillable,
            'postdrill' => $postdrill,
            'discovery' => $discovery,
            'postdrill_well' => $postdrillWell,
            'discovery_well' => $discoveryWell,
        ];
    }

    public function printA1($w)
    {
        $a1 = array();

        $basin_id = $this->qGet("SELECT basin_id
            FROM rsc_play WHERE wk_id='{$w}'
            GROUP BY basin_id");

        foreach ($basin_id as $key => $val) {
            $ac_bas = $val['basin_id'];
            $name_bas = $this->qGet("SELECT basin_name FROM adm_basin
                    WHERE basin_id='{$ac_bas}'");

            $s_pl = $this->qGet("SELECT COUNT(play_id) s FROM rsc_play
                    WHERE wk_id='{$w}'
                    AND basin_id='{$ac_bas}'");
            $s_ld = $this->qGet("SELECT COUNT(ld.lead_id) s
                    FROM rsc_play pl, rsc_lead ld
                    WHERE pl.wk_id='{$w}'
                    AND pl.basin_id='{$ac_bas}'
                    AND ld.play_id=pl.play_id
                    AND ld.lead_is_deleted=0");
            $s_dr = $this->qGet("SELECT COUNT(pr.prospect_id) s
                    FROM rsc_play pl, rsc_prospect pr
                    WHERE pl.wk_id='{$w}'
                    AND pl.basin_id='{$ac_bas}'
                    AND pr.play_id=pl.play_id
                    AND pr.prospect_type='Drillable'
                    AND pr.prospect_is_deleted=0");
            $s_pd = $this->qGet("SELECT COUNT(pr.prospect_id) s
                    FROM rsc_play pl, rsc_prospect pr
                    WHERE pl.wk_id='{$w}'
                    AND pl.basin_id='{$ac_bas}'
                    AND pr.play_id=pl.play_id
                    AND pr.prospect_type='Postdrill'
                    AND pr.prospect_is_deleted=0");
            $s_dc = $this->qGet("SELECT COUNT(pr.prospect_id) s
                    FROM rsc_play pl, rsc_prospect pr
                    WHERE pl.wk_id='{$w}'
                    AND pl.basin_id='{$ac_bas}'
                    AND pr.play_id=pl.play_id
                    AND pr.prospect_type='Discovery'
                    AND pr.prospect_is_deleted=0");

            $a1[] = array(
                'basin_name' => $name_bas[0]['basin_name'],
                'play' => $s_pl[0]['s'],
                'lead' => $s_ld[0]['s'],
                'drillable' => $s_dr[0]['s'],
                'postdrill' => $s_pd[0]['s'],
                'discovery' => $s_dc[0]['s']);
        }

        return $a1;

    }

    public function printA2($w)
    {
        $a2 = $this->qGet("SELECT wl_name, prospect_type,
            wl_latitude, wl_longitude,
            wl_type, wl_result, wl_formation, YEAR(wl_date_complete) wl_year
            FROM rsc_well WHERE wk_id='{$w}'");

        foreach ($a2 as $k => $v) {
            $t = explode(',', $v['wl_latitude']);
            $n = explode(',', $v['wl_longitude']);

            $a2[$k]['wl_latitude'] = $t[0] . '&deg; ' . $t[1] . '&#39; ' . $t[2] . '&quot; ' . strtoupper($t[3]);
            $a2[$k]['wl_longitude'] = $n[0] . '&deg; ' . $n[1] . '&#39; ' . $n[2] . '&quot; ' . strtoupper($n[3]);
        }

        return $a2;

    }

    public function printA3Play($w)
    {
        $play = array();

        $a3_pl = $this->qGet("
            SELECT b.basin_name,
            CONCAT(
                g.gcf_res_lithology, '-',
                g.gcf_res_formation, '-',
                g.gcf_res_age_serie, ' ',
                g.gcf_res_age_system, '-',
                g.gcf_res_depos_env, '-',
                g.gcf_trap_type
            ) pl_name,
            g.gcf_is_sr sr,
            g.gcf_is_res re,
            g.gcf_is_trap tr,
            g.gcf_is_dyn dn,
            g.gcf_sr_kerogen sr_ker,
            g.gcf_sr_toc sr_toc,
            g.gcf_sr_hfu sr_hfu,
            g.gcf_sr_maturity sr_mat,
            g.gcf_sr_otr sr_otr,
            g.gcf_res_distribution re_dis,
            g.gcf_res_lithology re_lit,
            g.gcf_res_por_secondary re_sec,
            g.gcf_res_por_primary re_pri,
            g.gcf_trap_seal_distribution tr_sdi,
            g.gcf_trap_seal_continuity tr_scn,
            g.gcf_trap_seal_type tr_stp,
            g.gcf_trap_geometry tr_geo,
            g.gcf_trap_type tr_trp,
            g.gcf_dyn_kitchen dn_kit,
            g.gcf_dyn_petroleum dn_tec,
            g.gcf_dyn_preservation dn_prv,
            g.gcf_dyn_pathways dn_mig
            FROM rsc_play pl, rsc_gcf g, adm_basin b
            WHERE g.gcf_id=pl.gcf_id
            AND b.basin_id=pl.basin_id
            AND pl.wk_id='{$w}'");

        foreach ($a3_pl as $key => $val) {
            $play[] = array(
                'basin_name' => $val['basin_name'],
                'play_name' => $this->notAvailableDestroyer($val['pl_name']),
                'gcf_val' => $this->gcfCalc(array_slice($val, 2), 'play'));
        }

        return $play;
    }

    public function printA3Lead($w)
    {
        $a3_ld = $this->qGet("
            SELECT b.basin_name,
             CONCAT(ld.structure_name, ' @ ',
                g.gcf_res_lithology, '-',
                g.gcf_res_formation, '-',
                g.gcf_res_age_serie, ' ',
                g.gcf_res_age_system, '-',
                g.gcf_res_depos_env, '-',
                g.gcf_trap_type) ld_name,
             ld.lead_id,
             g.gcf_is_sr sr,
             g.gcf_is_res re,
             g.gcf_is_trap tr,
             g.gcf_is_dyn dn,
             g.gcf_sr_kerogen sr_ker,
             g.gcf_sr_toc sr_toc,
             g.gcf_sr_hfu sr_hfu,
             g.gcf_sr_maturity sr_mat,
             g.gcf_sr_otr sr_otr,
             g.gcf_res_distribution re_dis,
             g.gcf_res_lithology re_lit,
             g.gcf_res_por_secondary re_sec,
             g.gcf_res_por_primary re_pri,
             g.gcf_trap_seal_distribution tr_sdi,
             g.gcf_trap_seal_continuity tr_scn,
             g.gcf_trap_seal_type tr_stp,
             g.gcf_trap_geometry tr_geo,
             g.gcf_trap_type tr_trp,
             g.gcf_dyn_kitchen dn_kit,
             g.gcf_dyn_petroleum dn_tec,
             g.gcf_dyn_preservation dn_prv,
             g.gcf_dyn_pathways dn_mig
            FROM rsc_lead ld, rsc_play pl, rsc_gcf g, adm_basin b
            WHERE
             ld.play_id=pl.play_id
             AND g.gcf_id=ld.gcf_id
             AND b.basin_id=pl.basin_id
             AND ld.lead_is_deleted='0'
             AND pl.wk_id='{$w}'");

        return $this->printA3LeadCalc($a3_ld);
    }

    public function printA3LeadCalc($arr)
    {
        $fc = 0;
        $lead = array();

        foreach ($arr as $key => $val) {
            $sur = $this->leadSurveyCheck($val['lead_id']);

            if ($sur != 0) {
                $sur_data = array('low' => array(), 'best' => array(), 'high' => array());
                $lead_id = $val['lead_id'];

                foreach ($sur as $skey => $sval) {
                    ($skey == '2d_ls2d' ? $y = 'hight' : $y = 'high');

                    $spe = explode('_', $skey);
                    $sql = $this->qGet("
                        SELECT
                            {$spe[1]}_low_estimate low,
                            {$spe[1]}_best_estimate best,
                            {$spe[1]}_{$y}_estimate high
                        FROM rsc_lead_{$spe[0]}
                        WHERE lead_id='{$lead_id}'");
                    $sur_data['low'][] = floatval($sql[0]['low']);
                    $sur_data['best'][] = floatval($sql[0]['best']);
                    $sur_data['high'][] = floatval($sql[0]['high']);
                }

                $sur_data['low'] = $this->smin($sur_data['low']);
                $sur_data['best'] = $this->average($sur_data['best']);
                $sur_data['high'] = $this->smax($sur_data['high']);

                $lead[] = array(
                    'basin_name' => $val['basin_name'],
                    'lead_name' => $this->notAvailableDestroyer($val['ld_name']),
                    'low' => $sur_data['low'],
                    'best' => $sur_data['best'],
                    'high' => $sur_data['high'],
                    'gcf_val' => $this->gcfCalc(array_slice($arr[$fc], 3), 'lead'),
                );
                $fc++;
            } else {
                $lead[] = array(
                    'basin_name' => $val['basin_name'],
                    'lead_name' => $this->notAvailableDestroyer($val['ld_name']),
                    'low' => 0,
                    'best' => 0,
                    'high' => 0,
                    'gcf_val' => 0,
                );
            }
        }
        return $lead;
    }

    public function printA3Drillable($w)
    {
        $drillable = array();

        $a3_dr = $this->qGet("
            SELECT
                b.basin_name,
                CONCAT(pr.structure_name, ' @ ',
                    g.gcf_res_lithology, '-',
                    g.gcf_res_formation, '-',
                    g.gcf_res_age_serie, ' ',
                    g.gcf_res_age_system, '-',
                    g.gcf_res_depos_env, '-',
                    g.gcf_trap_type) dr_name,
                pr.prospect_id,
                g.gcf_is_sr sr,
                g.gcf_is_res re,
                g.gcf_is_trap tr,
                g.gcf_is_dyn dn,
                g.gcf_sr_kerogen sr_ker,
                g.gcf_sr_toc sr_toc,
                g.gcf_sr_hfu sr_hfu,
                g.gcf_sr_maturity sr_mat,
                g.gcf_sr_otr sr_otr,
                g.gcf_res_distribution re_dis,
                g.gcf_res_lithology re_lit,
                g.gcf_res_por_secondary re_sec,
                g.gcf_res_por_primary re_pri,
                g.gcf_trap_seal_distribution tr_sdi,
                g.gcf_trap_seal_continuity tr_scn,
                g.gcf_trap_seal_type tr_stp,
                g.gcf_trap_geometry tr_geo,
                g.gcf_trap_type tr_trp,
                g.gcf_dyn_kitchen dn_kit,
                g.gcf_dyn_petroleum dn_tec,
                g.gcf_dyn_preservation dn_prv,
                g.gcf_dyn_pathways dn_mig,
                dr.dr_por_p90 por_p90,
                dr.dr_por_p50 por_p50,
                dr.dr_por_p10 por_p10,
                dr.dr_satur_p90 sat_p90,
                dr.dr_satur_p50 sat_p50,
                dr.dr_satur_p10 sat_p10,
            " . $this->_survey_sql . "
            FROM
                rsc_prospect pr,
                rsc_play pl,
                rsc_gcf g,
                rsc_drillable dr,
                adm_basin b
            WHERE
                pr.play_id=pl.play_id AND g.gcf_id=pr.gcf_id
                AND b.basin_id=pl.basin_id
                AND pr.prospect_is_deleted='0'
                AND pl.wk_id='{$w}'
                AND pr.prospect_type='drillable'
                AND dr.prospect_id=pr.prospect_id
            ");

        foreach ($a3_dr as $key => $val) {
            if (!$this->ifAllZero(array_slice($val, 31, 8))) {
                $area_p90 = 0;
                $area_p50 = 0;
                $area_p10 = 0;
                $net_p90 = 0;
                $net_p50 = 0;
                $net_p10 = 0;
                $sat_p90 = 0;
                $sat_p50 = 0;
                $sat_p10 = 0;
                $por_p90 = 0;
                $por_p50 = 0;
                $por_p10 = 0;
                $oil_p90 = 0;
                $oil_p50 = 0;
                $oil_p10 = 0;
                $gas_p90 = 0;
                $gas_p50 = 0;
                $gas_p10 = 0;
                //$gcf_dr = $this->gcfCalc(array_slice($val, 3, 22), 'drillable');
                $gcf_dr = 0;
            } else {
                $area_p90 = floatval($this->smin($this->rationalize(array_slice($val, 31, 8))));
                $area_p50 = floatval($this->average($this->rationalize(array_slice($val, 39, 8))));
                $area_p10 = floatval($this->smax($this->rationalize(array_slice($val, 47, 8))));

                $net_p90 = floatval($this->smin($this->rationalize(array_slice($val, 55, 2))));
                $net_p50 = floatval($this->average($this->rationalize(array_slice($val, 57, 2))));
                $net_p10 = floatval($this->smax($this->rationalize(array_slice($val, 59, 2))));
                $sat_p90 = ($val['sat_p90'] == 0 ? 0 : floatval(1 - ($val['sat_p90'] / 100)));
                $sat_p50 = ($val['sat_p50'] == 0 ? 0 : floatval(1 - ($val['sat_p50'] / 100)));
                $sat_p10 = ($val['sat_p10'] == 0 ? 0 : floatval(1 - ($val['sat_p10'] / 100)));
                $por_p90 = floatval($val['por_p90'] / 100);
                $por_p50 = floatval($val['por_p50'] / 100);
                $por_p10 = floatval($val['por_p10'] / 100);

                $oil_p90 = (7758 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 6);
                $oil_p50 = (7758 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 6);
                $oil_p10 = (7758 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 6);
                $gas_p90 = (43560 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 9);
                $gas_p50 = (43560 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 9);
                $gas_p10 = (43560 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 9);
                $gcf_dr = $this->gcfCalc(array_slice($val, 3, 22), 'drillable');
            }

            $drillable[] = array(
                'basin_name' => $val['basin_name'],
                'prospect_name' => $this->notAvailableDestroyer($val['dr_name']),
                'area_p90' => $area_p90,
                'area_p50' => $area_p50,
                'area_p10' => $area_p10,
                'net_p90' => $net_p90,
                'net_p50' => $net_p50,
                'net_p10' => $net_p10,
                'por_p90' => $por_p90 * 100,
                'por_p50' => $por_p50 * 100,
                'por_p10' => $por_p10 * 100,
                'sat_p90' => $sat_p90 * 100,
                'sat_p50' => $sat_p50 * 100,
                'sat_p10' => $sat_p10 * 100,
                'oil_p90' => $this->to_float($oil_p90),
                'oil_p50' => $this->to_float($oil_p50),
                'oil_p10' => $this->to_float($oil_p10),
                'gas_p90' => $this->to_float($gas_p90, 4),
                'gas_p50' => $this->to_float($gas_p50, 4),
                'gas_p10' => $this->to_float($gas_p10, 4),
                'gcf_val' => $gcf_dr,
            );

        }

        return $drillable;
    }

    public function printA3Postdrill($w)
    {
        $postdrill = array();

        $a3_pd = $this->qGet("
            SELECT
                b.basin_name,
                CONCAT(pr.structure_name, ' @ ',
                    g.gcf_res_lithology, '-',
                    g.gcf_res_formation, '-',
                    g.gcf_res_age_serie, ' ',
                    g.gcf_res_age_system, '-',
                    g.gcf_res_depos_env, '-',
                    g.gcf_trap_type) pd_name,
                pr.prospect_id,
                g.gcf_is_sr sr,
                g.gcf_is_res re,
                g.gcf_is_trap tr,
                g.gcf_is_dyn dn,
                g.gcf_sr_kerogen sr_ker,
                g.gcf_sr_toc sr_toc,
                g.gcf_sr_hfu sr_hfu,
                g.gcf_sr_maturity sr_mat,
                g.gcf_sr_otr sr_otr,
                g.gcf_res_distribution re_dis,
                g.gcf_res_lithology re_lit,
                g.gcf_res_por_secondary re_sec,
                g.gcf_res_por_primary re_pri,
                g.gcf_trap_seal_distribution tr_sdi,
                g.gcf_trap_seal_continuity tr_scn,
                g.gcf_trap_seal_type tr_stp,
                g.gcf_trap_geometry tr_geo,
                g.gcf_trap_type tr_trp,
                g.gcf_dyn_kitchen dn_kit,
                g.gcf_dyn_petroleum dn_tec,
                g.gcf_dyn_preservation dn_prv,
                g.gcf_dyn_pathways dn_mig,

                " . $this->_survey_sql . ",

                IFNULL(wz.zone_clastic_p90_por, wz.zone_carbo_p90_por) por_p90,
                IFNULL(wz.zone_clastic_p50_por, wz.zone_carbo_p50_por) por_p50,
                IFNULL(wz.zone_clastic_p10_por, wz.zone_carbo_p10_por) por_p10,
                IFNULL(wz.zone_clastic_p90_satur, wz.zone_carbo_p90_satur) sat_p90,
                IFNULL(wz.zone_clastic_p50_satur, wz.zone_carbo_p50_satur) sat_p50,
                IFNULL(wz.zone_clastic_p10_satur, wz.zone_carbo_p10_satur) sat_p10,

                IFNULL(wz.zone_fvf_oil_p90, 1.22) boi_p90,
                IFNULL(wz.zone_fvf_oil_p50, 1.22) boi_p50,
                IFNULL(wz.zone_fvf_oil_p10, 1.22) boi_p10,
                IFNULL(wz.zone_fvf_gas_p90, 0.0063) bgi_p90,
                IFNULL(wz.zone_fvf_gas_p50, 0.0063) bgi_p50,
                IFNULL(wz.zone_fvf_gas_p10, 0.0063) bgi_p10,
        wl.wl_result well_result
            FROM
                rsc_prospect pr,
                rsc_play pl,
                rsc_gcf g,
                rsc_well wl,
                rsc_wellzone wz,
                adm_basin b
            WHERE
                pr.play_id=pl.play_id AND g.gcf_id=pr.gcf_id
                AND b.basin_id=pl.basin_id
                AND pr.prospect_is_deleted='0'
                AND pl.wk_id='{$w}'
                AND pr.prospect_type='postdrill'
                AND wl.wk_id=pl.wk_id
                AND wl.prospect_id=pr.prospect_id
                AND wz.wl_id=wl.wl_id
        ");

        foreach ($a3_pd as $key => $val) {
            if (!$this->ifAllZero(array_slice($val, 25, 38))) {
                $boi_p90 = 0;
                $boi_p50 = 0;
                $boi_p10 = 0;
                $bgi_p90 = 0;
                $bgi_p50 = 0;
                $bgi_p10 = 0;

                $area_p90 = 0;
                $area_p50 = 0;
                $area_p10 = 0;
                $net_p90 = 0;
                $net_p50 = 0;
                $net_p10 = 0;
                $sat_p90 = 0;
                $sat_p50 = 0;
                $sat_p10 = 0;
                $por_p90 = 0;
                $por_p50 = 0;
                $por_p10 = 0;

                $mmbo_p90 = 0;
                $mmbo_p50 = 0;
                $mmbo_p10 = 0;
                $bcf_p90 = 0;
                $bcf_p50 = 0;
                $bcf_p10 = 0;

                $mmstb_p90 = 0;
                $mmstb_p50 = 0;
                $mmstb_p10 = 0;
                $bscf_p90 = 0;
                $bscf_p50 = 0;
                $bscf_p10 = 0;
                $gcf_dr = 0;
            } else {
                $boi_p90 = $val['boi_p90'];
                $boi_p50 = $val['boi_p50'];
                $boi_p10 = $val['boi_p10'];
                $bgi_p90 = $val['bgi_p90'];
                $bgi_p50 = $val['bgi_p50'];
                $bgi_p10 = $val['bgi_p10'];

                $area_p90 = floatval($this->smin($this->rationalize(array_slice($val, 25, 8))));
                $area_p50 = floatval($this->average($this->rationalize(array_slice($val, 33, 8))));
                $area_p10 = floatval($this->smax($this->rationalize(array_slice($val, 41, 8))));
                $net_p90 = floatval($this->smin($this->rationalize(array_slice($val, 49, 2))));
                $net_p50 = floatval($this->average($this->rationalize(array_slice($val, 51, 2))));
                $net_p10 = floatval($this->smax($this->rationalize(array_slice($val, 53, 2))));

                $sat_p90 = ($val['sat_p90'] == 0 ? 0 : floatval(($val['sat_p90'] / 100)));
                $sat_p50 = ($val['sat_p50'] == 0 ? 0 : floatval(($val['sat_p50'] / 100)));
                $sat_p10 = ($val['sat_p10'] == 0 ? 0 : floatval(($val['sat_p10'] / 100)));
                $por_p90 = floatval($val['por_p90'] / 100);
                $por_p50 = floatval($val['por_p50'] / 100);
                $por_p10 = floatval($val['por_p10'] / 100);

                $well_res = explode("+", $val['well_result']);

                if (in_array("Oil", $well_res)) {
                    $mmbo_p90 = (7758 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 6);
                    $mmbo_p50 = (7758 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 6);
                    $mmbo_p10 = (7758 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 6);

                    $mmstb_p90 = ((7758 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 6)) / $boi_p90;
                    $mmstb_p50 = ((7758 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 6)) / $boi_p50;
                    $mmstb_p10 = ((7758 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 6)) / $boi_p10;

                    $bcf_p90 = 0;
                    $bcf_p50 = 0;
                    $bcf_p10 = 0;
                    $bscf_p90 = 0;
                    $bscf_p50 = 0;
                    $bscf_p10 = 0;
                    $bgi_p90 = 0;
                    $bgi_p50 = 0;
                    $bgi_p10 = 0;
                } else if (in_array("Gas", $well_res)) {
                    $mmbo_p90 = 0;
                    $mmbo_p50 = 0;
                    $mmbo_p10 = 0;

                    $mmstb_p90 = 0;
                    $mmstb_p50 = 0;
                    $mmstb_p10 = 0;
                    $boi_p90 = 0;
                    $boi_p50 = 0;
                    $boi_p10 = 0;

                    $bcf_p90 = (43560 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 9);
                    $bcf_p50 = (43560 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 9);
                    $bcf_p10 = (43560 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 9);
                    $bscf_p90 = ((43560 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 9)) / $bgi_p90;
                    $bscf_p50 = ((43560 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 9)) / $bgi_p50;
                    $bscf_p10 = ((43560 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 9)) / $bgi_p10;
                } else {
                    $mmbo_p90 = (7758 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 6);
                    $mmbo_p50 = (7758 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 6);
                    $mmbo_p10 = (7758 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 6);
                    $bcf_p90 = (43560 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 9);
                    $bcf_p50 = (43560 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 9);
                    $bcf_p10 = (43560 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 9);

                    $mmstb_p90 = ((7758 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 6)) / $boi_p90;
                    $mmstb_p50 = ((7758 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 6)) / $boi_p50;
                    $mmstb_p10 = ((7758 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 6)) / $boi_p10;
                    $bscf_p90 = ((43560 * $area_p90 * $net_p90 * $por_p90 * $sat_p90) / pow(10, 9)) / $bgi_p90;
                    $bscf_p50 = ((43560 * $area_p50 * $net_p50 * $por_p50 * $sat_p50) / pow(10, 9)) / $bgi_p50;
                    $bscf_p10 = ((43560 * $area_p10 * $net_p10 * $por_p10 * $sat_p10) / pow(10, 9)) / $bgi_p10;
                }

                $gcf_dr = $this->gcfCalc(array_slice($val, 3, 22), 'postdrill');
            }

            $postdrill[] = array(
                'basin_name' => $val['basin_name'],
                'prospect_name' => $this->notAvailableDestroyer($val['pd_name']),
                'area_p90' => $area_p90,
                'area_p50' => $area_p50,
                'area_p10' => $area_p10,
                'net_p90' => $net_p90,
                'net_p50' => $net_p50,
                'net_p10' => $net_p10,
                'por_p90' => $por_p90 * 100,
                'por_p50' => $por_p50 * 100,
                'por_p10' => $por_p10 * 100,
                'sat_p90' => $sat_p90 * 100,
                'sat_p50' => $sat_p50 * 100,
                'sat_p10' => $sat_p10 * 100,
                'mmbo_p90' => $this->to_float($mmbo_p90),
                'mmbo_p50' => $this->to_float($mmbo_p50),
                'mmbo_p10' => $this->to_float($mmbo_p10),
                'mmstb_p90' => $this->to_float($mmstb_p90),
                'mmstb_p50' => $this->to_float($mmstb_p50),
                'mmstb_p10' => $this->to_float($mmstb_p10),
                'bcf_p90' => $this->to_float($bcf_p90, 4),
                'bcf_p50' => $this->to_float($bcf_p50, 4),
                'bcf_p10' => $this->to_float($bcf_p10, 4),
                'bscf_p90' => $this->to_float($bscf_p90, 4),
                'bscf_p50' => $this->to_float($bscf_p50, 4),
                'bscf_p10' => $this->to_float($bscf_p10, 4),
                'boi_p90' => $boi_p90,
                'boi_p50' => $boi_p50,
                'boi_p10' => $boi_p10,
                'bgi_p90' => $bgi_p90,
                'bgi_p50' => $bgi_p50,
                'bgi_p10' => $bgi_p10,
                'gcf_val' => $gcf_dr,
            );

        }

        return $postdrill;

    }

    public function printA3Discovery($w)
    {
        $discovery = array();

        $a3_dc = $this->qGet("
            SELECT
                b.basin_name,
                pr.structure_name,
                pr.prospect_id,
                g.gcf_trap_type trap_type,

                " . $this->_survey_sql . "

            FROM
                rsc_prospect pr,
                rsc_play pl,
                rsc_gcf g,
                adm_basin b
            WHERE
                pr.play_id=pl.play_id AND g.gcf_id=pr.gcf_id
                AND b.basin_id=pl.basin_id
                AND pr.prospect_is_deleted='0'
                AND pl.wk_id='{$w}'
                AND pr.prospect_type='discovery'
        ");

        $discovery_channel = array();
        $pai = 0;
        foreach ($a3_dc as $key => $val) {
            $pai++;
            $discovery_channel[$pai] = array(
                'basin_name' => $val['basin_name'],
                'structure_name' => $val['structure_name'],
                'trap_type' => $val['trap_type'],
                'area' => $this->area_check(array_slice($val, 4, 24)),
                'net' => $this->net_check(array_slice($val, 28, 6)),
                'well' => array(),
            );

            $wl = $this->qGet("
                SELECT
                    wl_name well_name,
                    wl_formation formation_name,
                    wl_id
                FROM
                    rsc_well
                WHERE
                    wk_id='{$w}'
                    AND prospect_id='{$val['prospect_id']}'
            ");

            $wai = 0;
            foreach ($wl as $wkey => $wval) {
                $wai++;
                $discovery_channel[$pai]['well'][$wai] = array(
                    'well_name' => $wval['well_name'],
                    'formation_name' => $wval['formation_name'],
                    'wl_id' => $wval['wl_id'],
                    'por_p90' => array(),
                    'por_p50' => array(),
                    'por_p10' => array(),
                    'sat_p90' => array(),
                    'sat_p50' => array(),
                    'sat_p10' => array(),
                    'ooip_p90' => 0,
                    'ooip_p50' => 0,
                    'ooip_p10' => 0,
                    'avg_bo' => 0,
                    'stoip_p90' => 0,
                    'stoip_p50' => 0,
                    'stoip_p10' => 0,
                    'avg_re_oil' => 0,
                    'p1_oil' => 0,
                    '2p_oil' => 0,
                    'c1_oil' => 0,
                    'c2_oil' => 0,
                    'c3_oil' => 0,
                    'ogip_p90' => 0,
                    'ogip_p50' => 0,
                    'ogip_p10' => 0,
                    'avg_bg' => 0,
                    'igip_p90' => 0,
                    'igip_p50' => 0,
                    'igip_p10' => 0,
                    'avg_re_gas' => 0,
                    'p1_gas' => 0,
                    '2p_gas' => 0,
                    'c1_gas' => 0,
                    'c2_gas' => 0,
                    'c3_gas' => 0,
                    'used_zone' => 0,
                    'zone' => array(),
                );

                $wz = $this->qGet("
                    SELECT
                        wz.zone_name,
                        wz.zone_clastic_p90_thickness cl_gross_p90,
                        wz.zone_clastic_p50_thickness cl_gross_p50,
                        wz.zone_clastic_p10_thickness cl_gross_p10,
                        wz.zone_clastic_p90_net cl_netgross_p90,
                        wz.zone_clastic_p50_net cl_netgross_p50,
                        wz.zone_clastic_p10_net cl_netgross_p10,
                        wz.zone_carbo_p90_thickness cr_gross_p90,
                        wz.zone_carbo_p50_thickness cr_gross_p50,
                        wz.zone_carbo_p10_thickness cr_gross_p10,
                        wz.zone_carbo_p90_net cr_netgross_p90,
                        wz.zone_carbo_p50_net cr_netgross_p50,
                        wz.zone_carbo_p10_net cr_netgross_p10,

                        IFNULL(wz.zone_clastic_p90_por, 0) cl_por_p90,
                        IFNULL(wz.zone_clastic_p50_por, 0) cl_por_p50,
                        IFNULL(wz.zone_clastic_p10_por, 0) cl_por_p10,
                        IFNULL(wz.zone_carbo_p90_por, 0) cr_por_p90,
                        IFNULL(wz.zone_carbo_p50_por, 0) cr_por_p50,
                        IFNULL(wz.zone_carbo_p10_por, 0) cr_por_p10,

                        IFNULL(wz.zone_clastic_p90_satur, 0) cl_sat_p90,
                        IFNULL(wz.zone_clastic_p50_satur, 0) cl_sat_p50,
                        IFNULL(wz.zone_clastic_p10_satur, 0) cl_sat_p10,
                        IFNULL(wz.zone_carbo_p90_satur, 0) cr_sat_p90,
                        IFNULL(wz.zone_carbo_p50_satur, 0) cr_sat_p50,
                        IFNULL(wz.zone_carbo_p10_satur, 0) cr_sat_p10,

                        IFNULL(IF(wz.zone_fvf_oil_p50 = 0, NULL, wz.zone_fvf_oil_p50), 1.22) boi_p50,
                        IFNULL(IF(wz.zone_fvf_gas_p50 = 0, NULL, wz.zone_fvf_gas_p50), 0.0063) bgi_p50,

                        IFNULL(IF(wz.zone_prod_res_radius = 0, NULL, wz.zone_prod_res_radius), 820) re_oil,
                        IFNULL(IF(wz.zone_prod_res_radius = 0, NULL, wz.zone_prod_res_radius), 2460) re_gas,
                        wz.zone_result
                    FROM
                        rsc_wellzone wz
                    WHERE
                        wz.wl_id='{$wval['wl_id']}'
                ");

                $zai = 0;
                foreach ($wz as $zkey => $zval) {
                    $zai++;
                    $clcr = $this->clastic_check(array_slice($zval, 13, 6), array_slice($zval, 19, 6));

                    /**
                     * `re_oil` and `re_gas` become `radius_re` (Radius of Investigation)
                     * but we check whether `zone_result` is Oil or Gas.
                     *
                     * the "else" from the following structure, become a failsafe if
                     * `zone_result` from database is NULL.
                     *
                     * Radius of Investigation already determined from query above ($wz)
                     */

                    if ($zval['zone_result'] == 'Oil') {
                        $radius_re = $zval['re_oil'];
                    } else {
                        // Failsafe if
                        $radius_re = $zval['re_gas'];
                    }

                    $pone_oil = $this->pone_oil_calc($radius_re, $discovery_channel[$pai]['net']['p50'],
                        $clcr['por_p50'], $clcr['sat_p50'], $zval['boi_p50']);

                    $twop_oil = $this->twop_oil_calc($radius_re, $discovery_channel[$pai]['net']['p50'],
                        $clcr['por_p50'], $clcr['sat_p50'], $zval['boi_p50']);

                    $pone_gas = $this->pone_gas_calc($radius_re, $discovery_channel[$pai]['net']['p50'],
                        $clcr['por_p50'], $clcr['sat_p50'], $zval['boi_p50']);

                    $twop_gas = $this->twop_gas_calc($radius_re, $discovery_channel[$pai]['net']['p50'],
                        $clcr['por_p50'], $clcr['sat_p50'], $zval['boi_p50']);

                    if ($clcr['no_avail'] == 0) {
                        $discovery_channel[$pai]['well'][$wai]['por_p90'][] = $clcr['por_p90'];
                        $discovery_channel[$pai]['well'][$wai]['por_p50'][] = $clcr['por_p50'];
                        $discovery_channel[$pai]['well'][$wai]['por_p10'][] = $clcr['por_p10'];
                        $discovery_channel[$pai]['well'][$wai]['sat_p90'][] = $clcr['sat_p90'];
                        $discovery_channel[$pai]['well'][$wai]['sat_p50'][] = $clcr['sat_p50'];
                        $discovery_channel[$pai]['well'][$wai]['sat_p10'][] = $clcr['sat_p10'];

                        $discovery_channel[$pai]['well'][$wai]['p1_oil'] += $pone_oil;
                        $discovery_channel[$pai]['well'][$wai]['2p_oil'] += $twop_oil;
                        $discovery_channel[$pai]['well'][$wai]['p1_gas'] += $pone_gas;
                        $discovery_channel[$pai]['well'][$wai]['2p_gas'] += $twop_gas;

                        $discovery_channel[$pai]['well'][$wai]['avg_bo'] += $zval['boi_p50'];
                        $discovery_channel[$pai]['well'][$wai]['avg_bg'] += $zval['bgi_p50'];

                        $discovery_channel[$pai]['well'][$wai]['used_zone']++;
                    }

                    $discovery_channel[$pai]['well'][$wai]['zone'][$zai] = array(
                        'zone_name' => $zval['zone_name'],
                        'no_avail' => $clcr['no_avail'],
                        'por_p90' => $clcr['por_p90'],
                        'por_p50' => $clcr['por_p50'],
                        'por_p10' => $clcr['por_p10'],
                        'sat_p90' => $clcr['sat_p90'],
                        'sat_p50' => $clcr['sat_p50'],
                        'sat_p10' => $clcr['sat_p10'],
                        'boi_p50' => $zval['boi_p50'],
                        'bgi_p50' => $zval['bgi_p50'],
                        'radius_re' => $radius_re,
                    );
                }
            }
        }

        $pai = 0;
        foreach ($discovery_channel as $key => $val) {
            $pai++;
            $wai = 0;
            foreach ($val['well'] as $wkey => $wval) {
                $wai++;
                $used_zone = ($wval['used_zone'] == 0 ? -1 : $wval['used_zone']);
                $por_p90 = $discovery_channel[$pai]['well'][$wai]['por_p90'];
                $por_p50 = $discovery_channel[$pai]['well'][$wai]['por_p50'];
                $por_p10 = $discovery_channel[$pai]['well'][$wai]['por_p10'];
                $sat_p90 = $discovery_channel[$pai]['well'][$wai]['sat_p90'];
                $sat_p50 = $discovery_channel[$pai]['well'][$wai]['sat_p50'];
                $sat_p10 = $discovery_channel[$pai]['well'][$wai]['sat_p10'];

                $avg_bo = $wval['avg_bo'] / $used_zone;
                $avg_bg = $wval['avg_bg'] / $used_zone;

                if ($avg_bo == 0) {
                    $avg_bo = 1.22;
                }

                if ($avg_bg == 0) {
                    $avg_bg = 0.0063;
                }

                $avg_re_oil = $wval['avg_re_oil'] / $used_zone;
                $avg_re_gas = $wval['avg_re_gas'] / $used_zone;

                $area_p90 = $discovery_channel[$pai]['area']['p90'];
                $area_p50 = $discovery_channel[$pai]['area']['p50'];
                $area_p10 = $discovery_channel[$pai]['area']['p10'];
                $net_p90 = $discovery_channel[$pai]['net']['p90'];
                $net_p50 = $discovery_channel[$pai]['net']['p50'];
                $net_p10 = $discovery_channel[$pai]['net']['p10'];
                $por_p90 = (!empty($por_p90) ? $this->smin($por_p90) : 0);
                $por_p50 = (!empty($por_p50) ? $this->average($por_p50) : 0);
                $por_p10 = (!empty($por_p10) ? $this->smax($por_p10) : 0);
                $sat_p90 = (!empty($sat_p90) ? $this->smin($sat_p90) : 0);
                $sat_p50 = (!empty($sat_p50) ? $this->average($sat_p50) : 0);
                $sat_p10 = (!empty($sat_p10) ? $this->smax($sat_p10) : 0);

                $ooip_p90 = $this->ooip_calc($area_p90, $net_p90, $por_p90, $sat_p90);
                $ooip_p50 = $this->ooip_calc($area_p50, $net_p50, $por_p50, $sat_p50);
                $ooip_p10 = $this->ooip_calc($area_p10, $net_p10, $por_p10, $sat_p10);

                $stoip_p90 = $this->stoip_calc($area_p90, $net_p90, $por_p90, $sat_p90, $avg_bo);
                $stoip_p50 = $this->stoip_calc($area_p50, $net_p50, $por_p50, $sat_p50, $avg_bo);
                $stoip_p10 = $this->stoip_calc($area_p10, $net_p10, $por_p10, $sat_p10, $avg_bo);

                $oil_p1 = $discovery_channel[$pai]['well'][$wai]['p1_oil'];
                $oil_2p = $discovery_channel[$pai]['well'][$wai]['2p_oil'];

                $oil_c1 = ($stoip_p90 - $oil_2p < 0 ? 0 : $stoip_p90 - $oil_2p);
                $oil_c2 = ($stoip_p50 - $oil_2p < 0 ? 0 : $stoip_p50 - $oil_2p);
                $oil_c3 = ($stoip_p10 - $oil_2p < 0 ? 0 : $stoip_p10 - $oil_2p);

                $ogip_p90 = $this->ogip_calc($area_p90, $net_p90, $por_p90, $sat_p90);
                $ogip_p50 = $this->ogip_calc($area_p50, $net_p50, $por_p50, $sat_p50);
                $ogip_p10 = $this->ogip_calc($area_p10, $net_p10, $por_p10, $sat_p10);
                $igip_p90 = $this->igip_calc($area_p90, $net_p90, $por_p90, $sat_p90, $avg_bg);
                $igip_p50 = $this->igip_calc($area_p50, $net_p50, $por_p50, $sat_p50, $avg_bg);
                $igip_p10 = $this->igip_calc($area_p10, $net_p10, $por_p10, $sat_p10, $avg_bg);

                $gas_p1 = $discovery_channel[$pai]['well'][$wai]['p1_gas'];
                $gas_2p = $discovery_channel[$pai]['well'][$wai]['2p_gas'];

                $gas_c1 = ($igip_p90 - $gas_2p < 0 ? 0 : $igip_p90 - $gas_2p);
                $gas_c2 = ($igip_p50 - $gas_2p < 0 ? 0 : $igip_p50 - $gas_2p);
                $gas_c3 = ($igip_p10 - $gas_2p < 0 ? 0 : $igip_p10 - $gas_2p);

                // Insert to discovery list
                $discovery_channel[$pai]['well'][$wai]['avg_bo'] = $avg_bo;
                $discovery_channel[$pai]['well'][$wai]['avg_bg'] = $avg_bg;
                $discovery_channel[$pai]['well'][$wai]['avg_re_oil'] = $avg_re_oil;
                $discovery_channel[$pai]['well'][$wai]['avg_re_gas'] = $avg_re_gas;

                // Oil
                $discovery_channel[$pai]['well'][$wai]['ooip_p90'] = $this->to_float($ooip_p90);
                $discovery_channel[$pai]['well'][$wai]['ooip_p50'] = $this->to_float($ooip_p50);
                $discovery_channel[$pai]['well'][$wai]['ooip_p10'] = $this->to_float($ooip_p10);
                $discovery_channel[$pai]['well'][$wai]['stoip_p90'] = $this->to_float($stoip_p90);
                $discovery_channel[$pai]['well'][$wai]['stoip_p50'] = $this->to_float($stoip_p50);
                $discovery_channel[$pai]['well'][$wai]['stoip_p10'] = $this->to_float($stoip_p10);

                $discovery_channel[$pai]['well'][$wai]['p1_oil'] = $this->to_float($oil_p1);
                $discovery_channel[$pai]['well'][$wai]['2p_oil'] = $this->to_float($oil_2p);
                $discovery_channel[$pai]['well'][$wai]['c1_oil'] = $this->to_float($oil_c1);
                $discovery_channel[$pai]['well'][$wai]['c2_oil'] = $this->to_float($oil_c2);
                $discovery_channel[$pai]['well'][$wai]['c3_oil'] = $this->to_float($oil_c3);

                // Gas
                $discovery_channel[$pai]['well'][$wai]['ogip_p90'] = $this->to_float($ogip_p90, 4);
                $discovery_channel[$pai]['well'][$wai]['ogip_p50'] = $this->to_float($ogip_p50, 4);
                $discovery_channel[$pai]['well'][$wai]['ogip_p10'] = $this->to_float($ogip_p10, 4);
                $discovery_channel[$pai]['well'][$wai]['igip_p90'] = $this->to_float($igip_p90, 4);
                $discovery_channel[$pai]['well'][$wai]['igip_p50'] = $this->to_float($igip_p50, 4);
                $discovery_channel[$pai]['well'][$wai]['igip_p10'] = $this->to_float($igip_p10, 4);

                $discovery_channel[$pai]['well'][$wai]['p1_gas'] = $this->to_float($gas_p1, 4);
                $discovery_channel[$pai]['well'][$wai]['2p_gas'] = $this->to_float($gas_2p, 4);
                $discovery_channel[$pai]['well'][$wai]['c1_gas'] = $this->to_float($gas_c1, 4);
                $discovery_channel[$pai]['well'][$wai]['c2_gas'] = $this->to_float($gas_c2, 4);
                $discovery_channel[$pai]['well'][$wai]['c3_gas'] = $this->to_float($gas_c3, 4);
            }
        }

        return $discovery_channel;
    }

    public function gcfCalc($d, $stage)
    {

        if ($d['sr_ker'] == 'IV' || $d['sr_mat'] == 'Immature') {
            $is_sr_low = 1;
        } else {
            $is_sr_low = 0;
        }

        $ker = 0.15;
        $toc = 0.3;
        $hfu = 0.07;
        $mat = 0.45;
        $otr = 0.03;

        $dis = 0.2;
        $lit = 0.2;
        $pri = 0.4;
        $sec = 0.2;

        $sdi = 0.35;
        $scn = 0.25;
        $stp = 0.2;
        $geo = 0.1;
        $trp = 0.1;

        $kit = 0.4;
        $tec = 0.1;
        $prv = 0.25;
        $mig = 0.25;

        $sr_ker = array(
            'I' => 1.0,
            'I/II' => 0.9,
            'II' => 0.8,
            'II/III' => 0.7,
            'III' => 0.6,
            'IV' => 0.1,
            'Unknown' => 0.5);

        $sr_toc = array(
            '0 - 0.5' => 0.2,
            '0.6 - 1.0' => 0.6,
            '1.1 - 2.0' => 0.7,
            '2.1 - 4.0' => 0.8,
            '> 4' => 1.0,
            'Unknown' => 0.5);

        $sr_hfu = array(
            '<= 1.0' => 0.5,
            '1.1 - 2.0' => 0.7,
            '2.1 - 3.0' => 0.9,
            '> 3.0' => 1.0,
            'Unknown' => 0.5);

        $sr_mat = array(
            'Immature' => 0.1,
            'Early Mature' => 0.4,
            'Mature' => 1.0,
            'Over Mature' => 0.8,
            'Unknown' => 0.5);

        $sr_otr = array(
            'Yes' => 1.0,
            'No' => 0.0,
            'Unknown' => 0.5);

        $re_dis = array(
            'Single Distribution' => 1.0,
            'Double Distribution' => 0.8,
            'Multiple Distribution' => 0.7,
            'Unknown' => 0.5);

        $re_lit = array(
            'Siltstone' => 0.5,
            'Sandstone' => 1.0,
            'Reef Carbonate' => 1.0,
            'Platform Carbonate' => 0.5,
            'Coal' => 0.6,
            'Alternate Rocks' => 0.5,
            'Naturally Fractured' => 0.7,
            'Others' => 0.3,
            'Unknown' => 0.5);

        $re_sec = array(
            'Vugs Porosity' => 0.4,
            'Fracture Porosity' => 0.9,
            'Integrated Porosity' => 1.0,
            'None' => 0.0,
            'Unknown' => 0.5);

        $re_pri = array(
            '0 - 10' => 0.4,
            '11 - 15' => 0.6,
            '16 - 20' => 0.7,
            '21 - 30' => 0.9,
            '> 30' => 1.0,
            'Unknown' => 0.5);

        $tr_sdi = array(
            'Single Distribution Impermeable Rocks' => 0.6,
            'Double Distribution Impermeable Rocks' => 0.8,
            'Multiple Distribution Impermeable Rocks' => 1.0,
            'Unknown' => 0.5);

        $tr_scn = array(
            'Tank' => 0.4,
            'Truncated' => 0.5,
            'Limited Spread' => 0.7,
            'Spread' => 0.9,
            'Extensive Spread' => 1.0,
            'Unknown' => 0.5);

        $tr_stp = array(
            'Primary' => 0.8,
            'Diagenetic' => 0.9,
            'Alternate Tectonic Mech' => 1.0,
            'Unknown' => 0.5);

        $tr_geo = array(
            'Anticline Hangingwall' => 0.9,
            'Anticline Footwall' => 1.0,
            'Anticline Pericline' => 0.9,
            'Anticline Dome' => 0.9,
            'Anticline Nose' => 0.9,
            'Anticline Unqualified' => 0.9,
            'Faulted Anticline Hangingwall' => 0.8,
            'Faulted Anticline Footwall' => 0.8,
            'Faulted Anticline Pericline' => 0.8,
            'Faulted Anticline Dome' => 0.8,
            'Faulted Anticline Nose' => 0.8,
            'Faulted Anticline Unqualified' => 0.8,
            'Tilted Hangingwall' => 0.7,
            'Tilted Fault Block Footwall' => 0.7,
            'Tilted Fault Block Terrace' => 0.7,
            'Tilted Fault Block Unqualified' => 0.7,
            'Horst Simple' => 0.6,
            'Horst Faulted' => 0.6,
            'Horst Complex' => 0.6,
            'Horst Unqualified' => 0.6,
            'Wedge Tilted' => 0.5,
            'Wedge Flexured' => 0.5,
            'Wedge Radial' => 0.5,
            'Wedge Marginal' => 0.5,
            'Wedge Faulted' => 0.5,
            'Wedge Onlap' => 0.5,
            'Wedge Subcrop' => 0.5,
            'Wedge Unqualified' => 0.5,
            'Abutment Hangingwall' => 0.4,
            'Abutment Footwall' => 0.4,
            'Abutment Pericline' => 0.4,
            'Abutment Terrace' => 0.4,
            'Abutment Complex' => 0.4,
            'Abutment Unqualified' => 0.4,
            'Irregular Hangingwall' => 0.4,
            'Irregular Footwall' => 0.4,
            'Irregular Pericline' => 0.4,
            'Irregular Dome' => 0.4,
            'Irregular Terrace' => 0.4,
            'Irregular Unqualified' => 0.4,
            'Unknown' => 0.5);

        $tr_trp = array(
            'Structural Tectonic Extensional' => 0.6,
            'Structural Tectonic Compressional Thin Skinned' => 0.9,
            'Structural Tectonic Compressional Inverted' => 1.0,
            'Structural Tectonic Strike Slip' => 1.0,
            'Structural Tectonic Unqualified' => 0.8,
            'Structural Drape' => 0.8,
            'Structural Compactional' => 0.8,
            'Structural Diapiric Salt' => 0.7,
            'Structural Diapiric Mud' => 0.7,
            'Structural Diapiric Unqualified' => 0.7,
            'Structural Gravitational' => 0.6,
            'Structural Unqualified' => 0.6,
            'Stratigraphic Depositional Pinch-out' => 0.6,
            'Stratigraphic Depositional Shale-out' => 0.6,
            'Stratigraphic Depositional Facies-limited' => 0.6,
            'Stratigraphic Depositional Reef' => 1.0,
            'Stratigraphic Depositional Unqualified' => 0.6,
            'Stratigraphic Unconformity Subcrop' => 0.6,
            'Stratigraphic Unconformity Onlap' => 0.6,
            'Stratigraphic Unconformity Unqualified' => 0.6,
            'Stratigraphic Other Diagenetic' => 0.6,
            'Stratigraphic Other Tar Mat' => 0.6,
            'Stratigraphic Other Gas Hydrate' => 0.6,
            'Stratigraphic Other Gas Permafrosh' => 0.6,
            'Stratigraphic Unqualified' => 0.6,
            'Unknown' => 0.5);

        $dn_kit = array(
            'Very Near (0 - 2 Km)' => 1.0,
            'Near (2 - 5 Km)' => 0.9,
            'Middle (5 - 10 Km)' => 0.8,
            'Long (10 - 20 Km)' => 0.7,
            'Very Long (> 20 Km)' => 0.5,
            'Unknown' => 0.5);

        $dn_tec = array(
            'Single Order' => 0.7,
            'Multiple Order' => 1.0,
            'Unknown' => 0.5);

        $dn_prv = array(
            'Not Occur' => 0.2,
            'Occur' => 1.0,
            'Unknown' => 0.5);

        $dn_mig = array(
            'Vertical' => 0.7,
            'Horizontal' => 0.8,
            'Multiple Directions' => 1.0,
            'Unknown' => 0.5);

        foreach (array_slice($d, 4) as $key => $val) {
            if (in_array($val, array_keys(${$key}))) {
                if ($d[substr($key, 0, 2)] == 'Proven') {
                    $d[$key] = ${$key}[$val] * ${substr($key, 3)};
                } else {
                    $d[$key] = ((0.4 * ${$key}[$val]) + 0.3) * ${substr($key, 3)};
                }
            } else {
                $d[$key] = 0.5 * ${substr($key, 3)};
            }
        }

        $gcf = array();
        if ($is_sr_low == 1) {
            $gcf['sr'] = 0.125;
        } else {
            $gcf['sr'] = array_sum(array_slice($d, 4, 5));
        }
        $gcf['re'] = array_sum(array_slice($d, 9, 4));
        $gcf['tr'] = array_sum(array_slice($d, 13, 5));
        $gcf['dn'] = array_sum(array_slice($d, 18, 4));
        $gcf['rda'] = $gcf['sr'] * $gcf['re'] * $gcf['tr'] * $gcf['dn'];

        if ($stage == 'play') {
            $gcf['rda_final'] = $this->to_float((($gcf['rda'] / 100) * 0.2) * 100);
        } elseif ($stage == 'lead') {
            $gcf['rda_final'] = $this->to_float((($gcf['rda'] / 100) * 0.5 + 0.1) * 100);
        } elseif ($stage == 'drillable') {
            $gcf['rda_final'] = $this->to_float((($gcf['rda'] / 100) * 0.65 + 0.3) * 100);
        } elseif ($stage == 'postdrill') {
            $gcf['rda_final'] = $this->to_float((($gcf['rda'] / 100) * 0.7 + 0.3) * 100);
        }

        $gcf['rda'] = $this->to_float($gcf['rda'] * 100);

        return $gcf;
    }

    public function playDetail($playId = null)
    {
        $plays = Yii::app()->db->createCommand()
            ->select([
                'basin.basin_name',
                'province.province_name',
                'kkks.kkks_name',
                'wk.wk_id',
                'wk.wk_name',

                'CONCAT(gcf.gcf_res_lithology, " - ",
                    gcf.gcf_res_formation_serie, " ",
                    gcf.gcf_res_formation, " - ",
                    gcf.gcf_res_age_serie, " ",
                    gcf.gcf_res_age_system, " - ",
                    gcf.gcf_res_depos_env, " - ",
                    gcf.gcf_trap_type) play_name',
                'play.play_analog_to',
                'play.play_analog_distance',
                'play.play_exr_method',
                'play.play_shore',
                'play.play_terrain',
                'play.play_near_field',
                'play.play_near_infra_structure',
                'play.play_support_data',
                'play.play_outcrop_distance',
                'play.play_s2d_year',
                'play.play_s2d_crossline',
                'play.play_s2d_line_intervall',
                'play.play_s2d_img_quality',
                'play.play_sgc_sample',
                'play.play_sgc_depth',
                'play.play_sgv_acre',
                'play.play_sgv_depth',
                'play.play_sel_acre',
                'play.play_sel_depth',
                'play.play_srt_acre',
                'play.play_map_scale',
                'play.play_map_author',
                'play.play_map_year',
                'play.play_remark',

                'gcf.gcf_is_sr',
                'gcf.gcf_sr_age_system',
                'gcf.gcf_sr_age_serie',
                'gcf.gcf_sr_formation',
                'gcf.gcf_sr_formation_serie',
                'gcf.gcf_sr_kerogen',
                'gcf.gcf_sr_toc',
                'gcf.gcf_sr_hfu',
                'gcf.gcf_sr_distribution',
                'gcf.gcf_sr_continuity',
                'gcf.gcf_sr_maturity',
                'gcf.gcf_sr_otr',
                'gcf.gcf_sr_remark',
                'gcf.gcf_is_res',
                'gcf.gcf_res_age_system',
                'gcf.gcf_res_age_serie',
                'gcf.gcf_res_formation',
                'gcf.gcf_res_formation_serie',
                'gcf.gcf_res_depos_set',
                'gcf.gcf_res_depos_env',
                'gcf.gcf_res_distribution',
                'gcf.gcf_res_continuity',
                'gcf.gcf_res_lithology',
                'gcf.gcf_res_por_primary',
                'gcf.gcf_res_por_secondary',
                'gcf.gcf_res_remark',
                'gcf.gcf_is_trap',
                'gcf.gcf_trap_seal_age_system',
                'gcf.gcf_trap_seal_age_serie',
                'gcf.gcf_trap_seal_formation',
                'gcf.gcf_trap_seal_formation_serie',
                'gcf.gcf_trap_seal_distribution',
                'gcf.gcf_trap_seal_continuity',
                'gcf.gcf_trap_seal_type',
                'gcf.gcf_trap_age_system',
                'gcf.gcf_trap_age_serie',
                'gcf.gcf_trap_geometry',
                'gcf.gcf_trap_type',
                'gcf.gcf_trap_closure',
                'gcf.gcf_trap_remark',
                'gcf.gcf_is_dyn',
                'gcf.gcf_dyn_migration',
                'gcf.gcf_dyn_kitchen',
                'gcf.gcf_dyn_petroleum',
                'gcf.gcf_dyn_early_age_system',
                'gcf.gcf_dyn_early_age_serie',
                'gcf.gcf_dyn_late_age_system',
                'gcf.gcf_dyn_late_age_serie',
                'gcf.gcf_dyn_preservation',
                'gcf.gcf_dyn_pathways',
                'gcf.gcf_dyn_migration_age_system',
                'gcf.gcf_dyn_migration_age_serie',
                'gcf.gcf_dyn_remark',
            ])
            ->from('rsc_play play')
            ->leftJoin('rsc_gcf gcf', 'play.gcf_id=gcf.gcf_id')
            ->leftJoin('adm_basin basin', 'play.basin_id=basin.basin_id')
            ->leftJoin('adm_province province', 'play.province_id=province.province_id')
            ->leftJoin('adm_working_area wk', 'wk.wk_id = play.wk_id')
            ->leftJoin('adm_psc psc', 'psc.wk_id = wk.wk_id')
            ->leftJoin('adm_kkks kkks', 'kkks.psc_id = psc.psc_id')
            ->where('play.wk_id <> "WK0000"')
            ->andWhere('play.wk_id <> "WK9999"');

        if ($playId) {
            $plays->andWhere('play.play_id = :playId', [':playId' => $playId]);
            return $plays->queryRow();
        }

        return $plays->queryAll();
    }

    public function leadDetail($leadId = null)
    {
        $leads = Yii::app()->db->createCommand()
            ->select([
                'basin.basin_name',
                'province.province_name',
                'kkks.kkks_name',
                'wk.wk_id',
                'wk.wk_name',

                'lead.structure_name',
                'lead.lead_clarified',
                'lead.lead_year_study',
                'lead.lead_date_initiate',
                'lead.lead_latitude',
                'lead.lead_longitude',
                'lead.lead_shore',
                'lead.lead_terrain',
                'lead.lead_near_field',
                'lead.lead_near_infra_structure',

                'pgcf.gcf_res_lithology plithology',
                'pgcf.gcf_res_formation_serie pformation_serie',
                'pgcf.gcf_res_formation pformation',
                'pgcf.gcf_res_age_serie page_serie',
                'pgcf.gcf_res_age_system page',
                'pgcf.gcf_res_depos_env penv',
                'pgcf.gcf_trap_type ptrap',

                's2.ls2d_vintage_number',
                's2.ls2d_year_survey',
                's2.ls2d_total_crossline',
                's2.ls2d_total_coverage',
                's2.ls2d_average_interval',
                's2.ls2d_year_late_process',
                's2.ls2d_late_method',
                's2.ls2d_img_quality',
                's2.ls2d_low_estimate',
                's2.ls2d_best_estimate',
                's2.ls2d_hight_estimate',
                's2.ls2d_remark',

                'gl.lsgf_year_survey',
                'gl.lsgf_survey_method',
                'gl.lsgf_coverage_area',
                'gl.lsgf_low_estimate',
                'gl.lsgf_best_estimate',
                'gl.lsgf_high_estimate',
                'gl.lsgf_remark',

                'gh.lsgc_year_survey',
                'gh.lsgc_range_interval',
                'gh.lsgc_number_sample',
                'gh.lsgc_number_rock',
                'gh.lsgc_number_fluid',
                'gh.lsgc_hc_availability',
                'gh.lsgc_hc_composition',
                'gh.lsgc_year_report',
                'gh.lsgc_low_estimate',
                'gh.lsgc_high_estimate',
                'gh.lsgc_best_estimate',
                'gh.lsgc_remark',

                'gr.lsgv_year_survey',
                'gr.lsgv_survey_method',
                'gr.lsgv_coverage_area',
                'gr.lsgv_range_penetration',
                'gr.lsgv_spacing_interval',
                'gr.lsgv_low_estimate',
                'gr.lsgv_best_estimate',
                'gr.lsgv_high_estimate',
                'gr.lsgv_remark',

                'el.lsel_year_survey',
                'el.lsel_survey_method',
                'el.lsel_coverage_area',
                'el.lsel_range_penetration',
                'el.lsel_spacing_interval',
                'el.lsel_low_estimate',
                'el.lsel_best_estimate',
                'el.lsel_high_estimate',
                'el.lsel_remark',

                'rs.lsrt_year_survey',
                'rs.lsrt_survey_method',
                'rs.lsrt_coverage_area',
                'rs.lsrt_range_penetration',
                'rs.lsrt_spacing_interval',
                'rs.lsrt_low_estimate',
                'rs.lsrt_best_estimate',
                'rs.lsrt_high_estimate',
                'rs.lsrt_remark',

                'ot.lsor_year_survey',
                'ot.lsor_low_estimate',
                'ot.lsor_best_estimate',
                'ot.lsor_high_estimate',
                'ot.lsor_remark',

                'gcf.gcf_is_sr',
                'gcf.gcf_sr_age_system',
                'gcf.gcf_sr_age_serie',
                'gcf.gcf_sr_formation',
                'gcf.gcf_sr_formation_serie',
                'gcf.gcf_sr_kerogen',
                'gcf.gcf_sr_toc',
                'gcf.gcf_sr_hfu',
                'gcf.gcf_sr_distribution',
                'gcf.gcf_sr_continuity',
                'gcf.gcf_sr_maturity',
                'gcf.gcf_sr_otr',
                'gcf.gcf_sr_remark',
                'gcf.gcf_is_res',
                'gcf.gcf_res_age_system',
                'gcf.gcf_res_age_serie',
                'gcf.gcf_res_formation',
                'gcf.gcf_res_formation_serie',
                'gcf.gcf_res_depos_set',
                'gcf.gcf_res_depos_env',
                'gcf.gcf_res_distribution',
                'gcf.gcf_res_continuity',
                'gcf.gcf_res_lithology',
                'gcf.gcf_res_por_primary',
                'gcf.gcf_res_por_secondary',
                'gcf.gcf_res_remark',
                'gcf.gcf_is_trap',
                'gcf.gcf_trap_seal_age_system',
                'gcf.gcf_trap_seal_age_serie',
                'gcf.gcf_trap_seal_formation',
                'gcf.gcf_trap_seal_formation_serie',
                'gcf.gcf_trap_seal_distribution',
                'gcf.gcf_trap_seal_continuity',
                'gcf.gcf_trap_seal_type',
                'gcf.gcf_trap_age_system',
                'gcf.gcf_trap_age_serie',
                'gcf.gcf_trap_geometry',
                'gcf.gcf_trap_type',
                'gcf.gcf_trap_closure',
                'gcf.gcf_trap_remark',
                'gcf.gcf_is_dyn',
                'gcf.gcf_dyn_migration',
                'gcf.gcf_dyn_kitchen',
                'gcf.gcf_dyn_petroleum',
                'gcf.gcf_dyn_early_age_system',
                'gcf.gcf_dyn_early_age_serie',
                'gcf.gcf_dyn_late_age_system',
                'gcf.gcf_dyn_late_age_serie',
                'gcf.gcf_dyn_preservation',
                'gcf.gcf_dyn_pathways',
                'gcf.gcf_dyn_migration_age_system',
                'gcf.gcf_dyn_migration_age_serie',
                'gcf.gcf_dyn_remark',
            ])
            ->from('rsc_lead lead')
            ->leftJoin('rsc_lead_2d s2', 's2.lead_id = lead.lead_id')
            ->leftJoin('rsc_lead_geological gl', 'gl.lead_id = lead.lead_id')
            ->leftJoin('rsc_lead_geochemistry gh', 'gh.lead_id = lead.lead_id')
            ->leftJoin('rsc_lead_gravity gr', 'gr.lead_id = lead.lead_id')
            ->leftJoin('rsc_lead_electromagnetic el', 'el.lead_id = lead.lead_id')
            ->leftJoin('rsc_lead_resistivity rs', 'rs.lead_id = lead.lead_id')
            ->leftJoin('rsc_lead_other ot', 'ot.lead_id = lead.lead_id')
            ->leftJoin('rsc_play play', 'play.play_id = lead.play_id')
            ->leftJoin('rsc_gcf gcf', 'gcf.gcf_id = lead.gcf_id')
            ->leftJoin('rsc_gcf pgcf', 'pgcf.gcf_id = play.gcf_id')
            ->leftJoin('adm_basin basin', 'basin.basin_id = play.basin_id')
            ->leftJoin('adm_province province', 'province.province_id = play.province_id')
            ->leftJoin('adm_working_area wk', 'wk.wk_id = play.wk_id')
            ->leftJoin('adm_psc psc', 'psc.wk_id = wk.wk_id')
            ->leftJoin('adm_kkks kkks', 'kkks.psc_id = psc.psc_id')
            ->where('play.wk_id <> "WK0000"')
            ->andWhere('play.wk_id <> "WK9999"')
            ->andWhere('lead.lead_is_deleted = 0');

        if ($leadId) {
            $leads->andWhere('lead.lead_id=:leadId', [':leadId' => $leadId]);
            return $leads->queryRow();
        }

        return $leads->queryAll();
    }

    public function drillableDetail($drillableId = null)
    {
        $drillable = Yii::app()->db->createCommand()
            ->select([
                'basin.basin_name',
                'province.province_name',
                'wk.wk_id',
                'wk.wk_name',

                'prospect.structure_name',

                'pgcf.gcf_res_lithology plithology',
                'pgcf.gcf_res_formation_serie pformation_serie',
                'pgcf.gcf_res_formation pformation',
                'pgcf.gcf_res_age_serie page_serie',
                'pgcf.gcf_res_age_system page',
                'pgcf.gcf_res_depos_env penv',
                'pgcf.gcf_trap_type ptrap',

                'prospect.prospect_clarified',
                'prospect.prospect_date_initiate',
                'prospect.prospect_latitude',
                'prospect.prospect_longitude',
                'prospect.prospect_shore',
                'prospect.prospect_terrain',
                'prospect.prospect_near_field',
                'prospect.prospect_near_infra_structure',

                'dr.dr_por_p90',
                'dr.dr_por_p50',
                'dr.dr_por_p10',
                'dr.dr_por_remark',
                'dr.dr_satur_p90',
                'dr.dr_satur_p50',
                'dr.dr_satur_p10',
                'dr.dr_satur_remark',

                's3.s3d_year_survey',
                's3.s3d_vintage_number',
                's3.s3d_bin_size',
                's3.s3d_coverage_area',
                's3.s3d_frequency',
                's3.s3d_frequency_lateral',
                's3.s3d_frequency_vertical',
                's3.s3d_year_late_process',
                's3.s3d_late_method',
                's3.s3d_img_quality',
                's3.s3d_top_depth_ft',
                's3.s3d_top_depth_ms',
                's3.s3d_bot_depth_ft',
                's3.s3d_bot_depth_ms',
                's3.s3d_depth_spill',
                's3.s3d_depth_estimate',
                's3.s3d_depth_low',
                's3.s3d_formation_thickness',
                's3.s3d_gross_thickness',
                's3.s3d_vol_p90_area',
                's3.s3d_vol_p50_area',
                's3.s3d_vol_p10_area',
                's3.s3d_net_p90_thickness',
                's3.s3d_net_p50_thickness',
                's3.s3d_net_p10_thickness',
                's3.s3d_net_p90_por',
                's3.s3d_net_p50_por',
                's3.s3d_net_p10_por',
                's3.s3d_net_p90_satur',
                's3.s3d_net_p50_satur',
                's3.s3d_net_p10_satur',
                's3.s3d_net_p90_vsh',
                's3.s3d_net_p50_vsh',
                's3.s3d_net_p10_vsh',
                's3.s3d_remark',

                's2.s2d_year_survey',
                's2.s2d_vintage_number',
                's2.s2d_total_crossline',
                's2.s2d_seismic_line',
                's2.s2d_average_interval',
                's2.s2d_record_length_ms',
                's2.s2d_record_length_ft',
                's2.s2d_year_late_process',
                's2.s2d_late_method',
                's2.s2d_img_quality',
                's2.s2d_top_depth_ft',
                's2.s2d_top_depth_ms',
                's2.s2d_bot_depth_ft',
                's2.s2d_bot_depth_ms',
                's2.s2d_depth_spill',
                's2.s2d_depth_estimate',
                's2.s2d_depth_low',
                's2.s2d_formation_thickness',
                's2.s2d_gross_thickness',
                's2.s2d_vol_p90_area',
                's2.s2d_vol_p50_area',
                's2.s2d_vol_p10_area',
                's2.s2d_net_p90_thickness',
                's2.s2d_net_p50_thickness',
                's2.s2d_net_p10_thickness',
                's2.s2d_net_p90_por',
                's2.s2d_net_p50_por',
                's2.s2d_net_p10_por',
                's2.s2d_net_p90_satur',
                's2.s2d_net_p50_satur',
                's2.s2d_net_p10_satur',
                's2.s2d_net_p90_vsh',
                's2.s2d_net_p50_vsh',
                's2.s2d_net_p10_vsh',
                's2.s2d_remark',

                'gl.sgf_year_survey',
                'gl.sgf_survey_method',
                'gl.sgf_coverage_area',
                'gl.sgf_p90_area',
                'gl.sgf_p50_area',
                'gl.sgf_p10_area',
                'gl.sgf_p90_thickness',
                'gl.sgf_p50_thickness',
                'gl.sgf_p10_thickness',
                'gl.sgf_p90_net',
                'gl.sgf_p50_net',
                'gl.sgf_p10_net',
                'gl.sgf_remark',

                'gh.sgc_year_survey',
                'gh.sgc_sample_interval',
                'gh.sgc_number_sample',
                'gh.sgc_number_rock',
                'gh.sgc_number_fluid',
                'gh.sgc_hc_composition',
                'gh.sgc_vol_p90_area',
                'gh.sgc_vol_p50_area',
                'gh.sgc_vol_p10_area',
                'gh.sgc_remark',

                'gr.sgv_year_survey',
                'gr.sgv_survey_method',
                'gr.sgv_coverage_area',
                'gr.sgv_depth_range',
                'gr.sgv_spacing_interval',
                'gr.sgv_depth_spill',
                'gr.sgv_depth_estimate',
                'gr.sgv_depth_low',
                'gr.sgv_res_thickness',
                'gr.sgv_res_top_depth',
                'gr.sgv_res_bot_depth',
                'gr.sgv_gross_thickness',
                'gr.sgv_gross_according',
                'gr.sgv_net_gross',
                'gr.sgv_vol_p90_area',
                'gr.sgv_vol_p50_area',
                'gr.sgv_vol_p10_area',
                'gr.sgv_remark',

                'el.sel_year_survey',
                'el.sel_survey_method',
                'el.sel_coverage_area',
                'el.sel_depth_range',
                'el.sel_spacing_interval',
                'el.sel_vol_p90_area',
                'el.sel_vol_p50_area',
                'el.sel_vol_p10_area',
                'el.sel_remark',

                'rs.rst_year_survey',
                'rs.rst_survey_method',
                'rs.rst_coverage_area',
                'rs.rst_depth_range',
                'rs.rst_spacing_interval',
                'rs.rst_vol_p90_area',
                'rs.rst_vol_p50_area',
                'rs.rst_vol_p10_area',
                'rs.rst_remark',

                'ot.sor_year_survey',
                'ot.sor_vol_p90_area',
                'ot.sor_vol_p50_area',
                'ot.sor_vol_p10_area',
                'ot.sor_remark',

                'gcf.gcf_is_sr',
                'gcf.gcf_sr_age_system',
                'gcf.gcf_sr_age_serie',
                'gcf.gcf_sr_formation',
                'gcf.gcf_sr_formation_serie',
                'gcf.gcf_sr_kerogen',
                'gcf.gcf_sr_toc',
                'gcf.gcf_sr_hfu',
                'gcf.gcf_sr_distribution',
                'gcf.gcf_sr_continuity',
                'gcf.gcf_sr_maturity',
                'gcf.gcf_sr_otr',
                'gcf.gcf_sr_remark',
                'gcf.gcf_is_res',
                'gcf.gcf_res_age_system',
                'gcf.gcf_res_age_serie',
                'gcf.gcf_res_formation',
                'gcf.gcf_res_formation_serie',
                'gcf.gcf_res_depos_set',
                'gcf.gcf_res_depos_env',
                'gcf.gcf_res_distribution',
                'gcf.gcf_res_continuity',
                'gcf.gcf_res_lithology',
                'gcf.gcf_res_por_primary',
                'gcf.gcf_res_por_secondary',
                'gcf.gcf_res_remark',
                'gcf.gcf_is_trap',
                'gcf.gcf_trap_seal_age_system',
                'gcf.gcf_trap_seal_age_serie',
                'gcf.gcf_trap_seal_formation',
                'gcf.gcf_trap_seal_formation_serie',
                'gcf.gcf_trap_seal_distribution',
                'gcf.gcf_trap_seal_continuity',
                'gcf.gcf_trap_seal_type',
                'gcf.gcf_trap_age_system',
                'gcf.gcf_trap_age_serie',
                'gcf.gcf_trap_geometry',
                'gcf.gcf_trap_type',
                'gcf.gcf_trap_closure',
                'gcf.gcf_trap_remark',
                'gcf.gcf_is_dyn',
                'gcf.gcf_dyn_migration',
                'gcf.gcf_dyn_kitchen',
                'gcf.gcf_dyn_petroleum',
                'gcf.gcf_dyn_early_age_system',
                'gcf.gcf_dyn_early_age_serie',
                'gcf.gcf_dyn_late_age_system',
                'gcf.gcf_dyn_late_age_serie',
                'gcf.gcf_dyn_preservation',
                'gcf.gcf_dyn_pathways',
                'gcf.gcf_dyn_migration_age_system',
                'gcf.gcf_dyn_migration_age_serie',
                'gcf.gcf_dyn_remark',
            ])
            ->from('rsc_prospect prospect')
            ->leftJoin('rsc_drillable dr', 'dr.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_seismic_3d s3', 's3.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_seismic_2d s2', 's2.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_geological gl', 'gl.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_geochemistry gh', 'gh.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_gravity gr', 'gr.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_electromagnetic el', 'el.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_resistivity rs', 'rs.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_other ot', 'ot.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_play play', 'play.play_id = prospect.play_id')
            ->leftJoin('rsc_gcf gcf', 'gcf.gcf_id = prospect.gcf_id')
            ->leftJoin('rsc_gcf pgcf', 'pgcf.gcf_id = play.gcf_id')
            ->leftJoin('adm_basin basin', 'basin.basin_id = play.basin_id')
            ->leftJoin('adm_province province', 'province.province_id = play.province_id')
            ->leftJoin('adm_working_area wk', 'wk.wk_id = play.wk_id')
            ->leftJoin('adm_psc psc', 'psc.wk_id = wk.wk_id')
            ->leftJoin('adm_kkks kkks', 'kkks.psc_id = psc.psc_id')
            ->where('play.wk_id <> "WK0000"')
            ->andWhere('play.wk_id <> "WK9999"')
            ->andWhere('prospect.prospect_is_deleted = 0')
            ->andWhere('prospect.prospect_type = "drillable"');

        if ($drillableId) {
            $drillable->andWhere('prospect.prospect_id=:prospectId', [':prospectId' => $drillableId]);
            return $drillable->queryRow();
        }

        return $drillable->queryAll();
    }

    public function postdrillDetail($postdrillId = null)
    {
        $postdrill = Yii::app()->db->createCommand()
            ->select([
                'basin.basin_name',
                'province.province_name',
                'wk.wk_id',
                'wk.wk_name',

                'prospect.structure_name',

                'pgcf.gcf_res_lithology plithology',
                'pgcf.gcf_res_formation_serie pformation_serie',
                'pgcf.gcf_res_formation pformation',
                'pgcf.gcf_res_age_serie page_serie',
                'pgcf.gcf_res_age_system page',
                'pgcf.gcf_res_depos_env penv',
                'pgcf.gcf_trap_type ptrap',

                'prospect.prospect_clarified',
                'prospect.prospect_date_initiate',
                'prospect.prospect_latitude',
                'prospect.prospect_longitude',
                'prospect.prospect_shore',
                'prospect.prospect_terrain',
                'prospect.prospect_near_field',
                'prospect.prospect_near_infra_structure',

                's3.s3d_year_survey',
                's3.s3d_vintage_number',
                's3.s3d_bin_size',
                's3.s3d_coverage_area',
                's3.s3d_frequency',
                's3.s3d_frequency_lateral',
                's3.s3d_frequency_vertical',
                's3.s3d_year_late_process',
                's3.s3d_late_method',
                's3.s3d_img_quality',
                's3.s3d_top_depth_ft',
                's3.s3d_top_depth_ms',
                's3.s3d_bot_depth_ft',
                's3.s3d_bot_depth_ms',
                's3.s3d_depth_spill',
                's3.s3d_depth_estimate',
                's3.s3d_depth_low',
                's3.s3d_formation_thickness',
                's3.s3d_gross_thickness',
                's3.s3d_vol_p90_area',
                's3.s3d_vol_p50_area',
                's3.s3d_vol_p10_area',
                's3.s3d_net_p90_thickness',
                's3.s3d_net_p50_thickness',
                's3.s3d_net_p10_thickness',
                's3.s3d_net_p90_por',
                's3.s3d_net_p50_por',
                's3.s3d_net_p10_por',
                's3.s3d_net_p90_satur',
                's3.s3d_net_p50_satur',
                's3.s3d_net_p10_satur',
                's3.s3d_net_p90_vsh',
                's3.s3d_net_p50_vsh',
                's3.s3d_net_p10_vsh',
                's3.s3d_remark',

                's2.s2d_year_survey',
                's2.s2d_vintage_number',
                's2.s2d_total_crossline',
                's2.s2d_seismic_line',
                's2.s2d_average_interval',
                's2.s2d_record_length_ms',
                's2.s2d_record_length_ft',
                's2.s2d_year_late_process',
                's2.s2d_late_method',
                's2.s2d_img_quality',
                's2.s2d_top_depth_ft',
                's2.s2d_top_depth_ms',
                's2.s2d_bot_depth_ft',
                's2.s2d_bot_depth_ms',
                's2.s2d_depth_spill',
                's2.s2d_depth_estimate',
                's2.s2d_depth_low',
                's2.s2d_formation_thickness',
                's2.s2d_gross_thickness',
                's2.s2d_vol_p90_area',
                's2.s2d_vol_p50_area',
                's2.s2d_vol_p10_area',
                's2.s2d_net_p90_thickness',
                's2.s2d_net_p50_thickness',
                's2.s2d_net_p10_thickness',
                's2.s2d_net_p90_por',
                's2.s2d_net_p50_por',
                's2.s2d_net_p10_por',
                's2.s2d_net_p90_satur',
                's2.s2d_net_p50_satur',
                's2.s2d_net_p10_satur',
                's2.s2d_net_p90_vsh',
                's2.s2d_net_p50_vsh',
                's2.s2d_net_p10_vsh',
                's2.s2d_remark',

                'gl.sgf_year_survey',
                'gl.sgf_survey_method',
                'gl.sgf_coverage_area',
                'gl.sgf_p90_area',
                'gl.sgf_p50_area',
                'gl.sgf_p10_area',
                'gl.sgf_p90_thickness',
                'gl.sgf_p50_thickness',
                'gl.sgf_p10_thickness',
                'gl.sgf_p90_net',
                'gl.sgf_p50_net',
                'gl.sgf_p10_net',
                'gl.sgf_remark',

                'gh.sgc_year_survey',
                'gh.sgc_sample_interval',
                'gh.sgc_number_sample',
                'gh.sgc_number_rock',
                'gh.sgc_number_fluid',
                'gh.sgc_hc_composition',
                'gh.sgc_vol_p90_area',
                'gh.sgc_vol_p50_area',
                'gh.sgc_vol_p10_area',
                'gh.sgc_remark',

                'gr.sgv_year_survey',
                'gr.sgv_survey_method',
                'gr.sgv_coverage_area',
                'gr.sgv_depth_range',
                'gr.sgv_spacing_interval',
                'gr.sgv_depth_spill',
                'gr.sgv_depth_estimate',
                'gr.sgv_depth_low',
                'gr.sgv_res_thickness',
                'gr.sgv_res_top_depth',
                'gr.sgv_res_bot_depth',
                'gr.sgv_gross_thickness',
                'gr.sgv_gross_according',
                'gr.sgv_net_gross',
                'gr.sgv_vol_p90_area',
                'gr.sgv_vol_p50_area',
                'gr.sgv_vol_p10_area',
                'gr.sgv_remark',

                'el.sel_year_survey',
                'el.sel_survey_method',
                'el.sel_coverage_area',
                'el.sel_depth_range',
                'el.sel_spacing_interval',
                'el.sel_vol_p90_area',
                'el.sel_vol_p50_area',
                'el.sel_vol_p10_area',
                'el.sel_remark',

                'rs.rst_year_survey',
                'rs.rst_survey_method',
                'rs.rst_coverage_area',
                'rs.rst_depth_range',
                'rs.rst_spacing_interval',
                'rs.rst_vol_p90_area',
                'rs.rst_vol_p50_area',
                'rs.rst_vol_p10_area',
                'rs.rst_remark',

                'ot.sor_year_survey',
                'ot.sor_vol_p90_area',
                'ot.sor_vol_p50_area',
                'ot.sor_vol_p10_area',
                'ot.sor_remark',

                'well.wl_name',
                'well.wl_latitude',
                'well.wl_longitude',
                'well.wl_type',
                'well.wl_result',
                'well.wl_shore',
                'well.wl_terrain',
                'well.wl_status',
                'well.wl_formation',
                'well.wl_date_complete',
                'well.wl_target_depth_tvd',
                'well.wl_target_depth_md',
                'well.wl_actual_depth',
                'well.wl_target_play',
                'well.wl_actual_play',
                'well.wl_number_mdt',
                'well.wl_number_rft',
                'well.wl_res_pressure',
                'well.wl_last_pressure',
                'well.wl_pressure_gradient',
                'well.wl_last_temp',
                'well.wl_integrity',
                'well.wl_electro_list',

                'zone.zone_hc_oil_show',
                'zone.zone_hc_gas_show',
                'zone.zone_hc_water_cut',
                'zone.zone_hc_water_gwc',
                'zone.zone_hc_water_owc',

                'zone.zone_rock_method',
                'zone.zone_rock_petro',
                'zone.zone_rock_petro_sample',
                'zone.zone_rock_top_depth',
                'zone.zone_rock_bot_depth',
                'zone.zone_rock_barrel',
                'zone.zone_rock_barrel_equal',
                'zone.zone_rock_total_core',
                'zone.zone_rock_preservative',
                'zone.zone_rock_routine',
                'zone.zone_rock_scal',
                'zone.zone_clastic_p10_thickness',
                'zone.zone_clastic_p50_thickness',
                'zone.zone_clastic_p90_thickness',
                'zone.zone_clastic_p10_gr',
                'zone.zone_clastic_p50_gr',
                'zone.zone_clastic_p90_gr',
                'zone.zone_clastic_p10_sp',
                'zone.zone_clastic_p50_sp',
                'zone.zone_clastic_p90_sp',
                'zone.zone_clastic_p10_net',
                'zone.zone_clastic_p50_net',
                'zone.zone_clastic_p90_net',
                'zone.zone_clastic_p10_por',
                'zone.zone_clastic_p50_por',
                'zone.zone_clastic_p90_por',
                'zone.zone_clastic_p10_satur',
                'zone.zone_clastic_p50_satur',
                'zone.zone_clastic_p90_satur',
                'zone.zone_carbo_p10_thickness',
                'zone.zone_carbo_p50_thickness',
                'zone.zone_carbo_p90_thickness',
                'zone.zone_carbo_p10_dtc',
                'zone.zone_carbo_p50_dtc',
                'zone.zone_carbo_p90_dtc',
                'zone.zone_carbo_p10_total',
                'zone.zone_carbo_p50_total',
                'zone.zone_carbo_p90_total',
                'zone.zone_carbo_p10_net',
                'zone.zone_carbo_p50_net',
                'zone.zone_carbo_p90_net',
                'zone.zone_carbo_p10_por',
                'zone.zone_carbo_p50_por',
                'zone.zone_carbo_p90_por',
                'zone.zone_carbo_p10_satur',
                'zone.zone_carbo_p50_satur',
                'zone.zone_carbo_p90_satur',

                'zone.zone_fluid_date',
                'zone.zone_fluid_sample',
                'zone.zone_fluid_ratio',
                'zone.zone_fluid_pressure',
                'zone.zone_fluid_temp',
                'zone.zone_fluid_tubing',
                'zone.zone_fluid_casing',
                'zone.zone_fluid_by',
                'zone.zone_fluid_report',
                'zone.zone_fluid_finger',
                'zone.zone_fluid_grv_oil',
                'zone.zone_fluid_grv_gas',
                'zone.zone_fluid_grv_conden',
                'zone.zone_fluid_pvt',
                'zone.zone_fluid_quantity',
                'zone.zone_fluid_z',
                'zone.zone_fvf_oil_p10',
                'zone.zone_fvf_oil_p50',
                'zone.zone_fvf_oil_p90',
                'zone.zone_fvf_gas_p10',
                'zone.zone_fvf_gas_p50',
                'zone.zone_fvf_gas_p90',
                'zone.zone_viscocity',

                'zone.zone_name',
                'zone.zone_thickness',
                'zone.zone_depth',
                'zone.zone_perforation',
                'zone.zone_well_test',
                'zone.zone_well_duration',
                'zone.zone_initial_flow',
                'zone.zone_initial_shutin',
                'zone.zone_tubing_size',
                'zone.zone_initial_temp',
                'zone.zone_initial_pressure',
                'zone.zone_pseudostate',
                'zone.zone_well_formation',
                'zone.zone_head',
                'zone.zone_pressure_wellbore',
                'zone.zone_average_por',
                'zone.zone_water_cut',
                'zone.zone_initial_water',
                'zone.zone_low_gas',
                'zone.zone_low_oil',
                'zone.zone_free_water',
                'zone.zone_grv_gas',
                'zone.zone_grv_grv_oil',
                'zone.zone_wellbore_coefficient',
                'zone.zone_wellbore_time',
                'zone.zone_res_shape',

                'zone.zone_prod_oil_choke',
                'zone.zone_prod_oil_flow',
                'zone.zone_prod_gas_choke',
                'zone.zone_prod_gas_flow',
                'zone.zone_prod_gasoil_ratio',
                'zone.zone_prod_conden_ratio',
                'zone.zone_prod_cumm_gas',
                'zone.zone_prod_cumm_oil',
                'zone.zone_prod_aof_bbl',
                'zone.zone_prod_aof_scf',
                'zone.zone_prod_critical_bbl',
                'zone.zone_prod_critical_scf',
                'zone.zone_prod_index_bbl',
                'zone.zone_prod_index_scf',
                'zone.zone_prod_diffusity',
                'zone.zone_prod_permeability',
                'zone.zone_prod_infinite',
                'zone.zone_prod_well_radius',
                'zone.zone_prod_pseudostate',
                'zone.zone_prod_res_radius',
                'zone.zone_prod_delta',
                'zone.zone_prod_wellbore',
                'zone.zone_prod_compres_rock',
                'zone.zone_prod_compres_fluid',
                'zone.zone_prod_compres_total',
                'zone.zone_prod_second',
                'zone.zone_prod_lambda',
                'zone.zone_prod_omega',
                'zone.zone_result',

                'gcf.gcf_is_sr',
                'gcf.gcf_sr_age_system',
                'gcf.gcf_sr_age_serie',
                'gcf.gcf_sr_formation',
                'gcf.gcf_sr_formation_serie',
                'gcf.gcf_sr_kerogen',
                'gcf.gcf_sr_toc',
                'gcf.gcf_sr_hfu',
                'gcf.gcf_sr_distribution',
                'gcf.gcf_sr_continuity',
                'gcf.gcf_sr_maturity',
                'gcf.gcf_sr_otr',
                'gcf.gcf_sr_remark',
                'gcf.gcf_is_res',
                'gcf.gcf_res_age_system',
                'gcf.gcf_res_age_serie',
                'gcf.gcf_res_formation',
                'gcf.gcf_res_formation_serie',
                'gcf.gcf_res_depos_set',
                'gcf.gcf_res_depos_env',
                'gcf.gcf_res_distribution',
                'gcf.gcf_res_continuity',
                'gcf.gcf_res_lithology',
                'gcf.gcf_res_por_primary',
                'gcf.gcf_res_por_secondary',
                'gcf.gcf_res_remark',
                'gcf.gcf_is_trap',
                'gcf.gcf_trap_seal_age_system',
                'gcf.gcf_trap_seal_age_serie',
                'gcf.gcf_trap_seal_formation',
                'gcf.gcf_trap_seal_formation_serie',
                'gcf.gcf_trap_seal_distribution',
                'gcf.gcf_trap_seal_continuity',
                'gcf.gcf_trap_seal_type',
                'gcf.gcf_trap_age_system',
                'gcf.gcf_trap_age_serie',
                'gcf.gcf_trap_geometry',
                'gcf.gcf_trap_type',
                'gcf.gcf_trap_closure',
                'gcf.gcf_trap_remark',
                'gcf.gcf_is_dyn',
                'gcf.gcf_dyn_migration',
                'gcf.gcf_dyn_kitchen',
                'gcf.gcf_dyn_petroleum',
                'gcf.gcf_dyn_early_age_system',
                'gcf.gcf_dyn_early_age_serie',
                'gcf.gcf_dyn_late_age_system',
                'gcf.gcf_dyn_late_age_serie',
                'gcf.gcf_dyn_preservation',
                'gcf.gcf_dyn_pathways',
                'gcf.gcf_dyn_migration_age_system',
                'gcf.gcf_dyn_migration_age_serie',
                'gcf.gcf_dyn_remark',
            ])
            ->from('rsc_prospect prospect')
            ->leftJoin('rsc_seismic_3d s3', 's3.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_seismic_2d s2', 's2.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_geological gl', 'gl.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_geochemistry gh', 'gh.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_gravity gr', 'gr.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_electromagnetic el', 'el.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_resistivity rs', 'rs.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_other ot', 'ot.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_play play', 'play.play_id = prospect.play_id')
            ->leftJoin('rsc_well well', 'well.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_wellzone zone', 'zone.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_gcf gcf', 'gcf.gcf_id = prospect.gcf_id')
            ->leftJoin('rsc_gcf pgcf', 'pgcf.gcf_id = play.gcf_id')
            ->leftJoin('adm_basin basin', 'basin.basin_id = play.basin_id')
            ->leftJoin('adm_province province', 'province.province_id = play.province_id')
            ->leftJoin('adm_working_area wk', 'wk.wk_id = play.wk_id')
            ->leftJoin('adm_psc psc', 'psc.wk_id = wk.wk_id')
            ->leftJoin('adm_kkks kkks', 'kkks.psc_id = psc.psc_id')
            ->where('play.wk_id <> "WK0000"')
            ->andWhere('play.wk_id <> "WK9999"')
            ->andWhere('prospect.prospect_is_deleted = 0')
            ->andWhere('prospect.prospect_type = "postdrill"');

        if ($postdrillId) {
            $postdrill->andWhere('prospect.prospect_id = :prospectId', [':prospectId' => $postdrillId]);
            return $postdrill->queryRow();
        }

        return $postdrill->queryAll();
    }

    public function discoveryDetail($discoveryId = null)
    {
        $discovery = Yii::app()->db->createCommand()
            ->select([
                'basin.basin_name',
                'province.province_name',
                'wk.wk_id',
                'wk.wk_name',

                'prospect.structure_name',

                'pgcf.gcf_res_lithology plithology',
                'pgcf.gcf_res_formation_serie pformation_serie',
                'pgcf.gcf_res_formation pformation',
                'pgcf.gcf_res_age_serie page_serie',
                'pgcf.gcf_res_age_system page',
                'pgcf.gcf_res_depos_env penv',
                'pgcf.gcf_trap_type ptrap',

                'prospect.prospect_clarified',
                'prospect.prospect_date_initiate',
                'prospect.prospect_latitude',
                'prospect.prospect_longitude',
                'prospect.prospect_shore',
                'prospect.prospect_terrain',
                'prospect.prospect_near_field',
                'prospect.prospect_near_infra_structure',

                's3.s3d_year_survey',
                's3.s3d_vintage_number',
                's3.s3d_bin_size',
                's3.s3d_coverage_area',
                's3.s3d_frequency',
                's3.s3d_frequency_lateral',
                's3.s3d_frequency_vertical',
                's3.s3d_year_late_process',
                's3.s3d_late_method',
                's3.s3d_img_quality',
                's3.s3d_top_depth_ft',
                's3.s3d_top_depth_ms',
                's3.s3d_bot_depth_ft',
                's3.s3d_bot_depth_ms',
                's3.s3d_depth_spill',
                's3.s3d_depth_estimate',
                's3.s3d_depth_low',
                's3.s3d_formation_thickness',
                's3.s3d_gross_thickness',
                's3.s3d_vol_p90_area',
                's3.s3d_vol_p50_area',
                's3.s3d_vol_p10_area',
                's3.s3d_net_p90_thickness',
                's3.s3d_net_p50_thickness',
                's3.s3d_net_p10_thickness',
                's3.s3d_net_p90_por',
                's3.s3d_net_p50_por',
                's3.s3d_net_p10_por',
                's3.s3d_net_p90_satur',
                's3.s3d_net_p50_satur',
                's3.s3d_net_p10_satur',
                's3.s3d_net_p90_vsh',
                's3.s3d_net_p50_vsh',
                's3.s3d_net_p10_vsh',
                's3.s3d_remark',

                's2.s2d_year_survey',
                's2.s2d_vintage_number',
                's2.s2d_total_crossline',
                's2.s2d_seismic_line',
                's2.s2d_average_interval',
                's2.s2d_record_length_ms',
                's2.s2d_record_length_ft',
                's2.s2d_year_late_process',
                's2.s2d_late_method',
                's2.s2d_img_quality',
                's2.s2d_top_depth_ft',
                's2.s2d_top_depth_ms',
                's2.s2d_bot_depth_ft',
                's2.s2d_bot_depth_ms',
                's2.s2d_depth_spill',
                's2.s2d_depth_estimate',
                's2.s2d_depth_low',
                's2.s2d_formation_thickness',
                's2.s2d_gross_thickness',
                's2.s2d_vol_p90_area',
                's2.s2d_vol_p50_area',
                's2.s2d_vol_p10_area',
                's2.s2d_net_p90_thickness',
                's2.s2d_net_p50_thickness',
                's2.s2d_net_p10_thickness',
                's2.s2d_net_p90_por',
                's2.s2d_net_p50_por',
                's2.s2d_net_p10_por',
                's2.s2d_net_p90_satur',
                's2.s2d_net_p50_satur',
                's2.s2d_net_p10_satur',
                's2.s2d_net_p90_vsh',
                's2.s2d_net_p50_vsh',
                's2.s2d_net_p10_vsh',
                's2.s2d_remark',

                'gl.sgf_year_survey',
                'gl.sgf_survey_method',
                'gl.sgf_coverage_area',
                'gl.sgf_p90_area',
                'gl.sgf_p50_area',
                'gl.sgf_p10_area',
                'gl.sgf_p90_thickness',
                'gl.sgf_p50_thickness',
                'gl.sgf_p10_thickness',
                'gl.sgf_p90_net',
                'gl.sgf_p50_net',
                'gl.sgf_p10_net',
                'gl.sgf_remark',

                'gh.sgc_year_survey',
                'gh.sgc_sample_interval',
                'gh.sgc_number_sample',
                'gh.sgc_number_rock',
                'gh.sgc_number_fluid',
                'gh.sgc_hc_composition',
                'gh.sgc_vol_p90_area',
                'gh.sgc_vol_p50_area',
                'gh.sgc_vol_p10_area',
                'gh.sgc_remark',

                'gr.sgv_year_survey',
                'gr.sgv_survey_method',
                'gr.sgv_coverage_area',
                'gr.sgv_depth_range',
                'gr.sgv_spacing_interval',
                'gr.sgv_depth_spill',
                'gr.sgv_depth_estimate',
                'gr.sgv_depth_low',
                'gr.sgv_res_thickness',
                'gr.sgv_res_top_depth',
                'gr.sgv_res_bot_depth',
                'gr.sgv_gross_thickness',
                'gr.sgv_gross_according',
                'gr.sgv_net_gross',
                'gr.sgv_vol_p90_area',
                'gr.sgv_vol_p50_area',
                'gr.sgv_vol_p10_area',
                'gr.sgv_remark',

                'el.sel_year_survey',
                'el.sel_survey_method',
                'el.sel_coverage_area',
                'el.sel_depth_range',
                'el.sel_spacing_interval',
                'el.sel_vol_p90_area',
                'el.sel_vol_p50_area',
                'el.sel_vol_p10_area',
                'el.sel_remark',

                'rs.rst_year_survey',
                'rs.rst_survey_method',
                'rs.rst_coverage_area',
                'rs.rst_depth_range',
                'rs.rst_spacing_interval',
                'rs.rst_vol_p90_area',
                'rs.rst_vol_p50_area',
                'rs.rst_vol_p10_area',
                'rs.rst_remark',

                'ot.sor_year_survey',
                'ot.sor_vol_p90_area',
                'ot.sor_vol_p50_area',
                'ot.sor_vol_p10_area',
                'ot.sor_remark',

                'well.wl_name',
                'well.wl_latitude',
                'well.wl_longitude',
                'well.wl_type',
                'well.wl_result',
                'well.wl_shore',
                'well.wl_terrain',
                'well.wl_status',
                'well.wl_formation',
                'well.wl_date_complete',
                'well.wl_target_depth_tvd',
                'well.wl_target_depth_md',
                'well.wl_actual_depth',
                'well.wl_target_play',
                'well.wl_actual_play',
                'well.wl_number_mdt',
                'well.wl_number_rft',
                'well.wl_res_pressure',
                'well.wl_last_pressure',
                'well.wl_pressure_gradient',
                'well.wl_last_temp',
                'well.wl_integrity',
                'well.wl_electro_list',

                'zone.zone_hc_oil_show',
                'zone.zone_hc_gas_show',
                'zone.zone_hc_water_cut',
                'zone.zone_hc_water_gwc',
                'zone.zone_hc_water_owc',

                'zone.zone_rock_method',
                'zone.zone_rock_petro',
                'zone.zone_rock_petro_sample',
                'zone.zone_rock_top_depth',
                'zone.zone_rock_bot_depth',
                'zone.zone_rock_barrel',
                'zone.zone_rock_barrel_equal',
                'zone.zone_rock_total_core',
                'zone.zone_rock_preservative',
                'zone.zone_rock_routine',
                'zone.zone_rock_scal',
                'zone.zone_clastic_p10_thickness',
                'zone.zone_clastic_p50_thickness',
                'zone.zone_clastic_p90_thickness',
                'zone.zone_clastic_p10_gr',
                'zone.zone_clastic_p50_gr',
                'zone.zone_clastic_p90_gr',
                'zone.zone_clastic_p10_sp',
                'zone.zone_clastic_p50_sp',
                'zone.zone_clastic_p90_sp',
                'zone.zone_clastic_p10_net',
                'zone.zone_clastic_p50_net',
                'zone.zone_clastic_p90_net',
                'zone.zone_clastic_p10_por',
                'zone.zone_clastic_p50_por',
                'zone.zone_clastic_p90_por',
                'zone.zone_clastic_p10_satur',
                'zone.zone_clastic_p50_satur',
                'zone.zone_clastic_p90_satur',
                'zone.zone_carbo_p10_thickness',
                'zone.zone_carbo_p50_thickness',
                'zone.zone_carbo_p90_thickness',
                'zone.zone_carbo_p10_dtc',
                'zone.zone_carbo_p50_dtc',
                'zone.zone_carbo_p90_dtc',
                'zone.zone_carbo_p10_total',
                'zone.zone_carbo_p50_total',
                'zone.zone_carbo_p90_total',
                'zone.zone_carbo_p10_net',
                'zone.zone_carbo_p50_net',
                'zone.zone_carbo_p90_net',
                'zone.zone_carbo_p10_por',
                'zone.zone_carbo_p50_por',
                'zone.zone_carbo_p90_por',
                'zone.zone_carbo_p10_satur',
                'zone.zone_carbo_p50_satur',
                'zone.zone_carbo_p90_satur',

                'zone.zone_fluid_date',
                'zone.zone_fluid_sample',
                'zone.zone_fluid_ratio',
                'zone.zone_fluid_pressure',
                'zone.zone_fluid_temp',
                'zone.zone_fluid_tubing',
                'zone.zone_fluid_casing',
                'zone.zone_fluid_by',
                'zone.zone_fluid_report',
                'zone.zone_fluid_finger',
                'zone.zone_fluid_grv_oil',
                'zone.zone_fluid_grv_gas',
                'zone.zone_fluid_grv_conden',
                'zone.zone_fluid_pvt',
                'zone.zone_fluid_quantity',
                'zone.zone_fluid_z',
                'zone.zone_fvf_oil_p10',
                'zone.zone_fvf_oil_p50',
                'zone.zone_fvf_oil_p90',
                'zone.zone_fvf_gas_p10',
                'zone.zone_fvf_gas_p50',
                'zone.zone_fvf_gas_p90',
                'zone.zone_viscocity',

                'zone.zone_name',
                'zone.zone_thickness',
                'zone.zone_depth',
                'zone.zone_perforation',
                'zone.zone_well_test',
                'zone.zone_well_duration',
                'zone.zone_initial_flow',
                'zone.zone_initial_shutin',
                'zone.zone_tubing_size',
                'zone.zone_initial_temp',
                'zone.zone_initial_pressure',
                'zone.zone_pseudostate',
                'zone.zone_well_formation',
                'zone.zone_head',
                'zone.zone_pressure_wellbore',
                'zone.zone_average_por',
                'zone.zone_water_cut',
                'zone.zone_initial_water',
                'zone.zone_low_gas',
                'zone.zone_low_oil',
                'zone.zone_free_water',
                'zone.zone_grv_gas',
                'zone.zone_grv_grv_oil',
                'zone.zone_wellbore_coefficient',
                'zone.zone_wellbore_time',
                'zone.zone_res_shape',

                'zone.zone_prod_oil_choke',
                'zone.zone_prod_oil_flow',
                'zone.zone_prod_gas_choke',
                'zone.zone_prod_gas_flow',
                'zone.zone_prod_gasoil_ratio',
                'zone.zone_prod_conden_ratio',
                'zone.zone_prod_cumm_gas',
                'zone.zone_prod_cumm_oil',
                'zone.zone_prod_aof_bbl',
                'zone.zone_prod_aof_scf',
                'zone.zone_prod_critical_bbl',
                'zone.zone_prod_critical_scf',
                'zone.zone_prod_index_bbl',
                'zone.zone_prod_index_scf',
                'zone.zone_prod_diffusity',
                'zone.zone_prod_permeability',
                'zone.zone_prod_infinite',
                'zone.zone_prod_well_radius',
                'zone.zone_prod_pseudostate',
                'zone.zone_prod_res_radius',
                'zone.zone_prod_delta',
                'zone.zone_prod_wellbore',
                'zone.zone_prod_compres_rock',
                'zone.zone_prod_compres_fluid',
                'zone.zone_prod_compres_total',
                'zone.zone_prod_second',
                'zone.zone_prod_lambda',
                'zone.zone_prod_omega',
                'zone.zone_result',

                'gcf.gcf_is_sr',
                'gcf.gcf_sr_age_system',
                'gcf.gcf_sr_age_serie',
                'gcf.gcf_sr_formation',
                'gcf.gcf_sr_formation_serie',
                'gcf.gcf_sr_kerogen',
                'gcf.gcf_sr_toc',
                'gcf.gcf_sr_hfu',
                'gcf.gcf_sr_distribution',
                'gcf.gcf_sr_continuity',
                'gcf.gcf_sr_maturity',
                'gcf.gcf_sr_otr',
                'gcf.gcf_sr_remark',
                'gcf.gcf_is_res',
                'gcf.gcf_res_age_system',
                'gcf.gcf_res_age_serie',
                'gcf.gcf_res_formation',
                'gcf.gcf_res_formation_serie',
                'gcf.gcf_res_depos_set',
                'gcf.gcf_res_depos_env',
                'gcf.gcf_res_distribution',
                'gcf.gcf_res_continuity',
                'gcf.gcf_res_lithology',
                'gcf.gcf_res_por_primary',
                'gcf.gcf_res_por_secondary',
                'gcf.gcf_res_remark',
                'gcf.gcf_is_trap',
                'gcf.gcf_trap_seal_age_system',
                'gcf.gcf_trap_seal_age_serie',
                'gcf.gcf_trap_seal_formation',
                'gcf.gcf_trap_seal_formation_serie',
                'gcf.gcf_trap_seal_distribution',
                'gcf.gcf_trap_seal_continuity',
                'gcf.gcf_trap_seal_type',
                'gcf.gcf_trap_age_system',
                'gcf.gcf_trap_age_serie',
                'gcf.gcf_trap_geometry',
                'gcf.gcf_trap_type',
                'gcf.gcf_trap_closure',
                'gcf.gcf_trap_remark',
                'gcf.gcf_is_dyn',
                'gcf.gcf_dyn_migration',
                'gcf.gcf_dyn_kitchen',
                'gcf.gcf_dyn_petroleum',
                'gcf.gcf_dyn_early_age_system',
                'gcf.gcf_dyn_early_age_serie',
                'gcf.gcf_dyn_late_age_system',
                'gcf.gcf_dyn_late_age_serie',
                'gcf.gcf_dyn_preservation',
                'gcf.gcf_dyn_pathways',
                'gcf.gcf_dyn_migration_age_system',
                'gcf.gcf_dyn_migration_age_serie',
                'gcf.gcf_dyn_remark',
            ])
            ->from('rsc_prospect prospect')
            ->leftJoin('rsc_seismic_3d s3', 's3.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_seismic_2d s2', 's2.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_geological gl', 'gl.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_geochemistry gh', 'gh.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_gravity gr', 'gr.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_electromagnetic el', 'el.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_resistivity rs', 'rs.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_other ot', 'ot.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_play play', 'play.play_id = prospect.play_id')
            ->leftJoin('rsc_well well', 'well.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_wellzone zone', 'zone.prospect_id = prospect.prospect_id')
            ->leftJoin('rsc_gcf gcf', 'gcf.gcf_id = prospect.gcf_id')
            ->leftJoin('rsc_gcf pgcf', 'pgcf.gcf_id = play.gcf_id')
            ->leftJoin('adm_basin basin', 'basin.basin_id = play.basin_id')
            ->leftJoin('adm_province province', 'province.province_id = play.province_id')
            ->leftJoin('adm_working_area wk', 'wk.wk_id = play.wk_id')
            ->leftJoin('adm_psc psc', 'psc.wk_id = wk.wk_id')
            ->leftJoin('adm_kkks kkks', 'kkks.psc_id = psc.psc_id')
            ->where('play.wk_id <> "WK0000"')
            ->andWhere('play.wk_id <> "WK9999"')
            ->andWhere('play.wk_id <> "WK1452"')
            ->andWhere('prospect.prospect_is_deleted = 0')
            ->andWhere('prospect.prospect_type = "discovery"');

        if ($discoveryId) {
            $discovery->andWhere('prospect.prospect_id = :prospectId', [':prospectId' => $discoveryId]);
            return $discovery->queryRow();
        }

        return $discovery->queryAll();
    }

    public function wellDetail($wellId = null)
    {
        $well = Yii::app()->db->createCommand()
            ->select([
                'well.wl_name',
                'well.wl_latitude',
                'well.wl_longitude',
                'well.wl_type',
                'well.wl_result',
                'well.wl_shore',
                'well.wl_terrain',
                'well.wl_status',
                'well.wl_formation',
                'well.wl_date_complete',
                'well.wl_target_depth_tvd',
                'well.wl_target_depth_md',
                'well.wl_actual_depth',
                'well.wl_target_play',
                'well.wl_actual_play',
                'well.wl_number_mdt',
                'well.wl_number_rft',
                'well.wl_res_pressure',
                'well.wl_last_pressure',
                'well.wl_pressure_gradient',
                'well.wl_last_temp',
                'well.wl_integrity',
                'well.wl_electro_list',
            ])
            ->from('rsc_well well')
            ->leftJoin('rsc_wellzone zone', 'zone.wl_id = well.wl_id')
            ->where('well.wl_id = :wellId', [':wellId' => $wellId]);

        return $well->queryRow();
    }

    public function zoneDetail($wellId = null)
    {
        $zones = Yii::app()->db->createCommand()
            ->select([
                'zone.zone_hc_oil_show',
                'zone.zone_hc_gas_show',
                'zone.zone_hc_water_cut',
                'zone.zone_hc_water_gwc',
                'zone.zone_hc_water_owc',

                'zone.zone_rock_method',
                'zone.zone_rock_petro',
                'zone.zone_rock_petro_sample',
                'zone.zone_rock_top_depth',
                'zone.zone_rock_bot_depth',
                'zone.zone_rock_barrel',
                'zone.zone_rock_barrel_equal',
                'zone.zone_rock_total_core',
                'zone.zone_rock_preservative',
                'zone.zone_rock_routine',
                'zone.zone_rock_scal',
                'zone.zone_clastic_p10_thickness',
                'zone.zone_clastic_p50_thickness',
                'zone.zone_clastic_p90_thickness',
                'zone.zone_clastic_p10_gr',
                'zone.zone_clastic_p50_gr',
                'zone.zone_clastic_p90_gr',
                'zone.zone_clastic_p10_sp',
                'zone.zone_clastic_p50_sp',
                'zone.zone_clastic_p90_sp',
                'zone.zone_clastic_p10_net',
                'zone.zone_clastic_p50_net',
                'zone.zone_clastic_p90_net',
                'zone.zone_clastic_p10_por',
                'zone.zone_clastic_p50_por',
                'zone.zone_clastic_p90_por',
                'zone.zone_clastic_p10_satur',
                'zone.zone_clastic_p50_satur',
                'zone.zone_clastic_p90_satur',
                'zone.zone_carbo_p10_thickness',
                'zone.zone_carbo_p50_thickness',
                'zone.zone_carbo_p90_thickness',
                'zone.zone_carbo_p10_dtc',
                'zone.zone_carbo_p50_dtc',
                'zone.zone_carbo_p90_dtc',
                'zone.zone_carbo_p10_total',
                'zone.zone_carbo_p50_total',
                'zone.zone_carbo_p90_total',
                'zone.zone_carbo_p10_net',
                'zone.zone_carbo_p50_net',
                'zone.zone_carbo_p90_net',
                'zone.zone_carbo_p10_por',
                'zone.zone_carbo_p50_por',
                'zone.zone_carbo_p90_por',
                'zone.zone_carbo_p10_satur',
                'zone.zone_carbo_p50_satur',
                'zone.zone_carbo_p90_satur',

                'zone.zone_fluid_date',
                'zone.zone_fluid_sample',
                'zone.zone_fluid_ratio',
                'zone.zone_fluid_pressure',
                'zone.zone_fluid_temp',
                'zone.zone_fluid_tubing',
                'zone.zone_fluid_casing',
                'zone.zone_fluid_by',
                'zone.zone_fluid_report',
                'zone.zone_fluid_finger',
                'zone.zone_fluid_grv_oil',
                'zone.zone_fluid_grv_gas',
                'zone.zone_fluid_grv_conden',
                'zone.zone_fluid_pvt',
                'zone.zone_fluid_quantity',
                'zone.zone_fluid_z',
                'zone.zone_fvf_oil_p10',
                'zone.zone_fvf_oil_p50',
                'zone.zone_fvf_oil_p90',
                'zone.zone_fvf_gas_p10',
                'zone.zone_fvf_gas_p50',
                'zone.zone_fvf_gas_p90',
                'zone.zone_viscocity',

                'zone.zone_name',
                'zone.zone_thickness',
                'zone.zone_depth',
                'zone.zone_perforation',
                'zone.zone_well_test',
                'zone.zone_well_duration',
                'zone.zone_initial_flow',
                'zone.zone_initial_shutin',
                'zone.zone_tubing_size',
                'zone.zone_initial_temp',
                'zone.zone_initial_pressure',
                'zone.zone_pseudostate',
                'zone.zone_well_formation',
                'zone.zone_head',
                'zone.zone_pressure_wellbore',
                'zone.zone_average_por',
                'zone.zone_water_cut',
                'zone.zone_initial_water',
                'zone.zone_low_gas',
                'zone.zone_low_oil',
                'zone.zone_free_water',
                'zone.zone_grv_gas',
                'zone.zone_grv_grv_oil',
                'zone.zone_wellbore_coefficient',
                'zone.zone_wellbore_time',
                'zone.zone_res_shape',

                'zone.zone_prod_oil_choke',
                'zone.zone_prod_oil_flow',
                'zone.zone_prod_gas_choke',
                'zone.zone_prod_gas_flow',
                'zone.zone_prod_gasoil_ratio',
                'zone.zone_prod_conden_ratio',
                'zone.zone_prod_cumm_gas',
                'zone.zone_prod_cumm_oil',
                'zone.zone_prod_aof_bbl',
                'zone.zone_prod_aof_scf',
                'zone.zone_prod_critical_bbl',
                'zone.zone_prod_critical_scf',
                'zone.zone_prod_index_bbl',
                'zone.zone_prod_index_scf',
                'zone.zone_prod_diffusity',
                'zone.zone_prod_permeability',
                'zone.zone_prod_infinite',
                'zone.zone_prod_well_radius',
                'zone.zone_prod_pseudostate',
                'zone.zone_prod_res_radius',
                'zone.zone_prod_delta',
                'zone.zone_prod_wellbore',
                'zone.zone_prod_compres_rock',
                'zone.zone_prod_compres_fluid',
                'zone.zone_prod_compres_total',
                'zone.zone_prod_second',
                'zone.zone_prod_lambda',
                'zone.zone_prod_omega',
                'zone.zone_result',
            ])
            ->from('rsc_well well')
            ->leftJoin('rsc_wellzone zone', 'zone.wl_id = well.wl_id')
            ->where('well.wl_id = :wellId', [':wellId' => $wellId]);

        return $zones->queryAll();
    }

    public function getWkListBasin($basinName) {
        return Yii::app()->db->createCommand()
            ->select([
                'wk',
            ])
            ->from('adm_basin_wk')
            ->where('wk <> "WK1047"')
            ->andWhere('basin = :basin', [':basin' => $basinName])
            ->order('wk')
            ->queryColumn();
    }

    public function getBasinList() {
        return Yii::app()->db->createCommand()
            ->select(['basin_name'])
            ->from('adm_basin')
            ->where('basin_name <> "unclassified"')
            ->order('basin_urut')
            ->queryColumn();
    }
}
