<?php 

class Format extends CFormatter
{
	public $numberFormat = array('decimalSeparator'=>',', 'thousandSeparator'=>'.');
	
	public function formatNumber($value)
	{
		return number_format($value, $this->numberFormat['decimalSeparator'],
            $this->numberFormat['thousandSeparator']);
	} 
}

?>
