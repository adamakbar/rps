<?php

class ResourcesRepository
{
    /**
     * Mencari seluruh list index Play dari wilayah kerja tertentu.
     *
     * @param  string $wkid Wilayah Kerja ID
     * @param  boolean $withDeleted Ambil data termasuk yang sudah terhapus
     * @return array
     */
    public function getPlayIndex($wkid, $withDeleted=false)
    {
        return Yii::app()->db->createCommand()
            ->select([
                'play.play_id',
                'basin.basin_name',
                'gcf.gcf_res_lithology',
                'gcf.gcf_res_formation_serie',
                'gcf.gcf_res_formation',
                'gcf.gcf_res_age_serie',
                'gcf.gcf_res_age_system',
                'gcf.gcf_res_depos_env',
                'gcf.gcf_trap_type',
            ])
            ->from([
                'rsc_play AS play',
                'rsc_gcf AS gcf',
                'adm_basin AS basin',
            ])
            ->where([
                'and',
                'play.wk_id = :wkid',
                'gcf.gcf_id = play.gcf_id',
                'basin.basin_id = play.basin_id',
            ], [':wkid' => $wkid])
            ->queryAll();
    }

	/**
	 * Mencari seluruh data Play berdasarkan play_id yang diberikan.
	 * 
	 * @param  int $playId
	 * @return array
	 */
	public function getPlayDetail($playId)
	{
        $play = Yii::app()->db->createCommand()
        	->select([
        		'basin.basin_name',
        		'province.province_name',
        		'play.play_analog_to',
        		'play.play_analog_distance',
        		'play.play_exr_method',
        		'play.play_shore',
        		'play.play_terrain',
        		'play.play_near_field',
        		'play.play_near_infra_structure',
        		'play.play_support_data',
        		'play.play_outcrop_distance',
        		'play.play_s2d_year',
        		'play.play_s2d_crossline',
        		'play.play_s2d_line_intervall',
        		'play.play_s2d_img_quality',
        		'play.play_sgc',
        		'play.play_sgc_sample',
        		'play.play_sgc_depth',
        		'play.play_sgv',
        		'play.play_sgv_acre',
        		'play.play_sgv_depth',
        		'play.play_srt',
        		'play.play_srt_acre',
        		'play.play_map_scale',
        		'play.play_map_author',
        		'play.play_map_year',
        		'play.play_remark',
        	])
        	->from('rsc_play AS play')
            ->leftJoin('adm_basin AS basin', 'play.basin_id=basin.basin_id')
            ->leftJoin('adm_province AS province', 'play.province_id=province.province_id')
              ->where('play.play_id = :playId', [':playId' => $playId]);

        $play = $this->withGcf($play, 'play');

        return $play->queryRow();
	}

    public function withGcf($query, $class)
    {
        $select = explode(', ', str_replace('`', '', $query->getSelect()));
        $select = array_merge($select, [
            'gcf.gcf_is_sr',
            'gcf.gcf_sr_age_system',
            'gcf.gcf_sr_age_serie',
            'gcf.gcf_sr_formation',
            'gcf.gcf_sr_formation_serie',
            'gcf.gcf_sr_kerogen',
            'gcf.gcf_sr_toc',
            'gcf.gcf_sr_hfu',
            'gcf.gcf_sr_distribution',
            'gcf.gcf_sr_continuity',
            'gcf.gcf_sr_maturity',
            'gcf.gcf_sr_otr',
            'gcf.gcf_sr_remark',
            'gcf.gcf_is_res',
            'gcf.gcf_res_age_system',
            'gcf.gcf_res_age_serie',
            'gcf.gcf_res_formation',
            'gcf.gcf_res_formation_serie',
            'gcf.gcf_res_depos_set',
            'gcf.gcf_res_depos_env',
            'gcf.gcf_res_distribution',
            'gcf.gcf_res_continuity',
            'gcf.gcf_res_lithology',
            'gcf.gcf_res_por_primary',
            'gcf.gcf_res_por_secondary',
            'gcf.gcf_res_remark',
            'gcf.gcf_is_trap',
            'gcf.gcf_trap_seal_age_system',
            'gcf.gcf_trap_seal_age_serie',
            'gcf.gcf_trap_seal_formation',
            'gcf.gcf_trap_seal_formation_serie',
            'gcf.gcf_trap_seal_distribution',
            'gcf.gcf_trap_seal_continuity',
            'gcf.gcf_trap_seal_type',
            'gcf.gcf_trap_age_system',
            'gcf.gcf_trap_age_serie',
            'gcf.gcf_trap_geometry',
            'gcf.gcf_trap_type',
            'gcf.gcf_trap_closure',
            'gcf.gcf_trap_remark',
            'gcf.gcf_is_dyn',
            'gcf.gcf_dyn_migration',
            'gcf.gcf_dyn_kitchen',
            'gcf.gcf_dyn_petroleum',
            'gcf.gcf_dyn_early_age_system',
            'gcf.gcf_dyn_early_age_serie',
            'gcf.gcf_dyn_late_age_system',
            'gcf.gcf_dyn_late_age_serie',
            'gcf.gcf_dyn_preservation',
            'gcf.gcf_dyn_pathways',
            'gcf.gcf_dyn_migration_age_system',
            'gcf.gcf_dyn_migration_age_serie',
            'gcf.gcf_dyn_remark']);
        
        $query->select($select)
            ->leftJoin('rsc_gcf as gcf', $class . '.gcf_id=gcf.gcf_id');

        return $query;
    }
}
