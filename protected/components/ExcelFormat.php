<?php

class ExcelFormat
{
    public $file;
    public $base;
    public $sheet; // Current active sheet
    public $wkid;
    public $wkName;
    public $contractor;
    public $basin;

    public function __construct($wkid)
    {
        $workingArea = "
            SELECT
             wk.wk_id,
             wk.wk_name wkName,

             (SELECT skps.kkks_name
              FROM adm_kkks skps,
               adm_psc spsc
              WHERE spsc.wk_id = wk.wk_id
              AND skps.psc_id = spsc.psc_id) contractorName,

             (SELECT basin FROM adm_basin_wk
               WHERE wk = wk.wk_id) basin

            FROM adm_working_area wk
            WHERE wk.wk_id = '{$wkid}'";

        $workingArea = Yii::app()->db->createCommand($workingArea)->queryRow();

        $this->wkid = $wkid;
        $this->wkName = $workingArea['wkName'];
        $this->contractor = $workingArea['contractorName'];
        $this->basin = $workingArea['basin'];

        // Hapus jika file dengan WKID yang sama sudah ada
        $this->file = Yii::app()->basePath . '/../blob/generated/excel/' . $wkid . '.xlsx';
        if (file_exists($this->file)) {
            unlink($this->file);
        }

        $this->base = new PHPExcel();
        $this->base->getProperties()
            ->setCreator('SKK Migas')
            ->setLastModifiedBy('SKK Migas')
            ->setTitle($this->wkName)
            ->setDescription("Data Sumberdaya {$this->wkName}");

        $this->base->setActiveSheetIndex(0)
            ->setTitle('Umum')
            ->setCellValue('B2', 'WKID')
            ->setCellValue('C2', $this->wkid)
            ->setCellValue('B3', 'Wilayah Kerja')
            ->setCellValue('C3', $this->wkName)
            ->setCellValue('B4', 'Kontraktor')
            ->setCellValue('C4', $this->contractor)
            ->setCellValue('B5', 'Basin')
            ->setCellValue('C5', $this->basin)
            ->getColumnDimension('B')->setWidth("20");

        $this->sheet = $this->base->getActiveIndex();
    }

    public function save()
    {
        PHPExcel_IOFactory::createWriter($this->base, 'Excel2007')
            ->save($this->file);
    }

    public function addPlay($data)
    {
        $sheet = new PHPExcel_Worksheet($this->base, 'Play ' . $this->getLatestNumbering('Play'));

        $this->base->addSheet($sheet);

        $sheet->setCellValue('A1', 'Play name');
        $sheet->setCellValue('B1', $data['play_name']);

        $sheet->setCellValue('A3', 'General Data');

        $sheet->setCellValue('B4', 'Province');
        $sheet->setCellValue('C4', $data['province']);

        $sheet->setCellValue('B5', 'By analog to');
        $sheet->setCellValue('C5', $data['play_analog_to']);

        $sheet->setCellValue('B6', 'Analog distance');
        $sheet->setCellValue('C6', $data['play_analog_distance']);
    }

    private function getLatestNumbering($class)
    {
        $sheets = array_diff($this->base->getSheetNames(), [$class]);
        $sheets = preg_grep("/^{$class}.*/i", $sheets);

        foreach ($sheets as &$sheet) {
            $sheet = preg_replace("/[^0-9]/", '', $sheet);
        }

        if ($sheets) {
            return (int) max($sheets) + 1;
        }

        return 1;
    }

    private function field($label, $value)
    {
    }
}