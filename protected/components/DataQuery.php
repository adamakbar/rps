<?php

class DataQuery
{
    public function detail_gcf_sql() {
    }

    public function total_wk_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(wk_id) total_wk
            FROM adm_working_area
            WHERE
             wk_id <> 'WK0000'
             AND wk_id <> 'WK9999'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_play_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(pl.play_id) total_play
            FROM
             rsc_play pl,
             adm_working_area wk
            WHERE
             pl.wk_id = wk.wk_id
             AND wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_lead_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(ld.lead_id) total_lead
            FROM
             rsc_lead ld,
             rsc_play pl,
             adm_working_area wk
            WHERE
             ld.play_id = pl.play_id
             AND pl.wk_id = wk.wk_id
             AND wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
             AND ld.lead_is_deleted = 0
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_drillable_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(pr.prospect_id) total_drillable
            FROM
             rsc_prospect pr,
             rsc_play pl,
             adm_working_area wk
            WHERE
             pr.play_id = pl.play_id
             AND pr.prospect_type = 'drillable'
             AND pl.wk_id = wk.wk_id
             AND wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_postdrill_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(pr.prospect_id) total_postdrill
            FROM
             rsc_prospect pr,
             rsc_play pl,
             adm_working_area wk
            WHERE
             pr.play_id = pl.play_id
             AND pr.prospect_type = 'postdrill'
             AND pl.wk_id = wk.wk_id
             AND wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_discovery_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(pr.prospect_id) total_discovery
            FROM
             rsc_prospect pr,
             rsc_play pl,
             adm_working_area wk
            WHERE
             pr.play_id = pl.play_id
             AND pr.prospect_type = 'discovery'
             AND pl.wk_id = wk.wk_id
             AND wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_well_postdrill_sql() {
        $sql = "
            SELECT COUNT(wl_id) total_well_postdrill
            FROM
             rsc_well
            WHERE
             prospect_type = 'postdrill'
             AND wk_id <> 'WK0000'
             AND wk_id <> 'WK9999'
        ";

        return $sql;
    }

    public function total_well_discovery_sql() {
        $sql = "
            SELECT COUNT(wl_id) total_well_discovery
            FROM
             rsc_well
            WHERE
             prospect_type = 'discovery'
             AND wk_id <> 'WK0000'
             AND wk_id <> 'WK9999'
        ";

        return $sql;
    }

    public function total_kirim_surat_sql() {
        $sql = "
            SELECT COUNT(wk_id) total_kirim_surat
            FROM adm_working_area
            WHERE
             wk_surat = 1
             AND wk_id <> 'WK0000'
             AND wk_id <> 'WK9999'
        ";

        return $sql;
    }

    public function total_tidak_kirim_surat_sql() {
        $sql = "
            SELECT COUNT(wk_id) total_tidak_kirim_surat
            FROM adm_working_area
            WHERE
             wk_surat IS NULL
             AND wk_id <> 'WK0000'
             AND wk_id <> 'WK9999'
        ";

        return $sql;
    }

    public function total_ada_data_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(wk_id) total_ada_data
            FROM adm_working_area
            WHERE wk_id IN
             (
              SELECT DISTINCT pl.wk_id
              FROM rsc_play pl
              WHERE pl.wk_id = wk_id
             )
            AND wk_id <> 'WK9999'
            AND wk_id <> 'WK0000'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_tidak_ada_data_sql($exc_terminate=false) {
        $sql = "
            SELECT COUNT(wk_id) total_tidak_ada_data
            FROM adm_working_area
            WHERE wk_id NOT IN
             (
              SELECT DISTINCT pl.wk_id
              FROM rsc_play pl
              WHERE pl.wk_id = wk_id
             )
            AND wk_id <> 'WK9999'
            AND wk_id <> 'WK0000'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk_stage <> 'Terminasi'";
        }

        return $sql;
    }

    public function total_wk_eksplorasi_sql() {
        $sql = "
            SELECT COUNT(wk_id) total_wk_eksplorasi
            FROM adm_working_area
            WHERE
             wk_stage = 'Eksplorasi'
             AND wk_id <> 'WK9999'
             AND wk_id <> 'WK0000'
        ";

        return $sql;
    }

    public function total_wk_eksploitasi_sql() {
        $sql = "
            SELECT COUNT(wk_id) total_wk_eksploitasi
            FROM adm_working_area
            WHERE
             wk_stage = 'Eksploitasi'
             AND wk_id <> 'WK9999'
             AND wk_id <> 'WK0000'
        ";

        return $sql;
    }

    public function total_wk_terminasi_sql() {
        $sql = "
            SELECT COUNT(wk_id) total_wk_terminasi
            FROM adm_working_area
            WHERE
             wk_stage = 'Terminasi'
             AND wk_id <> 'WK9999'
             AND wk_id <> 'WK0000'
        ";

        return $sql;
    }

    public function resources_sql() {
        $sql = "
            SELECT
             wk.wk_id,
             wk.wk_name wk,

             (SELECT skps.kkks_name
              FROM adm_kkks skps,
               adm_psc spsc
              WHERE spsc.wk_id = wk.wk_id
              AND skps.psc_id = spsc.psc_id) kkks,

             CONCAT('images/montage_2012/', wk.wk_id, '.png') montage_2012,
             CONCAT('images/montage_2013/', wk.wk_id, '.pdf') montage_2013,
         CONCAT('images/montage_2014/', wk.wk_name, '.pdf') montage_2014,

             (SELECT user_name
              FROM adm_user
              WHERE wk_id = wk.wk_id) username,

             (SELECT urut FROM adm_basin_wk
               WHERE wk = wk.wk_id) basin_urut,

             (SELECT basin FROM adm_basin_wk
               WHERE wk = wk.wk_id) basin,

             (SELECT COUNT(play_id)
              FROM rsc_play
              WHERE wk_id = wk.wk_id) play,

             (SELECT COUNT(ld.lead_id)
              FROM rsc_play pl, rsc_lead ld
              WHERE pl.wk_id = wk.wk_id
              AND ld.play_id = pl.play_id
              AND ld.lead_is_deleted = 0) lead,

             (SELECT COUNT(dr.prospect_id)
              FROM rsc_play pl, rsc_prospect dr
              WHERE pl.wk_id = wk.wk_id
              AND dr.play_id = pl.play_id
              AND dr.prospect_type = 'drillable'
              AND dr.prospect_is_deleted = 0) drillable,

             (SELECT COUNT(dr.prospect_id)
              FROM rsc_play pl, rsc_prospect dr
              WHERE pl.wk_id = wk.wk_id
              AND dr.play_id = pl.play_id
              AND dr.prospect_type = 'postdrill'
              AND dr.prospect_is_deleted = 0) postdrill,

             (SELECT COUNT(dr.prospect_id)
              FROM rsc_play pl, rsc_prospect dr
              WHERE pl.wk_id = wk.wk_id
              AND dr.play_id = pl.play_id
              AND dr.prospect_type = 'discovery'
              AND dr.prospect_is_deleted = 0) discovery,

             CASE WHEN wk.wk_surat = 1
              THEN 'Sudah'
              ELSE 'Belum'
              END surat,

             wk.wk_stage stage

            FROM adm_working_area wk
            WHERE
             wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
            ORDER BY basin_urut
            ";

        $sql_after = "
            SELECT
             wk.wk_id,
             wk.wk_name wk,

             (SELECT skps.kkks_name
              FROM adm_kkks skps,
               adm_psc spsc
              WHERE spsc.wk_id = wk.wk_id
              AND skps.psc_id = spsc.psc_id) kkks,

             (SELECT user_name
              FROM adm_user
              WHERE wk_id = wk.wk_id) username,

             (SELECT DISTINCT basin_urut
              FROM adm_working_area wks, adm_basin bs, rsc_play pl
              WHERE pl.wk_id = wks.wk_id
              AND bs.basin_id = pl.basin_id
              AND wks.wk_id = wk.wk_id
              ORDER BY basin_urut LIMIT 1) basin_urut,

             (SELECT GROUP_CONCAT(DISTINCT bs.basin_name,','
               ORDER BY bs.basin_urut SEPARATOR '')
              FROM adm_working_area wks, adm_basin bs, rsc_play pl
              WHERE pl.wk_id = wks.wk_id
              AND bs.basin_id = pl.basin_id
              AND wks.wk_id = wk.wk_id
              ORDER BY bs.basin_urut) basin,

             (SELECT COUNT(play_id)
              FROM rsc_play
              WHERE wk_id = wk.wk_id
                  AND DATE(play_submit_date) >= DATE('2015-01-01')) play,

             (SELECT COUNT(ld.lead_id)
              FROM rsc_play pl, rsc_lead ld
              WHERE pl.wk_id = wk.wk_id
              AND ld.play_id = pl.play_id
              AND ld.lead_is_deleted = 0
                  AND DATE(ld.lead_submit_date) >= DATE('2015-01-01')) lead,

             (SELECT COUNT(dr.prospect_id)
              FROM rsc_play pl, rsc_prospect dr
              WHERE pl.wk_id = wk.wk_id
              AND dr.play_id = pl.play_id
              AND dr.prospect_type = 'drillable'
              AND dr.prospect_is_deleted = 0
                  AND DATE(dr.prospect_submit_date) >= DATE('2015-01-01')) drillable,

             (SELECT COUNT(dr.prospect_id)
              FROM rsc_play pl, rsc_prospect dr
              WHERE pl.wk_id = wk.wk_id
              AND dr.play_id = pl.play_id
              AND dr.prospect_type = 'postdrill'
              AND dr.prospect_is_deleted = 0
                  AND DATE(dr.prospect_submit_date) >= DATE('2015-01-01')) postdrill,

             (SELECT COUNT(dr.prospect_id)
              FROM rsc_play pl, rsc_prospect dr
              WHERE pl.wk_id = wk.wk_id
              AND dr.play_id = pl.play_id
              AND dr.prospect_type = 'discovery'
              AND dr.prospect_is_deleted = 0
                  AND DATE(dr.prospect_submit_date) >= DATE('2015-01-01')) discovery,

             CASE WHEN wk.wk_surat = 1
              THEN 'Sudah'
              ELSE 'Belum'
              END surat,

             wk.wk_stage stage

            FROM adm_working_area wk
            WHERE
             wk.wk_id <> 'WK0000'
             AND wk.wk_id <> 'WK9999'
            ORDER BY basin_urut, wk
        ";

        $header = array(
            'Basin',
            'WKID',
            'Nama WK',
            'Play',
            'Lead',
            'Drillable',
            'Postdrill',
            'Discovery',
            'Status Pengiriman Surat',
            'Status WK',
        );

        return array('sql' => $sql, 'header' => $header);

        return $sql;

    }

    public function rekap_play_sql($exc_terminate=false) {
        $sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,

             gcf.gcf_res_lithology play_litho,
             gcf.gcf_res_formation_serie play_form_serie,
             gcf.gcf_res_formation play_form,
             gcf.gcf_res_age_serie play_age_serie,
             gcf.gcf_res_age_system play_age,
             gcf.gcf_res_depos_env play_env,
             gcf.gcf_trap_type play_trap,

             gcf.gcf_is_sr sr,
             gcf.gcf_is_res re,
             gcf.gcf_is_trap tr,
             gcf.gcf_is_dyn dn,

             gcf.gcf_sr_age_system sr_age_system,
             gcf.gcf_sr_age_serie sr_age_serie,
             gcf.gcf_sr_formation sr_formation,
             gcf.gcf_sr_formation_serie sr_formation_serie,
             gcf.gcf_sr_kerogen sr_ker,
             gcf.gcf_sr_toc sr_toc,
             gcf.gcf_sr_hfu sr_hfu,
             gcf.gcf_sr_distribution sr_distribution,
             gcf.gcf_sr_continuity sr_continuity,
             gcf.gcf_sr_maturity sr_mat,
             gcf.gcf_sr_otr sr_otr,

             gcf.gcf_res_age_system re_age_system,
             gcf.gcf_res_age_serie re_age_serie,
             gcf.gcf_res_formation re_formation,
             gcf.gcf_res_formation_serie re_formation_serie,
             gcf.gcf_res_depos_set re_depos_set,
             gcf.gcf_res_depos_env re_depos_env,
             gcf.gcf_res_distribution re_dis,
             gcf.gcf_res_continuity re_continuity,
             gcf.gcf_res_lithology re_lit,
             gcf.gcf_res_por_secondary re_sec,
             gcf.gcf_res_por_primary re_pri,

             gcf.gcf_trap_seal_age_system tr_seal_age_system,
             gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             gcf.gcf_trap_seal_formation tr_seal_formation,
             gcf.gcf_trap_seal_formation_serie tr_seal_formation_serie,
             gcf.gcf_trap_seal_distribution tr_sdi,
             gcf.gcf_trap_seal_continuity tr_scn,
             gcf.gcf_trap_seal_type tr_stp,
             gcf.gcf_trap_age_system tr_age_system,
             gcf.gcf_trap_age_serie tr_age_serie,
             gcf.gcf_trap_geometry tr_geo,
             gcf.gcf_trap_type tr_trp,
             gcf.gcf_trap_closure tr_closure,

             gcf.gcf_dyn_migration dn_migration,
             gcf.gcf_dyn_kitchen dn_kit,
             gcf.gcf_dyn_petroleum dn_tec,
             gcf.gcf_dyn_early_age_system dn_age_early_system,
             gcf.gcf_dyn_early_age_serie dn_age_early_serie,
             gcf.gcf_dyn_late_age_system dn_age_late_system,
             gcf.gcf_dyn_late_age_serie dn_age_late_serie,
             gcf.gcf_dyn_preservation dn_prv,
             gcf.gcf_dyn_pathways dn_mig,
             gcf.gcf_dyn_migration_age_system dn_migration_age_system,
             gcf.gcf_dyn_migration_age_serie dn_migration_age_serie

            FROM
             rsc_play pl
             LEFT JOIN rsc_gcf gcf ON gcf.gcf_id = pl.gcf_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        $sql = $sql . " ORDER BY wk_id";

        $header = array(
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Nama Play',
            'GCF',
        );

        return array('sql' => $sql, 'header' => $header);
    }

    public function rekap_lead_sql($exc_terminate=false) {
        $sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             ld.structure_name structure,
             ld.lead_latitude,
             ld.lead_longitude,
             pgcf.gcf_res_lithology play_litho,
             pgcf.gcf_res_formation_serie play_form_serie,
             pgcf.gcf_res_formation play_form,
             pgcf.gcf_res_age_serie play_age_serie,
             pgcf.gcf_res_age_system play_age,
             pgcf.gcf_res_depos_env play_env,
             pgcf.gcf_trap_type play_trap,

             gcf.gcf_is_sr sr,
             gcf.gcf_is_res re,
             gcf.gcf_is_trap tr,
             gcf.gcf_is_dyn dn,

             gcf.gcf_sr_age_system sr_age_system,
             gcf.gcf_sr_age_serie sr_age_serie,
             gcf.gcf_sr_formation sr_formation,
             gcf.gcf_sr_formation_serie sr_formation_serie,
             gcf.gcf_sr_kerogen sr_ker,
             gcf.gcf_sr_toc sr_toc,
             gcf.gcf_sr_hfu sr_hfu,
             gcf.gcf_sr_distribution sr_distribution,
             gcf.gcf_sr_continuity sr_continuity,
             gcf.gcf_sr_maturity sr_mat,
             gcf.gcf_sr_otr sr_otr,

             gcf.gcf_res_age_system re_age_system,
             gcf.gcf_res_age_serie re_age_serie,
             gcf.gcf_res_formation re_formation,
             gcf.gcf_res_formation_serie re_formation_serie,
             gcf.gcf_res_depos_set re_depos_set,
             gcf.gcf_res_depos_env re_depos_env,
             gcf.gcf_res_distribution re_dis,
             gcf.gcf_res_continuity re_continuity,
             gcf.gcf_res_lithology re_lit,
             gcf.gcf_res_por_secondary re_sec,
             gcf.gcf_res_por_primary re_pri,

             gcf.gcf_trap_seal_age_system tr_seal_age_system,
             gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             gcf.gcf_trap_seal_formation tr_seal_formation,
             gcf.gcf_trap_seal_formation_serie tr_seal_formation_serie,
             gcf.gcf_trap_seal_distribution tr_sdi,
             gcf.gcf_trap_seal_continuity tr_scn,
             gcf.gcf_trap_seal_type tr_stp,
             gcf.gcf_trap_age_system tr_age_system,
             gcf.gcf_trap_age_serie tr_age_serie,
             gcf.gcf_trap_geometry tr_geo,
             gcf.gcf_trap_type tr_trp,
             gcf.gcf_trap_closure tr_closure,

             gcf.gcf_dyn_migration dn_migration,
             gcf.gcf_dyn_kitchen dn_kit,
             gcf.gcf_dyn_petroleum dn_tec,
             gcf.gcf_dyn_early_age_system dn_age_early_system,
             gcf.gcf_dyn_early_age_serie dn_age_early_serie,
             gcf.gcf_dyn_late_age_system dn_age_late_system,
             gcf.gcf_dyn_late_age_serie dn_age_late_serie,
             gcf.gcf_dyn_preservation dn_prv,
             gcf.gcf_dyn_pathways dn_mig,
             gcf.gcf_dyn_migration_age_system dn_migration_age_system,
             gcf.gcf_dyn_migration_age_serie dn_migration_age_serie,

             s2.ls2d_low_estimate s2_low,
             s2.ls2d_best_estimate s2_best,
             s2.ls2d_hight_estimate s2_high,

             gl.lsgf_low_estimate gl_low,
             gl.lsgf_best_estimate gl_best,
             gl.lsgf_high_estimate gl_high,

             gh.lsgc_low_estimate gh_low,
             gh.lsgc_best_estimate gh_best,
             gh.lsgc_high_estimate gh_high,

             gr.lsgv_low_estimate gr_low,
             gr.lsgv_best_estimate gr_best,
             gr.lsgv_high_estimate gr_high,

             el.lsel_low_estimate el_low,
             el.lsel_best_estimate el_best,
             el.lsel_high_estimate el_high,

             rs.lsrt_low_estimate rs_low,
             rs.lsrt_best_estimate rs_best,
             rs.lsrt_high_estimate rs_high,

             ot.lsor_low_estimate ot_low,
             ot.lsor_best_estimate ot_best,
             ot.lsor_high_estimate ot_high

            FROM
             rsc_lead ld
             LEFT JOIN rsc_lead_2d s2 ON s2.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_geological gl ON gl.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_geochemistry gh ON gh.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_gravity gr ON gr.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_electromagnetic el ON el.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_resistivity rs ON rs.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_other ot ON ot.lead_id = ld.lead_id
             LEFT JOIN rsc_play pl ON pl.play_id = ld.play_id
             LEFT JOIN rsc_gcf gcf ON gcf.gcf_id = ld.gcf_id
             LEFT JOIN rsc_gcf pgcf ON pgcf.gcf_id = pl.gcf_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND ld.lead_is_deleted = 0
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        $sql = $sql . " ORDER BY wk_id, structure";

        $header = array(
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Latitude',
            'Longitude',
            'Struktur',
            'Nama Play',
            'Low Estimate',
            'Best Estimate',
            'High Estimate',
            'Source Rock Data',
            'Source Rock - Age',
            'Source Rock - Age Serie',
            'Source Rock - Formation',
            'Source Rock - Formation Serie',
            'Source Rock - Kerogen Type',
            'Source Rock - Capacity (TOC)',
            'Source Rock - Heat Flow Unit (HFU)',
            'Source Rock - Distribution',
            'Source Rock - Continuity',
            'Source Rock - Maturity',
            'Source Rock - Other',
            'Reservoir Data',
            'Reservoir - Age',
            'Reservoir - Age Serie',
            'Reservoir - Formation',
            'Reservoir - Formation Serie',
            'Reservoir - Depositional Setting',
            'Reservoir - Depositional Environment',
            'Reservoir - Distribution',
            'Reservoir - Continuity',
            'Reservoir - Lithology',
            'Reservoir - Primary Porosity',
            'Reservoir - Secondary Porosity',
            'Trap Data',
            'Trap - Sealing Age',
            'Trap - Sealing Age Serie',
            'Trap - Sealing Formation',
            'Trap - Sealing Formation Serie',
            'Trap - Sealing Distribution',
            'Trap - Sealing Continuity',
            'Trap - Sealing Type',
            'Trap - Age',
            'Trap - Age Serie',
            'Trap - Geometry',
            'Trap - Type',
            'Trap - Closure Type',
            'Dynamic Data',
            'Dynamic - Authenticate Migration',
            'Dynamic - Trap Position due to Kitchen',
            'Dynamic - Tectonic Order',
            'Dynamic - Tectonic Regime (Earliest)',
            'Dynamic - Tectonic Regime Serie (Earliest)',
            'Dynamic - Tectonic Regime (Latest)',
            'Dynamic - Tectonic Regime Serie (Latest)',
            'Dynamic - Preservation',
            'Dynamic - Migration Pathways',
            'Dynamic - Migration Age',
            'Dynamic - Migration Age Serie',
            'GCF P Source Rock',
            'GCF P Reservoir',
            'GCF P Trap',
            'GCF P Dynamic',
            'GCF',
            'GCF Normal',
        );

        return array('sql' => $sql, 'header' => $header);
    }

    public function rekap_drillable_sql($exc_terminate=false) {
        $sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             pr.structure_name structure,
             pr.prospect_latitude,
             pr.prospect_longitude,
             pgcf.gcf_res_lithology play_litho,
             pgcf.gcf_res_formation_serie play_form_serie,
             pgcf.gcf_res_formation play_form,
             pgcf.gcf_res_age_serie play_age_serie,
             pgcf.gcf_res_age_system play_age,
             pgcf.gcf_res_depos_env play_env,
             pgcf.gcf_trap_type play_trap,

             gcf.gcf_is_sr sr,
             gcf.gcf_is_res re,
             gcf.gcf_is_trap tr,
             gcf.gcf_is_dyn dn,

             gcf.gcf_sr_age_system sr_age_system,
             gcf.gcf_sr_age_serie sr_age_serie,
             gcf.gcf_sr_formation sr_formation,
             gcf.gcf_sr_formation_serie sr_formation_serie,
             gcf.gcf_sr_kerogen sr_ker,
             gcf.gcf_sr_toc sr_toc,
             gcf.gcf_sr_hfu sr_hfu,
             gcf.gcf_sr_distribution sr_distribution,
             gcf.gcf_sr_continuity sr_continuity,
             gcf.gcf_sr_maturity sr_mat,
             gcf.gcf_sr_otr sr_otr,

             gcf.gcf_res_age_system re_age_system,
             gcf.gcf_res_age_serie re_age_serie,
             gcf.gcf_res_formation re_formation,
             gcf.gcf_res_formation_serie re_formation_serie,
             gcf.gcf_res_depos_set re_depos_set,
             gcf.gcf_res_depos_env re_depos_env,
             gcf.gcf_res_distribution re_dis,
             gcf.gcf_res_continuity re_continuity,
             gcf.gcf_res_lithology re_lit,
             gcf.gcf_res_por_secondary re_sec,
             gcf.gcf_res_por_primary re_pri,

             gcf.gcf_trap_seal_age_system tr_seal_age_system,
             gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             gcf.gcf_trap_seal_formation tr_seal_formation,
             gcf.gcf_trap_seal_formation_serie tr_seal_formation_serie,
             gcf.gcf_trap_seal_distribution tr_sdi,
             gcf.gcf_trap_seal_continuity tr_scn,
             gcf.gcf_trap_seal_type tr_stp,
             gcf.gcf_trap_age_system tr_age_system,
             gcf.gcf_trap_age_serie tr_age_serie,
             gcf.gcf_trap_geometry tr_geo,
             gcf.gcf_trap_type tr_trp,
             gcf.gcf_trap_closure tr_closure,

             gcf.gcf_dyn_migration dn_migration,
             gcf.gcf_dyn_kitchen dn_kit,
             gcf.gcf_dyn_petroleum dn_tec,
             gcf.gcf_dyn_early_age_system dn_age_early_system,
             gcf.gcf_dyn_early_age_serie dn_age_early_serie,
             gcf.gcf_dyn_late_age_system dn_age_late_system,
             gcf.gcf_dyn_late_age_serie dn_age_late_serie,
             gcf.gcf_dyn_preservation dn_prv,
             gcf.gcf_dyn_pathways dn_mig,
             gcf.gcf_dyn_migration_age_system dn_migration_age_system,
             gcf.gcf_dyn_migration_age_serie dn_migration_age_serie,

             dr.dr_por_p90 por_p90,
             dr.dr_por_p50 por_p50,
             dr.dr_por_p10 por_p10,
             dr.dr_satur_p90 sat_p90,
             dr.dr_satur_p50 sat_p50,
             dr.dr_satur_p10 sat_p10,

             s2.s2d_vol_p90_area s2_area_p90,
             s3.s3d_vol_p90_area s3_area_p90,
             gl.sgf_p90_area gl_area_p90,
             gh.sgc_vol_p90_area gh_area_p90,
             gr.sgv_vol_p90_area gr_area_p90,
             el.sel_vol_p90_area el_area_p90,
             rs.rst_vol_p90_area rs_area_p90,
             ot.sor_vol_p90_area ot_area_p90,

             s2.s2d_vol_p50_area s2_area_p50,
             s3.s3d_vol_p50_area s3_area_p50,
             gl.sgf_p50_area gl_area_p50,
             gh.sgc_vol_p50_area gh_area_p50,
             gr.sgv_vol_p50_area gr_area_p50,
             el.sel_vol_p50_area el_area_p50,
             rs.rst_vol_p50_area rs_area_p50,
             ot.sor_vol_p50_area ot_area_p50,

             s2.s2d_vol_p10_area s2_area_p10,
             s3.s3d_vol_p10_area s3_area_p10,
             gl.sgf_p10_area gl_area_p10,
             gh.sgc_vol_p10_area gh_area_p10,
             gr.sgv_vol_p10_area gr_area_p10,
             el.sel_vol_p10_area el_area_p10,
             rs.rst_vol_p10_area rs_area_p10,
             ot.sor_vol_p10_area ot_area_p10,

             s2.s2d_net_p90_thickness s2_net_p90,
             s2.s2d_net_p50_thickness s2_net_p50,
             s2.s2d_net_p10_thickness s2_net_p10,
             s3.s3d_net_p90_thickness s3_net_p90,
             s3.s3d_net_p50_thickness s3_net_p50,
             s3.s3d_net_p10_thickness s3_net_p10

            FROM
             rsc_prospect pr
             LEFT JOIN rsc_drillable dr ON dr.prospect_id = pr.prospect_id
             LEFT JOIN rsc_seismic_2d s2 ON s2.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geological gl ON gl.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geochemistry gh ON gh.prospect_id = pr.prospect_id
             LEFT JOIN rsc_gravity gr ON gr.prospect_id = pr.prospect_id
             LEFT JOIN rsc_electromagnetic el ON el.prospect_id = pr.prospect_id
             LEFT JOIN rsc_resistivity rs ON rs.prospect_id = pr.prospect_id
             LEFT JOIN rsc_other ot ON ot.prospect_id = pr.prospect_id
             LEFT JOIN rsc_seismic_3d s3 ON s3.prospect_id = pr.prospect_id
             LEFT JOIN rsc_play pl ON pl.play_id = pr.play_id
             LEFT JOIN rsc_gcf gcf ON gcf.gcf_id = pr.gcf_id
             LEFT JOIN rsc_gcf pgcf ON pgcf.gcf_id = pl.gcf_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
             AND pr.prospect_type = 'drillable'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        $sql = $sql . " ORDER BY wk_id, structure";

        $header = array(
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Latitude',
            'Longitude',
            'Struktur',
            'Nama Play',
            'Area P90',
            'Area P50',
            'Area P10',
            'Thickness P90',
            'Thickness P50',
            'Thickness P10',
            'Porositas P90',
            'Porositas P50',
            'Porositas P10',
            '1-Sw P90',
            '1-Sw P50',
            '1-Sw P10',
            'OOIP P90',
            'OOIP P50',
            'OOIP P10',
            'OGIP P90',
            'OGIP P50',
            'OGIP P10',
            'Source Rock Data',
            'Source Rock - Age',
            'Source Rock - Age Serie',
            'Source Rock - Formation',
            'Source Rock - Formation Serie',
            'Source Rock - Kerogen Type',
            'Source Rock - Capacity (TOC)',
            'Source Rock - Heat Flow Unit (HFU)',
            'Source Rock - Distribution',
            'Source Rock - Continuity',
            'Source Rock - Maturity',
            'Source Rock - Other',
            'Reservoir Data',
            'Reservoir - Age',
            'Reservoir - Age Serie',
            'Reservoir - Formation',
            'Reservoir - Formation Serie',
            'Reservoir - Depositional Setting',
            'Reservoir - Depositional Environment',
            'Reservoir - Distribution',
            'Reservoir - Continuity',
            'Reservoir - Lithology',
            'Reservoir - Primary Porosity',
            'Reservoir - Secondary Porosity',
            'Trap Data',
            'Trap - Sealing Age',
            'Trap - Sealing Age Serie',
            'Trap - Sealing Formation',
            'Trap - Sealing Formation Serie',
            'Trap - Sealing Distribution',
            'Trap - Sealing Continuity',
            'Trap - Sealing Type',
            'Trap - Age',
            'Trap - Age Serie',
            'Trap - Geometry',
            'Trap - Type',
            'Trap - Closure Type',
            'Dynamic Data',
            'Dynamic - Authenticate Migration',
            'Dynamic - Trap Position due to Kitchen',
            'Dynamic - Tectonic Order',
            'Dynamic - Tectonic Regime (Earliest)',
            'Dynamic - Tectonic Regime Serie (Earliest)',
            'Dynamic - Tectonic Regime (Latest)',
            'Dynamic - Tectonic Regime Serie (Latest)',
            'Dynamic - Preservation',
            'Dynamic - Migration Pathways',
            'Dynamic - Migration Age',
            'Dynamic - Migration Age Serie',
            'GCF P Source Rock',
            'GCF P Reservoir',
            'GCF P Trap',
            'GCF P Dynamic',
            'GCF',
            'GCF Normal',
        );

        return array('sql' => $sql, 'header' => $header);
    }

    public function rekap_postdrill_sql($exc_terminate=false) {
        $sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             pr.structure_name structure,
             pr.prospect_latitude,
             pr.prospect_longitude,
             wl.wl_name well,
             wl.wl_formation well_formation,
             wl.wl_latitude well_latitude,
             wl.wl_longitude well_longitude,
             wl.wl_result well_result,
             wz.zone_hc_oil_show zone_oil_show,
             wz.zone_hc_gas_show zone_gas_show,
             pgcf.gcf_res_lithology play_litho,
             pgcf.gcf_res_formation_serie play_form_serie,
             pgcf.gcf_res_formation play_form,
             pgcf.gcf_res_age_serie play_age_serie,
             pgcf.gcf_res_age_system play_age,
             pgcf.gcf_res_depos_env play_env,
             pgcf.gcf_trap_type play_trap,

             gcf.gcf_is_sr sr,
             gcf.gcf_is_res re,
             gcf.gcf_is_trap tr,
             gcf.gcf_is_dyn dn,

             gcf.gcf_sr_age_system sr_age_system,
             gcf.gcf_sr_age_serie sr_age_serie,
             gcf.gcf_sr_formation sr_formation,
             gcf.gcf_sr_formation_serie sr_formation_serie,
             gcf.gcf_sr_kerogen sr_ker,
             gcf.gcf_sr_toc sr_toc,
             gcf.gcf_sr_hfu sr_hfu,
             gcf.gcf_sr_distribution sr_distribution,
             gcf.gcf_sr_continuity sr_continuity,
             gcf.gcf_sr_maturity sr_mat,
             gcf.gcf_sr_otr sr_otr,

             gcf.gcf_res_age_system re_age_system,
             gcf.gcf_res_age_serie re_age_serie,
             gcf.gcf_res_formation re_formation,
             gcf.gcf_res_formation_serie re_formation_serie,
             gcf.gcf_res_depos_set re_depos_set,
             gcf.gcf_res_depos_env re_depos_env,
             gcf.gcf_res_distribution re_dis,
             gcf.gcf_res_continuity re_continuity,
             gcf.gcf_res_lithology re_lit,
             gcf.gcf_res_por_secondary re_sec,
             gcf.gcf_res_por_primary re_pri,

             gcf.gcf_trap_seal_age_system tr_seal_age_system,
             gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             gcf.gcf_trap_seal_formation tr_seal_formation,
             gcf.gcf_trap_seal_formation_serie tr_seal_formation_serie,
             gcf.gcf_trap_seal_distribution tr_sdi,
             gcf.gcf_trap_seal_continuity tr_scn,
             gcf.gcf_trap_seal_type tr_stp,
             gcf.gcf_trap_age_system tr_age_system,
             gcf.gcf_trap_age_serie tr_age_serie,
             gcf.gcf_trap_geometry tr_geo,
             gcf.gcf_trap_type tr_trp,
             gcf.gcf_trap_closure tr_closure,

             gcf.gcf_dyn_migration dn_migration,
             gcf.gcf_dyn_kitchen dn_kit,
             gcf.gcf_dyn_petroleum dn_tec,
             gcf.gcf_dyn_early_age_system dn_age_early_system,
             gcf.gcf_dyn_early_age_serie dn_age_early_serie,
             gcf.gcf_dyn_late_age_system dn_age_late_system,
             gcf.gcf_dyn_late_age_serie dn_age_late_serie,
             gcf.gcf_dyn_preservation dn_prv,
             gcf.gcf_dyn_pathways dn_mig,
             gcf.gcf_dyn_migration_age_system dn_migration_age_system,
             gcf.gcf_dyn_migration_age_serie dn_migration_age_serie,

             s2.s2d_vol_p90_area s2_area_p90,
             s3.s3d_vol_p90_area s3_area_p90,
             gl.sgf_p90_area gl_area_p90,
             gh.sgc_vol_p90_area gh_area_p90,
             gr.sgv_vol_p90_area gr_area_p90,
             el.sel_vol_p90_area el_area_p90,
             rs.rst_vol_p90_area rs_area_p90,
             ot.sor_vol_p90_area ot_area_p90,

             s2.s2d_vol_p50_area s2_area_p50,
             s3.s3d_vol_p50_area s3_area_p50,
             gl.sgf_p50_area gl_area_p50,
             gh.sgc_vol_p50_area gh_area_p50,
             gr.sgv_vol_p50_area gr_area_p50,
             el.sel_vol_p50_area el_area_p50,
             rs.rst_vol_p50_area rs_area_p50,
             ot.sor_vol_p50_area ot_area_p50,

             s2.s2d_vol_p10_area s2_area_p10,
             s3.s3d_vol_p10_area s3_area_p10,
             gl.sgf_p10_area gl_area_p10,
             gh.sgc_vol_p10_area gh_area_p10,
             gr.sgv_vol_p10_area gr_area_p10,
             el.sel_vol_p10_area el_area_p10,
             rs.rst_vol_p10_area rs_area_p10,
             ot.sor_vol_p10_area ot_area_p10,

             s2.s2d_net_p90_thickness s2_net_p90,
             s2.s2d_net_p50_thickness s2_net_p50,
             s2.s2d_net_p10_thickness s2_net_p10,
             s3.s3d_net_p90_thickness s3_net_p90,
             s3.s3d_net_p50_thickness s3_net_p50,
             s3.s3d_net_p10_thickness s3_net_p10,

             wz.zone_clastic_p90_por wz_por_p90_clastic,
             wz.zone_clastic_p50_por wz_por_p50_clastic,
             wz.zone_clastic_p10_por wz_por_p10_clastic,
             wz.zone_clastic_p90_satur wz_sat_p90_clastic,
             wz.zone_clastic_p50_satur wz_sat_p50_clastic,
             wz.zone_clastic_p10_satur wz_sat_p10_clastic,

             wz.zone_carbo_p90_por wz_por_p90_carbo,
             wz.zone_carbo_p50_por wz_por_p50_carbo,
             wz.zone_carbo_p10_por wz_por_p10_carbo,
             wz.zone_carbo_p90_satur wz_sat_p90_carbo,
             wz.zone_carbo_p50_satur wz_sat_p50_carbo,
             wz.zone_carbo_p10_satur wz_sat_p10_carbo,

             wz.zone_fvf_oil_p90 boi_p90,
             wz.zone_fvf_oil_p50 boi_p50,
             wz.zone_fvf_oil_p10 boi_p10,
             wz.zone_fvf_gas_p90 bgi_p90,
             wz.zone_fvf_gas_p50 bgi_p50,
             wz.zone_fvf_gas_p10 bgi_p10

            FROM
             rsc_prospect pr
             LEFT JOIN rsc_seismic_2d s2 ON s2.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geological gl ON gl.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geochemistry gh ON gh.prospect_id = pr.prospect_id
             LEFT JOIN rsc_gravity gr ON gr.prospect_id = pr.prospect_id
             LEFT JOIN rsc_electromagnetic el ON el.prospect_id = pr.prospect_id
             LEFT JOIN rsc_resistivity rs ON rs.prospect_id = pr.prospect_id
             LEFT JOIN rsc_other ot ON ot.prospect_id = pr.prospect_id
             LEFT JOIN rsc_seismic_3d s3 ON s3.prospect_id = pr.prospect_id
             LEFT JOIN rsc_play pl ON pl.play_id = pr.play_id
             LEFT JOIN rsc_gcf gcf ON gcf.gcf_id = pr.gcf_id
             LEFT JOIN rsc_gcf pgcf ON pgcf.gcf_id = pl.gcf_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id
             LEFT JOIN rsc_well wl ON (wl.prospect_id = pr.prospect_id
               AND wl.wk_id = pl.wk_id AND wl.prospect_type = 'postdrill')
             LEFT JOIN rsc_wellzone wz ON wz.wl_id = wl.wl_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
             AND pr.prospect_type = 'postdrill'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        $sql = $sql . " ORDER BY wk_id, structure";

        $header = array(
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Latitude',
            'Longitude',
            'Struktur',
            'Nama Play',
            'Well',
            'Well Formation',
            'Well Result',
            'Area P90',
            'Area P50',
            'Area P10',
            'Thickness P90',
            'Thickness P50',
            'Thickness P10',
            'Porositas P90',
            'Porositas P50',
            'Porositas P10',
            '1-Sw P90',
            '1-Sw P50',
            '1-Sw P10',
            'OOIP P90',
            'OOIP P50',
            'OOIP P10',
            'Boi P50',
            'STOIP P90',
            'STOIP P50',
            'STOIP P10',
            'OGIP P90',
            'OGIP P50',
            'OGIP P10',
            'Bgi P50',
            'IGIP P90',
            'IGIP P50',
            'IGIP P10',
            'Source Rock Data',
            'Source Rock - Age',
            'Source Rock - Age Serie',
            'Source Rock - Formation',
            'Source Rock - Formation Serie',
            'Source Rock - Kerogen Type',
            'Source Rock - Capacity (TOC)',
            'Source Rock - Heat Flow Unit (HFU)',
            'Source Rock - Distribution',
            'Source Rock - Continuity',
            'Source Rock - Maturity',
            'Source Rock - Other',
            'Reservoir Data',
            'Reservoir - Age',
            'Reservoir - Age Serie',
            'Reservoir - Formation',
            'Reservoir - Formation Serie',
            'Reservoir - Depositional Setting',
            'Reservoir - Depositional Environment',
            'Reservoir - Distribution',
            'Reservoir - Continuity',
            'Reservoir - Lithology',
            'Reservoir - Primary Porosity',
            'Reservoir - Secondary Porosity',
            'Trap Data',
            'Trap - Sealing Age',
            'Trap - Sealing Age Serie',
            'Trap - Sealing Formation',
            'Trap - Sealing Formation Serie',
            'Trap - Sealing Distribution',
            'Trap - Sealing Continuity',
            'Trap - Sealing Type',
            'Trap - Age',
            'Trap - Age Serie',
            'Trap - Geometry',
            'Trap - Type',
            'Trap - Closure Type',
            'Dynamic Data',
            'Dynamic - Authenticate Migration',
            'Dynamic - Trap Position due to Kitchen',
            'Dynamic - Tectonic Order',
            'Dynamic - Tectonic Regime (Earliest)',
            'Dynamic - Tectonic Regime Serie (Earliest)',
            'Dynamic - Tectonic Regime (Latest)',
            'Dynamic - Tectonic Regime Serie (Latest)',
            'Dynamic - Preservation',
            'Dynamic - Migration Pathways',
            'Dynamic - Migration Age',
            'Dynamic - Migration Age Serie',
            'GCF P Source Rock',
            'GCF P Reservoir',
            'GCF P Trap',
            'GCF P Dynamic',
            'GCF',
            'GCF Normal',
        );

        return array('sql' => $sql, 'header' => $header);
    }

    public function rekap_discovery_sql($exc_terminate=false) {
        $sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             pr.structure_name structure,
             pr.prospect_latitude,
             pr.prospect_longitude,
             pgcf.gcf_res_lithology play_litho,
             pgcf.gcf_res_formation_serie play_form_serie,
             pgcf.gcf_res_formation play_form,
             pgcf.gcf_res_age_serie play_age_serie,
             pgcf.gcf_res_age_system play_age,
             pgcf.gcf_res_depos_env play_env,
             pgcf.gcf_trap_type play_trap,

             gcf.gcf_is_sr sr,
             gcf.gcf_is_res re,
             gcf.gcf_is_trap tr,
             gcf.gcf_is_dyn dn,

             gcf.gcf_sr_age_system sr_age_system,
             gcf.gcf_sr_age_serie sr_age_serie,
             gcf.gcf_sr_formation sr_formation,
             gcf.gcf_sr_formation_serie sr_formation_serie,
             gcf.gcf_sr_kerogen sr_ker,
             gcf.gcf_sr_toc sr_toc,
             gcf.gcf_sr_hfu sr_hfu,
             gcf.gcf_sr_distribution sr_distribution,
             gcf.gcf_sr_continuity sr_continuity,
             gcf.gcf_sr_maturity sr_mat,
             gcf.gcf_sr_otr sr_otr,

             gcf.gcf_res_age_system re_age_system,
             gcf.gcf_res_age_serie re_age_serie,
             gcf.gcf_res_formation re_formation,
             gcf.gcf_res_formation_serie re_formation_serie,
             gcf.gcf_res_depos_set re_depos_set,
             gcf.gcf_res_depos_env re_depos_env,
             gcf.gcf_res_distribution re_dis,
             gcf.gcf_res_continuity re_continuity,
             gcf.gcf_res_lithology re_lit,
             gcf.gcf_res_por_secondary re_sec,
             gcf.gcf_res_por_primary re_pri,

             gcf.gcf_trap_seal_age_system tr_seal_age_system,
             gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             gcf.gcf_trap_seal_formation tr_seal_formation,
             gcf.gcf_trap_seal_formation_serie tr_seal_formation_serie,
             gcf.gcf_trap_seal_distribution tr_sdi,
             gcf.gcf_trap_seal_continuity tr_scn,
             gcf.gcf_trap_seal_type tr_stp,
             gcf.gcf_trap_age_system tr_age_system,
             gcf.gcf_trap_age_serie tr_age_serie,
             gcf.gcf_trap_geometry tr_geo,
             gcf.gcf_trap_type tr_trp,
             gcf.gcf_trap_closure tr_closure,

             gcf.gcf_dyn_migration dn_migration,
             gcf.gcf_dyn_kitchen dn_kit,
             gcf.gcf_dyn_petroleum dn_tec,
             gcf.gcf_dyn_early_age_system dn_age_early_system,
             gcf.gcf_dyn_early_age_serie dn_age_early_serie,
             gcf.gcf_dyn_late_age_system dn_age_late_system,
             gcf.gcf_dyn_late_age_serie dn_age_late_serie,
             gcf.gcf_dyn_preservation dn_prv,
             gcf.gcf_dyn_pathways dn_mig,
             gcf.gcf_dyn_migration_age_system dn_migration_age_system,
             gcf.gcf_dyn_migration_age_serie dn_migration_age_serie,

             s2.s2d_vol_p90_area s2_area_p90,
             s3.s3d_vol_p90_area s3_area_p90,
             gl.sgf_p90_area gl_area_p90,
             gh.sgc_vol_p90_area gh_area_p90,
             gr.sgv_vol_p90_area gr_area_p90,
             el.sel_vol_p90_area el_area_p90,
             rs.rst_vol_p90_area rs_area_p90,
             ot.sor_vol_p90_area ot_area_p90,

             s2.s2d_vol_p50_area s2_area_p50,
             s3.s3d_vol_p50_area s3_area_p50,
             gl.sgf_p50_area gl_area_p50,
             gh.sgc_vol_p50_area gh_area_p50,
             gr.sgv_vol_p50_area gr_area_p50,
             el.sel_vol_p50_area el_area_p50,
             rs.rst_vol_p50_area rs_area_p50,
             ot.sor_vol_p50_area ot_area_p50,

             s2.s2d_vol_p10_area s2_area_p10,
             s3.s3d_vol_p10_area s3_area_p10,
             gl.sgf_p10_area gl_area_p10,
             gh.sgc_vol_p10_area gh_area_p10,
             gr.sgv_vol_p10_area gr_area_p10,
             el.sel_vol_p10_area el_area_p10,
             rs.rst_vol_p10_area rs_area_p10,
             ot.sor_vol_p10_area ot_area_p10,

             s2.s2d_net_p90_thickness s2_net_p90,
             s2.s2d_net_p50_thickness s2_net_p50,
             s2.s2d_net_p10_thickness s2_net_p10,
             s3.s3d_net_p90_thickness s3_net_p90,
             s3.s3d_net_p50_thickness s3_net_p50,
             s3.s3d_net_p10_thickness s3_net_p10

            FROM
             rsc_prospect pr
             LEFT JOIN rsc_seismic_2d s2 ON s2.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geological gl ON gl.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geochemistry gh ON gh.prospect_id = pr.prospect_id
             LEFT JOIN rsc_gravity gr ON gr.prospect_id = pr.prospect_id
             LEFT JOIN rsc_electromagnetic el ON el.prospect_id = pr.prospect_id
             LEFT JOIN rsc_resistivity rs ON rs.prospect_id = pr.prospect_id
             LEFT JOIN rsc_other ot ON ot.prospect_id = pr.prospect_id
             LEFT JOIN rsc_seismic_3d s3 ON s3.prospect_id = pr.prospect_id
             LEFT JOIN rsc_play pl ON pl.play_id = pr.play_id
             LEFT JOIN rsc_gcf gcf ON gcf.gcf_id = pr.gcf_id
             LEFT JOIN rsc_gcf pgcf ON pgcf.gcf_id = pl.gcf_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
             AND pr.prospect_type = 'discovery'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        $sql = $sql . " ORDER BY wk_id, structure";

        $header = array(
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Latitude',
            'Longitude',
            'Struktur',
            'Nama Play',
            'Latitude',
            'Longitude',
            'Area P90',
            'Area P50',
            'Area P10',
            'Thickness P90',
            'Thickness P50',
            'Thickness P10',
        );

        return array('sql' => $sql, 'header' => $header);
    }

    public function rekap_well_discovery_sql($exc_terminate=false) {
        $sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             pr.structure_name structure,
             wl.wl_name well,
             wl.wl_formation well_formation,
             wl.wl_latitude well_latitude,
             wl.wl_longitude well_longitude,
             wl.wl_result well_result,

             wz.zone_name wz_name,
             wz.zone_clastic_p90_por wz_por_p90_clastic,
             wz.zone_clastic_p50_por wz_por_p50_clastic,
             wz.zone_clastic_p10_por wz_por_p10_clastic,
             wz.zone_clastic_p90_satur wz_sat_p90_clastic,
             wz.zone_clastic_p50_satur wz_sat_p50_clastic,
             wz.zone_clastic_p10_satur wz_sat_p10_clastic,

             wz.zone_carbo_p90_por wz_por_p90_carbo,
             wz.zone_carbo_p50_por wz_por_p50_carbo,
             wz.zone_carbo_p10_por wz_por_p10_carbo,
             wz.zone_carbo_p90_satur wz_sat_p90_carbo,
             wz.zone_carbo_p50_satur wz_sat_p50_carbo,
             wz.zone_carbo_p10_satur wz_sat_p10_carbo,

             wz.zone_fvf_oil_p90 boi_p90,
             wz.zone_fvf_oil_p50 boi_p50,
             wz.zone_fvf_oil_p10 boi_p10,
             wz.zone_fvf_gas_p90 bgi_p90,
             wz.zone_fvf_gas_p50 bgi_p50,
             wz.zone_fvf_gas_p10 bgi_p10,

             wz.zone_prod_gasoil_ratio ratio,
             wz.zone_prod_well_radius radius,
             wz.zone_thickness wz_thickness,
             wz.zone_average_por wz_avg_por,
             wz.zone_initial_water wz_initial_sw,
             wz.zone_hc_oil_show wz_oil_show,
             wz.zone_hc_gas_show wz_gas_show,
             wz.zone_prod_second wz_second_por,
             wz.zone_area wz_area

            FROM
             rsc_prospect pr
             LEFT JOIN rsc_play pl ON pl.play_id = pr.play_id
             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id
             LEFT JOIN rsc_well wl ON (wl.prospect_id = pr.prospect_id
               AND wl.wk_id = pl.wk_id AND wl.prospect_type = 'discovery')
             LEFT JOIN rsc_wellzone wz ON wz.wl_id = wl.wl_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
             AND pr.prospect_type = 'discovery'
        ";

        if ($exc_terminate == true) {
            $sql = $sql . " AND wk.wk_stage <> 'Terminasi'";
        }

        $sql = $sql . " ORDER BY wk_id, structure";

        $header = array(
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Struktur',
            'Well',
            'Well Formation',
            'Well Latitude',
            'Well Longitude',
            'Well Result',
            'Well Gas Oil Ratio',
            'Well Radius',
            'Zone Name',
            'Zone Area',
            'Zone Thickness',
            'Zone Average Porosity',
            'Zone Secondary Porosity',
            'Zone Initial Water Saturation',
            'Zone Oil Show',
            'Zone Gas Show',
            'Porositas P90',
            'Porositas P50',
            'Porositas P10',
            '1-Sw P90',
            '1-Sw P50',
            '1-Sw P10',
            'Boi P90',
            'Boi P50',
            'Boi P10',
            'Bgi P90',
            'Bgi P50',
            'Bgi P10',
        );

        return array('sql' => $sql, 'header' => $header);
    }

    public function rekap_all_data_sql() {
        $ld_sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             ld.structure_name closure,
             'lead' resource_stage,

             pl_gcf.gcf_res_lithology pl_litho,
             pl_gcf.gcf_res_formation pl_form_serie,
             pl_gcf.gcf_res_formation pl_form,
             pl_gcf.gcf_res_age_serie pl_age_serie,
             pl_gcf.gcf_res_age_system pl_age,
             pl_gcf.gcf_res_depos_env pl_env,
             pl_gcf.gcf_trap_type pl_trap,

             ld.lead_clarified clarified,
             ld.lead_latitude latitude,
             ld.lead_longitude longitude,
             ld.lead_shore shore,
             ld.lead_terrain terrain,
             ld.lead_near_field near_field,
             ld.lead_near_infra_structure near_infra_structure,

             ld_gcf.gcf_is_sr sr,
             ld_gcf.gcf_is_res re,
             ld_gcf.gcf_is_trap tr,
             ld_gcf.gcf_is_dyn dn,
             ld_gcf.gcf_sr_kerogen sr_ker,
             ld_gcf.gcf_sr_toc sr_toc,
             ld_gcf.gcf_sr_hfu sr_hfu,
             ld_gcf.gcf_sr_maturity sr_mat,
             ld_gcf.gcf_sr_otr sr_otr,
             ld_gcf.gcf_res_distribution re_dis,
             ld_gcf.gcf_res_lithology re_lit,
             ld_gcf.gcf_res_por_secondary re_sec,
             ld_gcf.gcf_res_por_primary re_pri,
             ld_gcf.gcf_trap_seal_distribution tr_sdi,
             ld_gcf.gcf_trap_seal_continuity tr_scn,
             ld_gcf.gcf_trap_seal_type tr_stp,
             ld_gcf.gcf_trap_geometry tr_geo,
             ld_gcf.gcf_trap_type tr_trp,
             ld_gcf.gcf_dyn_kitchen dn_kit,
             ld_gcf.gcf_dyn_petroleum dn_tec,
             ld_gcf.gcf_dyn_preservation dn_prv,
             ld_gcf.gcf_dyn_pathways dn_mig,
             ld_gcf.gcf_sr_remark sr_remark,
             ld_gcf.gcf_res_remark re_remark,
             ld_gcf.gcf_trap_remark tr_remark,
             ld_gcf.gcf_dyn_remark dn_remark,
             ld_gcf.gcf_sr_age_serie sr_age_serie,
             ld_gcf.gcf_sr_age_system sr_age_system,
             ld_gcf.gcf_sr_formation_serie sr_form_serie,
             ld_gcf.gcf_sr_formation sr_form_system,
             ld_gcf.gcf_sr_distribution sr_distribution,
             ld_gcf.gcf_sr_continuity sr_continuity,
             ld_gcf.gcf_res_age_serie re_age_serie,
             ld_gcf.gcf_res_age_system re_age_system,
             ld_gcf.gcf_res_formation_serie re_form_serie,
             ld_gcf.gcf_res_formation re_form_system,
             ld_gcf.gcf_res_depos_set re_depos_set,
             ld_gcf.gcf_res_depos_env re_depos_env,
             ld_gcf.gcf_res_continuity re_continuity,
             ld_gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             ld_gcf.gcf_trap_seal_age_system tr_seal_age_system,
             ld_gcf.gcf_trap_seal_formation_serie tr_form_serie,
             ld_gcf.gcf_trap_seal_formation tr_form_system,
             ld_gcf.gcf_trap_age_serie tr_age_serie,
             ld_gcf.gcf_trap_age_system tr_age_system,
             ld_gcf.gcf_trap_closure tr_closure,
             ld_gcf.gcf_dyn_migration dn_migration,
             ld_gcf.gcf_dyn_early_age_serie dn_early_age_serie,
             ld_gcf.gcf_dyn_early_age_system dn_early_age_system,
             ld_gcf.gcf_dyn_late_age_serie dn_late_age_serie,
             ld_gcf.gcf_dyn_late_age_system dn_late_age_system,
             ld_gcf.gcf_dyn_migration_age_serie dn_migration_age_serie,
             ld_gcf.gcf_dyn_migration_age_system dn_migration_age_system,

             ld_s2.ls2d_year_survey ld_s2_year,
             ld_s2.ls2d_total_crossline ld_s2_crossline,
             ld_s2.ls2d_total_coverage ld_s2_coverage,
             ld_s2.ls2d_average_interval ld_s2_interval,
             ld_s2.ls2d_late_method ld_s2_method,
             ld_s2.ls2d_img_quality ld_s2_img,
             ld_s2.ls2d_remark ld_s2_remark,
             ld_s2.ls2d_low_estimate ld_s2_low,
             ld_s2.ls2d_best_estimate ld_s2_best,
             ld_s2.ls2d_hight_estimate ld_s2_high,

             ld_gl.lsgf_year_survey ld_gl_year,
             ld_gl.lsgf_survey_method ld_gl_method,
             ld_gl.lsgf_coverage_area ld_gl_area,
             ld_gl.lsgf_remark ld_gl_remark,
             ld_gl.lsgf_low_estimate ld_gl_low,
             ld_gl.lsgf_best_estimate ld_gl_best,
             ld_gl.lsgf_high_estimate ld_gl_high,

             ld_gh.lsgc_year_survey ld_gh_year,
             ld_gh.lsgc_range_interval ld_gh_interval,
             ld_gh.lsgc_remark ld_gh_remark,
             ld_gh.lsgc_low_estimate ld_gh_low,
             ld_gh.lsgc_best_estimate ld_gh_best,
             ld_gh.lsgc_high_estimate ld_gh_high,

             ld_gr.lsgv_year_survey ld_gr_year,
             ld_gr.lsgv_survey_method ld_gr_method,
             ld_gr.lsgv_remark ld_gr_remark,
             ld_gr.lsgv_low_estimate ld_gr_low,
             ld_gr.lsgv_best_estimate ld_gr_best,
             ld_gr.lsgv_high_estimate ld_gr_high,

             ld_el.lsel_year_survey ld_el_year,
             ld_el.lsel_survey_method ld_el_method,
             ld_el.lsel_coverage_area ld_el_area,
             ld_el.lsel_remark ld_el_remark,
             ld_el.lsel_low_estimate ld_el_low,
             ld_el.lsel_best_estimate ld_el_best,
             ld_el.lsel_high_estimate ld_el_high,

             ld_rs.lsrt_year_survey ld_rs_year,
             ld_rs.lsrt_survey_method ld_rs_method,
             ld_rs.lsrt_coverage_area ld_rs_area,
             ld_rs.lsrt_remark ld_rs_remark,
             ld_rs.lsrt_low_estimate ld_rs_low,
             ld_rs.lsrt_best_estimate ld_rs_best,
             ld_rs.lsrt_high_estimate ld_rs_high,

             ld_ot.lsor_year_survey ld_ot_year,
             ld_ot.lsor_low_estimate ld_ot_low,
             ld_ot.lsor_best_estimate ld_ot_best,
             ld_ot.lsor_high_estimate ld_ot_high

            FROM
             rsc_play pl
             LEFT JOIN rsc_lead ld ON ld.play_id = pl.play_id

             LEFT JOIN rsc_gcf pl_gcf ON pl_gcf.gcf_id = pl.gcf_id
             LEFT JOIN rsc_gcf ld_gcf ON ld_gcf.gcf_id = ld.gcf_id

             LEFT JOIN rsc_lead_2d ld_s2 ON ld_s2.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_geological ld_gl ON ld_gl.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_geochemistry ld_gh ON ld_gh.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_gravity ld_gr ON ld_gr.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_electromagnetic ld_el ON ld_el.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_resistivity ld_rs ON ld_rs.lead_id = ld.lead_id
             LEFT JOIN rsc_lead_other ld_ot ON ld_ot.lead_id = ld.lead_id

             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND ld.lead_is_deleted = 0
            ORDER BY wk_id
        ";

        $pr_sql = "
            SELECT
             bs.basin_name basin,
             pv.province_name province,
             ks.kkks_name kkks,
             wk.wk_id,
             wk.wk_name wk,
             wk.wk_stage stage,
             pr.structure_name closure,
             pr.prospect_type resource_stage,

             pl_gcf.gcf_res_lithology pl_litho,
             pl_gcf.gcf_res_formation pl_form_serie,
             pl_gcf.gcf_res_formation pl_form,
             pl_gcf.gcf_res_age_serie pl_age_serie,
             pl_gcf.gcf_res_age_system pl_age,
             pl_gcf.gcf_res_depos_env pl_env,
             pl_gcf.gcf_trap_type pl_trap,

             pr.prospect_clarified clarified,
             pr.prospect_latitude latitude,
             pr.prospect_longitude longitude,
             pr.prospect_shore shore,
             pr.prospect_terrain terrain,
             pr.prospect_near_field near_field,
             pr.prospect_near_infra_structure near_infra_structure,

             pr_gcf.gcf_is_sr sr,
             pr_gcf.gcf_is_res re,
             pr_gcf.gcf_is_trap tr,
             pr_gcf.gcf_is_dyn dn,
             pr_gcf.gcf_sr_kerogen sr_ker,
             pr_gcf.gcf_sr_toc sr_toc,
             pr_gcf.gcf_sr_hfu sr_hfu,
             pr_gcf.gcf_sr_maturity sr_mat,
             pr_gcf.gcf_sr_otr sr_otr,
             pr_gcf.gcf_res_distribution re_dis,
             pr_gcf.gcf_res_lithology re_lit,
             pr_gcf.gcf_res_por_secondary re_sec,
             pr_gcf.gcf_res_por_primary re_pri,
             pr_gcf.gcf_trap_seal_distribution tr_sdi,
             pr_gcf.gcf_trap_seal_continuity tr_scn,
             pr_gcf.gcf_trap_seal_type tr_stp,
             pr_gcf.gcf_trap_geometry tr_geo,
             pr_gcf.gcf_trap_type tr_trp,
             pr_gcf.gcf_dyn_kitchen dn_kit,
             pr_gcf.gcf_dyn_petroleum dn_tec,
             pr_gcf.gcf_dyn_preservation dn_prv,
             pr_gcf.gcf_dyn_pathways dn_mig,
             pr_gcf.gcf_sr_remark sr_remark,
             pr_gcf.gcf_res_remark re_remark,
             pr_gcf.gcf_trap_remark tr_remark,
             pr_gcf.gcf_dyn_remark dn_remark,
             pr_gcf.gcf_sr_age_serie sr_age_serie,
             pr_gcf.gcf_sr_age_system sr_age_system,
             pr_gcf.gcf_sr_formation_serie sr_form_serie,
             pr_gcf.gcf_sr_formation sr_form_system,
             pr_gcf.gcf_sr_distribution sr_distribution,
             pr_gcf.gcf_sr_continuity sr_continuity,
             pr_gcf.gcf_res_age_serie re_age_serie,
             pr_gcf.gcf_res_age_system re_age_system,
             pr_gcf.gcf_res_formation_serie re_form_serie,
             pr_gcf.gcf_res_formation re_form_system,
             pr_gcf.gcf_res_depos_set re_depos_set,
             pr_gcf.gcf_res_depos_env re_depos_env,
             pr_gcf.gcf_res_continuity re_continuity,
             pr_gcf.gcf_trap_seal_age_serie tr_seal_age_serie,
             pr_gcf.gcf_trap_seal_age_system tr_seal_age_system,
             pr_gcf.gcf_trap_seal_formation_serie tr_form_serie,
             pr_gcf.gcf_trap_seal_formation tr_form_system,
             pr_gcf.gcf_trap_age_serie tr_age_serie,
             pr_gcf.gcf_trap_age_system tr_age_system,
             pr_gcf.gcf_trap_closure tr_closure,
             pr_gcf.gcf_dyn_migration dn_migration,
             pr_gcf.gcf_dyn_early_age_serie dn_early_age_serie,
             pr_gcf.gcf_dyn_early_age_system dn_early_age_system,
             pr_gcf.gcf_dyn_late_age_serie dn_late_age_serie,
             pr_gcf.gcf_dyn_late_age_system dn_late_age_system,
             pr_gcf.gcf_dyn_migration_age_serie dn_migration_age_serie,
             pr_gcf.gcf_dyn_migration_age_system dn_migration_age_system,

             pr_s2.s2d_year_survey s2_year,
             pr_s2.s2d_total_crossline s2_crossline,
             pr_s2.s2d_seismic_line s2_line,
             pr_s2.s2d_average_interval s2_interval,
             pr_s2.s2d_record_length_ms s2_record_ms,
             pr_s2.s2d_record_length_ft s2_record_ft,
             pr_s2.s2d_late_method s2_method,
             pr_s2.s2d_img_quality s2_img,
             pr_s2.s2d_formation_thickness,
             pr_s2.s2d_gross_thickness,

             pr_s3.s3d_year_survey s3_year,
             pr_s3.s3d_coverage_area s3_coverage,
             pr_s3.s3d_frequency s3_frequency,
             pr_s3.s3d_frequency_lateral s3_lateral,
             pr_s3.s3d_frequency_vertical s3_vertical,
             pr_s3.s3d_late_method s3_method,
             pr_s3.s3d_img_quality s3_img,
             pr_s3.s3d_top_depth_ft s3_top_depth_ft,
             pr_s3.s3d_top_depth_ms s3_top_depth_ms,
             pr_s3.s3d_bot_depth_ft s3_bot_depth_ft,
             pr_s3.s3d_bot_depth_ms s3_bot_depth_ms,
             pr_s3.s3d_formation_thickness,
             pr_s3.s3d_gross_thickness,

             pr_gl.sgf_year_survey gl_year,
             pr_gl.sgf_survey_method gl_method,

             pr_gh.sgc_year_survey gh_year,
             pr_gh.sgc_sample_interval gh_interval,
             pr_gh.sgc_number_sample gh_sample,

             pr_gr.sgv_year_survey gr_year,
             pr_gr.sgv_survey_method gr_method,
             pr_gr.sgv_coverage_area gr_coverage,
             pr_gr.sgv_depth_range gr_depth_range,
             pr_gr.sgv_spacing_interval gr_interval,

             pr_el.sel_year_survey el_year,
             pr_el.sel_survey_method el_method,
             pr_el.sel_coverage_area el_coverage,
             pr_el.sel_depth_range el_depth_range,
             pr_el.sel_spacing_interval el_interval,

             pr_rs.rst_year_survey rs_year,
             pr_rs.rst_survey_method rs_method,
             pr_rs.rst_coverage_area rs_coverage,
             pr_rs.rst_depth_range rs_depth_range,
             pr_rs.rst_spacing_interval rs_interval,

             pr_ot.sor_year_survey ot_year,

             pr_s2.s2d_vol_p90_area s2_area_p90,
             pr_s3.s3d_vol_p90_area s3_area_p90,
             pr_gl.sgf_p90_area gl_area_p90,
             pr_gh.sgc_vol_p90_area gh_area_p90,
             pr_gr.sgv_vol_p90_area gr_area_p90,
             pr_el.sel_vol_p90_area el_area_p90,
             pr_rs.rst_vol_p90_area rs_area_p90,
             pr_ot.sor_vol_p90_area ot_area_p90,

             pr_s2.s2d_vol_p50_area s2_area_p50,
             pr_s3.s3d_vol_p50_area s3_area_p50,
             pr_gl.sgf_p50_area gl_area_p50,
             pr_gh.sgc_vol_p50_area gh_area_p50,
             pr_gr.sgv_vol_p50_area gr_area_p50,
             pr_el.sel_vol_p50_area el_area_p50,
             pr_rs.rst_vol_p50_area rs_area_p50,
             pr_ot.sor_vol_p50_area ot_area_p50,

             pr_s2.s2d_vol_p10_area s2_area_p10,
             pr_s3.s3d_vol_p10_area s3_area_p10,
             pr_gl.sgf_p10_area gl_area_p10,
             pr_gh.sgc_vol_p10_area gh_area_p10,
             pr_gr.sgv_vol_p10_area gr_area_p10,
             pr_el.sel_vol_p10_area el_area_p10,
             pr_rs.rst_vol_p10_area rs_area_p10,
             pr_ot.sor_vol_p10_area ot_area_p10,

             pr_s2.s2d_net_p90_thickness s2_net_p90,
             pr_s2.s2d_net_p50_thickness s2_net_p50,
             pr_s2.s2d_net_p10_thickness s2_net_p10,
             pr_s3.s3d_net_p90_thickness s3_net_p90,
             pr_s3.s3d_net_p50_thickness s3_net_p50,
             pr_s3.s3d_net_p10_thickness s3_net_p10,

             dr.dr_por_p90,
             dr.dr_por_p50,
             dr.dr_por_p10,
             dr.dr_satur_p90,
             dr.dr_satur_p50,
             dr.dr_satur_p10,

             wl.wl_name,
             wl.wl_latitude,
             wl.wl_longitude,
             wl.wl_type,
             wl.wl_result,
             wl.wl_shore,
             wl.wl_terrain,
             wl.wl_status,
             wl.wl_formation,
             wl.wl_date_complete,
             wl.wl_target_depth_tvd,
             wl.wl_target_depth_md,
             wl.wl_actual_depth,
             wl.wl_target_play,
             wl.wl_actual_play,
             wl.wl_number_mdt,
             wl.wl_number_rft,
             wl.wl_res_pressure,
             wl.wl_last_pressure,
             wl.wl_pressure_gradient,
             wl.wl_last_temp,
             wl.wl_integrity,

             wz.zone_name,
             wz.zone_area,
             wz.zone_result,
             wz.zone_hc_oil_show,
             wz.zone_hc_gas_show,
             wz.zone_thickness,
             wz.zone_depth,
             wz.zone_perforation,
             wz.zone_well_duration,
             wz.zone_initial_flow,
             wz.zone_initial_shutin,
             wz.zone_tubing_size,
             wz.zone_initial_temp,
             wz.zone_initial_pressure,
             wz.zone_well_formation,

             wz.zone_clastic_p90_por,
             wz.zone_clastic_p50_por,
             wz.zone_clastic_p10_por,
             wz.zone_clastic_p90_satur,
             wz.zone_clastic_p50_satur,
             wz.zone_clastic_p10_satur,

             wz.zone_carbo_p90_por,
             wz.zone_carbo_p50_por,
             wz.zone_carbo_p10_por,
             wz.zone_carbo_p90_satur,
             wz.zone_carbo_p50_satur,
             wz.zone_carbo_p10_satur,

             wz.zone_fvf_oil_p90,
             wz.zone_fvf_oil_p50,
             wz.zone_fvf_oil_p10,
             wz.zone_fvf_gas_p90,
             wz.zone_fvf_gas_p50,
             wz.zone_fvf_gas_p10

            FROM
             rsc_play pl
             LEFT JOIN rsc_prospect pr ON pr.play_id = pl.play_id
             LEFT JOIN rsc_drillable dr ON dr.prospect_id = pr.prospect_id AND pr.prospect_type = 'drillable'

             LEFT JOIN rsc_gcf pl_gcf ON pl_gcf.gcf_id = pl.gcf_id
             LEFT JOIN rsc_gcf pr_gcf ON pr_gcf.gcf_id = pr.gcf_id

             LEFT JOIN rsc_seismic_2d pr_s2 ON pr_s2.prospect_id = pr.prospect_id
             LEFT JOIN rsc_seismic_3d pr_s3 ON pr_s3.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geological pr_gl ON pr_gl.prospect_id = pr.prospect_id
             LEFT JOIN rsc_geochemistry pr_gh ON pr_gh.prospect_id = pr.prospect_id
             LEFT JOIN rsc_gravity pr_gr ON pr_gr.prospect_id = pr.prospect_id
             LEFT JOIN rsc_electromagnetic pr_el ON pr_el.prospect_id = pr.prospect_id
             LEFT JOIN rsc_resistivity pr_rs ON pr_rs.prospect_id = pr.prospect_id
             LEFT JOIN rsc_other pr_ot ON pr_ot.prospect_id = pr.prospect_id

             LEFT JOIN rsc_well wl ON wl.prospect_id = pr.prospect_id
             LEFT JOIN rsc_wellzone wz ON wz.wl_id = wl.wl_id

             LEFT JOIN adm_basin bs ON bs.basin_id = pl.basin_id
             LEFT JOIN adm_province pv ON pv.province_id = pl.province_id
             LEFT JOIN adm_working_area wk ON wk.wk_id = pl.wk_id
             LEFT JOIN adm_psc pc ON pc.wk_id = wk.wk_id
             LEFT JOIN adm_kkks ks ON ks.psc_id = pc.psc_id

            WHERE
             pl.wk_id <> 'WK0000'
             AND pl.wk_id <> 'WK9999'
             AND pr.prospect_is_deleted = 0
        ";

        $header = array(
            'Tahun RPS',
            'Basin',
            'Provinsi',
            'KKKS',
            'WKID',
            'Nama WK',
            'Status WK',
            'Nama Play',
            'Status Sumberdaya',

            'Closure',
            'Struktur',

            'Latitude',
            'Longitude',
            'Clarified by',
            'Onshore or Offshore',
            'Terrain',
            'Near Field',
            'Near Infrastructure',

            'GCF Source Rock',
            'GCF Source Age',
            'GCF Source Formation',
            'GCF Kerogen Type',
            'GCF Capacity (TOC)',
            'GCF Source Heat Flow Unit',
            'GCF Distribution',
            'GCF Continuity',
            'GCF Maturity',
            'GCF Present of Other Source Rock',
            'GCF Source Rock Remark',
            'GCF Reservoir',
            'GCF Reservoir Age',
            'GCF Reservoir Formation',
            'GCF Depositional Setting',
            'GCF Distribution',
            'GCF Continuity',
            'GCF Lithology',
            'GCF Primary Porosity',
            'GCF Secondary Porosity',
            'GCF Reservoir Remark',
            'GCF Trap',
            'GCF Sealing Age',
            'GCF Sealing Formation',
            'GCF Sealing Distribution',
            'GCF Sealing Continuity',
            'GCF Sealing Type',
            'GCF Trapping Age',
            'GCF Trapping Geometry',
            'GCF Trapping Type',
            'GCF Closure Type',
            'GCF Trap Remark',
            'GCF Dynamic',
            'GCF Authenticate Migration',
            'GCF Trap Posistion due to Kitchen',
            'GCF Tectonic Order to Establish Petroleum System',
            'GCF Earliest Tectonic Regime',
            'GCF Latest Tectonic Regime',
            'GCF Preservation',
            'GCF Migration Pathways',
            'GCF Estimation Migration Age',
            'GCF Dynamic Remark',

            'Lead Survey 2D Acquisition Year',
            'Lead Survey 2D Total Crossline',
            'Lead Survey 2D Average Spacing Parallel Intervall',
            'Lead Survey 2D Latest Processing Method',
            'Lead Survey 2D Seismic Image Quality',
            'Lead Survey 2D Remark',

            'Lead Survey Geological Acquisition Year',
            'Lead Survey Geological Survey Method',
            'Lead Survey Geological Total Coverage Area',
            'Lead Survey Geological Remark',
            'Lead Survey Geochemistry Acquisition Year',
            'Lead Survey Geochemistry Interval Samples Range',
            'Lead Survey Geochemistry Remark',

            'Lead Survey Gravity Acquisition Year',
            'Lead Survey Gravity Survey Method',
            'Lead Survey Gravity Total Coverage Area',
            'Lead Survey Gravity Remark',

            'Lead Survey Electromagnetic Acquisition Year',
            'Lead Survey Electromagnetic Survey Method',
            'Lead Survey Electromagnetic Remark',

            'Lead Survey Resistivity Acquisition Year',
            'Lead Survey Resistivity Survey Method',
            'Lead Survey Resistivity Remark',

            'Lead Survey Other Acquisition Year',
            'Lead Survey Other Remark',

            'Lead Low Estimate',
            'Lead Best Estimate',
            'Lead High Estimate',

            'Prospect Survey 2D Acquisition Year',
            'Prospect Survey 2D Total Crossline',
            'Prospect Survey 2D Seismic Line Total Length',
            'Prospect Survey 2D Average Spacing Intervall',
            'Prospect Survey 2D Record Length',
            'Prospect Survey 2D Latest Processing Method',
            'Prospect Survey 2D Image Quality',
            'Prospect Survey 2D Formation Thickness',
            'Prospect Survey 2D Gross Thickness',

            'Prospect Survey 3D Acquisition Year',
            'Prospect Survey 3D Total Coverage Area',
            'Prospect Survey 3D Dominant Frequency at Reservoir Target',
            'Prospect Survey 3D Lateral Seismic Resolution',
            'Prospect Survey 3D Vertical Seismic Resolution',
            'Prospect Survey 3D Latest Processing Method',
            'Prospect Survey 3D Image Quality',
            'Prospect Survey 3D Top Reservoir Target Depth',
            'Prospect Survey 3D Bottom Reservoir Target Depth',
            'Prospect Survey 3D Formation Thickness',
            'Prospect Survey 3D Gross Thickness',

            'Prospect Survey Geological Acquisition Year',
            'Prospect Survey Geological Survey Method',

            'Prospect Survey Geochemistry Acquisition Year',
            'Prospect Survey Geochemistry Intervall Samples Range',
            'Prospect Survey Geochemistry Total Samples',

            'Prospect Survey Gravity Acquisition Year',
            'Prospect Survey Gravity Survey Method',
            'Prospect Survey Gravity Total Coverage Area',
            'Prospect Survey Gravity Depth Survey Penetration Range',
            'Prospect Survey Gravity Recorder Spacing Intervall',

            'Prospect Survey Electromagnetic Acquisition Year',
            'Prospect Survey Electromagnetic Survey Method',
            'Prospect Survey Electromagnetic Total Coverage Area',
            'Prospect Survey Electromagnetic Depth Survey Penetration Range',
            'Prospect Survey Electromagnetic Recorder Spacing Intervall',

            'Prospect Survey Resistivity Acquisition Year',
            'Prospect Survey Resistivity Survey Method',
            'Prospect Survey Resistivity Total Coverage Area',
            'Prospect Survey Resistivity Depth Survey Penetration Range',
            'Prospect Survey Resistivity Recorder Spacing Intervall',

            'Prospect Survey Other Acquisition Year',

            'Area P90',
            'Area P50',
            'Area P10',
            'Thickness P90',
            'Thickness P50',
            'Thickness P10',
            'Porositas P90',
            'Porositas P50',
            'Porositas P10',
            '1-Sw P90',
            '1-Sw P50',
            '1-Sw P10',
            'OOIP P90',
            'OOIP P50',
            'OOIP P10',
            'Boi P50',
            'STOIP P90',
            'STOIP P50',
            'STOIP P10',
            'OGIP P90',
            'OGIP P50',
            'OGIP P10',

            'Well',
            'Well Formation',
            'Well Result',
            'Bgi P50',
            'IGIP P90',
            'IGIP P50',
            'IGIP P10',
            'GCF',
            'GCF Normalisasi',
        );

        return array('sql' => array('lead' => $ld_sql, 'prospect' => $pr_sql), 'header' => $header);
    }

    public function standard_gcf_val() {
    /* Not implemented yet */

        $data['sr_ker'] = array(
            'pv' => 0.15,
            'I' => 1.0,
            'I/II' => 0.9,
            'II' => 0.8,
            'II/III' => 0.7,
            'III' => 0.6,
            'IV' => 0.1,
            'Unknown' => 0.5);

        $data['sr_toc'] = array(
            'pv' => 0.30,
            '0 - 0.5' => 0.2,
            '0.6 - 1.0' => 0.6,
            '1.1 - 2.0' => 0.7,
            '2.1 - 4.0' => 0.8,
            '> 4' => 1.0,
            'Unknown' => 0.5);

        $data['sr_hfu'] = array(
            'pv' => 0.07,
            '<= 1.0' => 0.5,
            '1.1 - 2.0' => 0.7,
            '2.1 - 3.0' => 0.9,
            '> 3.0' => 1.0,
            'Unknown' => 0.5);

        $data['sr_mat'] = array(
            'pv' => 0.45,
            'Immature' => 0.1,
            'Early Mature' => 0.4,
            'Mature' => 1.0,
            'Over Mature' => 0.8,
            'Unknown' => 0.5);

        $data['sr_otr'] = array(
            'pv' => 0.03,
            'Yes' => 1.0,
            'No' => 0.0,
            'Unknown' => 0.5);

        $data['re_dis'] = array(
            'pv' => 0.2,
            'Single Distribution' => 1.0,
            'Double Distribution' => 0.8,
            'Multiple Distribution' => 0.7,
            'Unknown' => 0.5);

        $data['re_lit'] = array(
            'pv' => 0.2,
            'Siltstone' => 0.5,
            'Sandstone' => 1.0,
            'Reef Carbonate' => 1.0,
            'Platform Carbonate' => 0.5,
            'Coal' => 0.6,
            'Alternate Rocks' => 0.5,
            'Naturally Fractured' => 0.7,
            'Others' => 0.3,
            'Unknown' => 0.5);

        $data['re_sec'] = array(
            'pv' => 0.2,
            'Vugs Porosity' => 0.4,
            'Fracture Porosity' => 0.9,
            'Integrated Porosity' => 1.0,
            'None' => 0.0,
            'Unknown' => 0.5);

        $data['re_pri'] = array(
            'pv' => 0.2,
            '0 - 10' => 0.4,
            '11 - 15' => 0.6,
            '16 - 20' => 0.7,
            '21 - 30' => 0.9,
            '> 30' => 1.0,
            'Unknown' => 0.5);

        $data['tr_sdi'] = array(
            'pv' => 0.35,
            'Single Distribution Impermeable Rocks' => 0.6,
            'Double Distribution Impermeable Rocks' => 0.8,
            'Multiple Distribution Impermeable Rocks' => 1.0,
            'Unknown' => 0.5);

        $data['tr_scn'] = array(
            'pv' => 0.25,
            'Tank' => 0.4,
            'Truncated' => 0.5,
            'Limited Spread' => 0.7,
            'Spread' => 0.9,
            'Extensive Spread' => 1.0,
            'Unknown' => 0.5);

        $data['tr_stp'] = array(
            'pv' => 0.20,
            'Primary' => 0.8,
            'Diagenetic' => 0.9,
            'Alternate Tectonic Mech' => 1.0,
            'Unknown' => 0.5);

        $data['tr_geo'] = array(
            'pv' => 0.10,
            'Anticline Hangingwall' => 0.9,
            'Anticline Footwall' => 1.0,
            'Anticline Pericline' => 0.9,
            'Anticline Dome' => 0.9,
            'Anticline Nose' => 0.9,
            'Anticline Unqualified' => 0.9,
            'Faulted Anticline Hangingwall' => 0.8,
            'Faulted Anticline Footwall' => 0.8,
            'Faulted Anticline Pericline' => 0.8,
            'Faulted Anticline Dome' => 0.8,
            'Faulted Anticline Nose' => 0.8,
            'Faulted Anticline Unqualified' => 0.8,
            'Tilted Hangingwall' => 0.7,
            'Tilted Fault Block Footwall' => 0.7,
            'Tilted Fault Block Terrace' => 0.7,
            'Tilted Fault Block Unqualified' => 0.7,
            'Horst Simple' => 0.6,
            'Horst Faulted' => 0.6,
            'Horst Complex' => 0.6,
            'Horst Unqualified' => 0.6,
            'Wedge Tilted' => 0.5,
            'Wedge Flexured' => 0.5,
            'Wedge Radial' => 0.5,
            'Wedge Marginal' => 0.5,
            'Wedge Faulted' => 0.5,
            'Wedge Onlap' => 0.5,
            'Wedge Subcrop' => 0.5,
            'Wedge Unqualified' => 0.5,
            'Abutment Hangingwall' => 0.4,
            'Abutment Footwall' => 0.4,
            'Abutment Pericline' => 0.4,
            'Abutment Terrace' => 0.4,
            'Abutment Complex' => 0.4,
            'Abutment Unqualified' => 0.4,
            'Irregular Hangingwall' => 0.4,
            'Irregular Footwall' => 0.4,
            'Irregular Pericline' => 0.4,
            'Irregular Dome' => 0.4,
            'Irregular Terrace' => 0.4,
            'Irregular Unqualified' => 0.4,
            'Unknown' => 0.5);

        $data['tr_trp'] = array(
            'pv' => 0.10,
            'Structural Tectonic Extensional' => 0.6,
            'Structural Tectonic Compressional Thin Skinned' => 0.9,
            'Structural Tectonic Compressional Inverted' => 1.0,
            'Structural Tectonic Strike Slip' => 1.0,
            'Structural Tectonic Unqualified' => 0.8,
            'Structural Drape' => 0.8,
            'Structural Compactional' => 0.8,
            'Structural Diapiric Salt' => 0.7,
            'Structural Diapiric Mud' => 0.7,
            'Structural Diapiric Unqualified' => 0.7,
            'Structural Gravitational' => 0.6,
            'Structural Unqualified' => 0.6,
            'Stratigraphic Depositional Pinch-out' => 0.6,
            'Stratigraphic Depositional Shale-out' => 0.6,
            'Stratigraphic Depositional Facies-limited' => 0.6,
            'Stratigraphic Depositional Reef' => 1.0,
            'Stratigraphic Depositional Unqualified' => 0.6,
            'Stratigraphic Unconformity Subcrop' => 0.6,
            'Stratigraphic Unconformity Onlap' => 0.6,
            'Stratigraphic Unconformity Unqualified' => 0.6,
            'Stratigraphic Other Diagenetic' => 0.6,
            'Stratigraphic Other Tar Mat' => 0.6,
            'Stratigraphic Other Gas Hydrate' => 0.6,
            'Stratigraphic Other Gas Permafrost' => 0.6,
            'Stratigraphic Unqualified' => 0.6,
            'Unknown' => 0.5);

        $data['dn_kit'] = array(
            'pv' => 0.40,
            'Very Near (0 - 2 Km)' => 1.0,
            'Near (2 - 5 Km)' => 0.9,
            'Middle (5 - 10 Km)' => 0.8,
            'Long (10 - 20 Km)' => 0.7,
            'Very Long (> 20 Km)' => 0.5,
            'Unknown' => 0.5);

        $data['dn_tec'] = array(
            'pv' => 0.10,
            'Single Order' => 0.7,
            'Multiple Order' => 1.0,
            'Unknown' => 0.5);

        $data['dn_prv'] = array(
            'pv' => 0.25,
            'Not Occur' => 0.2,
            'Occur' => 1.0,
            'Unknown' => 0.5);

        $data['dn_mig'] = array(
            'pv' => 0.25,
            'Vertical' => 0.7,
            'Horizontal' => 0.8,
            'Multiple Directions' => 1.0,
            'Unknown' => 0.5);

        return $data;
    }
}