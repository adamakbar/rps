<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'              => 'RPS | SKK MIGAS',
    'defaultController' => 'web/maingate',

    // preloading 'log' component
    'preload'           => array('log'),

    // autoloading model and component classes
    'import'            => array(
        'application.models.*',
        'application.components.*',
        'ext.galleryManager.models.*',
        'ext.image.Image',
    ),
    'aliases'           => array(
        'xupload' => 'ext.xupload',
    ),
    'modules'           => array(
    ),

    // application components
    'components'        => array(
        'session'      => array(
            'class'   => 'CDbHttpSession',
            'timeout' => 3600,
        ),
        'image'        => array(
            'class'  => 'application.extensions.image.CImageComponent',
            'driver' => 'GD',
        ),
        'user'         => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl'       => array('web/login'),
        ),
        'db'           => array(
            'connectionString' => 'mysql:host=localhost;dbname=rps_skkmigas',
            'emulatePrepare'   => true,
            'username'         => 'root',
            'password'         => '',
            'charset'          => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'web/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'            => array(
        // this is used in contact page
        'adminEmail' => 'sumberdaya@skkmigas.go.id',
    ),
);
